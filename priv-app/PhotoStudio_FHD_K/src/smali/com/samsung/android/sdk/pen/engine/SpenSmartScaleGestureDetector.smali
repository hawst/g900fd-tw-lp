.class Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
.super Landroid/os/Handler;
.source "SpenSmartScaleGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;,
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;,
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;,
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffect2;,
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;,
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;,
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;,
        Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State:[I = null

.field private static final DBG:Z = false

.field private static final HANDLER_TIME:I = 0x0

.field private static final HOLD_FINGER_MAX_DISTANCE:F = 0.5f

.field private static final HOLD_FINGER_MAX_TIME:I = 0x5dc

.field private static final HOLD_HOVER_MAX_TIME:I = 0xc8

.field private static final HOVER_ICON_BOTTOM:I = 0x7

.field private static final HOVER_ICON_BOTTOM_LEFT:I = 0xc

.field private static final HOVER_ICON_LEFT:I = 0x5

.field private static final HOVER_ICON_LEFT_TOP:I = 0x6

.field private static final HOVER_ICON_RIGHT:I = 0x3

.field private static final HOVER_ICON_RIGHT_BOTTOM:I = 0xa

.field private static final HOVER_ICON_TOP:I = 0x1

.field private static final HOVER_ICON_TOP_RIGHT:I = 0x4


# instance fields
.field private final m1CMPixel:F

.field private mBottomScrollRegion:Landroid/graphics/Rect;

.field private mCenterX:F

.field private mCenterY:F

.field private final mDPI:F

.field private mDeltaX:F

.field private mDeltaY:F

.field private final mDensity:F

.field private mDiffX:F

.field private mDiffY:F

.field private mDistanceX:F

.field private mDistanceY:F

.field private mDownTime:J

.field private mDownX:F

.field private mDownY:F

.field private mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

.field private mEdgePaint:Landroid/graphics/Paint;

.field private mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

.field private mEffectFrame:I

.field private mFactor:F

.field private mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

.field private mHoldHoverStartTimeLR:J

.field private mHoldHoverStartTimeTB:J

.field private mHoldStartTime:J

.field private mHorizontalEnterTime:J

.field private mHorizontalResponseTime:I

.field private mHorizontalVelocity:I

.field private mHoverIconType:I

.field private mIsMultiTouch:Z

.field private mIsReleaseEdgeEffectLR:Z

.field private mIsReleaseEdgeEffectTB:Z

.field private mIsReleaseHoverEdgeEffectLR:Z

.field private mIsReleaseHoverEdgeEffectTB:Z

.field private mIsUpdateEEAnimation:Z

.field private mIsUpdateFloating:Z

.field private mIsUpdateFrameBuffer:Z

.field private mLeftScrollRegion:Landroid/graphics/Rect;

.field private mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

.field private mMaxDeltaX:F

.field private mMaxDeltaY:F

.field private mOrigRatio:F

.field private mParentRect:Landroid/graphics/Rect;

.field private mPreEventX:F

.field private mPreEventY:F

.field private mRatio:F

.field private mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

.field private mRequestUpdateFrameBuffer:Ljava/lang/Runnable;

.field private mRightScrollRegion:Landroid/graphics/Rect;

.field private mScrollX:F

.field private mScrollY:F

.field private mSmartScaleRegion:Landroid/graphics/Rect;

.field private mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

.field private mToolAndActionMap:Landroid/util/SparseIntArray;

.field private mTopScrollRegion:Landroid/graphics/Rect;

.field private mUseHorizontalScroll:Z

.field private mUseSmartScale:Z

.field private mUseVerticalScroll:Z

.field private mVerticalEnterTime:J

.field private mVerticalResponseTime:I

.field private mVerticalVelocity:I

.field private mZoomOutResponseTime:I

.field private mZoomRatio:F


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State()[I
    .locals 3

    .prologue
    .line 16
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->values()[Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->EDGE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->READY_FOR_ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->SCROLL_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_5
    :try_start_5
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_6
    :try_start_6
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMIN_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_7
    :try_start_7
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_8
    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;FF)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dpi"    # F
    .param p3, "density"    # F

    .prologue
    const-wide/16 v6, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 198
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 119
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 133
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    .line 134
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    .line 135
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    .line 140
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    .line 141
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    .line 142
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    .line 143
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mSmartScaleRegion:Landroid/graphics/Rect;

    .line 144
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    .line 145
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    .line 146
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    .line 147
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    .line 150
    const/high16 v0, 0x3e000000    # 0.125f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    .line 151
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    .line 152
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalResponseTime:I

    .line 153
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalEnterTime:J

    .line 154
    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalVelocity:I

    .line 155
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalResponseTime:I

    .line 156
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalEnterTime:J

    .line 157
    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalVelocity:I

    .line 159
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownTime:J

    .line 160
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownX:F

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownY:F

    .line 161
    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventX:F

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventY:F

    .line 163
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    .line 169
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    .line 171
    new-instance v0, Landroid/util/SparseIntArray;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    .line 179
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

    .line 180
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateFrameBuffer:Ljava/lang/Runnable;

    .line 182
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    .line 183
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFloating:Z

    .line 184
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsMultiTouch:Z

    .line 185
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 187
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    .line 188
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    .line 189
    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    .line 190
    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    .line 191
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectLR:Z

    .line 192
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectTB:Z

    .line 193
    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldStartTime:J

    .line 199
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDensity:F

    .line 200
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    .line 201
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    const v1, 0x3ec9932d

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->m1CMPixel:F

    .line 203
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    .line 204
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    .line 206
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgePaint:Landroid/graphics/Paint;

    .line 207
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 208
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 210
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

    .line 219
    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateFrameBuffer:Ljava/lang/Runnable;

    .line 228
    return-void
.end method

.method private Fling()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/high16 v8, 0x42c80000    # 100.0f

    const/high16 v7, 0x3f800000    # 1.0f

    const v6, 0x3e19999a    # 0.15f

    const/high16 v5, -0x3d380000    # -100.0f

    .line 1318
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    mul-float v0, v4, v6

    .line 1319
    .local v0, "currentDistanceX":F
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    mul-float v1, v4, v6

    .line 1321
    .local v1, "currentDistanceY":F
    cmpg-float v4, v0, v5

    if-gez v4, :cond_3

    .line 1322
    const/high16 v0, -0x3d380000    # -100.0f

    .line 1327
    :cond_0
    :goto_0
    cmpg-float v4, v1, v5

    if-gez v4, :cond_4

    .line 1328
    const/high16 v1, -0x3d380000    # -100.0f

    .line 1333
    :cond_1
    :goto_1
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    sub-float/2addr v4, v0

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    .line 1334
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    sub-float/2addr v4, v1

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    .line 1336
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    sub-float v2, v4, v0

    .line 1337
    .local v2, "deltaX":F
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    sub-float v3, v4, v1

    .line 1339
    .local v3, "deltaY":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v4, v4, v7

    if-gez v4, :cond_6

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v4, v4, v7

    if-gez v4, :cond_6

    .line 1340
    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 1341
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1342
    float-to-int v4, v2

    int-to-float v4, v4

    float-to-int v5, v3

    int-to-float v5, v5

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangePan(FF)V

    .line 1343
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v4, :cond_2

    .line 1344
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    .line 1356
    :cond_2
    :goto_2
    return-void

    .line 1323
    .end local v2    # "deltaX":F
    .end local v3    # "deltaY":F
    :cond_3
    cmpl-float v4, v0, v8

    if-lez v4, :cond_0

    .line 1324
    const/high16 v0, 0x42c80000    # 100.0f

    goto :goto_0

    .line 1329
    :cond_4
    cmpl-float v4, v1, v8

    if-lez v4, :cond_1

    .line 1330
    const/high16 v1, 0x42c80000    # 100.0f

    goto :goto_1

    .line 1347
    .restart local v2    # "deltaX":F
    .restart local v3    # "deltaY":F
    :cond_5
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1348
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v4, :cond_2

    .line 1349
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v4, v9}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdate(Z)V

    goto :goto_2

    .line 1353
    :cond_6
    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangePan(FF)V

    .line 1354
    const-wide/16 v4, 0x0

    invoke-virtual {p0, v9, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2
.end method

.method private ReadyForZoomout()V
    .locals 2

    .prologue
    .line 1277
    const-string v0, "SPen_Library"

    const-string v1, "[SMART SCALE] READY FOR ZOOM OUT()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1278
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 1279
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ZoomOut()V

    .line 1280
    return-void
.end method

.method private Scroll()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1195
    const/4 v2, 0x1

    .line 1196
    .local v2, "sendMessage":Z
    const/4 v3, 0x0

    .line 1198
    .local v3, "setPan":Z
    const/4 v0, 0x0

    .line 1199
    .local v0, "dx":I
    const/4 v1, 0x0

    .line 1201
    .local v1, "dy":I
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpl-float v4, v4, v7

    if-eqz v4, :cond_0

    .line 1202
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1206
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalVelocity:I

    sub-int/2addr v1, v4

    .line 1208
    :cond_0
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v4, :cond_1

    .line 1209
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1213
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalVelocity:I

    add-int/2addr v1, v4

    .line 1215
    :cond_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    cmpl-float v4, v4, v7

    if-eqz v4, :cond_2

    .line 1216
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1220
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalVelocity:I

    sub-int/2addr v0, v4

    .line 1222
    :cond_2
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    .line 1223
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1227
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalVelocity:I

    add-int/2addr v0, v4

    .line 1230
    :cond_3
    if-eqz v0, :cond_5

    .line 1234
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    int-to-float v5, v0

    add-float/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    .line 1235
    const/4 v2, 0x1

    .line 1236
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    cmpg-float v4, v4, v7

    if-gez v4, :cond_9

    .line 1237
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    .line 1238
    const/4 v2, 0x0

    .line 1243
    :cond_4
    :goto_0
    const/4 v3, 0x1

    .line 1246
    :cond_5
    if-eqz v1, :cond_7

    .line 1250
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    int-to-float v5, v1

    add-float/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    .line 1251
    const/4 v2, 0x1

    .line 1252
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpg-float v4, v4, v7

    if-gez v4, :cond_a

    .line 1253
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    .line 1254
    const/4 v2, 0x0

    .line 1259
    :cond_6
    :goto_1
    const/4 v3, 0x1

    .line 1262
    :cond_7
    if-eqz v3, :cond_8

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v4, :cond_8

    .line 1266
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangePan(FF)V

    .line 1269
    :cond_8
    if-eqz v2, :cond_b

    .line 1270
    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-virtual {p0, v4, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    .line 1274
    :goto_2
    return-void

    .line 1239
    :cond_9
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_4

    .line 1240
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    .line 1241
    const/4 v2, 0x0

    goto :goto_0

    .line 1255
    :cond_a
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_6

    .line 1256
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    .line 1257
    const/4 v2, 0x0

    goto :goto_1

    .line 1272
    :cond_b
    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    goto :goto_2
.end method

.method private ZoomIn()V
    .locals 4

    .prologue
    .line 1283
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 1284
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 1285
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v0, :cond_0

    .line 1286
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    float-to-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    float-to-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangeScale(FFF)V

    .line 1287
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    .line 1288
    const-string v0, "SPen_Library"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[SMART SCALE] ZOOM IN(), RATIO : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", STATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1299
    :cond_0
    :goto_0
    return-void

    .line 1291
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMIN_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 1292
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    .line 1293
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v0, :cond_2

    .line 1294
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangeScale(FFF)V

    .line 1296
    :cond_2
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    .line 1297
    const-string v0, "SPen_Library"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[SMART SCALE] ZOOM IN(), RATIO : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", STATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private ZoomOut()V
    .locals 4

    .prologue
    .line 1302
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    .line 1303
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 1304
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    .line 1305
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 1307
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_1

    .line 1308
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangeScale(FFF)V

    .line 1309
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    .line 1314
    :goto_0
    const-string v0, "SPen_Library"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[SMART SCALE] ZOOM OUT(), RATIO : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", STATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1315
    return-void

    .line 1311
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    float-to-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    float-to-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangeScale(FFF)V

    .line 1312
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    return-object v0
.end method

.method private calculateEdgeEffect(Landroid/view/MotionEvent;)V
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 879
    if-nez p1, :cond_1

    .line 1062
    :cond_0
    :goto_0
    return-void

    .line 882
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    if-lez v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    if-lez v6, :cond_0

    .line 885
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 886
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 887
    .local v1, "eventX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 888
    .local v2, "eventY":F
    const/4 v4, 0x0

    .line 889
    .local v4, "pullLR":F
    const/4 v5, 0x0

    .line 890
    .local v5, "pullTB":F
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 891
    const/16 v6, 0xa

    if-eq v0, v6, :cond_26

    .line 892
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-boolean v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->enable:Z

    if-eqz v6, :cond_16

    .line 893
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    if-ne v6, v7, :cond_11

    .line 894
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    const/high16 v7, 0x3f800000    # 1.0f

    cmpg-float v6, v6, v7

    if-gtz v6, :cond_f

    .line 895
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_e

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 896
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    .line 897
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iput v1, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    .line 947
    :cond_2
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-boolean v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->enable:Z

    if-eqz v6, :cond_1f

    .line 948
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    if-ne v6, v7, :cond_1a

    .line 949
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    const/high16 v7, 0x3f800000    # 1.0f

    cmpg-float v6, v6, v7

    if-gtz v6, :cond_18

    .line 950
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_17

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 951
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    .line 952
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iput v2, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    .line 1001
    :cond_3
    :goto_2
    const/4 v6, 0x0

    cmpl-float v6, v4, v6

    if-lez v6, :cond_22

    .line 1002
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_20

    .line 1003
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    .line 1018
    :cond_4
    :goto_3
    const/4 v6, 0x0

    cmpl-float v6, v5, v6

    if-lez v6, :cond_25

    .line 1019
    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_23

    .line 1020
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    .line 1034
    :cond_5
    :goto_4
    const/4 v3, 0x0

    .line 1035
    .local v3, "isPull":Z
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    if-nez v6, :cond_6

    const/4 v6, 0x0

    cmpl-float v6, v4, v6

    if-lez v6, :cond_6

    .line 1036
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6, v4}, Landroid/widget/EdgeEffect;->onPull(F)V

    .line 1037
    const/4 v3, 0x1

    .line 1039
    :cond_6
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    if-nez v6, :cond_7

    const/4 v6, 0x0

    cmpl-float v6, v5, v6

    if-lez v6, :cond_7

    .line 1040
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6, v5}, Landroid/widget/EdgeEffect;->onPull(F)V

    .line 1041
    const/4 v3, 0x1

    .line 1043
    :cond_7
    if-nez v3, :cond_d

    .line 1044
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-lez v6, :cond_a

    .line 1045
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_9

    :cond_8
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    .line 1046
    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1047
    :cond_9
    const/16 v6, 0xa

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1049
    :cond_a
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-lez v6, :cond_d

    .line 1050
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_b

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_c

    :cond_b
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_d

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    .line 1051
    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1052
    :cond_c
    const/16 v6, 0xa

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1060
    .end local v3    # "isPull":Z
    :cond_d
    :goto_5
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setHoverIcon(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 898
    :cond_e
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v6

    if-nez v6, :cond_2

    .line 899
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 900
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    goto/16 :goto_1

    .line 903
    :cond_f
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v7, v8

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_2

    .line 904
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_10

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 905
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    .line 906
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iput v1, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    goto/16 :goto_1

    .line 907
    :cond_10
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v6

    if-nez v6, :cond_2

    .line 908
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 909
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    goto/16 :goto_1

    .line 912
    :cond_11
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    if-ne v6, v7, :cond_13

    .line 913
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_13

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 914
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v6, v6, v7

    if-lez v6, :cond_12

    .line 915
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    .line 916
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 917
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 918
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    goto/16 :goto_1

    .line 920
    :cond_12
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    sub-float v6, v1, v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    div-float v4, v6, v7

    .line 921
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 922
    float-to-double v6, v4

    const-wide v8, 0x3fb999999999999aL    # 0.1

    cmpg-double v6, v6, v8

    if-gez v6, :cond_2

    .line 923
    const v4, 0x3dcccccd    # 0.1f

    .line 926
    goto/16 :goto_1

    :cond_13
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    if-ne v6, v7, :cond_15

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_15

    .line 927
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 928
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v7, v8

    cmpg-float v6, v6, v7

    if-gez v6, :cond_14

    .line 929
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    .line 930
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 931
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 932
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    goto/16 :goto_1

    .line 934
    :cond_14
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    sub-float/2addr v6, v1

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    div-float v4, v6, v7

    .line 935
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 936
    float-to-double v6, v4

    const-wide v8, 0x3fb999999999999aL    # 0.1

    cmpg-double v6, v6, v8

    if-gez v6, :cond_2

    .line 937
    const v4, 0x3dcccccd    # 0.1f

    .line 940
    goto/16 :goto_1

    .line 941
    :cond_15
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    goto/16 :goto_1

    .line 944
    :cond_16
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    goto/16 :goto_1

    .line 953
    :cond_17
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v6

    if-nez v6, :cond_3

    .line 954
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 955
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    goto/16 :goto_2

    .line 957
    :cond_18
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v7, v8

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_3

    .line 958
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_19

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 959
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    .line 960
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iput v2, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    goto/16 :goto_2

    .line 961
    :cond_19
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v6

    if-nez v6, :cond_3

    .line 962
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 963
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    goto/16 :goto_2

    .line 966
    :cond_1a
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    if-ne v6, v7, :cond_1c

    .line 967
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_1c

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 968
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v6, v6, v7

    if-lez v6, :cond_1b

    .line 969
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    .line 970
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 971
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 972
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    goto/16 :goto_2

    .line 974
    :cond_1b
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    sub-float v6, v2, v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    div-float v5, v6, v7

    .line 975
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 976
    float-to-double v6, v5

    const-wide v8, 0x3fb999999999999aL    # 0.1

    cmpg-double v6, v6, v8

    if-gez v6, :cond_3

    .line 977
    const v5, 0x3dcccccd    # 0.1f

    .line 980
    goto/16 :goto_2

    :cond_1c
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    if-ne v6, v7, :cond_1e

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v6, :cond_1e

    .line 981
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    float-to-int v7, v1

    float-to-int v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 982
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v7, v8

    cmpg-float v6, v6, v7

    if-gez v6, :cond_1d

    .line 983
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    .line 984
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 985
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 986
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    goto/16 :goto_2

    .line 988
    :cond_1d
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    sub-float/2addr v6, v2

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    div-float v5, v6, v7

    .line 989
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 990
    float-to-double v6, v5

    const-wide v8, 0x3fb999999999999aL    # 0.1

    cmpg-double v6, v6, v8

    if-gez v6, :cond_3

    .line 991
    const v5, 0x3dcccccd    # 0.1f

    .line 994
    goto/16 :goto_2

    .line 995
    :cond_1e
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    goto/16 :goto_2

    .line 998
    :cond_1f
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v7, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v7, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    goto/16 :goto_2

    .line 1005
    :cond_20
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0xc8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_4

    .line 1006
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    if-nez v6, :cond_21

    .line 1007
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1008
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1010
    :cond_21
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    goto/16 :goto_3

    .line 1014
    :cond_22
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    .line 1015
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    goto/16 :goto_3

    .line 1022
    :cond_23
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0xc8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_5

    .line 1023
    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    if-nez v6, :cond_24

    .line 1024
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1025
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1027
    :cond_24
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    goto/16 :goto_4

    .line 1031
    :cond_25
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    .line 1032
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    goto/16 :goto_4

    .line 1056
    :cond_26
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1057
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v6}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1058
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    goto/16 :goto_5
.end method

.method private getHoverIconId(II)I
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v4, 0x0

    .line 804
    const/4 v1, 0x0

    .line 805
    .local v1, "type":I
    const/4 v0, 0x0

    .line 806
    .local v0, "pos":I
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    invoke-virtual {v3, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 807
    add-int/lit8 v0, v0, 0x5

    .line 809
    :cond_0
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    invoke-virtual {v3, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 810
    add-int/lit8 v0, v0, 0x1

    .line 812
    :cond_1
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    invoke-virtual {v3, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 813
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 814
    add-int/lit8 v0, v0, 0x3

    .line 816
    :cond_2
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    invoke-virtual {v3, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 817
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    .line 818
    add-int/lit8 v0, v0, 0x7

    .line 820
    :cond_3
    if-nez v0, :cond_4

    move v2, v1

    .line 850
    .end local v1    # "type":I
    .local v2, "type":I
    :goto_0
    return v2

    .line 824
    .end local v2    # "type":I
    .restart local v1    # "type":I
    :cond_4
    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v2, v1

    .line 850
    .end local v1    # "type":I
    .restart local v2    # "type":I
    goto :goto_0

    .line 826
    .end local v2    # "type":I
    .restart local v1    # "type":I
    :pswitch_1
    const/16 v1, 0x11

    .line 827
    goto :goto_1

    .line 829
    :pswitch_2
    const/16 v1, 0xb

    .line 830
    goto :goto_1

    .line 832
    :pswitch_3
    const/16 v1, 0xd

    .line 833
    goto :goto_1

    .line 835
    :pswitch_4
    const/16 v1, 0xf

    .line 836
    goto :goto_1

    .line 838
    :pswitch_5
    const/16 v1, 0x12

    .line 839
    goto :goto_1

    .line 841
    :pswitch_6
    const/16 v1, 0xc

    .line 842
    goto :goto_1

    .line 844
    :pswitch_7
    const/16 v1, 0xe

    .line 845
    goto :goto_1

    .line 847
    :pswitch_8
    const/16 v1, 0x10

    goto :goto_1

    .line 824
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_6
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method private isLollipopOS()Z
    .locals 2

    .prologue
    .line 1359
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 1360
    const/4 v0, 0x1

    .line 1362
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onChangePan(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v1, 0x0

    .line 1174
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v0, :cond_0

    .line 1175
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    .line 1176
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFloating:Z

    .line 1177
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1178
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onChangePan(FF)V

    .line 1180
    :cond_0
    return-void
.end method

.method private onChangeScale(FFF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "ratio"    # F

    .prologue
    const/4 v1, 0x0

    .line 1183
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v0, :cond_0

    .line 1184
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    .line 1185
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFloating:Z

    .line 1186
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1187
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onChangeScale(FFF)V

    .line 1189
    :cond_0
    return-void
.end method

.method private releaseEdgeEffects()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1065
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1066
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1067
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1069
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1070
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 1071
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 1073
    :cond_1
    return-void
.end method

.method private setHoverIcon(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 855
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/16 v3, 0xa

    if-eq v2, v3, :cond_0

    .line 856
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->getHoverIconId(II)I

    move-result v1

    .line 861
    .local v1, "type":I
    :goto_0
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    invoke-static {v2, v1}, Landroid/view/PointerIcon;->setIcon(II)V

    .line 862
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoverIconType:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 866
    :goto_1
    return-void

    .line 858
    .end local v1    # "type":I
    :cond_0
    const/4 v1, 0x1

    .restart local v1    # "type":I
    goto :goto_0

    .line 863
    :catch_0
    move-exception v0

    .line 864
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 231
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 233
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    if-eqz v0, :cond_1

    .line 236
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->close()V

    .line 237
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    .line 239
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    if-eqz v0, :cond_2

    .line 240
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->close()V

    .line 241
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    .line 243
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

    if-eqz v0, :cond_3

    .line 244
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 246
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateFrameBuffer:Ljava/lang/Runnable;

    if-eqz v0, :cond_4

    .line 247
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateFrameBuffer:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 249
    :cond_4
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgePaint:Landroid/graphics/Paint;

    .line 250
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 304
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    if-nez v2, :cond_1

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    const/4 v0, 0x0

    .line 309
    .local v0, "isUpdate":Z
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    if-ne v2, v3, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    if-nez v2, :cond_6

    .line 310
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 311
    .local v1, "restoreCount":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v4, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 312
    const/high16 v2, 0x43870000    # 270.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 313
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 314
    const/4 v0, 0x1

    .line 316
    :cond_2
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 326
    .end local v1    # "restoreCount":I
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    if-ne v2, v3, :cond_8

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    if-nez v2, :cond_8

    .line 327
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 328
    .restart local v1    # "restoreCount":I
    invoke-virtual {p1, v4, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 329
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 330
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 331
    const/4 v0, 0x1

    .line 333
    :cond_4
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 343
    .end local v1    # "restoreCount":I
    :cond_5
    :goto_2
    if-eqz v0, :cond_a

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

    if-eqz v2, :cond_a

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    if-eqz v2, :cond_a

    .line 344
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 345
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    .line 346
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateCanvasLayer:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 317
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    if-nez v2, :cond_3

    .line 318
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 319
    .restart local v1    # "restoreCount":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 320
    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 321
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 322
    const/4 v0, 0x1

    .line 324
    :cond_7
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_1

    .line 334
    .end local v1    # "restoreCount":I
    :cond_8
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    if-nez v2, :cond_5

    .line 335
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 336
    .restart local v1    # "restoreCount":I
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 337
    const/high16 v2, 0x43340000    # 180.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 338
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 339
    const/4 v0, 0x1

    .line 341
    :cond_9
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_2

    .line 347
    .end local v1    # "restoreCount":I
    :cond_a
    if-nez v0, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    if-eqz v2, :cond_0

    .line 348
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateFrameBuffer:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 349
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRequestUpdateFrameBuffer:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->post(Ljava/lang/Runnable;)Z

    .line 350
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFrameBuffer:Z

    goto/16 :goto_0
.end method

.method public enableHorizontalSmartScroll(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 0
    .param p1, "use"    # Z
    .param p2, "leftScrollRegion"    # Landroid/graphics/Rect;
    .param p3, "rightScrollRegion"    # Landroid/graphics/Rect;
    .param p4, "responseTime"    # I
    .param p5, "velocity"    # I

    .prologue
    .line 287
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    .line 288
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    .line 289
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    .line 290
    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalResponseTime:I

    .line 291
    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalVelocity:I

    .line 292
    return-void
.end method

.method public enableSmartScale(ZLandroid/graphics/Rect;IIF)V
    .locals 3
    .param p1, "use"    # Z
    .param p2, "region"    # Landroid/graphics/Rect;
    .param p3, "effectFrame"    # I
    .param p4, "zoomOutResponseTime"    # I
    .param p5, "zoomRatio"    # F

    .prologue
    .line 261
    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    if-nez v0, :cond_1

    .line 279
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    const-string v0, "SPen_Library"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[SMART SCALE] enableSmartScale use : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    .line 268
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mSmartScaleRegion:Landroid/graphics/Rect;

    .line 269
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEffectFrame:I

    .line 270
    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomOutResponseTime:I

    .line 271
    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    .line 273
    if-nez p1, :cond_0

    .line 274
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMIN_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v0, v1, :cond_0

    .line 275
    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    float-to-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    float-to-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangeScale(FFF)V

    .line 276
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    goto :goto_0
.end method

.method public enableVerticalSmartScroll(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 0
    .param p1, "use"    # Z
    .param p2, "topScrollRegion"    # Landroid/graphics/Rect;
    .param p3, "bottomScrollRegion"    # Landroid/graphics/Rect;
    .param p4, "responseTime"    # I
    .param p5, "velocity"    # I

    .prologue
    .line 296
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    .line 297
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    .line 298
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    .line 299
    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalResponseTime:I

    .line 300
    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalVelocity:I

    .line 301
    return-void
.end method

.method public getCenterX()F
    .locals 1

    .prologue
    .line 1162
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    return v0
.end method

.method public getCenterY()F
    .locals 1

    .prologue
    .line 1166
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    return v0
.end method

.method public getState()Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;
    .locals 1

    .prologue
    .line 1158
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 356
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State()[I

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 398
    :goto_0
    :pswitch_0
    return-void

    .line 366
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ZoomIn()V

    goto :goto_0

    .line 372
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->Scroll()V

    goto :goto_0

    .line 384
    :pswitch_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ReadyForZoomout()V

    goto :goto_0

    .line 390
    :pswitch_4
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ZoomOut()V

    goto :goto_0

    .line 393
    :pswitch_5
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->Fling()V

    goto :goto_0

    .line 356
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method protected isEdgeEffectWorking()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 869
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    .line 875
    :cond_0
    :goto_0
    return v0

    .line 872
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 875
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isUpdateFloating()Z
    .locals 1

    .prologue
    .line 282
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFloating:Z

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 6
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/high16 v5, 0x40800000    # 4.0f

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 1076
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    if-ne v0, v1, :cond_6

    .line 1077
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    mul-float/2addr v0, v5

    div-float v0, p3, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    .line 1078
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    mul-float/2addr v0, v5

    div-float v0, p4, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    .line 1080
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    sub-float/2addr v0, v1

    cmpg-float v0, v0, v4

    if-gez v0, :cond_4

    .line 1081
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    .line 1086
    :cond_0
    :goto_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    sub-float/2addr v0, v1

    cmpg-float v0, v0, v4

    if-gez v0, :cond_5

    .line 1087
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    .line 1092
    :cond_1
    :goto_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    float-to-double v0, v0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    float-to-double v0, v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_3

    .line 1093
    :cond_2
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 1094
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->Fling()V

    .line 1105
    :cond_3
    :goto_2
    return-void

    .line 1082
    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1083
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    goto :goto_0

    .line 1088
    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1089
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    goto :goto_1

    .line 1097
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v0, :cond_3

    .line 1098
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    if-ne v0, v1, :cond_7

    .line 1099
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onFlick(I)Z

    goto :goto_2

    .line 1100
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    if-ne v0, v1, :cond_3

    .line 1101
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onFlick(I)Z

    goto :goto_2
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)V
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x2

    const/high16 v12, 0x3f800000    # 1.0f

    const-wide/16 v10, -0x1

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 401
    if-nez p1, :cond_1

    .line 571
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 405
    .local v0, "action":I
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    if-ne v3, v6, :cond_0

    .line 406
    const/16 v3, 0x9

    if-ne v0, v3, :cond_2

    .line 407
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    .line 408
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    .line 409
    iput-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    .line 410
    iput-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    .line 412
    :cond_2
    const/16 v3, 0x9

    if-ne v0, v3, :cond_a

    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    if-eqz v3, :cond_a

    .line 413
    const-string v3, "SPen_Library"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[SMART SCALE] ON HOVER ENTER. STATE : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->READY_FOR_ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v3, v4, :cond_3

    .line 416
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomOutResponseTime:I

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->removeMessages(I)V

    .line 418
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_4

    .line 419
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->EDGE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v3, v4, :cond_0

    .line 420
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v3

    if-ne v3, v6, :cond_5

    .line 421
    const-string v3, "SPen_Library"

    const-string v4, "[SMART SCALE] ON HOVER ENTER. BUTTON_SECONDARY"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 425
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    .line 426
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    .line 428
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_6

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_6

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->EDGE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v3, v4, :cond_7

    .line 429
    :cond_6
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    .line 432
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mSmartScaleRegion:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 433
    const-string v3, "SPen_Library"

    const-string v4, "[SMART SCALE] ON HOVER ENTER. SMART REGION CONTAINS"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_8

    .line 435
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEffectFrame:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    .line 439
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ZoomIn()V

    goto/16 :goto_0

    .line 437
    :cond_8
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    goto :goto_1

    .line 441
    :cond_9
    const-string v3, "SPen_Library"

    const-string v4, "[SMART SCALE] ON HOVER ENTER. SMART REGION NOT CONTAINS!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 444
    :cond_a
    const/16 v3, 0xa

    if-eq v0, v3, :cond_1f

    .line 445
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->SCROLL_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_b

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_b

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v3, v4, :cond_1f

    .line 446
    :cond_b
    const/4 v2, 0x0

    .line 447
    .local v2, "isScroll":Z
    const/4 v1, 0x0

    .line 448
    .local v1, "isEdgeEffect":Z
    const/16 v3, 0x9

    if-eq v0, v3, :cond_0

    .line 450
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    .line 451
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    .line 452
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    .line 453
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    .line 452
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    .line 453
    if-nez v3, :cond_e

    :cond_c
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    .line 454
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    .line 453
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    .line 454
    if-nez v3, :cond_e

    .line 455
    :cond_d
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalEnterTime:J

    .line 457
    :cond_e
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v3, :cond_f

    .line 458
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-nez v3, :cond_11

    :cond_f
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    .line 459
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-nez v3, :cond_11

    .line 460
    :cond_10
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalEnterTime:J

    .line 462
    :cond_11
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_12

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_12

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->SCROLL_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v3, v4, :cond_0

    .line 463
    :cond_12
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v3, :cond_17

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalEnterTime:J

    sub-long/2addr v4, v6

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalResponseTime:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-lez v3, :cond_17

    .line 464
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_17

    .line 465
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 466
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpl-float v3, v3, v12

    if-lez v3, :cond_16

    .line 467
    const/4 v2, 0x1

    .line 481
    :cond_13
    :goto_2
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v3, :cond_1a

    .line 482
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalEnterTime:J

    sub-long/2addr v4, v6

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalResponseTime:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-lez v3, :cond_1a

    .line 483
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_1a

    .line 484
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 485
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    cmpl-float v3, v3, v12

    if-lez v3, :cond_19

    .line 486
    const/4 v2, 0x1

    .line 503
    :cond_14
    :goto_3
    if-nez v2, :cond_15

    .line 504
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 506
    :cond_15
    if-eqz v2, :cond_1c

    .line 507
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setHoverIcon(Landroid/view/MotionEvent;)V

    .line 508
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->SCROLL_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 509
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->Scroll()V

    .line 510
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    .line 511
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    .line 512
    iput-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    .line 513
    iput-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    goto/16 :goto_0

    .line 469
    :cond_16
    const/4 v1, 0x1

    .line 471
    goto :goto_2

    :cond_17
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v3, :cond_13

    .line 472
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalEnterTime:J

    sub-long/2addr v4, v6

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalResponseTime:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-lez v3, :cond_13

    .line 473
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_13

    .line 474
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 475
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    sub-float/2addr v4, v12

    cmpg-float v3, v3, v4

    if-gez v3, :cond_18

    .line 476
    const/4 v2, 0x1

    .line 477
    goto :goto_2

    .line 478
    :cond_18
    const/4 v1, 0x1

    goto :goto_2

    .line 488
    :cond_19
    const/4 v1, 0x1

    .line 490
    goto :goto_3

    :cond_1a
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v3, :cond_14

    .line 491
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalEnterTime:J

    sub-long/2addr v4, v6

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalResponseTime:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-lez v3, :cond_14

    .line 492
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v3, :cond_14

    .line 493
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 494
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    sub-float/2addr v4, v12

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1b

    .line 495
    const/4 v2, 0x1

    .line 496
    goto :goto_3

    .line 497
    :cond_1b
    const/4 v1, 0x1

    goto :goto_3

    .line 514
    :cond_1c
    if-eqz v1, :cond_1d

    .line 515
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v3, :cond_0

    .line 516
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->calculateEdgeEffect(Landroid/view/MotionEvent;)V

    .line 517
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdate(Z)V

    goto/16 :goto_0

    .line 521
    :cond_1d
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectLR:Z

    .line 522
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseHoverEdgeEffectTB:Z

    .line 523
    iput-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeLR:J

    .line 524
    iput-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldHoverStartTimeTB:J

    .line 525
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 526
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 527
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 528
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 529
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v3, :cond_1e

    .line 530
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdate(Z)V

    .line 534
    :cond_1e
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoverIconType:I

    if-eq v3, v9, :cond_0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoverIconType:I

    if-eqz v3, :cond_0

    .line 535
    const/16 v3, 0xa

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->setAction(I)V

    .line 536
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setHoverIcon(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 541
    .end local v1    # "isEdgeEffect":Z
    .end local v2    # "isScroll":Z
    :cond_1f
    const/16 v3, 0xa

    if-ne v0, v3, :cond_0

    .line 542
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoverIconType:I

    if-eq v3, v9, :cond_20

    .line 543
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setHoverIcon(Landroid/view/MotionEvent;)V

    .line 545
    :cond_20
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v3

    if-eq v3, v6, :cond_0

    .line 548
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    if-eqz v3, :cond_22

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v4, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v3, v4, :cond_22

    .line 549
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->READY_FOR_ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 550
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomOutResponseTime:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomOutResponseTime:I

    int-to-long v4, v4

    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    .line 568
    :cond_21
    :goto_4
    const-string v3, "SPen_Library"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[SMART SCALE] ON HOVER EXIT. STATE : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 551
    :cond_22
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-nez v3, :cond_23

    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v3, :cond_21

    .line 552
    :cond_23
    sget-object v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 554
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v3

    if-eqz v3, :cond_24

    .line 555
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 556
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 557
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 558
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v3, :cond_21

    .line 559
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v3, v8}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdate(Z)V

    goto :goto_4

    .line 562
    :cond_24
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v3, :cond_21

    .line 563
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    .line 564
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v3, v9}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdate(Z)V

    goto :goto_4
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 22
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 574
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v18

    move/from16 v0, v18

    and-int/lit16 v4, v0, 0xff

    .line 575
    .local v4, "action":I
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v15

    .line 577
    .local v15, "toolType":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v11

    .line 578
    .local v11, "eventX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v12

    .line 580
    .local v12, "eventY":F
    if-nez v4, :cond_0

    .line 581
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsMultiTouch:Z

    .line 582
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateFloating:Z

    .line 583
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectLR:Z

    .line 584
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectTB:Z

    .line 585
    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldStartTime:J

    .line 587
    :cond_0
    const/16 v18, 0x5

    move/from16 v0, v18

    if-ne v4, v0, :cond_2

    .line 588
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsMultiTouch:Z

    .line 589
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-nez v18, :cond_1

    .line 590
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 591
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 594
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-nez v18, :cond_2

    .line 595
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 596
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 600
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsMultiTouch:Z

    move/from16 v18, v0

    if-eqz v18, :cond_4

    .line 801
    :cond_3
    :goto_0
    return-void

    .line 605
    :cond_4
    if-nez v4, :cond_7

    .line 606
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->READY_FOR_ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_6

    .line 607
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v19, v0

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onChangePan(FF)V

    .line 608
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    .line 611
    :cond_6
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v15, v0, :cond_b

    .line 612
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    .line 619
    :cond_7
    :goto_1
    const/16 v18, 0x6

    move/from16 v0, v18

    if-ne v4, v0, :cond_c

    .line 620
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownTime:J

    .line 654
    :cond_8
    :goto_2
    const/4 v5, 0x0

    .local v5, "diffX":F
    const/4 v8, 0x0

    .line 655
    .local v8, "diffY":F
    const/16 v16, 0x0

    .line 656
    .local v16, "velocityX":F
    const/16 v17, 0x0

    .line 657
    .local v17, "velocityY":F
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v4, v0, :cond_a

    .line 658
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventX:F

    move/from16 v18, v0

    sub-float v5, v11, v18

    .line 659
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventY:F

    move/from16 v18, v0

    sub-float v8, v12, v18

    .line 660
    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventX:F

    .line 661
    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventY:F

    .line 662
    const/high16 v18, 0x43960000    # 300.0f

    mul-float v18, v18, v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    move/from16 v19, v0

    div-float v16, v18, v19

    .line 663
    const/high16 v18, 0x43960000    # 300.0f

    mul-float v18, v18, v8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    move/from16 v19, v0

    div-float v17, v18, v19

    .line 664
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffX:F

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->m1CMPixel:F

    move/from16 v19, v0

    const/high16 v20, 0x43480000    # 200.0f

    div-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gez v18, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffY:F

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->m1CMPixel:F

    move/from16 v19, v0

    const/high16 v20, 0x43480000    # 200.0f

    div-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gez v18, :cond_9

    .line 665
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_3

    .line 667
    :cond_9
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gez v18, :cond_a

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gez v18, :cond_a

    .line 668
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_3

    .line 673
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v18

    if-lez v18, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->height()I

    move-result v18

    if-lez v18, :cond_3

    .line 676
    const/4 v13, 0x0

    .line 677
    .local v13, "pullLR":F
    const/4 v14, 0x0

    .line 678
    .local v14, "pullTB":F
    const/high16 v9, 0x3f000000    # 0.5f

    .line 679
    .local v9, "displacementLR":F
    const/high16 v10, 0x3f000000    # 0.5f

    .line 681
    .local v10, "displacementTB":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Landroid/util/SparseIntArray;->get(I)I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    .line 682
    if-nez v4, :cond_13

    .line 683
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    .line 684
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    goto/16 :goto_0

    .line 614
    .end local v5    # "diffX":F
    .end local v8    # "diffY":F
    .end local v9    # "displacementLR":F
    .end local v10    # "displacementTB":F
    .end local v13    # "pullLR":F
    .end local v14    # "pullTB":F
    .end local v16    # "velocityX":F
    .end local v17    # "velocityY":F
    :cond_b
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    goto/16 :goto_1

    .line 621
    :cond_c
    if-nez v4, :cond_d

    .line 622
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownTime:J

    .line 623
    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownX:F

    .line 624
    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownY:F

    .line 625
    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventX:F

    .line 626
    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mPreEventY:F

    .line 627
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_2

    .line 628
    :cond_d
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v4, v0, :cond_8

    .line 629
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownTime:J

    move-wide/from16 v20, v0

    sub-long v6, v18, v20

    .line 630
    .local v6, "diffTime":J
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownX:F

    move/from16 v18, v0

    sub-float v18, v11, v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffX:F

    .line 631
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownY:F

    move/from16 v18, v0

    sub-float v18, v12, v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffY:F

    .line 632
    const/high16 v18, 0x43480000    # 200.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffX:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    long-to-float v0, v6

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    div-float v16, v18, v19

    .line 633
    .restart local v16    # "velocityX":F
    const/high16 v18, 0x43480000    # 200.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffY:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    long-to-float v0, v6

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    div-float v17, v18, v19

    .line 635
    .restart local v17    # "velocityY":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffX:F

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->m1CMPixel:F

    move/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gez v18, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffY:F

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->m1CMPixel:F

    move/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gez v18, :cond_e

    .line 636
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_2

    .line 637
    :cond_e
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gez v18, :cond_f

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gez v18, :cond_f

    .line 638
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_2

    .line 639
    :cond_f
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v19

    cmpl-float v18, v18, v19

    if-lez v18, :cond_11

    .line 640
    const/16 v18, 0x0

    cmpl-float v18, v16, v18

    if-lez v18, :cond_10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmpg-double v18, v18, v20

    if-gtz v18, :cond_10

    .line 641
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_2

    .line 642
    :cond_10
    const/16 v18, 0x0

    cmpg-float v18, v16, v18

    if-gez v18, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    move/from16 v19, v0

    cmpl-float v18, v18, v19

    if-ltz v18, :cond_8

    .line 643
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_2

    .line 646
    :cond_11
    const/16 v18, 0x0

    cmpl-float v18, v17, v18

    if-lez v18, :cond_12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_12

    .line 647
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_2

    .line 648
    :cond_12
    const/16 v18, 0x0

    cmpg-float v18, v17, v18

    if-gez v18, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    move/from16 v19, v0

    cmpl-float v18, v18, v19

    if-ltz v18, :cond_8

    .line 649
    sget-object v18, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_2

    .line 685
    .end local v6    # "diffTime":J
    .restart local v5    # "diffX":F
    .restart local v8    # "diffY":F
    .restart local v9    # "displacementLR":F
    .restart local v10    # "displacementTB":F
    .restart local v13    # "pullLR":F
    .restart local v14    # "pullTB":F
    :cond_13
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v4, v0, :cond_2c

    .line 686
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->enable:Z

    move/from16 v18, v0

    if-eqz v18, :cond_1f

    .line 687
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_1b

    .line 688
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_1a

    const/16 v18, 0x0

    cmpl-float v18, v16, v18

    if-lez v18, :cond_1a

    .line 689
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    .line 690
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    .line 716
    :cond_14
    :goto_3
    const/16 v18, 0x0

    cmpg-float v18, v13, v18

    if-gez v18, :cond_15

    .line 717
    const/4 v13, 0x0

    .line 723
    :cond_15
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->enable:Z

    move/from16 v18, v0

    if-eqz v18, :cond_25

    .line 724
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_21

    .line 725
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_20

    const/16 v18, 0x0

    cmpl-float v18, v17, v18

    if-lez v18, :cond_20

    .line 726
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    .line 727
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    .line 753
    :cond_16
    :goto_5
    const/16 v18, 0x0

    cmpg-float v18, v14, v18

    if-gez v18, :cond_17

    .line 754
    const/4 v14, 0x0

    .line 760
    :cond_17
    :goto_6
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x3f000000    # 0.5f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDensity:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_29

    .line 761
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x3f000000    # 0.5f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDensity:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_29

    .line 762
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldStartTime:J

    move-wide/from16 v18, v0

    const-wide/16 v20, -0x1

    cmp-long v18, v18, v20

    if-nez v18, :cond_26

    .line 763
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldStartTime:J

    .line 776
    :cond_18
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectLR:Z

    move/from16 v18, v0

    if-nez v18, :cond_19

    const/16 v18, 0x0

    cmpl-float v18, v13, v18

    if-lez v18, :cond_19

    .line 777
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isLollipopOS()Z

    move-result v18

    if-eqz v18, :cond_2a

    .line 778
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v13, v9}, Landroid/widget/EdgeEffect;->onPull(FF)V

    .line 783
    :cond_19
    :goto_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectTB:Z

    move/from16 v18, v0

    if-nez v18, :cond_3

    const/16 v18, 0x0

    cmpl-float v18, v14, v18

    if-lez v18, :cond_3

    .line 784
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isLollipopOS()Z

    move-result v18

    if-eqz v18, :cond_2b

    .line 785
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v10}, Landroid/widget/EdgeEffect;->onPull(FF)V

    goto/16 :goto_0

    .line 691
    :cond_1a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v19, v19, v20

    cmpl-float v18, v18, v19

    if-ltz v18, :cond_14

    const/16 v18, 0x0

    cmpg-float v18, v16, v18

    if-gez v18, :cond_14

    .line 692
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    .line 693
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput v11, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    goto/16 :goto_3

    .line 695
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_1d

    .line 696
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    cmpl-float v18, v18, v19

    if-lez v18, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-nez v18, :cond_1c

    .line 698
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 699
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 700
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectLR:Z

    goto/16 :goto_3

    .line 701
    :cond_1c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_14

    .line 702
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    move/from16 v18, v0

    sub-float v18, v11, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->width()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v13, v18, v19

    .line 703
    const/high16 v18, 0x3f800000    # 1.0f

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->height()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v19, v19, v20

    sub-float v9, v18, v19

    .line 705
    goto/16 :goto_3

    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_14

    .line 706
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gez v18, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-nez v18, :cond_1e

    .line 708
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 709
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 710
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectLR:Z

    goto/16 :goto_3

    .line 711
    :cond_1e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v19, v19, v20

    cmpl-float v18, v18, v19

    if-ltz v18, :cond_14

    .line 712
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    move/from16 v18, v0

    sub-float v18, v18, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->width()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v13, v18, v19

    .line 713
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v9, v18, v19

    goto/16 :goto_3

    .line 720
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    goto/16 :goto_4

    .line 728
    :cond_20
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v19, v19, v20

    cmpl-float v18, v18, v19

    if-ltz v18, :cond_16

    const/16 v18, 0x0

    cmpg-float v18, v17, v18

    if-gez v18, :cond_16

    .line 729
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    .line 730
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    goto/16 :goto_5

    .line 732
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_23

    .line 733
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    cmpl-float v18, v18, v19

    if-lez v18, :cond_22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-nez v18, :cond_22

    .line 735
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 736
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 737
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectTB:Z

    goto/16 :goto_5

    .line 738
    :cond_22
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gtz v18, :cond_16

    .line 739
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    move/from16 v18, v0

    sub-float v18, v12, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v14, v18, v19

    .line 740
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->width()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v10, v18, v19

    .line 742
    goto/16 :goto_5

    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_16

    .line 743
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v19, v19, v20

    cmpg-float v18, v18, v19

    if-gez v18, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v18

    if-nez v18, :cond_24

    .line 745
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 746
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 747
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectTB:Z

    goto/16 :goto_5

    .line 748
    :cond_24
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    move/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v19, v19, v20

    cmpl-float v18, v18, v19

    if-ltz v18, :cond_16

    .line 749
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    move/from16 v18, v0

    sub-float v18, v18, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v14, v18, v19

    .line 750
    const/high16 v18, 0x3f800000    # 1.0f

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->width()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v19, v19, v20

    sub-float v10, v18, v19

    goto/16 :goto_5

    .line 757
    :cond_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    goto/16 :goto_6

    .line 765
    :cond_26
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldStartTime:J

    move-wide/from16 v20, v0

    sub-long v18, v18, v20

    const-wide/16 v20, 0x5dc

    cmp-long v18, v18, v20

    if-ltz v18, :cond_18

    .line 766
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectLR:Z

    move/from16 v18, v0

    if-eqz v18, :cond_27

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectTB:Z

    move/from16 v18, v0

    if-nez v18, :cond_28

    .line 767
    :cond_27
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->releaseEdgeEffects()V

    .line 769
    :cond_28
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectLR:Z

    .line 770
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsReleaseEdgeEffectTB:Z

    goto/16 :goto_7

    .line 774
    :cond_29
    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHoldStartTime:J

    goto/16 :goto_7

    .line 780
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Landroid/widget/EdgeEffect;->onPull(F)V

    goto/16 :goto_8

    .line 787
    :cond_2b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Landroid/widget/EdgeEffect;->onPull(F)V

    goto/16 :goto_0

    .line 791
    :cond_2c
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->isEdgeEffectWorking()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 792
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 793
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 794
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mIsUpdateEEAnimation:Z

    .line 795
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 796
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-interface/range {v18 .. v19}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdate(Z)V

    goto/16 :goto_0
.end method

.method public onZoom(FFF)V
    .locals 0
    .param p1, "deltaX"    # F
    .param p2, "deltaY"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 1108
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    .line 1109
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    .line 1110
    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    .line 1111
    return-void
.end method

.method public setDrawInformation(IIII)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "startX"    # I
    .param p4, "startY"    # I

    .prologue
    .line 1142
    return-void
.end method

.method public setLimitHeight(FF)V
    .locals 4
    .param p1, "maxDeltaX"    # F
    .param p2, "maxDeltaY"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1114
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    .line 1115
    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    .line 1117
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1118
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->enable:Z

    .line 1123
    :goto_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1124
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->enable:Z

    .line 1128
    :goto_1
    return-void

    .line 1120
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->enable:Z

    goto :goto_0

    .line 1126
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->enable:Z

    goto :goto_1
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    .prologue
    .line 253
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    .line 254
    return-void
.end method

.method protected setParentRect(Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 1145
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1146
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    .line 1155
    :goto_0
    return-void

    .line 1149
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    if-nez v0, :cond_2

    .line 1150
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    .line 1152
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1153
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    .line 1154
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0
.end method

.method public setToolTypeAction(II)V
    .locals 1
    .param p1, "toolType"    # I
    .param p2, "action"    # I

    .prologue
    .line 257
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 258
    return-void
.end method
