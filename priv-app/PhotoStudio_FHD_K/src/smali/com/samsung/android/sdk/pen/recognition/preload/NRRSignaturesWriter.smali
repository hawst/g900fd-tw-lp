.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;
.super Ljava/lang/Object;
.source "NRRSignaturesWriter.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mWriter:Ljava/io/BufferedWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljavax/crypto/Cipher;Ljavax/crypto/Cipher;)V
    .locals 15
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "encryptCipher"    # Ljavax/crypto/Cipher;
    .param p3, "decryptCypher"    # Ljavax/crypto/Cipher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v2, 0x0

    .line 29
    .local v2, "cis":Ljavax/crypto/CipherInputStream;
    const/4 v10, 0x0

    .line 32
    .local v10, "previosData":[B
    :try_start_0
    new-instance v3, Ljavax/crypto/CipherInputStream;

    new-instance v11, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-direct {v3, v11, v0}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    .end local v2    # "cis":Ljavax/crypto/CipherInputStream;
    .local v3, "cis":Ljavax/crypto/CipherInputStream;
    :try_start_1
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 34
    .local v1, "buffer":Ljava/io/ByteArrayOutputStream;
    const/16 v11, 0x4000

    new-array v6, v11, [B

    .line 36
    .local v6, "data":[B
    :goto_0
    const/4 v11, 0x0

    array-length v12, v6

    invoke-virtual {v3, v6, v11, v12}, Ljavax/crypto/CipherInputStream;->read([BII)I

    move-result v9

    .local v9, "nRead":I
    const/4 v11, -0x1

    if-ne v9, v11, :cond_2

    .line 39
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 40
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_b
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v10

    .line 46
    if-eqz v3, :cond_4

    .line 48
    :try_start_2
    invoke-virtual {v3}, Ljavax/crypto/CipherInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    move-object v2, v3

    .line 55
    .end local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v3    # "cis":Ljavax/crypto/CipherInputStream;
    .end local v6    # "data":[B
    .end local v9    # "nRead":I
    .restart local v2    # "cis":Ljavax/crypto/CipherInputStream;
    :cond_0
    :goto_1
    const/4 v4, 0x0

    .line 57
    .local v4, "cos":Ljavax/crypto/CipherOutputStream;
    if-eqz v10, :cond_1

    .line 59
    :try_start_3
    new-instance v5, Ljavax/crypto/CipherOutputStream;

    new-instance v11, Ljava/io/FileOutputStream;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-direct {v5, v11, v0}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7

    .line 60
    .end local v4    # "cos":Ljavax/crypto/CipherOutputStream;
    .local v5, "cos":Ljavax/crypto/CipherOutputStream;
    :try_start_4
    invoke-virtual {v5, v10}, Ljavax/crypto/CipherOutputStream;->write([B)V

    .line 61
    invoke-virtual {v5}, Ljavax/crypto/CipherOutputStream;->flush()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9

    move-object v4, v5

    .line 75
    .end local v5    # "cos":Ljavax/crypto/CipherOutputStream;
    .restart local v4    # "cos":Ljavax/crypto/CipherOutputStream;
    :cond_1
    :goto_2
    if-eqz v4, :cond_5

    .line 76
    new-instance v11, Ljava/io/BufferedWriter;

    new-instance v12, Ljava/io/OutputStreamWriter;

    invoke-direct {v12, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v11, v12}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    iput-object v11, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->mWriter:Ljava/io/BufferedWriter;

    .line 80
    :goto_3
    return-void

    .line 37
    .end local v2    # "cis":Ljavax/crypto/CipherInputStream;
    .end local v4    # "cos":Ljavax/crypto/CipherOutputStream;
    .restart local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "cis":Ljavax/crypto/CipherInputStream;
    .restart local v6    # "data":[B
    .restart local v9    # "nRead":I
    :cond_2
    const/4 v11, 0x0

    :try_start_5
    invoke-virtual {v1, v6, v11, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_b
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    .line 41
    .end local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v6    # "data":[B
    .end local v9    # "nRead":I
    :catch_0
    move-exception v7

    move-object v2, v3

    .line 42
    .end local v3    # "cis":Ljavax/crypto/CipherInputStream;
    .restart local v2    # "cis":Ljavax/crypto/CipherInputStream;
    .local v7, "ex":Ljava/io/FileNotFoundException;
    :goto_4
    :try_start_6
    sget-object v11, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->LOG_TAG:Ljava/lang/String;

    const-string v12, "File with original signatures does not exist, may be plugin runing first time!"

    invoke-static {v11, v12, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 46
    if-eqz v2, :cond_0

    .line 48
    :try_start_7
    invoke-virtual {v2}, Ljavax/crypto/CipherInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_1

    .line 49
    :catch_1
    move-exception v7

    .line 50
    .local v7, "ex":Ljava/io/IOException;
    sget-object v11, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->LOG_TAG:Ljava/lang/String;

    const-string v12, "IO error ocured while closing file with original signatures!"

    invoke-static {v11, v12, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 43
    .end local v7    # "ex":Ljava/io/IOException;
    :catch_2
    move-exception v7

    .line 44
    .restart local v7    # "ex":Ljava/io/IOException;
    :goto_5
    :try_start_8
    sget-object v11, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->LOG_TAG:Ljava/lang/String;

    const-string v12, "IO error ocured while reading original signatures from file!"

    invoke-static {v11, v12, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 46
    if-eqz v2, :cond_0

    .line 48
    :try_start_9
    invoke-virtual {v2}, Ljavax/crypto/CipherInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_1

    .line 49
    :catch_3
    move-exception v7

    .line 50
    sget-object v11, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->LOG_TAG:Ljava/lang/String;

    const-string v12, "IO error ocured while closing file with original signatures!"

    invoke-static {v11, v12, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 45
    .end local v7    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    .line 46
    :goto_6
    if-eqz v2, :cond_3

    .line 48
    :try_start_a
    invoke-virtual {v2}, Ljavax/crypto/CipherInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 53
    :cond_3
    :goto_7
    throw v11

    .line 49
    :catch_4
    move-exception v7

    .line 50
    .restart local v7    # "ex":Ljava/io/IOException;
    sget-object v12, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->LOG_TAG:Ljava/lang/String;

    const-string v13, "IO error ocured while closing file with original signatures!"

    invoke-static {v12, v13, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_7

    .line 49
    .end local v2    # "cis":Ljavax/crypto/CipherInputStream;
    .end local v7    # "ex":Ljava/io/IOException;
    .restart local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "cis":Ljavax/crypto/CipherInputStream;
    .restart local v6    # "data":[B
    .restart local v9    # "nRead":I
    :catch_5
    move-exception v7

    .line 50
    .restart local v7    # "ex":Ljava/io/IOException;
    sget-object v11, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->LOG_TAG:Ljava/lang/String;

    const-string v12, "IO error ocured while closing file with original signatures!"

    invoke-static {v11, v12, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v7    # "ex":Ljava/io/IOException;
    :cond_4
    move-object v2, v3

    .end local v3    # "cis":Ljavax/crypto/CipherInputStream;
    .restart local v2    # "cis":Ljavax/crypto/CipherInputStream;
    goto :goto_1

    .line 62
    .end local v1    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v6    # "data":[B
    .end local v9    # "nRead":I
    .restart local v4    # "cos":Ljavax/crypto/CipherOutputStream;
    :catch_6
    move-exception v7

    .line 63
    .local v7, "ex":Ljava/io/FileNotFoundException;
    :goto_8
    sget-object v11, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->LOG_TAG:Ljava/lang/String;

    const-string v12, "File with original signatures ca not be opend for writing!"

    invoke-static {v11, v12, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 64
    .end local v7    # "ex":Ljava/io/FileNotFoundException;
    :catch_7
    move-exception v7

    .line 65
    .local v7, "ex":Ljava/io/IOException;
    :goto_9
    sget-object v11, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->LOG_TAG:Ljava/lang/String;

    const-string v12, "IO exeption ocured while rewriting original signatures to file, prewious data will be destroyed!"

    invoke-static {v11, v12, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 67
    :try_start_b
    invoke-virtual {v4}, Ljavax/crypto/CipherOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    .line 71
    :goto_a
    const/4 v4, 0x0

    goto :goto_2

    .line 68
    :catch_8
    move-exception v8

    .line 69
    .local v8, "exi":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 78
    .end local v7    # "ex":Ljava/io/IOException;
    .end local v8    # "exi":Ljava/io/IOException;
    :cond_5
    new-instance v11, Ljava/io/BufferedWriter;

    new-instance v12, Ljava/io/OutputStreamWriter;

    new-instance v13, Ljavax/crypto/CipherOutputStream;

    new-instance v14, Ljava/io/FileOutputStream;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-direct {v13, v14, v0}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V

    invoke-direct {v12, v13}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v11, v12}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    iput-object v11, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->mWriter:Ljava/io/BufferedWriter;

    goto/16 :goto_3

    .line 64
    .end local v4    # "cos":Ljavax/crypto/CipherOutputStream;
    .restart local v5    # "cos":Ljavax/crypto/CipherOutputStream;
    :catch_9
    move-exception v7

    move-object v4, v5

    .end local v5    # "cos":Ljavax/crypto/CipherOutputStream;
    .restart local v4    # "cos":Ljavax/crypto/CipherOutputStream;
    goto :goto_9

    .line 62
    .end local v4    # "cos":Ljavax/crypto/CipherOutputStream;
    .restart local v5    # "cos":Ljavax/crypto/CipherOutputStream;
    :catch_a
    move-exception v7

    move-object v4, v5

    .end local v5    # "cos":Ljavax/crypto/CipherOutputStream;
    .restart local v4    # "cos":Ljavax/crypto/CipherOutputStream;
    goto :goto_8

    .line 45
    .end local v2    # "cis":Ljavax/crypto/CipherInputStream;
    .end local v4    # "cos":Ljavax/crypto/CipherOutputStream;
    .restart local v3    # "cis":Ljavax/crypto/CipherInputStream;
    :catchall_1
    move-exception v11

    move-object v2, v3

    .end local v3    # "cis":Ljavax/crypto/CipherInputStream;
    .restart local v2    # "cis":Ljavax/crypto/CipherInputStream;
    goto :goto_6

    .line 43
    .end local v2    # "cis":Ljavax/crypto/CipherInputStream;
    .restart local v3    # "cis":Ljavax/crypto/CipherInputStream;
    :catch_b
    move-exception v7

    move-object v2, v3

    .end local v3    # "cis":Ljavax/crypto/CipherInputStream;
    .restart local v2    # "cis":Ljavax/crypto/CipherInputStream;
    goto :goto_5

    .line 41
    :catch_c
    move-exception v7

    goto/16 :goto_4
.end method


# virtual methods
.method public append(Lcom/samsung/recognitionengine/Signature;)Z
    .locals 14
    .param p1, "sig"    # Lcom/samsung/recognitionengine/Signature;

    .prologue
    const/16 v13, 0xa

    const/4 v6, 0x0

    const/16 v12, 0x20

    .line 92
    if-nez p1, :cond_0

    .line 128
    :goto_0
    return v6

    .line 96
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    .local v4, "strBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/recognitionengine/Signature;->size()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 99
    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 101
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    int-to-long v8, v1

    invoke-virtual {p1}, Lcom/samsung/recognitionengine/Signature;->size()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-ltz v7, :cond_1

    .line 122
    :try_start_0
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->mWriter:Ljava/io/BufferedWriter;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    const/4 v6, 0x1

    goto :goto_0

    .line 102
    :cond_1
    invoke-virtual {p1, v1}, Lcom/samsung/recognitionengine/Signature;->get(I)Lcom/samsung/recognitionengine/Stroke;

    move-result-object v5

    .line 103
    .local v5, "stroke":Lcom/samsung/recognitionengine/Stroke;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    int-to-long v8, v2

    invoke-virtual {v5}, Lcom/samsung/recognitionengine/Stroke;->size()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-ltz v7, :cond_2

    .line 118
    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 101
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 104
    :cond_2
    invoke-virtual {v5, v2}, Lcom/samsung/recognitionengine/Stroke;->get(I)Lcom/samsung/recognitionengine/TouchPoint;

    move-result-object v3

    .line 105
    .local v3, "point":Lcom/samsung/recognitionengine/TouchPoint;
    invoke-virtual {v3}, Lcom/samsung/recognitionengine/TouchPoint;->getPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/recognitionengine/PointF;->getX()F

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 106
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 107
    invoke-virtual {v3}, Lcom/samsung/recognitionengine/TouchPoint;->getPoint()Lcom/samsung/recognitionengine/PointF;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/recognitionengine/PointF;->getY()F

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 108
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 109
    invoke-virtual {v3}, Lcom/samsung/recognitionengine/TouchPoint;->getPressure()F

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 110
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 111
    invoke-virtual {v3}, Lcom/samsung/recognitionengine/TouchPoint;->getTimestamp()D

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 112
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {v3}, Lcom/samsung/recognitionengine/TouchPoint;->getOrientation()F

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 114
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {v3}, Lcom/samsung/recognitionengine/TouchPoint;->getTilt()F

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 116
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 103
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 123
    .end local v2    # "j":I
    .end local v3    # "point":Lcom/samsung/recognitionengine/TouchPoint;
    .end local v5    # "stroke":Lcom/samsung/recognitionengine/Stroke;
    :catch_0
    move-exception v0

    .line 124
    .local v0, "ex":Ljava/io/IOException;
    sget-object v7, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->LOG_TAG:Ljava/lang/String;

    const-string v8, "Can not write to file!"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method public close()V
    .locals 3

    .prologue
    .line 85
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->mWriter:Ljava/io/BufferedWriter;

    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :goto_0
    return-void

    .line 86
    :catch_0
    move-exception v0

    .line 87
    .local v0, "ex":Ljava/io/IOException;
    sget-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignaturesWriter;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Can not close writer!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
