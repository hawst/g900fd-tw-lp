.class Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;
.super Ljava/lang/Object;
.source "NRRSignatureRecognitionPlugin.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResultNotifier"
.end annotation


# instance fields
.field private final mResult:Z

.field private final mSpenObjectStrokes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;Ljava/util/List;Z)V
    .locals 0
    .param p3, "result"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 243
    .local p2, "spenObjectStrokes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;->mSpenObjectStrokes:Ljava/util/List;

    .line 245
    iput-boolean p3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;->mResult:Z

    .line 246
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;Ljava/util/List;ZLcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;)V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;Ljava/util/List;Z)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 250
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;->access$3(Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    move-result-object v0

    .line 251
    .local v0, "listener":Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;
    if-nez v0, :cond_0

    .line 253
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;->mSpenObjectStrokes:Ljava/util/List;

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/NRRSignatureRecognitionPlugin$ResultNotifier;->mResult:Z

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;->onResult(Ljava/util/List;Z)V

    goto :goto_0
.end method
