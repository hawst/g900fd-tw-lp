.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$4;
.super Ljava/lang/Object;
.source "SpenSettingRemoverLayout2.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    .line 396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 399
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMaxButton:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 400
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;->onPopup(Z)V

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 403
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupMinButton:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;->onPopup(Z)V

    goto :goto_0
.end method
