.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;
.super Ljava/lang/Object;
.source "SpenSettingTextLayout2.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$NameDropdownSelectListner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    .line 375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSelectItem(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 379
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;Ljava/lang/String;)V

    .line 381
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewTypeface(Landroid/graphics/Typeface;)V

    .line 383
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->invalidate()V

    .line 384
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v1, :cond_0

    .line 385
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    .line 386
    .local v0, "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontList()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontName(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    .line 387
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 390
    .end local v0    # "info":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 393
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Landroid/widget/Button;

    move-result-object v1

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 394
    return-void
.end method
