.class public Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;
.super Ljava/lang/Object;
.source "SpenTextMeasure.java"


# instance fields
.field mDiffY:F

.field mHeight:F

.field private mIs64:Z

.field mMinimumHeight:F

.field private final mNativeTextView:J

.field private mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 22
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mIs64:Z

    .line 35
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->osType()I

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mIs64:Z

    .line 36
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->Native_init()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:J

    .line 37
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:J

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->Native_construct(JLandroid/content/Context;)Z

    .line 38
    return-void

    .line 35
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private Native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "nativeCanvas"    # J
    .param p3, "command"    # I
    .param p5, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 340
    .local p4, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mIs64:Z

    if-eqz v0, :cond_0

    .line 341
    invoke-static {p1, p2, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 344
    :goto_0
    return-object v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method private Native_construct(JLandroid/content/Context;)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mIs64:Z

    if-eqz v0, :cond_0

    .line 259
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_construct(JLandroid/content/Context;)Z

    move-result v0

    .line 262
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_construct(ILandroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_finalize(J)V
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 249
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mIs64:Z

    if-eqz v0, :cond_0

    .line 250
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_finalize(J)V

    .line 255
    :goto_0
    return-void

    .line 253
    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_finalize(I)V

    goto :goto_0
.end method

.method private Native_getHeight(J)I
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 285
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mIs64:Z

    if-eqz v0, :cond_0

    .line 286
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getHeight(J)I

    move-result v0

    .line 289
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getHeight(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getLineCount(J)I
    .locals 1
    .param p1, "nativeTextView"    # J

    .prologue
    .line 294
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mIs64:Z

    if-eqz v0, :cond_0

    .line 295
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getLineCount(J)I

    move-result v0

    .line 298
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getLineCount(I)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getLineEndIndex(JI)I
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "lineNumber"    # I

    .prologue
    .line 330
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mIs64:Z

    if-eqz v0, :cond_0

    .line 331
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getLineEndIndex(JI)I

    move-result v0

    .line 334
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getLineEndIndex(II)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getLinePosition(JILandroid/graphics/PointF;)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "lineNumber"    # I
    .param p4, "position"    # Landroid/graphics/PointF;

    .prologue
    .line 303
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mIs64:Z

    if-eqz v0, :cond_0

    .line 304
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getLinePosition(JILandroid/graphics/PointF;)Z

    move-result v0

    .line 307
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getLinePosition(IILandroid/graphics/PointF;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_getLineStartIndex(JI)I
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "lineNumber"    # I

    .prologue
    .line 321
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mIs64:Z

    if-eqz v0, :cond_0

    .line 322
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getLineStartIndex(JI)I

    move-result v0

    .line 325
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getLineStartIndex(II)I

    move-result v0

    goto :goto_0
.end method

.method private Native_getTextRect(JILandroid/graphics/RectF;)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "textIndex"    # I
    .param p4, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 312
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mIs64:Z

    if-eqz v0, :cond_0

    .line 313
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getTextRect(JILandroid/graphics/RectF;)Z

    move-result v0

    .line 316
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getTextRect(IILandroid/graphics/RectF;)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_init()J
    .locals 2

    .prologue
    .line 240
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mIs64:Z

    if-eqz v0, :cond_0

    .line 241
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_init_64()J

    move-result-wide v0

    .line 244
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_init()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method private Native_measure(JI)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "width"    # I

    .prologue
    .line 276
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mIs64:Z

    if-eqz v0, :cond_0

    .line 277
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_measure(JI)Z

    move-result v0

    .line 280
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_measure(II)Z

    move-result v0

    goto :goto_0
.end method

.method private Native_setObjectText(JLcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z
    .locals 1
    .param p1, "nativeTextView"    # J
    .param p3, "objectText"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mIs64:Z

    if-eqz v0, :cond_0

    .line 268
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_setObjectText(JLcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z

    move-result v0

    .line 271
    :goto_0
    return v0

    :cond_0
    long-to-int v0, p1

    invoke-static {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_setObjectText(ILcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z

    move-result v0

    goto :goto_0
.end method

.method private static native native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_command(JILjava/util/ArrayList;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private static native native_construct(ILandroid/content/Context;)Z
.end method

.method private static native native_construct(JLandroid/content/Context;)Z
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_finalize(J)V
.end method

.method private static native native_getHeight(I)I
.end method

.method private static native native_getHeight(J)I
.end method

.method private static native native_getLineCount(I)I
.end method

.method private static native native_getLineCount(J)I
.end method

.method private static native native_getLineEndIndex(II)I
.end method

.method private static native native_getLineEndIndex(JI)I
.end method

.method private static native native_getLinePosition(IILandroid/graphics/PointF;)Z
.end method

.method private static native native_getLinePosition(JILandroid/graphics/PointF;)Z
.end method

.method private static native native_getLineStartIndex(II)I
.end method

.method private static native native_getLineStartIndex(JI)I
.end method

.method private static native native_getTextRect(IILandroid/graphics/RectF;)Z
.end method

.method private static native native_getTextRect(JILandroid/graphics/RectF;)Z
.end method

.method private static native native_init()I
.end method

.method private static native native_init_64()J
.end method

.method private static native native_measure(II)Z
.end method

.method private static native native_measure(JI)Z
.end method

.method private static native native_setObjectText(ILcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z
.end method

.method private static native native_setObjectText(JLcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z
.end method


# virtual methods
.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 233
    :try_start_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->Native_finalize(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 237
    return-void

    .line 234
    :catchall_0
    move-exception v0

    .line 235
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 236
    throw v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mHeight:F

    float-to-int v0, v0

    return v0
.end method

.method public getLineCount()I
    .locals 2

    .prologue
    .line 137
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->Native_getLineCount(J)I

    move-result v0

    return v0
.end method

.method public getLineEndIndex(I)I
    .locals 2
    .param p1, "lineNumber"    # I

    .prologue
    .line 227
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->Native_getLineEndIndex(JI)I

    move-result v0

    return v0
.end method

.method public getLinePosition(I)Landroid/graphics/PointF;
    .locals 4
    .param p1, "lineNumber"    # I

    .prologue
    .line 157
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 158
    .local v0, "result":Landroid/graphics/PointF;
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:J

    invoke-direct {p0, v2, v3, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->Native_getLinePosition(JILandroid/graphics/PointF;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 159
    const/4 v0, 0x0

    .line 162
    .end local v0    # "result":Landroid/graphics/PointF;
    :goto_0
    return-object v0

    .line 161
    .restart local v0    # "result":Landroid/graphics/PointF;
    :cond_0
    const/4 v1, 0x0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mDiffY:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->offset(FF)V

    goto :goto_0
.end method

.method public getLineStartIndex(I)I
    .locals 2
    .param p1, "lineNumber"    # I

    .prologue
    .line 207
    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:J

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->Native_getLineStartIndex(JI)I

    move-result v0

    return v0
.end method

.method public getMinHeight()I
    .locals 2

    .prologue
    .line 121
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mMinimumHeight:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public getTextRect(I)Landroid/graphics/RectF;
    .locals 4
    .param p1, "textIndex"    # I

    .prologue
    .line 182
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 183
    .local v0, "result":Landroid/graphics/RectF;
    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:J

    invoke-direct {p0, v2, v3, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->Native_getTextRect(JILandroid/graphics/RectF;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 184
    const/4 v0, 0x0

    .line 187
    .end local v0    # "result":Landroid/graphics/RectF;
    :goto_0
    return-object v0

    .line 186
    .restart local v0    # "result":Landroid/graphics/RectF;
    :cond_0
    const/4 v1, 0x0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mDiffY:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_0
.end method

.method public setObjectText(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z
    .locals 7
    .param p1, "objectText"    # Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    if-nez p1, :cond_0

    .line 60
    const/4 v4, 0x7

    const-string v5, " objectText instance must not be null"

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    .line 63
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    .line 65
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:J

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-direct {p0, v4, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->Native_setObjectText(JLcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 89
    :cond_1
    :goto_0
    return v2

    .line 69
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 70
    .local v1, "objectRect":Landroid/graphics/RectF;
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v4, v4

    if-lez v4, :cond_1

    .line 73
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:J

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v6

    float-to-int v6, v6

    invoke-direct {p0, v4, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->Native_measure(JI)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 77
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getGravity()I

    move-result v0

    .line 78
    .local v0, "gravity":I
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mDiffY:F

    .line 79
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mHeight:F

    .line 81
    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:J

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->Native_getHeight(J)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mMinimumHeight:F

    .line 82
    if-eq v0, v3, :cond_3

    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    .line 83
    :cond_3
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mHeight:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mMinimumHeight:F

    sub-float/2addr v2, v4

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mDiffY:F

    .line 84
    if-ne v0, v3, :cond_4

    .line 85
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mDiffY:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mDiffY:F

    :cond_4
    move v2, v3

    .line 89
    goto :goto_0
.end method
