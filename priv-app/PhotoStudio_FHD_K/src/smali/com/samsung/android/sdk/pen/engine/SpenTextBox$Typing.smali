.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;
.super Landroid/os/Handler;
.source "SpenTextBox.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Typing"
.end annotation


# static fields
.field private static final INPUT_DELAY:I = 0x64

.field public static final INPUT_END_MESSAGE:I = 0x1


# instance fields
.field private final mTextBox:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenTextBox;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 1
    .param p1, "textBox"    # Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .prologue
    .line 5771
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 5772
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->mTextBox:Ljava/lang/ref/WeakReference;

    .line 5773
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 5777
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    .line 5798
    :cond_0
    :goto_0
    return-void

    .line 5781
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    .line 5782
    .local v0, "textBox":Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    if-eqz v0, :cond_0

    .line 5786
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 5797
    :goto_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 5788
    :pswitch_0
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    .line 5789
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSettingInfo()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    .line 5790
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    goto :goto_1

    .line 5786
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public startInput()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5801
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->removeMessages(I)V

    .line 5803
    const-wide/16 v0, 0x64

    invoke-virtual {p0, v2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->sendEmptyMessageDelayed(IJ)Z

    .line 5804
    return-void
.end method

.method public stopInput()V
    .locals 1

    .prologue
    .line 5807
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->removeMessages(I)V

    .line 5808
    return-void
.end method
