.class Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
.super Ljava/lang/Object;
.source "SPenImageUtil.java"


# instance fields
.field protected mContextResources:Landroid/content/res/Resources;

.field protected mCustom_imagepath:Ljava/lang/String;

.field protected mDrawableContext:Landroid/content/Context;

.field protected mOnePT:F

.field protected mSdkResources:Landroid/content/res/Resources;

.field protected mSdkVersion:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;F)V
    .locals 5
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "customImagePath"    # Ljava/lang/String;
    .param p3, "ratio"    # F

    .prologue
    const/4 v4, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    .line 33
    const-string v3, ""

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    .line 34
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    .line 35
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    .line 36
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkVersion:I

    .line 48
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 53
    .local v1, "localDisplayMetrics":Landroid/util/DisplayMetrics;
    iget v3, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, p3

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    .line 56
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 57
    .local v2, "manager":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    .line 58
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    .end local v2    # "manager":Landroid/content/pm/PackageManager;
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected getColorStateList(I)Landroid/content/res/ColorStateList;
    .locals 8
    .param p1, "color"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 707
    new-array v0, v7, [[I

    .line 708
    .local v0, "arrayOfStates":[[I
    new-array v1, v6, [I

    .line 709
    .local v1, "arrayOfStates1":[I
    const v4, -0x10100a7

    aput v4, v1, v5

    .line 710
    aput-object v1, v0, v5

    .line 711
    new-array v2, v6, [I

    .line 712
    .local v2, "arrayOfStates2":[I
    const v4, 0x10100a7

    aput v4, v2, v5

    .line 713
    aput-object v2, v0, v6

    .line 714
    new-array v3, v7, [I

    .line 715
    .local v3, "textColor":[I
    aput p1, v3, v5

    .line 716
    aput p1, v3, v6

    .line 718
    new-instance v4, Landroid/content/res/ColorStateList;

    invoke-direct {v4, v0, v3}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v4
.end method

.method protected getIntValueAppliedDensity(F)I
    .locals 1
    .param p1, "paramFloat"    # F

    .prologue
    .line 502
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public resizeImage(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;
    .locals 20
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I
    .param p3, "iconWidth"    # I
    .param p4, "iconHeight"    # I
    .param p5, "forceResize"    # Z

    .prologue
    .line 139
    invoke-virtual/range {p1 .. p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v14

    .line 140
    .local v14, "stream":Ljava/io/InputStream;
    invoke-static {v14}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 143
    .local v2, "BitmapOrg":Landroid/graphics/Bitmap;
    if-nez v2, :cond_0

    .line 144
    const/4 v3, 0x0

    .line 175
    :goto_0
    return-object v3

    .line 146
    :cond_0
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 147
    .local v5, "width":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 148
    .local v6, "height":I
    move/from16 v0, p3

    int-to-float v3, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    .line 149
    .local v10, "newWidth":I
    move/from16 v0, p4

    int-to-float v3, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v9

    .line 152
    .local v9, "newHeight":I
    int-to-float v3, v10

    int-to-float v4, v5

    div-float v13, v3, v4

    .line 153
    .local v13, "scaleWidth":F
    int-to-float v3, v9

    int-to-float v4, v6

    div-float v12, v3, v4

    .line 155
    .local v12, "scaleHeight":F
    float-to-double v0, v13

    move-wide/from16 v16, v0

    const-wide v18, 0x3fefae147ae147aeL    # 0.99

    cmpl-double v3, v16, v18

    if-lez v3, :cond_1

    float-to-double v0, v12

    move-wide/from16 v16, v0

    const-wide v18, 0x3fefae147ae147aeL    # 0.99

    cmpl-double v3, v16, v18

    if-lez v3, :cond_1

    float-to-double v0, v13

    move-wide/from16 v16, v0

    const-wide v18, 0x3ff199999999999aL    # 1.1

    cmpg-double v3, v16, v18

    if-gez v3, :cond_1

    float-to-double v0, v12

    move-wide/from16 v16, v0

    const-wide v18, 0x3ff199999999999aL    # 1.1

    cmpg-double v3, v16, v18

    if-gez v3, :cond_1

    if-nez p5, :cond_1

    .line 156
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 160
    :cond_1
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 162
    .local v7, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v7, v13, v12}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 168
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 172
    .local v11, "resizedBitmap":Landroid/graphics/Bitmap;
    if-eqz p5, :cond_2

    .line 173
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    invoke-direct {v3, v4, v11}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 175
    :cond_2
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v11}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "checkFalseImg"    # Ljava/lang/String;
    .param p2, "checkTrueImg"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 303
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 305
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 306
    new-array v1, v4, [I

    const v2, -0x10100a0

    aput v2, v1, v3

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 308
    :cond_0
    if-eqz p2, :cond_1

    .line 309
    new-array v1, v4, [I

    const v2, 0x10100a0

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 311
    :cond_1
    return-object v0
.end method

.method protected setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "checkFalseImg"    # Ljava/lang/String;
    .param p2, "checkTrueImg"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 316
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 318
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 319
    new-array v1, v4, [I

    const v2, -0x10100a0

    aput v2, v1, v3

    .line 320
    invoke-virtual {p0, p1, p3, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 319
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 322
    :cond_0
    if-eqz p2, :cond_1

    .line 323
    new-array v1, v4, [I

    const v2, 0x10100a0

    aput v2, v1, v3

    .line 324
    invoke-virtual {p0, p2, p3, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 323
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 326
    :cond_1
    return-object v0
.end method

.method protected setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "checkFalseImg"    # Ljava/lang/String;
    .param p2, "checkTrueImg"    # Ljava/lang/String;
    .param p3, "focusCheckImg"    # Ljava/lang/String;
    .param p4, "focusUncheckImg"    # Ljava/lang/String;
    .param p5, "width"    # I
    .param p6, "height"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x3

    .line 332
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 334
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p2, :cond_0

    .line 335
    new-array v1, v3, [I

    fill-array-data v1, :array_0

    .line 336
    invoke-virtual {p0, p2, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 335
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 339
    :cond_0
    if-eqz p1, :cond_1

    .line 340
    new-array v1, v3, [I

    fill-array-data v1, :array_1

    .line 341
    invoke-virtual {p0, p1, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 340
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 344
    :cond_1
    if-eqz p3, :cond_2

    .line 345
    new-array v1, v3, [I

    fill-array-data v1, :array_2

    .line 346
    invoke-virtual {p0, p3, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 345
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 349
    :cond_2
    if-eqz p4, :cond_3

    .line 350
    new-array v1, v3, [I

    fill-array-data v1, :array_3

    .line 351
    invoke-virtual {p0, p4, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 350
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 354
    :cond_3
    if-eqz p1, :cond_4

    .line 355
    new-array v1, v4, [I

    fill-array-data v1, :array_4

    .line 356
    invoke-virtual {p0, p1, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 355
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 359
    :cond_4
    if-eqz p2, :cond_5

    .line 360
    new-array v1, v4, [I

    fill-array-data v1, :array_5

    .line 361
    invoke-virtual {p0, p2, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 360
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 364
    :cond_5
    return-object v0

    .line 335
    :array_0
    .array-data 4
        0x10100a0
        -0x101009c
        0x101009e
    .end array-data

    .line 340
    :array_1
    .array-data 4
        -0x10100a0
        -0x101009c
        0x101009e
    .end array-data

    .line 345
    :array_2
    .array-data 4
        0x10100a0
        0x101009c
        0x101009e
    .end array-data

    .line 350
    :array_3
    .array-data 4
        -0x10100a0
        0x101009c
        0x101009e
    .end array-data

    .line 355
    :array_4
    .array-data 4
        -0x10100a0
        0x101009e
    .end array-data

    .line 360
    :array_5
    .array-data 4
        0x10100a0
        0x101009e
    .end array-data
.end method

.method protected setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "checkFalseImg"    # Ljava/lang/String;
    .param p2, "checkTrueImg"    # Ljava/lang/String;
    .param p3, "focusCheckImg"    # Ljava/lang/String;
    .param p4, "focusUncheckImg"    # Ljava/lang/String;
    .param p5, "pressCheckImg"    # Ljava/lang/String;
    .param p6, "pressUncheckImg"    # Ljava/lang/String;
    .param p7, "width"    # I
    .param p8, "height"    # I

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    .line 370
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 372
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p2, :cond_0

    .line 373
    new-array v1, v4, [I

    fill-array-data v1, :array_0

    .line 375
    invoke-virtual {p0, p2, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 373
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 378
    :cond_0
    if-eqz p1, :cond_1

    .line 379
    new-array v1, v4, [I

    fill-array-data v1, :array_1

    .line 381
    invoke-virtual {p0, p1, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 379
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 384
    :cond_1
    if-eqz p3, :cond_2

    .line 385
    new-array v1, v4, [I

    fill-array-data v1, :array_2

    .line 387
    invoke-virtual {p0, p3, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 385
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 390
    :cond_2
    if-eqz p4, :cond_3

    .line 391
    new-array v1, v4, [I

    fill-array-data v1, :array_3

    .line 393
    invoke-virtual {p0, p4, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 391
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 395
    :cond_3
    if-eqz p5, :cond_4

    .line 396
    new-array v1, v3, [I

    fill-array-data v1, :array_4

    .line 397
    invoke-virtual {p0, p5, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 396
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 400
    :cond_4
    if-eqz p6, :cond_5

    .line 401
    new-array v1, v3, [I

    fill-array-data v1, :array_5

    .line 402
    invoke-virtual {p0, p6, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 401
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 405
    :cond_5
    if-eqz p1, :cond_6

    .line 406
    new-array v1, v3, [I

    fill-array-data v1, :array_6

    .line 407
    invoke-virtual {p0, p1, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 406
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 410
    :cond_6
    if-eqz p2, :cond_7

    .line 411
    new-array v1, v3, [I

    fill-array-data v1, :array_7

    .line 412
    invoke-virtual {p0, p2, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 411
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 415
    :cond_7
    return-object v0

    .line 373
    :array_0
    .array-data 4
        0x10100a0
        -0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 379
    :array_1
    .array-data 4
        -0x10100a0
        -0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 385
    :array_2
    .array-data 4
        0x10100a0
        0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 391
    :array_3
    .array-data 4
        -0x10100a0
        0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 396
    :array_4
    .array-data 4
        0x10100a0
        0x10100a7
        0x101009e
    .end array-data

    .line 401
    :array_5
    .array-data 4
        -0x10100a0
        0x10100a7
        0x101009e
    .end array-data

    .line 406
    :array_6
    .array-data 4
        -0x10100a0
        -0x10100a7
        0x101009e
    .end array-data

    .line 411
    :array_7
    .array-data 4
        0x10100a0
        -0x10100a7
        0x101009e
    .end array-data
.end method

.method protected setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "checkFalseImg"    # Ljava/lang/String;
    .param p2, "checkTrueImg"    # Ljava/lang/String;
    .param p3, "focusCheckImg"    # Ljava/lang/String;
    .param p4, "focusUncheckImg"    # Ljava/lang/String;
    .param p5, "pressCheckImg"    # Ljava/lang/String;
    .param p6, "pressUncheckImg"    # Ljava/lang/String;
    .param p7, "width"    # I
    .param p8, "height"    # I
    .param p9, "forceRezie"    # Z

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    .line 553
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 555
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p2, :cond_0

    .line 556
    new-array v1, v4, [I

    fill-array-data v1, :array_0

    .line 558
    invoke-virtual {p0, p2, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 556
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 561
    :cond_0
    if-eqz p1, :cond_1

    .line 562
    new-array v1, v4, [I

    fill-array-data v1, :array_1

    .line 564
    invoke-virtual {p0, p1, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 562
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 567
    :cond_1
    if-eqz p3, :cond_2

    .line 568
    new-array v1, v4, [I

    fill-array-data v1, :array_2

    .line 570
    invoke-virtual {p0, p3, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 568
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 573
    :cond_2
    if-eqz p4, :cond_3

    .line 574
    new-array v1, v4, [I

    fill-array-data v1, :array_3

    .line 576
    invoke-virtual {p0, p4, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 574
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 578
    :cond_3
    if-eqz p5, :cond_4

    .line 579
    new-array v1, v3, [I

    fill-array-data v1, :array_4

    .line 580
    invoke-virtual {p0, p5, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 579
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 583
    :cond_4
    if-eqz p6, :cond_5

    .line 584
    new-array v1, v3, [I

    fill-array-data v1, :array_5

    .line 585
    invoke-virtual {p0, p6, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 584
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 588
    :cond_5
    if-eqz p1, :cond_6

    .line 589
    new-array v1, v3, [I

    fill-array-data v1, :array_6

    .line 590
    invoke-virtual {p0, p1, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 589
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 593
    :cond_6
    if-eqz p2, :cond_7

    .line 594
    new-array v1, v3, [I

    fill-array-data v1, :array_7

    .line 595
    invoke-virtual {p0, p2, p7, p8, p9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 594
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 598
    :cond_7
    return-object v0

    .line 556
    :array_0
    .array-data 4
        0x10100a0
        -0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 562
    :array_1
    .array-data 4
        -0x10100a0
        -0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 568
    :array_2
    .array-data 4
        0x10100a0
        0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 574
    :array_3
    .array-data 4
        -0x10100a0
        0x101009c
        -0x10100a7
        0x101009e
    .end array-data

    .line 579
    :array_4
    .array-data 4
        0x10100a0
        0x10100a7
        0x101009e
    .end array-data

    .line 584
    :array_5
    .array-data 4
        -0x10100a0
        0x10100a7
        0x101009e
    .end array-data

    .line 589
    :array_6
    .array-data 4
        -0x10100a0
        -0x10100a7
        0x101009e
    .end array-data

    .line 594
    :array_7
    .array-data 4
        0x10100a0
        -0x10100a7
        0x101009e
    .end array-data
.end method

.method protected setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "focusedImg"    # Ljava/lang/String;
    .param p4, "dimImg"    # Ljava/lang/String;
    .param p5, "width"    # I
    .param p6, "height"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 268
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 270
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 271
    const/4 v1, 0x4

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 273
    invoke-virtual {p0, p1, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 271
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 276
    :cond_0
    if-eqz p2, :cond_1

    .line 277
    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    .line 278
    invoke-virtual {p0, p2, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 277
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 281
    :cond_1
    if-eqz p3, :cond_2

    .line 282
    new-array v1, v4, [I

    const v2, 0x101009c

    aput v2, v1, v3

    .line 283
    invoke-virtual {p0, p3, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 282
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 285
    :cond_2
    if-eqz p4, :cond_3

    .line 286
    new-array v1, v4, [I

    const v2, -0x101009e

    aput v2, v1, v3

    .line 287
    invoke-virtual {p0, p4, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 286
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 289
    :cond_3
    return-object v0

    .line 271
    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
        0x101009e
    .end array-data
.end method

.method protected setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "drawableName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 75
    const/4 v0, 0x0

    .line 77
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    const-string v4, "spen_sdk_resource_custom"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 78
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    const-string v4, "drawable"

    .line 79
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 78
    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 81
    .local v1, "mDrawableResID":I
    if-nez v1, :cond_2

    .line 82
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string v4, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 83
    if-nez v1, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-object v2

    .line 88
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    invoke-static {v2, v1}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v2, v0

    .line 89
    goto :goto_0

    .line 92
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    invoke-static {v2, v1}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v2, v0

    .line 93
    goto :goto_0

    .line 95
    .end local v1    # "mDrawableResID":I
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string v4, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 96
    .restart local v1    # "mDrawableResID":I
    if-eqz v1, :cond_0

    .line 100
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    invoke-static {v2, v1}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v2, v0

    .line 101
    goto :goto_0
.end method

.method protected setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;
    .locals 7
    .param p1, "drawableName"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 108
    const/4 v6, 0x0

    .line 110
    .local v6, "drawable":Landroid/graphics/drawable/Drawable;
    const-string v1, "spen_sdk_resource_custom"

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 111
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    const-string v3, "drawable"

    .line 112
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 111
    invoke-virtual {v1, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 114
    .local v2, "mDrawableResID":I
    if-nez v2, :cond_2

    .line 115
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string v3, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 116
    if-nez v2, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-object v0

    .line 119
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    move-object v0, p0

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    move-object v0, v6

    .line 121
    goto :goto_0

    .line 123
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    move-object v0, p0

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    move-object v0, v6

    .line 125
    goto :goto_0

    .line 127
    .end local v2    # "mDrawableResID":I
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string v3, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 128
    .restart local v2    # "mDrawableResID":I
    if-eqz v2, :cond_0

    .line 131
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    move-object v0, p0

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    move-object v0, v6

    .line 132
    goto :goto_0
.end method

.method protected setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;
    .locals 7
    .param p1, "drawableName"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "forceResize"    # Z

    .prologue
    const/4 v0, 0x0

    .line 628
    const/4 v6, 0x0

    .line 630
    .local v6, "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    const-string v3, "spen_sdk_resource_custom"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 631
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    const-string v3, "drawable"

    .line 632
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 631
    invoke-virtual {v1, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 634
    .local v2, "mDrawableResID":I
    if-nez v2, :cond_2

    .line 635
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string v3, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 636
    if-nez v2, :cond_1

    .line 652
    :cond_0
    :goto_0
    return-object v0

    .line 639
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    move-object v0, p0

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    move-object v0, v6

    .line 641
    goto :goto_0

    .line 643
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    move-object v0, p0

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    move-object v0, v6

    .line 645
    goto :goto_0

    .line 647
    .end local v2    # "mDrawableResID":I
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string v3, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 648
    .restart local v2    # "mDrawableResID":I
    if-eqz v2, :cond_0

    .line 651
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    move-object v0, p0

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;IIIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    move-object v0, v6

    .line 652
    goto :goto_0
.end method

.method protected setDrawableSelectColor(III)Landroid/graphics/drawable/StateListDrawable;
    .locals 7
    .param p1, "defaultColor"    # I
    .param p2, "selectedColor"    # I
    .param p3, "pressColor"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 477
    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 479
    .local v1, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    const/4 v4, 0x2

    new-array v0, v4, [I

    .line 480
    .local v0, "defaultState":[I
    const v4, -0x10100a7

    aput v4, v0, v5

    .line 481
    const v4, -0x10100a1

    aput v4, v0, v6

    .line 483
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v0, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 484
    new-array v3, v6, [I

    .line 485
    .local v3, "selecteState":[I
    const v4, 0x10100a7

    aput v4, v3, v5

    .line 486
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, p2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 487
    new-array v2, v6, [I

    .line 488
    .local v2, "pressState":[I
    const v4, 0x10100a1

    aput v4, v2, v5

    .line 489
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, p3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 491
    return-object v1
.end method

.method protected setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "focusedImg"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 194
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 196
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 197
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 198
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 197
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 200
    :cond_0
    if-eqz p2, :cond_1

    .line 201
    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 203
    :cond_1
    if-eqz p3, :cond_2

    .line 204
    new-array v1, v4, [I

    const v2, 0x101009c

    aput v2, v1, v3

    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 206
    :cond_2
    if-eqz p2, :cond_3

    .line 207
    new-array v1, v4, [I

    const v2, 0x10100a1

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 210
    :cond_3
    return-object v0

    .line 197
    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
    .end array-data
.end method

.method protected setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "focusedImg"    # Ljava/lang/String;
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 216
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 217
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 218
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 219
    invoke-virtual {p0, p1, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 218
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 222
    :cond_0
    if-eqz p2, :cond_1

    .line 223
    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    .line 224
    invoke-virtual {p0, p2, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 223
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 226
    :cond_1
    if-eqz p3, :cond_2

    .line 227
    new-array v1, v4, [I

    const v2, 0x101009c

    aput v2, v1, v3

    .line 228
    invoke-virtual {p0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 227
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 230
    :cond_2
    if-eqz p2, :cond_3

    .line 231
    new-array v1, v4, [I

    const v2, 0x10100a1

    aput v2, v1, v3

    .line 232
    invoke-virtual {p0, p2, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 231
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 235
    :cond_3
    return-object v0

    .line 218
    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
    .end array-data
.end method

.method protected setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Landroid/graphics/drawable/StateListDrawable;
    .locals 5
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "focusedImg"    # Ljava/lang/String;
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "forceResize"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 604
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 605
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 606
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 607
    invoke-virtual {p0, p1, p4, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 606
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 610
    :cond_0
    if-eqz p2, :cond_1

    .line 611
    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    .line 612
    invoke-virtual {p0, p2, p4, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 611
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 614
    :cond_1
    if-eqz p3, :cond_2

    .line 615
    new-array v1, v4, [I

    const v2, 0x101009c

    aput v2, v1, v3

    .line 616
    invoke-virtual {p0, p3, p4, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 615
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 618
    :cond_2
    if-eqz p2, :cond_3

    .line 619
    new-array v1, v4, [I

    const v2, 0x10100a1

    aput v2, v1, v3

    .line 620
    invoke-virtual {p0, p2, p4, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 619
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 623
    :cond_3
    return-object v0

    .line 606
    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
    .end array-data
.end method

.method protected setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 9
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "focusedImg"    # Ljava/lang/String;
    .param p4, "selectedImg"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 433
    new-instance v4, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v4}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 434
    .local v4, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 436
    const/4 v5, 0x3

    new-array v3, v5, [I

    .line 437
    .local v3, "arrayOfInt4":[I
    const v5, -0x10100a7

    aput v5, v3, v7

    .line 438
    const v5, -0x10100a1

    aput v5, v3, v8

    .line 439
    const/4 v5, 0x2

    const v6, -0x101009c

    aput v6, v3, v5

    .line 440
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 442
    .end local v3    # "arrayOfInt4":[I
    :cond_0
    if-eqz p3, :cond_1

    .line 444
    new-array v0, v8, [I

    .line 445
    .local v0, "arrayOfInt1":[I
    const v5, 0x101009c

    aput v5, v0, v7

    .line 446
    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 448
    .end local v0    # "arrayOfInt1":[I
    :cond_1
    if-eqz p2, :cond_2

    .line 450
    new-array v2, v8, [I

    .line 451
    .local v2, "arrayOfInt3":[I
    const v5, 0x10100a7

    aput v5, v2, v7

    .line 452
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 455
    .end local v2    # "arrayOfInt3":[I
    :cond_2
    if-eqz p4, :cond_3

    .line 457
    new-array v1, v8, [I

    .line 458
    .local v1, "arrayOfInt2":[I
    const v5, 0x10100a1

    aput v5, v1, v7

    .line 459
    invoke-virtual {p0, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 462
    .end local v1    # "arrayOfInt2":[I
    :cond_3
    return-object v4
.end method

.method protected setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 12
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "focusedImg"    # Ljava/lang/String;
    .param p4, "selectedImg"    # Ljava/lang/String;
    .param p5, "dimImg"    # Ljava/lang/String;
    .param p6, "width"    # I
    .param p7, "height"    # I

    .prologue
    .line 659
    new-instance v9, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v9}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 660
    .local v9, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 662
    const/4 v10, 0x4

    new-array v7, v10, [I

    .line 663
    .local v7, "arrayOfInt4":[I
    const/4 v10, 0x0

    const v11, -0x10100a7

    aput v11, v7, v10

    .line 664
    const/4 v10, 0x1

    const v11, -0x10100a1

    aput v11, v7, v10

    .line 665
    const/4 v10, 0x2

    const v11, -0x101009c

    aput v11, v7, v10

    .line 666
    const/4 v10, 0x3

    const v11, 0x101009e

    aput v11, v7, v10

    .line 668
    move/from16 v0, p6

    move/from16 v1, p7

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v7, v10}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 670
    if-nez p2, :cond_0

    .line 671
    const/4 v10, 0x1

    new-array v8, v10, [I

    .line 672
    .local v8, "arrayOfInt5":[I
    const/4 v10, 0x0

    const v11, 0x10100a7

    aput v11, v8, v10

    .line 674
    move/from16 v0, p6

    move/from16 v1, p7

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v8, v10}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 678
    .end local v7    # "arrayOfInt4":[I
    .end local v8    # "arrayOfInt5":[I
    :cond_0
    if-eqz p3, :cond_1

    .line 680
    const/4 v10, 0x1

    new-array v4, v10, [I

    .line 681
    .local v4, "arrayOfInt1":[I
    const/4 v10, 0x0

    const v11, 0x101009c

    aput v11, v4, v10

    .line 682
    move/from16 v0, p6

    move/from16 v1, p7

    invoke-virtual {p0, p3, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v4, v10}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 684
    .end local v4    # "arrayOfInt1":[I
    :cond_1
    if-eqz p2, :cond_2

    .line 686
    const/4 v10, 0x1

    new-array v6, v10, [I

    .line 687
    .local v6, "arrayOfInt3":[I
    const/4 v10, 0x0

    const v11, 0x10100a7

    aput v11, v6, v10

    .line 688
    move/from16 v0, p6

    move/from16 v1, p7

    invoke-virtual {p0, p2, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v6, v10}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 691
    .end local v6    # "arrayOfInt3":[I
    :cond_2
    if-eqz p4, :cond_3

    .line 693
    const/4 v10, 0x1

    new-array v5, v10, [I

    .line 694
    .local v5, "arrayOfInt2":[I
    const/4 v10, 0x0

    const v11, 0x10100a1

    aput v11, v5, v10

    .line 695
    move-object/from16 v0, p4

    move/from16 v1, p6

    move/from16 v2, p7

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v5, v10}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 697
    .end local v5    # "arrayOfInt2":[I
    :cond_3
    if-eqz p5, :cond_4

    .line 698
    const/4 v10, 0x1

    new-array v3, v10, [I

    .line 699
    .local v3, "arrayOfInt0":[I
    const/4 v10, 0x0

    const v11, -0x101009e

    aput v11, v3, v10

    .line 700
    move-object/from16 v0, p5

    move/from16 v1, p6

    move/from16 v2, p7

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v3, v10}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 703
    .end local v3    # "arrayOfInt0":[I
    :cond_4
    return-object v9
.end method

.method protected setDrawableSelectedFocusImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 6
    .param p1, "defaultImg"    # Ljava/lang/String;
    .param p2, "pressedImg"    # Ljava/lang/String;
    .param p3, "selectedImg"    # Ljava/lang/String;
    .param p4, "selectfocusedImg"    # Ljava/lang/String;
    .param p5, "unselectfocusedImg"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 241
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 243
    .local v0, "localStateListDrawable":Landroid/graphics/drawable/StateListDrawable;
    if-eqz p1, :cond_0

    .line 244
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 245
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 244
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 247
    :cond_0
    if-eqz p2, :cond_1

    .line 248
    new-array v1, v4, [I

    const v2, 0x10100a7

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 250
    :cond_1
    if-eqz p4, :cond_2

    .line 251
    new-array v1, v5, [I

    fill-array-data v1, :array_1

    .line 252
    invoke-virtual {p0, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 251
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 254
    :cond_2
    if-eqz p5, :cond_3

    .line 255
    new-array v1, v5, [I

    fill-array-data v1, :array_2

    .line 256
    invoke-virtual {p0, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 255
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 258
    :cond_3
    if-eqz p3, :cond_4

    .line 259
    new-array v1, v4, [I

    const v2, 0x10100a1

    aput v2, v1, v3

    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 262
    :cond_4
    return-object v0

    .line 244
    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
    .end array-data

    .line 251
    :array_1
    .array-data 4
        0x101009c
        0x10100a1
    .end array-data

    .line 255
    :array_2
    .array-data 4
        0x101009c
        -0x10100a1
    .end array-data
.end method

.method public unbindDrawables(Landroid/view/View;)V
    .locals 4
    .param p1, "root"    # Landroid/view/View;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 507
    if-nez p1, :cond_1

    .line 547
    :cond_0
    :goto_0
    return-void

    .line 511
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_7

    .line 512
    invoke-virtual {p1, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 517
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 518
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 520
    :cond_2
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    .line 521
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    move-object v1, p1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_8

    .line 524
    instance-of v1, p1, Landroid/widget/AdapterView;

    if-nez v1, :cond_9

    move-object v1, p1

    .line 525
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 531
    .end local v0    # "i":I
    :cond_3
    :goto_3
    instance-of v1, p1, Landroid/widget/SeekBar;

    if-eqz v1, :cond_4

    move-object v1, p1

    .line 532
    check-cast v1, Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 535
    :cond_4
    instance-of v1, p1, Landroid/widget/ImageView;

    if-eqz v1, :cond_5

    move-object v1, p1

    .line 536
    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v1, p1

    .line 537
    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 539
    :cond_5
    instance-of v1, p1, Landroid/widget/ImageButton;

    if-eqz v1, :cond_6

    move-object v1, p1

    .line 540
    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v1, p1

    .line 541
    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 544
    :cond_6
    if-eqz p1, :cond_0

    .line 545
    const/4 p1, 0x0

    goto :goto_0

    .line 514
    :cond_7
    invoke-virtual {p1, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .restart local v0    # "i":I
    :cond_8
    move-object v1, p1

    .line 522
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    .line 521
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 527
    :cond_9
    check-cast p1, Landroid/widget/AdapterView;

    .end local p1    # "root":Landroid/view/View;
    invoke-virtual {p1, v3}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 528
    const/4 p1, 0x0

    .restart local p1    # "root":Landroid/view/View;
    goto :goto_3
.end method
