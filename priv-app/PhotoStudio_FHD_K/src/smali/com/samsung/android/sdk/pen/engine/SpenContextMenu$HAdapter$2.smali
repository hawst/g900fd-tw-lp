.class Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$2;
.super Ljava/lang/Object;
.source "SpenContextMenu.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$2;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;

    .line 1213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1217
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1233
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1219
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$2;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$2;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1220
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$2;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->drawTalkbackBorder(I)V

    goto :goto_0

    .line 1225
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$2;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$2;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1226
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$2;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->drawTalkbackBorder(I)V

    goto :goto_0

    .line 1217
    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
