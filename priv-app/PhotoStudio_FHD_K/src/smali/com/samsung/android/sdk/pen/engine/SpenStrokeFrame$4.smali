.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;
.super Ljava/lang/Object;
.source "SpenStrokeFrame.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->startCamera()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    .line 604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnPreview()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    const v5, 0x38d1b717    # 1.0E-4f

    .line 608
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mReadyCameraPreview:Z
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 609
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v4, v8}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Z)V

    .line 610
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/ImageButton;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 611
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/ImageButton;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 612
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeCameraButton:Landroid/widget/ImageButton;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/ImageButton;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 614
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->getCamera()Landroid/hardware/Camera;

    move-result-object v4

    invoke-virtual {v4}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v1

    .line 615
    .local v1, "size":Landroid/hardware/Camera$Size;
    if-nez v1, :cond_2

    .line 616
    const-string v4, "SpenStrokeFrame"

    const-string v5, "Camera PreviewSize is null"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    :cond_1
    :goto_0
    return-void

    .line 619
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v3

    .line 620
    .local v3, "width":F
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v0

    .line 622
    .local v0, "height":F
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_1

    iget v4, v1, Landroid/hardware/Camera$Size;->width:I

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-float v4, v4

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_1

    .line 623
    iget v4, v1, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-float v4, v4

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_1

    .line 627
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$24(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v6, :cond_5

    .line 628
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget v5, v1, Landroid/hardware/Camera$Size;->width:I

    int-to-float v5, v5

    iget v6, v1, Landroid/hardware/Camera$Size;->height:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V

    .line 630
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v4

    div-float v5, v0, v3

    cmpl-float v4, v4, v5

    if-lez v4, :cond_4

    .line 631
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v4

    div-float v5, v3, v0

    mul-float/2addr v4, v5

    mul-float/2addr v4, v0

    float-to-int v4, v4

    int-to-float v0, v4

    .line 635
    :goto_1
    cmpl-float v4, v3, v0

    if-lez v4, :cond_3

    .line 636
    move v2, v3

    .line 637
    .local v2, "tmp":F
    move v3, v0

    .line 638
    move v0, v2

    .line 655
    .end local v2    # "tmp":F
    :cond_3
    :goto_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    float-to-int v6, v3

    float-to-int v7, v0

    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$27(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/widget/RelativeLayout$LayoutParams;)V

    .line 656
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v6

    iget v6, v6, Landroid/graphics/RectF;->top:F

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 658
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaY:F
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 659
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaY:F
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 660
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaX:F
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 661
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mDeltaX:F
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 663
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 633
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v4

    div-float v5, v3, v0

    mul-float/2addr v4, v5

    div-float v4, v3, v4

    float-to-int v4, v4

    int-to-float v3, v4

    goto/16 :goto_1

    .line 641
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget v5, v1, Landroid/hardware/Camera$Size;->height:I

    int-to-float v5, v5

    iget v6, v1, Landroid/hardware/Camera$Size;->width:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V

    .line 643
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v4

    div-float v5, v0, v3

    cmpl-float v4, v4, v5

    if-lez v4, :cond_6

    .line 644
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v4

    div-float v5, v3, v0

    mul-float/2addr v4, v5

    mul-float/2addr v4, v0

    float-to-int v4, v4

    int-to-float v0, v4

    .line 648
    :goto_3
    cmpg-float v4, v3, v0

    if-gez v4, :cond_3

    .line 649
    move v2, v3

    .line 650
    .restart local v2    # "tmp":F
    move v3, v0

    .line 651
    move v0, v2

    goto/16 :goto_2

    .line 646
    .end local v2    # "tmp":F
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v4

    div-float v5, v3, v0

    mul-float/2addr v4, v5

    div-float v4, v3, v4

    float-to-int v4, v4

    int-to-float v3, v4

    goto :goto_3
.end method
