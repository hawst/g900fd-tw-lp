.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;
.super Ljava/lang/Object;
.source "SpenSettingPenLayout2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

.field private final synthetic val$fromFinal:F

.field private final synthetic val$step:F

.field private final synthetic val$toFinal:F


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;FFF)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    iput p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$fromFinal:F

    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$toFinal:F

    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$step:F

    .line 6717
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 6720
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCountForScrollPen:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$38(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$39(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V

    .line 6722
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$fromFinal:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$toFinal:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 6723
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$fromFinal:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCountForScrollPen:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$38(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$step:F

    mul-float/2addr v2, v3

    sub-float v0, v1, v2

    .line 6724
    .local v0, "pos":F
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$toFinal:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 6725
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    float-to-int v2, v0

    invoke-virtual {v1, v2, v4}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    .line 6747
    :cond_0
    :goto_0
    return-void

    .line 6727
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$toFinal:F

    float-to-int v2, v2

    invoke-virtual {v1, v2, v4}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    .line 6729
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimerForScrollPen:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Ljava/util/Timer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6730
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimerForScrollPen:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 6731
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    invoke-static {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Ljava/util/Timer;)V

    goto :goto_0

    .line 6735
    .end local v0    # "pos":F
    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$fromFinal:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCountForScrollPen:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$38(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$step:F

    mul-float/2addr v2, v3

    add-float v0, v1, v2

    .line 6736
    .restart local v0    # "pos":F
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$toFinal:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_3

    .line 6737
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    float-to-int v2, v0

    invoke-virtual {v1, v2, v4}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    goto :goto_0

    .line 6739
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$toFinal:F

    float-to-int v2, v2

    invoke-virtual {v1, v2, v4}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    .line 6741
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimerForScrollPen:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Ljava/util/Timer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6742
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimerForScrollPen:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 6743
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    invoke-static {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Ljava/util/Timer;)V

    goto/16 :goto_0
.end method
