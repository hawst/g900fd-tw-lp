.class public Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;
.super Ljava/lang/Object;
.source "Video.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation


# static fields
.field private static final EXTRADATASTRING_VIDEOPATH_KEY:Ljava/lang/String; = "VideoPath"

.field private static final GETEXTRADATASTRING_FUNCTION:Ljava/lang/String; = "getExtraDataString"

.field private static final GETEXTRADATASTRING_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static HASH_KEY_IMAGE_MARGIN:I = 0x0

.field private static HASH_KEY_IMAGE_SIZE:I = 0x0

.field private static HASH_KEY_PIXEL_1080_1920:I = 0x0

.field private static HASH_KEY_PIXEL_2560_1600:I = 0x0

.field private static HASH_KEY_PIXEL_720_1280:I = 0x0

.field private static HASH_KEY_PIXEL_DEFAULT:I = 0x0

.field private static final SETEXTRADATASTRING_FUNCTION:Ljava/lang/String; = "setExtraDataString"

.field private static final SETEXTRADATASTRING_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final SETIMAGE_FUNCTION:Ljava/lang/String; = "setImage"

.field private static final SETIMAGE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final SETRECT_FUNCTION:Ljava/lang/String; = "setRect"

.field private static final SETRECT_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final SETRESIZEOPTION_FUNCTION:Ljava/lang/String; = "setResizeOption"

.field private static final SETRESIZEOPTION_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final SETROTATABLE_FUNCTION:Ljava/lang/String; = "setRotatable"

.field private static final SETROTATABLE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final SETVISIBILITY_FUNCTION:Ljava/lang/String; = "setVisibility"

.field private static final SETVISIBILITY_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final SETWFDTCPDISABLE_FUNCTION:Ljava/lang/String; = "setWFDTcpDisable"

.field private static final SETWFDTCPDISABLE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "Video"

.field private static final VIDEOPLAY_IMAGE_NAME:Ljava/lang/String; = "snote_insert_video_icon_cue"

.field private static final VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Rect;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private fm:Landroid/app/FragmentManager;

.field private mActivity:Landroid/app/Activity;

.field mAudioLisner:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field mAudioManager:Landroid/media/AudioManager;

.field private mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

.field private mFragmentListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

.field private mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

.field private mObject:Ljava/lang/Object;

.field private mPan:Landroid/graphics/PointF;

.field private mRatio:F

.field private mRect:Landroid/graphics/RectF;

.field private mStartFramePosition:Landroid/graphics/PointF;

.field private mVideoFilePath:Ljava/lang/String;

.field private mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

.field private mVideoViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mVideoViewListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;

.field private mViewGroup:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/16 v8, 0x16

    const/4 v7, 0x7

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 53
    new-array v3, v6, [Ljava/lang/Class;

    const-class v4, Landroid/graphics/Bitmap;

    aput-object v4, v3, v5

    sput-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETIMAGE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    .line 55
    new-array v3, v9, [Ljava/lang/Class;

    const-class v4, Landroid/graphics/RectF;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    sput-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETRECT_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    .line 59
    new-array v3, v6, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sput-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->GETEXTRADATASTRING_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    .line 61
    new-array v3, v9, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    .line 62
    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v6

    .line 61
    sput-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETEXTRADATASTRING_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    .line 64
    new-array v3, v6, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v5

    sput-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETROTATABLE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    .line 66
    new-array v3, v6, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v5

    sput-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETVISIBILITY_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    .line 68
    new-array v3, v6, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v5

    sput-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETRESIZEOPTION_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    .line 70
    new-array v3, v6, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v5

    sput-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETWFDTCPDISABLE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    .line 75
    sput v5, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_SIZE:I

    .line 76
    sget v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_SIZE:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_MARGIN:I

    .line 77
    const/16 v3, 0xbb8

    sput v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_1080_1920:I

    .line 78
    const/16 v3, 0x1040

    sput v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_2560_1600:I

    .line 79
    const/16 v3, 0x7d0

    sput v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_720_1280:I

    .line 80
    sget v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_1080_1920:I

    sput v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_DEFAULT:I

    .line 82
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;

    .line 84
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v5, v5, v8, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 85
    .local v2, "size":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v5, v7, v7, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 86
    .local v1, "margin":Landroid/graphics/Rect;
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 87
    .local v0, "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    sget v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_1080_1920:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    new-instance v2, Landroid/graphics/Rect;

    .end local v2    # "size":Landroid/graphics/Rect;
    const/16 v3, 0x21

    const/16 v4, 0x21

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 92
    .restart local v2    # "size":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    const/16 v3, 0xa

    const/16 v4, 0xa

    invoke-direct {v1, v5, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 93
    .restart local v1    # "margin":Landroid/graphics/Rect;
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 94
    .restart local v0    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    sget v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_2560_1600:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    new-instance v2, Landroid/graphics/Rect;

    .end local v2    # "size":Landroid/graphics/Rect;
    invoke-direct {v2, v5, v5, v8, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 99
    .restart local v2    # "size":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "margin":Landroid/graphics/Rect;
    invoke-direct {v1, v5, v7, v7, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 100
    .restart local v1    # "margin":Landroid/graphics/Rect;
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 101
    .restart local v0    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    sget v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v4, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_720_1280:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;I)V
    .locals 0

    .prologue
    .line 622
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V

    return-void
.end method

.method static synthetic access$10()[Ljava/lang/Class;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETRECT_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 306
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 427
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->createPlayVideoBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$13()[Ljava/lang/Class;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETIMAGE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$14()[Ljava/lang/Class;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETEXTRADATASTRING_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$15()[Ljava/lang/Class;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETROTATABLE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$16()[Ljava/lang/Class;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETRESIZEOPTION_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

    return-object v0
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V
    .locals 0

    .prologue
    .line 628
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->complete()V

    return-void
.end method

.method static synthetic access$19()[Ljava/lang/Class;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETVISIBILITY_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoFilePath:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;IILandroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 325
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->getRatioRect(IILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 591
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->convertAbsolute(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    return-object v0
.end method

.method private cancel(I)V
    .locals 2
    .param p1, "action"    # I

    .prologue
    .line 623
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cleanResource()V

    .line 624
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;->onCanceled(ILjava/lang/Object;)V

    .line 626
    return-void
.end method

.method private cleanResource()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 635
    new-array v0, v3, [Ljava/lang/Object;

    .line 636
    .local v0, "args":[Ljava/lang/Object;
    const/4 v2, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v0, v2

    .line 638
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 639
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    const-string v3, "setVisibility"

    sget-object v4, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETVISIBILITY_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    invoke-direct {p0, v2, v3, v4, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 640
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;->onObjectUpdated(Landroid/graphics/RectF;Ljava/lang/Object;)V

    .line 643
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioLisner:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 644
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    if-eqz v2, :cond_1

    .line 645
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 646
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    .line 649
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    if-eqz v2, :cond_3

    .line 650
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->setVideoIntentFragmentListener(Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;)V

    .line 651
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->fm:Landroid/app/FragmentManager;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 653
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->fm:Landroid/app/FragmentManager;

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 658
    :cond_2
    :goto_0
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    .line 660
    :cond_3
    return-void

    .line 654
    :catch_0
    move-exception v1

    .line 655
    .local v1, "e":Ljava/lang/IllegalStateException;
    const-string v2, "Video"

    const-string v3, "commitAllowingStateLoss IllegalStateException is occured. but is not problem."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private complete()V
    .locals 2

    .prologue
    .line 629
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cleanResource()V

    .line 630
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;->onCompleted(Ljava/lang/Object;)V

    .line 632
    return-void
.end method

.method private convertAbsolute(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 3
    .param p1, "relativeRect"    # Landroid/graphics/RectF;

    .prologue
    .line 592
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 594
    .local v0, "dstRect":Landroid/graphics/RectF;
    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 595
    iget v1, p1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 596
    iget v1, p1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 597
    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 599
    return-object v0
.end method

.method private createPlayVideoBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    .locals 24
    .param p1, "srcBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "noneRatioRect"    # Landroid/graphics/RectF;

    .prologue
    .line 428
    const/4 v14, 0x0

    .line 429
    .local v14, "res":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    .line 430
    .local v13, "pm":Landroid/content/pm/PackageManager;
    if-nez v13, :cond_0

    .line 431
    const-string v20, "Video"

    const-string v21, "PackageManager is null."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    const/16 v19, 0x0

    .line 502
    :goto_0
    return-object v19

    .line 436
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    .line 442
    invoke-virtual {v14}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    .line 443
    .local v6, "dm":Landroid/util/DisplayMetrics;
    if-nez v6, :cond_1

    .line 444
    const-string v20, "Video"

    const-string v21, "DisplayMetrics Get is failed"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    const/16 v19, 0x0

    goto :goto_0

    .line 437
    .end local v6    # "dm":Landroid/util/DisplayMetrics;
    :catch_0
    move-exception v8

    .line 438
    .local v8, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v8}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 439
    const/16 v19, 0x0

    goto :goto_0

    .line 448
    .end local v8    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v6    # "dm":Landroid/util/DisplayMetrics;
    :cond_1
    const-string v20, "snote_insert_video_icon_cue"

    const-string v21, "drawable"

    invoke-static {}, Lcom/samsung/android/sdk/pen/Spen;->getSpenPackageName()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v14, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 449
    .local v9, "id":I
    if-nez v9, :cond_2

    .line 450
    const-string v20, "Video"

    const-string v21, "Resource is not founded"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    const/16 v19, 0x0

    goto :goto_0

    .line 454
    :cond_2
    const/16 v18, 0x0

    .line 456
    .local v18, "stream":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v14, v9}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v18

    .line 462
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 463
    .local v15, "resourceBitmap":Landroid/graphics/Bitmap;
    if-nez v15, :cond_3

    .line 464
    const-string v20, "Video"

    const-string v21, "The bitmap of resource is null."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    const/16 v19, 0x0

    goto :goto_0

    .line 457
    .end local v15    # "resourceBitmap":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v8

    .line 458
    .local v8, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v8}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    .line 459
    const/16 v19, 0x0

    goto :goto_0

    .line 468
    .end local v8    # "e":Landroid/content/res/Resources$NotFoundException;
    .restart local v15    # "resourceBitmap":Landroid/graphics/Bitmap;
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    .line 469
    sget-object v22, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 468
    invoke-static/range {v20 .. v22}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 470
    .local v19, "workBitmap":Landroid/graphics/Bitmap;
    if-nez v19, :cond_4

    .line 471
    const-string v20, "Video"

    const-string v21, "The workBitmap of result is null. out fo memory"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    invoke-virtual {v15}, Landroid/graphics/Bitmap;->recycle()V

    .line 473
    const/4 v15, 0x0

    .line 474
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 476
    :cond_4
    new-instance v5, Landroid/graphics/Canvas;

    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 477
    .local v5, "canvas":Landroid/graphics/Canvas;
    new-instance v17, Landroid/graphics/Rect;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Rect;-><init>()V

    .line 478
    .local v17, "src":Landroid/graphics/Rect;
    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v22

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v23

    move-object/from16 v0, v17

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 479
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 480
    .local v7, "dst":Landroid/graphics/Rect;
    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v22

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v7, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 481
    new-instance v12, Landroid/graphics/Paint;

    const/16 v20, 0x7

    move/from16 v0, v20

    invoke-direct {v12, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 482
    .local v12, "paint":Landroid/graphics/Paint;
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v5, v0, v1, v7, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 484
    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v22

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v23

    move-object/from16 v0, v17

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 486
    iget v0, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v20, v0

    iget v0, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v21, v0

    add-int v10, v20, v21

    .line 487
    .local v10, "key":I
    sget-object v20, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_5

    .line 488
    sget v10, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_DEFAULT:I

    .line 491
    :cond_5
    sget-object v20, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/HashMap;

    sget v21, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Rect;

    .line 492
    .local v11, "margin":Landroid/graphics/Rect;
    sget-object v20, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/HashMap;

    sget v21, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_SIZE:I

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/graphics/Rect;

    .line 493
    .local v16, "size":Landroid/graphics/Rect;
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    iget v0, v6, Landroid/util/DisplayMetrics;->density:F

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->getIntValueAppliedDensity(FF)I

    move-result v21

    sub-int v20, v20, v21

    .line 494
    iget v0, v11, Landroid/graphics/Rect;->right:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    iget v0, v6, Landroid/util/DisplayMetrics;->density:F

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->getIntValueAppliedDensity(FF)I

    move-result v21

    sub-int v20, v20, v21

    .line 493
    move/from16 v0, v20

    iput v0, v7, Landroid/graphics/Rect;->left:I

    .line 495
    iget v0, v11, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    iget v0, v6, Landroid/util/DisplayMetrics;->density:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->getIntValueAppliedDensity(FF)I

    move-result v20

    move/from16 v0, v20

    iput v0, v7, Landroid/graphics/Rect;->top:I

    .line 496
    iget v0, v7, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    iget v0, v6, Landroid/util/DisplayMetrics;->density:F

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->getIntValueAppliedDensity(FF)I

    move-result v21

    add-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v7, Landroid/graphics/Rect;->right:I

    .line 497
    iget v0, v7, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    iget v0, v6, Landroid/util/DisplayMetrics;->density:F

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->getIntValueAppliedDensity(FF)I

    move-result v21

    add-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    .line 498
    move-object/from16 v0, v17

    invoke-virtual {v5, v15, v0, v7, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 499
    invoke-virtual {v15}, Landroid/graphics/Bitmap;->recycle()V

    .line 500
    const/4 v15, 0x0

    .line 502
    goto/16 :goto_0
.end method

.method private getIntValueAppliedDensity(FF)I
    .locals 1
    .param p1, "paramFloat"    # F
    .param p2, "density"    # F

    .prologue
    .line 663
    mul-float v0, p1, p2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private getRatioRect(IILandroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 12
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "outNoneRatioRect"    # Landroid/graphics/RectF;

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const v10, 0x3f2aaaab

    .line 326
    int-to-float v8, p1

    int-to-float v9, p2

    div-float v4, v8, v9

    .line 328
    .local v4, "ratio":F
    const/4 v7, 0x0

    .line 329
    .local v7, "width":F
    const/4 v2, 0x0

    .line 331
    .local v2, "height":F
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v9

    cmpg-float v8, v8, v9

    if-gez v8, :cond_1

    .line 332
    if-le p1, p2, :cond_0

    .line 333
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    mul-float v7, v8, v10

    .line 334
    div-float v2, v7, v4

    .line 349
    :goto_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getWidth()I

    move-result v8

    div-int/lit8 v0, v8, 0x2

    .line 350
    .local v0, "centerX":I
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getHeight()I

    move-result v8

    div-int/lit8 v1, v8, 0x2

    .line 352
    .local v1, "centerY":I
    int-to-float v8, v0

    div-float v9, v7, v11

    sub-float/2addr v8, v9

    iput v8, p3, Landroid/graphics/RectF;->left:F

    .line 353
    int-to-float v8, v1

    div-float v9, v2, v11

    sub-float/2addr v8, v9

    iput v8, p3, Landroid/graphics/RectF;->top:F

    .line 354
    iget v8, p3, Landroid/graphics/RectF;->left:F

    add-float/2addr v8, v7

    iput v8, p3, Landroid/graphics/RectF;->right:F

    .line 355
    iget v8, p3, Landroid/graphics/RectF;->top:F

    add-float/2addr v8, v2

    iput v8, p3, Landroid/graphics/RectF;->bottom:F

    .line 357
    iget v8, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRatio:F

    mul-float v6, v7, v8

    .line 358
    .local v6, "ratioWidth":F
    iget v8, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRatio:F

    mul-float v5, v2, v8

    .line 360
    .local v5, "ratioHeight":F
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 361
    .local v3, "r":Landroid/graphics/RectF;
    int-to-float v8, v0

    div-float v9, v6, v11

    sub-float/2addr v8, v9

    iput v8, v3, Landroid/graphics/RectF;->left:F

    .line 362
    int-to-float v8, v1

    div-float v9, v5, v11

    sub-float/2addr v8, v9

    iput v8, v3, Landroid/graphics/RectF;->top:F

    .line 363
    iget v8, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v8, v6

    iput v8, v3, Landroid/graphics/RectF;->right:F

    .line 364
    iget v8, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v8, v5

    iput v8, v3, Landroid/graphics/RectF;->bottom:F

    .line 366
    return-object v3

    .line 336
    .end local v0    # "centerX":I
    .end local v1    # "centerY":I
    .end local v3    # "r":Landroid/graphics/RectF;
    .end local v5    # "ratioHeight":F
    .end local v6    # "ratioWidth":F
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    mul-float v2, v8, v10

    .line 337
    mul-float v7, v2, v4

    .line 339
    goto :goto_0

    .line 340
    :cond_1
    if-le p1, p2, :cond_2

    .line 341
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v8

    mul-float v7, v8, v10

    .line 342
    div-float v2, v7, v4

    .line 343
    goto :goto_0

    .line 344
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v8

    mul-float v2, v8, v10

    .line 345
    mul-float v7, v2, v4

    goto :goto_0
.end method

.method private varargs mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "instance"    # Ljava/lang/Object;
    .param p2, "runner"    # Ljava/lang/String;
    .param p3, "clazz"    # [Ljava/lang/Class;
    .param p4, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 308
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1, p1, p4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v1

    .line 322
    :goto_0
    return-object v1

    .line 309
    :catch_0
    move-exception v0

    .line 311
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 322
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 312
    :catch_1
    move-exception v0

    .line 314
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 315
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v0

    .line 317
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1

    .line 318
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_3
    move-exception v0

    .line 320
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1
.end method

.method private startVideoPlay()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 370
    new-instance v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    .line 371
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    if-nez v3, :cond_0

    .line 372
    const-string v2, "Video"

    const-string v3, "VideoView is null."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    :goto_0
    return v1

    .line 375
    :cond_0
    new-instance v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$3;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$3;-><init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoViewListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;

    .line 385
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoViewListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setListener(Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;)V

    .line 387
    new-array v0, v2, [Ljava/lang/Object;

    .line 388
    .local v0, "args":[Ljava/lang/Object;
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v0, v1

    .line 389
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->requestFocusFromTouch()Z

    .line 390
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    const-string v4, "setWFDTcpDisable"

    sget-object v5, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETWFDTCPDISABLE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    invoke-direct {p0, v3, v4, v5, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoFilePath:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setVideoPath(Ljava/lang/String;)V

    .line 392
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 393
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setZOrderOnTop(Z)V

    .line 394
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setBackgroundColor(I)V

    .line 395
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 396
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setStopMusic(Z)V

    .line 397
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioLisner:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v4, 0x3

    invoke-virtual {v1, v3, v4, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 398
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->start()V

    .line 399
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    new-instance v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$4;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$4;-><init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 406
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    new-instance v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;-><init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    move v1, v2

    .line 424
    goto :goto_0
.end method


# virtual methods
.method public getNativeHandle()J
    .locals 2

    .prologue
    .line 550
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 578
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 525
    return-void
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 588
    const/4 v0, 0x1

    return v0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 136
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;

    .line 137
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->fm:Landroid/app/FragmentManager;

    .line 138
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioManager:Landroid/media/AudioManager;

    .line 139
    new-instance v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$1;-><init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioLisner:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 148
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 559
    return-void
.end method

.method public onUnload()V
    .locals 1

    .prologue
    .line 533
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;

    .line 534
    return-void
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

    .prologue
    .line 511
    if-nez p1, :cond_0

    .line 512
    const-string v0, "Video"

    const-string v1, "argument listener is null."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    const/4 v0, 0x0

    .line 516
    :goto_0
    return v0

    .line 515
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

    .line 516
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 542
    return-void
.end method

.method public setRect(Landroid/graphics/RectF;)V
    .locals 4
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v3, 0x0

    .line 292
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mViewGroup:Landroid/view/ViewGroup;

    if-nez v0, :cond_2

    .line 293
    :cond_0
    const-string v0, "Video"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "rect = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " or viewGroup = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " This SOR started yet."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    :cond_1
    :goto_0
    return-void

    .line 296
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    .line 298
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 299
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 301
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    if-eqz v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public start(Ljava/lang/Object;Landroid/graphics/RectF;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V
    .locals 5
    .param p1, "objectBase"    # Ljava/lang/Object;
    .param p2, "rectF"    # Landroid/graphics/RectF;
    .param p3, "arg2"    # Landroid/graphics/PointF;
    .param p4, "arg3"    # F
    .param p5, "arg4"    # Landroid/graphics/PointF;
    .param p6, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x1

    .line 157
    if-eqz p1, :cond_0

    if-eqz p6, :cond_0

    if-eqz p2, :cond_0

    if-nez p5, :cond_1

    .line 158
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Argument is null. ObjectBase = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Rect = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 159
    const-string v3, " ViewGroup = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " startFramePosition = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 158
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 162
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    .line 163
    iput-object p6, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mViewGroup:Landroid/view/ViewGroup;

    .line 164
    iput-object p3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mPan:Landroid/graphics/PointF;

    .line 165
    iput p4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRatio:F

    .line 166
    iput-object p5, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mStartFramePosition:Landroid/graphics/PointF;

    .line 171
    new-array v0, v4, [Ljava/lang/Object;

    .line 172
    .local v0, "args":[Ljava/lang/Object;
    const/4 v1, 0x0

    const-string v2, "VideoPath"

    aput-object v2, v0, v1

    .line 173
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    const-string v2, "getExtraDataString"

    .line 174
    sget-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->GETEXTRADATASTRING_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    .line 173
    invoke-direct {p0, v1, v2, v3, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoFilePath:Ljava/lang/String;

    .line 175
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->setRect(Landroid/graphics/RectF;)V

    .line 176
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoFilePath:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 177
    new-instance v1, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    .line 178
    new-instance v1, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;-><init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragmentListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    .line 264
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragmentListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->setVideoIntentFragmentListener(Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;)V

    .line 266
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->fm:Landroid/app/FragmentManager;

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    const-string v3, "PlayVideo"

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 273
    :cond_2
    :goto_0
    return-void

    .line 268
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->startVideoPlay()Z

    move-result v1

    if-nez v1, :cond_2

    .line 269
    const-string v1, "Video"

    const-string v2, "StartVideoPlay is failed."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V

    goto :goto_0
.end method

.method public stop(Z)V
    .locals 0
    .param p1, "cancel"    # Z

    .prologue
    .line 282
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cleanResource()V

    .line 283
    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 568
    const/4 v0, 0x1

    return v0
.end method
