.class public Lcom/sec/android/collage/MultiGridROINode;
.super Ljava/lang/Object;
.source "MultiGridROINode.java"


# instance fields
.field m_Level:I

.field m_VertexPosList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation
.end field

.field m_szFilePath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/collage/MultiGridROINode;->m_VertexPosList:Ljava/util/ArrayList;

    .line 17
    return-void
.end method


# virtual methods
.method public addVertexPos(Landroid/graphics/Point;)V
    .locals 1
    .param p1, "pt"    # Landroid/graphics/Point;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/collage/MultiGridROINode;->m_VertexPosList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 22
    return-void
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/collage/MultiGridROINode;->m_szFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getLevel()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/collage/MultiGridROINode;->m_Level:I

    return v0
.end method

.method public getVertexCount()I
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/collage/MultiGridROINode;->m_VertexPosList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getVertexPos(I)Landroid/graphics/Point;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/collage/MultiGridROINode;->m_VertexPosList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le p1, v0, :cond_0

    .line 42
    const/4 v0, 0x0

    .line 44
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/collage/MultiGridROINode;->m_VertexPosList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    goto :goto_0
.end method

.method public setFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/collage/MultiGridROINode;->m_szFilePath:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public setLevel(I)V
    .locals 0
    .param p1, "level"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/sec/android/collage/MultiGridROINode;->m_Level:I

    .line 32
    return-void
.end method
