.class public Lcom/sec/android/collage/MultiGridCollage;
.super Ljava/lang/Object;
.source "MultiGridCollage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/collage/MultiGridCollage$MultiGridImageFileFormat;
    }
.end annotation


# static fields
.field public static TAG:Ljava/lang/String;


# instance fields
.field private m_DetailedInfo:Ljava/nio/ByteBuffer;

.field private m_ImageBitmap:Landroid/graphics/Bitmap;

.field private m_ImageROIList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/collage/MultiGridROINode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "MULTIGRID"

    sput-object v0, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    .line 16
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-object v0, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    const-string v1, "jar LIB Version MultiGridCollageLib_WK13Y2013_R01"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    iput-object v2, p0, Lcom/sec/android/collage/MultiGridCollage;->m_ImageBitmap:Landroid/graphics/Bitmap;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/collage/MultiGridCollage;->m_ImageROIList:Ljava/util/ArrayList;

    .line 32
    iput-object v2, p0, Lcom/sec/android/collage/MultiGridCollage;->m_DetailedInfo:Ljava/nio/ByteBuffer;

    .line 33
    return-void
.end method

.method private dumpROI(Ljava/io/RandomAccessFile;)Z
    .locals 17
    .param p1, "fp"    # Ljava/io/RandomAccessFile;

    .prologue
    .line 111
    const-string v9, "MULTIGRID"

    .line 115
    .local v9, "string":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v10

    .line 116
    .local v10, "seekindex":J
    sget-object v14, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    const-string v15, "MULTIGRID:dumpROI entered"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/collage/MultiGridCollage;->getROICount()I

    move-result v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeInt(I)V

    .line 119
    const-string v14, "\n"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    .line 121
    sget-object v14, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "MULTIGRID:getROICount done"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/collage/MultiGridCollage;->getROICount()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/collage/MultiGridCollage;->getROICount()I

    move-result v14

    if-lt v4, v14, :cond_0

    .line 157
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/collage/MultiGridCollage;->getDetailedInfo()Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 158
    .local v2, "detailedInfo":Ljava/nio/ByteBuffer;
    sget-object v14, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "MULTIGRID:detailedInfo capacity:"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeInt(I)V

    .line 160
    const-string v14, "\n"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    .line 161
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->write([B)V

    .line 162
    const-string v14, "\n"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    .line 163
    invoke-virtual/range {p1 .. p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v12

    .line 164
    .local v12, "seekindexnew":J
    sub-long v14, v12, v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Ljava/io/RandomAccessFile;->writeLong(J)V

    .line 165
    const-string v14, "\n"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    .line 166
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    .line 167
    const-string v14, "\n"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    .line 168
    invoke-virtual/range {p1 .. p1}, Ljava/io/RandomAccessFile;->close()V

    .line 174
    .end local v2    # "detailedInfo":Ljava/nio/ByteBuffer;
    .end local v4    # "i":I
    .end local v10    # "seekindex":J
    .end local v12    # "seekindexnew":J
    :goto_1
    const/4 v14, 0x1

    :goto_2
    return v14

    .line 125
    .restart local v4    # "i":I
    .restart local v10    # "seekindex":J
    :cond_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/collage/MultiGridCollage;->getROIInfo(I)Lcom/sec/android/collage/MultiGridROINode;

    move-result-object v5

    .line 126
    .local v5, "imageNode":Lcom/sec/android/collage/MultiGridROINode;
    if-nez v5, :cond_1

    .line 128
    sget-object v14, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    const-string v15, "MULTIGRID:image node is NULL!!!"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    invoke-virtual/range {p1 .. p1}, Ljava/io/RandomAccessFile;->close()V

    .line 130
    const/4 v14, 0x0

    goto :goto_2

    .line 132
    :cond_1
    invoke-virtual {v5}, Lcom/sec/android/collage/MultiGridROINode;->getVertexCount()I

    move-result v7

    .line 133
    .local v7, "nVertices":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/io/RandomAccessFile;->writeInt(I)V

    .line 134
    const-string v14, "\n"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    .line 135
    sget-object v14, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "MULTIGRID:vertices="

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_3
    if-lt v6, v7, :cond_2

    .line 147
    sget-object v14, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    const-string v15, "MULTIGRID: vertices saved"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    invoke-virtual {v5}, Lcom/sec/android/collage/MultiGridROINode;->getFilePath()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeUTF(Ljava/lang/String;)V

    .line 150
    sget-object v14, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "MULTIGRID: file path saved"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/sec/android/collage/MultiGridROINode;->getFilePath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    invoke-virtual {v5}, Lcom/sec/android/collage/MultiGridROINode;->getLevel()I

    move-result v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeInt(I)V

    .line 153
    sget-object v14, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "MULTIGRID: getLevel saved"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/sec/android/collage/MultiGridROINode;->getLevel()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    const-string v14, "\n"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    .line 123
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 138
    :cond_2
    invoke-virtual {v5, v6}, Lcom/sec/android/collage/MultiGridROINode;->getVertexPos(I)Landroid/graphics/Point;

    move-result-object v8

    .line 140
    .local v8, "pt":Landroid/graphics/Point;
    iget v14, v8, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeInt(I)V

    .line 141
    const-string v14, "\n"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    .line 143
    iget v14, v8, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeInt(I)V

    .line 144
    const-string v14, "\n"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 170
    .end local v4    # "i":I
    .end local v5    # "imageNode":Lcom/sec/android/collage/MultiGridROINode;
    .end local v6    # "j":I
    .end local v7    # "nVertices":I
    .end local v8    # "pt":Landroid/graphics/Point;
    .end local v10    # "seekindex":J
    :catch_0
    move-exception v3

    .line 172
    .local v3, "e":Ljava/io/IOException;
    sget-object v14, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "MULTIGRID: My Exception save: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private encodeMultiGrid(Ljava/lang/String;Lcom/sec/android/collage/MultiGridCollage$MultiGridImageFileFormat;)Z
    .locals 7
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "fileFormat"    # Lcom/sec/android/collage/MultiGridCollage$MultiGridImageFileFormat;

    .prologue
    const/4 v3, 0x0

    .line 81
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 83
    .local v2, "fp":Ljava/io/FileOutputStream;
    sget-object v4, Lcom/sec/android/collage/MultiGridCollage$MultiGridImageFileFormat;->JPEG:Lcom/sec/android/collage/MultiGridCollage$MultiGridImageFileFormat;

    if-ne v4, p2, :cond_0

    .line 85
    iget-object v4, p0, Lcom/sec/android/collage/MultiGridCollage;->m_ImageBitmap:Landroid/graphics/Bitmap;

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x50

    invoke-virtual {v4, v5, v6, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 87
    const-string v0, "MULTIGRID:Encoding to JPG file is failed"

    .line 88
    .local v0, "Error":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    invoke-static {v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 107
    .end local v0    # "Error":Ljava/lang/String;
    .end local v2    # "fp":Ljava/io/FileOutputStream;
    :goto_0
    return v3

    .line 93
    .restart local v2    # "fp":Ljava/io/FileOutputStream;
    :cond_0
    sget-object v4, Lcom/sec/android/collage/MultiGridCollage$MultiGridImageFileFormat;->PNG:Lcom/sec/android/collage/MultiGridCollage$MultiGridImageFileFormat;

    if-ne v4, p2, :cond_1

    .line 95
    iget-object v4, p0, Lcom/sec/android/collage/MultiGridCollage;->m_ImageBitmap:Landroid/graphics/Bitmap;

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x50

    invoke-virtual {v4, v5, v6, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 97
    const-string v0, "MULTIGRID:Encoding to PNG file is failed"

    .line 98
    .restart local v0    # "Error":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    invoke-static {v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 104
    .end local v0    # "Error":Ljava/lang/String;
    .end local v2    # "fp":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 105
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 107
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    .line 103
    .restart local v2    # "fp":Ljava/io/FileOutputStream;
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private loadMultiGridData(Ljava/lang/String;J)Z
    .locals 20
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "seekpos"    # J

    .prologue
    .line 277
    :try_start_0
    new-instance v6, Ljava/io/RandomAccessFile;

    const-string v16, "r"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v6, v0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    .local v6, "fr":Ljava/io/RandomAccessFile;
    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 279
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v11

    .line 280
    .local v11, "nROI":I
    sget-object v16, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "MULTIGRID: number of ROIs"

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readByte()B

    .line 282
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-lt v7, v11, :cond_0

    .line 314
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v2

    .line 315
    .local v2, "bytebufSize":I
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readByte()B

    .line 316
    sget-object v16, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "MULTIGRID: Detailed Infobuf size "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 318
    .local v3, "detailedInfo":Ljava/nio/ByteBuffer;
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/io/RandomAccessFile;->readFully([B)V

    .line 319
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/collage/MultiGridCollage;->setDetailedInfo(Ljava/nio/ByteBuffer;)V

    .line 320
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V

    .line 326
    const/16 v16, 0x1

    .end local v2    # "bytebufSize":I
    .end local v3    # "detailedInfo":Ljava/nio/ByteBuffer;
    .end local v6    # "fr":Ljava/io/RandomAccessFile;
    .end local v7    # "i":I
    .end local v11    # "nROI":I
    :goto_1
    return v16

    .line 284
    .restart local v6    # "fr":Ljava/io/RandomAccessFile;
    .restart local v7    # "i":I
    .restart local v11    # "nROI":I
    :cond_0
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v12

    .line 285
    .local v12, "nVertices":I
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readByte()B

    .line 286
    sget-object v16, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "MULTIGRID: ROI:"

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " Vertices:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    const/4 v14, 0x0

    .local v14, "pt_x":I
    const/4 v15, 0x0

    .line 288
    .local v15, "pt_y":I
    new-instance v8, Lcom/sec/android/collage/MultiGridROINode;

    invoke-direct {v8}, Lcom/sec/android/collage/MultiGridROINode;-><init>()V

    .line 290
    .local v8, "imageNode":Lcom/sec/android/collage/MultiGridROINode;
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_2
    if-lt v9, v12, :cond_1

    .line 304
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readUTF()Ljava/lang/String;

    move-result-object v5

    .line 305
    .local v5, "filePath":Ljava/lang/String;
    sget-object v16, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "MULTIGRID: FilePath "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    invoke-virtual {v8, v5}, Lcom/sec/android/collage/MultiGridROINode;->setFilePath(Ljava/lang/String;)V

    .line 308
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v10

    .line 309
    .local v10, "level":I
    iput v10, v8, Lcom/sec/android/collage/MultiGridROINode;->m_Level:I

    .line 310
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readByte()B

    .line 312
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/collage/MultiGridCollage;->addROInfo(Lcom/sec/android/collage/MultiGridROINode;)V

    .line 282
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 293
    .end local v5    # "filePath":Ljava/lang/String;
    .end local v10    # "level":I
    :cond_1
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v14

    .line 294
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readByte()B

    .line 297
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v15

    .line 298
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->readByte()B

    .line 299
    sget-object v16, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "MULTIGRID: Vertex pt_x"

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " pt_y:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    new-instance v13, Landroid/graphics/Point;

    invoke-direct {v13, v14, v15}, Landroid/graphics/Point;-><init>(II)V

    .line 301
    .local v13, "pt":Landroid/graphics/Point;
    invoke-virtual {v8, v13}, Lcom/sec/android/collage/MultiGridROINode;->addVertexPos(Landroid/graphics/Point;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 322
    .end local v6    # "fr":Ljava/io/RandomAccessFile;
    .end local v7    # "i":I
    .end local v8    # "imageNode":Lcom/sec/android/collage/MultiGridROINode;
    .end local v9    # "j":I
    .end local v11    # "nROI":I
    .end local v12    # "nVertices":I
    .end local v13    # "pt":Landroid/graphics/Point;
    .end local v14    # "pt_x":I
    .end local v15    # "pt_y":I
    :catch_0
    move-exception v4

    .line 324
    .local v4, "e":Ljava/io/IOException;
    const/16 v16, 0x0

    goto/16 :goto_1
.end method


# virtual methods
.method public addROInfo(Lcom/sec/android/collage/MultiGridROINode;)V
    .locals 2
    .param p1, "imageNode"    # Lcom/sec/android/collage/MultiGridROINode;

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/android/collage/MultiGridCollage;->m_ImageROIList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 342
    sget-object v0, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    const-string v1, "MULTIGRID:m_ImageROIList is null!! "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :goto_0
    return-void

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/sec/android/collage/MultiGridCollage;->m_ImageROIList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getCollageBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "collageBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 335
    iget-object v0, p0, Lcom/sec/android/collage/MultiGridCollage;->m_ImageBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getDetailedInfo()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/collage/MultiGridCollage;->m_DetailedInfo:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public getROICount()I
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/android/collage/MultiGridCollage;->m_ImageROIList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 357
    sget-object v0, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    const-string v1, "MULTIGRID:getROICount m_ImageROIList is null!! "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    const/4 v0, 0x0

    .line 360
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/collage/MultiGridCollage;->m_ImageROIList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getROIInfo(I)Lcom/sec/android/collage/MultiGridROINode;
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 365
    iget-object v1, p0, Lcom/sec/android/collage/MultiGridCollage;->m_ImageROIList:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 372
    :cond_0
    :goto_0
    return-object v0

    .line 369
    :cond_1
    iget-object v1, p0, Lcom/sec/android/collage/MultiGridCollage;->m_ImageROIList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt p1, v1, :cond_0

    .line 372
    iget-object v0, p0, Lcom/sec/android/collage/MultiGridCollage;->m_ImageROIList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/collage/MultiGridROINode;

    goto :goto_0
.end method

.method public load(Ljava/lang/String;)Z
    .locals 30
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 181
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/collage/MultiGridCollage;->m_ImageROIList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->clear()V

    .line 182
    new-instance v8, Ljava/io/RandomAccessFile;

    const-string v25, "r"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-direct {v8, v0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    .local v8, "fr":Ljava/io/RandomAccessFile;
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v6

    .line 184
    .local v6, "filelength":J
    sget-object v25, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "MULTIGRID:File Length:"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const-wide/16 v26, 0x13

    sub-long v26, v6, v26

    move-wide/from16 v0, v26

    invoke-virtual {v8, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 186
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readLong()J

    move-result-wide v12

    .line 187
    .local v12, "seekpos":J
    sget-object v25, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "MULTIGRID:Read Seek Pos:"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readByte()B

    .line 189
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;

    move-result-object v18

    .line 190
    .local v18, "string":Ljava/lang/String;
    sget-object v25, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "MULTIGRID:String read is:"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    const-string v25, "MULTIGRID"

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 193
    sget-object v25, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    const-string v26, "MULTIGRID marker found!!"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V

    .line 195
    const/4 v10, 0x1

    .line 196
    .local v10, "isDataLoaded":Z
    sub-long v26, v6, v12

    const-wide/16 v28, 0x13

    sub-long v26, v26, v28

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/collage/MultiGridCollage;->loadMultiGridData(Ljava/lang/String;J)Z

    move-result v10

    .line 269
    .end local v6    # "filelength":J
    .end local v8    # "fr":Ljava/io/RandomAccessFile;
    .end local v10    # "isDataLoaded":Z
    .end local v12    # "seekpos":J
    .end local v18    # "string":Ljava/lang/String;
    :goto_0
    return v10

    .line 200
    .restart local v6    # "filelength":J
    .restart local v8    # "fr":Ljava/io/RandomAccessFile;
    .restart local v12    # "seekpos":J
    .restart local v18    # "string":Ljava/lang/String;
    :cond_0
    const-string v25, "Samsung"

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 202
    sget-object v25, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "MULTIGRID: Photonote marker found!!"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V

    .line 205
    new-instance v8, Ljava/io/RandomAccessFile;

    .end local v8    # "fr":Ljava/io/RandomAccessFile;
    const-string v25, "r"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-direct {v8, v0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    .restart local v8    # "fr":Ljava/io/RandomAccessFile;
    const-wide/16 v16, 0x0

    .line 207
    .local v16, "seekposphotonote":J
    const-wide/16 v26, 0x10

    sub-long v26, v6, v26

    move-wide/from16 v0, v26

    invoke-virtual {v8, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 213
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v25

    move/from16 v0, v25

    int-to-char v0, v0

    move/from16 v24, v0

    .line 214
    .local v24, "unitPlace":C
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v25

    move/from16 v0, v25

    int-to-char v0, v0

    move/from16 v23, v0

    .line 215
    .local v23, "tensPlace":C
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v25

    move/from16 v0, v25

    int-to-char v9, v0

    .line 216
    .local v9, "hundPlace":C
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v25

    move/from16 v0, v25

    int-to-char v0, v0

    move/from16 v22, v0

    .line 217
    .local v22, "tenThousPlace":C
    sget-object v25, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "MULTIGRID: Place "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    const-wide/16 v20, 0x0

    .line 220
    .local v20, "temp":J
    and-int/lit8 v25, v24, 0xf

    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 221
    shr-int/lit8 v25, v24, 0x4

    mul-int/lit8 v25, v25, 0x10

    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v26, v0

    add-long v26, v26, v20

    const-wide/16 v28, 0xff

    and-long v20, v26, v28

    .line 222
    and-int/lit8 v25, v23, 0xf

    move/from16 v0, v25

    mul-int/lit16 v0, v0, 0x100

    move/from16 v25, v0

    shr-int/lit8 v26, v23, 0x4

    move/from16 v0, v26

    mul-int/lit16 v0, v0, 0x1000

    move/from16 v26, v0

    add-int v25, v25, v26

    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v26, v0

    add-long v26, v26, v20

    const-wide/32 v28, 0xffff

    and-long v20, v26, v28

    .line 223
    and-int/lit8 v25, v9, 0xf

    const/high16 v26, 0x10000

    mul-int v25, v25, v26

    shr-int/lit8 v26, v9, 0x4

    const/high16 v27, 0x100000

    mul-int v26, v26, v27

    add-int v25, v25, v26

    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v26, v0

    add-long v26, v26, v20

    const-wide/32 v28, 0xffffff

    and-long v20, v26, v28

    .line 225
    and-int/lit8 v25, v22, 0xf

    const/high16 v26, 0x1000000

    mul-int v25, v25, v26

    shr-int/lit8 v26, v22, 0x4

    const/high16 v27, 0x10000000

    mul-int v26, v26, v27

    add-int v25, v25, v26

    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v26, v0

    add-long v26, v26, v20

    const-wide/16 v28, -0x1

    and-long v16, v26, v28

    .line 227
    sget-object v25, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "MULTIGRID: Started"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const/4 v11, 0x0

    .line 231
    .local v11, "markerfound":Z
    sub-long v26, v6, v16

    const-wide/16 v28, 0x1e

    sub-long v26, v26, v28

    move-wide/from16 v0, v26

    invoke-virtual {v8, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 235
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;

    move-result-object v19

    .line 236
    .local v19, "stringvar":Ljava/lang/String;
    const-string v25, "MULTIGRID"

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1

    .line 238
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 239
    .local v5, "filePointer":Ljava/lang/Long;
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    const-wide/16 v28, 0x13

    sub-long v26, v26, v28

    move-wide/from16 v0, v26

    invoke-virtual {v8, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 240
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->readLong()J

    move-result-wide v14

    .line 241
    .local v14, "seekposition":J
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v26

    sub-long v26, v26, v14

    const-wide/16 v28, 0x8

    sub-long v16, v26, v28

    .line 242
    const/4 v11, 0x1

    .line 243
    sget-object v25, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "MULTIGRID:Read Seek Pos:"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    .end local v5    # "filePointer":Ljava/lang/Long;
    .end local v14    # "seekposition":J
    :cond_1
    if-nez v11, :cond_2

    .line 248
    sget-object v25, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    const-string v26, "MULTIGRID: marker NOT found!!"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V

    .line 250
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 252
    :cond_2
    sget-object v25, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    const-string v26, "MULTIGRID marker found!!"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V

    .line 254
    const/4 v10, 0x1

    .line 255
    .restart local v10    # "isDataLoaded":Z
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v16

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/collage/MultiGridCollage;->loadMultiGridData(Ljava/lang/String;J)Z

    move-result v10

    .line 256
    goto/16 :goto_0

    .line 260
    .end local v9    # "hundPlace":C
    .end local v10    # "isDataLoaded":Z
    .end local v11    # "markerfound":Z
    .end local v16    # "seekposphotonote":J
    .end local v19    # "stringvar":Ljava/lang/String;
    .end local v20    # "temp":J
    .end local v22    # "tenThousPlace":C
    .end local v23    # "tensPlace":C
    .end local v24    # "unitPlace":C
    :cond_3
    sget-object v25, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    const-string v26, "MULTIGRID marker not found..returning!!"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 265
    .end local v6    # "filelength":J
    .end local v8    # "fr":Ljava/io/RandomAccessFile;
    .end local v12    # "seekpos":J
    .end local v18    # "string":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 267
    .local v4, "e":Ljava/io/IOException;
    sget-object v25, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "MULTIGRID:My Exception load: "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    const/4 v10, 0x0

    goto/16 :goto_0
.end method

.method public save(Ljava/lang/String;Lcom/sec/android/collage/MultiGridCollage$MultiGridImageFileFormat;)Z
    .locals 11
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "fileFormat"    # Lcom/sec/android/collage/MultiGridCollage$MultiGridImageFileFormat;

    .prologue
    const/4 v5, 0x0

    .line 38
    const/4 v4, 0x1

    .line 42
    .local v4, "isEncoded":Z
    invoke-direct {p0, p1, p2}, Lcom/sec/android/collage/MultiGridCollage;->encodeMultiGrid(Ljava/lang/String;Lcom/sec/android/collage/MultiGridCollage$MultiGridImageFileFormat;)Z

    move-result v4

    .line 44
    if-eqz v4, :cond_1

    .line 47
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 48
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 49
    .local v6, "nbytes":J
    sget-object v8, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    const-string v9, "MULTIGRID:entering dumpROI"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v8, "rw"

    invoke-direct {v3, p1, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .local v3, "fw":Ljava/io/RandomAccessFile;
    invoke-virtual {v3, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 55
    invoke-direct {p0, v3}, Lcom/sec/android/collage/MultiGridCollage;->dumpROI(Ljava/io/RandomAccessFile;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 57
    sget-object v8, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    const-string v9, "MULTIGRID:DumpROI Failed!!"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V

    .line 76
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fw":Ljava/io/RandomAccessFile;
    .end local v6    # "nbytes":J
    :goto_0
    return v5

    .line 61
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "fw":Ljava/io/RandomAccessFile;
    .restart local v6    # "nbytes":J
    :cond_0
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    const/4 v5, 0x1

    goto :goto_0

    .line 63
    .end local v3    # "fw":Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v1

    .line 65
    .local v1, "e":Ljava/io/IOException;
    sget-object v8, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "MULTIGRID: Exception"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 71
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "file":Ljava/io/File;
    .end local v6    # "nbytes":J
    :cond_1
    const-string v0, "MULTIGRID:Encoding to JPG file is failed"

    .line 72
    .local v0, "Error":Ljava/lang/String;
    sget-object v8, Lcom/sec/android/collage/MultiGridCollage;->TAG:Ljava/lang/String;

    invoke-static {v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setCollageBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "collageBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 330
    iput-object p1, p0, Lcom/sec/android/collage/MultiGridCollage;->m_ImageBitmap:Landroid/graphics/Bitmap;

    .line 331
    return-void
.end method

.method public setDetailedInfo(Ljava/nio/ByteBuffer;)V
    .locals 0
    .param p1, "detailedInfo"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 350
    iput-object p1, p0, Lcom/sec/android/collage/MultiGridCollage;->m_DetailedInfo:Ljava/nio/ByteBuffer;

    .line 351
    return-void
.end method
