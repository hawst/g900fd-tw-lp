.class public Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;
.super Landroid/service/wallpaper/WallpaperService;
.source "SAGLWallpaperService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;,
        Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLRenderer;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "GLWallpaperService"

.field public static mEngine:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/service/wallpaper/WallpaperService;-><init>()V

    return-void
.end method

.method public static requestRenderView()V
    .locals 1

    .prologue
    .line 225
    sget-object v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;->mEngine:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;

    if-eqz v0, :cond_0

    .line 226
    sget-object v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;->mEngine:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->requestRenderView()V

    .line 227
    :cond_0
    invoke-static {}, Lcom/sec/android/samsunganimation/slide/SASlideManager;->getInstance()Lcom/sec/android/samsunganimation/slide/SASlideManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/slide/SASlideManager;->animaitonRenderNotify()V

    .line 228
    return-void
.end method


# virtual methods
.method public onCreateEngine()Landroid/service/wallpaper/WallpaperService$Engine;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;

    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;-><init>(Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;->mEngine:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;

    .line 35
    sget-object v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;->mEngine:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;

    return-object v0
.end method

.method public requestRender()V
    .locals 1

    .prologue
    .line 220
    sget-object v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;->mEngine:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;

    if-eqz v0, :cond_0

    .line 221
    sget-object v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;->mEngine:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->requestRender()V

    .line 222
    :cond_0
    return-void
.end method

.method public setLogFPS(Z)V
    .locals 1
    .param p1, "isLog"    # Z

    .prologue
    .line 211
    sget-object v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;->mEngine:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;->mEngine:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;

    invoke-virtual {v0, p1}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->setLogFPS(Z)V

    .line 212
    :cond_0
    return-void
.end method

.method public setShowFPS(Z)V
    .locals 1
    .param p1, "isShow"    # Z

    .prologue
    .line 207
    sget-object v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;->mEngine:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;->mEngine:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;

    invoke-virtual {v0, p1}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->setShowFPS(Z)V

    .line 208
    :cond_0
    return-void
.end method

.method public swapAnimationSAGLSurface()V
    .locals 1

    .prologue
    .line 215
    sget-object v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;->mEngine:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;

    if-eqz v0, :cond_0

    .line 216
    sget-object v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;->mEngine:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->swapAnimationSAGLSurface()V

    .line 217
    :cond_0
    return-void
.end method
