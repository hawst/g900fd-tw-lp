.class public Lcom/sec/android/samsunganimation/particle/SAParticleEffect;
.super Ljava/lang/Object;
.source "SAParticleEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/samsunganimation/particle/SAParticleEffect$ParticleEffectType;
    }
.end annotation


# instance fields
.field protected mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

.field protected mColorKeyFrameDuration:I

.field protected mColorKeyFrameInterpolaterType:I

.field protected mDuration:I

.field protected mNativeHandle:I

.field protected mPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

.field protected mPositionKeyFrameDuration:I

.field protected mPositionKeyFrameInterpolaterType:I

.field protected mScale:Lcom/sec/android/samsunganimation/basetype/SAVector3;

.field protected mScaleKeyFrameDuration:I

.field protected mScaleKeyFrameInterpolaterType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 204
    sget-object v0, Lcom/sec/android/samsunganimation/SamsungAnimationDesc;->mName:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 11
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->initialize()V

    .line 25
    invoke-static {}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->nativeCreateParticleEffect()I

    move-result v0

    iput v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    .line 26
    return-void
.end method

.method protected static native nativeAddColorKeyFrame(IFFFFF)V
.end method

.method protected static native nativeAddParticles(II)V
.end method

.method protected static native nativeAddPositionKeyFrame(IFFFF)V
.end method

.method protected static native nativeAddScaleKeyFrame(IFFFF)V
.end method

.method private static native nativeCreateParticleEffect()I
.end method

.method protected static native nativeSetColor(IFFFF)V
.end method

.method protected static native nativeSetColorKeyFrameAnimationProperty(IIII)V
.end method

.method protected static native nativeSetDuration(II)V
.end method

.method protected static native nativeSetPosition(IFFF)V
.end method

.method protected static native nativeSetPositionKeyFrameAnimationProperty(IIII)V
.end method

.method protected static native nativeSetScale(IFFF)V
.end method

.method protected static native nativeSetScaleKeyFrameAnimationProperty(IIII)V
.end method


# virtual methods
.method public addColorKeyFrame(FLcom/sec/android/samsunganimation/basetype/SAColor;)V
    .locals 6
    .param p1, "keyTime"    # F
    .param p2, "color"    # Lcom/sec/android/samsunganimation/basetype/SAColor;

    .prologue
    .line 159
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    iget v3, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    iget v4, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    iget v5, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    move v1, p1

    invoke-static/range {v0 .. v5}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->nativeAddColorKeyFrame(IFFFFF)V

    .line 160
    return-void
.end method

.method public addParticles(Lcom/sec/android/samsunganimation/particle/SAParticles;)V
    .locals 2
    .param p1, "particles"    # Lcom/sec/android/samsunganimation/particle/SAParticles;

    .prologue
    .line 170
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    invoke-virtual {p1}, Lcom/sec/android/samsunganimation/particle/SAParticles;->getNativeHandle()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->nativeAddParticles(II)V

    .line 171
    return-void
.end method

.method public addPositionKeyFrame(FLcom/sec/android/samsunganimation/basetype/SAVector3;)V
    .locals 4
    .param p1, "keyTime"    # F
    .param p2, "position"    # Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .prologue
    .line 164
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    iget v1, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iget v3, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    invoke-static {v0, p1, v1, v2, v3}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->nativeAddPositionKeyFrame(IFFFF)V

    .line 165
    return-void
.end method

.method public addScaleKeyFrame(FLcom/sec/android/samsunganimation/basetype/SAVector3;)V
    .locals 4
    .param p1, "keyTime"    # F
    .param p2, "scale"    # Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    iget v1, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iget v3, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    invoke-static {v0, p1, v1, v2, v3}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->nativeAddScaleKeyFrame(IFFFF)V

    .line 155
    return-void
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 42
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    if-eq v0, v1, :cond_0

    .line 44
    iput v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    .line 46
    :cond_0
    return-void
.end method

.method public getNativeHandle()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    return v0
.end method

.method protected initialize()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    .line 31
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SAVector3;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .line 32
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SAVector3;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScale:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .line 33
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAColor;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SAColor;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    .line 35
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    const/4 v3, 0x0

    iput v3, v2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v3, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v3, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 36
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScale:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScale:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScale:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput v4, v2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v4, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v4, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 37
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v3, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput v4, v3, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    iput v4, v2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    iput v4, v1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    iput v4, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    .line 38
    return-void
.end method

.method public setColor(FFFF)V
    .locals 1
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "a"    # F

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput p1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    .line 110
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput p2, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    .line 111
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput p3, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    .line 112
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput p4, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    .line 114
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    invoke-static {v0, p1, p2, p3, p4}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->nativeSetColor(IFFFF)V

    .line 115
    return-void
.end method

.method public setColor(Lcom/sec/android/samsunganimation/basetype/SAColor;)V
    .locals 4
    .param p1, "color"    # Lcom/sec/android/samsunganimation/basetype/SAColor;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v1, p1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    .line 100
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v1, p1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    .line 101
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v1, p1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    .line 102
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v1, p1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    .line 104
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v0, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    iget-object v3, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v3, v3, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->setColor(FFFF)V

    .line 105
    return-void
.end method

.method public setColorKeyFrameAnimationProperty(IIZ)V
    .locals 4
    .param p1, "duration"    # I
    .param p2, "interpolatorType"    # I
    .param p3, "keyValueReset"    # Z

    .prologue
    .line 130
    iput p1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColorKeyFrameDuration:I

    .line 131
    iput p2, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColorKeyFrameInterpolaterType:I

    .line 133
    const/4 v0, 0x0

    .line 134
    .local v0, "reset":I
    if-eqz p3, :cond_0

    .line 135
    const/4 v0, 0x1

    .line 137
    :cond_0
    iget v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    iget v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColorKeyFrameDuration:I

    iget v3, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mColorKeyFrameInterpolaterType:I

    invoke-static {v1, v2, v3, v0}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->nativeSetColorKeyFrameAnimationProperty(IIII)V

    .line 138
    return-void
.end method

.method public setDuration(I)V
    .locals 2
    .param p1, "duration"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mDuration:I

    .line 57
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    iget v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mDuration:I

    invoke-static {v0, v1}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->nativeSetDuration(II)V

    .line 58
    return-void
.end method

.method public setPosition(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 72
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p2, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 73
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p3, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 75
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    invoke-static {v0, p1, p2, p3}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->nativeSetPosition(IFFF)V

    .line 76
    return-void
.end method

.method public setPosition(Lcom/sec/android/samsunganimation/basetype/SAVector3;)V
    .locals 3
    .param p1, "position"    # Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, p1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 63
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, p1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 64
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, p1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 66
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v0, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->setPosition(FFF)V

    .line 67
    return-void
.end method

.method public setPositionKeyFrameAnimationProperty(IIZ)V
    .locals 4
    .param p1, "duration"    # I
    .param p2, "interpolatorType"    # I
    .param p3, "keyValueReset"    # Z

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPositionKeyFrameDuration:I

    .line 143
    iput p2, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPositionKeyFrameInterpolaterType:I

    .line 145
    const/4 v0, 0x0

    .line 146
    .local v0, "reset":I
    if-eqz p3, :cond_0

    .line 147
    const/4 v0, 0x1

    .line 149
    :cond_0
    iget v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    iget v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPositionKeyFrameDuration:I

    iget v3, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mPositionKeyFrameInterpolaterType:I

    invoke-static {v1, v2, v3, v0}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->nativeSetPositionKeyFrameAnimationProperty(IIII)V

    .line 150
    return-void
.end method

.method public setScale(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScale:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 90
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScale:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p2, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 91
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScale:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p3, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 93
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    invoke-static {v0, p1, p2, p3}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->nativeSetScale(IFFF)V

    .line 94
    return-void
.end method

.method public setScale(Lcom/sec/android/samsunganimation/basetype/SAVector3;)V
    .locals 3
    .param p1, "scale"    # Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScale:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, p1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 81
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScale:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, p1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 82
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScale:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, p1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 84
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScale:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v0, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScale:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScale:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->setScale(FFF)V

    .line 85
    return-void
.end method

.method public setScaleKeyFrameAnimationProperty(IIZ)V
    .locals 4
    .param p1, "duration"    # I
    .param p2, "interpolatorType"    # I
    .param p3, "keyValueReset"    # Z

    .prologue
    .line 119
    iput p1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScaleKeyFrameDuration:I

    .line 120
    iput p2, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScaleKeyFrameInterpolaterType:I

    .line 122
    const/4 v0, 0x0

    .line 123
    .local v0, "reset":I
    if-eqz p3, :cond_0

    .line 124
    const/4 v0, 0x1

    .line 126
    :cond_0
    iget v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mNativeHandle:I

    iget v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScaleKeyFrameDuration:I

    iget v3, p0, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->mScaleKeyFrameInterpolaterType:I

    invoke-static {v1, v2, v3, v0}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;->nativeSetScaleKeyFrameAnimationProperty(IIII)V

    .line 127
    return-void
.end method
