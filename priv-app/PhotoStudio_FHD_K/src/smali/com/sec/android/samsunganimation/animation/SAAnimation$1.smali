.class Lcom/sec/android/samsunganimation/animation/SAAnimation$1;
.super Landroid/os/Handler;
.source "SAAnimation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/samsunganimation/animation/SAAnimation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 349
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 356
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 357
    .local v1, "listenerID":I
    sget-object v3, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mListenerMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;

    .line 358
    .local v0, "listener":Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;
    sget-object v3, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mTagMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 360
    .local v2, "tagString":Ljava/lang/String;
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 377
    :cond_0
    :goto_0
    return-void

    .line 362
    :pswitch_0
    if-eqz v0, :cond_1

    .line 363
    invoke-interface {v0, v2}, Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;->onAnimationEnd(Ljava/lang/String;)V

    .line 365
    :cond_1
    sget-object v3, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mListenerMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    sget-object v3, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mTagMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 369
    :pswitch_1
    if-eqz v0, :cond_0

    .line 370
    invoke-interface {v0, v2}, Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;->onAnimationRepeat(Ljava/lang/String;)V

    goto :goto_0

    .line 373
    :pswitch_2
    if-eqz v0, :cond_0

    .line 374
    invoke-interface {v0, v2}, Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;->onAnimationStart(Ljava/lang/String;)V

    goto :goto_0

    .line 360
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
