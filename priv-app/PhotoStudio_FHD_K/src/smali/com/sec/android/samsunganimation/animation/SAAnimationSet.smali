.class public Lcom/sec/android/samsunganimation/animation/SAAnimationSet;
.super Lcom/sec/android/samsunganimation/animation/SAAnimation;
.source "SAAnimationSet.java"


# instance fields
.field private mAnimationList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/samsunganimation/animation/SAAnimation;",
            ">;"
        }
    .end annotation
.end field

.field private mShareAnimationInfo:Z

.field private mShareInterpolator:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 226
    sget-object v0, Lcom/sec/android/samsunganimation/SamsungAnimationDesc;->mName:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAAnimation;-><init>()V

    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mShareInterpolator:Z

    .line 219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mShareAnimationInfo:Z

    .line 35
    const-string v0, "default"

    invoke-virtual {p0, v0}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->initializeSAAnimationSet(Ljava/lang/String;)V

    .line 36
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mNativeAnimation:I

    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mDuration:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mRepeatCount:I

    iget-boolean v3, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAutoReverse:Z

    iget v4, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mOffset:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->nativeSetPropertySAAnimationSet(IIIZI)V

    .line 37
    return-void
.end method

.method public constructor <init>(IIZI)V
    .locals 5
    .param p1, "duration"    # I
    .param p2, "repeatCount"    # I
    .param p3, "autoReverse"    # Z
    .param p4, "offset"    # I

    .prologue
    const/4 v1, -0x1

    .line 57
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAAnimation;-><init>()V

    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mShareInterpolator:Z

    .line 219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mShareAnimationInfo:Z

    .line 58
    const-string v0, "default"

    invoke-virtual {p0, v0}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->initializeSAAnimationSet(Ljava/lang/String;)V

    .line 60
    iput p1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mDuration:I

    .line 61
    if-ne p2, v1, :cond_0

    .line 62
    iput v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mRepeatCount:I

    .line 67
    :goto_0
    iput-boolean p3, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAutoReverse:Z

    .line 68
    iput p4, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mOffset:I

    .line 70
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mNativeAnimation:I

    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mDuration:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mRepeatCount:I

    iget-boolean v3, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAutoReverse:Z

    iget v4, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mOffset:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->nativeSetPropertySAAnimationSet(IIIZI)V

    .line 71
    return-void

    .line 65
    :cond_0
    add-int/lit8 v0, p2, 0x1

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mRepeatCount:I

    goto :goto_0
.end method

.method public constructor <init>(IIZILjava/lang/String;)V
    .locals 5
    .param p1, "duration"    # I
    .param p2, "repeatCount"    # I
    .param p3, "autoReverse"    # Z
    .param p4, "offset"    # I
    .param p5, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 82
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAAnimation;-><init>()V

    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mShareInterpolator:Z

    .line 219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mShareAnimationInfo:Z

    .line 83
    invoke-virtual {p0, p5}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->initializeSAAnimationSet(Ljava/lang/String;)V

    .line 85
    iput p1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mDuration:I

    .line 86
    if-ne p2, v1, :cond_0

    .line 87
    iput v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mRepeatCount:I

    .line 92
    :goto_0
    iput-boolean p3, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAutoReverse:Z

    .line 93
    iput p4, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mOffset:I

    .line 95
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mNativeAnimation:I

    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mDuration:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mRepeatCount:I

    iget-boolean v3, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAutoReverse:Z

    iget v4, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mOffset:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->nativeSetPropertySAAnimationSet(IIIZI)V

    .line 96
    return-void

    .line 90
    :cond_0
    add-int/lit8 v0, p2, 0x1

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mRepeatCount:I

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 5
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAAnimation;-><init>()V

    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mShareInterpolator:Z

    .line 219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mShareAnimationInfo:Z

    .line 45
    invoke-virtual {p0, p1}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->initializeSAAnimationSet(Ljava/lang/String;)V

    .line 46
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mNativeAnimation:I

    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mDuration:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mRepeatCount:I

    iget-boolean v3, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAutoReverse:Z

    iget v4, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mOffset:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->nativeSetPropertySAAnimationSet(IIIZI)V

    .line 47
    return-void
.end method

.method private static native nativeAddAnimationSAAnimationSet(II)Z
.end method

.method private static native nativeCreateSAAnimationSet()I
.end method

.method private static native nativeRemoveAnimationSAAnimationSet(II)V
.end method

.method private static native nativeSetPropertySAAnimationSet(IIIZI)V
.end method

.method private static native nativeShareAnimationInfo(II)V
.end method


# virtual methods
.method public addAnimation(Lcom/sec/android/samsunganimation/animation/SAAnimation;)Z
    .locals 2
    .param p1, "animation"    # Lcom/sec/android/samsunganimation/animation/SAAnimation;

    .prologue
    .line 125
    invoke-virtual {p1}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->getLightType()I

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p1}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->getLightType()I

    move-result v0

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mLightType:I

    .line 127
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->getScaleType()I

    move-result v0

    if-nez v0, :cond_1

    .line 128
    invoke-virtual {p1}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->getScaleType()I

    move-result v0

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mScaleType:I

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAnimationList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mNativeAnimation:I

    iget v1, p1, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    invoke-static {v0, v1}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->nativeAddAnimationSAAnimationSet(II)Z

    move-result v0

    return v0
.end method

.method public clearAnimation()V
    .locals 4

    .prologue
    .line 156
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAnimationList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 160
    iget-object v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAnimationList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 161
    return-void

    .line 157
    :cond_0
    iget-object v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAnimationList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;

    .line 158
    .local v0, "animation":Lcom/sec/android/samsunganimation/animation/SAAnimation;
    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mNativeAnimation:I

    iget v3, v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    invoke-static {v2, v3}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->nativeRemoveAnimationSAAnimationSet(II)V

    .line 156
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->deleteNativeAnimationHandle()V

    .line 116
    return-void
.end method

.method public getShareInterpolator()Z
    .locals 1

    .prologue
    .line 187
    iget-boolean v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mShareInterpolator:Z

    return v0
.end method

.method public initializeSAAnimationSet(Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAnimationList:Ljava/util/ArrayList;

    .line 106
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mClassType:I

    .line 107
    invoke-static {}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->nativeCreateSAAnimationSet()I

    move-result v0

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mNativeAnimation:I

    .line 108
    return-void
.end method

.method public isShareAnimationInfo()Z
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mShareAnimationInfo:Z

    return v0
.end method

.method public removeAnimation(Lcom/sec/android/samsunganimation/animation/SAAnimation;)V
    .locals 2
    .param p1, "animation"    # Lcom/sec/android/samsunganimation/animation/SAAnimation;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAnimationList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    :goto_0
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAnimationList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 144
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mNativeAnimation:I

    iget v1, p1, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    invoke-static {v0, v1}, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->nativeRemoveAnimationSAAnimationSet(II)V

    goto :goto_0
.end method

.method public setAnimationSetInterpolator(I)V
    .locals 3
    .param p1, "interpolatorType"    # I

    .prologue
    .line 170
    iput p1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mInterpolatorType:I

    .line 175
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAnimationList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 179
    return-void

    .line 176
    :cond_0
    iget-object v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mAnimationList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;

    .line 177
    .local v0, "aniHandle":Lcom/sec/android/samsunganimation/animation/SAAnimation;
    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mInterpolatorType:I

    invoke-virtual {v0, v2}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->setInterpolator(I)V

    .line 175
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setShareAnimationInfo(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 205
    iput-boolean p1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mShareAnimationInfo:Z

    .line 206
    return-void
.end method

.method public setShareInterpolator(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 196
    iput-boolean p1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimationSet;->mShareInterpolator:Z

    .line 197
    return-void
.end method
