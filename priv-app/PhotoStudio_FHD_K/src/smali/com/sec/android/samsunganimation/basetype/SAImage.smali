.class public Lcom/sec/android/samsunganimation/basetype/SAImage;
.super Ljava/lang/Object;
.source "SAImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;
    }
.end annotation


# instance fields
.field private mAlphaType:Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;

.field public mBitmap:Landroid/graphics/Bitmap;

.field private mNativeHandle:I

.field private mReportFinalize:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lcom/sec/android/samsunganimation/SamsungAnimationDesc;->mName:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mBitmap:Landroid/graphics/Bitmap;

    .line 154
    iput v1, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    .line 156
    iput-boolean v1, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mReportFinalize:Z

    .line 38
    invoke-static {}, Lcom/sec/android/samsunganimation/basetype/SAImage;->nativeCreateSAImage()I

    move-result v0

    iput v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    .line 39
    sget-object v0, Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;->NORMAL:Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;

    iput-object v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mAlphaType:Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;

    .line 40
    return-void
.end method

.method private static native nativeCreateSAImage()I
.end method

.method private static native nativeDeleteSAImage(I)V
.end method

.method private static native nativeLock(I)V
.end method

.method private static native nativeSetAlphaType(II)V
.end method

.method private static native nativeSetBitmap(ILandroid/graphics/Bitmap;)V
.end method

.method private static native nativeUnlock(I)V
.end method


# virtual methods
.method public finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 105
    invoke-virtual {p0, v0}, Lcom/sec/android/samsunganimation/basetype/SAImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 106
    iput-object v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mBitmap:Landroid/graphics/Bitmap;

    .line 107
    iget-boolean v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mReportFinalize:Z

    if-eqz v0, :cond_0

    .line 108
    const-string v0, "SamsungAnimation"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SAImage is finalized nativeHandle:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :cond_0
    iget v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    if-eqz v0, :cond_1

    .line 110
    iget v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    invoke-static {v0}, Lcom/sec/android/samsunganimation/basetype/SAImage;->nativeDeleteSAImage(I)V

    .line 111
    :cond_1
    return-void
.end method

.method public getAlphaType()Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mAlphaType:Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;

    return-object v0
.end method

.method public getNativeHandle()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    return v0
.end method

.method public lock()V
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    invoke-static {v0}, Lcom/sec/android/samsunganimation/basetype/SAImage;->nativeLock(I)V

    .line 47
    return-void
.end method

.method public setAlphaType(Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;)V
    .locals 2
    .param p1, "alphaType"    # Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;

    .prologue
    .line 120
    iget v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    if-nez v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 123
    :cond_0
    iput-object p1, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mAlphaType:Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;

    .line 125
    iget-object v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mAlphaType:Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;

    sget-object v1, Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;->NORMAL:Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;

    if-ne v0, v1, :cond_1

    .line 126
    iget v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/samsunganimation/basetype/SAImage;->nativeSetAlphaType(II)V

    goto :goto_0

    .line 128
    :cond_1
    iget v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/samsunganimation/basetype/SAImage;->nativeSetAlphaType(II)V

    goto :goto_0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    if-nez v0, :cond_0

    .line 73
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mBitmap:Landroid/graphics/Bitmap;

    .line 68
    :cond_1
    iput-object p1, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mBitmap:Landroid/graphics/Bitmap;

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/basetype/SAImage;->lock()V

    .line 71
    iget v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    iget-object v1, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/sec/android/samsunganimation/basetype/SAImage;->nativeSetBitmap(ILandroid/graphics/Bitmap;)V

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/basetype/SAImage;->unlock()V

    goto :goto_0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "alphaType"    # Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    if-nez v0, :cond_0

    .line 99
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mBitmap:Landroid/graphics/Bitmap;

    .line 89
    :cond_1
    iput-object p1, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mBitmap:Landroid/graphics/Bitmap;

    .line 90
    iput-object p2, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mAlphaType:Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/basetype/SAImage;->lock()V

    .line 93
    iget v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    iget-object v1, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/sec/android/samsunganimation/basetype/SAImage;->nativeSetBitmap(ILandroid/graphics/Bitmap;)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mAlphaType:Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;

    sget-object v1, Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;->NORMAL:Lcom/sec/android/samsunganimation/basetype/SAImage$AlphaType;

    if-ne v0, v1, :cond_2

    .line 95
    iget v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/samsunganimation/basetype/SAImage;->nativeSetAlphaType(II)V

    .line 98
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/basetype/SAImage;->unlock()V

    goto :goto_0

    .line 97
    :cond_2
    iget v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/samsunganimation/basetype/SAImage;->nativeSetAlphaType(II)V

    goto :goto_1
.end method

.method public setReportFinalize(Z)V
    .locals 0
    .param p1, "report"    # Z

    .prologue
    .line 151
    iput-boolean p1, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mReportFinalize:Z

    return-void
.end method

.method public unlock()V
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/samsunganimation/basetype/SAImage;->mNativeHandle:I

    invoke-static {v0}, Lcom/sec/android/samsunganimation/basetype/SAImage;->nativeUnlock(I)V

    .line 54
    return-void
.end method
