.class public Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;
.super Lcom/sec/android/samsunganimation/particle/SAParticleEffect;
.source "SAKeyFrameParticleEffect.java"


# instance fields
.field protected mGeneratorPositionKeyFrameDuration:I

.field protected mGeneratorPositionKeyFrameInterpolaterType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/samsunganimation/SamsungAnimationDesc;->mName:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 7
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/particle/SAParticleEffect;-><init>()V

    .line 10
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->initialize()V

    .line 11
    invoke-static {}, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->nativeCreateKeyFrameParticleEffect()I

    move-result v0

    iput v0, p0, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->mNativeHandle:I

    .line 12
    return-void
.end method

.method protected static native nativeAddGeneratorPositionKeyFrame(IFFFF)V
.end method

.method protected static native nativeAddParticles(IFI)V
.end method

.method private static native nativeCreateKeyFrameParticleEffect()I
.end method

.method protected static native nativeSetGeneratorPositionKeyFrameAnimationProperty(IIII)V
.end method


# virtual methods
.method public addGeneratorPositionKeyFrame(FLcom/sec/android/samsunganimation/basetype/SAVector3;)V
    .locals 4
    .param p1, "keyTime"    # F
    .param p2, "pos"    # Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->mNativeHandle:I

    iget v1, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iget v3, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    invoke-static {v0, p1, v1, v2, v3}, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->nativeAddGeneratorPositionKeyFrame(IFFFF)V

    .line 53
    return-void
.end method

.method public addParticles(FLcom/sec/android/samsunganimation/particle/SAParticles;)V
    .locals 2
    .param p1, "keyTime"    # F
    .param p2, "particles"    # Lcom/sec/android/samsunganimation/particle/SAParticles;

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->mNativeHandle:I

    invoke-virtual {p2}, Lcom/sec/android/samsunganimation/particle/SAParticles;->getNativeHandle()I

    move-result v1

    invoke-static {v0, p1, v1}, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->nativeAddParticles(IFI)V

    .line 36
    return-void
.end method

.method public addParticles(Lcom/sec/android/samsunganimation/particle/SAParticles;)V
    .locals 1
    .param p1, "particles"    # Lcom/sec/android/samsunganimation/particle/SAParticles;

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->addParticles(FLcom/sec/android/samsunganimation/particle/SAParticles;)V

    .line 31
    return-void
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 16
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->mNativeHandle:I

    if-eq v0, v1, :cond_0

    .line 18
    iput v1, p0, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->mNativeHandle:I

    .line 20
    :cond_0
    return-void
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x2

    return v0
.end method

.method public setGeneratorPositionKeyFrameAnimationProperty(IIZ)V
    .locals 4
    .param p1, "duration"    # I
    .param p2, "interpolatorType"    # I
    .param p3, "keyValueReset"    # Z

    .prologue
    .line 40
    iput p1, p0, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->mGeneratorPositionKeyFrameDuration:I

    .line 41
    iput p2, p0, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->mGeneratorPositionKeyFrameInterpolaterType:I

    .line 43
    const/4 v0, 0x0

    .line 44
    .local v0, "reset":I
    if-eqz p3, :cond_0

    .line 45
    const/4 v0, 0x1

    .line 47
    :cond_0
    iget v1, p0, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->mNativeHandle:I

    iget v2, p0, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->mGeneratorPositionKeyFrameDuration:I

    iget v3, p0, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->mGeneratorPositionKeyFrameInterpolaterType:I

    invoke-static {v1, v2, v3, v0}, Lcom/sec/android/samsunganimation/particle/SAKeyFrameParticleEffect;->nativeSetGeneratorPositionKeyFrameAnimationProperty(IIII)V

    .line 48
    return-void
.end method
