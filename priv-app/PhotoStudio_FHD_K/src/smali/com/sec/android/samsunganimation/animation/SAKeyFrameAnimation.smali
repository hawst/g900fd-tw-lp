.class public Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;
.super Lcom/sec/android/samsunganimation/animation/SAPropertyAnimation;
.source "SAKeyFrameAnimation.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 159
    sget-object v0, Lcom/sec/android/samsunganimation/SamsungAnimationDesc;->mName:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAPropertyAnimation;-><init>()V

    .line 41
    invoke-virtual {p0, p1}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->initializeKeyFrameAnimation(I)V

    .line 42
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "lightType"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAPropertyAnimation;-><init>()V

    .line 52
    iput p2, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mLightType:I

    .line 53
    invoke-virtual {p0, p1}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->initializeKeyFrameAnimation(I)V

    .line 54
    return-void
.end method

.method private static native nativeAddKeyProperty(IIF[F)V
.end method

.method private static native nativeCreateSAKeyFrameAnimation(I)I
.end method


# virtual methods
.method public addKeyProperty(FF)V
    .locals 3
    .param p1, "keyTime"    # F
    .param p2, "value"    # F

    .prologue
    .line 77
    const/4 v1, 0x1

    new-array v0, v1, [F

    const/4 v1, 0x0

    aput p2, v0, v1

    .line 78
    .local v0, "data":[F
    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mNativeAnimation:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mAnimationType:I

    invoke-static {v1, v2, p1, v0}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->nativeAddKeyProperty(IIF[F)V

    .line 79
    return-void
.end method

.method public addKeyProperty(FLcom/sec/android/samsunganimation/basetype/SAColor;)V
    .locals 3
    .param p1, "keyTime"    # F
    .param p2, "color"    # Lcom/sec/android/samsunganimation/basetype/SAColor;

    .prologue
    .line 110
    const/4 v1, 0x4

    new-array v0, v1, [F

    const/4 v1, 0x0

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    aput v2, v0, v1

    const/4 v1, 0x2

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    aput v2, v0, v1

    const/4 v1, 0x3

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    aput v2, v0, v1

    .line 111
    .local v0, "data":[F
    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mNativeAnimation:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mAnimationType:I

    invoke-static {v1, v2, p1, v0}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->nativeAddKeyProperty(IIF[F)V

    .line 112
    return-void
.end method

.method public addKeyProperty(FLcom/sec/android/samsunganimation/basetype/SAPoint;)V
    .locals 3
    .param p1, "keyTime"    # F
    .param p2, "point"    # Lcom/sec/android/samsunganimation/basetype/SAPoint;

    .prologue
    .line 88
    const/4 v1, 0x2

    new-array v0, v1, [F

    const/4 v1, 0x0

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mX:F

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mY:F

    aput v2, v0, v1

    .line 89
    .local v0, "data":[F
    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mNativeAnimation:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mAnimationType:I

    invoke-static {v1, v2, p1, v0}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->nativeAddKeyProperty(IIF[F)V

    .line 90
    return-void
.end method

.method public addKeyProperty(FLcom/sec/android/samsunganimation/basetype/SARect;)V
    .locals 3
    .param p1, "keyTime"    # F
    .param p2, "rect"    # Lcom/sec/android/samsunganimation/basetype/SARect;

    .prologue
    .line 99
    const/4 v1, 0x4

    new-array v0, v1, [F

    const/4 v1, 0x0

    iget-object v2, p2, Lcom/sec/android/samsunganimation/basetype/SARect;->mOrigin:Lcom/sec/android/samsunganimation/basetype/SAPoint;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mX:F

    aput v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p2, Lcom/sec/android/samsunganimation/basetype/SARect;->mOrigin:Lcom/sec/android/samsunganimation/basetype/SAPoint;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mY:F

    aput v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p2, Lcom/sec/android/samsunganimation/basetype/SARect;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SASize;->mWidth:F

    aput v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p2, Lcom/sec/android/samsunganimation/basetype/SARect;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SASize;->mHeight:F

    aput v2, v0, v1

    .line 100
    .local v0, "data":[F
    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mNativeAnimation:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mAnimationType:I

    invoke-static {v1, v2, p1, v0}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->nativeAddKeyProperty(IIF[F)V

    .line 101
    return-void
.end method

.method public addKeyProperty(FLcom/sec/android/samsunganimation/basetype/SAVector3;)V
    .locals 3
    .param p1, "keyTime"    # F
    .param p2, "value"    # Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .prologue
    .line 121
    const/4 v1, 0x3

    new-array v0, v1, [F

    const/4 v1, 0x0

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    aput v2, v0, v1

    const/4 v1, 0x2

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    aput v2, v0, v1

    .line 122
    .local v0, "data":[F
    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mNativeAnimation:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mAnimationType:I

    invoke-static {v1, v2, p1, v0}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->nativeAddKeyProperty(IIF[F)V

    .line 123
    return-void
.end method

.method public addKeyProperty(FLcom/sec/android/samsunganimation/basetype/SAVector4;)V
    .locals 3
    .param p1, "keyTime"    # F
    .param p2, "value"    # Lcom/sec/android/samsunganimation/basetype/SAVector4;

    .prologue
    .line 132
    const/4 v1, 0x4

    new-array v0, v1, [F

    const/4 v1, 0x0

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector4;->mX:F

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector4;->mY:F

    aput v2, v0, v1

    const/4 v1, 0x2

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector4;->mZ:F

    aput v2, v0, v1

    const/4 v1, 0x3

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector4;->mW:F

    aput v2, v0, v1

    .line 133
    .local v0, "data":[F
    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mNativeAnimation:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mAnimationType:I

    invoke-static {v1, v2, p1, v0}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->nativeAddKeyProperty(IIF[F)V

    .line 134
    return-void
.end method

.method public addKeyProperty(F[F)V
    .locals 2
    .param p1, "keyTime"    # F
    .param p2, "value"    # [F

    .prologue
    .line 144
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mNativeAnimation:I

    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mAnimationType:I

    invoke-static {v0, v1, p1, p2}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->nativeAddKeyProperty(IIF[F)V

    .line 145
    return-void
.end method

.method public finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->deleteNativeAnimationHandle()V

    .line 154
    return-void
.end method

.method public initializeKeyFrameAnimation(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 63
    const/16 v0, 0x19

    if-ne p1, v0, :cond_0

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mScaleType:I

    .line 65
    :cond_0
    iput p1, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mAnimationType:I

    .line 66
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mClassType:I

    .line 67
    invoke-static {p1}, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->nativeCreateSAKeyFrameAnimation(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAKeyFrameAnimation;->mNativeAnimation:I

    .line 68
    return-void
.end method
