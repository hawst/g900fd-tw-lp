.class Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;
.super Ljava/lang/Thread;
.source "SAGLWallpaperService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;
    }
.end annotation


# static fields
.field public static final DEBUG_CHECK_GL_ERROR:I = 0x1

.field public static final DEBUG_LOG_GL_CALLS:I = 0x2

.field private static final LOG_THREADS:Z = true


# instance fields
.field public mDone:Z

.field private mEGLConfigChooser:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;

.field private mEGLContextFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLContextFactory;

.field private mEGLWindowSurfaceFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLWindowSurfaceFactory;

.field private mEglHelper:Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;

.field private mEglOwner:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

.field private mEventQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mEventsWaiting:Z

.field private mGLWrapper:Lcom/sec/android/samsunganimation/glwallpaperservice/GLWrapper;

.field private mHasSurface:Z

.field private mHaveEgl:Z

.field private mHeight:I

.field public mHolder:Landroid/view/SurfaceHolder;

.field private mPaused:Z

.field private mRenderMode:I

.field private mRenderer:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLRenderer;

.field private mRequestRender:Z

.field private mSizeChanged:Z

.field private mWaitingForSurface:Z

.field private mWidth:I

.field private final sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;


# direct methods
.method constructor <init>(Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLRenderer;Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;Lcom/sec/android/samsunganimation/glwallpaperservice/EGLContextFactory;Lcom/sec/android/samsunganimation/glwallpaperservice/EGLWindowSurfaceFactory;Lcom/sec/android/samsunganimation/glwallpaperservice/GLWrapper;)V
    .locals 4
    .param p1, "renderer"    # Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLRenderer;
    .param p2, "chooser"    # Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;
    .param p3, "contextFactory"    # Lcom/sec/android/samsunganimation/glwallpaperservice/EGLContextFactory;
    .param p4, "surfaceFactory"    # Lcom/sec/android/samsunganimation/glwallpaperservice/EGLWindowSurfaceFactory;
    .param p5, "wrapper"    # Lcom/sec/android/samsunganimation/glwallpaperservice/GLWrapper;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 567
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 536
    new-instance v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;-><init>(Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;)V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    .line 545
    iput-boolean v3, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mSizeChanged:Z

    .line 562
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEventQueue:Ljava/util/ArrayList;

    .line 568
    iput-boolean v2, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mDone:Z

    .line 569
    iput v2, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mWidth:I

    .line 570
    iput v2, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHeight:I

    .line 571
    iput-boolean v3, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mRequestRender:Z

    .line 572
    iput v3, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mRenderMode:I

    .line 573
    iput-object p1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mRenderer:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLRenderer;

    .line 574
    iput-object p2, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEGLConfigChooser:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;

    .line 575
    iput-object p3, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEGLContextFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLContextFactory;

    .line 576
    iput-object p4, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEGLWindowSurfaceFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLWindowSurfaceFactory;

    .line 577
    iput-object p5, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mGLWrapper:Lcom/sec/android/samsunganimation/glwallpaperservice/GLWrapper;

    .line 580
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;)Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEglOwner:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;)V
    .locals 0

    .prologue
    .line 537
    iput-object p1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEglOwner:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    return-void
.end method

.method private getEvent()Ljava/lang/Runnable;
    .locals 2

    .prologue
    .line 862
    monitor-enter p0

    .line 863
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 864
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEventQueue:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    monitor-exit p0

    .line 868
    :goto_0
    return-object v0

    .line 862
    :cond_0
    monitor-exit p0

    .line 868
    const/4 v0, 0x0

    goto :goto_0

    .line 862
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private guardedRun()V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/16 v16, 0x1

    .line 610
    new-instance v11, Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEGLConfigChooser:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEGLContextFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLContextFactory;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEGLWindowSurfaceFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLWindowSurfaceFactory;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mGLWrapper:Lcom/sec/android/samsunganimation/glwallpaperservice/GLWrapper;

    invoke-direct {v11, v12, v13, v14, v15}, Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;-><init>(Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;Lcom/sec/android/samsunganimation/glwallpaperservice/EGLContextFactory;Lcom/sec/android/samsunganimation/glwallpaperservice/EGLWindowSurfaceFactory;Lcom/sec/android/samsunganimation/glwallpaperservice/GLWrapper;)V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEglHelper:Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;

    .line 612
    const/4 v4, 0x0

    .line 613
    .local v4, "gl":Ljavax/microedition/khronos/opengles/GL10;
    const/4 v9, 0x1

    .line 614
    .local v9, "tellRendererSurfaceCreated":Z
    const/4 v8, 0x1

    .line 619
    .local v8, "tellRendererSurfaceChanged":Z
    :cond_0
    :goto_0
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->isDone()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    move-result v11

    if-eqz v11, :cond_1

    .line 739
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v12

    .line 740
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->stopEglLocked()V

    .line 741
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEglHelper:Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;

    invoke-virtual {v11}, Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;->finish()V

    .line 739
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    .line 744
    :goto_1
    return-void

    .line 623
    :cond_1
    const/4 v10, 0x0

    .line 624
    .local v10, "w":I
    const/4 v5, 0x0

    .line 625
    .local v5, "h":I
    const/4 v2, 0x0

    .line 626
    .local v2, "changed":Z
    const/4 v6, 0x0

    .line 627
    .local v6, "needStart":Z
    const/4 v3, 0x0

    .line 629
    .local v3, "eventsWaiting":Z
    :try_start_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 633
    :goto_2
    :try_start_3
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mPaused:Z

    if-eqz v11, :cond_2

    .line 634
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->stopEglLocked()V

    .line 636
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHasSurface:Z

    if-nez v11, :cond_4

    .line 637
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mWaitingForSurface:Z

    if-nez v11, :cond_3

    .line 638
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->stopEglLocked()V

    .line 639
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mWaitingForSurface:Z

    .line 640
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v11}, Ljava/lang/Object;->notifyAll()V

    .line 658
    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mDone:Z

    if-eqz v11, :cond_5

    .line 659
    monitor-exit v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 739
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v12

    .line 740
    :try_start_4
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->stopEglLocked()V

    .line 741
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEglHelper:Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;

    invoke-virtual {v11}, Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;->finish()V

    .line 739
    monitor-exit v12

    goto :goto_1

    :catchall_0
    move-exception v11

    monitor-exit v12
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v11

    .line 643
    :cond_4
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHaveEgl:Z

    if-nez v11, :cond_3

    .line 644
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;->tryAcquireEglSurface(Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 645
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHaveEgl:Z

    .line 646
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEglHelper:Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;

    invoke-virtual {v11}, Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;->start()V

    .line 647
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mRequestRender:Z

    .line 648
    const/4 v6, 0x1

    goto :goto_3

    .line 662
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEventsWaiting:Z

    if-eqz v11, :cond_8

    .line 663
    const/4 v3, 0x1

    .line 664
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEventsWaiting:Z

    .line 629
    :cond_6
    :goto_4
    monitor-exit v12
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 695
    if-eqz v3, :cond_b

    .line 697
    :cond_7
    :try_start_6
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->getEvent()Ljava/lang/Runnable;

    move-result-object v7

    .local v7, "r":Ljava/lang/Runnable;
    if-eqz v7, :cond_0

    .line 698
    invoke-interface {v7}, Ljava/lang/Runnable;->run()V

    .line 699
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->isDone()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-result v11

    if-eqz v11, :cond_7

    .line 739
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v12

    .line 740
    :try_start_7
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->stopEglLocked()V

    .line 741
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEglHelper:Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;

    invoke-virtual {v11}, Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;->finish()V

    .line 739
    monitor-exit v12

    goto/16 :goto_1

    :catchall_1
    move-exception v11

    monitor-exit v12
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v11

    .line 668
    .end local v7    # "r":Ljava/lang/Runnable;
    :cond_8
    :try_start_8
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mPaused:Z

    if-nez v11, :cond_a

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHasSurface:Z

    if-eqz v11, :cond_a

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHaveEgl:Z

    if-eqz v11, :cond_a

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mWidth:I

    if-lez v11, :cond_a

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHeight:I

    if-lez v11, :cond_a

    .line 669
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mRequestRender:Z

    if-nez v11, :cond_9

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mRenderMode:I

    move/from16 v0, v16

    if-ne v11, v0, :cond_a

    .line 670
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mSizeChanged:Z

    .line 671
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mWidth:I

    .line 672
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHeight:I

    .line 673
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mSizeChanged:Z

    .line 674
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mRequestRender:Z

    .line 675
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHasSurface:Z

    if-eqz v11, :cond_6

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mWaitingForSurface:Z

    if-eqz v11, :cond_6

    .line 676
    const/4 v2, 0x1

    .line 677
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mWaitingForSurface:Z

    .line 678
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v11}, Ljava/lang/Object;->notifyAll()V

    goto/16 :goto_4

    .line 629
    :catchall_2
    move-exception v11

    monitor-exit v12
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v11
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 735
    .end local v2    # "changed":Z
    .end local v3    # "eventsWaiting":Z
    .end local v5    # "h":I
    .end local v6    # "needStart":Z
    .end local v10    # "w":I
    :catchall_3
    move-exception v11

    .line 739
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v12

    .line 740
    :try_start_a
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->stopEglLocked()V

    .line 741
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEglHelper:Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;

    invoke-virtual {v13}, Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;->finish()V

    .line 739
    monitor-exit v12
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 743
    throw v11

    .line 686
    .restart local v2    # "changed":Z
    .restart local v3    # "eventsWaiting":Z
    .restart local v5    # "h":I
    .restart local v6    # "needStart":Z
    .restart local v10    # "w":I
    :cond_a
    :try_start_b
    const-string v11, "GLThread"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "waiting tid="

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->getId()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v11}, Ljava/lang/Object;->wait()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto/16 :goto_2

    .line 707
    :cond_b
    if-eqz v6, :cond_c

    .line 708
    const/4 v9, 0x1

    .line 709
    const/4 v2, 0x1

    .line 711
    :cond_c
    if-eqz v2, :cond_d

    .line 712
    :try_start_c
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEglHelper:Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v11, v12}, Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;->createSurface(Landroid/view/SurfaceHolder;)Ljavax/microedition/khronos/opengles/GL;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Ljavax/microedition/khronos/opengles/GL10;

    move-object v4, v0

    .line 713
    const/4 v8, 0x1

    .line 715
    :cond_d
    if-eqz v9, :cond_e

    .line 716
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mRenderer:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLRenderer;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEglHelper:Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;

    iget-object v12, v12, Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-virtual {v11, v4, v12}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLRenderer;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 717
    const/4 v9, 0x0

    .line 719
    :cond_e
    if-eqz v8, :cond_f

    .line 720
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mRenderer:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLRenderer;

    invoke-virtual {v11, v4, v10, v5}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLRenderer;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 721
    const/4 v8, 0x0

    .line 723
    :cond_f
    if-lez v10, :cond_0

    if-lez v5, :cond_0

    .line 725
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mRenderer:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLRenderer;

    invoke-virtual {v11, v4}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLRenderer;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 731
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEglHelper:Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;

    invoke-virtual {v11}, Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;->swap()Z

    .line 732
    const-wide/16 v12, 0xa

    invoke-static {v12, v13}, Ljava/lang/Thread;->sleep(J)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    goto/16 :goto_0

    .line 739
    .end local v2    # "changed":Z
    .end local v3    # "eventsWaiting":Z
    .end local v5    # "h":I
    .end local v6    # "needStart":Z
    .end local v10    # "w":I
    :catchall_4
    move-exception v11

    :try_start_d
    monitor-exit v12
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    throw v11

    :catchall_5
    move-exception v11

    :try_start_e
    monitor-exit v12
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    throw v11
.end method

.method private isDone()Z
    .locals 2

    .prologue
    .line 747
    iget-object v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v1

    .line 748
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mDone:Z

    monitor-exit v1

    return v0

    .line 747
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private stopEglLocked()V
    .locals 1

    .prologue
    .line 602
    iget-boolean v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHaveEgl:Z

    if-eqz v0, :cond_0

    .line 603
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHaveEgl:Z

    .line 604
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEglHelper:Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/EglHelper;->destroySurface()V

    .line 605
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;->releaseEglSurface(Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;)V

    .line 607
    :cond_0
    return-void
.end method


# virtual methods
.method public enableLogFPS(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 912
    invoke-static {p1}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAWallpaperNative;->nativeEnableLogFPS(Z)V

    .line 913
    return-void
.end method

.method public enableShowFPS(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 908
    invoke-static {p1}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAWallpaperNative;->nativeEnableShowFPS(Z)V

    .line 909
    return-void
.end method

.method public enableTranslucent(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 917
    invoke-static {p1}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAWallpaperNative;->nativeEnableTranslucent(Z)V

    .line 918
    return-void
.end method

.method public getRenderMode()I
    .locals 2

    .prologue
    .line 765
    iget-object v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v1

    .line 766
    :try_start_0
    iget v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mRenderMode:I

    monitor-exit v1

    return v0

    .line 765
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 806
    iget-object v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v1

    .line 807
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mPaused:Z

    .line 808
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 809
    invoke-static {}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAWallpaperNative;->nativePauseSAGLSurface()V

    .line 806
    monitor-exit v1

    .line 811
    return-void

    .line 806
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 814
    iget-object v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v1

    .line 815
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mPaused:Z

    .line 816
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mRequestRender:Z

    .line 817
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 818
    invoke-static {}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAWallpaperNative;->nativeResumeSAGLSurface()V

    .line 814
    monitor-exit v1

    .line 820
    return-void

    .line 814
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onWindowResize(II)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 823
    iget-object v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v1

    .line 824
    :try_start_0
    iput p1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mWidth:I

    .line 825
    iput p2, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHeight:I

    .line 826
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mSizeChanged:Z

    .line 827
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 823
    monitor-exit v1

    .line 829
    return-void

    .line 823
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public queueEvent(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 852
    monitor-enter p0

    .line 853
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 854
    iget-object v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 855
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mEventsWaiting:Z

    .line 856
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 854
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 852
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 859
    return-void

    .line 854
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 852
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public requestExitAndWait()V
    .locals 3

    .prologue
    .line 834
    iget-object v2, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v2

    .line 835
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mDone:Z

    .line 836
    iget-object v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 834
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 839
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 843
    :goto_0
    return-void

    .line 834
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 840
    :catch_0
    move-exception v0

    .line 841
    .local v0, "ex":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method public requestRender()V
    .locals 2

    .prologue
    .line 771
    iget-object v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v1

    .line 772
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mRequestRender:Z

    .line 773
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 771
    monitor-exit v1

    .line 775
    return-void

    .line 771
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 584
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GLThread "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->setName(Ljava/lang/String;)V

    .line 586
    const-string v0, "GLThread"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "starting tid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->guardedRun()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 594
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;->threadExiting(Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;)V

    .line 596
    :goto_0
    return-void

    .line 591
    :catch_0
    move-exception v0

    .line 594
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;->threadExiting(Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;)V

    goto :goto_0

    .line 593
    :catchall_0
    move-exception v0

    .line 594
    iget-object v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v1, p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;->threadExiting(Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;)V

    .line 595
    throw v0
.end method

.method public setRenderMode(I)V
    .locals 2
    .param p1, "renderMode"    # I

    .prologue
    const/4 v0, 0x1

    .line 753
    if-ltz p1, :cond_0

    if-le p1, v0, :cond_1

    .line 754
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "renderMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 756
    :cond_1
    iget-object v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v1

    .line 757
    :try_start_0
    iput p1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mRenderMode:I

    .line 758
    if-ne p1, v0, :cond_2

    .line 759
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 756
    :cond_2
    monitor-exit v1

    .line 762
    return-void

    .line 756
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 6
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 778
    iput-object p1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHolder:Landroid/view/SurfaceHolder;

    .line 779
    iget-object v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v1

    .line 781
    :try_start_0
    const-string v0, "GLThread"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "surfaceCreated tid="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHasSurface:Z

    .line 784
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 779
    monitor-exit v1

    .line 786
    return-void

    .line 779
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public surfaceDestroyed()V
    .locals 6

    .prologue
    .line 789
    iget-object v2, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    monitor-enter v2

    .line 791
    :try_start_0
    const-string v1, "GLThread"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "surfaceDestroyed tid="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 793
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mHasSurface:Z

    .line 794
    iget-object v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 795
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mWaitingForSurface:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->mDone:Z

    if-eqz v1, :cond_1

    .line 789
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803
    return-void

    .line 797
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->sGLThreadManager:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread$GLThreadManager;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 798
    :catch_0
    move-exception v0

    .line 799
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 789
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public swapAnimationSAGLSurface()V
    .locals 0

    .prologue
    .line 921
    invoke-static {}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAWallpaperNative;->nativeSwapAnimationSAGLSurface()V

    .line 922
    return-void
.end method
