.class public Lcom/sec/android/samsunganimation/particle/SAParticles;
.super Ljava/lang/Object;
.source "SAParticles.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/samsunganimation/particle/SAParticles$ParticleBlendType;
    }
.end annotation


# instance fields
.field protected mBlendType:I

.field protected mColorKeyFrameDuration:I

.field protected mColorKeyFrameInterpolaterType:I

.field protected mDefaultForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

.field protected mDefaultGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

.field protected mDefaultPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

.field protected mImage:Lcom/sec/android/samsunganimation/basetype/SAImage;

.field protected mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

.field protected mMaxDuration:I

.field protected mMaxMass:F

.field protected mMaxParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

.field private mNativeHandle:I

.field protected mParticleCount:I

.field protected mPositionKeyFrameDuration:I

.field protected mPositionKeyFrameInterpolaterType:I

.field protected mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

.field protected mRandomDuration:I

.field protected mRandomForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

.field protected mRandomGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

.field protected mRandomMass:F

.field protected mRandomParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

.field protected mRandomPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

.field protected mScaleKeyFrameDuration:I

.field protected mScaleKeyFrameInterpolaterType:I

.field protected mTextureFileName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 382
    sget-object v0, Lcom/sec/android/samsunganimation/SamsungAnimationDesc;->mName:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 12
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    .line 22
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/particle/SAParticles;->initialize()V

    .line 24
    invoke-static {}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeCreateParticles()I

    move-result v0

    iput v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    .line 25
    return-void
.end method

.method protected static native nativeAddColorKeyFrame(IFFFFF)V
.end method

.method protected static native nativeAddPositionKeyFrame(IFFFF)V
.end method

.method protected static native nativeAddScaleKeyFrame(IFFFF)V
.end method

.method private static native nativeCreateParticles()I
.end method

.method protected static native nativeSetColorKeyFrameAnimationProperty(IIII)V
.end method

.method protected static native nativeSetDefaultForce(IFFF)V
.end method

.method protected static native nativeSetDefaultGravity(IFFF)V
.end method

.method protected static native nativeSetDefaultPosition(IFFF)V
.end method

.method protected static native nativeSetMaxColor(IFFFF)V
.end method

.method protected static native nativeSetMaxDuration(II)V
.end method

.method protected static native nativeSetMaxMass(IF)V
.end method

.method protected static native nativeSetMaxSize(IFFF)V
.end method

.method protected static native nativeSetParticleCount(II)V
.end method

.method protected static native nativeSetPositionKeyFrameAnimationProperty(IIII)V
.end method

.method protected static native nativeSetRandomColor(IFFFF)V
.end method

.method protected static native nativeSetRandomDuration(II)V
.end method

.method protected static native nativeSetRandomForce(IFFF)V
.end method

.method protected static native nativeSetRandomGravity(IFFF)V
.end method

.method protected static native nativeSetRandomMass(IF)V
.end method

.method protected static native nativeSetRandomPosition(IFFF)V
.end method

.method protected static native nativeSetRandomSize(IFFF)V
.end method

.method protected static native nativeSetScaleKeyFrameAnimationProperty(IIII)V
.end method

.method protected static native nativeSetTextureFile(ILjava/lang/String;I)V
.end method


# virtual methods
.method public addColorKeyFrame(FLcom/sec/android/samsunganimation/basetype/SAColor;)V
    .locals 6
    .param p1, "keyTime"    # F
    .param p2, "color"    # Lcom/sec/android/samsunganimation/basetype/SAColor;

    .prologue
    .line 323
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    iget v3, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    iget v4, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    iget v5, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    move v1, p1

    invoke-static/range {v0 .. v5}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeAddColorKeyFrame(IFFFFF)V

    .line 324
    return-void
.end method

.method public addPositionKeyFrame(FLcom/sec/android/samsunganimation/basetype/SAVector3;)V
    .locals 4
    .param p1, "keyTime"    # F
    .param p2, "position"    # Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .prologue
    .line 328
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    iget v1, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iget v3, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    invoke-static {v0, p1, v1, v2, v3}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeAddPositionKeyFrame(IFFFF)V

    .line 329
    return-void
.end method

.method public addScaleKeyFrame(FLcom/sec/android/samsunganimation/basetype/SAVector3;)V
    .locals 4
    .param p1, "keyTime"    # F
    .param p2, "scale"    # Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .prologue
    .line 318
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    iget v1, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iget v3, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    invoke-static {v0, p1, v1, v2, v3}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeAddScaleKeyFrame(IFFFF)V

    .line 319
    return-void
.end method

.method public copy(Lcom/sec/android/samsunganimation/particle/SAParticles;)V
    .locals 4
    .param p1, "info"    # Lcom/sec/android/samsunganimation/particle/SAParticles;

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 90
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mParticleCount:I

    iput v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mParticleCount:I

    .line 91
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mTextureFileName:Ljava/lang/String;

    iput-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mTextureFileName:Ljava/lang/String;

    .line 92
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxDuration:I

    iput v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxDuration:I

    .line 93
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomDuration:I

    iput v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomDuration:I

    .line 95
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mScaleKeyFrameDuration:I

    iput v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mScaleKeyFrameDuration:I

    .line 96
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mScaleKeyFrameInterpolaterType:I

    iput v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mScaleKeyFrameInterpolaterType:I

    .line 97
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mColorKeyFrameDuration:I

    iput v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mColorKeyFrameDuration:I

    .line 98
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mColorKeyFrameInterpolaterType:I

    iput v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mColorKeyFrameInterpolaterType:I

    .line 99
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mPositionKeyFrameDuration:I

    iput v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mPositionKeyFrameDuration:I

    .line 100
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mPositionKeyFrameInterpolaterType:I

    iput v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mPositionKeyFrameInterpolaterType:I

    .line 102
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 103
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 104
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 105
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 106
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 107
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 108
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    .line 109
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    .line 110
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    .line 111
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    .line 112
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    .line 113
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    .line 114
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    .line 115
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    .line 116
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 117
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 118
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 119
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 120
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 121
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 122
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 123
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 124
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 125
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 126
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 127
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 128
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxMass:F

    iput v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxMass:F

    .line 129
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomMass:F

    iput v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomMass:F

    .line 130
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 131
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 132
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 133
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 134
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 135
    iget-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget v1, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 136
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mImage:Lcom/sec/android/samsunganimation/basetype/SAImage;

    iput-object v0, p1, Lcom/sec/android/samsunganimation/particle/SAParticles;->mImage:Lcom/sec/android/samsunganimation/basetype/SAImage;

    .line 138
    iput v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mScaleKeyFrameDuration:I

    .line 139
    iput v3, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mScaleKeyFrameInterpolaterType:I

    .line 140
    iput v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mColorKeyFrameDuration:I

    .line 141
    iput v3, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mColorKeyFrameInterpolaterType:I

    .line 142
    iput v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mPositionKeyFrameDuration:I

    .line 143
    iput v3, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mPositionKeyFrameInterpolaterType:I

    .line 144
    return-void
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 80
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    if-eq v0, v1, :cond_0

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mImage:Lcom/sec/android/samsunganimation/basetype/SAImage;

    .line 83
    iput v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    .line 85
    :cond_0
    return-void
.end method

.method public getNativeHandle()I
    .locals 1

    .prologue
    .line 333
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    return v0
.end method

.method protected initialize()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 29
    iput v4, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mParticleCount:I

    .line 30
    iput-object v8, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mTextureFileName:Ljava/lang/String;

    .line 31
    iput v4, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxDuration:I

    .line 32
    iput v4, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomDuration:I

    .line 34
    iput v4, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mBlendType:I

    .line 36
    iput v4, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mScaleKeyFrameDuration:I

    .line 37
    iput v6, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mScaleKeyFrameInterpolaterType:I

    .line 38
    iput v4, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mColorKeyFrameDuration:I

    .line 39
    iput v6, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mColorKeyFrameInterpolaterType:I

    .line 40
    iput v4, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mPositionKeyFrameDuration:I

    .line 41
    iput v6, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mPositionKeyFrameInterpolaterType:I

    .line 44
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SAVector3;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .line 45
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SAVector3;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .line 46
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAColor;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SAColor;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    .line 47
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAColor;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SAColor;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    .line 48
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SAVector3;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .line 49
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SAVector3;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .line 50
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SAVector3;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .line 51
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SAVector3;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .line 52
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SAVector3;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .line 53
    new-instance v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/basetype/SAVector3;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .line 56
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput v5, v2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v5, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v5, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 57
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput v5, v2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v5, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v5, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 58
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v3, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput v7, v3, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    iput v7, v2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    iput v7, v1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    iput v7, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    .line 59
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iget-object v3, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput v5, v3, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    iput v5, v2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    iput v5, v1, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    iput v5, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    .line 60
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput v5, v2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v5, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v5, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 61
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput v5, v2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v5, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v5, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 62
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput v5, v2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v5, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v5, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 63
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput v5, v2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v5, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v5, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 64
    iput v7, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxMass:F

    .line 65
    iput v5, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomMass:F

    .line 66
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput v7, v2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v7, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v7, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 67
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput v5, v2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    iput v5, v1, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    iput v5, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 68
    iput-object v8, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mImage:Lcom/sec/android/samsunganimation/basetype/SAImage;

    .line 70
    iput v4, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mScaleKeyFrameDuration:I

    .line 71
    iput v6, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mScaleKeyFrameInterpolaterType:I

    .line 72
    iput v4, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mColorKeyFrameDuration:I

    .line 73
    iput v6, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mColorKeyFrameInterpolaterType:I

    .line 74
    iput v4, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mPositionKeyFrameDuration:I

    .line 75
    iput v6, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mPositionKeyFrameInterpolaterType:I

    .line 76
    return-void
.end method

.method public setColorKeyFrameAnimationProperty(IIZ)V
    .locals 4
    .param p1, "duration"    # I
    .param p2, "interpolatorType"    # I
    .param p3, "keyValueReset"    # Z

    .prologue
    .line 294
    iput p1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mColorKeyFrameDuration:I

    .line 295
    iput p2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mColorKeyFrameInterpolaterType:I

    .line 297
    const/4 v0, 0x0

    .line 298
    .local v0, "reset":I
    if-eqz p3, :cond_0

    .line 299
    const/4 v0, 0x1

    .line 301
    :cond_0
    iget v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    iget v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mColorKeyFrameDuration:I

    iget v3, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mColorKeyFrameInterpolaterType:I

    invoke-static {v1, v2, v3, v0}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetColorKeyFrameAnimationProperty(IIII)V

    .line 302
    return-void
.end method

.method public setDefaultForce(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 252
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p2, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 253
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p3, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 255
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    invoke-static {v0, p1, p2, p3}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetDefaultForce(IFFF)V

    .line 256
    return-void
.end method

.method public setDefaultGravity(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 234
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p2, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 235
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p3, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 237
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    invoke-static {v0, p1, p2, p3}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetDefaultGravity(IFFF)V

    .line 238
    return-void
.end method

.method public setDefaultPosition(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 196
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p2, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 197
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mDefaultPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p3, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 199
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    invoke-static {v0, p1, p2, p3}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetDefaultPosition(IFFF)V

    .line 200
    return-void
.end method

.method public setMaxColor(FFFF)V
    .locals 1
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "a"    # F

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput p1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    .line 214
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput p2, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    .line 215
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput p3, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    .line 216
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput p4, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    .line 218
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    invoke-static {v0, p1, p2, p3, p4}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetMaxColor(IFFFF)V

    .line 219
    return-void
.end method

.method public setMaxDuration(I)V
    .locals 1
    .param p1, "duration"    # I

    .prologue
    .line 163
    iput p1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxDuration:I

    .line 165
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    invoke-static {v0, p1}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetMaxDuration(II)V

    .line 166
    return-void
.end method

.method public setMaxMass(F)V
    .locals 1
    .param p1, "mass"    # F

    .prologue
    .line 269
    iput p1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxMass:F

    .line 271
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    invoke-static {v0, p1}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetMaxMass(IF)V

    .line 272
    return-void
.end method

.method public setMaxSize(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 178
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p2, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 179
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mMaxParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p3, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 181
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    invoke-static {v0, p1, p2, p3}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetMaxSize(IFFF)V

    .line 182
    return-void
.end method

.method public setParticleCount(I)V
    .locals 2
    .param p1, "particleCount"    # I

    .prologue
    .line 156
    iput p1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mParticleCount:I

    .line 158
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    iget v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mParticleCount:I

    invoke-static {v0, v1}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetParticleCount(II)V

    .line 159
    return-void
.end method

.method public setPositionKeyFrameAnimationProperty(IIZ)V
    .locals 4
    .param p1, "duration"    # I
    .param p2, "interpolatorType"    # I
    .param p3, "keyValueReset"    # Z

    .prologue
    .line 306
    iput p1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mPositionKeyFrameDuration:I

    .line 307
    iput p2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mPositionKeyFrameInterpolaterType:I

    .line 309
    const/4 v0, 0x0

    .line 310
    .local v0, "reset":I
    if-eqz p3, :cond_0

    .line 311
    const/4 v0, 0x1

    .line 313
    :cond_0
    iget v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    iget v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mPositionKeyFrameDuration:I

    iget v3, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mPositionKeyFrameInterpolaterType:I

    invoke-static {v1, v2, v3, v0}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetPositionKeyFrameAnimationProperty(IIII)V

    .line 314
    return-void
.end method

.method public setRandomColor(FFFF)V
    .locals 1
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "a"    # F

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput p1, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    .line 224
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput p2, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    .line 225
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput p3, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    .line 226
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomColor:Lcom/sec/android/samsunganimation/basetype/SAColor;

    iput p4, v0, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    .line 228
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    invoke-static {v0, p1, p2, p3, p4}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetRandomColor(IFFFF)V

    .line 229
    return-void
.end method

.method public setRandomDuration(I)V
    .locals 1
    .param p1, "duration"    # I

    .prologue
    .line 170
    iput p1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomDuration:I

    .line 172
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    invoke-static {v0, p1}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetRandomDuration(II)V

    .line 173
    return-void
.end method

.method public setRandomForce(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 261
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p2, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 262
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomForce:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p3, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 264
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    invoke-static {v0, p1, p2, p3}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetRandomForce(IFFF)V

    .line 265
    return-void
.end method

.method public setRandomGravity(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 243
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p2, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 244
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomGravity:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p3, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 246
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    invoke-static {v0, p1, p2, p3}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetRandomGravity(IFFF)V

    .line 247
    return-void
.end method

.method public setRandomMass(F)V
    .locals 1
    .param p1, "mass"    # F

    .prologue
    .line 276
    iput p1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomMass:F

    .line 278
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    invoke-static {v0, p1}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetRandomMass(IF)V

    .line 279
    return-void
.end method

.method public setRandomPosition(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 205
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p2, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 206
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomPosition:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p3, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 208
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    invoke-static {v0, p1, p2, p3}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetRandomPosition(IFFF)V

    .line 209
    return-void
.end method

.method public setRandomSize(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p1, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    .line 187
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p2, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    .line 188
    iget-object v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mRandomParticleSize:Lcom/sec/android/samsunganimation/basetype/SAVector3;

    iput p3, v0, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    .line 190
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    invoke-static {v0, p1, p2, p3}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetRandomSize(IFFF)V

    .line 191
    return-void
.end method

.method public setScaleKeyFrameAnimationProperty(IIZ)V
    .locals 4
    .param p1, "duration"    # I
    .param p2, "interpolatorType"    # I
    .param p3, "keyValueReset"    # Z

    .prologue
    .line 283
    iput p1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mScaleKeyFrameDuration:I

    .line 284
    iput p2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mScaleKeyFrameInterpolaterType:I

    .line 286
    const/4 v0, 0x0

    .line 287
    .local v0, "reset":I
    if-eqz p3, :cond_0

    .line 288
    const/4 v0, 0x1

    .line 290
    :cond_0
    iget v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    iget v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mScaleKeyFrameDuration:I

    iget v3, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mScaleKeyFrameInterpolaterType:I

    invoke-static {v1, v2, v3, v0}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetScaleKeyFrameAnimationProperty(IIII)V

    .line 291
    return-void
.end method

.method public setTextureFile(Ljava/lang/String;Lcom/sec/android/samsunganimation/basetype/SAImage;)V
    .locals 3
    .param p1, "textureFileName"    # Ljava/lang/String;
    .param p2, "image"    # Lcom/sec/android/samsunganimation/basetype/SAImage;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mTextureFileName:Ljava/lang/String;

    .line 149
    iput-object p2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mImage:Lcom/sec/android/samsunganimation/basetype/SAImage;

    .line 151
    iget v0, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mNativeHandle:I

    iget-object v1, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mTextureFileName:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/particle/SAParticles;->mImage:Lcom/sec/android/samsunganimation/basetype/SAImage;

    invoke-virtual {v2}, Lcom/sec/android/samsunganimation/basetype/SAImage;->getNativeHandle()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/samsunganimation/particle/SAParticles;->nativeSetTextureFile(ILjava/lang/String;I)V

    .line 152
    return-void
.end method
