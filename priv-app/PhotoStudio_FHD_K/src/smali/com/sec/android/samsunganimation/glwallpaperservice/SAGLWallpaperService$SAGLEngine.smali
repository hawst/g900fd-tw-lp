.class public Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;
.super Landroid/service/wallpaper/WallpaperService$Engine;
.source "SAGLWallpaperService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SAGLEngine"
.end annotation


# static fields
.field public static final RENDERMODE_CONTINUOUSLY:I = 0x1

.field public static final RENDERMODE_WHEN_DIRTY:I


# instance fields
.field private mDebugFlags:I

.field private mEGLConfigChooser:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;

.field private mEGLContextFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLContextFactory;

.field private mEGLWindowSurfaceFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLWindowSurfaceFactory;

.field private mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

.field private mGLWrapper:Lcom/sec/android/samsunganimation/glwallpaperservice/GLWrapper;

.field final synthetic this$0:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;


# direct methods
.method public constructor <init>(Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->this$0:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService;

    .line 50
    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;-><init>(Landroid/service/wallpaper/WallpaperService;)V

    .line 53
    return-void
.end method

.method private checkRenderThreadState()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    if-eqz v0, :cond_0

    .line 185
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_0
    return-void
.end method


# virtual methods
.method public getDebugFlags()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mDebugFlags:I

    return v0
.end method

.method public getRenderMode()I
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->getRenderMode()I

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "surfaceHolder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onCreate(Landroid/view/SurfaceHolder;)V

    .line 69
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->onDestroy()V

    .line 75
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->requestExitAndWait()V

    .line 76
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->onPause()V

    .line 173
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->onResume()V

    .line 177
    return-void
.end method

.method public onSurfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0, p3, p4}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->onWindowResize(II)V

    .line 82
    invoke-super {p0, p1, p2, p3, p4}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceChanged(Landroid/view/SurfaceHolder;III)V

    .line 83
    return-void
.end method

.method public onSurfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 87
    const-string v0, "GLWallpaperService"

    const-string v1, "onSurfaceCreated()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0, p1}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->surfaceCreated(Landroid/view/SurfaceHolder;)V

    .line 89
    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceCreated(Landroid/view/SurfaceHolder;)V

    .line 90
    return-void
.end method

.method public onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 94
    const-string v0, "GLWallpaperService"

    const-string v1, "onSurfaceDestroyed()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->surfaceDestroyed()V

    .line 96
    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V

    .line 97
    return-void
.end method

.method public onVisibilityChanged(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 57
    if-eqz p1, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->onResume()V

    .line 62
    :goto_0
    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onVisibilityChanged(Z)V

    .line 63
    return-void

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->onPause()V

    goto :goto_0
.end method

.method public queueEvent(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0, p1}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->queueEvent(Ljava/lang/Runnable;)V

    .line 181
    return-void
.end method

.method public requestRender()V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->requestRender()V

    .line 169
    return-void
.end method

.method public requestRenderView()V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->requestRender()V

    .line 195
    :cond_0
    return-void
.end method

.method public setDebugFlags(I)V
    .locals 0
    .param p1, "debugFlags"    # I

    .prologue
    .line 107
    iput p1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mDebugFlags:I

    .line 108
    return-void
.end method

.method public setEGLConfigChooser(IIIIII)V
    .locals 7
    .param p1, "redSize"    # I
    .param p2, "greenSize"    # I
    .param p3, "blueSize"    # I
    .param p4, "alphaSize"    # I
    .param p5, "depthSize"    # I
    .param p6, "stencilSize"    # I

    .prologue
    .line 155
    new-instance v0, Lcom/sec/android/samsunganimation/glwallpaperservice/BaseConfigChooser$ComponentSizeChooser;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    .line 156
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/samsunganimation/glwallpaperservice/BaseConfigChooser$ComponentSizeChooser;-><init>(IIIIII)V

    .line 155
    invoke-virtual {p0, v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->setEGLConfigChooser(Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;)V

    .line 157
    return-void
.end method

.method public setEGLConfigChooser(Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;)V
    .locals 0
    .param p1, "configChooser"    # Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->checkRenderThreadState()V

    .line 146
    iput-object p1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mEGLConfigChooser:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;

    .line 147
    return-void
.end method

.method public setEGLConfigChooser(Z)V
    .locals 1
    .param p1, "needDepth"    # Z

    .prologue
    .line 150
    new-instance v0, Lcom/sec/android/samsunganimation/glwallpaperservice/BaseConfigChooser$SimpleEGLConfigChooser;

    invoke-direct {v0, p1}, Lcom/sec/android/samsunganimation/glwallpaperservice/BaseConfigChooser$SimpleEGLConfigChooser;-><init>(Z)V

    invoke-virtual {p0, v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->setEGLConfigChooser(Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;)V

    .line 151
    return-void
.end method

.method public setEGLContextFactory(Lcom/sec/android/samsunganimation/glwallpaperservice/EGLContextFactory;)V
    .locals 0
    .param p1, "factory"    # Lcom/sec/android/samsunganimation/glwallpaperservice/EGLContextFactory;

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->checkRenderThreadState()V

    .line 136
    iput-object p1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mEGLContextFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLContextFactory;

    .line 137
    return-void
.end method

.method public setEGLWindowSurfaceFactory(Lcom/sec/android/samsunganimation/glwallpaperservice/EGLWindowSurfaceFactory;)V
    .locals 0
    .param p1, "factory"    # Lcom/sec/android/samsunganimation/glwallpaperservice/EGLWindowSurfaceFactory;

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->checkRenderThreadState()V

    .line 141
    iput-object p1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mEGLWindowSurfaceFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLWindowSurfaceFactory;

    .line 142
    return-void
.end method

.method public setGLWrapper(Lcom/sec/android/samsunganimation/glwallpaperservice/GLWrapper;)V
    .locals 0
    .param p1, "glWrapper"    # Lcom/sec/android/samsunganimation/glwallpaperservice/GLWrapper;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLWrapper:Lcom/sec/android/samsunganimation/glwallpaperservice/GLWrapper;

    .line 104
    return-void
.end method

.method public setLogFPS(Z)V
    .locals 1
    .param p1, "isLog"    # Z

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0, p1}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->enableLogFPS(Z)V

    .line 203
    :cond_0
    return-void
.end method

.method public setRenderMode(I)V
    .locals 1
    .param p1, "renderMode"    # I

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0, p1}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->setRenderMode(I)V

    .line 161
    return-void
.end method

.method public setRenderer(Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLRenderer;)V
    .locals 6
    .param p1, "renderer"    # Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLRenderer;

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->checkRenderThreadState()V

    .line 116
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mEGLConfigChooser:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;

    if-nez v0, :cond_0

    .line 117
    new-instance v0, Lcom/sec/android/samsunganimation/glwallpaperservice/BaseConfigChooser$SimpleEGLConfigChooser;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sec/android/samsunganimation/glwallpaperservice/BaseConfigChooser$SimpleEGLConfigChooser;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mEGLConfigChooser:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mEGLContextFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLContextFactory;

    if-nez v0, :cond_1

    .line 120
    new-instance v0, Lcom/sec/android/samsunganimation/glwallpaperservice/DefaultContextFactory;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/DefaultContextFactory;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mEGLContextFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLContextFactory;

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mEGLWindowSurfaceFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLWindowSurfaceFactory;

    if-nez v0, :cond_2

    .line 123
    new-instance v0, Lcom/sec/android/samsunganimation/glwallpaperservice/DefaultWindowSurfaceFactory;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/DefaultWindowSurfaceFactory;-><init>()V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mEGLWindowSurfaceFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLWindowSurfaceFactory;

    .line 126
    :cond_2
    invoke-static {}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->initAnimationOnMainThread()V

    .line 128
    new-instance v0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    iget-object v2, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mEGLConfigChooser:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;

    iget-object v3, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mEGLContextFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLContextFactory;

    iget-object v4, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mEGLWindowSurfaceFactory:Lcom/sec/android/samsunganimation/glwallpaperservice/EGLWindowSurfaceFactory;

    iget-object v5, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLWrapper:Lcom/sec/android/samsunganimation/glwallpaperservice/GLWrapper;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;-><init>(Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLRenderer;Lcom/sec/android/samsunganimation/glwallpaperservice/EGLConfigChooser;Lcom/sec/android/samsunganimation/glwallpaperservice/EGLContextFactory;Lcom/sec/android/samsunganimation/glwallpaperservice/EGLWindowSurfaceFactory;Lcom/sec/android/samsunganimation/glwallpaperservice/GLWrapper;)V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    .line 129
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->start()V

    .line 131
    invoke-static {}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->initAnimationOnMainThread()V

    .line 132
    return-void
.end method

.method public setShowFPS(Z)V
    .locals 1
    .param p1, "isShow"    # Z

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0, p1}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->enableShowFPS(Z)V

    .line 199
    :cond_0
    return-void
.end method

.method public swapAnimationSAGLSurface()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLWallpaperService$SAGLEngine;->mGLThread:Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/glwallpaperservice/SAGLThread;->swapAnimationSAGLSurface()V

    .line 191
    :cond_0
    return-void
.end method
