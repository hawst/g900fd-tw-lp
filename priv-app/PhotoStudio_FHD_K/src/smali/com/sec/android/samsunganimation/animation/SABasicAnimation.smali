.class public Lcom/sec/android/samsunganimation/animation/SABasicAnimation;
.super Lcom/sec/android/samsunganimation/animation/SAPropertyAnimation;
.source "SABasicAnimation.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 203
    sget-object v0, Lcom/sec/android/samsunganimation/SamsungAnimationDesc;->mName:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method public constructor <init>(IFF)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "from"    # F
    .param p3, "to"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAPropertyAnimation;-><init>()V

    .line 68
    new-array v0, v3, [F

    aput p2, v0, v2

    .line 69
    .local v0, "fromData":[F
    new-array v1, v3, [F

    aput p3, v1, v2

    .line 71
    .local v1, "toData":[F
    invoke-virtual {p0, p1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->initializeSABasicAnimation(I)V

    .line 72
    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mNativeAnimation:I

    iget v3, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mAnimationType:I

    invoke-static {v2, v3, v0, v1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->nativeSetValueSABasicAnimation(II[F[F)V

    .line 73
    return-void
.end method

.method public constructor <init>(IFFI)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "from"    # F
    .param p3, "to"    # F
    .param p4, "lightType"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 84
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAPropertyAnimation;-><init>()V

    .line 85
    new-array v0, v3, [F

    aput p2, v0, v2

    .line 86
    .local v0, "fromData":[F
    new-array v1, v3, [F

    aput p3, v1, v2

    .line 88
    .local v1, "toData":[F
    iput p4, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mLightType:I

    .line 90
    invoke-virtual {p0, p1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->initializeSABasicAnimation(I)V

    .line 91
    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mNativeAnimation:I

    iget v3, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mAnimationType:I

    invoke-static {v2, v3, v0, v1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->nativeSetValueSABasicAnimation(II[F[F)V

    .line 92
    return-void
.end method

.method public constructor <init>(ILcom/sec/android/samsunganimation/basetype/SAColor;Lcom/sec/android/samsunganimation/basetype/SAColor;)V
    .locals 8
    .param p1, "type"    # I
    .param p2, "from"    # Lcom/sec/android/samsunganimation/basetype/SAColor;
    .param p3, "to"    # Lcom/sec/android/samsunganimation/basetype/SAColor;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 153
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAPropertyAnimation;-><init>()V

    .line 154
    new-array v0, v7, [F

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    aput v2, v0, v3

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    aput v2, v0, v4

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    aput v2, v0, v5

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    aput v2, v0, v6

    .line 155
    .local v0, "fromData":[F
    new-array v1, v7, [F

    iget v2, p3, Lcom/sec/android/samsunganimation/basetype/SAColor;->mR:F

    aput v2, v1, v3

    iget v2, p3, Lcom/sec/android/samsunganimation/basetype/SAColor;->mG:F

    aput v2, v1, v4

    iget v2, p3, Lcom/sec/android/samsunganimation/basetype/SAColor;->mB:F

    aput v2, v1, v5

    iget v2, p3, Lcom/sec/android/samsunganimation/basetype/SAColor;->mA:F

    aput v2, v1, v6

    .line 157
    .local v1, "toData":[F
    invoke-virtual {p0, p1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->initializeSABasicAnimation(I)V

    .line 158
    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mNativeAnimation:I

    iget v3, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mAnimationType:I

    invoke-static {v2, v3, v0, v1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->nativeSetValueSABasicAnimation(II[F[F)V

    .line 159
    return-void
.end method

.method public constructor <init>(ILcom/sec/android/samsunganimation/basetype/SAPoint;Lcom/sec/android/samsunganimation/basetype/SAPoint;)V
    .locals 6
    .param p1, "type"    # I
    .param p2, "from"    # Lcom/sec/android/samsunganimation/basetype/SAPoint;
    .param p3, "to"    # Lcom/sec/android/samsunganimation/basetype/SAPoint;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 102
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAPropertyAnimation;-><init>()V

    .line 103
    new-array v0, v5, [F

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mX:F

    aput v2, v0, v3

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mY:F

    aput v2, v0, v4

    .line 104
    .local v0, "fromData":[F
    new-array v1, v5, [F

    iget v2, p3, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mX:F

    aput v2, v1, v3

    iget v2, p3, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mY:F

    aput v2, v1, v4

    .line 106
    .local v1, "toData":[F
    invoke-virtual {p0, p1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->initializeSABasicAnimation(I)V

    .line 107
    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mNativeAnimation:I

    iget v3, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mAnimationType:I

    invoke-static {v2, v3, v0, v1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->nativeSetValueSABasicAnimation(II[F[F)V

    .line 108
    return-void
.end method

.method public constructor <init>(ILcom/sec/android/samsunganimation/basetype/SAPoint;Lcom/sec/android/samsunganimation/basetype/SAPoint;I)V
    .locals 6
    .param p1, "type"    # I
    .param p2, "from"    # Lcom/sec/android/samsunganimation/basetype/SAPoint;
    .param p3, "to"    # Lcom/sec/android/samsunganimation/basetype/SAPoint;
    .param p4, "lightType"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 119
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAPropertyAnimation;-><init>()V

    .line 120
    new-array v0, v5, [F

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mX:F

    aput v2, v0, v3

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mY:F

    aput v2, v0, v4

    .line 121
    .local v0, "fromData":[F
    new-array v1, v5, [F

    iget v2, p3, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mX:F

    aput v2, v1, v3

    iget v2, p3, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mY:F

    aput v2, v1, v4

    .line 123
    .local v1, "toData":[F
    iput p4, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mLightType:I

    .line 125
    invoke-virtual {p0, p1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->initializeSABasicAnimation(I)V

    .line 126
    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mNativeAnimation:I

    iget v3, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mAnimationType:I

    invoke-static {v2, v3, v0, v1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->nativeSetValueSABasicAnimation(II[F[F)V

    .line 127
    return-void
.end method

.method public constructor <init>(ILcom/sec/android/samsunganimation/basetype/SARect;Lcom/sec/android/samsunganimation/basetype/SARect;)V
    .locals 8
    .param p1, "type"    # I
    .param p2, "from"    # Lcom/sec/android/samsunganimation/basetype/SARect;
    .param p3, "to"    # Lcom/sec/android/samsunganimation/basetype/SARect;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 137
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAPropertyAnimation;-><init>()V

    .line 138
    new-array v0, v7, [F

    iget-object v2, p2, Lcom/sec/android/samsunganimation/basetype/SARect;->mOrigin:Lcom/sec/android/samsunganimation/basetype/SAPoint;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mX:F

    aput v2, v0, v3

    iget-object v2, p2, Lcom/sec/android/samsunganimation/basetype/SARect;->mOrigin:Lcom/sec/android/samsunganimation/basetype/SAPoint;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mY:F

    aput v2, v0, v4

    iget-object v2, p2, Lcom/sec/android/samsunganimation/basetype/SARect;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SASize;->mWidth:F

    aput v2, v0, v5

    iget-object v2, p2, Lcom/sec/android/samsunganimation/basetype/SARect;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SASize;->mHeight:F

    aput v2, v0, v6

    .line 139
    .local v0, "fromData":[F
    new-array v1, v7, [F

    iget-object v2, p3, Lcom/sec/android/samsunganimation/basetype/SARect;->mOrigin:Lcom/sec/android/samsunganimation/basetype/SAPoint;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mX:F

    aput v2, v1, v3

    iget-object v2, p3, Lcom/sec/android/samsunganimation/basetype/SARect;->mOrigin:Lcom/sec/android/samsunganimation/basetype/SAPoint;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SAPoint;->mY:F

    aput v2, v1, v4

    iget-object v2, p3, Lcom/sec/android/samsunganimation/basetype/SARect;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SASize;->mWidth:F

    aput v2, v1, v5

    iget-object v2, p3, Lcom/sec/android/samsunganimation/basetype/SARect;->mSize:Lcom/sec/android/samsunganimation/basetype/SASize;

    iget v2, v2, Lcom/sec/android/samsunganimation/basetype/SASize;->mHeight:F

    aput v2, v1, v6

    .line 141
    .local v1, "toData":[F
    invoke-virtual {p0, p1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->initializeSABasicAnimation(I)V

    .line 142
    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mNativeAnimation:I

    iget v3, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mAnimationType:I

    invoke-static {v2, v3, v0, v1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->nativeSetValueSABasicAnimation(II[F[F)V

    .line 143
    return-void
.end method

.method public constructor <init>(ILcom/sec/android/samsunganimation/basetype/SAVector3;Lcom/sec/android/samsunganimation/basetype/SAVector3;)V
    .locals 7
    .param p1, "type"    # I
    .param p2, "from"    # Lcom/sec/android/samsunganimation/basetype/SAVector3;
    .param p3, "to"    # Lcom/sec/android/samsunganimation/basetype/SAVector3;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 169
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAPropertyAnimation;-><init>()V

    .line 170
    new-array v0, v6, [F

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    aput v2, v0, v3

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    aput v2, v0, v4

    iget v2, p2, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    aput v2, v0, v5

    .line 171
    .local v0, "fromData":[F
    new-array v1, v6, [F

    iget v2, p3, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mX:F

    aput v2, v1, v3

    iget v2, p3, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mY:F

    aput v2, v1, v4

    iget v2, p3, Lcom/sec/android/samsunganimation/basetype/SAVector3;->mZ:F

    aput v2, v1, v5

    .line 173
    .local v1, "toData":[F
    invoke-virtual {p0, p1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->initializeSABasicAnimation(I)V

    .line 174
    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mNativeAnimation:I

    iget v3, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mAnimationType:I

    invoke-static {v2, v3, v0, v1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->nativeSetValueSABasicAnimation(II[F[F)V

    .line 175
    return-void
.end method

.method public constructor <init>(I[F[F)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "from"    # [F
    .param p3, "to"    # [F

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAPropertyAnimation;-><init>()V

    .line 39
    invoke-virtual {p0, p1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->initializeSABasicAnimation(I)V

    .line 40
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mNativeAnimation:I

    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mAnimationType:I

    invoke-static {v0, v1, p2, p3}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->nativeSetValueSABasicAnimation(II[F[F)V

    .line 41
    return-void
.end method

.method public constructor <init>(I[F[FI)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "from"    # [F
    .param p3, "to"    # [F
    .param p4, "lightType"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/samsunganimation/animation/SAPropertyAnimation;-><init>()V

    .line 53
    iput p4, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mLightType:I

    .line 55
    invoke-virtual {p0, p1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->initializeSABasicAnimation(I)V

    .line 56
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mNativeAnimation:I

    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mAnimationType:I

    invoke-static {v0, v1, p2, p3}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->nativeSetValueSABasicAnimation(II[F[F)V

    .line 57
    return-void
.end method

.method private static native nativeCreateSABasicAnimation(I)I
.end method

.method private static native nativeSetValueSABasicAnimation(II[F[F)V
.end method


# virtual methods
.method public finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->deleteNativeAnimationHandle()V

    .line 199
    return-void
.end method

.method public initializeSABasicAnimation(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 184
    const/16 v0, 0x19

    if-ne p1, v0, :cond_0

    .line 185
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mScaleType:I

    .line 186
    :cond_0
    iput p1, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mAnimationType:I

    .line 187
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mClassType:I

    .line 188
    invoke-static {p1}, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->nativeCreateSABasicAnimation(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SABasicAnimation;->mNativeAnimation:I

    .line 189
    return-void
.end method
