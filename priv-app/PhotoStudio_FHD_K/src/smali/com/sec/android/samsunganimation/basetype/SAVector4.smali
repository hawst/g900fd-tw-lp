.class public Lcom/sec/android/samsunganimation/basetype/SAVector4;
.super Ljava/lang/Object;
.source "SAVector4.java"


# instance fields
.field public mW:F

.field public mX:F

.field public mY:F

.field public mZ:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput v0, p0, Lcom/sec/android/samsunganimation/basetype/SAVector4;->mX:F

    .line 45
    iput v0, p0, Lcom/sec/android/samsunganimation/basetype/SAVector4;->mY:F

    .line 46
    iput v0, p0, Lcom/sec/android/samsunganimation/basetype/SAVector4;->mZ:F

    .line 47
    iput v0, p0, Lcom/sec/android/samsunganimation/basetype/SAVector4;->mW:F

    .line 48
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "w"    # F

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput p1, p0, Lcom/sec/android/samsunganimation/basetype/SAVector4;->mX:F

    .line 60
    iput p2, p0, Lcom/sec/android/samsunganimation/basetype/SAVector4;->mY:F

    .line 61
    iput p3, p0, Lcom/sec/android/samsunganimation/basetype/SAVector4;->mZ:F

    .line 62
    iput p4, p0, Lcom/sec/android/samsunganimation/basetype/SAVector4;->mW:F

    .line 63
    return-void
.end method
