.class public Lcom/sec/android/samsunganimation/animation/SAAnimation;
.super Ljava/lang/Object;
.source "SAAnimation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/samsunganimation/animation/SAAnimation$InterpolatorType;,
        Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationClass;,
        Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;
    }
.end annotation


# static fields
.field private static final MSG_EXPLICIT_ANIMATION_END:I = 0x1

.field private static final MSG_EXPLICIT_ANIMATION_REPEAT:I = 0x2

.field private static final MSG_EXPLICIT_ANIMATION_START:I = 0x3

.field private static mExplicitHandler:Landroid/os/Handler;

.field protected static mExplicitProxy:I

.field protected static mImplicitProxy:I

.field private static mListenerCounter:I

.field protected static mListenerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;",
            ">;"
        }
    .end annotation
.end field

.field protected static mTagMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final defaultAutoReverse:Z

.field final defaultDuration:I

.field final defaultOffset:I

.field final defaultRepeatCount:I

.field private mAnimationSet:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/samsunganimation/animation/SAAnimationSet;",
            ">;"
        }
    .end annotation
.end field

.field protected mAutoReverse:Z

.field protected mClassType:I

.field protected mDuration:I

.field protected mInterpolatorType:I

.field protected mLightType:I

.field protected mListener:Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;

.field protected mNativeAnimation:I

.field protected mOffset:I

.field protected mRepeatCount:I

.field protected mScaleType:I

.field protected mTag:Ljava/lang/String;

.field final unlimitedRepeat:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    sput v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mListenerCounter:I

    .line 53
    sput v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mExplicitProxy:I

    .line 54
    sput v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mImplicitProxy:I

    .line 349
    new-instance v0, Lcom/sec/android/samsunganimation/animation/SAAnimation$1;

    invoke-direct {v0}, Lcom/sec/android/samsunganimation/animation/SAAnimation$1;-><init>()V

    sput-object v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mExplicitHandler:Landroid/os/Handler;

    .line 443
    sget-object v0, Lcom/sec/android/samsunganimation/SamsungAnimationDesc;->mName:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/16 v0, 0xfa

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->defaultDuration:I

    .line 34
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->defaultRepeatCount:I

    .line 35
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->unlimitedRepeat:I

    .line 36
    iput-boolean v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->defaultAutoReverse:Z

    .line 37
    iput v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->defaultOffset:I

    .line 124
    sget-object v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mListenerMap:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 125
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mListenerMap:Ljava/util/HashMap;

    .line 128
    :cond_0
    sget-object v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mTagMap:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 129
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mTagMap:Ljava/util/HashMap;

    .line 132
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->initializeSAAnimation()V

    .line 133
    return-void
.end method

.method public static getExplicitAnimationProxy()I
    .locals 1

    .prologue
    .line 328
    sget v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mExplicitProxy:I

    if-nez v0, :cond_0

    .line 329
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->nativeCreteProxySAAnimation(I)I

    move-result v0

    sput v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mExplicitProxy:I

    .line 331
    :cond_0
    sget v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mExplicitProxy:I

    return v0
.end method

.method private static getExplicitHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 381
    sget-object v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mExplicitHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getImplicitAnimationProxy()I
    .locals 1

    .prologue
    .line 340
    sget v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mImplicitProxy:I

    if-nez v0, :cond_0

    .line 341
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->nativeCreteProxySAAnimation(I)I

    move-result v0

    sput v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mImplicitProxy:I

    .line 343
    :cond_0
    sget v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mImplicitProxy:I

    return v0
.end method

.method public static initAnimationOnMainThread()V
    .locals 0

    .prologue
    .line 430
    invoke-static {}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->getExplicitHandler()Landroid/os/Handler;

    .line 431
    return-void
.end method

.method private static native nativeCreteProxySAAnimation(I)I
.end method

.method private static native nativeDeleteProxySAAnimation(I)V
.end method

.method private static native nativeDeleteSAAnimation(II)V
.end method

.method private static native nativeSetAnimationPropertySAAnimation(IIIZI)V
.end method

.method private static native nativeSetInterpolatorSAAnimation(II)V
.end method

.method private static native nativeSetListenerID(II)V
.end method

.method protected static native nativeSetListenerSAAnimation(II)V
.end method

.method public static onSAAnimationEnd(I)V
    .locals 2
    .param p0, "listenerID"    # I

    .prologue
    .line 390
    invoke-static {}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->getExplicitHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 391
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 392
    iput p0, v0, Landroid/os/Message;->arg1:I

    .line 393
    invoke-static {}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->getExplicitHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 394
    return-void
.end method

.method public static onSAAnimationRepeat(I)V
    .locals 2
    .param p0, "listenerID"    # I

    .prologue
    .line 402
    invoke-static {}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->getExplicitHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 403
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 404
    iput p0, v0, Landroid/os/Message;->arg1:I

    .line 405
    invoke-static {}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->getExplicitHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 406
    return-void
.end method

.method public static onSAAnimationStart(I)V
    .locals 2
    .param p0, "listenerID"    # I

    .prologue
    .line 414
    invoke-static {}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->getExplicitHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 415
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    .line 416
    iput p0, v0, Landroid/os/Message;->arg1:I

    .line 417
    invoke-static {}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->getExplicitHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 418
    return-void
.end method


# virtual methods
.method public deleteNativeAnimationHandle()V
    .locals 2

    .prologue
    .line 435
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    if-eqz v0, :cond_0

    .line 436
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mClassType:I

    invoke-static {v0, v1}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->nativeDeleteSAAnimation(II)V

    .line 437
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    .line 439
    :cond_0
    return-void
.end method

.method public finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 155
    const-string v0, "SamsungAnimation"

    const-string v1, "SAAnimation\'s finalize"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    return-void
.end method

.method public getInterpolator()I
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mInterpolatorType:I

    return v0
.end method

.method public getLightType()I
    .locals 1

    .prologue
    .line 288
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mLightType:I

    return v0
.end method

.method public getNativeAnimationHandle()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    return v0
.end method

.method public getScaleType()I
    .locals 1

    .prologue
    .line 297
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mScaleType:I

    return v0
.end method

.method public initializeSAAnimation()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 139
    iput v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    .line 140
    iput v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mInterpolatorType:I

    .line 141
    const/16 v0, 0xfa

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mDuration:I

    .line 142
    iput v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mRepeatCount:I

    .line 143
    iput-boolean v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mAutoReverse:Z

    .line 144
    iput v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mOffset:I

    .line 145
    iput v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mLightType:I

    .line 146
    iput v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mScaleType:I

    .line 147
    iput v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mClassType:I

    .line 148
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mListener:Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;

    .line 149
    return-void
.end method

.method public registerListener()V
    .locals 4

    .prologue
    .line 312
    iget-object v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mListener:Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;

    if-eqz v1, :cond_1

    .line 313
    sget v0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mListenerCounter:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mListenerCounter:I

    .line 314
    .local v0, "id":I
    sget-object v1, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mListenerMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mListener:Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    iget-object v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mTag:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 316
    sget-object v1, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mTagMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mTag:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    :cond_0
    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    invoke-static {v1, v0}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->nativeSetListenerID(II)V

    .line 318
    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    invoke-static {}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->getExplicitAnimationProxy()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->nativeSetListenerSAAnimation(II)V

    .line 320
    .end local v0    # "id":I
    :cond_1
    return-void
.end method

.method public setAnimationProperty(IIZI)V
    .locals 5
    .param p1, "duration"    # I
    .param p2, "repeatCount"    # I
    .param p3, "autoReverse"    # Z
    .param p4, "offset"    # I

    .prologue
    const/4 v0, -0x1

    .line 238
    iput p1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mDuration:I

    .line 239
    if-ne p2, v0, :cond_0

    .line 240
    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mRepeatCount:I

    .line 245
    :goto_0
    iput-boolean p3, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mAutoReverse:Z

    .line 246
    iput p4, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mOffset:I

    .line 247
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mDuration:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mRepeatCount:I

    iget-boolean v3, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mAutoReverse:Z

    iget v4, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mOffset:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->nativeSetAnimationPropertySAAnimation(IIIZI)V

    .line 248
    return-void

    .line 243
    :cond_0
    add-int/lit8 v0, p2, 0x1

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mRepeatCount:I

    goto :goto_0
.end method

.method public setAutoReverse(Z)V
    .locals 5
    .param p1, "autoReverse"    # Z

    .prologue
    .line 213
    iput-boolean p1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mAutoReverse:Z

    .line 214
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mDuration:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mRepeatCount:I

    iget-boolean v3, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mAutoReverse:Z

    iget v4, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mOffset:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->nativeSetAnimationPropertySAAnimation(IIIZI)V

    .line 215
    return-void
.end method

.method public setDuration(I)V
    .locals 5
    .param p1, "duration"    # I

    .prologue
    .line 186
    iput p1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mDuration:I

    .line 187
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mDuration:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mRepeatCount:I

    iget-boolean v3, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mAutoReverse:Z

    iget v4, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mOffset:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->nativeSetAnimationPropertySAAnimation(IIIZI)V

    .line 188
    return-void
.end method

.method public setInterpolator(I)V
    .locals 2
    .param p1, "interpolatorType"    # I

    .prologue
    .line 165
    iput p1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mInterpolatorType:I

    .line 166
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mInterpolatorType:I

    invoke-static {v0, v1}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->nativeSetInterpolatorSAAnimation(II)V

    .line 167
    return-void
.end method

.method public setLightType(I)V
    .locals 0
    .param p1, "lightType"    # I

    .prologue
    .line 278
    iput p1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mLightType:I

    .line 279
    return-void
.end method

.method public setListener(Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mListener:Lcom/sec/android/samsunganimation/animation/SAAnimation$SAAnimationListener;

    .line 260
    return-void
.end method

.method public setOffset(I)V
    .locals 5
    .param p1, "offset"    # I

    .prologue
    .line 224
    iput p1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mOffset:I

    .line 225
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mDuration:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mRepeatCount:I

    iget-boolean v3, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mAutoReverse:Z

    iget v4, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mOffset:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->nativeSetAnimationPropertySAAnimation(IIIZI)V

    .line 226
    return-void
.end method

.method public setRepeatCount(I)V
    .locals 5
    .param p1, "repeatCount"    # I

    .prologue
    const/4 v0, -0x1

    .line 197
    if-ne p1, v0, :cond_0

    .line 198
    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mRepeatCount:I

    .line 203
    :goto_0
    iget v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mNativeAnimation:I

    iget v1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mDuration:I

    iget v2, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mRepeatCount:I

    iget-boolean v3, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mAutoReverse:Z

    iget v4, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mOffset:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->nativeSetAnimationPropertySAAnimation(IIIZI)V

    .line 204
    return-void

    .line 201
    :cond_0
    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mRepeatCount:I

    goto :goto_0
.end method

.method public setTag(Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 305
    iput-object p1, p0, Lcom/sec/android/samsunganimation/animation/SAAnimation;->mTag:Ljava/lang/String;

    .line 306
    return-void
.end method
