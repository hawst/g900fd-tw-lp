.class public Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;
.super Landroid/opengl/GLSurfaceView;
.source "SAGLSurface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$ConfigChooser;,
        Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$ContextFactory;,
        Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$GLRenderer;,
        Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$RenderNotifier;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static TAG:Ljava/lang/String;

.field private static mGLView:Landroid/opengl/GLSurfaceView;

.field private static mRenderNotifier:Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$RenderNotifier;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field protected mPreserveEGLContext:Z

.field private mRenderer:Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$GLRenderer;

.field protected mUseNativeTestBed:Z

.field private mUseUpdateListener:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    const-string v0, "SamsungAnimation"

    sput-object v0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->TAG:Ljava/lang/String;

    .line 49
    sput-object v1, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mRenderNotifier:Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$RenderNotifier;

    .line 462
    sput-object v1, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mGLView:Landroid/opengl/GLSurfaceView;

    .line 471
    sget-object v0, Lcom/sec/android/samsunganimation/SamsungAnimationDesc;->mName:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 61
    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    .line 45
    iput-boolean v2, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mUseNativeTestBed:Z

    .line 46
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mActivity:Landroid/app/Activity;

    .line 47
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mUseUpdateListener:Z

    .line 53
    iput-boolean v2, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mPreserveEGLContext:Z

    move-object v1, p1

    .line 63
    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mActivity:Landroid/app/Activity;

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mUseNativeTestBed:Z

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 67
    .local v0, "assetMgr":Landroid/content/res/AssetManager;
    invoke-static {v0}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativeSetAssetManager(Landroid/content/res/AssetManager;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 77
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    iput-boolean v2, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mUseNativeTestBed:Z

    .line 46
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mActivity:Landroid/app/Activity;

    .line 47
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mUseUpdateListener:Z

    .line 53
    iput-boolean v2, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mPreserveEGLContext:Z

    move-object v1, p1

    .line 79
    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mActivity:Landroid/app/Activity;

    .line 80
    iput-boolean v2, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mUseNativeTestBed:Z

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 83
    .local v0, "assetMgr":Landroid/content/res/AssetManager;
    invoke-static {v0}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativeSetAssetManager(Landroid/content/res/AssetManager;)V

    .line 84
    return-void
.end method

.method static synthetic access$0(I)V
    .locals 0

    .prologue
    .line 548
    invoke-static {p0}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativeRenderSAGLSurface(I)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$2(III)V
    .locals 0

    .prologue
    .line 547
    invoke-static {p0, p1, p2}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativeResizeSAGLSurface(III)V

    return-void
.end method

.method static synthetic access$3(I)V
    .locals 0

    .prologue
    .line 546
    invoke-static {p0}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativeInitSAGLSurface(I)V

    return-void
.end method

.method static synthetic access$4()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Ljava/lang/String;Ljavax/microedition/khronos/egl/EGL10;)V
    .locals 0

    .prologue
    .line 236
    invoke-static {p0, p1}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->checkEglError(Ljava/lang/String;Ljavax/microedition/khronos/egl/EGL10;)V

    return-void
.end method

.method private static checkEglError(Ljava/lang/String;Ljavax/microedition/khronos/egl/EGL10;)V
    .locals 6
    .param p0, "prompt"    # Ljava/lang/String;
    .param p1, "egl"    # Ljavax/microedition/khronos/egl/EGL10;

    .prologue
    .line 238
    :goto_0
    invoke-interface {p1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    .local v0, "error":I
    const/16 v1, 0x3000

    if-ne v0, v1, :cond_0

    .line 241
    return-void

    .line 239
    :cond_0
    sget-object v1, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->TAG:Ljava/lang/String;

    const-string v2, "%s: EGL error: 0x%x"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static native nativeClearBlurCapture()V
.end method

.method private static native nativeClearRemainTexture()V
.end method

.method private static native nativeEnableLogFPS(Z)V
.end method

.method private static native nativeEnableShowFPS(Z)V
.end method

.method private static native nativeEnableTranslucent(Z)V
.end method

.method private static native nativeEnableUpdateListener(Z)V
.end method

.method private static native nativeGetFirstLoadFinished()Z
.end method

.method private static native nativeInitSAGLSurface(I)V
.end method

.method private static native nativePauseSAGLSurface()V
.end method

.method private static native nativeRenderSAGLSurface(I)V
.end method

.method private static native nativeResizeSAGLSurface(III)V
.end method

.method private static native nativeResumeSAGLSurface()V
.end method

.method private static native nativeSetAssetManager(Landroid/content/res/AssetManager;)V
.end method

.method private static native nativeSetBlurCapture(I)V
.end method

.method private static native nativeSwapAnimationSAGLSurface()V
.end method

.method public static nativeUpdate()V
    .locals 1

    .prologue
    .line 535
    sget-object v0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mRenderNotifier:Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$RenderNotifier;

    if-eqz v0, :cond_0

    .line 536
    sget-object v0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mRenderNotifier:Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$RenderNotifier;

    invoke-interface {v0}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$RenderNotifier;->onUpdateFinished()V

    .line 538
    :cond_0
    return-void
.end method

.method static requestRenderView()V
    .locals 1

    .prologue
    .line 455
    sget-object v0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mGLView:Landroid/opengl/GLSurfaceView;

    if-eqz v0, :cond_0

    .line 456
    sget-object v0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mGLView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 458
    :cond_0
    invoke-static {}, Lcom/sec/android/samsunganimation/slide/SASlideManager;->getInstance()Lcom/sec/android/samsunganimation/slide/SASlideManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/samsunganimation/slide/SASlideManager;->animaitonRenderNotify()V

    .line 459
    return-void
.end method


# virtual methods
.method public clearRemainTextures()V
    .locals 0

    .prologue
    .line 510
    invoke-static {}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativeClearRemainTexture()V

    .line 511
    return-void
.end method

.method public enableBlur(ZI)V
    .locals 0
    .param p1, "enable"    # Z
    .param p2, "level"    # I

    .prologue
    .line 481
    if-eqz p1, :cond_0

    .line 482
    invoke-static {p2}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativeSetBlurCapture(I)V

    .line 485
    :goto_0
    return-void

    .line 484
    :cond_0
    invoke-static {}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativeClearBlurCapture()V

    goto :goto_0
.end method

.method public enableLogFPS(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 506
    invoke-static {p1}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativeEnableLogFPS(Z)V

    .line 507
    return-void
.end method

.method public enableShowFPS(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 495
    invoke-static {p1}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativeEnableShowFPS(Z)V

    .line 496
    return-void
.end method

.method public enableTranslucent(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 515
    invoke-static {p1}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativeEnableTranslucent(Z)V

    .line 516
    return-void
.end method

.method public enableUpdateListener(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 529
    iput-boolean p1, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mUseUpdateListener:Z

    .line 530
    invoke-static {p1}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativeEnableUpdateListener(Z)V

    .line 531
    return-void
.end method

.method public initialize(ZII)V
    .locals 8
    .param p1, "translucent"    # Z
    .param p2, "depth"    # I
    .param p3, "stencil"    # I

    .prologue
    const/4 v4, 0x5

    const/4 v7, 0x0

    const/16 v1, 0x8

    .line 94
    new-instance v0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$GLRenderer;

    invoke-direct {v0, p0}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$GLRenderer;-><init>(Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;)V

    iput-object v0, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mRenderer:Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$GLRenderer;

    .line 96
    if-eqz p1, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v2, -0x3

    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 103
    :cond_0
    new-instance v0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$ContextFactory;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$ContextFactory;-><init>(Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$ContextFactory;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->setEGLContextFactory(Landroid/opengl/GLSurfaceView$EGLContextFactory;)V

    .line 111
    if-eqz p1, :cond_1

    .line 112
    new-instance v0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$ConfigChooser;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$ConfigChooser;-><init>(IIIIII)V

    .line 111
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V

    .line 115
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->setDebugFlags(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mRenderer:Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$GLRenderer;

    invoke-virtual {p0, v0}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 118
    invoke-virtual {p0, v7}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->setRenderMode(I)V

    .line 119
    sput-object p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mGLView:Landroid/opengl/GLSurfaceView;

    .line 124
    invoke-virtual {p0, p1}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->enableTranslucent(Z)V

    .line 126
    invoke-static {}, Lcom/sec/android/samsunganimation/animation/SAAnimation;->initAnimationOnMainThread()V

    .line 127
    return-void

    .line 113
    :cond_1
    new-instance v0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$ConfigChooser;

    const/4 v2, 0x6

    move v1, v4

    move v3, v4

    move v4, v7

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$ConfigChooser;-><init>(IIIIII)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 4

    .prologue
    .line 137
    const-string v0, "Thread"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPause :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    invoke-static {}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativePauseSAGLSurface()V

    .line 139
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onPause()V

    .line 140
    return-void
.end method

.method public onResize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 172
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 155
    const-string v0, "Thread"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onResume :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onResume()V

    .line 158
    iget-boolean v0, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mPreserveEGLContext:Z

    if-nez v0, :cond_0

    .line 159
    invoke-static {}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativeResumeSAGLSurface()V

    .line 161
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 184
    const/4 v0, 0x1

    return v0
.end method

.method public setRenderNotifier(Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$RenderNotifier;)V
    .locals 1
    .param p1, "notifier"    # Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$RenderNotifier;

    .prologue
    .line 519
    sget-object v0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mRenderNotifier:Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$RenderNotifier;

    if-eqz v0, :cond_0

    .line 520
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mRenderNotifier:Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$RenderNotifier;

    .line 522
    :cond_0
    sput-object p1, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mRenderNotifier:Lcom/sec/android/samsunganimation/glsurface/SAGLSurface$RenderNotifier;

    .line 523
    return-void
.end method

.method public setSAPreserveEGLContext(Z)V
    .locals 0
    .param p1, "preserveEGLContext"    # Z

    .prologue
    .line 143
    iput-boolean p1, p0, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->mPreserveEGLContext:Z

    .line 144
    return-void
.end method

.method public swapAnimationSAGLSurface()V
    .locals 0

    .prologue
    .line 451
    invoke-static {}, Lcom/sec/android/samsunganimation/glsurface/SAGLSurface;->nativeSwapAnimationSAGLSurface()V

    .line 452
    return-void
.end method
