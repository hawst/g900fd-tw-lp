.class public Lcom/sec/android/pen/util/SettingView;
.super Landroid/widget/FrameLayout;
.source "SettingView.java"


# static fields
.field public static final MODE_CUTTER:I = 0x4

.field public static final MODE_ERASER:I = 0x2

.field public static final MODE_PEN:I = 0x1

.field public static final MODE_SELECTION:I = 0x5

.field public static final MODE_TEXT:I = 0x3


# instance fields
.field private mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

.field private mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

.field private mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

.field private mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

.field private mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

.field private mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

.field private mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "canvasLayout"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 70
    invoke-static {}, Lcom/sec/android/pen/util/SettingView;->createImagePath()Ljava/lang/String;

    move-result-object v0

    .line 71
    invoke-static {}, Lcom/sec/android/pen/util/SettingView;->createFontName()Ljava/util/HashMap;

    move-result-object v1

    .line 70
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/sec/android/pen/util/SettingView;->initSettingView(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "canvasLayout"    # Landroid/widget/RelativeLayout;
    .param p3, "customImagePath"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 87
    invoke-static {}, Lcom/sec/android/pen/util/SettingView;->createFontName()Ljava/util/HashMap;

    move-result-object v0

    .line 86
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/pen/util/SettingView;->initSettingView(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "canvasLayout"    # Landroid/widget/RelativeLayout;
    .param p3, "customImagePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/RelativeLayout;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p4, "fontName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 57
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/pen/util/SettingView;->initSettingView(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/util/HashMap;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "canvasLayout"    # Landroid/widget/RelativeLayout;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/RelativeLayout;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 100
    .local p3, "fontName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 102
    invoke-static {}, Lcom/sec/android/pen/util/SettingView;->createImagePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/sec/android/pen/util/SettingView;->initSettingView(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 103
    return-void
.end method

.method private static createFontName()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 107
    .local v0, "custFontName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    return-object v0
.end method

.method private static createImagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 112
    .local v0, "custImagePath":Ljava/lang/String;
    return-object v0
.end method

.method private initSettingView(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "canvasLayout"    # Landroid/widget/RelativeLayout;
    .param p3, "customImagePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/RelativeLayout;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 125
    .local p4, "fontName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050299

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 127
    .local v7, "padding":I
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    .line 128
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p1, p3, p2, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    .line 127
    iput-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    .line 129
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    .line 130
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p1, p3, p2, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    .line 129
    iput-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    .line 131
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    .line 132
    const/high16 v5, 0x3f800000    # 1.0f

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashMap;Landroid/widget/RelativeLayout;F)V

    .line 131
    iput-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    .line 133
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    .line 134
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p1, p3, p2, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    .line 133
    iput-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    .line 135
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    .line 136
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, p1, p3, p2, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    .line 135
    iput-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    .line 138
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isNewSpenJarSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v7, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setPadding(IIII)V

    .line 141
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iput-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 142
    new-instance v8, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v8}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    .line 143
    .local v8, "penEraser":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    new-instance v10, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v10}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    .line 144
    .local v10, "textEraser":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, v8, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 145
    const/4 v0, 0x0

    iput v0, v8, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 146
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, v10, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 147
    const/4 v0, 0x1

    iput v0, v10, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 148
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v1, 0x0

    aput-object v8, v0, v1

    .line 149
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v1, 0x1

    aput-object v10, v0, v1

    .line 150
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setRemoverInfoList([Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 152
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iput-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 153
    new-instance v6, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v6}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    .line 154
    .local v6, "cutCutter":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    new-instance v9, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v9}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    .line 155
    .local v9, "removeCutter":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, v6, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 156
    const/4 v0, 0x0

    iput v0, v6, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 157
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, v9, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 158
    const/4 v0, 0x1

    iput v0, v9, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 159
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v1, 0x0

    aput-object v6, v0, v1

    .line 160
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/4 v1, 0x1

    aput-object v9, v0, v1

    .line 161
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setRemoverInfoList([Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/pen/util/SettingView;->addView(Landroid/view/View;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/pen/util/SettingView;->addView(Landroid/view/View;)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/pen/util/SettingView;->addView(Landroid/view/View;)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/pen/util/SettingView;->addView(Landroid/view/View;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/pen/util/SettingView;->addView(Landroid/view/View;)V

    .line 169
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    if-eqz v0, :cond_0

    .line 524
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->close()V

    .line 526
    :cond_0
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    if-eqz v0, :cond_1

    .line 527
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->close()V

    .line 529
    :cond_1
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    if-eqz v0, :cond_2

    .line 530
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->close()V

    .line 533
    :cond_2
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    if-eqz v0, :cond_3

    .line 534
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->close()V

    .line 537
    :cond_3
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    if-eqz v0, :cond_4

    .line 538
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->close()V

    .line 540
    :cond_4
    return-void
.end method

.method public closeSettingView()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 344
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 345
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setVisibility(I)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setVisibility(I)V

    .line 347
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 348
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->setVisibility(I)V

    .line 349
    return-void
.end method

.method public getCutterInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v0

    return-object v0
.end method

.method public getEraserInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v0

    return-object v0
.end method

.method public getSelectionInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->getInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    move-result-object v0

    return-object v0
.end method

.method public getSpenPenInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v0

    return-object v0
.end method

.method public getSpenSettingEraserLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    return-object v0
.end method

.method public getSpenSettingPenLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    return-object v0
.end method

.method public getSpenSettingRemoverLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    return-object v0
.end method

.method public getSpenSettingSelectionLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    return-object v0
.end method

.method public getSpenSettingTextLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    return-object v0
.end method

.method public getTextInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    return-object v0
.end method

.method public getViewMode(I)I
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 353
    const/4 v0, -0x1

    .line 354
    .local v0, "viewMode":I
    packed-switch p1, :pswitch_data_0

    .line 377
    const/4 v0, -0x1

    .line 380
    :goto_0
    return v0

    .line 357
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getViewMode()I

    move-result v0

    .line 358
    goto :goto_0

    .line 361
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getViewMode()I

    move-result v0

    .line 362
    goto :goto_0

    .line 365
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getViewMode()I

    move-result v0

    .line 366
    goto :goto_0

    .line 369
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getViewMode()I

    move-result v0

    .line 370
    goto :goto_0

    .line 373
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->getViewMode()I

    move-result v0

    .line 374
    goto :goto_0

    .line 354
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public isSettingViewVisible(I)Z
    .locals 1
    .param p1, "nWhichSettingView"    # I

    .prologue
    .line 181
    packed-switch p1, :pswitch_data_0

    .line 198
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 183
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isShown()Z

    move-result v0

    goto :goto_0

    .line 186
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->isShown()Z

    move-result v0

    goto :goto_0

    .line 189
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isShown()Z

    move-result v0

    goto :goto_0

    .line 192
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->isShown()Z

    move-result v0

    goto :goto_0

    .line 195
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->isShown()Z

    move-result v0

    goto :goto_0

    .line 181
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public isShown()Z
    .locals 2

    .prologue
    .line 327
    const/4 v0, 0x0

    .line 328
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isShown()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 329
    const/4 v0, 0x1

    .line 340
    :cond_0
    :goto_0
    return v0

    .line 330
    :cond_1
    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->isShown()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 331
    const/4 v0, 0x1

    .line 332
    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->isShown()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 333
    const/4 v0, 0x1

    .line 334
    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->isShown()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 335
    const/4 v0, 0x1

    .line 336
    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->isShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 337
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 205
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 207
    return-void
.end method

.method public setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
    .locals 1
    .param p1, "canvasView"    # Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    .line 212
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    .line 213
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    .line 214
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    .line 216
    return-void
.end method

.method public setCutterInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 2
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 416
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    aput-object p1, v0, v1

    .line 417
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 418
    return-void
.end method

.method public setCutterInfoList(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 2
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 453
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    aput-object p1, v0, v1

    .line 454
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setRemoverInfoList([Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 455
    return-void
.end method

.method public setCutterInfoList([Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 2
    .param p1, "settingInfoList"    # [Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 443
    iput-object p1, p0, Lcom/sec/android/pen/util/SettingView;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 444
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setRemoverInfoList([Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 445
    return-void
.end method

.method public setCutterListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .prologue
    .line 519
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setRemoverListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;)V

    .line 520
    return-void
.end method

.method public setEraserInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 1
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 397
    return-void
.end method

.method public setEraserInfoList(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 2
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 448
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    aput-object p1, v0, v1

    .line 449
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setRemoverInfoList([Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 450
    return-void
.end method

.method public setEraserInfoList([Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 2
    .param p1, "settingInfoList"    # [Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 438
    iput-object p1, p0, Lcom/sec/android/pen/util/SettingView;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 439
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v1, p0, Lcom/sec/android/pen/util/SettingView;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setRemoverInfoList([Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 440
    return-void
.end method

.method public setEraserListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .prologue
    .line 513
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setRemoverListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;)V

    .line 514
    return-void
.end method

.method public setSelectionInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 1
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    .prologue
    .line 427
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V

    .line 429
    return-void
.end method

.method public setSpenPenInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 1
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    .line 384
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 386
    return-void
.end method

.method public setSpenPenInfoList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 459
    .local p1, "mSpenSettingPenInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;>;"
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setPenInfoList(Ljava/util/List;)V

    .line 460
    return-void
.end method

.method public setTextInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 1
    .param p1, "settingInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    .prologue
    .line 406
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 407
    return-void
.end method

.method public setViewMode(II)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "viewMode"    # I

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 277
    packed-switch p1, :pswitch_data_0

    .line 323
    :goto_0
    return-void

    .line 279
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setVisibility(I)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setVisibility(I)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->setVisibility(I)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setViewMode(I)V

    goto :goto_0

    .line 288
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setVisibility(I)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setVisibility(I)V

    .line 291
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setViewMode(I)V

    goto :goto_0

    .line 297
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setVisibility(I)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->setVisibility(I)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setViewMode(I)V

    goto :goto_0

    .line 306
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setVisibility(I)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setVisibility(I)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->setVisibility(I)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setViewMode(I)V

    goto :goto_0

    .line 315
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setVisibility(I)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->setViewMode(I)V

    goto/16 :goto_0

    .line 277
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public showSettingView(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 232
    packed-switch p1, :pswitch_data_0

    .line 273
    :goto_0
    return-void

    .line 234
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setVisibility(I)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->setVisibility(I)V

    goto :goto_0

    .line 242
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setVisibility(I)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->setVisibility(I)V

    goto :goto_0

    .line 250
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setVisibility(I)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->setVisibility(I)V

    goto :goto_0

    .line 258
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setVisibility(I)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->setVisibility(I)V

    goto :goto_0

    .line 266
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mEraserSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mCutterSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setVisibility(I)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setVisibility(I)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/pen/util/SettingView;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 232
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public toggleShowSettingView(II)Z
    .locals 2
    .param p1, "type"    # I
    .param p2, "viewMode"    # I

    .prologue
    const/4 v0, 0x1

    .line 219
    if-lt p1, v0, :cond_0

    const/4 v1, 0x5

    if-le p1, v1, :cond_1

    .line 220
    :cond_0
    const/4 v0, 0x0

    .line 227
    :goto_0
    return v0

    .line 222
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/pen/util/SettingView;->isSettingViewVisible(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 223
    invoke-virtual {p0}, Lcom/sec/android/pen/util/SettingView;->closeSettingView()V

    goto :goto_0

    .line 225
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/pen/util/SettingView;->setViewMode(II)V

    goto :goto_0
.end method
