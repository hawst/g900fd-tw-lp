.class public Lcom/sec/android/pen/util/SPenSDKUtils;
.super Ljava/lang/Object;
.source "SPenSDKUtils.java"


# static fields
.field public static final INTEGER_KEYNAME:Ljava/lang/String; = "Integer Key"

.field public static final STRING_KEYNAME:Ljava/lang/String; = "String Key"

.field public static final USER_FONT_PATH1:Ljava/lang/String; = "fontpath1"

.field public static final USER_FONT_PATH2:Ljava/lang/String; = "fontpath2"

.field public static final USER_FONT_PATH3:Ljava/lang/String; = "fontpath3"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static alertActivityFinish(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 529
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 530
    .local v0, "ad":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 531
    const v2, 0x1080027

    .line 530
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    .line 533
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f060000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 534
    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 535
    const-string v2, "OK"

    new-instance v3, Lcom/sec/android/pen/util/SPenSDKUtils$1;

    invoke-direct {v3, p0}, Lcom/sec/android/pen/util/SPenSDKUtils$1;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 543
    const-string v2, "Cancel"

    .line 544
    new-instance v3, Lcom/sec/android/pen/util/SPenSDKUtils$2;

    invoke-direct {v3}, Lcom/sec/android/pen/util/SPenSDKUtils$2;-><init>()V

    .line 543
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 550
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 551
    const/4 v0, 0x0

    .line 552
    return-void
.end method

.method public static alertExceptionFinish(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 555
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 556
    .local v0, "ad":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 557
    const v2, 0x1080027

    .line 556
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    .line 559
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f060000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 560
    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 561
    const-string v2, "OK"

    new-instance v3, Lcom/sec/android/pen/util/SPenSDKUtils$3;

    invoke-direct {v3, p0}, Lcom/sec/android/pen/util/SPenSDKUtils$3;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 568
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 569
    const/4 v0, 0x0

    .line 570
    return-void
.end method

.method public static convertAbsolute(Lcom/samsung/android/sdk/pen/engine/SpenView;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 5
    .param p0, "view"    # Lcom/samsung/android/sdk/pen/engine/SpenView;
    .param p1, "srcRect"    # Landroid/graphics/RectF;

    .prologue
    .line 623
    move-object v0, p1

    .line 624
    .local v0, "dstRect":Landroid/graphics/RectF;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenView;->getPan()Landroid/graphics/PointF;

    move-result-object v1

    .line 625
    .local v1, "point":Landroid/graphics/PointF;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenView;->getZoomRatio()F

    move-result v2

    .line 627
    .local v2, "ratio":F
    iget v3, v0, Landroid/graphics/RectF;->left:F

    div-float/2addr v3, v2

    iget v4, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->left:F

    .line 628
    iget v3, v0, Landroid/graphics/RectF;->right:F

    div-float/2addr v3, v2

    iget v4, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->right:F

    .line 629
    iget v3, v0, Landroid/graphics/RectF;->top:F

    div-float/2addr v3, v2

    iget v4, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->top:F

    .line 630
    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    div-float/2addr v3, v2

    iget v4, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 632
    return-object v0
.end method

.method public static decodeImageFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;Z)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "strImagePath"    # Ljava/lang/String;
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "checkOrientation"    # Z

    .prologue
    .line 370
    if-nez p2, :cond_0

    .line 371
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 375
    :goto_0
    return-object v2

    .line 373
    :cond_0
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 374
    .local v0, "bm":Landroid/graphics/Bitmap;
    invoke-static {p0}, Lcom/sec/android/pen/util/SPenSDKUtils;->getExifDegree(Ljava/lang/String;)I

    move-result v1

    .line 375
    .local v1, "degree":I
    invoke-static {v0, v1}, Lcom/sec/android/pen/util/SPenSDKUtils;->getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method public static getBitmapSize(Landroid/content/res/Resources;I)Landroid/graphics/BitmapFactory$Options;
    .locals 2
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "id"    # I

    .prologue
    .line 395
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 396
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 398
    invoke-static {p0, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 400
    return-object v0
.end method

.method public static getBitmapSize(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;
    .locals 2
    .param p0, "strImagePath"    # Ljava/lang/String;

    .prologue
    .line 383
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 384
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 386
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 388
    return-object v0
.end method

.method public static getBitmapSize(Ljava/lang/String;Z)Landroid/graphics/BitmapFactory$Options;
    .locals 4
    .param p0, "strImagePath"    # Ljava/lang/String;
    .param p1, "checkOrientation"    # Z

    .prologue
    .line 408
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 409
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 411
    invoke-static {p0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 413
    if-eqz p1, :cond_1

    .line 414
    invoke-static {p0}, Lcom/sec/android/pen/util/SPenSDKUtils;->getExifDegree(Ljava/lang/String;)I

    move-result v0

    .line 415
    .local v0, "degree":I
    const/16 v3, 0x5a

    if-eq v0, v3, :cond_0

    const/16 v3, 0x10e

    if-ne v0, v3, :cond_1

    .line 416
    :cond_0
    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 417
    .local v2, "temp":I
    iget v3, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v3, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 418
    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 422
    .end local v0    # "degree":I
    .end local v2    # "temp":I
    :cond_1
    return-object v1
.end method

.method public static getExifDegree(Ljava/lang/String;)I
    .locals 6
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v5, -0x1

    .line 461
    const/4 v0, 0x0

    .line 464
    .local v0, "degree":I
    :try_start_0
    new-instance v2, Landroid/media/ExifInterface;

    invoke-direct {v2, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 470
    .local v2, "exif":Landroid/media/ExifInterface;
    if-eqz v2, :cond_0

    .line 472
    const-string v4, "Orientation"

    .line 471
    invoke-virtual {v2, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v3

    .line 474
    .local v3, "orientation":I
    if-eq v3, v5, :cond_0

    .line 475
    packed-switch v3, :pswitch_data_0

    .end local v3    # "orientation":I
    :cond_0
    :goto_0
    :pswitch_0
    move v4, v0

    .line 488
    .end local v2    # "exif":Landroid/media/ExifInterface;
    :goto_1
    return v4

    .line 465
    :catch_0
    move-exception v1

    .line 466
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 467
    const/4 v4, 0x0

    goto :goto_1

    .line 477
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    .restart local v3    # "orientation":I
    :pswitch_1
    const/16 v0, 0x5a

    .line 478
    goto :goto_0

    .line 480
    :pswitch_2
    const/16 v0, 0xb4

    .line 481
    goto :goto_0

    .line 483
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    .line 475
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getRealPathFromURI(Landroid/app/Activity;Landroid/net/Uri;)Ljava/lang/String;
    .locals 12
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 52
    sget-object v9, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 54
    .local v9, "releaseNumber":Ljava/lang/String;
    if-eqz v9, :cond_9

    .line 56
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x34

    if-ne v1, v2, :cond_4

    .line 58
    new-array v3, v6, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v3, v5

    .line 59
    .local v3, "proj":[Ljava/lang/String;
    const-string v10, ""

    .line 60
    .local v10, "strFileName":Ljava/lang/String;
    new-instance v0, Landroid/content/CursorLoader;

    move-object v1, p0

    move-object v2, p1

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .local v0, "cursorLoader":Landroid/content/CursorLoader;
    invoke-virtual {v0}, Landroid/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v8

    .line 63
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 65
    const-string v1, "_data"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 66
    .local v7, "column_index":I
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 67
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 68
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 70
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 73
    .end local v7    # "column_index":I
    :cond_1
    if-eqz v10, :cond_2

    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 74
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "file"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 75
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    :cond_3
    move-object v11, v10

    .line 127
    .end local v0    # "cursorLoader":Landroid/content/CursorLoader;
    .end local v10    # "strFileName":Ljava/lang/String;
    .local v11, "strFileName":Ljava/lang/String;
    :goto_0
    return-object v11

    .line 80
    .end local v3    # "proj":[Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v11    # "strFileName":Ljava/lang/String;
    :cond_4
    const-string v1, "2.3"

    invoke-virtual {v9, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 81
    new-array v3, v6, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v3, v5

    .line 82
    .restart local v3    # "proj":[Ljava/lang/String;
    const-string v10, ""

    .restart local v10    # "strFileName":Ljava/lang/String;
    move-object v1, p0

    move-object v2, p1

    move-object v5, v4

    move-object v6, v4

    .line 83
    invoke-virtual/range {v1 .. v6}, Landroid/app/Activity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 85
    .restart local v8    # "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_6

    .line 87
    const-string v1, "_data"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 89
    .restart local v7    # "column_index":I
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 90
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_5

    .line 91
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 93
    :cond_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 96
    .end local v7    # "column_index":I
    :cond_6
    if-eqz v10, :cond_7

    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 97
    :cond_7
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "file"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_8

    .line 98
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    :cond_8
    move-object v11, v10

    .line 100
    .end local v10    # "strFileName":Ljava/lang/String;
    .restart local v11    # "strFileName":Ljava/lang/String;
    goto :goto_0

    .line 108
    .end local v3    # "proj":[Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v11    # "strFileName":Ljava/lang/String;
    :cond_9
    new-array v3, v6, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v3, v5

    .line 109
    .restart local v3    # "proj":[Ljava/lang/String;
    const-string v10, ""

    .restart local v10    # "strFileName":Ljava/lang/String;
    move-object v1, p0

    move-object v2, p1

    move-object v5, v4

    move-object v6, v4

    .line 110
    invoke-virtual/range {v1 .. v6}, Landroid/app/Activity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 112
    .restart local v8    # "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_b

    .line 114
    const-string v1, "_data"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 117
    .restart local v7    # "column_index":I
    invoke-virtual {p0, v8}, Landroid/app/Activity;->startManagingCursor(Landroid/database/Cursor;)V

    .line 119
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 120
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_a

    .line 121
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 125
    :cond_a
    invoke-virtual {p0, v8}, Landroid/app/Activity;->stopManagingCursor(Landroid/database/Cursor;)V

    .end local v7    # "column_index":I
    :cond_b
    move-object v11, v10

    .line 127
    .end local v10    # "strFileName":Ljava/lang/String;
    .restart local v11    # "strFileName":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "degrees"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 492
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 493
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 494
    .local v5, "m":Landroid/graphics/Matrix;
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    .line 495
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    .line 494
    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 497
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 498
    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    .line 497
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 499
    .local v7, "b2":Landroid/graphics/Bitmap;
    invoke-virtual {p0, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 501
    move-object p0, v7

    .line 507
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v7    # "b2":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p0

    .line 503
    .restart local v5    # "m":Landroid/graphics/Matrix;
    :catch_0
    move-exception v8

    .line 504
    .local v8, "ex":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v8}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method public static getSafeResizingBitmap(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "strImagePath"    # Ljava/lang/String;
    .param p1, "nMaxResizedWidth"    # I
    .param p2, "nMaxResizedHeight"    # I
    .param p3, "checkOrientation"    # Z

    .prologue
    const/16 v8, 0x10e

    const/16 v7, 0x5a

    const/4 v6, 0x0

    .line 331
    invoke-static {p0}, Lcom/sec/android/pen/util/SPenSDKUtils;->getBitmapSize(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v2

    .line 332
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    if-nez v2, :cond_1

    .line 333
    const/4 v3, 0x0

    .line 365
    :cond_0
    :goto_0
    return-object v3

    .line 339
    :cond_1
    const/4 v0, 0x0

    .line 340
    .local v0, "degree":I
    if-eqz p3, :cond_2

    .line 341
    invoke-static {p0}, Lcom/sec/android/pen/util/SPenSDKUtils;->getExifDegree(Ljava/lang/String;)I

    move-result v0

    .line 344
    :cond_2
    if-eq v0, v7, :cond_3

    if-ne v0, v8, :cond_5

    .line 345
    :cond_3
    iget v4, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 346
    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 345
    invoke-static {v4, v5, p1, p2}, Lcom/sec/android/pen/util/SPenSDKUtils;->getSafeResizingSampleSize(IIII)I

    move-result v1

    .line 355
    .local v1, "nSampleSize":I
    :goto_1
    iput-boolean v6, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 356
    iput v1, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 357
    iput-boolean v6, v2, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 358
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v4, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 359
    const/4 v4, 0x1

    iput-boolean v4, v2, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 361
    invoke-static {p0, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 362
    .local v3, "photo":Landroid/graphics/Bitmap;
    if-eqz p3, :cond_0

    if-eq v0, v7, :cond_4

    if-ne v0, v8, :cond_0

    .line 363
    :cond_4
    invoke-static {v3, v0}, Lcom/sec/android/pen/util/SPenSDKUtils;->getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0

    .line 348
    .end local v1    # "nSampleSize":I
    .end local v3    # "photo":Landroid/graphics/Bitmap;
    :cond_5
    iget v4, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 349
    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 348
    invoke-static {v4, v5, p1, p2}, Lcom/sec/android/pen/util/SPenSDKUtils;->getSafeResizingSampleSize(IIII)I

    move-result v1

    .restart local v1    # "nSampleSize":I
    goto :goto_1
.end method

.method public static getSafeResizingSampleSize(IIII)I
    .locals 6
    .param p0, "nOrgWidth"    # I
    .param p1, "nOrgHeight"    # I
    .param p2, "nMaxWidth"    # I
    .param p3, "nMaxHeight"    # I

    .prologue
    .line 438
    const/4 v3, 0x1

    .line 440
    .local v3, "size":I
    const/4 v1, 0x0

    .line 441
    .local v1, "fWidthScale":F
    const/4 v0, 0x0

    .line 443
    .local v0, "fHeightScale":F
    if-gt p0, p2, :cond_0

    if-le p1, p3, :cond_3

    .line 444
    :cond_0
    if-le p0, p2, :cond_1

    .line 445
    int-to-float v4, p0

    int-to-float v5, p2

    div-float v1, v4, v5

    .line 446
    :cond_1
    if-le p1, p3, :cond_2

    .line 447
    int-to-float v4, p1

    int-to-float v5, p3

    div-float v0, v4, v5

    .line 449
    :cond_2
    cmpl-float v4, v1, v0

    if-ltz v4, :cond_4

    .line 450
    move v2, v1

    .line 454
    .local v2, "fsize":F
    :goto_0
    float-to-int v3, v2

    .line 457
    .end local v2    # "fsize":F
    :cond_3
    return v3

    .line 452
    :cond_4
    move v2, v0

    .restart local v2    # "fsize":F
    goto :goto_0
.end method

.method public static getSettingLayoutStringResourceMap(ZZZZ)Ljava/util/HashMap;
    .locals 3
    .param p0, "bUsePenSetting"    # Z
    .param p1, "bUseEraserSetting"    # Z
    .param p2, "bUseTextSetting"    # Z
    .param p3, "bUseFillingSetting"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZZ)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 513
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 514
    .local v0, "settingResourcesString":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 516
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "settingResourcesString":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 518
    .restart local v0    # "settingResourcesString":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "fontpath1"

    const-string v2, "fonts/malgun.ttf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 519
    const-string v1, "fontpath2"

    const-string v2, "fonts/nanum.ttf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 520
    const-string v1, "fontpath3"

    const-string v2, "fonts/sandol.ttf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    const-string v1, "R.string.sdk_resource_path"

    .line 522
    const-string v2, "spen_sdk_resource_custom"

    .line 521
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 525
    :cond_0
    return-object v0
.end method

.method public static isValidImagePath(Ljava/lang/String;)Z
    .locals 4
    .param p0, "strImagePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 292
    if-nez p0, :cond_1

    .line 299
    :cond_0
    :goto_0
    return v1

    .line 295
    :cond_1
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 296
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 297
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 299
    iget-object v3, v0, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public static isValidSaveName(Ljava/lang/String;)Z
    .locals 4
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 305
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 306
    .local v2, "len":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 315
    const/4 v3, 0x1

    :goto_1
    return v3

    .line 307
    :cond_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 309
    .local v0, "c":C
    const/16 v3, 0x5c

    if-eq v0, v3, :cond_1

    const/16 v3, 0x3a

    if-eq v0, v3, :cond_1

    const/16 v3, 0x2f

    if-eq v0, v3, :cond_1

    const/16 v3, 0x2a

    if-eq v0, v3, :cond_1

    const/16 v3, 0x3f

    if-eq v0, v3, :cond_1

    .line 310
    const/16 v3, 0x22

    if-eq v0, v3, :cond_1

    const/16 v3, 0x3c

    if-eq v0, v3, :cond_1

    const/16 v3, 0x3e

    if-eq v0, v3, :cond_1

    const/16 v3, 0x7c

    if-eq v0, v3, :cond_1

    .line 311
    const/16 v3, 0x9

    if-eq v0, v3, :cond_1

    const/16 v3, 0xa

    if-ne v0, v3, :cond_2

    .line 312
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 306
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static saveBitmapJPEGWithBackgroundColor(Ljava/lang/String;Landroid/graphics/Bitmap;II)Z
    .locals 17
    .param p0, "strFileName"    # Ljava/lang/String;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "nQuality"    # I
    .param p3, "nBackgroundColor"    # I

    .prologue
    .line 145
    const/4 v1, 0x0

    .line 146
    .local v1, "bSuccess1":Z
    const/4 v2, 0x0

    .line 148
    .local v2, "bSuccess2":Z
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 150
    .local v12, "saveFile":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 151
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    move-result v13

    if-nez v13, :cond_0

    .line 152
    const/4 v13, 0x0

    .line 210
    :goto_0
    return v13

    .line 155
    :cond_0
    shr-int/lit8 v13, p3, 0x18

    and-int/lit16 v7, v13, 0xff

    .line 158
    .local v7, "nA":I
    if-nez v7, :cond_1

    .line 159
    const/16 p3, -0x1

    .line 161
    :cond_1
    new-instance v11, Landroid/graphics/Rect;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    move/from16 v0, v16

    invoke-direct {v11, v13, v14, v15, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 162
    .local v11, "rect":Landroid/graphics/Rect;
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    .line 163
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    sget-object v15, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 162
    invoke-static {v13, v14, v15}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 164
    .local v8, "newBitmap":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 165
    .local v4, "canvas":Landroid/graphics/Canvas;
    move/from16 v0, p3

    invoke-virtual {v4, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 166
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v11, v11, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 169
    const/16 v13, 0xa

    move/from16 v0, p2

    if-ge v0, v13, :cond_4

    .line 170
    const/16 p2, 0xa

    .line 174
    :cond_2
    :goto_1
    const/4 v9, 0x0

    .line 177
    .local v9, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {v12}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 183
    :goto_2
    :try_start_1
    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 184
    .end local v9    # "out":Ljava/io/OutputStream;
    .local v10, "out":Ljava/io/OutputStream;
    :try_start_2
    sget-object v13, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    move/from16 v0, p2

    invoke-virtual {v8, v13, v0, v10}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    move-result v2

    move-object v9, v10

    .line 190
    .end local v10    # "out":Ljava/io/OutputStream;
    .restart local v9    # "out":Ljava/io/OutputStream;
    :goto_3
    if-eqz v9, :cond_5

    .line 191
    :try_start_3
    invoke-virtual {v9}, Ljava/io/OutputStream;->flush()V

    .line 192
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 193
    const/4 v3, 0x1

    .line 201
    .local v3, "bSuccess3":Z
    :goto_4
    if-eqz v9, :cond_3

    .line 203
    :try_start_4
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 210
    :cond_3
    :goto_5
    if-eqz v1, :cond_7

    if-eqz v2, :cond_7

    if-eqz v3, :cond_7

    const/4 v13, 0x1

    goto :goto_0

    .line 171
    .end local v3    # "bSuccess3":Z
    .end local v9    # "out":Ljava/io/OutputStream;
    :cond_4
    const/16 v13, 0x64

    move/from16 v0, p2

    if-le v0, v13, :cond_2

    .line 172
    const/16 p2, 0x64

    goto :goto_1

    .line 178
    .restart local v9    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v6

    .line 179
    .local v6, "e1":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 185
    .end local v6    # "e1":Ljava/io/IOException;
    :catch_1
    move-exception v5

    .line 186
    .local v5, "e":Ljava/lang/Exception;
    :goto_6
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 195
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_5
    const/4 v3, 0x0

    .restart local v3    # "bSuccess3":Z
    goto :goto_4

    .line 197
    .end local v3    # "bSuccess3":Z
    :catch_2
    move-exception v5

    .line 198
    .local v5, "e":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 199
    const/4 v3, 0x0

    .line 201
    .restart local v3    # "bSuccess3":Z
    if-eqz v9, :cond_3

    .line 203
    :try_start_6
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_5

    .line 204
    :catch_3
    move-exception v5

    .line 205
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 200
    .end local v3    # "bSuccess3":Z
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v13

    .line 201
    if-eqz v9, :cond_6

    .line 203
    :try_start_7
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 208
    :cond_6
    :goto_7
    throw v13

    .line 204
    :catch_4
    move-exception v5

    .line 205
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 204
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v3    # "bSuccess3":Z
    :catch_5
    move-exception v5

    .line 205
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 210
    .end local v5    # "e":Ljava/io/IOException;
    :cond_7
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 185
    .end local v3    # "bSuccess3":Z
    .end local v9    # "out":Ljava/io/OutputStream;
    .restart local v10    # "out":Ljava/io/OutputStream;
    :catch_6
    move-exception v5

    move-object v9, v10

    .end local v10    # "out":Ljava/io/OutputStream;
    .restart local v9    # "out":Ljava/io/OutputStream;
    goto :goto_6
.end method

.method public static saveBitmapPNGWithBackgroundColor(Ljava/lang/String;Landroid/graphics/Bitmap;I)Z
    .locals 17
    .param p0, "strFileName"    # Ljava/lang/String;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "nBackgroundColor"    # I

    .prologue
    .line 228
    const/4 v1, 0x0

    .line 229
    .local v1, "bSuccess1":Z
    const/4 v2, 0x0

    .line 231
    .local v2, "bSuccess2":Z
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 233
    .local v12, "saveFile":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 234
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    move-result v13

    if-nez v13, :cond_0

    .line 235
    const/4 v13, 0x0

    .line 287
    :goto_0
    return v13

    .line 238
    :cond_0
    shr-int/lit8 v13, p2, 0x18

    and-int/lit16 v7, v13, 0xff

    .line 241
    .local v7, "nA":I
    if-nez v7, :cond_1

    .line 242
    const/16 p2, -0x1

    .line 244
    :cond_1
    new-instance v11, Landroid/graphics/Rect;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    move/from16 v0, v16

    invoke-direct {v11, v13, v14, v15, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 245
    .local v11, "rect":Landroid/graphics/Rect;
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    .line 246
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    sget-object v15, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 245
    invoke-static {v13, v14, v15}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 247
    .local v8, "newBitmap":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 248
    .local v4, "canvas":Landroid/graphics/Canvas;
    move/from16 v0, p2

    invoke-virtual {v4, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 249
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v11, v11, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 251
    const/4 v9, 0x0

    .line 254
    .local v9, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {v12}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 260
    :goto_1
    :try_start_1
    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 261
    .end local v9    # "out":Ljava/io/OutputStream;
    .local v10, "out":Ljava/io/OutputStream;
    :try_start_2
    sget-object v13, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v14, 0x64

    invoke-virtual {v8, v13, v14, v10}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    move-result v2

    move-object v9, v10

    .line 267
    .end local v10    # "out":Ljava/io/OutputStream;
    .restart local v9    # "out":Ljava/io/OutputStream;
    :goto_2
    if-eqz v9, :cond_3

    .line 268
    :try_start_3
    invoke-virtual {v9}, Ljava/io/OutputStream;->flush()V

    .line 269
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 270
    const/4 v3, 0x1

    .line 278
    .local v3, "bSuccess3":Z
    :goto_3
    if-eqz v9, :cond_2

    .line 280
    :try_start_4
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 287
    :cond_2
    :goto_4
    if-eqz v1, :cond_5

    if-eqz v2, :cond_5

    if-eqz v3, :cond_5

    const/4 v13, 0x1

    goto :goto_0

    .line 255
    .end local v3    # "bSuccess3":Z
    :catch_0
    move-exception v6

    .line 256
    .local v6, "e1":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 262
    .end local v6    # "e1":Ljava/io/IOException;
    :catch_1
    move-exception v5

    .line 263
    .local v5, "e":Ljava/lang/Exception;
    :goto_5
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 272
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_3
    const/4 v3, 0x0

    .restart local v3    # "bSuccess3":Z
    goto :goto_3

    .line 274
    .end local v3    # "bSuccess3":Z
    :catch_2
    move-exception v5

    .line 275
    .local v5, "e":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 276
    const/4 v3, 0x0

    .line 278
    .restart local v3    # "bSuccess3":Z
    if-eqz v9, :cond_2

    .line 280
    :try_start_6
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_4

    .line 281
    :catch_3
    move-exception v5

    .line 282
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 277
    .end local v3    # "bSuccess3":Z
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v13

    .line 278
    if-eqz v9, :cond_4

    .line 280
    :try_start_7
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 285
    :cond_4
    :goto_6
    throw v13

    .line 281
    :catch_4
    move-exception v5

    .line 282
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 281
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v3    # "bSuccess3":Z
    :catch_5
    move-exception v5

    .line 282
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 287
    .end local v5    # "e":Ljava/io/IOException;
    :cond_5
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 262
    .end local v3    # "bSuccess3":Z
    .end local v9    # "out":Ljava/io/OutputStream;
    .restart local v10    # "out":Ljava/io/OutputStream;
    :catch_6
    move-exception v5

    move-object v9, v10

    .end local v10    # "out":Ljava/io/OutputStream;
    .restart local v9    # "out":Ljava/io/OutputStream;
    goto :goto_5
.end method
