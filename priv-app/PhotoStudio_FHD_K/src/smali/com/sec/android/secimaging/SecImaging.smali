.class public Lcom/sec/android/secimaging/SecImaging;
.super Ljava/lang/Object;
.source "SecImaging.java"


# static fields
.field public static final FILTER_AUTOFIX:I = 0xd9

.field public static final FILTER_BLACKANDWHITE:I = 0xdc

.field public static final FILTER_BLEACHING:I = 0x110

.field public static final FILTER_BLUEPRINT:I = 0xeb

.field public static final FILTER_BRIGHTNESS:I = 0xd4

.field public static final FILTER_BUMP:I = 0xee

.field public static final FILTER_CARTOON:I = 0xc9

.field public static final FILTER_CHALK_DRAWING:I = 0xe4

.field public static final FILTER_CHEEK_MAKEUP:I = 0x131

.field public static final FILTER_COLORTEMPERATURE:I = 0xda

.field public static final FILTER_COMICBOOM:I = 0x10b

.field public static final FILTER_CONCEALEYES:I = 0x130

.field public static final FILTER_CONTOUR:I = 0x10a

.field public static final FILTER_CONTRAST:I = 0xd6

.field public static final FILTER_CRAYON_DRAWING:I = 0xe8

.field public static final FILTER_CROP:I = 0xd8

.field public static final FILTER_CROSS:I = 0xcc

.field public static final FILTER_CRYSTALLIZE:I = 0x102

.field public static final FILTER_DARK_CIRCLE:I = 0x133

.field public static final FILTER_DECRYPTION:I = 0x107

.field public static final FILTER_DOCUMENTARY:I = 0xd1

.field public static final FILTER_DUOTONE:I = 0xde

.field public static final FILTER_ENCRYPTION:I = 0x106

.field public static final FILTER_ENGRAVING:I = 0xe7

.field public static final FILTER_FACE_BEAUTY:I = 0x12e

.field public static final FILTER_FACE_GROW:I = 0x12c

.field public static final FILTER_FACE_SHRINK:I = 0x12d

.field public static final FILTER_FACE_STRETCH:I = 0x132

.field public static final FILTER_FILLLIGHT:I = 0xd5

.field public static final FILTER_FISHEYE:I = 0xd0

.field public static final FILTER_FOG:I = 0xfb

.field public static final FILTER_FUME:I = 0xfc

.field public static final FILTER_GLASS_CRACK:I = 0xf8

.field public static final FILTER_GOTHAMNOIR:I = 0x10d

.field public static final FILTER_GRAIN:I = 0xcf

.field public static final FILTER_GRANNYPAPER:I = 0x10e

.field public static final FILTER_HALFTONE:I = 0x113

.field public static final FILTER_INFRARED:I = 0xe9

.field public static final FILTER_KALEIDOSCOPE:I = 0xf0

.field public static final FILTER_LIGHT:I = 0x104

.field public static final FILTER_LOMO:I = 0xcb

.field public static final FILTER_LOMOISH:I = 0xd2

.field public static final FILTER_MARBLE:I = 0xef

.field public static final FILTER_MIRROR:I = 0xff

.field public static final FILTER_MOSAIC:I = 0x12f

.field public static final FILTER_MOSAIC_DRAWING:I = 0xf5

.field public static final FILTER_MULTIPLE_EFFECT:I = 0x105

.field public static final FILTER_NEGATIVE:I = 0xdf

.field public static final FILTER_NEON:I = 0x108

.field public static final FILTER_NEONCOLA:I = 0x112

.field public static final FILTER_NIGHT_VISION:I = 0x109

.field public static final FILTER_OIL:I = 0xf1

.field public static final FILTER_OLDPRINTER:I = 0x111

.field public static final FILTER_PASTEL:I = 0xca

.field public static final FILTER_PASTELPERFECT:I = 0x10f

.field public static final FILTER_PASTEL_DRAWING:I = 0xe6

.field public static final FILTER_PENCIL_DRAWING:I = 0xe5

.field public static final FILTER_PENCIL_SKETCH:I = 0xe1

.field public static final FILTER_POINTILLIZE:I = 0x103

.field public static final FILTER_POSTERIZE:I = 0xcd

.field public static final FILTER_PUZZLE_IMAGE:I = 0xf4

.field public static final FILTER_RAIN:I = 0xfa

.field public static final FILTER_REDEYE:I = 0xe2

.field public static final FILTER_ROTATE:I = 0xf2

.field public static final FILTER_SATURATE:I = 0xdb

.field public static final FILTER_SCALE:I = 0x100

.field public static final FILTER_SEPIA:I = 0xdd

.field public static final FILTER_SHARPEN:I = 0xd3

.field public static final FILTER_SHEAR:I = 0x101

.field public static final FILTER_SKETCHUP:I = 0x10c

.field public static final FILTER_SNOW:I = 0xf9

.field public static final FILTER_SWIM:I = 0xed

.field public static final FILTER_TEXTURE:I = 0xf7

.field public static final FILTER_THERMAL:I = 0xea

.field public static final FILTER_TINT:I = 0xe0

.field public static final FILTER_TOGRAY:I = 0xd7

.field public static final FILTER_TRANSFER:I = 0xc8

.field public static final FILTER_VIGNETTE:I = 0xce

.field public static final FILTER_WALL_CRACK:I = 0xf6

.field public static final FILTER_WARP:I = 0xf3

.field public static final FILTER_WATER:I = 0xec

.field public static final FILTER_WATER_COLOR:I = 0xe3

.field public static final FILTER_WATER_REFLECTION:I = 0xfd

.field public static final FILTER_WATER_WAVE:I = 0xfe

.field public static final SI_IMAGE_ANDROID_BITMAP:I = 0x41524742

.field public static final SI_IMAGE_ARGB8888:I = 0x42475241

.field public static final SI_IMAGE_BGR888:I = 0x38524742

.field public static final SI_IMAGE_BGRA8888:I = 0x41524742

.field public static final SI_IMAGE_GREY:I = 0x59455247

.field public static final SI_IMAGE_JPEG:I = 0x4745504a

.field public static final SI_IMAGE_NV12:I = 0x3231564e

.field public static final SI_IMAGE_NV21:I = 0x3132564e

.field public static final SI_IMAGE_RGB888:I = 0x38424752

.field public static final SI_IMAGE_Y:I = 0x59455247

.field public static final SI_IMAGE_YUV420P:I = 0x50565559

.field public static final SI_IMAGE_YUYV:I = 0x56595559

.field public static final SI_IMAGE_YVYU:I = 0x55595659

.field public static final SI_KEY_FACE_FILTER:I = 0x6a

.field public static final SI_KEY_FACE_FILTER_VALUE_EYES:I = 0x193

.field public static final SI_KEY_FACE_FILTER_VALUE_FACE:I = 0x190

.field public static final SI_KEY_FACE_FILTER_VALUE_LEFT_EYE:I = 0x191

.field public static final SI_KEY_FACE_FILTER_VALUE_MOUTH:I = 0x195

.field public static final SI_KEY_FACE_FILTER_VALUE_NOSE:I = 0x194

.field public static final SI_KEY_FACE_FILTER_VALUE_RIGHT_EYE:I = 0x192

.field public static final SI_KEY_FACE_FILTER_VALUE_WIDE_FACE:I = 0x196

.field public static final SI_KEY_FACE_STRETCH_TYPE:I = 0x70

.field public static final SI_KEY_FACE_STRETCH_VALUE_CORN_HEAD:I = 0x1f4

.field public static final SI_KEY_FACE_STRETCH_VALUE_DISTANT_EYES:I = 0x1f7

.field public static final SI_KEY_FACE_STRETCH_VALUE_LONG:I = 0x1f5

.field public static final SI_KEY_FACE_STRETCH_VALUE_SMALL:I = 0x1f8

.field public static final SI_KEY_FACE_STRETCH_VALUE_WIDE:I = 0x1f6

.field private static final TAG:Ljava/lang/String; = "SecImaging"

.field private static sLibraryLoaded:Z


# instance fields
.field private mEffectId:I

.field private mNativeObject:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 166
    sput-boolean v4, Lcom/sec/android/secimaging/SecImaging;->sLibraryLoaded:Z

    .line 172
    :try_start_0
    const-string v1, "secimaging"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 173
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/secimaging/SecImaging;->sLibraryLoaded:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    .local v0, "ule":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 174
    .end local v0    # "ule":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 175
    .restart local v0    # "ule":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "SecImaging"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to load the native library: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    sput-boolean v4, Lcom/sec/android/secimaging/SecImaging;->sLibraryLoaded:Z

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/secimaging/SecImaging;->mEffectId:I

    .line 188
    iput p1, p0, Lcom/sec/android/secimaging/SecImaging;->mEffectId:I

    .line 189
    invoke-direct {p0}, Lcom/sec/android/secimaging/SecImaging;->init()V

    .line 190
    return-void
.end method

.method public static convertBitmapData(Landroid/graphics/Bitmap;I)[B
    .locals 8
    .param p0, "bmp"    # Landroid/graphics/Bitmap;
    .param p1, "destFormat"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 315
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 318
    :goto_0
    return-object v0

    .line 316
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    mul-int/2addr v0, v3

    new-array v1, v0, [I

    .line 317
    .local v1, "pixels":[I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move-object v0, p0

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 318
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v0, v2, p1}, Lcom/sec/android/secimaging/SecImaging;->native_convertBitmapData([IIII)[B

    move-result-object v0

    goto :goto_0
.end method

.method private convertToBitmap([III)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "argbData"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 303
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, p3, v1}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 304
    .local v0, "bmp":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method private native getValue(II)I
.end method

.method private init()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 193
    iget v0, p0, Lcom/sec/android/secimaging/SecImaging;->mEffectId:I

    invoke-direct {p0, v0}, Lcom/sec/android/secimaging/SecImaging;->native_create(I)I

    .line 194
    return-void
.end method

.method private native native_clear()V
.end method

.method static native native_convertBitmapData([IIII)[B
.end method

.method private native native_create(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method private native native_doEffect([BIII)[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native setInt(II)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcom/sec/android/secimaging/SecImaging;->mNativeObject:I

    if-eqz v0, :cond_0

    .line 201
    invoke-direct {p0}, Lcom/sec/android/secimaging/SecImaging;->native_clear()V

    .line 202
    :cond_0
    return-void
.end method

.method public doEffect(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const v6, 0x3132564e

    .line 239
    if-nez p1, :cond_0

    .line 240
    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "Input image is null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 242
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    .line 243
    .local v0, "cfg":Landroid/graphics/Bitmap$Config;
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v0, v4, :cond_1

    .line 244
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid input format: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 260
    :cond_1
    new-instance v1, Lcom/sec/android/secimaging/Image;

    invoke-direct {v1, p1}, Lcom/sec/android/secimaging/Image;-><init>(Landroid/graphics/Bitmap;)V

    .line 261
    .local v1, "image":Lcom/sec/android/secimaging/Image;
    invoke-virtual {v1, v6}, Lcom/sec/android/secimaging/Image;->convert(I)[B

    move-result-object v2

    .line 262
    .local v2, "input":[B
    invoke-virtual {v1}, Lcom/sec/android/secimaging/Image;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Lcom/sec/android/secimaging/Image;->getHeight()I

    move-result v5

    invoke-direct {p0, v2, v4, v5, v6}, Lcom/sec/android/secimaging/SecImaging;->native_doEffect([BIII)[I

    move-result-object v3

    .line 264
    .local v3, "output":[I
    invoke-virtual {v1}, Lcom/sec/android/secimaging/Image;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Lcom/sec/android/secimaging/Image;->getHeight()I

    move-result v5

    invoke-direct {p0, v3, v4, v5}, Lcom/sec/android/secimaging/SecImaging;->convertToBitmap([III)Landroid/graphics/Bitmap;

    move-result-object v4

    return-object v4
.end method

.method public doEffect([BII)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "previewData"    # [B
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 277
    const v0, 0x3132564e

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/android/secimaging/SecImaging;->doEffect([BIII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public doEffect([BIII)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "previewData"    # [B
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "format"    # I

    .prologue
    .line 289
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/secimaging/SecImaging;->native_doEffect([BIII)[I

    move-result-object v0

    .line 290
    .local v0, "data":[I
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 292
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, v0, p2, p3}, Lcom/sec/android/secimaging/SecImaging;->convertToBitmap([III)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 208
    iget v0, p0, Lcom/sec/android/secimaging/SecImaging;->mNativeObject:I

    if-eqz v0, :cond_0

    .line 209
    const-string v0, "SecImaging"

    const-string v1, "Finalize called and object is not released yet!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/secimaging/SecImaging;->clear()V

    .line 212
    :cond_0
    return-void
.end method

.method public getValue(I)I
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 220
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/secimaging/SecImaging;->getValue(II)I

    move-result v0

    return v0
.end method

.method native native_getOptionValue(Ljava/lang/String;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method native native_getOptions()[Lcom/sec/android/secimaging/EffectOption;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method native native_setOptionF(Ljava/lang/String;F)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method native native_setOptionI(Ljava/lang/String;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method native native_setOptionS(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method public setValue(II)V
    .locals 0
    .param p1, "key"    # I
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 230
    invoke-direct {p0, p1, p2}, Lcom/sec/android/secimaging/SecImaging;->setInt(II)I

    .line 231
    return-void
.end method
