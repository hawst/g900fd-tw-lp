.class public Lcom/sec/android/secimaging/EffectFactory;
.super Ljava/lang/Object;
.source "EffectFactory.java"


# static fields
.field public static final FILTER_AUTOFIX:I = 0xd9

.field static final FILTER_BEAUTY:I = 0x12e

.field public static final FILTER_BENCHMARK_BLEACHING:I = 0x110

.field public static final FILTER_BENCHMARK_COMICBOOM:I = 0x10b

.field public static final FILTER_BENCHMARK_GOTHAMNOIR:I = 0x10d

.field public static final FILTER_BENCHMARK_GRANNYPAPER:I = 0x10e

.field public static final FILTER_BENCHMARK_HALFTONE:I = 0x113

.field public static final FILTER_BENCHMARK_NEONCOLA:I = 0x112

.field public static final FILTER_BENCHMARK_OLDPRINTER:I = 0x111

.field public static final FILTER_BENCHMARK_PASTELPERFECT:I = 0x10f

.field public static final FILTER_BENCHMARK_SKETCHUP:I = 0x10c

.field public static final FILTER_BLACKANDWHITE:I = 0xdc

.field public static final FILTER_BLUEPRINT:I = 0xeb

.field public static final FILTER_BRIGHTNESS:I = 0xd4

.field public static final FILTER_BUMP:I = 0xee

.field public static final FILTER_CARTOON:I = 0xc9

.field public static final FILTER_CHALK_DRAWING:I = 0xe4

.field static final FILTER_CHEEK_MAKEUP:I = 0x131

.field public static final FILTER_COLORTEMPERATURE:I = 0xda

.field static final FILTER_CONCEALEYES:I = 0x130

.field public static final FILTER_CONTOUR:I = 0x10a

.field public static final FILTER_CONTRAST:I = 0xd6

.field static final FILTER_CPU_BASE:I = 0xc8

.field static final FILTER_CPU_LAST:I = 0x126

.field public static final FILTER_CRAYON_DRAWING:I = 0xe8

.field public static final FILTER_CROP:I = 0xd8

.field public static final FILTER_CROSS:I = 0xcc

.field public static final FILTER_CRYSTALLIZE:I = 0x102

.field static final FILTER_DARK_CIRCLE:I = 0x133

.field public static final FILTER_DECRYPTION:I = 0x107

.field public static final FILTER_DOCUMENTARY:I = 0xd1

.field public static final FILTER_DUOTONE:I = 0xde

.field public static final FILTER_ENCRYPTION:I = 0x106

.field public static final FILTER_ENGRAVING:I = 0xe7

.field public static final FILTER_FACE_BEAUTY:I = 0x12002780

.field public static final FILTER_FACE_BIG_EYES:I = 0x11e342a3

.field public static final FILTER_FACE_BIG_HEAD:I = 0x11e342a0

.field public static final FILTER_FACE_BIG_NOSE:I = 0x11e342a4

.field public static final FILTER_FACE_CHEEK_MAKEUP:I = 0x122dee40

.field public static final FILTER_FACE_CONCEAL_EYES:I = 0x121eac00

.field public static final FILTER_FACE_CORN_HEAD:I = 0x123ee7f4

.field public static final FILTER_FACE_DARK_CIRCLES:I = 0x124c72c0

.field static final FILTER_FACE_EFFECTS:[I

.field static final FILTER_FACE_GROW:I = 0x12c

.field public static final FILTER_FACE_LONG_FACE:I = 0x123ee7f5

.field public static final FILTER_FACE_MOSAIC_EYES:I = 0x12110963

.field public static final FILTER_FACE_MOSAIC_FACE:I = 0x12110960

.field static final FILTER_FACE_SHRINK:I = 0x12d

.field public static final FILTER_FACE_SMALL_HEAD:I = 0x11f284e0

.field public static final FILTER_FACE_SMALL_NOSE:I = 0x11f284e4

.field static final FILTER_FACE_STRETCH:I = 0x132

.field public static final FILTER_FACE_WIDE_FACE:I = 0x123ee7f6

.field public static final FILTER_FILLLIGHT:I = 0xd5

.field public static final FILTER_FISHEYE:I = 0xd0

.field public static final FILTER_FOG:I = 0xfb

.field public static final FILTER_FUME:I = 0xfc

.field public static final FILTER_GLASS_CRACK:I = 0xf8

.field static final FILTER_GL_BASE:I = 0x384

.field public static final FILTER_GL_PAPERCAM_ACQUARELLO:I = 0x386

.field public static final FILTER_GL_PAPERCAM_BASE:I = 0x384

.field public static final FILTER_GL_PAPERCAM_BLEACHING:I = 0x38a

.field public static final FILTER_GL_PAPERCAM_CHARCOAL:I = 0x394

.field public static final FILTER_GL_PAPERCAM_COMICBOOM:I = 0x384

.field public static final FILTER_GL_PAPERCAM_CONTOURS:I = 0x389

.field public static final FILTER_GL_PAPERCAM_DISCODANCE:I = 0x391

.field public static final FILTER_GL_PAPERCAM_FIFTIESPOSTER:I = 0x396

.field public static final FILTER_GL_PAPERCAM_GOTHAMNOIR:I = 0x38b

.field public static final FILTER_GL_PAPERCAM_GRANNYS:I = 0x38d

.field public static final FILTER_GL_PAPERCAM_HALFTONE:I = 0x38c

.field static final FILTER_GL_PAPERCAM_LAST:I = 0x397

.field public static final FILTER_GL_PAPERCAM_MESSYPAINTER:I = 0x390

.field public static final FILTER_GL_PAPERCAM_NEONCOLA:I = 0x388

.field public static final FILTER_GL_PAPERCAM_OLDPRINTER:I = 0x387

.field public static final FILTER_GL_PAPERCAM_PASTELPERFECT:I = 0x38e

.field public static final FILTER_GL_PAPERCAM_PENPAPER:I = 0x393

.field public static final FILTER_GL_PAPERCAM_SKETCHUP:I = 0x385

.field public static final FILTER_GL_PAPERCAM_UNPROCESSED:I = 0x397

.field public static final FILTER_GL_PAPERCAM_VANGOGHISH:I = 0x395

.field public static final FILTER_GL_PAPERCAM_WARHOL:I = 0x38f

.field public static final FILTER_GL_PAPERCAM_WRINKLES:I = 0x392

.field public static final FILTER_GRAIN:I = 0xcf

.field public static final FILTER_INFRARED:I = 0xe9

.field public static final FILTER_KALEIDOSCOPE:I = 0xf0

.field public static final FILTER_LIGHT:I = 0x104

.field public static final FILTER_LOMO:I = 0xcb

.field public static final FILTER_LOMOISH:I = 0xd2

.field public static final FILTER_MARBLE:I = 0xef

.field public static final FILTER_MIRROR:I = 0xff

.field static final FILTER_MOSAIC:I = 0x12f

.field public static final FILTER_MOSAIC_DRAWING:I = 0xf5

.field public static final FILTER_MULTIPLE_EFFECT:I = 0x105

.field public static final FILTER_NEGATIVE:I = 0xdf

.field public static final FILTER_NEON:I = 0x108

.field public static final FILTER_NIGHT_VISION:I = 0x109

.field public static final FILTER_OIL:I = 0xf1

.field public static final FILTER_PASTEL:I = 0xca

.field public static final FILTER_PASTEL_DRAWING:I = 0xe6

.field public static final FILTER_PENCIL_DRAWING:I = 0xe5

.field public static final FILTER_PENCIL_SKETCH:I = 0xe1

.field static final FILTER_PHOTOEDITOR_BASE:I = 0x190

.field public static final FILTER_PHOTOEDITOR_BLUEWASH:I = 0x197

.field public static final FILTER_PHOTOEDITOR_CARTOONIZE:I = 0x19c

.field public static final FILTER_PHOTOEDITOR_CONTROL_FADED_COLOR:I = 0x19a

.field public static final FILTER_PHOTOEDITOR_DOWNLIGHT:I = 0x196

.field public static final FILTER_PHOTOEDITOR_INVERT:I = 0x19b

.field static final FILTER_PHOTOEDITOR_LAST:I = 0x1a2

.field public static final FILTER_PHOTOEDITOR_MAGIC_PEN:I = 0x19e

.field public static final FILTER_PHOTOEDITOR_NATIVE_BLUEWASH:I = 0x11a

.field public static final FILTER_PHOTOEDITOR_NATIVE_CARTOONIZE:I = 0x11f

.field public static final FILTER_PHOTOEDITOR_NATIVE_CONTROL_FADED_COLOR:I = 0x11d

.field public static final FILTER_PHOTOEDITOR_NATIVE_DOWNLIGHT:I = 0x119

.field public static final FILTER_PHOTOEDITOR_NATIVE_INVERT:I = 0x11e

.field public static final FILTER_PHOTOEDITOR_NATIVE_MAGIC_PEN:I = 0x121

.field public static final FILTER_PHOTOEDITOR_NATIVE_NOSTALGIA:I = 0x11b

.field public static final FILTER_PHOTOEDITOR_NATIVE_OIL_PAINT:I = 0x122

.field public static final FILTER_PHOTOEDITOR_NATIVE_OLD_PHOTO:I = 0x114

.field public static final FILTER_PHOTOEDITOR_NATIVE_PIXELIZE:I = 0x125

.field public static final FILTER_PHOTOEDITOR_NATIVE_POP_ART:I = 0x115

.field public static final FILTER_PHOTOEDITOR_NATIVE_POSTERIZE:I = 0x123

.field public static final FILTER_PHOTOEDITOR_NATIVE_RETRO:I = 0x117

.field public static final FILTER_PHOTOEDITOR_NATIVE_SEPIA:I = 0x116

.field public static final FILTER_PHOTOEDITOR_NATIVE_SKETCH:I = 0x124

.field public static final FILTER_PHOTOEDITOR_NATIVE_SOFTGLOW:I = 0x120

.field public static final FILTER_PHOTOEDITOR_NATIVE_SUNSHINE:I = 0x118

.field public static final FILTER_PHOTOEDITOR_NATIVE_VINTAGE:I = 0x126

.field public static final FILTER_PHOTOEDITOR_NATIVE_YELLOWGLOW:I = 0x11c

.field public static final FILTER_PHOTOEDITOR_NOSTALGIA:I = 0x198

.field public static final FILTER_PHOTOEDITOR_OIL_PAINT:I = 0x19f

.field public static final FILTER_PHOTOEDITOR_OLD_PHOTO:I = 0x191

.field public static final FILTER_PHOTOEDITOR_PIXELIZE:I = 0x1a2

.field public static final FILTER_PHOTOEDITOR_POP_ART:I = 0x192

.field public static final FILTER_PHOTOEDITOR_POSTERIZE:I = 0x1a0

.field public static final FILTER_PHOTOEDITOR_RETRO:I = 0x194

.field public static final FILTER_PHOTOEDITOR_SEPIA:I = 0x193

.field public static final FILTER_PHOTOEDITOR_SKETCH:I = 0x1a1

.field public static final FILTER_PHOTOEDITOR_SOFTGLOW:I = 0x19d

.field public static final FILTER_PHOTOEDITOR_SUNSHINE:I = 0x195

.field public static final FILTER_PHOTOEDITOR_VINTAGE:I = 0x190

.field public static final FILTER_PHOTOEDITOR_YELLOWGLOW:I = 0x199

.field public static final FILTER_POINTILLIZE:I = 0x103

.field public static final FILTER_POSTERIZE:I = 0xcd

.field public static final FILTER_PUZZLE_IMAGE:I = 0xf4

.field public static final FILTER_RAIN:I = 0xfa

.field public static final FILTER_REDEYE:I = 0xe2

.field public static final FILTER_ROTATE:I = 0xf2

.field public static final FILTER_SATURATE:I = 0xdb

.field public static final FILTER_SCALE:I = 0x100

.field public static final FILTER_SEPIA:I = 0xdd

.field public static final FILTER_SHARPEN:I = 0xd3

.field public static final FILTER_SHEAR:I = 0x101

.field public static final FILTER_SNOW:I = 0xf9

.field public static final FILTER_SWIM:I = 0xed

.field public static final FILTER_TEXTURE:I = 0xf7

.field public static final FILTER_THERMAL:I = 0xea

.field public static final FILTER_TINT:I = 0xe0

.field public static final FILTER_TOGRAY:I = 0xd7

.field public static final FILTER_TRANSFER:I = 0xc8

.field public static final FILTER_VIGNETTE:I = 0xce

.field public static final FILTER_WALL_CRACK:I = 0xf6

.field public static final FILTER_WARP:I = 0xf3

.field public static final FILTER_WATER:I = 0xec

.field public static final FILTER_WATER_COLOR:I = 0xe3

.field public static final FILTER_WATER_REFLECTION:I = 0xfd

.field public static final FILTER_WATER_WAVE:I = 0xfe


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 164
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/secimaging/EffectFactory;->FILTER_FACE_EFFECTS:[I

    return-void

    :array_0
    .array-data 4
        0x11e342a3
        0x11e342a0
        0x11e342a4
        0x11f284e4
        0x11f284e0
        0x12002780
        0x12110960
        0x12110963
        0x121eac00
        0x122dee40
        0x123ee7f6
        0x123ee7f5
        0x123ee7f4
        0x124c72c0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createEffect(I)Lcom/sec/android/secimaging/IEffect;
    .locals 12
    .param p0, "id"    # I

    .prologue
    const v10, 0xf4240

    .line 250
    const/16 v9, 0xc8

    if-ge p0, v9, :cond_0

    .line 251
    new-instance v9, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid effect ID: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 252
    :cond_0
    const/16 v9, 0x126

    if-gt p0, v9, :cond_2

    .line 253
    new-instance v1, Lcom/sec/android/secimaging/EffectNative;

    invoke-direct {v1, p0}, Lcom/sec/android/secimaging/EffectNative;-><init>(I)V

    .line 269
    :cond_1
    :goto_0
    return-object v1

    .line 254
    :cond_2
    const/16 v9, 0x384

    if-lt p0, v9, :cond_3

    const/16 v9, 0x397

    if-gt p0, v9, :cond_3

    .line 255
    invoke-static {p0}, Lcom/sec/android/secimaging/EffectFactory;->createEffectGL(I)Lcom/sec/android/secimaging/IEffectGL;

    move-result-object v1

    goto :goto_0

    .line 257
    :cond_3
    sget-object v0, Lcom/sec/android/secimaging/EffectFactory;->FILTER_FACE_EFFECTS:[I

    .local v0, "arr$":[I
    array-length v8, v0

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_1
    if-ge v6, v8, :cond_5

    aget v7, v0, v6

    .line 258
    .local v7, "index":I
    if-ne v7, p0, :cond_4

    .line 259
    div-int v2, v7, v10

    .line 260
    .local v2, "effId":I
    rem-int v3, v7, v10

    .line 261
    .local v3, "faceOpt":I
    new-instance v1, Lcom/sec/android/secimaging/EffectNative;

    invoke-direct {v1, v2}, Lcom/sec/android/secimaging/EffectNative;-><init>(I)V

    .line 263
    .local v1, "eff":Lcom/sec/android/secimaging/EffectNative;
    if-lez v3, :cond_1

    .line 264
    rem-int/lit16 v5, v3, 0x3e8

    .line 265
    .local v5, "faceOptVal":I
    div-int/lit16 v4, v3, 0x3e8

    .line 266
    .local v4, "faceOptKey":I
    invoke-virtual {v1, v4, v5}, Lcom/sec/android/secimaging/EffectNative;->setValue(II)V

    goto :goto_0

    .line 257
    .end local v1    # "eff":Lcom/sec/android/secimaging/EffectNative;
    .end local v2    # "effId":I
    .end local v3    # "faceOpt":I
    .end local v4    # "faceOptKey":I
    .end local v5    # "faceOptVal":I
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 272
    .end local v7    # "index":I
    :cond_5
    new-instance v9, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid effect ID: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9
.end method

.method public static createEffectGL(I)Lcom/sec/android/secimaging/IEffectGL;
    .locals 3
    .param p0, "id"    # I

    .prologue
    .line 281
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid effect ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static initLibrary()V
    .locals 0

    .prologue
    .line 289
    return-void
.end method
