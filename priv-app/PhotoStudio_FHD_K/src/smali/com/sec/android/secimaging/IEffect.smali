.class public interface abstract Lcom/sec/android/secimaging/IEffect;
.super Ljava/lang/Object;
.source "IEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secimaging/IEffect$EffectListener;
    }
.end annotation


# virtual methods
.method public abstract getEffectID()I
.end method

.method public abstract getOption(Ljava/lang/String;)Ljava/lang/Object;
.end method

.method public abstract getOptions()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/secimaging/EffectOption;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isCPU()Z
.end method

.method public abstract isOpenGL()Z
.end method

.method public abstract release()V
.end method

.method public abstract setEffectListener(Lcom/sec/android/secimaging/IEffect$EffectListener;)V
.end method

.method public abstract setImage(Lcom/sec/android/secimaging/Image;)V
.end method

.method public abstract setOption(Ljava/lang/String;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method public abstract startLowResProcessing()V
.end method

.method public abstract startProcessing(Ljava/lang/String;I)V
.end method
