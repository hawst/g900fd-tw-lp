.class public Lcom/sec/android/secimaging/EffectOption;
.super Ljava/lang/Object;
.source "EffectOption.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secimaging/EffectOption$1;,
        Lcom/sec/android/secimaging/EffectOption$Type;
    }
.end annotation


# instance fields
.field private mEnumValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFloatMax:F

.field private mFloatMin:F

.field private mIntMax:I

.field private mIntMin:I

.field private mName:Ljava/lang/String;

.field private mType:Lcom/sec/android/secimaging/EffectOption$Type;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-object v0, Lcom/sec/android/secimaging/EffectOption$Type;->INTEGER:Lcom/sec/android/secimaging/EffectOption$Type;

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    .line 41
    iput-object v3, p0, Lcom/sec/android/secimaging/EffectOption;->mName:Ljava/lang/String;

    .line 43
    iput v2, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMax:I

    .line 44
    iput v2, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMin:I

    .line 46
    iput v1, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMax:F

    .line 47
    iput v1, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMin:F

    .line 49
    iput-object v3, p0, Lcom/sec/android/secimaging/EffectOption;->mEnumValues:Ljava/util/ArrayList;

    .line 69
    sget-object v0, Lcom/sec/android/secimaging/EffectOption$Type;->INTEGER:Lcom/sec/android/secimaging/EffectOption$Type;

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    .line 70
    iput-object p1, p0, Lcom/sec/android/secimaging/EffectOption;->mName:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;FF)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "fmin"    # F
    .param p3, "fmax"    # F

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-object v0, Lcom/sec/android/secimaging/EffectOption$Type;->INTEGER:Lcom/sec/android/secimaging/EffectOption$Type;

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    .line 41
    iput-object v3, p0, Lcom/sec/android/secimaging/EffectOption;->mName:Ljava/lang/String;

    .line 43
    iput v2, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMax:I

    .line 44
    iput v2, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMin:I

    .line 46
    iput v1, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMax:F

    .line 47
    iput v1, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMin:F

    .line 49
    iput-object v3, p0, Lcom/sec/android/secimaging/EffectOption;->mEnumValues:Ljava/util/ArrayList;

    .line 61
    sget-object v0, Lcom/sec/android/secimaging/EffectOption$Type;->FLOAT:Lcom/sec/android/secimaging/EffectOption$Type;

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    .line 62
    iput p3, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMax:F

    .line 63
    iput p2, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMin:F

    .line 64
    iput-object p1, p0, Lcom/sec/android/secimaging/EffectOption;->mName:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "min"    # I
    .param p3, "max"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-object v0, Lcom/sec/android/secimaging/EffectOption$Type;->INTEGER:Lcom/sec/android/secimaging/EffectOption$Type;

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    .line 41
    iput-object v3, p0, Lcom/sec/android/secimaging/EffectOption;->mName:Ljava/lang/String;

    .line 43
    iput v2, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMax:I

    .line 44
    iput v2, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMin:I

    .line 46
    iput v1, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMax:F

    .line 47
    iput v1, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMin:F

    .line 49
    iput-object v3, p0, Lcom/sec/android/secimaging/EffectOption;->mEnumValues:Ljava/util/ArrayList;

    .line 53
    sget-object v0, Lcom/sec/android/secimaging/EffectOption$Type;->INTEGER:Lcom/sec/android/secimaging/EffectOption$Type;

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    .line 54
    iput p2, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMin:I

    .line 55
    iput p3, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMax:I

    .line 56
    iput-object p1, p0, Lcom/sec/android/secimaging/EffectOption;->mName:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sec/android/secimaging/EffectOption$Type;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/sec/android/secimaging/EffectOption$Type;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-object v0, Lcom/sec/android/secimaging/EffectOption$Type;->INTEGER:Lcom/sec/android/secimaging/EffectOption$Type;

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    .line 41
    iput-object v3, p0, Lcom/sec/android/secimaging/EffectOption;->mName:Ljava/lang/String;

    .line 43
    iput v2, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMax:I

    .line 44
    iput v2, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMin:I

    .line 46
    iput v1, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMax:F

    .line 47
    iput v1, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMin:F

    .line 49
    iput-object v3, p0, Lcom/sec/android/secimaging/EffectOption;->mEnumValues:Ljava/util/ArrayList;

    .line 81
    sget-object v0, Lcom/sec/android/secimaging/EffectOption$Type;->ENUM:Lcom/sec/android/secimaging/EffectOption$Type;

    if-ne p2, v0, :cond_0

    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t create an ENUM option without specifying the acceptable values"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_0
    iput-object p2, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    .line 85
    iput-object p1, p0, Lcom/sec/android/secimaging/EffectOption;->mName:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "enumValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-object v0, Lcom/sec/android/secimaging/EffectOption$Type;->INTEGER:Lcom/sec/android/secimaging/EffectOption$Type;

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    .line 41
    iput-object v3, p0, Lcom/sec/android/secimaging/EffectOption;->mName:Ljava/lang/String;

    .line 43
    iput v2, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMax:I

    .line 44
    iput v2, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMin:I

    .line 46
    iput v1, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMax:F

    .line 47
    iput v1, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMin:F

    .line 49
    iput-object v3, p0, Lcom/sec/android/secimaging/EffectOption;->mEnumValues:Ljava/util/ArrayList;

    .line 75
    sget-object v0, Lcom/sec/android/secimaging/EffectOption$Type;->ENUM:Lcom/sec/android/secimaging/EffectOption$Type;

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    .line 76
    iput-object p2, p0, Lcom/sec/android/secimaging/EffectOption;->mEnumValues:Ljava/util/ArrayList;

    .line 77
    return-void
.end method

.method private initAddEnumVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mEnumValues:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 211
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mEnumValues:Ljava/util/ArrayList;

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mEnumValues:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    return-void
.end method

.method private initMinMax(FF)V
    .locals 0
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    .line 204
    iput p1, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMin:F

    .line 205
    iput p2, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMax:F

    .line 206
    return-void
.end method

.method private initMinMax(II)V
    .locals 0
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    .line 198
    iput p1, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMin:I

    .line 199
    iput p2, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMax:I

    .line 200
    return-void
.end method

.method private initWithType(Ljava/lang/String;I)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/android/secimaging/EffectOption;->mName:Ljava/lang/String;

    .line 178
    packed-switch p2, :pswitch_data_0

    .line 192
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid type in initByType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :pswitch_0
    sget-object v0, Lcom/sec/android/secimaging/EffectOption$Type;->INTEGER:Lcom/sec/android/secimaging/EffectOption$Type;

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    .line 194
    :goto_0
    return-void

    .line 183
    :pswitch_1
    sget-object v0, Lcom/sec/android/secimaging/EffectOption$Type;->FLOAT:Lcom/sec/android/secimaging/EffectOption$Type;

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    goto :goto_0

    .line 186
    :pswitch_2
    sget-object v0, Lcom/sec/android/secimaging/EffectOption$Type;->STRING:Lcom/sec/android/secimaging/EffectOption$Type;

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    goto :goto_0

    .line 189
    :pswitch_3
    sget-object v0, Lcom/sec/android/secimaging/EffectOption$Type;->ENUM:Lcom/sec/android/secimaging/EffectOption$Type;

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    goto :goto_0

    .line 178
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getEnumValues()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    sget-object v1, Lcom/sec/android/secimaging/EffectOption$Type;->ENUM:Lcom/sec/android/secimaging/EffectOption$Type;

    if-eq v0, v1, :cond_0

    .line 127
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid call to getEnumValues for option of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mEnumValues:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMaxF()F
    .locals 3

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    sget-object v1, Lcom/sec/android/secimaging/EffectOption$Type;->FLOAT:Lcom/sec/android/secimaging/EffectOption$Type;

    if-eq v0, v1, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid call to getMaxF for option of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_0
    iget v0, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMax:F

    return v0
.end method

.method public getMaxI()I
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    sget-object v1, Lcom/sec/android/secimaging/EffectOption$Type;->INTEGER:Lcom/sec/android/secimaging/EffectOption$Type;

    if-eq v0, v1, :cond_0

    .line 93
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid call to getMaxI for option of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_0
    iget v0, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMax:I

    return v0
.end method

.method public getMinF()F
    .locals 3

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    sget-object v1, Lcom/sec/android/secimaging/EffectOption$Type;->FLOAT:Lcom/sec/android/secimaging/EffectOption$Type;

    if-eq v0, v1, :cond_0

    .line 120
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid call to getMinF for option of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_0
    iget v0, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMin:F

    return v0
.end method

.method public getMinI()I
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    sget-object v1, Lcom/sec/android/secimaging/EffectOption$Type;->INTEGER:Lcom/sec/android/secimaging/EffectOption$Type;

    if-eq v0, v1, :cond_0

    .line 102
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid call to getMinI for option of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_0
    iget v0, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMin:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/sec/android/secimaging/EffectOption$Type;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    return-object v0
.end method

.method public isAcceptableValue(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 133
    sget-object v4, Lcom/sec/android/secimaging/EffectOption$1;->$SwitchMap$com$sec$android$secimaging$EffectOption$Type:[I

    iget-object v5, p0, Lcom/sec/android/secimaging/EffectOption;->mType:Lcom/sec/android/secimaging/EffectOption$Type;

    invoke-virtual {v5}, Lcom/sec/android/secimaging/EffectOption$Type;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 158
    .end local p1    # "value":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v2

    .line 135
    .restart local p1    # "value":Ljava/lang/Object;
    :pswitch_0
    instance-of v4, p1, Ljava/lang/Integer;

    if-eqz v4, :cond_0

    .line 136
    iget v4, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMin:I

    if-nez v4, :cond_1

    iget v4, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMax:I

    if-nez v4, :cond_1

    move v2, v3

    goto :goto_0

    .line 137
    :cond_1
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "value":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 138
    .local v1, "i":I
    iget v4, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMin:I

    if-gt v4, v1, :cond_0

    .line 139
    iget v4, p0, Lcom/sec/android/secimaging/EffectOption;->mIntMax:I

    if-lt v4, v1, :cond_0

    move v2, v3

    .line 140
    goto :goto_0

    .line 143
    .end local v1    # "i":I
    .restart local p1    # "value":Ljava/lang/Object;
    :pswitch_1
    instance-of v4, p1, Ljava/lang/Float;

    if-eqz v4, :cond_0

    .line 144
    iget v4, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMin:F

    cmpl-float v4, v4, v6

    if-nez v4, :cond_2

    iget v4, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMax:F

    cmpl-float v4, v4, v6

    if-nez v4, :cond_2

    move v2, v3

    goto :goto_0

    .line 145
    :cond_2
    check-cast p1, Ljava/lang/Float;

    .end local p1    # "value":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 146
    .local v0, "f":F
    iget v4, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMin:F

    cmpl-float v4, v4, v0

    if-gtz v4, :cond_0

    .line 147
    iget v4, p0, Lcom/sec/android/secimaging/EffectOption;->mFloatMax:F

    cmpg-float v4, v4, v0

    if-ltz v4, :cond_0

    move v2, v3

    .line 148
    goto :goto_0

    .line 151
    .end local v0    # "f":F
    .restart local p1    # "value":Ljava/lang/Object;
    :pswitch_2
    instance-of v2, p1, Ljava/lang/String;

    goto :goto_0

    .line 154
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/secimaging/EffectOption;->mEnumValues:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 155
    instance-of v3, p1, Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 156
    iget-object v2, p0, Lcom/sec/android/secimaging/EffectOption;->mEnumValues:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0

    .line 133
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
