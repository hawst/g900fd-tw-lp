.class public Lcom/sec/android/secimaging/Image;
.super Ljava/lang/Object;
.source "Image.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secimaging/Image$2;
    }
.end annotation


# static fields
.field public static final FORMAT_ANDROID_BITMAP:I = 0x41524742

.field public static final FORMAT_ARGB8888:I = 0x42475241

.field public static final FORMAT_BGR888:I = 0x38524742

.field public static final FORMAT_BGRA8888:I = 0x41524742

.field public static final FORMAT_GREY:I = 0x59455247

.field public static final FORMAT_JPEG:I = 0x4745504a

.field public static final FORMAT_NV12:I = 0x3231564e

.field public static final FORMAT_NV21:I = 0x3132564e

.field public static final FORMAT_RGB888:I = 0x38424752

.field public static final FORMAT_Y:I = 0x59455247

.field public static final FORMAT_YUV420P:I = 0x50565559

.field public static final FORMAT_YUYV:I = 0x56595559

.field public static final FORMAT_YVYU:I = 0x55595659

.field private static final TAG:Ljava/lang/String; = "secimaging.Image"


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mFormat:I

.field private mHeight:I

.field private mJpegCond:Landroid/os/ConditionVariable;

.field private mJpegData:[B

.field private mJpegFile:Ljava/lang/String;

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput v4, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    .line 67
    iput-object v1, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    .line 68
    iput-object v1, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    .line 69
    new-instance v1, Landroid/os/ConditionVariable;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v1, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    .line 71
    iput v4, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    .line 72
    iput v4, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    .line 79
    iput-object p1, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    .line 81
    iget-object v1, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    .line 83
    .local v0, "cfg":Landroid/graphics/Bitmap$Config;
    sget-object v1, Lcom/sec/android/secimaging/Image$2;->$SwitchMap$android$graphics$Bitmap$Config:[I

    invoke-virtual {v0}, Landroid/graphics/Bitmap$Config;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 91
    const-string v1, "secimaging.Image"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Passing unsupported format "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to SecImaging"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iput v4, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    .line 96
    .end local v0    # "cfg":Landroid/graphics/Bitmap$Config;
    :cond_0
    :goto_0
    return-void

    .line 85
    .restart local v0    # "cfg":Landroid/graphics/Bitmap$Config;
    :pswitch_0
    const v1, 0x59455247

    iput v1, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    goto :goto_0

    .line 88
    :pswitch_1
    const v1, 0x41524742

    iput v1, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    goto :goto_0

    .line 83
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    .line 67
    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    .line 68
    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    .line 69
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    .line 71
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    .line 72
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    .line 103
    const v0, 0x4745504a

    iput v0, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    .line 104
    iput-object p1, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "decode"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    .line 67
    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    .line 68
    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    .line 69
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    .line 71
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    .line 72
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    .line 111
    const v0, 0x4745504a

    iput v0, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    .line 112
    iput-object p1, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    .line 114
    if-eqz p2, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/secimaging/Image;->startDecoding()V

    .line 116
    :cond_0
    return-void
.end method

.method public constructor <init>([B)V
    .locals 3
    .param p1, "jpegData"    # [B

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    .line 67
    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    .line 68
    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    .line 69
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    .line 71
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    .line 72
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    .line 124
    const v0, 0x4745504a

    iput v0, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    .line 125
    iput-object p1, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    .line 126
    return-void
.end method

.method public constructor <init>([BZ)V
    .locals 3
    .param p1, "jpegData"    # [B
    .param p2, "decode"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    .line 67
    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    .line 68
    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    .line 69
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    .line 71
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    .line 72
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    .line 133
    const v0, 0x4745504a

    iput v0, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    .line 134
    iput-object p1, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    .line 136
    if-eqz p2, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/sec/android/secimaging/Image;->startDecoding()V

    .line 138
    :cond_0
    return-void
.end method

.method public constructor <init>([III)V
    .locals 3
    .param p1, "data"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    .line 67
    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    .line 68
    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    .line 69
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    .line 71
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    .line 72
    iput v2, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    .line 146
    const v0, 0x41524742

    iput v0, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    .line 147
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, p3, v0}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    .line 148
    return-void
.end method

.method public constructor <init>([IIILandroid/graphics/Bitmap$Config;)V
    .locals 5
    .param p1, "data"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput v4, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    .line 67
    iput-object v1, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    .line 68
    iput-object v1, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    .line 69
    new-instance v1, Landroid/os/ConditionVariable;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v1, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    .line 71
    iput v4, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    .line 72
    iput v4, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    .line 160
    invoke-static {p1, p2, p3, p4}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    .line 162
    iget-object v1, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    .line 164
    .local v0, "cfg":Landroid/graphics/Bitmap$Config;
    sget-object v1, Lcom/sec/android/secimaging/Image$2;->$SwitchMap$android$graphics$Bitmap$Config:[I

    invoke-virtual {v0}, Landroid/graphics/Bitmap$Config;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 172
    const-string v1, "secimaging.Image"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Passing unsupported format "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to SecImaging"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iput v4, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    .line 177
    .end local v0    # "cfg":Landroid/graphics/Bitmap$Config;
    :cond_0
    :goto_0
    return-void

    .line 166
    .restart local v0    # "cfg":Landroid/graphics/Bitmap$Config;
    :pswitch_0
    const v1, 0x59455247

    iput v1, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    goto :goto_0

    .line 169
    :pswitch_1
    const v1, 0x41524742

    iput v1, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    goto :goto_0

    .line 164
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic access$000(Lcom/sec/android/secimaging/Image;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secimaging/Image;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/secimaging/Image;[B)[B
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secimaging/Image;
    .param p1, "x1"    # [B

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/secimaging/Image;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secimaging/Image;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/secimaging/Image;)Landroid/os/ConditionVariable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secimaging/Image;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/secimaging/Image;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secimaging/Image;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$402(Lcom/sec/android/secimaging/Image;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secimaging/Image;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    return p1
.end method

.method private readDimensionsFromJpeg()V
    .locals 6

    .prologue
    .line 220
    iget v2, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    if-nez v2, :cond_1

    .line 222
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 223
    iget-object v2, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    if-nez v2, :cond_2

    .line 238
    :cond_1
    :goto_0
    return-void

    .line 224
    :cond_2
    iget-object v2, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    array-length v4, v4

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/BitmapRegionDecoder;->newInstance([BIIZ)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v0

    .line 225
    .local v0, "brd":Landroid/graphics/BitmapRegionDecoder;
    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    .line 226
    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    .line 227
    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->recycle()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 234
    .end local v0    # "brd":Landroid/graphics/BitmapRegionDecoder;
    :catch_0
    move-exception v1

    .line 235
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 229
    .end local v1    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/lang/String;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v0

    .line 230
    .restart local v0    # "brd":Landroid/graphics/BitmapRegionDecoder;
    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    .line 231
    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    .line 232
    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->recycle()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public asBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/secimaging/Image;->asBitmap(Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized asBitmap(Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "decode"    # Z

    .prologue
    .line 191
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    :goto_0
    monitor-exit p0

    return-object v0

    .line 195
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 196
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 199
    :cond_1
    if-eqz p1, :cond_2

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/secimaging/Image;->startDecoding()V

    .line 201
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 204
    :cond_2
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public convert(I)[B
    .locals 1
    .param p1, "destFormat"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    .line 279
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/secimaging/Image;->asBitmap(Z)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/secimaging/SecImaging;->convertBitmapData(Landroid/graphics/Bitmap;I)[B

    move-result-object v0

    return-object v0
.end method

.method public createLowRes()Lcom/sec/android/secimaging/Image;
    .locals 1

    .prologue
    .line 372
    const/16 v0, 0x400

    invoke-virtual {p0, v0}, Lcom/sec/android/secimaging/Image;->createLowRes(I)Lcom/sec/android/secimaging/Image;

    move-result-object v0

    return-object v0
.end method

.method public createLowRes(I)Lcom/sec/android/secimaging/Image;
    .locals 16
    .param p1, "maxDim"    # I

    .prologue
    .line 380
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v12, :cond_5

    .line 381
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    move/from16 v0, p1

    if-gt v12, v0, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    move/from16 v0, p1

    if-gt v12, v0, :cond_0

    .line 382
    new-instance v12, Lcom/sec/android/secimaging/Image;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-direct {v12, v13}, Lcom/sec/android/secimaging/Image;-><init>(Landroid/graphics/Bitmap;)V

    .line 444
    :goto_0
    return-object v12

    .line 385
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    if-lez v12, :cond_1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    if-gtz v12, :cond_2

    :cond_1
    const/4 v12, 0x0

    goto :goto_0

    .line 387
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    if-le v12, v13, :cond_3

    const/4 v8, 0x1

    .line 389
    .local v8, "portrait":Z
    :goto_1
    if-eqz v8, :cond_4

    .line 390
    move/from16 v4, p1

    .line 391
    .local v4, "height":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    mul-int v12, v12, p1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    div-int v11, v12, v13

    .line 397
    .local v11, "width":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v13, 0x1

    invoke-static {v12, v11, v4, v13}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 398
    .local v1, "bmp":Landroid/graphics/Bitmap;
    new-instance v12, Lcom/sec/android/secimaging/Image;

    invoke-direct {v12, v1}, Lcom/sec/android/secimaging/Image;-><init>(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 387
    .end local v1    # "bmp":Landroid/graphics/Bitmap;
    .end local v4    # "height":I
    .end local v8    # "portrait":Z
    .end local v11    # "width":I
    :cond_3
    const/4 v8, 0x0

    goto :goto_1

    .line 393
    .restart local v8    # "portrait":Z
    :cond_4
    move/from16 v11, p1

    .line 394
    .restart local v11    # "width":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    mul-int v12, v12, p1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    div-int v4, v12, v13

    .restart local v4    # "height":I
    goto :goto_2

    .line 401
    .end local v4    # "height":I
    .end local v8    # "portrait":Z
    .end local v11    # "width":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    if-nez v12, :cond_6

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    if-eqz v12, :cond_d

    .line 403
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/secimaging/Image;->getWidth()I

    move-result v7

    .line 404
    .local v7, "origWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/secimaging/Image;->getHeight()I

    move-result v6

    .line 406
    .local v6, "origHeight":I
    if-lez v7, :cond_7

    if-gtz v6, :cond_8

    :cond_7
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 408
    :cond_8
    if-le v6, v7, :cond_9

    const/4 v8, 0x1

    .line 411
    .restart local v8    # "portrait":Z
    :goto_3
    if-eqz v8, :cond_a

    .line 412
    move/from16 v4, p1

    .line 413
    .restart local v4    # "height":I
    mul-int v12, v7, p1

    div-int v11, v12, v6

    .line 419
    .restart local v11    # "width":I
    :goto_4
    div-int v12, v7, v11

    int-to-float v9, v12

    .line 423
    .local v9, "scaling":F
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    if-eqz v12, :cond_b

    .line 424
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    array-length v14, v14

    const/4 v15, 0x0

    invoke-static {v12, v13, v14, v15}, Landroid/graphics/BitmapRegionDecoder;->newInstance([BIIZ)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v2

    .line 427
    .local v2, "brd":Landroid/graphics/BitmapRegionDecoder;
    :goto_5
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 428
    .local v5, "opt":Landroid/graphics/BitmapFactory$Options;
    float-to-int v12, v9

    iput v12, v5, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 430
    new-instance v12, Landroid/graphics/Rect;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-direct {v12, v13, v14, v7, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v2, v12, v5}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 431
    .restart local v1    # "bmp":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/BitmapRegionDecoder;->recycle()V

    .line 433
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    move/from16 v0, p1

    if-gt v12, v0, :cond_c

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    move/from16 v0, p1

    if-gt v12, v0, :cond_c

    .line 434
    new-instance v12, Lcom/sec/android/secimaging/Image;

    invoke-direct {v12, v1}, Lcom/sec/android/secimaging/Image;-><init>(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 441
    .end local v1    # "bmp":Landroid/graphics/Bitmap;
    .end local v2    # "brd":Landroid/graphics/BitmapRegionDecoder;
    .end local v5    # "opt":Landroid/graphics/BitmapFactory$Options;
    :catch_0
    move-exception v3

    .line 443
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 444
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 408
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "height":I
    .end local v8    # "portrait":Z
    .end local v9    # "scaling":F
    .end local v11    # "width":I
    :cond_9
    const/4 v8, 0x0

    goto :goto_3

    .line 415
    .restart local v8    # "portrait":Z
    :cond_a
    move/from16 v11, p1

    .line 416
    .restart local v11    # "width":I
    mul-int v12, v6, p1

    div-int v4, v12, v7

    .restart local v4    # "height":I
    goto :goto_4

    .line 426
    .restart local v9    # "scaling":F
    :cond_b
    :try_start_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static {v12, v13}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/lang/String;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v2

    .restart local v2    # "brd":Landroid/graphics/BitmapRegionDecoder;
    goto :goto_5

    .line 437
    .restart local v1    # "bmp":Landroid/graphics/Bitmap;
    .restart local v5    # "opt":Landroid/graphics/BitmapFactory$Options;
    :cond_c
    const/4 v12, 0x1

    invoke-static {v1, v11, v4, v12}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 438
    .local v10, "small":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 440
    new-instance v12, Lcom/sec/android/secimaging/Image;

    invoke-direct {v12, v10}, Lcom/sec/android/secimaging/Image;-><init>(Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 448
    .end local v1    # "bmp":Landroid/graphics/Bitmap;
    .end local v2    # "brd":Landroid/graphics/BitmapRegionDecoder;
    .end local v4    # "height":I
    .end local v5    # "opt":Landroid/graphics/BitmapFactory$Options;
    .end local v6    # "origHeight":I
    .end local v7    # "origWidth":I
    .end local v8    # "portrait":Z
    .end local v9    # "scaling":F
    .end local v10    # "small":Landroid/graphics/Bitmap;
    .end local v11    # "width":I
    :cond_d
    new-instance v12, Ljava/lang/IllegalStateException;

    const-string v13, "No bitmap, no jpeg? Please check your code!"

    invoke-direct {v12, v13}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v12
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    return-object v0
.end method

.method public getFormat()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/sec/android/secimaging/Image;->mFormat:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 245
    invoke-direct {p0}, Lcom/sec/android/secimaging/Image;->readDimensionsFromJpeg()V

    .line 246
    iget v0, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    .line 255
    :goto_0
    return v0

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 249
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    .line 251
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    .line 252
    iget v0, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    goto :goto_0

    .line 255
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized getJpegData()[B
    .locals 6

    .prologue
    .line 343
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    if-eqz v3, :cond_0

    .line 344
    iget-object v3, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363
    :goto_0
    monitor-exit p0

    return-object v3

    .line 346
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 347
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 350
    .local v1, "file":Ljava/io/File;
    :try_start_2
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 351
    .local v2, "fis":Ljava/io/FileInputStream;
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v3, v4

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    .line 352
    iget-object v3, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 353
    iget-object v3, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 354
    .end local v2    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 356
    .local v0, "e":Ljava/io/FileNotFoundException;
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 363
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    .end local v1    # "file":Ljava/io/File;
    :cond_1
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    .line 357
    .restart local v1    # "file":Ljava/io/File;
    :catch_1
    move-exception v0

    .line 359
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 343
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "file":Ljava/io/File;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/sec/android/secimaging/Image;->readDimensionsFromJpeg()V

    .line 264
    iget v0, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    .line 272
    :goto_0
    return v0

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 267
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    .line 269
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/secimaging/Image;->mHeight:I

    .line 270
    iget v0, p0, Lcom/sec/android/secimaging/Image;->mWidth:I

    goto :goto_0

    .line 272
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 213
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 216
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    .line 217
    return-void
.end method

.method public save(Ljava/lang/String;I)Z
    .locals 9
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "quality"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 456
    if-lt p2, v4, :cond_0

    const/16 v6, 0x64

    if-le p2, v6, :cond_1

    .line 457
    :cond_0
    const/16 p2, 0x55

    .line 460
    :cond_1
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 461
    .local v0, "f":Ljava/io/File;
    iget-object v6, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    if-nez v6, :cond_2

    .line 462
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v6, p0, Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 463
    .local v1, "fis":Ljava/io/FileInputStream;
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v6, v6

    new-array v6, v6, [B

    iput-object v6, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    .line 464
    iget-object v6, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    invoke-virtual {v1, v6}, Ljava/io/FileInputStream;->read([B)I

    .line 465
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 468
    .end local v1    # "fis":Ljava/io/FileInputStream;
    :cond_2
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 470
    .local v2, "fos":Ljava/io/FileOutputStream;
    iget-object v6, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    if-eqz v6, :cond_3

    .line 471
    const-string v6, "secimaging.Image"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Saving jpeg data to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    iget-object v6, p0, Lcom/sec/android/secimaging/Image;->mJpegData:[B

    invoke-virtual {v2, v6}, Ljava/io/FileOutputStream;->write([B)V

    .line 473
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 492
    .end local v0    # "f":Ljava/io/File;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    :goto_0
    return v4

    .line 477
    .restart local v0    # "f":Ljava/io/File;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :cond_3
    iget-object v6, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_4

    .line 478
    const-string v6, "secimaging.Image"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Saving Bitmap to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    iget-object v6, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v6, v7, p2, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v4

    .line 480
    .local v4, "ret":Z
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 485
    .end local v0    # "f":Ljava/io/File;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .end local v4    # "ret":Z
    :catch_0
    move-exception v3

    .line 486
    .local v3, "ioex":Ljava/io/IOException;
    const-string v6, "secimaging.Image"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to save image to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    move v4, v5

    .line 488
    goto :goto_0

    .line 484
    .end local v3    # "ioex":Ljava/io/IOException;
    .restart local v0    # "f":Ljava/io/File;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :cond_4
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 491
    const-string v6, "secimaging.Image"

    const-string v7, "This image has no data! Can\'t save"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 492
    goto :goto_0
.end method

.method public startDecoding()V
    .locals 2

    .prologue
    .line 287
    iget-object v1, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    .line 289
    iget-object v1, p0, Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 290
    iget-object v1, p0, Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    .line 330
    :goto_0
    return-void

    .line 294
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/secimaging/Image$1;

    invoke-direct {v1, p0}, Lcom/sec/android/secimaging/Image$1;-><init>(Lcom/sec/android/secimaging/Image;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 329
    .local v0, "td":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
