.class Lcom/sec/android/secimaging/Image$1;
.super Ljava/lang/Object;
.source "Image.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/secimaging/Image;->startDecoding()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/secimaging/Image;


# direct methods
.method constructor <init>(Lcom/sec/android/secimaging/Image;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/android/secimaging/Image$1;->this$0:Lcom/sec/android/secimaging/Image;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 298
    iget-object v4, p0, Lcom/sec/android/secimaging/Image$1;->this$0:Lcom/sec/android/secimaging/Image;

    # getter for: Lcom/sec/android/secimaging/Image;->mJpegData:[B
    invoke-static {v4}, Lcom/sec/android/secimaging/Image;->access$000(Lcom/sec/android/secimaging/Image;)[B

    move-result-object v4

    if-nez v4, :cond_0

    .line 300
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/secimaging/Image$1;->this$0:Lcom/sec/android/secimaging/Image;

    # getter for: Lcom/sec/android/secimaging/Image;->mJpegFile:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/secimaging/Image;->access$100(Lcom/sec/android/secimaging/Image;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 301
    .local v1, "file":Ljava/io/File;
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 303
    .local v2, "fis":Ljava/io/FileInputStream;
    iget-object v4, p0, Lcom/sec/android/secimaging/Image$1;->this$0:Lcom/sec/android/secimaging/Image;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v5, v6

    new-array v5, v5, [B

    # setter for: Lcom/sec/android/secimaging/Image;->mJpegData:[B
    invoke-static {v4, v5}, Lcom/sec/android/secimaging/Image;->access$002(Lcom/sec/android/secimaging/Image;[B)[B

    .line 304
    iget-object v4, p0, Lcom/sec/android/secimaging/Image$1;->this$0:Lcom/sec/android/secimaging/Image;

    # getter for: Lcom/sec/android/secimaging/Image;->mJpegData:[B
    invoke-static {v4}, Lcom/sec/android/secimaging/Image;->access$000(Lcom/sec/android/secimaging/Image;)[B

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/FileInputStream;->read([B)I

    .line 305
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 318
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "fis":Ljava/io/FileInputStream;
    :cond_0
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 319
    .local v3, "opts":Landroid/graphics/BitmapFactory$Options;
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v4, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 320
    iget-object v4, p0, Lcom/sec/android/secimaging/Image$1;->this$0:Lcom/sec/android/secimaging/Image;

    iget-object v5, p0, Lcom/sec/android/secimaging/Image$1;->this$0:Lcom/sec/android/secimaging/Image;

    # getter for: Lcom/sec/android/secimaging/Image;->mJpegData:[B
    invoke-static {v5}, Lcom/sec/android/secimaging/Image;->access$000(Lcom/sec/android/secimaging/Image;)[B

    move-result-object v5

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/secimaging/Image$1;->this$0:Lcom/sec/android/secimaging/Image;

    # getter for: Lcom/sec/android/secimaging/Image;->mJpegData:[B
    invoke-static {v7}, Lcom/sec/android/secimaging/Image;->access$000(Lcom/sec/android/secimaging/Image;)[B

    move-result-object v7

    array-length v7, v7

    invoke-static {v5, v6, v7, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    # setter for: Lcom/sec/android/secimaging/Image;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v4, v5}, Lcom/sec/android/secimaging/Image;->access$302(Lcom/sec/android/secimaging/Image;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 324
    iget-object v4, p0, Lcom/sec/android/secimaging/Image$1;->this$0:Lcom/sec/android/secimaging/Image;

    const v5, 0x41524742

    # setter for: Lcom/sec/android/secimaging/Image;->mFormat:I
    invoke-static {v4, v5}, Lcom/sec/android/secimaging/Image;->access$402(Lcom/sec/android/secimaging/Image;I)I

    .line 326
    iget-object v4, p0, Lcom/sec/android/secimaging/Image$1;->this$0:Lcom/sec/android/secimaging/Image;

    # getter for: Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;
    invoke-static {v4}, Lcom/sec/android/secimaging/Image;->access$200(Lcom/sec/android/secimaging/Image;)Landroid/os/ConditionVariable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/ConditionVariable;->open()V

    .line 327
    .end local v3    # "opts":Landroid/graphics/BitmapFactory$Options;
    :goto_0
    return-void

    .line 306
    :catch_0
    move-exception v0

    .line 307
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 308
    iget-object v4, p0, Lcom/sec/android/secimaging/Image$1;->this$0:Lcom/sec/android/secimaging/Image;

    # getter for: Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;
    invoke-static {v4}, Lcom/sec/android/secimaging/Image;->access$200(Lcom/sec/android/secimaging/Image;)Landroid/os/ConditionVariable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/ConditionVariable;->open()V

    goto :goto_0

    .line 310
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 311
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 312
    iget-object v4, p0, Lcom/sec/android/secimaging/Image$1;->this$0:Lcom/sec/android/secimaging/Image;

    # getter for: Lcom/sec/android/secimaging/Image;->mJpegCond:Landroid/os/ConditionVariable;
    invoke-static {v4}, Lcom/sec/android/secimaging/Image;->access$200(Lcom/sec/android/secimaging/Image;)Landroid/os/ConditionVariable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/ConditionVariable;->open()V

    goto :goto_0
.end method
