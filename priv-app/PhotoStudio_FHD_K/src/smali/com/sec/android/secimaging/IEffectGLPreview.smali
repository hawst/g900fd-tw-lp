.class public interface abstract Lcom/sec/android/secimaging/IEffectGLPreview;
.super Ljava/lang/Object;
.source "IEffectGLPreview.java"

# interfaces
.implements Lcom/sec/android/secimaging/IEffectGL;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secimaging/IEffectGLPreview$PreviewEffectListener;
    }
.end annotation


# virtual methods
.method public abstract onGLSurfaceChanged(II)V
.end method

.method public abstract passPreviewFrame([B)V
.end method

.method public abstract setPreviewParameters(III)V
.end method

.method public abstract setVideoRecordingParameters(Ljava/lang/String;III)V
.end method

.method public abstract startPreviewProcessing()V
.end method

.method public abstract startVideoRecording()V
.end method

.method public abstract stopPreviewProcessing()V
.end method

.method public abstract stopVideoRecording()V
.end method
