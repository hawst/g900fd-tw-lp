.class abstract Lcom/sec/android/secimaging/EffectGL;
.super Ljava/lang/Object;
.source "EffectGL.java"

# interfaces
.implements Lcom/sec/android/secimaging/IEffectGL;


# instance fields
.field protected mEffectId:I

.field protected mListener:Lcom/sec/android/secimaging/IEffect$EffectListener;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectGL;->mListener:Lcom/sec/android/secimaging/IEffect$EffectListener;

    .line 10
    iput p1, p0, Lcom/sec/android/secimaging/EffectGL;->mEffectId:I

    .line 11
    return-void
.end method


# virtual methods
.method public getEffectID()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/secimaging/EffectGL;->mEffectId:I

    return v0
.end method

.method public isCPU()Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    return v0
.end method

.method public isOpenGL()Z
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x1

    return v0
.end method

.method public setEffectListener(Lcom/sec/android/secimaging/IEffect$EffectListener;)V
    .locals 0
    .param p1, "el"    # Lcom/sec/android/secimaging/IEffect$EffectListener;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/sec/android/secimaging/EffectGL;->mListener:Lcom/sec/android/secimaging/IEffect$EffectListener;

    .line 16
    return-void
.end method
