.class public interface abstract Lcom/sec/android/secimaging/IEffect$EffectListener;
.super Ljava/lang/Object;
.source "IEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/secimaging/IEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EffectListener"
.end annotation


# virtual methods
.method public abstract onEffectComplete(Lcom/sec/android/secimaging/Image;)V
.end method

.method public abstract onEffectError(ILjava/lang/String;)V
.end method

.method public abstract onSaveComplete(Ljava/lang/String;)V
.end method
