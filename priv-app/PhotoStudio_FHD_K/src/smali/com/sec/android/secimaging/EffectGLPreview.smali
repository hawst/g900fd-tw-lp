.class abstract Lcom/sec/android/secimaging/EffectGLPreview;
.super Lcom/sec/android/secimaging/EffectGL;
.source "EffectGLPreview.java"

# interfaces
.implements Lcom/sec/android/secimaging/IEffectGLPreview;


# instance fields
.field protected mPreviewFormat:I

.field protected mPreviewHeight:I

.field protected mPreviewListener:Lcom/sec/android/secimaging/IEffectGLPreview$PreviewEffectListener;

.field protected mPreviewStarted:Z

.field protected mPreviewWidth:I


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0, p1}, Lcom/sec/android/secimaging/EffectGL;-><init>(I)V

    .line 5
    iput v1, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewWidth:I

    .line 6
    iput v1, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewHeight:I

    .line 7
    iput v1, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewFormat:I

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewListener:Lcom/sec/android/secimaging/IEffectGLPreview$PreviewEffectListener;

    .line 11
    iput-boolean v1, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewStarted:Z

    .line 15
    return-void
.end method


# virtual methods
.method public setEffectListener(Lcom/sec/android/secimaging/IEffect$EffectListener;)V
    .locals 1
    .param p1, "el"    # Lcom/sec/android/secimaging/IEffect$EffectListener;

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/sec/android/secimaging/EffectGL;->setEffectListener(Lcom/sec/android/secimaging/IEffect$EffectListener;)V

    .line 20
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/sec/android/secimaging/IEffectGLPreview$PreviewEffectListener;

    if-eqz v0, :cond_0

    .line 21
    check-cast p1, Lcom/sec/android/secimaging/IEffectGLPreview$PreviewEffectListener;

    .end local p1    # "el":Lcom/sec/android/secimaging/IEffect$EffectListener;
    iput-object p1, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewListener:Lcom/sec/android/secimaging/IEffectGLPreview$PreviewEffectListener;

    .line 24
    :goto_0
    return-void

    .line 23
    .restart local p1    # "el":Lcom/sec/android/secimaging/IEffect$EffectListener;
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewListener:Lcom/sec/android/secimaging/IEffectGLPreview$PreviewEffectListener;

    goto :goto_0
.end method

.method public setPreviewParameters(III)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "format"    # I

    .prologue
    .line 28
    iput p1, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewWidth:I

    .line 29
    iput p2, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewHeight:I

    .line 30
    iput p3, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewFormat:I

    .line 31
    return-void
.end method

.method public declared-synchronized startPreviewProcessing()V
    .locals 2

    .prologue
    .line 35
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewWidth:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewHeight:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewFormat:I

    if-nez v0, :cond_1

    .line 36
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Preview parameters must be set before calling startPreviewProcessing"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 37
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewStarted:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 38
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized stopPreviewProcessing()V
    .locals 1

    .prologue
    .line 42
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/android/secimaging/EffectGLPreview;->mPreviewStarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    monitor-exit p0

    return-void

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
