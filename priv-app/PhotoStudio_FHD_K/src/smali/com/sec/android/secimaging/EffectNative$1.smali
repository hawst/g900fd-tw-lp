.class Lcom/sec/android/secimaging/EffectNative$1;
.super Ljava/lang/Object;
.source "EffectNative.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/secimaging/EffectNative;->startLowResProcessing()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/secimaging/EffectNative;


# direct methods
.method constructor <init>(Lcom/sec/android/secimaging/EffectNative;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/secimaging/EffectNative$1;->this$0:Lcom/sec/android/secimaging/EffectNative;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 57
    const-string v1, "secimaging.EffectNative"

    const-string v2, "EffectNative before doEffect"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    iget-object v1, p0, Lcom/sec/android/secimaging/EffectNative$1;->this$0:Lcom/sec/android/secimaging/EffectNative;

    iget-object v1, v1, Lcom/sec/android/secimaging/EffectNative;->mNative:Lcom/sec/android/secimaging/SecImaging;

    iget-object v2, p0, Lcom/sec/android/secimaging/EffectNative$1;->this$0:Lcom/sec/android/secimaging/EffectNative;

    iget-object v2, v2, Lcom/sec/android/secimaging/EffectNative;->mInputImage:Lcom/sec/android/secimaging/Image;

    invoke-virtual {v2}, Lcom/sec/android/secimaging/Image;->asBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/secimaging/SecImaging;->doEffect(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 59
    .local v0, "output":Landroid/graphics/Bitmap;
    const-string v1, "secimaging.EffectNative"

    const-string v2, "EffectNative after doEffect"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    iget-object v1, p0, Lcom/sec/android/secimaging/EffectNative$1;->this$0:Lcom/sec/android/secimaging/EffectNative;

    iget-object v1, v1, Lcom/sec/android/secimaging/EffectNative;->mListener:Lcom/sec/android/secimaging/IEffect$EffectListener;

    if-eqz v1, :cond_0

    .line 62
    iget-object v1, p0, Lcom/sec/android/secimaging/EffectNative$1;->this$0:Lcom/sec/android/secimaging/EffectNative;

    iget-object v1, v1, Lcom/sec/android/secimaging/EffectNative;->mListener:Lcom/sec/android/secimaging/IEffect$EffectListener;

    new-instance v2, Lcom/sec/android/secimaging/Image;

    invoke-direct {v2, v0}, Lcom/sec/android/secimaging/Image;-><init>(Landroid/graphics/Bitmap;)V

    invoke-interface {v1, v2}, Lcom/sec/android/secimaging/IEffect$EffectListener;->onEffectComplete(Lcom/sec/android/secimaging/Image;)V

    .line 64
    :cond_0
    return-void
.end method
