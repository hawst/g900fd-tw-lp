.class Lcom/sec/android/secimaging/EffectNative;
.super Ljava/lang/Object;
.source "EffectNative.java"

# interfaces
.implements Lcom/sec/android/secimaging/IEffect;


# static fields
.field protected static final TAG:Ljava/lang/String; = "secimaging.EffectNative"


# instance fields
.field protected mEffectId:I

.field protected mInputImage:Lcom/sec/android/secimaging/Image;

.field protected mListener:Lcom/sec/android/secimaging/IEffect$EffectListener;

.field protected mNative:Lcom/sec/android/secimaging/SecImaging;

.field protected mOutputBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object v1, p0, Lcom/sec/android/secimaging/EffectNative;->mListener:Lcom/sec/android/secimaging/IEffect$EffectListener;

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/secimaging/EffectNative;->mEffectId:I

    .line 13
    iput-object v1, p0, Lcom/sec/android/secimaging/EffectNative;->mNative:Lcom/sec/android/secimaging/SecImaging;

    .line 15
    iput-object v1, p0, Lcom/sec/android/secimaging/EffectNative;->mInputImage:Lcom/sec/android/secimaging/Image;

    .line 16
    iput-object v1, p0, Lcom/sec/android/secimaging/EffectNative;->mOutputBitmap:Landroid/graphics/Bitmap;

    .line 19
    iput p1, p0, Lcom/sec/android/secimaging/EffectNative;->mEffectId:I

    .line 20
    invoke-direct {p0}, Lcom/sec/android/secimaging/EffectNative;->init()V

    .line 21
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectNative;->mNative:Lcom/sec/android/secimaging/SecImaging;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/sec/android/secimaging/SecImaging;

    iget v1, p0, Lcom/sec/android/secimaging/EffectNative;->mEffectId:I

    invoke-direct {v0, v1}, Lcom/sec/android/secimaging/SecImaging;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectNative;->mNative:Lcom/sec/android/secimaging/SecImaging;

    .line 27
    :cond_0
    return-void
.end method


# virtual methods
.method public getEffectID()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/secimaging/EffectNative;->mEffectId:I

    return v0
.end method

.method public getOption(Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 109
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/secimaging/EffectNative;->mNative:Lcom/sec/android/secimaging/SecImaging;

    invoke-virtual {v1, p1}, Lcom/sec/android/secimaging/SecImaging;->native_getOptionValue(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 113
    :goto_0
    return-object v1

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "rex":Ljava/lang/RuntimeException;
    const-string v1, "secimaging.EffectNative"

    const-string v2, "Failed to get option from native code!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 113
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getOptions()Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/secimaging/EffectOption;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 90
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/secimaging/EffectNative;->mNative:Lcom/sec/android/secimaging/SecImaging;

    invoke-virtual {v8}, Lcom/sec/android/secimaging/SecImaging;->native_getOptions()[Lcom/sec/android/secimaging/EffectOption;

    move-result-object v5

    .line 91
    .local v5, "opts":[Lcom/sec/android/secimaging/EffectOption;
    if-nez v5, :cond_1

    .line 92
    const-string v8, "secimaging.EffectNative"

    const-string v9, "Effect does not have any configurable options or failed to get the list"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v7

    .line 102
    .end local v5    # "opts":[Lcom/sec/android/secimaging/EffectOption;
    :cond_0
    :goto_0
    return-object v4

    .line 95
    .restart local v5    # "opts":[Lcom/sec/android/secimaging/EffectOption;
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    array-length v8, v5

    invoke-direct {v4, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 96
    .local v4, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secimaging/EffectOption;>;"
    move-object v0, v5

    .local v0, "arr$":[Lcom/sec/android/secimaging/EffectOption;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 97
    .local v3, "opt":Lcom/sec/android/secimaging/EffectOption;
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 99
    .end local v0    # "arr$":[Lcom/sec/android/secimaging/EffectOption;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "opt":Lcom/sec/android/secimaging/EffectOption;
    .end local v4    # "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secimaging/EffectOption;>;"
    .end local v5    # "opts":[Lcom/sec/android/secimaging/EffectOption;
    :catch_0
    move-exception v6

    .line 100
    .local v6, "rex":Ljava/lang/RuntimeException;
    const-string v8, "secimaging.EffectNative"

    const-string v9, "Failed to retrieve options list from native code!"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-virtual {v6}, Ljava/lang/RuntimeException;->printStackTrace()V

    move-object v4, v7

    .line 102
    goto :goto_0
.end method

.method getValue(I)I
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectNative;->mNative:Lcom/sec/android/secimaging/SecImaging;

    invoke-virtual {v0, p1}, Lcom/sec/android/secimaging/SecImaging;->getValue(I)I

    move-result v0

    return v0
.end method

.method public isCPU()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    return v0
.end method

.method public isOpenGL()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectNative;->mNative:Lcom/sec/android/secimaging/SecImaging;

    invoke-virtual {v0}, Lcom/sec/android/secimaging/SecImaging;->clear()V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/secimaging/EffectNative;->mNative:Lcom/sec/android/secimaging/SecImaging;

    .line 33
    return-void
.end method

.method public declared-synchronized setEffectListener(Lcom/sec/android/secimaging/IEffect$EffectListener;)V
    .locals 1
    .param p1, "el"    # Lcom/sec/android/secimaging/IEffect$EffectListener;

    .prologue
    .line 42
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/sec/android/secimaging/EffectNative;->mListener:Lcom/sec/android/secimaging/IEffect$EffectListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    monitor-exit p0

    return-void

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setImage(Lcom/sec/android/secimaging/Image;)V
    .locals 1
    .param p1, "img"    # Lcom/sec/android/secimaging/Image;

    .prologue
    .line 47
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/sec/android/secimaging/EffectNative;->mInputImage:Lcom/sec/android/secimaging/Image;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    monitor-exit p0

    return-void

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setOption(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 119
    if-nez p2, :cond_0

    .line 120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null argument in setOption"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_0
    instance-of v0, p2, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 122
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectNative;->mNative:Lcom/sec/android/secimaging/SecImaging;

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/secimaging/SecImaging;->native_setOptionI(Ljava/lang/String;I)V

    .line 129
    :goto_0
    return-void

    .line 123
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_1
    instance-of v0, p2, Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 124
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectNative;->mNative:Lcom/sec/android/secimaging/SecImaging;

    check-cast p2, Ljava/lang/Float;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/secimaging/SecImaging;->native_setOptionF(Ljava/lang/String;F)V

    goto :goto_0

    .line 125
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_2
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 126
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectNative;->mNative:Lcom/sec/android/secimaging/SecImaging;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/secimaging/SecImaging;->native_setOptionS(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 128
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported value type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method setValue(II)V
    .locals 1
    .param p1, "key"    # I
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/secimaging/EffectNative;->mNative:Lcom/sec/android/secimaging/SecImaging;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/secimaging/SecImaging;->setValue(II)V

    .line 86
    return-void
.end method

.method public declared-synchronized startLowResProcessing()V
    .locals 2

    .prologue
    .line 52
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/secimaging/EffectNative;->init()V

    .line 54
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/secimaging/EffectNative$1;

    invoke-direct {v1, p0}, Lcom/sec/android/secimaging/EffectNative$1;-><init>(Lcom/sec/android/secimaging/EffectNative;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 67
    .local v0, "td":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    monitor-exit p0

    return-void

    .line 52
    .end local v0    # "td":Ljava/lang/Thread;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public startProcessing(Ljava/lang/String;I)V
    .locals 2
    .param p1, "dest"    # Ljava/lang/String;
    .param p2, "quality"    # I

    .prologue
    .line 134
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not implemented yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
