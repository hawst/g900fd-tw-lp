.class public Lcom/sec/android/app/autoreframe/jni;
.super Ljava/lang/Object;
.source "jni.java"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    :try_start_0
    const-string v1, "AutoReframe"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 20
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 22
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Load library fail : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native getSuggestedImageInfo([III[I[I[I[I[BI[BI[BI[BI)V
.end method
