.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenImageData;
.super Lcom/sec/android/mimage/photoretouching/Core/ImageData;
.source "PenActivity.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DrawAllocation"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PenImageData"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;[IIIII)V
    .locals 9
    .param p2, "buffer"    # [I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "viewWidth"    # I
    .param p6, "viewHeight"    # I

    .prologue
    .line 2119
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenImageData;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    .line 2120
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;-><init>()V

    .line 2121
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v5, 0x0

    const/4 v6, 0x4

    move-object v0, p0

    move-object v2, p2

    move v3, p5

    move v4, p6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenImageData;->setDecodedImage(Landroid/content/Context;[IIILandroid/net/Uri;I)V

    .line 2124
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenImageData;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v8

    .line 2126
    .local v8, "roi":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenImageData;->getPreviewWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenImageData;->getPreviewHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$10(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/graphics/Bitmap;)V

    .line 2127
    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBitmap:Landroid/graphics/Bitmap;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$11(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    const/4 v2, 0x0

    .line 2128
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenImageData;->getPreviewWidth()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenImageData;->getPreviewWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenImageData;->getPreviewHeight()I

    move-result v7

    .line 2127
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 2130
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$12(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;I)V

    .line 2131
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$13(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;I)V

    .line 2132
    return-void
.end method
