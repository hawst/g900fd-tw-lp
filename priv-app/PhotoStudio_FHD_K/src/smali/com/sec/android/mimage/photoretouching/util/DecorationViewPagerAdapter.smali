.class public Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "DecorationViewPagerAdapter.java"


# instance fields
.field gridAdapter:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

.field mAdapterList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;",
            ">;"
        }
    .end annotation
.end field

.field mContext:Landroid/app/Activity;

.field mCurrenttype:I

.field private mFrameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field mGridList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/GridView;",
            ">;"
        }
    .end annotation
.end field

.field mGridView:Landroid/widget/GridView;

.field private mNumColumn:I

.field private mStampList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field menuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

.field private noRecentAdapter:Landroid/widget/BaseAdapter;

.field private noRecentType:I

.field pages:I

.field type:[I


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;[ILjava/util/ArrayList;)V
    .locals 2
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "mPages"    # I
    .param p3, "manager"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    .param p4, "mtypes"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;",
            "[I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p5, "mList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mNumColumn:I

    .line 35
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mFrameList:Ljava/util/ArrayList;

    .line 36
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mStampList:Ljava/util/ArrayList;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mGridList:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mAdapterList:Ljava/util/ArrayList;

    .line 353
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->noRecentType:I

    .line 368
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter$1;-><init>(Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->noRecentAdapter:Landroid/widget/BaseAdapter;

    .line 40
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->pages:I

    .line 41
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    .line 42
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->menuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 43
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->type:[I

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mFrameList:Ljava/util/ArrayList;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mStampList:Ljava/util/ArrayList;

    .line 46
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 58
    :pswitch_0
    return-void

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x31200000
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;)I
    .locals 1

    .prologue
    .line 353
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->noRecentType:I

    return v0
.end method

.method private checkForRecent(Landroid/widget/GridView;I)V
    .locals 3
    .param p1, "gridView"    # Landroid/widget/GridView;
    .param p2, "type"    # I

    .prologue
    const/4 v1, 0x1

    .line 356
    const/4 v2, 0x5

    if-eq p2, v2, :cond_0

    .line 357
    const/16 v2, 0xe

    if-eq p2, v2, :cond_0

    .line 358
    if-eq p2, v1, :cond_0

    const/16 v2, 0x9

    if-ne p2, v2, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->menuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 359
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 356
    if-ge v2, v1, :cond_2

    move v0, v1

    .line 360
    .local v0, "noRecent":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 361
    invoke-virtual {p1, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 362
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->noRecentType:I

    .line 363
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->noRecentAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {p1, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 366
    :cond_1
    return-void

    .line 356
    .end local v0    # "noRecent":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 417
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mGridList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_4

    .line 421
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mAdapterList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_5

    .line 426
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mGridList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 427
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mAdapterList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 428
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mGridView:Landroid/widget/GridView;

    if-eqz v2, :cond_2

    .line 429
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v2, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 431
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->gridAdapter:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    if-eqz v2, :cond_3

    .line 432
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->gridAdapter:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->destroy()V

    .line 434
    :cond_3
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mGridView:Landroid/widget/GridView;

    .line 435
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->gridAdapter:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    .line 436
    return-void

    .line 417
    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    .line 418
    .local v1, "gridView":Landroid/widget/GridView;
    if-eqz v1, :cond_0

    .line 419
    invoke-virtual {v1, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 421
    .end local v1    # "gridView":Landroid/widget/GridView;
    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    .line 422
    .local v0, "adapter":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;
    if-eqz v0, :cond_1

    .line 423
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->destroy()V

    goto :goto_1
.end method

.method public destroyItem(Landroid/view/View;ILjava/lang/Object;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Ljava/lang/Object;

    .prologue
    .line 104
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "arg0":Landroid/view/View;
    check-cast p3, Landroid/view/View;

    .end local p3    # "arg2":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 105
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->pages:I

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5
    .param p1, "collection"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 68
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 69
    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 68
    check-cast v0, Landroid/view/LayoutInflater;

    .line 71
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030046

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 73
    .local v1, "view":Landroid/view/View;
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 74
    const v2, 0x7f0900dd

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/GridView;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mGridView:Landroid/widget/GridView;

    .line 76
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 94
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mGridList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "collection":Landroid/view/ViewGroup;
    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;I)V

    .line 99
    return-object v1

    .line 79
    .restart local p1    # "collection":Landroid/view/ViewGroup;
    :sswitch_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->type:[I

    aget v2, v2, p2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mGridView:Landroid/widget/GridView;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->setThumbnailContext(ILandroid/widget/GridView;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 82
    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->type:[I

    aget v2, v2, p2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mGridView:Landroid/widget/GridView;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mStampList:Ljava/util/ArrayList;

    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->setThumbnailContext(ILandroid/widget/GridView;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 85
    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->type:[I

    aget v2, v2, p2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->setImageContext(ILandroid/widget/GridView;)V

    goto :goto_0

    .line 88
    :sswitch_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->type:[I

    aget v2, v2, p2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->setImageContext(ILandroid/widget/GridView;)V

    goto :goto_0

    .line 91
    :sswitch_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->type:[I

    aget v2, v2, p2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->setImageContext(ILandroid/widget/GridView;)V

    goto :goto_0

    .line 76
    nop

    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_2
        0x31200000 -> :sswitch_0
        0x31300000 -> :sswitch_3
        0x31400000 -> :sswitch_4
        0x31500000 -> :sswitch_1
    .end sparse-switch
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 109
    check-cast p2, Landroid/view/View;

    .end local p2    # "arg1":Ljava/lang/Object;
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    return-object v0
.end method

.method public setImageContext(ILandroid/widget/GridView;)V
    .locals 12
    .param p1, "type"    # I
    .param p2, "mGridView"    # Landroid/widget/GridView;

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x0

    .line 132
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->menuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/4 v5, 0x0

    move-object v2, p2

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;-><init>(Landroid/content/Context;Landroid/widget/GridView;ILcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->gridAdapter:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    .line 133
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->gridAdapter:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    invoke-virtual {p2, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mAdapterList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->gridAdapter:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    const/4 v6, 0x0

    .line 137
    .local v6, "hSpacing":I
    const/4 v9, 0x0

    .line 138
    .local v9, "vSpacing":I
    const/4 v7, 0x0

    .line 139
    .local v7, "margin":I
    const/4 v8, 0x0

    .line 141
    .local v8, "side_margin":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 142
    move v8, v7

    .line 144
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 183
    :goto_0
    invoke-direct {p0, p2, p1}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->checkForRecent(Landroid/widget/GridView;I)V

    .line 184
    invoke-virtual {p2, v6}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    .line 185
    invoke-virtual {p2, v9}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    .line 186
    invoke-virtual {p2, v8, v7, v8, v10}, Landroid/widget/GridView;->setPadding(IIII)V

    .line 187
    invoke-virtual {p2, v10}, Landroid/widget/GridView;->setClipToPadding(Z)V

    .line 188
    const/high16 v0, 0x2000000

    invoke-virtual {p2, v0}, Landroid/widget/GridView;->setScrollBarStyle(I)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->gridAdapter:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->notifyDataSetChanged()V

    .line 226
    invoke-virtual {p2}, Landroid/widget/GridView;->invalidateViews()V

    .line 227
    return-void

    .line 147
    :sswitch_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mNumColumn:I

    .line 149
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mNumColumn:I

    invoke-virtual {p2, v0}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 151
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05002a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 152
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05002c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 153
    goto :goto_0

    .line 154
    :cond_0
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mNumColumn:I

    .line 155
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mNumColumn:I

    invoke-virtual {p2, v0}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 157
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 158
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 160
    goto/16 :goto_0

    .line 162
    :sswitch_1
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mNumColumn:I

    .line 164
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mNumColumn:I

    invoke-virtual {p2, v0}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 166
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 167
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050037

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 168
    goto/16 :goto_0

    .line 169
    :cond_1
    iput v11, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mNumColumn:I

    .line 170
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mNumColumn:I

    invoke-virtual {p2, v0}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050031

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 172
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 173
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050036

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 175
    goto/16 :goto_0

    .line 177
    :sswitch_2
    iput v11, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mNumColumn:I

    .line 178
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mNumColumn:I

    invoke-virtual {p2, v0}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 180
    move v9, v6

    goto/16 :goto_0

    .line 144
    nop

    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_0
        0x31300000 -> :sswitch_1
        0x31400000 -> :sswitch_2
    .end sparse-switch
.end method

.method public setThumbnailContext(ILandroid/widget/GridView;Ljava/util/ArrayList;)V
    .locals 11
    .param p1, "type"    # I
    .param p2, "mGridView"    # Landroid/widget/GridView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/widget/GridView;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 258
    .local p3, "mViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->menuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-object v2, p2

    move v3, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;-><init>(Landroid/content/Context;Landroid/widget/GridView;ILcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->gridAdapter:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    .line 259
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->gridAdapter:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    invoke-virtual {p2, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 261
    const/4 v6, 0x0

    .line 262
    .local v6, "hSpacing":I
    const/4 v10, 0x0

    .line 263
    .local v10, "vSpacing":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 264
    .local v8, "margin":I
    const/4 v7, 0x0

    .line 265
    .local v7, "leftMargin":I
    const/4 v9, 0x0

    .line 266
    .local v9, "rightMargin":I
    packed-switch p1, :pswitch_data_0

    .line 313
    :goto_0
    :pswitch_0
    invoke-direct {p0, p2, p1}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->checkForRecent(Landroid/widget/GridView;I)V

    .line 314
    invoke-virtual {p2, v6}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    .line 315
    invoke-virtual {p2, v10}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    .line 316
    const/4 v0, 0x0

    invoke-virtual {p2, v7, v8, v9, v0}, Landroid/widget/GridView;->setPadding(IIII)V

    .line 317
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/GridView;->setClipToPadding(Z)V

    .line 318
    const/high16 v0, 0x2000000

    invoke-virtual {p2, v0}, Landroid/widget/GridView;->setScrollBarStyle(I)V

    .line 348
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->gridAdapter:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->notifyDataSetChanged()V

    .line 349
    invoke-virtual {p2}, Landroid/widget/GridView;->invalidateViews()V

    .line 350
    return-void

    .line 272
    :pswitch_1
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    const/4 v0, 0x6

    invoke-virtual {p2, v0}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 276
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05002a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 277
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05002f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 278
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 279
    goto :goto_0

    .line 282
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 284
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 285
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 286
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 288
    goto/16 :goto_0

    .line 294
    :pswitch_2
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 296
    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 298
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 299
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05002f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 300
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 301
    goto/16 :goto_0

    .line 304
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 306
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 307
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 308
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    goto/16 :goto_0

    .line 266
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
