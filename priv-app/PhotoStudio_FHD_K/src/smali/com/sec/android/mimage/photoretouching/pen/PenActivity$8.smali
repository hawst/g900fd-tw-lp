.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$8;
.super Ljava/lang/Object;
.source "PenActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->initDialogs()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$8;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    .line 765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 769
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$8;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$25(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isUndoable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 770
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$8;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$25(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->undoAll()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    .line 771
    .local v0, "userData":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$8;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    .line 772
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$8;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$26(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V

    .line 774
    .end local v0    # "userData":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$8;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$8;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mUndoStarckEmpty:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$27(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->refreshUndoRedoBtnState(Z)V

    .line 775
    return-void
.end method
