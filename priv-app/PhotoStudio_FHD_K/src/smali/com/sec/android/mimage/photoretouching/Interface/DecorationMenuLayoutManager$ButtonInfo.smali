.class public Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;
.super Ljava/lang/Object;
.source "DecorationMenuLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ButtonInfo"
.end annotation


# instance fields
.field private mButtonId:I

.field private mButtonIndex:I

.field private mRecentUsed:Z

.field private mUse:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;IIII)V
    .locals 4
    .param p2, "buttonId"    # I
    .param p3, "buttonIdx"    # I
    .param p4, "recentUsed"    # I
    .param p5, "use"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 861
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 856
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->mButtonId:I

    .line 857
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->mButtonIndex:I

    .line 858
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->mRecentUsed:Z

    .line 859
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->mUse:Z

    .line 862
    const/4 v0, 0x0

    .line 863
    .local v0, "bRecentUsed":Z
    const/4 v1, 0x0

    .line 864
    .local v1, "bUse":Z
    if-lez p4, :cond_0

    .line 865
    const/4 v0, 0x1

    .line 866
    :cond_0
    if-lez p5, :cond_1

    .line 867
    const/4 v1, 0x1

    .line 868
    :cond_1
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->mButtonId:I

    .line 869
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->mButtonIndex:I

    .line 870
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->mRecentUsed:Z

    .line 871
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->mUse:Z

    .line 872
    return-void
.end method


# virtual methods
.method public getButtonId()I
    .locals 1

    .prologue
    .line 875
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->mButtonId:I

    return v0
.end method

.method public getButtonIndex()I
    .locals 1

    .prologue
    .line 879
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->mButtonIndex:I

    return v0
.end method

.method public getButtonRecentUsed()Z
    .locals 1

    .prologue
    .line 883
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->mRecentUsed:Z

    return v0
.end method

.method public getButtonUse()Z
    .locals 1

    .prologue
    .line 887
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->mUse:Z

    return v0
.end method
