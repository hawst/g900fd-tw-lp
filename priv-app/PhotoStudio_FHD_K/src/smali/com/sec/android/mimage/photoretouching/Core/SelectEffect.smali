.class public Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;
.super Ljava/lang/Object;
.source "SelectEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;
    }
.end annotation


# static fields
.field public static final BRUSH_ADD_MASK:I = 0x2

.field public static final BRUSH_DEL_MASK:I = 0x3

.field public static final LASSO_ADD_MASK:I = 0x4

.field public static final LASSO_DEL_MASK:I = 0x5

.field public static final SMART_ADD_MASK:I = 0x0

.field public static final SMART_DEL_MASK:I = 0x1

.field public static final TOUCH_DOWN:I = 0x0

.field public static final TOUCH_MOVE:I = 0x0

.field public static final TOUCH_UP:I = 0x1


# instance fields
.field private final MAX_SIZE:I

.field private final TOLERANCE_TIME:I

.field private mBrushSize:I

.field private mCntr:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

.field private mContourFlag:Z

.field private mDrawLinePaint:Landroid/graphics/Paint;

.field private mDrawLinePaint1:Landroid/graphics/Paint;

.field private mFirstPt:Landroid/graphics/Point;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mIsDoingSelection:Z

.field private mLassoPath:Landroid/graphics/Path;

.field private mPathEffect:Landroid/graphics/DashPathEffect;

.field private mPrePt:Landroid/graphics/Point;

.field private mPreviewHeight:I

.field private mPreviewMask:[B

.field private mPreviewPath:Landroid/graphics/Path;

.field public mPreviewRoi:Landroid/graphics/Rect;

.field private mPreviewWidth:I

.field private mResetMask:Z

.field private mSelectDrawType:I

.field private mSelectRect:Landroid/graphics/RectF;

.field private mSelectType:I

.field private mStartSelect:Z

.field private mTempRoi:Landroid/graphics/Rect;

.field private mTempTouchDown:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1093
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mBrushSize:I

    .line 1094
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    .line 1095
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    .line 1096
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 1098
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    .line 1099
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    .line 1100
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    .line 1104
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1105
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mCntr:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    .line 1107
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempTouchDown:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;

    .line 1108
    const/16 v0, 0x46

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->MAX_SIZE:I

    .line 1112
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 1113
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 1114
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPathEffect:Landroid/graphics/DashPathEffect;

    .line 1116
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mStartSelect:Z

    .line 1117
    const v0, 0x14001407

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectType:I

    .line 1118
    const v0, 0x14001404

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectDrawType:I

    .line 1120
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mResetMask:Z

    .line 1122
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mLassoPath:Landroid/graphics/Path;

    .line 1123
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewPath:Landroid/graphics/Path;

    .line 1125
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mIsDoingSelection:Z

    .line 1126
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mContourFlag:Z

    .line 1137
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->TOLERANCE_TIME:I

    .line 33
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const/high16 v2, 0x40400000    # 3.0f

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPathEffect:Landroid/graphics/DashPathEffect;

    .line 34
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 35
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 36
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPathEffect:Landroid/graphics/DashPathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 37
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 40
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 41
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 42
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 43
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 45
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    .line 46
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    .line 48
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    .line 49
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mFirstPt:Landroid/graphics/Point;

    .line 51
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mLassoPath:Landroid/graphics/Path;

    .line 52
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewPath:Landroid/graphics/Path;

    .line 53
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mCntr:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    .line 54
    return-void

    .line 33
    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;Z)V
    .locals 0

    .prologue
    .line 1125
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mIsDoingSelection:Z

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 1112
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I
    .locals 1

    .prologue
    .line 1118
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectDrawType:I

    return v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)[B
    .locals 1

    .prologue
    .line 1094
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I
    .locals 1

    .prologue
    .line 1095
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I
    .locals 1

    .prologue
    .line 1096
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    return v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 1110
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 1122
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mLassoPath:Landroid/graphics/Path;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1100
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 1110
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mFirstPt:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 1123
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewPath:Landroid/graphics/Path;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/DashPathEffect;
    .locals 1

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPathEffect:Landroid/graphics/DashPathEffect;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;Landroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 1113
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 1113
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 1104
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;Z)V
    .locals 0

    .prologue
    .line 1116
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mStartSelect:Z

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I
    .locals 1

    .prologue
    .line 1093
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mBrushSize:I

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;I)V
    .locals 0

    .prologue
    .line 1060
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->updataBrushSize(I)V

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I
    .locals 1

    .prologue
    .line 1117
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectType:I

    return v0
.end method

.method private calcRemainRoi(Landroid/graphics/Rect;[BII)V
    .locals 7
    .param p1, "roi"    # Landroid/graphics/Rect;
    .param p2, "mask"    # [B
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 914
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/jni/Util;->get_objectROI(Landroid/graphics/Rect;)V

    .line 916
    iget v4, p1, Landroid/graphics/Rect;->right:I

    .line 917
    .local v4, "minX":I
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 918
    .local v5, "minY":I
    const/4 v2, 0x0

    .line 919
    .local v2, "maxX":I
    const/4 v3, 0x0

    .line 921
    .local v3, "maxY":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p4, :cond_0

    .line 935
    iput v4, p1, Landroid/graphics/Rect;->left:I

    iput v5, p1, Landroid/graphics/Rect;->top:I

    iput v2, p1, Landroid/graphics/Rect;->right:I

    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 936
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/jni/Util;->set_objectROI(Landroid/graphics/Rect;)V

    .line 937
    return-void

    .line 922
    :cond_0
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-lt v1, p3, :cond_1

    .line 921
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 923
    :cond_1
    mul-int v6, v0, p3

    add-int/2addr v6, v1

    aget-byte v6, p2, v6

    if-eqz v6, :cond_5

    .line 924
    if-le v4, v1, :cond_2

    .line 925
    move v4, v1

    .line 926
    :cond_2
    if-le v5, v0, :cond_3

    .line 927
    move v5, v0

    .line 928
    :cond_3
    if-ge v2, v1, :cond_4

    .line 929
    move v2, v1

    .line 930
    :cond_4
    if-ge v3, v0, :cond_5

    .line 931
    move v3, v0

    .line 922
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private calculateMinBrushSize(F)F
    .locals 4
    .param p1, "size"    # F

    .prologue
    .line 760
    const/high16 v0, 0x41a00000    # 20.0f

    .line 761
    .local v0, "minBrushSize":F
    const/4 v1, 0x0

    .line 763
    .local v1, "tempMinBrushSize":F
    mul-float v2, v0, p1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getBrushMax()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 764
    sub-float/2addr v0, v1

    .line 765
    return v0
.end method

.method private contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Z)V
    .locals 8
    .param p1, "previewMask"    # [B
    .param p2, "previewWidth"    # I
    .param p3, "previewHeight"    # I
    .param p4, "roi"    # Landroid/graphics/Rect;
    .param p5, "previewPath"    # Landroid/graphics/Path;
    .param p6, "imageData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .param p7, "isSub"    # Z

    .prologue
    .line 1072
    invoke-interface {p6}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawPathList()Landroid/graphics/Path;

    move-result-object v7

    .line 1073
    .local v7, "list":Landroid/graphics/Path;
    if-eqz v7, :cond_0

    .line 1075
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mCntr:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p7

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Z)V

    .line 1081
    invoke-virtual {v7, p5}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    .line 1091
    :cond_0
    return-void
.end method

.method private drawSelect(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "viewTransformMatrix"    # Landroid/graphics/Matrix;
    .param p3, "selectDrawType"    # I

    .prologue
    const/4 v8, -0x1

    const/high16 v7, -0x1000000

    const/high16 v6, 0x40000000    # 2.0f

    .line 812
    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mIsDoingSelection:Z

    if-nez v4, :cond_2

    .line 814
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 815
    .local v2, "p":Landroid/graphics/Path;
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 821
    .local v1, "m":Landroid/graphics/Matrix;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 824
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawPathList()Landroid/graphics/Path;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 825
    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mContourFlag:Z

    if-eqz v4, :cond_0

    .line 827
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 828
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPathEffect:Landroid/graphics/DashPathEffect;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 829
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 830
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 832
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 833
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 834
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 835
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 849
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 850
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 851
    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mContourFlag:Z

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    :goto_1
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mContourFlag:Z

    .line 852
    invoke-virtual {v2}, Landroid/graphics/Path;->rewind()V

    .line 911
    .end local v1    # "m":Landroid/graphics/Matrix;
    .end local v2    # "p":Landroid/graphics/Path;
    :goto_2
    return-void

    .line 839
    .restart local v1    # "m":Landroid/graphics/Matrix;
    .restart local v2    # "p":Landroid/graphics/Path;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 840
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPathEffect:Landroid/graphics/DashPathEffect;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 841
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 842
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 844
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 845
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {v4, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 846
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 847
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_0

    .line 851
    :cond_1
    const/4 v4, 0x1

    goto :goto_1

    .line 857
    .end local v1    # "m":Landroid/graphics/Matrix;
    .end local v2    # "p":Landroid/graphics/Path;
    :cond_2
    packed-switch p3, :pswitch_data_0

    :pswitch_0
    goto :goto_2

    .line 859
    :pswitch_1
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 860
    .local v3, "r":Landroid/graphics/RectF;
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 862
    .local v0, "dst":Landroid/graphics/RectF;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    :goto_3
    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 863
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    :goto_4
    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 864
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    :goto_5
    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 865
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_6

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    :goto_6
    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 866
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 870
    .restart local v1    # "m":Landroid/graphics/Matrix;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 872
    invoke-virtual {v1, v0, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 873
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 874
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 862
    .end local v1    # "m":Landroid/graphics/Matrix;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    goto :goto_3

    .line 863
    :cond_4
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    goto :goto_4

    .line 864
    :cond_5
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    goto :goto_5

    .line 865
    :cond_6
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    goto :goto_6

    .line 877
    .end local v0    # "dst":Landroid/graphics/RectF;
    .end local v3    # "r":Landroid/graphics/RectF;
    :pswitch_2
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 878
    .restart local v3    # "r":Landroid/graphics/RectF;
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 879
    .restart local v0    # "dst":Landroid/graphics/RectF;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_7

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    :goto_7
    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 880
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_8

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    :goto_8
    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 881
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_9

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    :goto_9
    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 882
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_a

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    :goto_a
    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 883
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 887
    .restart local v1    # "m":Landroid/graphics/Matrix;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 889
    invoke-virtual {v1, v0, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 890
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 891
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 879
    .end local v1    # "m":Landroid/graphics/Matrix;
    :cond_7
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    goto :goto_7

    .line 880
    :cond_8
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    goto :goto_8

    .line 881
    :cond_9
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    goto :goto_9

    .line 882
    :cond_a
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    goto :goto_a

    .line 894
    .end local v0    # "dst":Landroid/graphics/RectF;
    .end local v3    # "r":Landroid/graphics/RectF;
    :pswitch_3
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 895
    .restart local v2    # "p":Landroid/graphics/Path;
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 901
    .restart local v1    # "m":Landroid/graphics/Matrix;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 903
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mLassoPath:Landroid/graphics/Path;

    invoke-virtual {v4, v1, v2}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 904
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 905
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 906
    invoke-virtual {v2}, Landroid/graphics/Path;->rewind()V

    goto/16 :goto_2

    .line 857
    :pswitch_data_0
    .packed-switch 0x14001402
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private setRectCircleMask([BLandroid/graphics/Rect;IIFFFII)V
    .locals 26
    .param p1, "mask"    # [B
    .param p2, "roi"    # Landroid/graphics/Rect;
    .param p3, "previewWidth"    # I
    .param p4, "previewHeight"    # I
    .param p5, "leftFromOrg"    # F
    .param p6, "topFromOrg"    # F
    .param p7, "scale"    # F
    .param p8, "selectType"    # I
    .param p9, "selectDrawType"    # I

    .prologue
    .line 965
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_6

    .line 966
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v0, v5, Landroid/graphics/RectF;->right:F

    move/from16 v22, v0

    .line 967
    .local v22, "startX":F
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v0, v5, Landroid/graphics/RectF;->left:F

    move/from16 v17, v0

    .line 974
    .local v17, "endX":F
    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_7

    .line 975
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v0, v5, Landroid/graphics/RectF;->bottom:F

    move/from16 v23, v0

    .line 976
    .local v23, "startY":F
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v0, v5, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    .line 983
    .local v18, "endY":F
    :goto_1
    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getRealPosition(FF)Landroid/graphics/Point;

    move-result-object v21

    .line 984
    .local v21, "p":Landroid/graphics/Point;
    move-object/from16 v0, v21

    iget v5, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v5

    move/from16 v22, v0

    move-object/from16 v0, v21

    iget v5, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v5

    move/from16 v23, v0

    .line 985
    const/4 v5, 0x0

    cmpg-float v5, v22, v5

    if-gez v5, :cond_0

    .line 986
    const/16 v22, 0x0

    .line 987
    :cond_0
    const/4 v5, 0x0

    cmpg-float v5, v23, v5

    if-gez v5, :cond_1

    .line 988
    const/16 v23, 0x0

    .line 989
    :cond_1
    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getRealPosition(FF)Landroid/graphics/Point;

    move-result-object v21

    .line 990
    move-object/from16 v0, v21

    iget v5, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v5

    move/from16 v17, v0

    move-object/from16 v0, v21

    iget v5, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v5

    move/from16 v18, v0

    .line 991
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    cmpl-float v5, v17, v5

    if-lez v5, :cond_2

    .line 992
    const-string v5, "mytag"

    const-string v6, "endXtest"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 993
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    int-to-float v0, v5

    move/from16 v17, v0

    .line 995
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    cmpl-float v5, v18, v5

    if-lez v5, :cond_3

    .line 996
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    int-to-float v0, v5

    move/from16 v18, v0

    .line 999
    :cond_3
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    int-to-float v5, v5

    cmpl-float v5, v17, v5

    if-ltz v5, :cond_4

    .line 1000
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    add-int/lit8 v5, v5, -0x1

    int-to-float v0, v5

    move/from16 v17, v0

    .line 1001
    :cond_4
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    int-to-float v5, v5

    cmpl-float v5, v18, v5

    if-ltz v5, :cond_5

    .line 1002
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    add-int/lit8 v5, v5, -0x1

    int-to-float v0, v5

    move/from16 v18, v0

    .line 1005
    :cond_5
    move/from16 v24, p3

    .line 1006
    .local v24, "width":I
    const v5, 0x14001402

    move/from16 v0, p9

    if-ne v0, v5, :cond_d

    .line 1007
    const v5, 0x14001408

    move/from16 v0, p8

    if-ne v0, v5, :cond_a

    .line 1008
    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v19, v0

    .local v19, "i":I
    :goto_2
    move/from16 v0, v19

    int-to-float v5, v0

    cmpg-float v5, v5, v18

    if-lez v5, :cond_8

    .line 1013
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move/from16 v3, p3

    move/from16 v4, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->calcRemainRoi(Landroid/graphics/Rect;[BII)V

    .line 1055
    :goto_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    const/4 v6, 0x0

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 1056
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    const/4 v6, 0x0

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 1057
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    const/4 v6, 0x0

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 1058
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    const/4 v6, 0x0

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 1059
    return-void

    .line 970
    .end local v17    # "endX":F
    .end local v18    # "endY":F
    .end local v19    # "i":I
    .end local v21    # "p":Landroid/graphics/Point;
    .end local v22    # "startX":F
    .end local v23    # "startY":F
    .end local v24    # "width":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v0, v5, Landroid/graphics/RectF;->left:F

    move/from16 v22, v0

    .line 971
    .restart local v22    # "startX":F
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v0, v5, Landroid/graphics/RectF;->right:F

    move/from16 v17, v0

    .restart local v17    # "endX":F
    goto/16 :goto_0

    .line 979
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v0, v5, Landroid/graphics/RectF;->top:F

    move/from16 v23, v0

    .line 980
    .restart local v23    # "startY":F
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    iget v0, v5, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    .restart local v18    # "endY":F
    goto/16 :goto_1

    .line 1009
    .restart local v19    # "i":I
    .restart local v21    # "p":Landroid/graphics/Point;
    .restart local v24    # "width":I
    :cond_8
    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v20, v0

    .local v20, "j":I
    :goto_4
    move/from16 v0, v20

    int-to-float v5, v0

    cmpg-float v5, v5, v17

    if-lez v5, :cond_9

    .line 1008
    add-int/lit8 v19, v19, 0x1

    goto :goto_2

    .line 1010
    :cond_9
    mul-int v5, v19, v24

    add-int v5, v5, v20

    const/4 v6, 0x0

    aput-byte v6, p1, v5

    .line 1009
    add-int/lit8 v20, v20, 0x1

    goto :goto_4

    .line 1016
    .end local v19    # "i":I
    .end local v20    # "j":I
    :cond_a
    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v19, v0

    .restart local v19    # "i":I
    :goto_5
    move/from16 v0, v19

    int-to-float v5, v0

    cmpg-float v5, v5, v18

    if-lez v5, :cond_b

    .line 1021
    move/from16 v0, v22

    float-to-int v7, v0

    move/from16 v0, v23

    float-to-int v8, v0

    move/from16 v0, v17

    float-to-int v9, v0

    move/from16 v0, v18

    float-to-int v10, v0

    move-object/from16 v5, p0

    move-object/from16 v6, p2

    move/from16 v11, p8

    invoke-direct/range {v5 .. v11}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->unionRoi(Landroid/graphics/Rect;IIIII)V

    goto :goto_3

    .line 1017
    :cond_b
    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v20, v0

    .restart local v20    # "j":I
    :goto_6
    move/from16 v0, v20

    int-to-float v5, v0

    cmpg-float v5, v5, v17

    if-lez v5, :cond_c

    .line 1016
    add-int/lit8 v19, v19, 0x1

    goto :goto_5

    .line 1018
    :cond_c
    mul-int v5, v19, v24

    add-int v5, v5, v20

    const/4 v6, 0x1

    aput-byte v6, p1, v5

    .line 1017
    add-int/lit8 v20, v20, 0x1

    goto :goto_6

    .line 1026
    .end local v19    # "i":I
    .end local v20    # "j":I
    :cond_d
    sub-float v5, v17, v22

    const/high16 v6, 0x40000000    # 2.0f

    div-float v13, v5, v6

    .line 1027
    .local v13, "b":F
    sub-float v5, v18, v23

    const/high16 v6, 0x40000000    # 2.0f

    div-float v12, v5, v6

    .line 1028
    .local v12, "a":F
    add-float v14, v22, v13

    .line 1029
    .local v14, "center_x":F
    add-float v15, v23, v12

    .line 1031
    .local v15, "center_y":F
    const v5, 0x14001408

    move/from16 v0, p8

    if-ne v0, v5, :cond_11

    .line 1032
    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v19, v0

    .restart local v19    # "i":I
    :goto_7
    move/from16 v0, v19

    int-to-float v5, v0

    cmpg-float v5, v5, v18

    if-lez v5, :cond_e

    .line 1040
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move/from16 v3, p3

    move/from16 v4, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->calcRemainRoi(Landroid/graphics/Rect;[BII)V

    goto/16 :goto_3

    .line 1033
    :cond_e
    move/from16 v0, v19

    int-to-float v5, v0

    sub-float/2addr v5, v15

    move/from16 v0, v19

    int-to-float v6, v0

    sub-float/2addr v6, v15

    mul-float/2addr v5, v6

    mul-float v6, v12, v12

    div-float v25, v5, v6

    .line 1034
    .local v25, "y":F
    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v20, v0

    .restart local v20    # "j":I
    :goto_8
    move/from16 v0, v20

    int-to-float v5, v0

    cmpg-float v5, v5, v17

    if-lez v5, :cond_f

    .line 1032
    add-int/lit8 v19, v19, 0x1

    goto :goto_7

    .line 1035
    :cond_f
    move/from16 v0, v20

    int-to-float v5, v0

    sub-float/2addr v5, v14

    move/from16 v0, v20

    int-to-float v6, v0

    sub-float/2addr v6, v14

    mul-float/2addr v5, v6

    mul-float v6, v13, v13

    div-float/2addr v5, v6

    add-float v16, v25, v5

    .line 1036
    .local v16, "dist":F
    const/high16 v5, 0x3f800000    # 1.0f

    cmpg-float v5, v16, v5

    if-gez v5, :cond_10

    .line 1037
    mul-int v5, v19, v24

    add-int v5, v5, v20

    const/4 v6, 0x0

    aput-byte v6, p1, v5

    .line 1034
    :cond_10
    add-int/lit8 v20, v20, 0x1

    goto :goto_8

    .line 1043
    .end local v16    # "dist":F
    .end local v19    # "i":I
    .end local v20    # "j":I
    .end local v25    # "y":F
    :cond_11
    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v19, v0

    .restart local v19    # "i":I
    :goto_9
    move/from16 v0, v19

    int-to-float v5, v0

    cmpg-float v5, v5, v18

    if-lez v5, :cond_12

    .line 1051
    move/from16 v0, v22

    float-to-int v7, v0

    move/from16 v0, v23

    float-to-int v8, v0

    move/from16 v0, v17

    float-to-int v9, v0

    move/from16 v0, v18

    float-to-int v10, v0

    move-object/from16 v5, p0

    move-object/from16 v6, p2

    move/from16 v11, p8

    invoke-direct/range {v5 .. v11}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->unionRoi(Landroid/graphics/Rect;IIIII)V

    goto/16 :goto_3

    .line 1044
    :cond_12
    move/from16 v0, v19

    int-to-float v5, v0

    sub-float/2addr v5, v15

    move/from16 v0, v19

    int-to-float v6, v0

    sub-float/2addr v6, v15

    mul-float/2addr v5, v6

    mul-float v6, v12, v12

    div-float v25, v5, v6

    .line 1045
    .restart local v25    # "y":F
    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v20, v0

    .restart local v20    # "j":I
    :goto_a
    move/from16 v0, v20

    int-to-float v5, v0

    cmpg-float v5, v5, v17

    if-lez v5, :cond_13

    .line 1043
    add-int/lit8 v19, v19, 0x1

    goto :goto_9

    .line 1046
    :cond_13
    move/from16 v0, v20

    int-to-float v5, v0

    sub-float/2addr v5, v14

    move/from16 v0, v20

    int-to-float v6, v0

    sub-float/2addr v6, v14

    mul-float/2addr v5, v6

    mul-float v6, v13, v13

    div-float/2addr v5, v6

    add-float v16, v25, v5

    .line 1047
    .restart local v16    # "dist":F
    const/high16 v5, 0x3f800000    # 1.0f

    cmpg-float v5, v16, v5

    if-gtz v5, :cond_14

    .line 1048
    mul-int v5, v19, v24

    add-int v5, v5, v20

    const/4 v6, 0x1

    aput-byte v6, p1, v5

    .line 1045
    :cond_14
    add-int/lit8 v20, v20, 0x1

    goto :goto_a
.end method

.method private unionRoi(Landroid/graphics/Rect;IIIII)V
    .locals 2
    .param p1, "roi"    # Landroid/graphics/Rect;
    .param p2, "startX"    # I
    .param p3, "startY"    # I
    .param p4, "endX"    # I
    .param p5, "endY"    # I
    .param p6, "selectType"    # I

    .prologue
    .line 939
    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 940
    const v1, 0x14001408

    if-ne p6, v1, :cond_0

    .line 942
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p2, p3, p4, p5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 943
    .local v0, "r":Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/jni/Util;->set_objectROI(Landroid/graphics/Rect;)V

    .line 955
    .end local v0    # "r":Landroid/graphics/Rect;
    :goto_0
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/jni/Util;->get_objectROI(Landroid/graphics/Rect;)V

    .line 956
    return-void

    .line 947
    :cond_0
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/graphics/Rect;->union(IIII)V

    .line 948
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/jni/Util;->set_objectROI(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 952
    :cond_1
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/graphics/Rect;->union(IIII)V

    .line 953
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/jni/Util;->set_objectROI(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method private updataBrushSize(I)V
    .locals 4
    .param p1, "radius"    # I

    .prologue
    .line 1062
    const/16 v2, 0x9

    new-array v1, v2, [F

    .line 1063
    .local v1, "values":[F
    int-to-float v2, p1

    int-to-float v3, p1

    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->calculateMinBrushSize(F)F

    move-result v3

    add-float/2addr v2, v3

    float-to-int p1, v2

    .line 1064
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 1065
    .local v0, "m":Landroid/graphics/Matrix;
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1066
    int-to-float v2, p1

    const/4 v3, 0x0

    aget v3, v1, v3

    div-float/2addr v2, v3

    float-to-int p1, v2

    .line 1067
    div-int/lit8 v2, p1, 0x8

    invoke-static {p1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Util;->set_brush_size(II)V

    .line 1068
    return-void
.end method


# virtual methods
.method public applyOriginal()[I
    .locals 7

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v3, v0, [B

    .line 123
    .local v3, "OriginalMask":[B
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v6

    .line 124
    .local v6, "scale":F
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    .line 125
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    .line 127
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v4

    .line 128
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v5

    .line 124
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->makeScaleMaskBuff([BII[BII)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    div-float/2addr v1, v6

    float-to-int v1, v1

    .line 130
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    div-float/2addr v2, v6

    float-to-int v2, v2

    .line 131
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    div-float/2addr v4, v6

    float-to-int v4, v4

    .line 132
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    div-float/2addr v5, v6

    float-to-int v5, v5

    .line 129
    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalMaskBuff([B)V

    .line 135
    const/4 v0, 0x0

    return-object v0
.end method

.method public clearSelectArea()V
    .locals 8

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Util;->reset_objectsel([BII)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 143
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    .line 144
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    .line 145
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 146
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    .line 147
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewPath:Landroid/graphics/Path;

    .line 148
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 149
    const/4 v7, 0x0

    move-object v0, p0

    .line 143
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Z)V

    .line 152
    return-void
.end method

.method public configurationChanged()V
    .locals 0

    .prologue
    .line 160
    return-void
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)V
    .locals 1
    .param p1, "selecteffect"    # Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    .prologue
    .line 104
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 105
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    .line 106
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    .line 107
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 785
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    .line 786
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    .line 787
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    .line 788
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 789
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    .line 791
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 792
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 794
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mLassoPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->rewind()V

    .line 795
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mLassoPath:Landroid/graphics/Path;

    .line 796
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->rewind()V

    .line 797
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewPath:Landroid/graphics/Path;

    .line 798
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mCntr:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->destroy()V

    .line 799
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mCntr:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    .line 801
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/jni/Util;->destroy_objectsel()V

    .line 802
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 803
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Matrix;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "viewTransformMatrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 155
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectDrawType:I

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->drawSelect(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    .line 156
    return-void
.end method

.method public getBrushMax()I
    .locals 1

    .prologue
    .line 781
    const/16 v0, 0x46

    return v0
.end method

.method public getBrushSize()I
    .locals 1

    .prologue
    .line 773
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mBrushSize:I

    return v0
.end method

.method public getDrawMode()I
    .locals 1

    .prologue
    .line 807
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectDrawType:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 117
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPreviewMaskBuffer()[B
    .locals 1

    .prologue
    .line 769
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    return-object v0
.end method

.method public getPreviewRoi()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 751
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getRealPosition(FF)Landroid/graphics/Point;
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 727
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 728
    .local v0, "p":Landroid/graphics/Point;
    float-to-int v1, p1

    float-to-int v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 746
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "point:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/graphics/Point;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 747
    return-object v0
.end method

.method public getSelectedType()I
    .locals 1

    .prologue
    .line 777
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectType:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 112
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 3
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 58
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    .line 59
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 60
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    mul-int/2addr v0, v1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    .line 61
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mPreviewRoi:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 63
    const/16 v0, 0x2d

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mBrushSize:I

    .line 64
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mBrushSize:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->setBrushSize(I)V

    .line 65
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/jni/Util;->init_objectsel(II)I

    .line 66
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mCntr:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->init(II)V

    .line 80
    return-void
.end method

.method public init(Lcom/sec/android/mimage/photoretouching/Core/ImageData;I)V
    .locals 3
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "selectType"    # I

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 84
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    .line 85
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 86
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    mul-int/2addr v0, v1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    .line 87
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    .line 88
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectType:I

    .line 89
    const/16 v0, 0x2d

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mBrushSize:I

    .line 90
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mBrushSize:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->setBrushSize(I)V

    .line 91
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/jni/Util;->init_objectsel(II)I

    .line 92
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mCntr:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->init(II)V

    .line 101
    return-void
.end method

.method public inverse()V
    .locals 8

    .prologue
    .line 695
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Util;->inverse_objectsel([BII)V

    .line 696
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->calcRemainRoi(Landroid/graphics/Rect;[BII)V

    .line 697
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 698
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    .line 699
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    .line 700
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 701
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    .line 702
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewPath:Landroid/graphics/Path;

    .line 703
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 704
    const/4 v7, 0x0

    move-object v0, p0

    .line 698
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Z)V

    .line 724
    return-void
.end method

.method public setBrushSize(I)V
    .locals 1
    .param p1, "brushsize"    # I

    .prologue
    .line 755
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mBrushSize:I

    .line 756
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mBrushSize:I

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->updataBrushSize(I)V

    .line 757
    return-void
.end method

.method public setSelectDrawType(I)V
    .locals 0
    .param p1, "selectDrawType"    # I

    .prologue
    .line 167
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectDrawType:I

    .line 168
    return-void
.end method

.method public setSelectType(I)V
    .locals 0
    .param p1, "selectType"    # I

    .prologue
    .line 163
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectType:I

    .line 164
    return-void
.end method

.method public touchDown([ILandroid/view/MotionEvent;)V
    .locals 8
    .param p1, "previewBuffer"    # [I
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 171
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 172
    .local v3, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 173
    .local v4, "y":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    .line 175
    .local v6, "time":J
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;

    move-object v2, p0

    move-object v5, p1

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;FF[IJ)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempTouchDown:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;

    .line 299
    return-void
.end method

.method public touchMove([ILandroid/view/MotionEvent;)V
    .locals 22
    .param p1, "previewBuffer"    # [I
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 450
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mIsDoingSelection:Z

    .line 451
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempTouchDown:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;

    if-eqz v4, :cond_2

    .line 453
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempTouchDown:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;

    invoke-interface {v6}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;->firstTouchTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x1e

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    .line 691
    :cond_0
    :goto_0
    return-void

    .line 455
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempTouchDown:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;->touchDown(Landroid/view/MotionEvent;)V

    .line 456
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempTouchDown:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;

    goto :goto_0

    .line 459
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mStartSelect:Z

    if-eqz v4, :cond_0

    .line 461
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v20

    .line 462
    .local v20, "x":F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v21

    .line 463
    .local v21, "y":F
    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getRealPosition(FF)Landroid/graphics/Point;

    move-result-object v8

    .line 464
    .local v8, "point":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    .line 465
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectType:I

    const v5, 0x14001408

    if-ne v4, v5, :cond_13

    .line 467
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectDrawType:I

    packed-switch v4, :pswitch_data_0

    .line 561
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 562
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 563
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    iget v5, v8, Landroid/graphics/Point;->x:I

    iget v6, v8, Landroid/graphics/Point;->y:I

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Point;->set(II)V

    .line 564
    const/4 v8, 0x0

    .line 566
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getDrawMode()I

    move-result v4

    const v5, 0x14001401

    if-eq v4, v5, :cond_4

    .line 567
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getDrawMode()I

    move-result v4

    const v5, 0x14001403

    if-ne v4, v5, :cond_0

    .line 569
    :cond_4
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    .line 570
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    .line 571
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 572
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    .line 573
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewPath:Landroid/graphics/Path;

    .line 574
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 575
    const/16 v16, 0x0

    move-object/from16 v9, p0

    .line 569
    invoke-direct/range {v9 .. v16}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Z)V

    goto/16 :goto_0

    .line 470
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 471
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    const/4 v12, 0x0

    move-object/from16 v4, p1

    .line 470
    invoke-static/range {v4 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    goto :goto_1

    .line 474
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 475
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v10, 0x3

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    const/4 v12, 0x0

    move-object/from16 v4, p1

    .line 474
    invoke-static/range {v4 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    goto/16 :goto_1

    .line 479
    :pswitch_2
    new-instance v19, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    move-object/from16 v0, v19

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 481
    .local v19, "roi":Landroid/graphics/Rect;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sj, SE - touchMove() - roi : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / x : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / y : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / mPreviewWidth : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 482
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / mPreviewHeight : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 483
    const-string v5, " / mImageData.getDrawCanvasRoi() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 481
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 485
    move/from16 v0, v20

    float-to-int v4, v0

    move/from16 v0, v21

    float-to-int v5, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 488
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 489
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v10, 0x5

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    const/4 v12, 0x0

    move-object/from16 v4, p1

    .line 488
    invoke-static/range {v4 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    .line 490
    move/from16 v17, v20

    .line 491
    .local v17, "lassoX":F
    move/from16 v18, v21

    .line 531
    .local v18, "lassoY":F
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mLassoPath:Landroid/graphics/Path;

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_1

    .line 495
    .end local v17    # "lassoX":F
    .end local v18    # "lassoY":F
    :cond_5
    iget v4, v8, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    add-int/lit8 v5, v5, -0x1

    if-le v4, v5, :cond_6

    .line 496
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v8, Landroid/graphics/Point;->x:I

    .line 497
    :cond_6
    iget v4, v8, Landroid/graphics/Point;->x:I

    if-gez v4, :cond_7

    .line 498
    const/4 v4, 0x0

    iput v4, v8, Landroid/graphics/Point;->x:I

    .line 499
    :cond_7
    iget v4, v8, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    add-int/lit8 v5, v5, -0x1

    if-le v4, v5, :cond_8

    .line 500
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v8, Landroid/graphics/Point;->y:I

    .line 501
    :cond_8
    iget v4, v8, Landroid/graphics/Point;->y:I

    if-gez v4, :cond_9

    .line 502
    const/4 v4, 0x0

    iput v4, v8, Landroid/graphics/Point;->y:I

    .line 504
    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 505
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v10, 0x5

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    const/4 v12, 0x0

    move-object/from16 v4, p1

    .line 504
    invoke-static/range {v4 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    .line 517
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    cmpl-float v4, v20, v4

    if-lez v4, :cond_a

    .line 518
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v0, v4

    move/from16 v20, v0

    .line 519
    :cond_a
    const/4 v4, 0x0

    cmpg-float v4, v20, v4

    if-gez v4, :cond_b

    .line 520
    const/16 v20, 0x0

    .line 521
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    cmpl-float v4, v21, v4

    if-lez v4, :cond_c

    .line 522
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v0, v4

    move/from16 v21, v0

    .line 523
    :cond_c
    const/4 v4, 0x0

    cmpg-float v4, v21, v4

    if-gez v4, :cond_d

    .line 524
    const/16 v21, 0x0

    .line 527
    :cond_d
    move/from16 v17, v20

    .line 528
    .restart local v17    # "lassoX":F
    move/from16 v18, v21

    .restart local v18    # "lassoY":F
    goto/16 :goto_2

    .line 535
    .end local v17    # "lassoX":F
    .end local v18    # "lassoY":F
    .end local v19    # "roi":Landroid/graphics/Rect;
    :pswitch_3
    new-instance v19, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    move-object/from16 v0, v19

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 536
    .restart local v19    # "roi":Landroid/graphics/Rect;
    move/from16 v0, v20

    float-to-int v4, v0

    move/from16 v0, v21

    float-to-int v5, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 539
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    move/from16 v0, v20

    iput v0, v4, Landroid/graphics/RectF;->right:F

    .line 540
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    move/from16 v0, v21

    iput v0, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 546
    :cond_e
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    cmpl-float v4, v4, v20

    if-lez v4, :cond_f

    .line 547
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v4

    move/from16 v20, v0

    .line 548
    :cond_f
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    cmpg-float v4, v4, v20

    if-gez v4, :cond_10

    .line 549
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v4

    move/from16 v20, v0

    .line 550
    :cond_10
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    cmpl-float v4, v4, v21

    if-lez v4, :cond_11

    .line 551
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v4

    move/from16 v21, v0

    .line 552
    :cond_11
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    cmpg-float v4, v4, v21

    if-gez v4, :cond_12

    .line 553
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v4

    move/from16 v21, v0

    .line 555
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    move/from16 v0, v20

    iput v0, v4, Landroid/graphics/RectF;->right:F

    .line 556
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    move/from16 v0, v21

    iput v0, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 580
    .end local v19    # "roi":Landroid/graphics/Rect;
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mResetMask:Z

    if-nez v4, :cond_15

    .line 582
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectDrawType:I

    packed-switch v4, :pswitch_data_1

    .line 671
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_14

    .line 672
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 673
    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    iget v5, v8, Landroid/graphics/Point;->x:I

    iget v6, v8, Landroid/graphics/Point;->y:I

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Point;->set(II)V

    .line 674
    const/4 v8, 0x0

    .line 677
    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getDrawMode()I

    move-result v4

    const v5, 0x14001401

    if-eq v4, v5, :cond_16

    .line 678
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getDrawMode()I

    move-result v4

    const v5, 0x14001403

    if-ne v4, v5, :cond_0

    .line 680
    :cond_16
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    .line 681
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    .line 682
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 683
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    .line 684
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewPath:Landroid/graphics/Path;

    .line 685
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 686
    const/16 v16, 0x1

    move-object/from16 v9, p0

    .line 680
    invoke-direct/range {v9 .. v16}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Z)V

    goto/16 :goto_0

    .line 585
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 586
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    const/4 v12, 0x0

    move-object/from16 v4, p1

    .line 585
    invoke-static/range {v4 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    goto :goto_3

    .line 589
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 590
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v10, 0x2

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    const/4 v12, 0x0

    move-object/from16 v4, p1

    .line 589
    invoke-static/range {v4 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    goto/16 :goto_3

    .line 594
    :pswitch_6
    new-instance v19, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    move-object/from16 v0, v19

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 595
    .restart local v19    # "roi":Landroid/graphics/Rect;
    move/from16 v0, v20

    float-to-int v4, v0

    move/from16 v0, v21

    float-to-int v5, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 598
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 599
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v10, 0x4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    const/4 v12, 0x0

    move-object/from16 v4, p1

    .line 598
    invoke-static/range {v4 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    .line 600
    move/from16 v17, v20

    .line 601
    .restart local v17    # "lassoX":F
    move/from16 v18, v21

    .line 641
    .restart local v18    # "lassoY":F
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mLassoPath:Landroid/graphics/Path;

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_3

    .line 605
    .end local v17    # "lassoX":F
    .end local v18    # "lassoY":F
    :cond_17
    iget v4, v8, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    add-int/lit8 v5, v5, -0x1

    if-le v4, v5, :cond_18

    .line 606
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v8, Landroid/graphics/Point;->x:I

    .line 607
    :cond_18
    iget v4, v8, Landroid/graphics/Point;->x:I

    if-gez v4, :cond_19

    .line 608
    const/4 v4, 0x0

    iput v4, v8, Landroid/graphics/Point;->x:I

    .line 609
    :cond_19
    iget v4, v8, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    add-int/lit8 v5, v5, -0x1

    if-le v4, v5, :cond_1a

    .line 610
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v8, Landroid/graphics/Point;->y:I

    .line 611
    :cond_1a
    iget v4, v8, Landroid/graphics/Point;->y:I

    if-gez v4, :cond_1b

    .line 612
    const/4 v4, 0x0

    iput v4, v8, Landroid/graphics/Point;->y:I

    .line 614
    :cond_1b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 615
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v10, 0x4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempRoi:Landroid/graphics/Rect;

    const/4 v12, 0x0

    move-object/from16 v4, p1

    .line 614
    invoke-static/range {v4 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    .line 627
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    cmpl-float v4, v20, v4

    if-lez v4, :cond_1c

    .line 628
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v0, v4

    move/from16 v20, v0

    .line 629
    :cond_1c
    const/4 v4, 0x0

    cmpg-float v4, v20, v4

    if-gez v4, :cond_1d

    .line 630
    const/16 v20, 0x0

    .line 631
    :cond_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    cmpl-float v4, v21, v4

    if-lez v4, :cond_1e

    .line 632
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v0, v4

    move/from16 v21, v0

    .line 633
    :cond_1e
    const/4 v4, 0x0

    cmpg-float v4, v21, v4

    if-gez v4, :cond_1f

    .line 634
    const/16 v21, 0x0

    .line 637
    :cond_1f
    move/from16 v17, v20

    .line 638
    .restart local v17    # "lassoX":F
    move/from16 v18, v21

    .restart local v18    # "lassoY":F
    goto/16 :goto_4

    .line 645
    .end local v17    # "lassoX":F
    .end local v18    # "lassoY":F
    .end local v19    # "roi":Landroid/graphics/Rect;
    :pswitch_7
    new-instance v19, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    move-object/from16 v0, v19

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 646
    .restart local v19    # "roi":Landroid/graphics/Rect;
    move/from16 v0, v20

    float-to-int v4, v0

    move/from16 v0, v21

    float-to-int v5, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 649
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    move/from16 v0, v20

    iput v0, v4, Landroid/graphics/RectF;->right:F

    .line 650
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    move/from16 v0, v21

    iput v0, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_3

    .line 655
    :cond_20
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    cmpl-float v4, v4, v20

    if-lez v4, :cond_21

    .line 656
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v4

    move/from16 v20, v0

    .line 657
    :cond_21
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    cmpg-float v4, v4, v20

    if-gez v4, :cond_22

    .line 658
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v4

    move/from16 v20, v0

    .line 659
    :cond_22
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    cmpl-float v4, v4, v21

    if-lez v4, :cond_23

    .line 660
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v4

    move/from16 v21, v0

    .line 661
    :cond_23
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    cmpg-float v4, v4, v21

    if-gez v4, :cond_24

    .line 662
    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v4

    move/from16 v21, v0

    .line 666
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float v5, v20, v5

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 667
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float v5, v21, v5

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_3

    .line 467
    :pswitch_data_0
    .packed-switch 0x14001401
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 582
    :pswitch_data_1
    .packed-switch 0x14001401
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public touchUp([ILandroid/view/MotionEvent;)V
    .locals 32
    .param p1, "previewBuffer"    # [I
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 301
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mIsDoingSelection:Z

    .line 302
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempTouchDown:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;

    if-eqz v4, :cond_0

    .line 304
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempTouchDown:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;

    invoke-interface {v6}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;->firstTouchTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x1e

    cmp-long v4, v4, v6

    if-ltz v4, :cond_0

    .line 306
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempTouchDown:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;->touchDown(Landroid/view/MotionEvent;)V

    .line 307
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mTempTouchDown:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;

    .line 310
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mStartSelect:Z

    if-eqz v4, :cond_2

    .line 312
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v30

    .line 313
    .local v30, "x":F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v31

    .line 314
    .local v31, "y":F
    move-object/from16 v0, p0

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getRealPosition(FF)Landroid/graphics/Point;

    move-result-object v8

    .line 315
    .local v8, "point":Landroid/graphics/Point;
    new-instance v11, Landroid/graphics/Rect;

    invoke-direct {v11}, Landroid/graphics/Rect;-><init>()V

    .line 316
    .local v11, "r":Landroid/graphics/Rect;
    invoke-virtual {v11}, Landroid/graphics/Rect;->setEmpty()V

    .line 317
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectType:I

    const v5, 0x14001408

    if-ne v4, v5, :cond_5

    .line 319
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectDrawType:I

    packed-switch v4, :pswitch_data_0

    .line 371
    :goto_0
    const-string v4, "up start contourTrace"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 372
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v20, v0

    .line 373
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move/from16 v21, v0

    .line 374
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    move/from16 v22, v0

    .line 375
    new-instance v23, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    move-object/from16 v0, v23

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 376
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewPath:Landroid/graphics/Path;

    move-object/from16 v24, v0

    .line 377
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v25, v0

    .line 378
    const/16 v26, 0x0

    move-object/from16 v19, p0

    .line 372
    invoke-direct/range {v19 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Z)V

    .line 437
    :goto_1
    invoke-virtual {v11}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 438
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    invoke-virtual {v4, v11}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 439
    :cond_1
    const/4 v8, 0x0

    .line 442
    .end local v8    # "point":Landroid/graphics/Point;
    .end local v11    # "r":Landroid/graphics/Rect;
    .end local v30    # "x":F
    .end local v31    # "y":F
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v5, -0x1

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Point;->set(II)V

    .line 443
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mFirstPt:Landroid/graphics/Point;

    const/4 v5, -0x1

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Point;->set(II)V

    .line 445
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mStartSelect:Z

    .line 446
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mResetMask:Z

    .line 447
    return-void

    .line 327
    .restart local v8    # "point":Landroid/graphics/Point;
    .restart local v11    # "r":Landroid/graphics/Rect;
    .restart local v30    # "x":F
    .restart local v31    # "y":F
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->calcRemainRoi(Landroid/graphics/Rect;[BII)V

    goto :goto_0

    .line 330
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 331
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v10, 0x3

    const/4 v12, 0x1

    move-object/from16 v4, p1

    .line 330
    invoke-static/range {v4 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    .line 332
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->calcRemainRoi(Landroid/graphics/Rect;[BII)V

    goto/16 :goto_0

    .line 335
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 336
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mFirstPt:Landroid/graphics/Point;

    move-object/from16 v17, v0

    const/16 v18, 0x5

    const/16 v20, 0x1

    move-object/from16 v12, p1

    move-object/from16 v19, v11

    .line 335
    invoke-static/range {v12 .. v20}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    .line 338
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->calcRemainRoi(Landroid/graphics/Rect;[BII)V

    goto/16 :goto_0

    .line 346
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v28

    .line 347
    .local v28, "matrix":Landroid/graphics/Matrix;
    const/16 v4, 0x9

    new-array v0, v4, [F

    move-object/from16 v29, v0

    .line 348
    .local v29, "v":[F
    invoke-virtual/range {v28 .. v29}, Landroid/graphics/Matrix;->getValues([F)V

    .line 350
    const/4 v4, 0x2

    aget v17, v29, v4

    .line 351
    .local v17, "leftFromOrg":F
    const/4 v4, 0x0

    cmpg-float v4, v17, v4

    if-gez v4, :cond_3

    .line 352
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v17

    .line 356
    :goto_2
    const/4 v4, 0x5

    aget v18, v29, v4

    .line 357
    .local v18, "topFromOrg":F
    const/4 v4, 0x0

    cmpg-float v4, v18, v4

    if-gez v4, :cond_4

    .line 358
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    .line 362
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    .line 363
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    move/from16 v16, v0

    .line 364
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    aget v5, v29, v5

    div-float v19, v4, v5

    .line 365
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectType:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectDrawType:I

    move/from16 v21, v0

    move-object/from16 v12, p0

    .line 362
    invoke-direct/range {v12 .. v21}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->setRectCircleMask([BLandroid/graphics/Rect;IIFFFII)V

    goto/16 :goto_0

    .line 354
    .end local v18    # "topFromOrg":F
    :cond_3
    const/16 v17, 0x0

    goto :goto_2

    .line 360
    .restart local v18    # "topFromOrg":F
    :cond_4
    const/16 v18, 0x0

    goto :goto_3

    .line 383
    .end local v17    # "leftFromOrg":F
    .end local v18    # "topFromOrg":F
    .end local v28    # "matrix":Landroid/graphics/Matrix;
    .end local v29    # "v":[F
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mResetMask:Z

    if-nez v4, :cond_6

    .line 385
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectDrawType:I

    packed-switch v4, :pswitch_data_1

    .line 428
    :cond_6
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v20, v0

    .line 429
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move/from16 v21, v0

    .line 430
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    move/from16 v22, v0

    .line 431
    new-instance v23, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    move-object/from16 v0, v23

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewPath:Landroid/graphics/Path;

    move-object/from16 v24, v0

    .line 433
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v25, v0

    .line 434
    const/16 v26, 0x1

    move-object/from16 v19, p0

    .line 428
    invoke-direct/range {v19 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Z)V

    goto/16 :goto_1

    .line 388
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 389
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v10, 0x0

    const/4 v12, 0x1

    move-object/from16 v4, p1

    .line 388
    invoke-static/range {v4 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    goto :goto_4

    .line 392
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    .line 393
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v10, 0x2

    const/4 v12, 0x1

    move-object/from16 v4, p1

    .line 392
    invoke-static/range {v4 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    goto :goto_4

    .line 396
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    move/from16 v22, v0

    .line 397
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mFirstPt:Landroid/graphics/Point;

    move-object/from16 v24, v0

    const/16 v25, 0x4

    const/16 v27, 0x1

    move-object/from16 v19, p1

    move-object/from16 v26, v11

    .line 396
    invoke-static/range {v19 .. v27}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    goto/16 :goto_4

    .line 401
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v28

    .line 402
    .restart local v28    # "matrix":Landroid/graphics/Matrix;
    const/16 v4, 0x9

    new-array v0, v4, [F

    move-object/from16 v29, v0

    .line 403
    .restart local v29    # "v":[F
    invoke-virtual/range {v28 .. v29}, Landroid/graphics/Matrix;->getValues([F)V

    .line 405
    const/4 v4, 0x2

    aget v17, v29, v4

    .line 406
    .restart local v17    # "leftFromOrg":F
    const/4 v4, 0x0

    cmpg-float v4, v17, v4

    if-gez v4, :cond_7

    .line 407
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v17

    .line 411
    :goto_5
    const/4 v4, 0x5

    aget v18, v29, v4

    .line 412
    .restart local v18    # "topFromOrg":F
    const/4 v4, 0x0

    cmpg-float v4, v18, v4

    if-gez v4, :cond_8

    .line 413
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    .line 417
    :goto_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    .line 418
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I

    move/from16 v16, v0

    .line 419
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    aget v5, v29, v5

    div-float v19, v4, v5

    .line 420
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectType:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectDrawType:I

    move/from16 v21, v0

    move-object/from16 v12, p0

    .line 417
    invoke-direct/range {v12 .. v21}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->setRectCircleMask([BLandroid/graphics/Rect;IIFFFII)V

    goto/16 :goto_4

    .line 409
    .end local v18    # "topFromOrg":F
    :cond_7
    const/16 v17, 0x0

    goto :goto_5

    .line 415
    .restart local v18    # "topFromOrg":F
    :cond_8
    const/16 v18, 0x0

    goto :goto_6

    .line 319
    :pswitch_data_0
    .packed-switch 0x14001401
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 385
    :pswitch_data_1
    .packed-switch 0x14001401
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
