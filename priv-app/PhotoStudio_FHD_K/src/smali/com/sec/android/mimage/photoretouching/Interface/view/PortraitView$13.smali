.class Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;
.super Ljava/lang/Object;
.source "PortraitView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->init2DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    .line 710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 6
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 719
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Z)V

    .line 720
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v2

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getButtonPosition(I)Landroid/graphics/Point;

    move-result-object v0

    .line 721
    .local v0, "point":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f06000f

    iget v4, v0, Landroid/graphics/Point;->x:I

    iget v5, v0, Landroid/graphics/Point;->y:I

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, 0x2

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastActionbar(Landroid/content/Context;III)V

    .line 723
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "vibrator"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    .line 724
    .local v1, "vibrator":Landroid/os/Vibrator;
    const-wide/16 v2, 0xd

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 725
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Z)V

    .line 726
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 730
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mLongpressButton:Z
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$32(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 731
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 733
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v0

    .line 734
    .local v0, "uri":Landroid/net/Uri;
    if-nez v0, :cond_0

    .line 736
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 738
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x1

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$33(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Landroid/content/Intent;Z)V

    .line 742
    .end local v0    # "uri":Landroid/net/Uri;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Z)V

    .line 743
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 715
    return-void
.end method
