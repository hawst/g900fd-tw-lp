.class Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;
.super Landroid/os/AsyncTask;
.source "CopyToView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V
    .locals 0

    .prologue
    .line 962
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    .line 961
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 963
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 10
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 966
    const/4 v2, 0x0

    .line 967
    .local v2, "fileName":Ljava/lang/String;
    array-length v5, p1

    if-lez v5, :cond_0

    .line 968
    const/4 v5, 0x0

    aget-object v2, p1, v5

    .line 970
    :cond_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getAvailableExternalMemorySize()J

    move-result-wide v0

    .line 971
    .local v0, "available_memsize":J
    const-wide/32 v6, 0xa00000

    cmp-long v5, v0, v6

    if-gez v5, :cond_3

    .line 972
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 973
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->dismiss()V

    .line 974
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "memory size = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " %"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 976
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f06007b

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 1002
    :cond_2
    :goto_0
    return-object v9

    .line 979
    :cond_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->run_blending_org(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 980
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v6

    .line 981
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v7

    .line 982
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v8

    .line 980
    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([III)V

    .line 985
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 987
    const/4 v3, 0x0

    .line 988
    .local v3, "filePath":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_5

    .line 990
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 991
    .local v4, "tempPath":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 997
    .end local v4    # "tempPath":Ljava/lang/String;
    :goto_1
    if-nez v2, :cond_4

    .line 998
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSimpleDate()Ljava/lang/String;

    move-result-object v2

    .line 1000
    :cond_4
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v5

    invoke-virtual {v5, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->saveCurrentImage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 995
    :cond_5
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 7
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const v6, 0x7f0601df

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1013
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1015
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1016
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1018
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1020
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 1021
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 1022
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    .line 1025
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1027
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isPersonalPage()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1029
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const-string v4, "Studio"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1030
    .local v1, "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 1045
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 1046
    return-void

    .line 1034
    .end local v1    # "text":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPrivateSaveFolder()Ljava/lang/String;

    move-result-object v0

    .line 1035
    .local v0, "privateFolder":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1036
    .restart local v1    # "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 1041
    .end local v0    # "privateFolder":Ljava/lang/String;
    .end local v1    # "text":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const-string v4, "Studio"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1042
    .restart local v1    # "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 1005
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1006
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Landroid/app/ProgressDialog;)V

    .line 1007
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06008f

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1008
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1009
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1010
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1011
    return-void
.end method
