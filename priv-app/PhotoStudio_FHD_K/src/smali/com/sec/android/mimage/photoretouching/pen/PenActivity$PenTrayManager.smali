.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;
.super Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
.source "PenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PenTrayManager"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 2137
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    .line 2138
    invoke-direct {p0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;-><init>(Landroid/content/Context;)V

    .line 2139
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;)V
    .locals 0

    .prologue
    .line 2146
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;->initTrayLayout()V

    return-void
.end method

.method private initTrayLayout()V
    .locals 1

    .prologue
    .line 2148
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->disableButtonTouch()V

    .line 2149
    return-void
.end method


# virtual methods
.method public configurationChange()V
    .locals 2

    .prologue
    .line 2142
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2143
    .local v0, "vGroup":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 2144
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2145
    :cond_0
    return-void
.end method
