.class Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$2;
.super Ljava/lang/Object;
.source "ViewButtonsManager.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout$BottomLayoutCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBottomLayoutCallback()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutCallback()V
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 99
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 119
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->moveArrowToCurrentViewBtn()V

    .line 126
    :cond_0
    :goto_1
    return-void

    .line 109
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mButtonWidth:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)I

    move-result v2

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->adjustBottomButtonMargin(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;II)V

    goto :goto_0

    .line 112
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    goto :goto_1

    .line 99
    nop

    :sswitch_data_0
    .sparse-switch
        0x10000000 -> :sswitch_0
        0x11000000 -> :sswitch_0
        0x15000000 -> :sswitch_0
        0x16000000 -> :sswitch_0
        0x18000000 -> :sswitch_0
        0x1e110000 -> :sswitch_0
        0x20000000 -> :sswitch_0
        0x2c000000 -> :sswitch_0
        0x31000000 -> :sswitch_0
        0x31600000 -> :sswitch_1
    .end sparse-switch
.end method
