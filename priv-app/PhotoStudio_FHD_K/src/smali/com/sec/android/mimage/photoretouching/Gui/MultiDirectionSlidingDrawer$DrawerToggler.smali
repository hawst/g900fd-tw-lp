.class Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$DrawerToggler;
.super Ljava/lang/Object;
.source "MultiDirectionSlidingDrawer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrawerToggler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;)V
    .locals 0

    .prologue
    .line 1072
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$DrawerToggler;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$DrawerToggler;)V
    .locals 0

    .prologue
    .line 1072
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$DrawerToggler;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1076
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$DrawerToggler;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mLocked:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1086
    :goto_0
    return-void

    .line 1081
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$DrawerToggler;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimateOnClick:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1082
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$DrawerToggler;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->animateToggle()V

    goto :goto_0

    .line 1084
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$DrawerToggler;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->toggle()V

    goto :goto_0
.end method
