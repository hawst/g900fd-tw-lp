.class Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$41;
.super Ljava/lang/Object;
.source "NormalView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initSelectedDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$41;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    .line 2414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public TouchFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2418
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$41;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2419
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$41;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 2420
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$41;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2421
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$41;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->resetZoom()V

    .line 2422
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$41;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2423
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$41;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x17000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 2424
    :cond_2
    return-void
.end method
