.class Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$6;
.super Ljava/lang/Object;
.source "SubViewButtonsManager.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->initHorizontalScrollView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    .line 301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v5, 0x15001506

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 304
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 309
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mAnimate:Z
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 335
    :goto_0
    return v2

    .line 312
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 313
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_1
    move v2, v3

    .line 335
    goto :goto_0

    .line 315
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 316
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 317
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 318
    .local v0, "button":Landroid/view/View;
    invoke-virtual {v0, v3}, Landroid/view/View;->setPressed(Z)V

    .line 319
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v4

    if-eq v4, v5, :cond_2

    .line 320
    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 316
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 324
    .end local v0    # "button":Landroid/view/View;
    .end local v1    # "i":I
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 325
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 326
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 327
    .restart local v0    # "button":Landroid/view/View;
    invoke-virtual {v0, v3}, Landroid/view/View;->setPressed(Z)V

    .line 328
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v4

    if-eq v4, v5, :cond_3

    .line 329
    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 325
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 313
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
