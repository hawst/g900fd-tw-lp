.class public Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;
.super Ljava/lang/Object;
.source "DefaultButtonsListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$AnimationCallback;,
        Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;
    }
.end annotation


# instance fields
.field private mAnimationCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$AnimationCallback;

.field private mButton:Landroid/view/View;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mIsFunction:Z

.field private mList:Ljava/util/ArrayList;

.field private mTouchInterface:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "list"    # Ljava/util/ArrayList;
    .param p3, "touchInterface"    # Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mIsFunction:Z

    .line 151
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    .line 152
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;

    .line 153
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mAnimationCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$AnimationCallback;

    .line 154
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    .line 155
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mButton:Landroid/view/View;

    .line 29
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    .line 30
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;

    move-object v0, p1

    .line 32
    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$1;-><init>(Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 49
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;)Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;)Landroid/view/View;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;Landroid/view/GestureDetector;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    return-void
.end method

.method private disableAnotherButtons(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 57
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 59
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 68
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 61
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 62
    .local v0, "b":Landroid/view/View;
    if-eq v0, p1, :cond_2

    .line 64
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 59
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private enableButtons(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 71
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 73
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 82
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 75
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 76
    .local v0, "b":Landroid/view/View;
    if-eq v0, p1, :cond_2

    .line 78
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 73
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public isInButton(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 134
    const/4 v4, 0x0

    .line 135
    .local v4, "ret":Z
    const/4 v1, 0x0

    .line 136
    .local v1, "left":I
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 137
    .local v5, "right":I
    const/4 v6, 0x0

    .line 138
    .local v6, "top":I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 139
    .local v0, "bottom":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v2, v7

    .line 140
    .local v2, "posX":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v3, v7

    .line 144
    .local v3, "posY":I
    if-lt v2, v1, :cond_0

    if-gt v2, v5, :cond_0

    if-lt v3, v6, :cond_0

    if-gt v3, v0, :cond_0

    .line 145
    const/4 v4, 0x1

    .line 148
    :goto_0
    return v4

    .line 147
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 85
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mAnimationCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$AnimationCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mAnimationCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$AnimationCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$AnimationCallback;->isAnimation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    :goto_0
    return v2

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_1

    .line 90
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mButton:Landroid/view/View;

    .line 91
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;

    if-eqz v0, :cond_2

    .line 94
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;->TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 96
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 98
    :pswitch_0
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    .line 99
    invoke-virtual {p1, v2}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 101
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->disableAnotherButtons(Landroid/view/View;)V

    goto :goto_0

    .line 104
    :pswitch_1
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->isInButton(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 106
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 110
    :cond_3
    invoke-virtual {p1, v3}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 115
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mIsFunction:Z

    if-nez v0, :cond_6

    .line 116
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mIsFunction:Z

    .line 117
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    .line 118
    invoke-virtual {p1, v3}, Landroid/view/View;->playSoundEffect(I)V

    .line 119
    :cond_4
    invoke-virtual {p1, v3}, Landroid/view/View;->setPressed(Z)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;

    if-eqz v0, :cond_5

    .line 121
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;->TouchFunction(Landroid/view/View;)V

    .line 122
    :cond_5
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mIsFunction:Z

    .line 124
    :cond_6
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->enableButtons(Landroid/view/View;)V

    goto :goto_0

    .line 127
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->enableButtons(Landroid/view/View;)V

    goto :goto_0

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setAnimationCallback(Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$AnimationCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$AnimationCallback;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;->mAnimationCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$AnimationCallback;

    .line 54
    return-void
.end method
