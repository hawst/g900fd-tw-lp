.class Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;
.super Ljava/lang/Object;
.source "ButtonControlManager.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->addButton(IILandroid/graphics/Bitmap;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    return-object v0
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/high16 v6, -0x80000000

    const/4 v5, 0x0

    .line 121
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;-><init>(Landroid/content/Context;)V

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;)V

    move-object v1, p1

    .line 122
    check-cast v1, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 123
    .local v1, "myButton":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 124
    .local v0, "index":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v2

    const/high16 v3, -0x10000

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->initButton(ILandroid/graphics/Bitmap;I)V

    .line 125
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->copyButtonsNPosition(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 126
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v3

    # invokes: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->changeNearButtonInfo(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V
    invoke-static {v2, v1, v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$4(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 128
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-interface {v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setCurrentPosition(Landroid/graphics/Rect;)V

    .line 129
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 130
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/widget/FrameLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 131
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setCheckBoxVisibility(I)V

    .line 132
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 133
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mOverflowBtnCtrlLayout:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;

    move-result-object v2

    if-nez v2, :cond_0

    .line 135
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    const v4, 0x7f0900c6

    invoke-virtual {v2, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;

    invoke-static {v3, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$7(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;)V

    .line 136
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mOverflowBtnCtrlLayout:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;

    move-result-object v2

    invoke-virtual {v2, v6, v6}, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->measure(II)V

    .line 138
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mOverflowBtnCtrlLayout:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 140
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mOverflowBtnCtrlLayout:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;

    move-result-object v2

    check-cast p1, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    .end local p1    # "v":Landroid/view/View;
    new-instance v3, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;-><init>(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)V

    invoke-virtual {v2, p1, v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->setEnableIntercept(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$ActivityLayoutTouchUpCallback;)V

    .line 325
    :cond_1
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 117
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 109
    instance-of v0, p1, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 111
    check-cast v0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 113
    :cond_0
    return-void
.end method
