.class public Lcom/sec/android/mimage/photoretouching/launcher/IndicatorListener;
.super Ljava/lang/Object;
.source "IndicatorListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mPager:Landroid/support/v4/view/ViewPager;

.field private mView:[Landroid/widget/ImageView;


# direct methods
.method public constructor <init>([Landroid/widget/ImageView;Landroid/support/v4/view/ViewPager;)V
    .locals 3
    .param p1, "iv"    # [Landroid/widget/ImageView;
    .param p2, "pager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    const/4 v1, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/IndicatorListener;->mView:[Landroid/widget/ImageView;

    .line 10
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/IndicatorListener;->mPager:Landroid/support/v4/view/ViewPager;

    .line 13
    array-length v1, p1

    new-array v1, v1, [Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/IndicatorListener;->mView:[Landroid/widget/ImageView;

    .line 14
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    .line 16
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/launcher/IndicatorListener;->mPager:Landroid/support/v4/view/ViewPager;

    .line 17
    return-void

    .line 15
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/IndicatorListener;->mView:[Landroid/widget/ImageView;

    aget-object v2, p1, v0

    aput-object v2, v1, v0

    .line 14
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 22
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/IndicatorListener;->mView:[Landroid/widget/ImageView;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 30
    :goto_1
    return-void

    .line 24
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/IndicatorListener;->mView:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    if-ne p1, v1, :cond_1

    .line 26
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/IndicatorListener;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_1

    .line 22
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
