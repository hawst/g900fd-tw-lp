.class public Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout;
.super Landroid/widget/FrameLayout;
.source "ButtonControlLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout$LayoutCallback;
    }
.end annotation


# instance fields
.field private mLayoutCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout$LayoutCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout;->mLayoutCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout$LayoutCallback;

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout;->mLayoutCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout$LayoutCallback;

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout;->mLayoutCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout$LayoutCallback;

    .line 27
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout;->removeAllViews()V

    .line 32
    return-void
.end method

.method public init(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout$LayoutCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout$LayoutCallback;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout;->mLayoutCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout$LayoutCallback;

    .line 36
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 40
    if-eqz p1, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout;->mLayoutCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout$LayoutCallback;

    invoke-interface {v0, p2, p3, p4, p5}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlLayout$LayoutCallback;->onLayoutChange(IIII)V

    .line 42
    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 43
    return-void
.end method
