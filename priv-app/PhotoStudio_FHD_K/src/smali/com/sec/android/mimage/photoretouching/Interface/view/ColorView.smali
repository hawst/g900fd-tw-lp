.class public Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "ColorView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ResolverSetAsAdapter;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$SaveAsyncTask;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ThreadCallback;
    }
.end annotation


# instance fields
.field private final REDOALL_DIALOG:I

.field private final UNDOALL_DIALOG:I

.field private effectFlag:Z

.field private isReverseAnimRunning:Z

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

.field private mAlpha:I

.field private mAnimationBuffer:[I

.field private mApplyPreviewHandler:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;

.field private mBeforeX:F

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mClearPaint:Landroid/graphics/Paint;

.field private mColor:I

.field private mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

.field private mConfigurationChanged:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentEffectType:I

.field private mCurrentSaveSize:I

.field private mCurrentTime:J

.field private mDashPathEffectPaint:Landroid/graphics/Paint;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mDrawAnimRunnable:Ljava/lang/Runnable;

.field private mDrawContour:Z

.field private mDrawLinePaint:Landroid/graphics/Paint;

.field private mDrawLinePaint1:Landroid/graphics/Paint;

.field private mDrawOriginal:Z

.field private mEdit:Z

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHandler:Landroid/os/Handler;

.field private mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mIsDestroying:Z

.field private mIsMinimum:Z

.field private mLinePaint:Landroid/graphics/Paint;

.field private mLongpressButton:Z

.field private mMiddleBtn:Landroid/widget/Button;

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mOnlayoutCalledTime:J

.field private mOptionItemId:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPathEffect:Landroid/graphics/DashPathEffect;

.field private mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mProgressTextRect:Landroid/graphics/Rect;

.field private mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

.field private mSeekbarThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;

.field private mViewBitmapForApplyThread:Landroid/graphics/Bitmap;

.field private msave:Z

.field private msetas:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 110
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 3036
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    .line 3037
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 3038
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    .line 3039
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 3040
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 3041
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 3042
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 3043
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 3044
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 3045
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 3046
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mApplyPreviewHandler:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;

    .line 3047
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPaint:Landroid/graphics/Paint;

    .line 3048
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mClearPaint:Landroid/graphics/Paint;

    .line 3049
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mLinePaint:Landroid/graphics/Paint;

    .line 3050
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 3051
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    .line 3052
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 3053
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    .line 3054
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColor:I

    .line 3055
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentEffectType:I

    .line 3056
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawContour:Z

    .line 3057
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsMinimum:Z

    .line 3058
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsDestroying:Z

    .line 3060
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    .line 3061
    iput-wide v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentTime:J

    .line 3062
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mProgressTextRect:Landroid/graphics/Rect;

    .line 3064
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mBeforeX:F

    .line 3066
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 3067
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmapForApplyThread:Landroid/graphics/Bitmap;

    .line 3069
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .line 3071
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawOriginal:Z

    .line 3072
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 3074
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mConfigurationChanged:Z

    .line 3075
    iput-wide v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mOnlayoutCalledTime:J

    .line 3077
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->UNDOALL_DIALOG:I

    .line 3078
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->REDOALL_DIALOG:I

    .line 3080
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mOptionItemId:I

    .line 3082
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 3084
    const v0, 0x7a1200

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentSaveSize:I

    .line 3087
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAnimationBuffer:[I

    .line 3088
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mLongpressButton:Z

    .line 3090
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 3091
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 3092
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPathEffect:Landroid/graphics/DashPathEffect;

    .line 3094
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mEdit:Z

    .line 3095
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->msetas:Z

    .line 3096
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->msave:Z

    .line 3097
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->isReverseAnimRunning:Z

    .line 3099
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAlpha:I

    .line 3100
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->effectFlag:Z

    .line 3101
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    .line 111
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initColorView(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 112
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentEffectType:I

    .line 113
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initGesture()V

    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p6, "info"    # Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 122
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 3036
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    .line 3037
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 3038
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    .line 3039
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 3040
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 3041
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 3042
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 3043
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 3044
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 3045
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 3046
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mApplyPreviewHandler:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;

    .line 3047
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPaint:Landroid/graphics/Paint;

    .line 3048
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mClearPaint:Landroid/graphics/Paint;

    .line 3049
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mLinePaint:Landroid/graphics/Paint;

    .line 3050
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 3051
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    .line 3052
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 3053
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    .line 3054
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColor:I

    .line 3055
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentEffectType:I

    .line 3056
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawContour:Z

    .line 3057
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsMinimum:Z

    .line 3058
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsDestroying:Z

    .line 3060
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    .line 3061
    iput-wide v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentTime:J

    .line 3062
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mProgressTextRect:Landroid/graphics/Rect;

    .line 3064
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mBeforeX:F

    .line 3066
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 3067
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmapForApplyThread:Landroid/graphics/Bitmap;

    .line 3069
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .line 3071
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawOriginal:Z

    .line 3072
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 3074
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mConfigurationChanged:Z

    .line 3075
    iput-wide v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mOnlayoutCalledTime:J

    .line 3077
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->UNDOALL_DIALOG:I

    .line 3078
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->REDOALL_DIALOG:I

    .line 3080
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mOptionItemId:I

    .line 3082
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 3084
    const v0, 0x7a1200

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentSaveSize:I

    .line 3087
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAnimationBuffer:[I

    .line 3088
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mLongpressButton:Z

    .line 3090
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 3091
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 3092
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPathEffect:Landroid/graphics/DashPathEffect;

    .line 3094
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mEdit:Z

    .line 3095
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->msetas:Z

    .line 3096
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->msave:Z

    .line 3097
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->isReverseAnimRunning:Z

    .line 3099
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAlpha:I

    .line 3100
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->effectFlag:Z

    .line 3101
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    .line 123
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initColorView(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 124
    iput-object p6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .line 125
    if-eqz p6, :cond_0

    .line 126
    invoke-virtual {p6}, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->getPreviousSubStatus()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentEffectType:I

    .line 127
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initGesture()V

    .line 128
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 3037
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 3038
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;I)Z
    .locals 1

    .prologue
    .line 2256
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->runOptionItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;
    .locals 1

    .prologue
    .line 3051
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)I
    .locals 1

    .prologue
    .line 3055
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentEffectType:I

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 3041
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 3067
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmapForApplyThread:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Z
    .locals 1

    .prologue
    .line 3058
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsDestroying:Z

    return v0
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;I)V
    .locals 0

    .prologue
    .line 3055
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentEffectType:I

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Z
    .locals 1

    .prologue
    .line 3100
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->effectFlag:Z

    return v0
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .locals 1

    .prologue
    .line 3043
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    return-object v0
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 3042
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 3039
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 3060
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V
    .locals 0

    .prologue
    .line 348
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->setMainBtnListener()V

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)J
    .locals 2

    .prologue
    .line 3061
    iget-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentTime:J

    return-wide v0
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;)V
    .locals 0

    .prologue
    .line 3036
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    return-void
.end method

.method static synthetic access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;
    .locals 1

    .prologue
    .line 3036
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    return-object v0
.end method

.method static synthetic access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 3066
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$26(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 3066
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$27(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 3047
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;J)V
    .locals 1

    .prologue
    .line 3061
    iput-wide p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentTime:J

    return-void
.end method

.method static synthetic access$29(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Z)V
    .locals 0

    .prologue
    .line 3100
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->effectFlag:Z

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 3040
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$30(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V
    .locals 0

    .prologue
    .line 1102
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->init3DepthActionBar()V

    return-void
.end method

.method static synthetic access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V
    .locals 0

    .prologue
    .line 624
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initMiddleButton()V

    return-void
.end method

.method static synthetic access$32(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;I)V
    .locals 0

    .prologue
    .line 2716
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->checkHelpPopup(I)V

    return-void
.end method

.method static synthetic access$33(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;
    .locals 1

    .prologue
    .line 3044
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    return-object v0
.end method

.method static synthetic access$34(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Z
    .locals 1

    .prologue
    .line 3057
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsMinimum:Z

    return v0
.end method

.method static synthetic access$35(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V
    .locals 0

    .prologue
    .line 1676
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->clearnSelectionArea()V

    return-void
.end method

.method static synthetic access$36(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Z)V
    .locals 0

    .prologue
    .line 3071
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawOriginal:Z

    return-void
.end method

.method static synthetic access$37(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$38(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewHeight()I

    move-result v0

    return v0
.end method

.method static synthetic access$39(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Z)V
    .locals 0

    .prologue
    .line 3094
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mEdit:Z

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 3052
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method static synthetic access$40(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V
    .locals 0

    .prologue
    .line 2811
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->refreshImageAndBottomButtons()V

    return-void
.end method

.method static synthetic access$41(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Z)V
    .locals 0

    .prologue
    .line 3088
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mLongpressButton:Z

    return-void
.end method

.method static synthetic access$42(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Z
    .locals 1

    .prologue
    .line 3088
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mLongpressButton:Z

    return v0
.end method

.method static synthetic access$43(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Landroid/content/Intent;Z)V
    .locals 0

    .prologue
    .line 2327
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    return-void
.end method

.method static synthetic access$44(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;I)V
    .locals 0

    .prologue
    .line 3080
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mOptionItemId:I

    return-void
.end method

.method static synthetic access$45(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V
    .locals 0

    .prologue
    .line 1694
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->doCancel()V

    return-void
.end method

.method static synthetic access$46(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V
    .locals 0

    .prologue
    .line 1731
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->doDone()V

    return-void
.end method

.method static synthetic access$47(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 3067
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmapForApplyThread:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$48(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;
    .locals 1

    .prologue
    .line 3046
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mApplyPreviewHandler:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;

    return-object v0
.end method

.method static synthetic access$49(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;I)V
    .locals 0

    .prologue
    .line 3084
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentSaveSize:I

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)I
    .locals 1

    .prologue
    .line 3084
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentSaveSize:I

    return v0
.end method

.method static synthetic access$50(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V
    .locals 0

    .prologue
    .line 3045
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    return-void
.end method

.method static synthetic access$51(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Z
    .locals 1

    .prologue
    .line 3097
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->isReverseAnimRunning:Z

    return v0
.end method

.method static synthetic access$52(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Z)V
    .locals 0

    .prologue
    .line 3097
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->isReverseAnimRunning:Z

    return-void
.end method

.method static synthetic access$53(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;I)V
    .locals 0

    .prologue
    .line 3099
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAlpha:I

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 3037
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 3072
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 3072
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)I
    .locals 1

    .prologue
    .line 3080
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mOptionItemId:I

    return v0
.end method

.method private checkHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 2718
    const/4 v0, 0x0

    .line 2719
    .local v0, "isNoShow":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->isDoNotShowPopup(Landroid/content/Context;I)Z

    move-result v0

    .line 2721
    if-nez v0, :cond_0

    .line 2723
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-nez v1, :cond_0

    .line 2725
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initHelpPopup(I)V

    .line 2726
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->showCurrentModeHelpPopup()V

    .line 2729
    :cond_0
    return-void
.end method

.method private clearnSelectionArea()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1678
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1681
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawPathList()Landroid/graphics/Path;

    move-result-object v2

    .line 1682
    .local v2, "path":Landroid/graphics/Path;
    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 1683
    invoke-virtual {v2}, Landroid/graphics/Path;->rewind()V

    .line 1684
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskBuffer()[B

    move-result-object v3

    invoke-static {v3, v7}, Ljava/util/Arrays;->fill([BB)V

    .line 1685
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    new-instance v4, Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    invoke-direct {v4, v5, v6, v7, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPreviewMaskRoi(Landroid/graphics/Rect;)V

    .line 1687
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;-><init>()V

    .line 1688
    .local v0, "clearEffect":Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->init(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 1689
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    invoke-direct {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;)V

    .line 1690
    .local v1, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addMaskBuffer(Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;)V

    .line 1691
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->destroy()V

    .line 1693
    .end local v0    # "clearEffect":Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;
    .end local v1    # "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_0
    return-void
.end method

.method private doCancel()V
    .locals 12

    .prologue
    const v11, 0x15001506

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 1696
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 1698
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1699
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1702
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1705
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1706
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 1708
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 1700
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 1710
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->set2depth()V

    .line 1712
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->setMainBtnListener()V

    .line 1713
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 1715
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->effectFlag:Z

    .line 1716
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-direct {p0, v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    move-result-object v4

    invoke-virtual {v3, v11, v8, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 1718
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getEnhance()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1719
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3, v11}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getID(I)Landroid/widget/LinearLayout;

    move-result-object v10

    .line 1720
    .local v10, "btn":Landroid/widget/LinearLayout;
    const v3, 0x3e99999a    # 0.3f

    invoke-virtual {v10, v3}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1721
    invoke-virtual {v10, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1729
    :goto_0
    return-void

    .line 1725
    .end local v10    # "btn":Landroid/widget/LinearLayout;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2, v11}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getID(I)Landroid/widget/LinearLayout;

    move-result-object v10

    .line 1726
    .restart local v10    # "btn":Landroid/widget/LinearLayout;
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v10, v2}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1727
    invoke-virtual {v10, v8}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method private doDone()V
    .locals 9

    .prologue
    const v8, 0x15001506

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 1733
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->applyPreview()V

    .line 1735
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    invoke-direct {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;)V

    .line 1737
    .local v4, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1738
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 1739
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 1737
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([IIILcom/sec/android/mimage/photoretouching/Core/EffectInfo;Z)V

    .line 1741
    sput-boolean v5, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->enhanceFlag:Z

    .line 1742
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->effectFlag:Z

    .line 1744
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->set2depth()V

    .line 1745
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-direct {p0, v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    move-result-object v1

    invoke-virtual {v0, v8, v7, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 1746
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v8}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getID(I)Landroid/widget/LinearLayout;

    move-result-object v6

    .line 1747
    .local v6, "btn":Landroid/widget/LinearLayout;
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1748
    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1749
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->setMainBtnListener()V

    .line 1750
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 1751
    return-void
.end method

.method private getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 151
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    return-object v0
.end method

.method private init2DepthActionBar()V
    .locals 5

    .prologue
    const/4 v2, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 739
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isValidBackKey()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 741
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 744
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$10;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 741
    invoke-virtual {v0, v4, v2, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 786
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0x11

    .line 789
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$11;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 786
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 822
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 825
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$12;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 822
    invoke-virtual {v0, v4, v3, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 881
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x2

    .line 884
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 881
    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 947
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xe

    .line 950
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$14;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 947
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 977
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 980
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$15;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 977
    invoke-virtual {v0, v1, v4, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1009
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0x10

    .line 1012
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$16;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$16;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 1009
    invoke-virtual {v0, v1, v4, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1048
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x5

    .line 1051
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$17;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$17;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 1048
    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1087
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 1089
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 1091
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1092
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isSaved()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 1100
    :cond_0
    :goto_1
    return-void

    .line 782
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    goto :goto_0

    .line 1095
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isSaved()Z

    move-result v1

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_1
.end method

.method private init3DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1104
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 1106
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 1107
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 1110
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$18;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$18;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 1107
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1132
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 1135
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$19;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$19;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 1132
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1158
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 1159
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 1160
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 1162
    :cond_0
    return-void
.end method

.method private initBrushDialog()V
    .locals 1

    .prologue
    .line 1166
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-nez v0, :cond_0

    .line 1168
    :cond_0
    return-void
.end method

.method private initColorEffect(I)V
    .locals 0
    .param p1, "effectType"    # I

    .prologue
    .line 1172
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentEffectType:I

    .line 1173
    sparse-switch p1, :sswitch_data_0

    .line 1194
    :sswitch_0
    return-void

    .line 1173
    :sswitch_data_0
    .sparse-switch
        0x15001500 -> :sswitch_0
        0x15001501 -> :sswitch_0
        0x15001502 -> :sswitch_0
        0x15001503 -> :sswitch_0
        0x15001504 -> :sswitch_0
        0x15001505 -> :sswitch_0
        0x15001607 -> :sswitch_0
        0x15001608 -> :sswitch_0
    .end sparse-switch
.end method

.method private initColorView(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/high16 v5, 0x42c80000    # 100.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v3, 0x40000000    # 2.0f

    .line 2147
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHandler:Landroid/os/Handler;

    .line 2148
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    .line 2149
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 2150
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 2151
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2152
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 2154
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->setInterface(Ljava/lang/Object;)V

    .line 2156
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPaint:Landroid/graphics/Paint;

    .line 2157
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2159
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mClearPaint:Landroid/graphics/Paint;

    .line 2160
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mClearPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 2162
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mLinePaint:Landroid/graphics/Paint;

    .line 2163
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2164
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2166
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 2167
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2168
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2169
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    const/4 v2, 0x4

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-direct {v1, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 2171
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 2172
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 2173
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2174
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmapForApplyThread:Landroid/graphics/Bitmap;

    .line 2175
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    .line 2176
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$26;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$26;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->setOnCallback(Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;)V

    .line 2204
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    const/high16 v1, 0x15000000

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$27;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$27;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;-><init>(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 2211
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initProgressText()V

    .line 2213
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    .line 2214
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$28;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$28;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->setOnCallback(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;)V

    .line 2229
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v5

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v5

    if-gez v0, :cond_1

    .line 2230
    :cond_0
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsMinimum:Z

    .line 2234
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initBrushDialog()V

    .line 2235
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->setViewLayerType(I)V

    .line 2238
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAnimationBuffer:[I

    .line 2241
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_1

    const/high16 v2, 0x40400000    # 3.0f

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPathEffect:Landroid/graphics/DashPathEffect;

    .line 2242
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 2243
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2244
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPathEffect:Landroid/graphics/DashPathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 2245
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2246
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2248
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 2249
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint1:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2250
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint1:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2251
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2254
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mApplyPreviewHandler:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;

    .line 2255
    return-void

    .line 2232
    :cond_1
    iput-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsMinimum:Z

    goto :goto_0

    .line 2169
    nop

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data

    .line 2241
    :array_1
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method

.method private initGesture()V
    .locals 3

    .prologue
    .line 2743
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$36;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$36;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 2799
    .local v0, "gesture":Landroid/view/GestureDetector$OnGestureListener;
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 2800
    return-void
.end method

.method private initHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 2732
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 2733
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$35;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$35;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->setHelpPopupCallback(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;)V

    .line 2740
    return-void
.end method

.method private initMiddleButton()V
    .locals 13

    .prologue
    const v12, 0x7f050231

    const/4 v11, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 625
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050236

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 626
    .local v5, "viewWitdh":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050238

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 627
    .local v4, "viewHeight":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050239

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 628
    .local v3, "textSize":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    const v7, 0x7f0601c0

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 629
    .local v2, "text":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->toUppercaseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 630
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 632
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    const v7, 0x7f060193

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 633
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050237

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 635
    :cond_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    const v7, 0x7f0900b6

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    .line 637
    const-string v6, "sec-roboto-light"

    invoke-static {v6, v10}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 639
    .local v0, "font":Landroid/graphics/Typeface;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    if-eqz v6, :cond_2

    .line 642
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-static {v6, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->doNeedUppercaseString(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 643
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->toUppercaseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 645
    :cond_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    const v7, 0x7f020326

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 646
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 647
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 648
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    int-to-float v7, v3

    invoke-virtual {v6, v10, v7}, Landroid/widget/Button;->setTextSize(IF)V

    .line 649
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f040034

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v9, v9, v9, v7}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    .line 650
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v5}, Landroid/widget/Button;->setWidth(I)V

    .line 651
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v4}, Landroid/widget/Button;->setHeight(I)V

    .line 652
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v11}, Landroid/widget/Button;->setEnabled(Z)V

    .line 653
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v10}, Landroid/widget/Button;->setPressed(Z)V

    .line 654
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/Button;->setSingleLine()V

    .line 655
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 656
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v11}, Landroid/widget/Button;->setHorizontalFadingEdgeEnabled(Z)V

    .line 657
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 659
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 660
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v6, 0x9

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 661
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f05022e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 662
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050230

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 663
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 664
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    .line 665
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    const/16 v9, 0x8

    .line 664
    invoke-virtual {v6, v7, v10, v8, v9}, Landroid/widget/Button;->setPadding(IIII)V

    .line 667
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mMiddleBtn:Landroid/widget/Button;

    new-instance v7, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;

    invoke-direct {v7, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 691
    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    return-void
.end method

.method private initSaveOptionDialog()V
    .locals 11

    .prologue
    const v10, 0x7f0601f0

    const v9, 0x7f0601cb

    const/16 v2, 0x9

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 2590
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2643
    :goto_0
    return-void

    .line 2595
    :cond_0
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$29;

    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$29;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 2609
    .local v8, "saveListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2611
    const v3, 0x7f0601cd

    .line 2613
    const/4 v5, 0x1

    .line 2615
    const v7, 0x103012e

    .line 2609
    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZZLandroid/graphics/Point;I)V

    .line 2617
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 2618
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x810

    move v3, v9

    move v5, v9

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2622
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x820

    move v3, v10

    move v5, v10

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2624
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$30;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$30;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2631
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$31;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$31;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2642
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    goto :goto_0
.end method

.method private initSaveYesNoCancelForFinish()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 2647
    const/4 v7, 0x0

    .line 2648
    .local v7, "fileName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 2649
    .local v8, "path":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 2651
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2652
    if-nez v7, :cond_0

    .line 2654
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2658
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2659
    const/16 v2, 0x8

    .line 2660
    const v3, 0x7f06019d

    .line 2661
    const/4 v4, 0x1

    .line 2663
    const v6, 0x103012e

    .line 2658
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2665
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 2666
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x500

    .line 2669
    const v4, 0x7f060192

    move v2, v9

    move v3, v9

    move-object v6, v5

    .line 2666
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2673
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    .line 2674
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$32;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$32;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 2673
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnShowListener;)V

    .line 2696
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601b2

    .line 2697
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$33;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$33;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 2696
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2705
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 2706
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$34;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$34;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 2705
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2713
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2714
    return-void
.end method

.method private initSetAsDialog()V
    .locals 7

    .prologue
    .line 2576
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 2578
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2579
    const v2, 0x7f090158

    .line 2580
    const v3, 0x7f0600a3

    .line 2581
    const/4 v4, 0x0

    .line 2582
    const/4 v5, 0x0

    .line 2583
    const v6, 0x103012e

    .line 2578
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2584
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2586
    :cond_0
    return-void
.end method

.method private initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isShareVia"    # Z

    .prologue
    .line 2329
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2330
    .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    const/4 v2, 0x0

    .line 2331
    .local v2, "shareChooser":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 2332
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    const v4, 0x7f06000f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 2336
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Landroid/content/pm/LabeledIntent;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/pm/LabeledIntent;

    .line 2337
    .local v0, "extraIntents":[Landroid/content/pm/LabeledIntent;
    const-string v3, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2338
    const/high16 v3, 0x24000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2339
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2518
    return-void

    .line 2334
    .end local v0    # "extraIntents":[Landroid/content/pm/LabeledIntent;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    const v4, 0x7f0600a3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method private initShareViaDialog()V
    .locals 7

    .prologue
    .line 2564
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 2566
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2567
    const v2, 0x7f090157

    .line 2568
    const v3, 0x7f06000f

    .line 2569
    const/4 v4, 0x0

    .line 2570
    const/4 v5, 0x0

    .line 2571
    const v6, 0x103012e

    .line 2566
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2572
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2574
    :cond_0
    return-void
.end method

.method private initUndoRedoAllDialog()V
    .locals 15

    .prologue
    const v6, 0x103012e

    const/16 v1, 0x1000

    const/16 v14, 0x500

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 1871
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1873
    const v3, 0x7f0600a6

    move v4, v2

    .line 1871
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1878
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1879
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1880
    const v3, 0x7f06009c

    .line 1882
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$20;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$20;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 1879
    invoke-virtual {v0, v14, v3, v5, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1889
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060007

    .line 1890
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$21;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$21;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 1889
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1939
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060009

    .line 1940
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$22;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$22;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 1939
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1948
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1950
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1951
    const/4 v9, 0x2

    .line 1952
    const v10, 0x7f0600a1

    move v8, v1

    move v11, v2

    move-object v12, v5

    move v13, v6

    .line 1950
    invoke-virtual/range {v7 .. v13}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1957
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1958
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1959
    const v1, 0x7f0601ce

    .line 1961
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$23;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$23;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 1958
    invoke-virtual {v0, v14, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1968
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 1969
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$24;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$24;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 1968
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2018
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 2019
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$25;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$25;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 2018
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2027
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2028
    return-void
.end method

.method private refreshImageAndBottomButtons()V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/high16 v10, 0x42c80000    # 100.0f

    const/4 v2, 0x0

    .line 2813
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_1

    .line 2815
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-ltz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-gez v3, :cond_5

    .line 2816
    :cond_0
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsMinimum:Z

    .line 2820
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2821
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 2822
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 2823
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2826
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 2829
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 2830
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 2832
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 2824
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 2834
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "output":[I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_4

    .line 2836
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_3

    .line 2838
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-ltz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-gez v3, :cond_6

    .line 2839
    :cond_2
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsMinimum:Z

    .line 2843
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$37;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$37;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2853
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->invalidateViews()V

    .line 2854
    return-void

    .line 2818
    :cond_5
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsMinimum:Z

    goto :goto_0

    .line 2841
    :cond_6
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsMinimum:Z

    goto :goto_1
.end method

.method private runOptionItem(I)Z
    .locals 6
    .param p1, "optionItemId"    # I

    .prologue
    const v5, 0x7f090158

    const/4 v4, 0x7

    const/16 v3, 0x9

    .line 2258
    const/4 v0, 0x0

    .line 2259
    .local v0, "ret":Z
    const/4 v1, 0x0

    .line 2260
    .local v1, "uri":Landroid/net/Uri;
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mOptionItemId:I

    .line 2261
    sparse-switch p1, :sswitch_data_0

    .line 2325
    :cond_0
    :goto_0
    return v0

    .line 2264
    :sswitch_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2266
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2273
    :goto_1
    const/4 v0, 0x1

    .line 2274
    goto :goto_0

    .line 2271
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->openCamera(Landroid/content/Context;)V

    goto :goto_1

    .line 2276
    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2278
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2279
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2290
    :cond_2
    :goto_2
    const/4 v0, 0x1

    .line 2291
    goto :goto_0

    .line 2281
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f090157

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2282
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 2283
    if-nez v1, :cond_4

    .line 2285
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2287
    :cond_4
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    goto :goto_2

    .line 2293
    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v3, 0x2e000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 2294
    const/4 v0, 0x1

    .line 2295
    goto :goto_0

    .line 2298
    :sswitch_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2301
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2302
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2315
    :cond_5
    :goto_3
    const/4 v0, 0x1

    .line 2316
    goto/16 :goto_0

    .line 2304
    :cond_6
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2307
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 2308
    if-nez v1, :cond_7

    .line 2310
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2312
    :cond_7
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeSetAsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    .line 2313
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    goto :goto_3

    .line 2321
    :sswitch_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 2322
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v3, 0x17000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0

    .line 2261
    :sswitch_data_0
    .sparse-switch
        0x7f090007 -> :sswitch_2
        0x7f090157 -> :sswitch_1
        0x7f090158 -> :sswitch_3
        0x7f09015a -> :sswitch_0
        0x7f09015b -> :sswitch_4
    .end sparse-switch
.end method

.method private set2depth()V
    .locals 2

    .prologue
    .line 1651
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->init2DepthActionBar()V

    .line 1652
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    .line 1654
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1655
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 1656
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 1660
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1661
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 1662
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 1668
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->stop()V

    .line 1669
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setClearSelectedSubButton()V

    .line 1670
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 1672
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->clearnSelectionArea()V

    .line 1674
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->invalidateViews()V

    .line 1675
    return-void

    .line 1658
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    goto :goto_0

    .line 1664
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 1665
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_1
.end method

.method private setMainBtnListener()V
    .locals 8

    .prologue
    const v7, 0x10001006

    const v6, 0x10001005

    const v5, 0x10001004

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 350
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 351
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isVisibleTitle()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 353
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsMinimum:Z

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 356
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 381
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 382
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 383
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100b

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 555
    :goto_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getPreviousMode()I

    move-result v0

    const/high16 v1, -0x10000

    and-int/2addr v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 582
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    .line 583
    return-void

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$4;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 418
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 444
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$6;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 476
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$7;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    invoke-virtual {v0, v7, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 501
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEnableDecoration()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 503
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100b

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$8;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_0

    .line 531
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100b

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_0

    .line 537
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsMinimum:Z

    if-eqz v0, :cond_3

    .line 539
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 540
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 541
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 542
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 543
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100b

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto/16 :goto_0

    .line 547
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 548
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5, v3, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 549
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 550
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 551
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100b

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto/16 :goto_0

    .line 558
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001007

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 561
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001009

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 564
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001008

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 567
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 570
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 573
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 576
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001003

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 579
    :sswitch_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001002

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 555
    :sswitch_data_0
    .sparse-switch
        0x11100000 -> :sswitch_7
        0x11200000 -> :sswitch_6
        0x15000000 -> :sswitch_4
        0x16000000 -> :sswitch_3
        0x18000000 -> :sswitch_5
        0x1a000000 -> :sswitch_1
        0x31100000 -> :sswitch_2
        0x31200000 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v5, 0x15001506

    const/16 v3, 0xff

    const/4 v4, 0x1

    .line 1350
    const/4 v0, 0x1

    .line 1352
    .local v0, "ret":Z
    const/4 v1, 0x0

    .line 1358
    .local v1, "runningAnimation":Z
    if-eqz v1, :cond_1

    .line 1412
    :cond_0
    :goto_0
    :pswitch_0
    return v4

    .line 1361
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawOriginal:Z

    if-nez v2, :cond_2

    .line 1362
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentEffectType:I

    if-eq v2, v5, :cond_2

    .line 1363
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v2, p2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 1365
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1367
    const/4 v0, 0x1

    .line 1368
    goto :goto_0

    .line 1371
    :cond_3
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentEffectType:I

    packed-switch v2, :pswitch_data_0

    .line 1377
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    if-eq v2, v5, :cond_0

    .line 1378
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 1386
    :pswitch_1
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawOriginal:Z

    if-eqz v2, :cond_4

    .line 1388
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawOriginal:Z

    .line 1389
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->resetPreProgressValue()V

    .line 1392
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->invalidateViews()V

    .line 1394
    :cond_4
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v2

    if-eq v2, v4, :cond_0

    .line 1395
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->startseek()V

    .line 1396
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->isReverseAnimRunning:Z

    .line 1397
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAlpha:I

    if-le v2, v3, :cond_5

    .line 1399
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAlpha:I

    .line 1401
    :cond_5
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1371
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1378
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public backPressed()V
    .locals 2

    .prologue
    .line 696
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isVisibleTitle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 698
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 705
    :goto_0
    return-void

    .line 702
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->doCancel()V

    .line 703
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resetMenu()V

    goto :goto_0
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 1542
    return-void
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 1598
    const/4 v0, 0x0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 1603
    const/4 v0, 0x0

    return v0
.end method

.method public getStatusHeight()I
    .locals 3

    .prologue
    .line 1607
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1608
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1609
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1610
    iget v2, v0, Landroid/graphics/Rect;->top:I

    return v2
.end method

.method public initActionbar()V
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    if-eqz v0, :cond_3

    .line 711
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->init3DepthActionBar()V

    .line 712
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 721
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 722
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 723
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 727
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 728
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 729
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 734
    :cond_2
    :goto_2
    return-void

    .line 716
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->init2DepthActionBar()V

    .line 717
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    goto :goto_0

    .line 725
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    goto :goto_1

    .line 731
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 732
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_2
.end method

.method public initButtons()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 588
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_3

    .line 589
    const/4 v1, 0x0

    .line 590
    .local v1, "i":I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v3, :cond_3

    .line 591
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    if-eqz v3, :cond_0

    .line 592
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 593
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initSubViewButtons(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V

    .line 594
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->showSubBottomButton(I)V

    .line 596
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v4, 0x15001506

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getID(I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 597
    .local v0, "btn":Landroid/widget/LinearLayout;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getEnhance()Z

    move-result v3

    if-nez v3, :cond_1

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentEffectType:I

    if-eq v3, v6, :cond_2

    .line 598
    :cond_1
    const v3, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 599
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 602
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->setMainBtnListener()V

    .line 603
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initMiddleButton()V

    .line 605
    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_4

    .line 610
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    if-eqz v3, :cond_5

    .line 612
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->getPreviousSubStatus()I

    move-result v4

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSubViewButtonSelected(IZ)V

    .line 613
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 622
    .end local v0    # "btn":Landroid/widget/LinearLayout;
    .end local v1    # "i":I
    :cond_3
    :goto_1
    return-void

    .line 607
    .restart local v0    # "btn":Landroid/widget/LinearLayout;
    .restart local v1    # "i":I
    :cond_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 608
    .local v2, "v":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 605
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 617
    .end local v2    # "v":Landroid/view/View;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3, v7}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    goto :goto_1
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 1199
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initUndoRedoAllDialog()V

    .line 1200
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initShareViaDialog()V

    .line 1201
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initSetAsDialog()V

    .line 1202
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initSaveOptionDialog()V

    .line 1203
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initSaveYesNoCancelForFinish()V

    .line 1205
    return-void
.end method

.method public initEffect()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 134
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->getPreviousSubStatus()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->init(I)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->getPreviousEffectValue()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->setStep(F)V

    .line 139
    :cond_0
    return-void
.end method

.method public initProgressText()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1315
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mProgressTextRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 1316
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mProgressTextRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 1324
    :goto_0
    return-void

    .line 1318
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mProgressTextRect:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 1530
    return-void
.end method

.method public initTrayLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1533
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 1535
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 1537
    :cond_0
    return-void
.end method

.method public initView()V
    .locals 0

    .prologue
    .line 147
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 1472
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1473
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledSave()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1475
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1487
    :goto_0
    return-void

    .line 1479
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_1

    .line 1481
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 1482
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 1483
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1485
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1418
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsDestroying:Z

    .line 1419
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const v2, 0x7f0900bb

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1420
    .local v0, "assitLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1425
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;

    .line 1426
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1427
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPaint:Landroid/graphics/Paint;

    .line 1428
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mClearPaint:Landroid/graphics/Paint;

    .line 1429
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mLinePaint:Landroid/graphics/Paint;

    .line 1430
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 1432
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 1433
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1435
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1436
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_0

    .line 1437
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->free()V

    .line 1438
    :cond_0
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1440
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->Destroy()V

    .line 1441
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1443
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    .line 1445
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1446
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1452
    :cond_1
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAnimationBuffer:[I

    .line 1454
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mApplyPreviewHandler:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->destroy()V

    .line 1455
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mApplyPreviewHandler:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;

    .line 1457
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmapForApplyThread:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1460
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 1461
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1466
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    if-eqz v1, :cond_3

    .line 1467
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->destroy()V

    .line 1468
    :cond_3
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    .prologue
    const/16 v13, 0xff

    const/4 v2, 0x0

    .line 1210
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawOriginal:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_2

    .line 1212
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 1213
    .local v12, "tempOriginalViewBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1214
    .local v0, "tempCanvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 1216
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1219
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1220
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 1221
    const/4 v8, 0x1

    .line 1222
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 1214
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 1224
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPaint:Landroid/graphics/Paint;

    invoke-static {p1, v12, v1, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 1228
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->recycle()V

    .line 1270
    .end local v0    # "tempCanvas":Landroid/graphics/Canvas;
    .end local v12    # "tempOriginalViewBitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentEffectType:I

    const v3, 0x15001506

    if-eq v1, v3, :cond_1

    .line 1273
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->isReverseAnimRunning:Z

    if-eqz v1, :cond_5

    .line 1277
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAlpha:I

    if-lez v1, :cond_4

    .line 1280
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1281
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->getEffectType()I

    move-result v2

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawOriginal:Z

    .line 1282
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAlpha:I

    .line 1280
    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    .line 1283
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAlpha:I

    add-int/lit8 v1, v1, -0xf

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAlpha:I

    .line 1285
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1310
    :cond_1
    :goto_1
    return-void

    .line 1231
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mApplyPreviewHandler:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mApplyPreviewHandler:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1233
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmapForApplyThread:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPaint:Landroid/graphics/Paint;

    invoke-static {p1, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 1234
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1236
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 1237
    .local v11, "p":Landroid/graphics/Path;
    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    .line 1238
    .local v10, "m":Landroid/graphics/Matrix;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v10, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1239
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawPathList()Landroid/graphics/Path;

    move-result-object v1

    invoke-virtual {v1, v10, v11}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 1240
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v11, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1241
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v11, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1242
    invoke-virtual {v11}, Landroid/graphics/Path;->rewind()V

    goto :goto_0

    .line 1248
    .end local v10    # "m":Landroid/graphics/Matrix;
    .end local v11    # "p":Landroid/graphics/Path;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPaint:Landroid/graphics/Paint;

    invoke-static {p1, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 1249
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1252
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 1253
    .restart local v11    # "p":Landroid/graphics/Path;
    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    .line 1254
    .restart local v10    # "m":Landroid/graphics/Matrix;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v10, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1255
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawPathList()Landroid/graphics/Path;

    move-result-object v1

    invoke-virtual {v1, v10, v11}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 1256
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v11, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1257
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v11, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1258
    invoke-virtual {v11}, Landroid/graphics/Path;->rewind()V

    goto/16 :goto_0

    .line 1289
    .end local v10    # "m":Landroid/graphics/Matrix;
    .end local v11    # "p":Landroid/graphics/Path;
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->resetPreProgressValue()V

    .line 1290
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->isReverseAnimRunning:Z

    goto/16 :goto_1

    .line 1294
    :cond_5
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAlpha:I

    if-gt v1, v13, :cond_6

    .line 1296
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1297
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->getEffectType()I

    move-result v2

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawOriginal:Z

    .line 1298
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAlpha:I

    .line 1296
    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    .line 1299
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAlpha:I

    add-int/lit8 v1, v1, 0xf

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mAlpha:I

    goto/16 :goto_1

    .line 1303
    :cond_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1304
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->getEffectType()I

    move-result v2

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDrawOriginal:Z

    .line 1303
    invoke-virtual {v1, p1, v2, v3, v13}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    goto/16 :goto_1
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 1490
    const/16 v1, 0x17

    if-eq p1, v1, :cond_0

    const/16 v1, 0x42

    if-ne p1, v1, :cond_3

    .line 1492
    :cond_0
    const/4 v0, 0x0

    .line 1493
    .local v0, "isFocused":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_1

    .line 1494
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    move-result v0

    .line 1497
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_2

    .line 1498
    if-nez v0, :cond_2

    .line 1499
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 1522
    .end local v0    # "isFocused":Z
    :cond_2
    :goto_0
    return v2

    .line 1504
    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 1506
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->backPressed()V

    goto :goto_0
.end method

.method public onLayout()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1614
    const-string v3, "bigheadk, onLayout()"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1616
    const/4 v1, 0x0

    .line 1627
    .local v1, "previewBuffer":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 1628
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V

    .line 1629
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1630
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewHeight()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 1632
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1633
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1636
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1639
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1640
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 1641
    const/4 v8, 0x1

    .line 1642
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 1634
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 1644
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->onConfigurationChanged()V

    .line 1645
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mOnlayoutCalledTime:J

    .line 1646
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .line 1647
    return-void
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 1592
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->runOptionItem(I)Z

    .line 1593
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1550
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->msave:Z

    if-eqz v0, :cond_0

    .line 1551
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1552
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentSaveSize:I

    const v1, 0x7a1200

    if-ne v0, v1, :cond_3

    .line 1553
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cb

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setSelectedFalseAnotherButtons(I)V

    .line 1556
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->msave:Z

    .line 1558
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_1

    .line 1559
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 1561
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1562
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 1563
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1576
    :cond_2
    return-void

    .line 1555
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601f0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setSelectedFalseAnotherButtons(I)V

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1579
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->pause()V

    .line 1580
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f090158

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1581
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->msetas:Z

    .line 1582
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 1584
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1585
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->msave:Z

    .line 1586
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 1588
    :cond_1
    return-void
.end method

.method public refreshView()V
    .locals 3

    .prologue
    .line 2805
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 2807
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->refreshImageAndBottomButtons()V

    .line 2808
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 1

    .prologue
    .line 1328
    const-string v0, "bigheadk, setConfigurationChanged()"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1329
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 1330
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 1331
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 1332
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 1333
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V

    .line 1334
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_0

    .line 1335
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 1337
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initProgressText()V

    .line 1339
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initMiddleButton()V

    .line 1341
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPopupLayout()V

    .line 1345
    return-void
.end method
