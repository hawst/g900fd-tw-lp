.class Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$14;
.super Ljava/lang/Object;
.source "ViewButtonsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private final synthetic val$button:Landroid/widget/LinearLayout;

.field private final synthetic val$params:Landroid/widget/LinearLayout$LayoutParams;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Landroid/widget/LinearLayout$LayoutParams;Landroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$14;->val$params:Landroid/widget/LinearLayout$LayoutParams;

    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$14;->val$button:Landroid/widget/LinearLayout;

    .line 2755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 2760
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2761
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mButtonWidth:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)I

    move-result v2

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->adjustBottomButtonMargin(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;II)V

    .line 2762
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$14;->val$params:Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v0, :cond_1

    .line 2763
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$14;->val$button:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$14;->val$params:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2764
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$14;->val$button:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->invalidate()V

    .line 2765
    return-void
.end method
