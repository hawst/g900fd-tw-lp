.class public Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;
.super Ljava/lang/Object;
.source "MultigridDetailedInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo$DetailedInfoCallback;
    }
.end annotation


# static fields
.field public static final BYTE:I = 0x4

.field public static final B_GRID_COUNT:I = 0x4

.field public static final B_STYLE:I = 0x0

.field public static final B_STYLEB_CTRL_PT1_B:I = 0x7c

.field public static final B_STYLEB_CTRL_PT1_L:I = 0x70

.field public static final B_STYLEB_CTRL_PT1_R:I = 0x78

.field public static final B_STYLEB_CTRL_PT1_T:I = 0x74

.field public static final B_STYLEB_CTRL_PT2_B:I = 0x8c

.field public static final B_STYLEB_CTRL_PT2_L:I = 0x80

.field public static final B_STYLEB_CTRL_PT2_R:I = 0x88

.field public static final B_STYLEB_CTRL_PT2_T:I = 0x84

.field public static final B_STYLEB_CTRL_PT3_B:I = 0x9c

.field public static final B_STYLEB_CTRL_PT3_L:I = 0x90

.field public static final B_STYLEB_CTRL_PT3_R:I = 0x98

.field public static final B_STYLEB_CTRL_PT3_T:I = 0x94

.field public static final B_STYLEB_CTRL_PT4_B:I = 0xac

.field public static final B_STYLEB_CTRL_PT4_L:I = 0xa0

.field public static final B_STYLEB_CTRL_PT4_R:I = 0xa8

.field public static final B_STYLEB_CTRL_PT4_T:I = 0xa4

.field public static final B_STYLEB_CTRL_PT5_B:I = 0xbc

.field public static final B_STYLEB_CTRL_PT5_L:I = 0xb0

.field public static final B_STYLEB_CTRL_PT5_R:I = 0xb8

.field public static final B_STYLEB_CTRL_PT5_T:I = 0xb4

.field public static final B_STYLEB_DRAWRECT1_B:I = 0x1c

.field public static final B_STYLEB_DRAWRECT1_L:I = 0x10

.field public static final B_STYLEB_DRAWRECT1_R:I = 0x18

.field public static final B_STYLEB_DRAWRECT1_T:I = 0x14

.field public static final B_STYLEB_DRAWRECT2_B:I = 0x2c

.field public static final B_STYLEB_DRAWRECT2_L:I = 0x20

.field public static final B_STYLEB_DRAWRECT2_R:I = 0x28

.field public static final B_STYLEB_DRAWRECT2_T:I = 0x24

.field public static final B_STYLEB_DRAWRECT3_B:I = 0x3c

.field public static final B_STYLEB_DRAWRECT3_L:I = 0x30

.field public static final B_STYLEB_DRAWRECT3_R:I = 0x38

.field public static final B_STYLEB_DRAWRECT3_T:I = 0x34

.field public static final B_STYLEB_DRAWRECT4_B:I = 0x4c

.field public static final B_STYLEB_DRAWRECT4_L:I = 0x40

.field public static final B_STYLEB_DRAWRECT4_R:I = 0x48

.field public static final B_STYLEB_DRAWRECT4_T:I = 0x44

.field public static final B_STYLEB_DRAWRECT5_B:I = 0x5c

.field public static final B_STYLEB_DRAWRECT5_L:I = 0x50

.field public static final B_STYLEB_DRAWRECT5_R:I = 0x58

.field public static final B_STYLEB_DRAWRECT5_T:I = 0x54

.field public static final B_STYLEB_DRAWRECT6_B:I = 0x6c

.field public static final B_STYLEB_DRAWRECT6_L:I = 0x60

.field public static final B_STYLEB_DRAWRECT6_R:I = 0x68

.field public static final B_STYLEB_DRAWRECT6_T:I = 0x64

.field public static final B_STYLEB_DRAWSRCRECT1_B:I = 0xcc

.field public static final B_STYLEB_DRAWSRCRECT1_L:I = 0xc0

.field public static final B_STYLEB_DRAWSRCRECT1_R:I = 0xc8

.field public static final B_STYLEB_DRAWSRCRECT1_T:I = 0xc4

.field public static final B_STYLEB_DRAWSRCRECT2_B:I = 0xdc

.field public static final B_STYLEB_DRAWSRCRECT2_L:I = 0xd0

.field public static final B_STYLEB_DRAWSRCRECT2_R:I = 0xd8

.field public static final B_STYLEB_DRAWSRCRECT2_T:I = 0xd4

.field public static final B_STYLEB_DRAWSRCRECT3_B:I = 0xec

.field public static final B_STYLEB_DRAWSRCRECT3_L:I = 0xe0

.field public static final B_STYLEB_DRAWSRCRECT3_R:I = 0xe8

.field public static final B_STYLEB_DRAWSRCRECT3_T:I = 0xe4

.field public static final B_STYLEB_DRAWSRCRECT4_B:I = 0xfc

.field public static final B_STYLEB_DRAWSRCRECT4_L:I = 0xf0

.field public static final B_STYLEB_DRAWSRCRECT4_R:I = 0xf8

.field public static final B_STYLEB_DRAWSRCRECT4_T:I = 0xf4

.field public static final B_STYLEB_DRAWSRCRECT5_B:I = 0x10c

.field public static final B_STYLEB_DRAWSRCRECT5_L:I = 0x100

.field public static final B_STYLEB_DRAWSRCRECT5_R:I = 0x108

.field public static final B_STYLEB_DRAWSRCRECT5_T:I = 0x104

.field public static final B_STYLEB_DRAWSRCRECT6_B:I = 0x11c

.field public static final B_STYLEB_DRAWSRCRECT6_L:I = 0x110

.field public static final B_STYLEB_DRAWSRCRECT6_R:I = 0x118

.field public static final B_STYLEB_DRAWSRCRECT6_T:I = 0x114

.field public static final B_STYLEE_ANGLE1:I = 0x270

.field public static final B_STYLEE_ANGLE2:I = 0x274

.field public static final B_STYLEE_ANGLE3:I = 0x278

.field public static final B_STYLEE_ANGLE4:I = 0x27c

.field public static final B_STYLEE_ANGLE5:I = 0x280

.field public static final B_STYLEE_ANGLE6:I = 0x284

.field public static final B_STYLEE_CENTER1_PT_X:I = 0x240

.field public static final B_STYLEE_CENTER1_PT_Y:I = 0x244

.field public static final B_STYLEE_CENTER2_PT_X:I = 0x248

.field public static final B_STYLEE_CENTER2_PT_Y:I = 0x24c

.field public static final B_STYLEE_CENTER3_PT_X:I = 0x250

.field public static final B_STYLEE_CENTER3_PT_Y:I = 0x254

.field public static final B_STYLEE_CENTER4_PT_X:I = 0x258

.field public static final B_STYLEE_CENTER4_PT_Y:I = 0x25c

.field public static final B_STYLEE_CENTER5_PT_X:I = 0x260

.field public static final B_STYLEE_CENTER5_PT_Y:I = 0x264

.field public static final B_STYLEE_CENTER6_PT_X:I = 0x268

.field public static final B_STYLEE_CENTER6_PT_Y:I = 0x26c

.field public static final B_STYLEE_DESTROI1_B:I = 0x12c

.field public static final B_STYLEE_DESTROI1_L:I = 0x120

.field public static final B_STYLEE_DESTROI1_PT1_X:I = 0x180

.field public static final B_STYLEE_DESTROI1_PT1_Y:I = 0x184

.field public static final B_STYLEE_DESTROI1_PT2_X:I = 0x188

.field public static final B_STYLEE_DESTROI1_PT2_Y:I = 0x18c

.field public static final B_STYLEE_DESTROI1_PT3_X:I = 0x190

.field public static final B_STYLEE_DESTROI1_PT3_Y:I = 0x194

.field public static final B_STYLEE_DESTROI1_PT4_X:I = 0x198

.field public static final B_STYLEE_DESTROI1_PT4_Y:I = 0x19c

.field public static final B_STYLEE_DESTROI1_R:I = 0x128

.field public static final B_STYLEE_DESTROI1_T:I = 0x124

.field public static final B_STYLEE_DESTROI2_B:I = 0x13c

.field public static final B_STYLEE_DESTROI2_L:I = 0x130

.field public static final B_STYLEE_DESTROI2_PT1_X:I = 0x1a0

.field public static final B_STYLEE_DESTROI2_PT1_Y:I = 0x1a4

.field public static final B_STYLEE_DESTROI2_PT2_X:I = 0x1a8

.field public static final B_STYLEE_DESTROI2_PT2_Y:I = 0x1ac

.field public static final B_STYLEE_DESTROI2_PT3_X:I = 0x1b0

.field public static final B_STYLEE_DESTROI2_PT3_Y:I = 0x1b4

.field public static final B_STYLEE_DESTROI2_PT4_X:I = 0x1b8

.field public static final B_STYLEE_DESTROI2_PT4_Y:I = 0x1bc

.field public static final B_STYLEE_DESTROI2_R:I = 0x138

.field public static final B_STYLEE_DESTROI2_T:I = 0x134

.field public static final B_STYLEE_DESTROI3_B:I = 0x14c

.field public static final B_STYLEE_DESTROI3_L:I = 0x140

.field public static final B_STYLEE_DESTROI3_PT1_X:I = 0x1c0

.field public static final B_STYLEE_DESTROI3_PT1_Y:I = 0x1c4

.field public static final B_STYLEE_DESTROI3_PT2_X:I = 0x1c8

.field public static final B_STYLEE_DESTROI3_PT2_Y:I = 0x1cc

.field public static final B_STYLEE_DESTROI3_PT3_X:I = 0x1d0

.field public static final B_STYLEE_DESTROI3_PT3_Y:I = 0x1d4

.field public static final B_STYLEE_DESTROI3_PT4_X:I = 0x1d8

.field public static final B_STYLEE_DESTROI3_PT4_Y:I = 0x1dc

.field public static final B_STYLEE_DESTROI3_R:I = 0x148

.field public static final B_STYLEE_DESTROI3_T:I = 0x144

.field public static final B_STYLEE_DESTROI4_B:I = 0x15c

.field public static final B_STYLEE_DESTROI4_L:I = 0x150

.field public static final B_STYLEE_DESTROI4_PT1_X:I = 0x1e0

.field public static final B_STYLEE_DESTROI4_PT1_Y:I = 0x1e4

.field public static final B_STYLEE_DESTROI4_PT2_X:I = 0x1e8

.field public static final B_STYLEE_DESTROI4_PT2_Y:I = 0x1ec

.field public static final B_STYLEE_DESTROI4_PT3_X:I = 0x1f0

.field public static final B_STYLEE_DESTROI4_PT3_Y:I = 0x1f4

.field public static final B_STYLEE_DESTROI4_PT4_X:I = 0x1f8

.field public static final B_STYLEE_DESTROI4_PT4_Y:I = 0x1fc

.field public static final B_STYLEE_DESTROI4_R:I = 0x158

.field public static final B_STYLEE_DESTROI4_T:I = 0x154

.field public static final B_STYLEE_DESTROI5_B:I = 0x16c

.field public static final B_STYLEE_DESTROI5_L:I = 0x160

.field public static final B_STYLEE_DESTROI5_PT1_X:I = 0x200

.field public static final B_STYLEE_DESTROI5_PT1_Y:I = 0x204

.field public static final B_STYLEE_DESTROI5_PT2_X:I = 0x208

.field public static final B_STYLEE_DESTROI5_PT2_Y:I = 0x20c

.field public static final B_STYLEE_DESTROI5_PT3_X:I = 0x210

.field public static final B_STYLEE_DESTROI5_PT3_Y:I = 0x214

.field public static final B_STYLEE_DESTROI5_PT4_X:I = 0x218

.field public static final B_STYLEE_DESTROI5_PT4_Y:I = 0x21c

.field public static final B_STYLEE_DESTROI5_R:I = 0x168

.field public static final B_STYLEE_DESTROI5_T:I = 0x164

.field public static final B_STYLEE_DESTROI6_B:I = 0x17c

.field public static final B_STYLEE_DESTROI6_L:I = 0x170

.field public static final B_STYLEE_DESTROI6_PT1_X:I = 0x220

.field public static final B_STYLEE_DESTROI6_PT1_Y:I = 0x224

.field public static final B_STYLEE_DESTROI6_PT2_X:I = 0x228

.field public static final B_STYLEE_DESTROI6_PT2_Y:I = 0x22c

.field public static final B_STYLEE_DESTROI6_PT3_X:I = 0x230

.field public static final B_STYLEE_DESTROI6_PT3_Y:I = 0x234

.field public static final B_STYLEE_DESTROI6_PT4_X:I = 0x238

.field public static final B_STYLEE_DESTROI6_PT4_Y:I = 0x23c

.field public static final B_STYLEE_DESTROI6_R:I = 0x178

.field public static final B_STYLEE_DESTROI6_T:I = 0x174

.field public static final B_TOTAL_HEIGHT:I = 0xc

.field public static final B_TOTAL_WIDTH:I = 0x8

.field public static final MAX_CONTROL_COUNT_FOR_BCD:I = 0x5

.field public static final MAX_COUNT:I = 0x6


# instance fields
.field private index:I

.field private mBuf:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    .line 424
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->index:I

    .line 205
    return-void
.end method

.method private getBufLength()I
    .locals 1

    .prologue
    .line 421
    const/16 v0, 0x288

    return v0
.end method


# virtual methods
.method public checkBuffer()V
    .locals 7

    .prologue
    const/16 v5, 0x10

    .line 659
    const/4 v0, 0x0

    .line 660
    .local v0, "rect":Landroid/graphics/Rect;
    const/4 v1, 0x0

    .line 665
    .local v1, "rectf":Landroid/graphics/RectF;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBuffer B_STYLE "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 667
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBuffer B_GRID_COUNT "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 668
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBuffer B_STYLEB_DRAWRECT1_L "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 673
    new-instance v1, Landroid/graphics/RectF;

    .end local v1    # "rectf":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x14

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x18

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v6, 0x1c

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 674
    .restart local v1    # "rectf":Landroid/graphics/RectF;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBuffer DrawRect 1 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 675
    new-instance v1, Landroid/graphics/RectF;

    .end local v1    # "rectf":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x24

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x28

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v6, 0x2c

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 676
    .restart local v1    # "rectf":Landroid/graphics/RectF;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBuffer DrawRect 2 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 677
    new-instance v1, Landroid/graphics/RectF;

    .end local v1    # "rectf":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x30

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x34

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x38

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v6, 0x3c

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 678
    .restart local v1    # "rectf":Landroid/graphics/RectF;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBuffer DrawRect 3 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 679
    new-instance v1, Landroid/graphics/RectF;

    .end local v1    # "rectf":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x40

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x44

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x48

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v6, 0x4c

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 680
    .restart local v1    # "rectf":Landroid/graphics/RectF;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBuffer DrawRect 4 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 683
    new-instance v1, Landroid/graphics/RectF;

    .end local v1    # "rectf":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x70

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x74

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x78

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v6, 0x7c

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 684
    .restart local v1    # "rectf":Landroid/graphics/RectF;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBuffer ControlPoint1 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 687
    new-instance v1, Landroid/graphics/RectF;

    .end local v1    # "rectf":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x84

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x88

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v6, 0x8c

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 688
    .restart local v1    # "rectf":Landroid/graphics/RectF;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBuffer ControlPoint2 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 691
    new-instance v0, Landroid/graphics/Rect;

    .end local v0    # "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0xc0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0xc4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0xc8

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v6, 0xcc

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v5

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 692
    .restart local v0    # "rect":Landroid/graphics/Rect;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBuffer DrawSrcRect 1 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 693
    new-instance v0, Landroid/graphics/Rect;

    .end local v0    # "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0xd0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0xd4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0xd8

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v6, 0xdc

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v5

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 694
    .restart local v0    # "rect":Landroid/graphics/Rect;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBuffer DrawSrcRect 2 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 695
    new-instance v0, Landroid/graphics/Rect;

    .end local v0    # "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0xe0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0xe4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0xe8

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v6, 0xec

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v5

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 696
    .restart local v0    # "rect":Landroid/graphics/Rect;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBuffer DrawSrcRect 3 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 697
    new-instance v0, Landroid/graphics/Rect;

    .end local v0    # "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0xf0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0xf4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0xf8

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v6, 0xfc

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v5

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 698
    .restart local v0    # "rect":Landroid/graphics/Rect;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBuffer DrawSrcRect 4 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 700
    return-void
.end method

.method public checkPosition(ILjava/nio/ByteBuffer;)Z
    .locals 3
    .param p1, "pos"    # I
    .param p2, "ret"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 426
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 428
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Multigrid DetaliedInfo checkPosition index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->index:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->index:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " not equal! pos1 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",  pos2 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 429
    const/4 v0, 0x0

    .line 431
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public fileLoad()Ljava/nio/ByteBuffer;
    .locals 9

    .prologue
    .line 739
    const-string v7, "Save.txt"

    .line 740
    .local v7, "name":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    sget-object v8, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-direct {v6, v8, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    .local v6, "in":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    sget-object v8, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 743
    .local v2, "dir":Ljava/io/File;
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->getBufLength()I

    move-result v8

    new-array v0, v8, [B

    .line 745
    .local v0, "b":[B
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    .line 746
    const/4 v1, 0x0

    .line 772
    :goto_0
    return-object v1

    .line 749
    :cond_0
    const/4 v4, 0x0

    .line 752
    .local v4, "fIn":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 753
    .end local v4    # "fIn":Ljava/io/FileInputStream;
    .local v5, "fIn":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v5, v0}, Ljava/io/FileInputStream;->read([B)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-object v4, v5

    .line 759
    .end local v5    # "fIn":Ljava/io/FileInputStream;
    .restart local v4    # "fIn":Ljava/io/FileInputStream;
    :goto_1
    if-eqz v4, :cond_1

    .line 760
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/FileDescriptor;->sync()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 766
    :cond_1
    :goto_2
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 771
    :goto_3
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 772
    .local v1, "buf":Ljava/nio/ByteBuffer;
    goto :goto_0

    .line 754
    .end local v1    # "buf":Ljava/nio/ByteBuffer;
    :catch_0
    move-exception v3

    .line 755
    .local v3, "e":Ljava/lang/Exception;
    :goto_4
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 767
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 768
    .restart local v3    # "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 762
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v8

    goto :goto_2

    .line 754
    .end local v4    # "fIn":Ljava/io/FileInputStream;
    .restart local v5    # "fIn":Ljava/io/FileInputStream;
    :catch_3
    move-exception v3

    move-object v4, v5

    .end local v5    # "fIn":Ljava/io/FileInputStream;
    .restart local v4    # "fIn":Ljava/io/FileInputStream;
    goto :goto_4
.end method

.method public fileSave(Ljava/nio/ByteBuffer;)V
    .locals 8
    .param p1, "buf"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 704
    const-string v5, "Save.txt"

    .line 705
    .local v5, "name":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    sget-object v7, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-direct {v6, v7, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    .local v6, "out":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    sget-object v7, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 708
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 710
    .local v0, "b":[B
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 711
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 714
    :cond_0
    const/4 v3, 0x0

    .line 717
    .local v3, "fOut":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 718
    .end local v3    # "fOut":Ljava/io/FileOutputStream;
    .local v4, "fOut":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v4, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-object v3, v4

    .line 724
    .end local v4    # "fOut":Ljava/io/FileOutputStream;
    .restart local v3    # "fOut":Ljava/io/FileOutputStream;
    :goto_0
    if-eqz v3, :cond_1

    .line 725
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/FileDescriptor;->sync()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 731
    :cond_1
    :goto_1
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 735
    :goto_2
    return-void

    .line 719
    :catch_0
    move-exception v2

    .line 720
    .local v2, "e":Ljava/lang/Exception;
    :goto_3
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 732
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 733
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 727
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v7

    goto :goto_1

    .line 719
    .end local v3    # "fOut":Ljava/io/FileOutputStream;
    .restart local v4    # "fOut":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v2

    move-object v3, v4

    .end local v4    # "fOut":Ljava/io/FileOutputStream;
    .restart local v3    # "fOut":Ljava/io/FileOutputStream;
    goto :goto_3
.end method

.method public getAngle(I)I
    .locals 3
    .param p1, "i"    # I

    .prologue
    .line 385
    const/4 v0, -0x1

    .line 386
    .local v0, "ret":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_0

    .line 388
    packed-switch p1, :pswitch_data_0

    .line 398
    :cond_0
    :goto_0
    return v0

    .line 390
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x270

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    .line 391
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x274

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    .line 392
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x278

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    .line 393
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x27c

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    .line 394
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x280

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    .line 395
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x284

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    .line 388
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getCenterPt(I)Landroid/graphics/Point;
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 369
    const/4 v0, 0x0

    .line 370
    .local v0, "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_0

    .line 372
    packed-switch p1, :pswitch_data_0

    .line 382
    :cond_0
    :goto_0
    return-object v0

    .line 374
    :pswitch_0
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x240

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x244

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto :goto_0

    .line 375
    :pswitch_1
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x248

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x24c

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto :goto_0

    .line 376
    :pswitch_2
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x250

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x254

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto :goto_0

    .line 377
    :pswitch_3
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x258

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x25c

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto :goto_0

    .line 378
    :pswitch_4
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x260

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x264

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto :goto_0

    .line 379
    :pswitch_5
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x268

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x26c

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 372
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getControlPoint(I)Landroid/graphics/RectF;
    .locals 6
    .param p1, "i"    # I

    .prologue
    .line 272
    const/4 v0, 0x0

    .line 273
    .local v0, "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_0

    .line 275
    packed-switch p1, :pswitch_data_0

    .line 284
    :cond_0
    :goto_0
    return-object v0

    .line 277
    :pswitch_0
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x70

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x74

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x78

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x7c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto :goto_0

    .line 278
    :pswitch_1
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x84

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x88

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x8c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto :goto_0

    .line 279
    :pswitch_2
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x90

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x94

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x98

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x9c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto :goto_0

    .line 280
    :pswitch_3
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0xa0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0xa4

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0xa8

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0xac

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto/16 :goto_0

    .line 281
    :pswitch_4
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0xb0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0xb4

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0xb8

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0xbc

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto/16 :goto_0

    .line 275
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getDestPt(II)Landroid/graphics/Point;
    .locals 4
    .param p1, "gridIndex"    # I
    .param p2, "ptIndex"    # I

    .prologue
    .line 304
    const/4 v0, 0x0

    .line 305
    .local v0, "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_0

    .line 307
    packed-switch p1, :pswitch_data_0

    .line 365
    :cond_0
    :goto_0
    return-object v0

    .line 310
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 312
    :pswitch_1
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x180

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x184

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto :goto_0

    .line 313
    :pswitch_2
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x188

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x18c

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto :goto_0

    .line 314
    :pswitch_3
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x190

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x194

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto :goto_0

    .line 315
    :pswitch_4
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x198

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x19c

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto :goto_0

    .line 319
    :pswitch_5
    packed-switch p2, :pswitch_data_2

    goto :goto_0

    .line 321
    :pswitch_6
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x1a0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x1a4

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto :goto_0

    .line 322
    :pswitch_7
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x1a8

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x1ac

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 323
    :pswitch_8
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x1b0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x1b4

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 324
    :pswitch_9
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x1b8

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x1bc

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 328
    :pswitch_a
    packed-switch p2, :pswitch_data_3

    goto/16 :goto_0

    .line 330
    :pswitch_b
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x1c0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x1c4

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 331
    :pswitch_c
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x1c8

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x1cc

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 332
    :pswitch_d
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x1d0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x1d4

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 333
    :pswitch_e
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x1d8

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x1dc

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 337
    :pswitch_f
    packed-switch p2, :pswitch_data_4

    goto/16 :goto_0

    .line 339
    :pswitch_10
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x1e0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x1e4

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 340
    :pswitch_11
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x1e8

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x1ec

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 341
    :pswitch_12
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x1f0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x1f4

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 342
    :pswitch_13
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x1f8

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x1fc

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 346
    :pswitch_14
    packed-switch p2, :pswitch_data_5

    goto/16 :goto_0

    .line 348
    :pswitch_15
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x200

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x204

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 349
    :pswitch_16
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x208

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x20c

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 350
    :pswitch_17
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x210

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x214

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 351
    :pswitch_18
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x218

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x21c

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 355
    :pswitch_19
    packed-switch p2, :pswitch_data_6

    goto/16 :goto_0

    .line 357
    :pswitch_1a
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x220

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x224

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 358
    :pswitch_1b
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x228

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x22c

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 359
    :pswitch_1c
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x230

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x234

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 360
    :pswitch_1d
    new-instance v0, Landroid/graphics/Point;

    .end local v0    # "pt":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x238

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x23c

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .restart local v0    # "pt":Landroid/graphics/Point;
    goto/16 :goto_0

    .line 307
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
        :pswitch_a
        :pswitch_f
        :pswitch_14
        :pswitch_19
    .end packed-switch

    .line 310
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 319
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 328
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 337
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch

    .line 346
    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch

    .line 355
    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
    .end packed-switch
.end method

.method public getDestRoi(I)Landroid/graphics/RectF;
    .locals 6
    .param p1, "i"    # I

    .prologue
    .line 287
    const/4 v0, 0x0

    .line 288
    .local v0, "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_0

    .line 290
    packed-switch p1, :pswitch_data_0

    .line 300
    :cond_0
    :goto_0
    return-object v0

    .line 292
    :pswitch_0
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x120

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x124

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x128

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x12c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto :goto_0

    .line 293
    :pswitch_1
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x130

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x134

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x138

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x13c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto :goto_0

    .line 294
    :pswitch_2
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x140

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x144

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x148

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x14c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto :goto_0

    .line 295
    :pswitch_3
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x150

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x154

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x158

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x15c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto/16 :goto_0

    .line 296
    :pswitch_4
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x160

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x164

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x168

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x16c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto/16 :goto_0

    .line 297
    :pswitch_5
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x170

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x174

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x178

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x17c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto/16 :goto_0

    .line 290
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getDestRoiArray()[Landroid/graphics/RectF;
    .locals 3

    .prologue
    .line 413
    const/4 v2, 0x4

    new-array v1, v2, [Landroid/graphics/RectF;

    .line 414
    .local v1, "ret":[Landroid/graphics/RectF;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 416
    return-object v1

    .line 415
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->getDestRoi(I)Landroid/graphics/RectF;

    move-result-object v2

    aput-object v2, v1, v0

    .line 414
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getDrawRect(I)Landroid/graphics/RectF;
    .locals 6
    .param p1, "i"    # I

    .prologue
    .line 240
    const/4 v0, 0x0

    .line 241
    .local v0, "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_0

    .line 243
    packed-switch p1, :pswitch_data_0

    .line 253
    :cond_0
    :goto_0
    return-object v0

    .line 245
    :pswitch_0
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x18

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x1c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto :goto_0

    .line 246
    :pswitch_1
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x28

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x2c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto :goto_0

    .line 247
    :pswitch_2
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x30

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x34

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x38

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x3c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto :goto_0

    .line 248
    :pswitch_3
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x44

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x48

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x4c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto/16 :goto_0

    .line 249
    :pswitch_4
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x50

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x54

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x58

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x5c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto/16 :goto_0

    .line 250
    :pswitch_5
    new-instance v0, Landroid/graphics/RectF;

    .end local v0    # "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x60

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x68

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x6c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v0    # "ret":Landroid/graphics/RectF;
    goto/16 :goto_0

    .line 243
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getDrawRectArray()[Landroid/graphics/RectF;
    .locals 3

    .prologue
    .line 401
    const/4 v2, 0x4

    new-array v1, v2, [Landroid/graphics/RectF;

    .line 402
    .local v1, "ret":[Landroid/graphics/RectF;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 404
    return-object v1

    .line 403
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->getDrawRect(I)Landroid/graphics/RectF;

    move-result-object v2

    aput-object v2, v1, v0

    .line 402
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getDrawSrcRect(I)Landroid/graphics/Rect;
    .locals 6
    .param p1, "i"    # I

    .prologue
    .line 256
    const/4 v0, 0x0

    .line 257
    .local v0, "ret":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_0

    .line 259
    packed-switch p1, :pswitch_data_0

    .line 269
    :cond_0
    :goto_0
    return-object v0

    .line 261
    :pswitch_0
    new-instance v0, Landroid/graphics/Rect;

    .end local v0    # "ret":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0xc0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0xc4

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0xc8

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0xcc

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v0    # "ret":Landroid/graphics/Rect;
    goto :goto_0

    .line 262
    :pswitch_1
    new-instance v0, Landroid/graphics/Rect;

    .end local v0    # "ret":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0xd0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0xd4

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0xd8

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0xdc

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v0    # "ret":Landroid/graphics/Rect;
    goto :goto_0

    .line 263
    :pswitch_2
    new-instance v0, Landroid/graphics/Rect;

    .end local v0    # "ret":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0xe0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0xe4

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0xe8

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0xec

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v0    # "ret":Landroid/graphics/Rect;
    goto :goto_0

    .line 264
    :pswitch_3
    new-instance v0, Landroid/graphics/Rect;

    .end local v0    # "ret":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0xf0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0xf4

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0xf8

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0xfc

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v0    # "ret":Landroid/graphics/Rect;
    goto/16 :goto_0

    .line 265
    :pswitch_4
    new-instance v0, Landroid/graphics/Rect;

    .end local v0    # "ret":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x100

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x104

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x108

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x10c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v0    # "ret":Landroid/graphics/Rect;
    goto/16 :goto_0

    .line 266
    :pswitch_5
    new-instance v0, Landroid/graphics/Rect;

    .end local v0    # "ret":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v2, 0x110

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v3, 0x114

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v4, 0x118

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v5, 0x11c

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v0    # "ret":Landroid/graphics/Rect;
    goto/16 :goto_0

    .line 259
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getDrawSrcRectArray()[Landroid/graphics/Rect;
    .locals 3

    .prologue
    .line 407
    const/4 v2, 0x4

    new-array v1, v2, [Landroid/graphics/Rect;

    .line 408
    .local v1, "ret":[Landroid/graphics/Rect;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 410
    return-object v1

    .line 409
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->getDrawSrcRect(I)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v1, v0

    .line 408
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getGridCount()I
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    .line 227
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getStyle()I
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    .line 222
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getTotalHeight()I
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    .line 237
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getTotalWidth()I
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    .line 232
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public makeBufferFromItems(Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo$DetailedInfoCallback;)Ljava/nio/ByteBuffer;
    .locals 11
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo$DetailedInfoCallback;

    .prologue
    .line 441
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->getBufLength()I

    move-result v1

    .line 442
    .local v1, "capacity":I
    const/4 v5, 0x0

    .line 445
    .local v5, "position":I
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 446
    .local v6, "ret":Ljava/nio/ByteBuffer;
    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 447
    const/4 v9, 0x1

    iput v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->index:I

    .line 451
    const-string v9, "makeBufferFromItems start"

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 453
    const/4 v9, 0x0

    invoke-virtual {p0, v9, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->checkPosition(ILjava/nio/ByteBuffer;)Z

    move-result v9

    if-nez v9, :cond_0

    const/4 v6, 0x0

    .line 652
    .end local v6    # "ret":Ljava/nio/ByteBuffer;
    :goto_0
    return-object v6

    .line 454
    .restart local v6    # "ret":Ljava/nio/ByteBuffer;
    :cond_0
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo$DetailedInfoCallback;->getGridType()I

    move-result v9

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 456
    const/4 v9, 0x4

    invoke-virtual {p0, v9, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->checkPosition(ILjava/nio/ByteBuffer;)Z

    move-result v9

    if-nez v9, :cond_1

    const/4 v6, 0x0

    goto :goto_0

    .line 458
    :cond_1
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo$DetailedInfoCallback;->getGridSplitCount()I

    move-result v9

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 460
    const/16 v9, 0x8

    invoke-virtual {p0, v9, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->checkPosition(ILjava/nio/ByteBuffer;)Z

    move-result v9

    if-nez v9, :cond_2

    const/4 v6, 0x0

    goto :goto_0

    .line 461
    :cond_2
    const/16 v9, 0x640

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 463
    const/16 v9, 0xc

    invoke-virtual {p0, v9, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->checkPosition(ILjava/nio/ByteBuffer;)Z

    move-result v9

    if-nez v9, :cond_3

    const/4 v6, 0x0

    goto :goto_0

    .line 464
    :cond_3
    const/16 v9, 0x640

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 468
    const/16 v9, 0x10

    invoke-virtual {p0, v9, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->checkPosition(ILjava/nio/ByteBuffer;)Z

    move-result v9

    if-nez v9, :cond_4

    const/4 v6, 0x0

    goto :goto_0

    .line 469
    :cond_4
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    const/4 v9, 0x6

    if-lt v4, v9, :cond_5

    .line 490
    const/4 v4, 0x0

    :goto_2
    const/4 v9, 0x5

    if-lt v4, v9, :cond_7

    .line 531
    const/16 v9, 0xc0

    invoke-virtual {p0, v9, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->checkPosition(ILjava/nio/ByteBuffer;)Z

    move-result v9

    if-nez v9, :cond_a

    const/4 v6, 0x0

    goto :goto_0

    .line 470
    :cond_5
    invoke-interface {p1, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo$DetailedInfoCallback;->getDrawItemRect(I)Landroid/graphics/RectF;

    move-result-object v2

    .line 471
    .local v2, "drawItemRect":Landroid/graphics/RectF;
    if-nez v2, :cond_6

    .line 473
    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 474
    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 475
    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 476
    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 477
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "makeBuffer  DrawRect "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " -1 -1 -1 -1"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 469
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 482
    :cond_6
    iget v9, v2, Landroid/graphics/RectF;->left:F

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 483
    iget v9, v2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 484
    iget v9, v2, Landroid/graphics/RectF;->right:F

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 485
    iget v9, v2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 487
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "makeBuffer DrawRect "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Landroid/graphics/RectF;->toShortString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_3

    .line 492
    .end local v2    # "drawItemRect":Landroid/graphics/RectF;
    :cond_7
    const/4 v8, 0x0

    .line 493
    .local v8, "style_ctrl_pt":I
    packed-switch v4, :pswitch_data_0

    .line 511
    :goto_4
    invoke-virtual {p0, v8, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->checkPosition(ILjava/nio/ByteBuffer;)Z

    move-result v9

    if-nez v9, :cond_8

    const/4 v6, 0x0

    goto/16 :goto_0

    .line 496
    :pswitch_0
    const/16 v8, 0x70

    .line 497
    goto :goto_4

    .line 499
    :pswitch_1
    const/16 v8, 0x80

    .line 500
    goto :goto_4

    .line 502
    :pswitch_2
    const/16 v8, 0x90

    .line 503
    goto :goto_4

    .line 505
    :pswitch_3
    const/16 v8, 0xa0

    .line 506
    goto :goto_4

    .line 508
    :pswitch_4
    const/16 v8, 0xb0

    goto :goto_4

    .line 512
    :cond_8
    invoke-interface {p1, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo$DetailedInfoCallback;->getDrawItemControlRoi(I)Landroid/graphics/RectF;

    move-result-object v7

    .line 513
    .local v7, "roi":Landroid/graphics/RectF;
    if-nez v7, :cond_9

    .line 514
    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 515
    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 516
    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 517
    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 518
    const-string v9, "makeBuffer ControlPoint1 -1 -1 -1 -1"

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 490
    :goto_5
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 521
    :cond_9
    iget v9, v7, Landroid/graphics/RectF;->left:F

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 522
    iget v9, v7, Landroid/graphics/RectF;->top:F

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 523
    iget v9, v7, Landroid/graphics/RectF;->right:F

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 524
    iget v9, v7, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 525
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "makeBuffer ControlPoint1 "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Landroid/graphics/RectF;->toShortString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_5

    .line 532
    .end local v7    # "roi":Landroid/graphics/RectF;
    .end local v8    # "style_ctrl_pt":I
    :cond_a
    const/4 v4, 0x0

    :goto_6
    const/4 v9, 0x6

    if-lt v4, v9, :cond_b

    .line 555
    const/16 v9, 0x120

    invoke-virtual {p0, v9, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->checkPosition(ILjava/nio/ByteBuffer;)Z

    move-result v9

    if-nez v9, :cond_d

    const/4 v6, 0x0

    goto/16 :goto_0

    .line 533
    :cond_b
    invoke-interface {p1, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo$DetailedInfoCallback;->getDrawSrcRect(I)Landroid/graphics/RectF;

    move-result-object v3

    .line 534
    .local v3, "drawSrcRect":Landroid/graphics/RectF;
    if-nez v3, :cond_c

    .line 536
    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 537
    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 538
    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 539
    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 540
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "makeBuffer DrawSrcRect "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " -1 -1 -1 -1"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 532
    :goto_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 545
    :cond_c
    iget v9, v3, Landroid/graphics/RectF;->left:F

    float-to-int v9, v9

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 546
    iget v9, v3, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 547
    iget v9, v3, Landroid/graphics/RectF;->right:F

    float-to-int v9, v9

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 548
    iget v9, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v9, v9

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 550
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "makeBuffer DrawSrcRect "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Landroid/graphics/RectF;->toShortString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_7

    .line 556
    .end local v3    # "drawSrcRect":Landroid/graphics/RectF;
    :cond_d
    const/4 v4, 0x0

    :goto_8
    const/4 v9, 0x6

    if-lt v4, v9, :cond_e

    .line 576
    const/16 v9, 0x180

    invoke-virtual {p0, v9, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->checkPosition(ILjava/nio/ByteBuffer;)Z

    move-result v9

    if-nez v9, :cond_10

    const/4 v6, 0x0

    goto/16 :goto_0

    .line 557
    :cond_e
    invoke-interface {p1, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo$DetailedInfoCallback;->getDrawSrcRect(I)Landroid/graphics/RectF;

    move-result-object v3

    .line 558
    .restart local v3    # "drawSrcRect":Landroid/graphics/RectF;
    if-nez v3, :cond_f

    .line 560
    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 561
    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 562
    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 563
    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 564
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "makeBuffer DestROI "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " -1 -1 -1 -1"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 556
    :goto_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 568
    :cond_f
    iget v9, v3, Landroid/graphics/RectF;->left:F

    float-to-int v9, v9

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 569
    iget v9, v3, Landroid/graphics/RectF;->top:F

    float-to-int v9, v9

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 570
    iget v9, v3, Landroid/graphics/RectF;->right:F

    float-to-int v9, v9

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 571
    iget v9, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v9, v9

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_9

    .line 577
    .end local v3    # "drawSrcRect":Landroid/graphics/RectF;
    :cond_10
    const/4 v4, 0x0

    :goto_a
    const/4 v9, 0x6

    if-lt v4, v9, :cond_11

    .line 619
    const/16 v9, 0x240

    invoke-virtual {p0, v9, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->checkPosition(ILjava/nio/ByteBuffer;)Z

    move-result v9

    if-nez v9, :cond_12

    const/4 v6, 0x0

    goto/16 :goto_0

    .line 580
    :cond_11
    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 581
    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 582
    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 583
    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 584
    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 585
    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 586
    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 587
    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 577
    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    .line 620
    :cond_12
    const/4 v4, 0x0

    :goto_b
    const/4 v9, 0x6

    if-lt v4, v9, :cond_13

    .line 644
    const/16 v9, 0x270

    invoke-virtual {p0, v9, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->checkPosition(ILjava/nio/ByteBuffer;)Z

    move-result v9

    if-nez v9, :cond_14

    const/4 v6, 0x0

    goto/16 :goto_0

    .line 624
    :cond_13
    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 625
    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 620
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 645
    :cond_14
    const/4 v4, 0x0

    :goto_c
    const/4 v9, 0x6

    if-lt v4, v9, :cond_15

    .line 651
    const-string v9, "makeBufferFromItems end successfully"

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 646
    :cond_15
    invoke-interface {p1, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo$DetailedInfoCallback;->getDrawItemAngel(I)I

    move-result v0

    .line 647
    .local v0, "angle":I
    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 648
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "makeBuffer angle "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 645
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    .line 493
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setByteBuffer(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1, "b"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 209
    if-eqz p1, :cond_0

    .line 211
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 212
    .local v0, "t":[B
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->mBuf:Ljava/nio/ByteBuffer;

    .line 217
    .end local v0    # "t":[B
    :goto_0
    return-void

    .line 215
    :cond_0
    const-string v1, "setByteBuffer Bytebuffer is null!!"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method
