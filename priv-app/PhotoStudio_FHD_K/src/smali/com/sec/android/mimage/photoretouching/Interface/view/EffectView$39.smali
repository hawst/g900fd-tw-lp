.class Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$39;
.super Ljava/lang/Object;
.source "EffectView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->refreshImageAndBottomButtonsAndActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$39;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    .line 2789
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2805
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2792
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$39;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isEffectManagerPressed:Z
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$29(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2793
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$39;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$30(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Z)V

    .line 2794
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$39;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const-class v2, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2795
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$39;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v2, 0x80000

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2797
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2801
    return-void
.end method
