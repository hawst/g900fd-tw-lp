.class Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;
.super Ljava/lang/Thread;
.source "EffectSplashMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->applySplashPreview(Landroid/view/MotionEvent;[I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

.field private final synthetic val$output:[I

.field private final synthetic val$point:Landroid/graphics/Point;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;Landroid/graphics/Point;[I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->val$point:Landroid/graphics/Point;

    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->val$output:[I

    .line 246
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$1(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempOriginal:[I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->val$point:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->val$point:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v4

    .line 250
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$1(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$1(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    move-result-object v8

    iget v8, v8, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mRadius:F

    float-to-int v8, v8

    .line 249
    invoke-static/range {v0 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->drawSplashPreview([IIIII[IIII)I

    .line 252
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    if-lt v9, v0, :cond_0

    .line 265
    return-void

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$1(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    .line 255
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    mul-int/2addr v1, v9

    .line 256
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 254
    invoke-static {v0, v1, v2}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v10

    .line 257
    .local v10, "inputBuffer":Ljava/nio/IntBuffer;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->val$output:[I

    .line 258
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v1

    add-int/2addr v1, v9

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v2

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v2

    add-int/2addr v1, v2

    .line 259
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 257
    invoke-static {v0, v1, v2}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v11

    .line 260
    .local v11, "outputBuffer":Ljava/nio/IntBuffer;
    invoke-virtual {v11, v10}, Ljava/nio/IntBuffer;->put(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;

    .line 261
    const/4 v10, 0x0

    .line 252
    add-int/lit8 v9, v9, 0x1

    goto :goto_0
.end method
