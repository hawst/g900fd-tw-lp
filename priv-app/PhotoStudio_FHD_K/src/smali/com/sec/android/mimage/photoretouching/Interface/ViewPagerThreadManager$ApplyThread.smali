.class Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$ApplyThread;
.super Ljava/lang/Object;
.source "ViewPagerThreadManager.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ApplyThread"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$ApplyThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$ApplyThread;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$ApplyThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic run(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$ApplyThread;->run(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public run(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;)Ljava/lang/Void;
    .locals 4
    .param p1, "jc"    # Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;

    .prologue
    const/4 v3, 0x0

    .line 79
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$ApplyThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;Z)V

    .line 80
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$ApplyThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mIndexList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$ApplyThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;Z)V

    .line 86
    const/4 v1, 0x0

    return-object v1

    .line 82
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$ApplyThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mIndexList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 83
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$ApplyThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->applyData(I)V
    invoke-static {v1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;I)V

    goto :goto_0
.end method
