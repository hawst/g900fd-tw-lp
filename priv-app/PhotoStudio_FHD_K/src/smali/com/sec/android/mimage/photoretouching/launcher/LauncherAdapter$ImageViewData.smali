.class public Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
.super Ljava/lang/Object;
.source "LauncherAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ImageViewData"
.end annotation


# instance fields
.field public backgroundView:Landroid/widget/ImageView;

.field public frameLayout:Landroid/widget/FrameLayout;

.field public frameView:Landroid/widget/ImageView;

.field public imageView:Landroid/widget/ImageView;

.field public isFlagData:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

.field public uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/net/Uri;Landroid/widget/ImageView;Landroid/widget/FrameLayout;)V
    .locals 2
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "fv"    # Landroid/widget/ImageView;
    .param p4, "uri"    # Landroid/net/Uri;
    .param p5, "bv"    # Landroid/widget/ImageView;
    .param p6, "fl"    # Landroid/widget/FrameLayout;

    .prologue
    const/4 v1, 0x0

    .line 653
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 646
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    .line 647
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameView:Landroid/widget/ImageView;

    .line 648
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    .line 649
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    .line 650
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->isFlagData:Z

    .line 651
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->uri:Landroid/net/Uri;

    .line 654
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameView:Landroid/widget/ImageView;

    .line 655
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    .line 656
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    .line 657
    iput-object p6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    .line 658
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->uri:Landroid/net/Uri;

    .line 659
    return-void
.end method
