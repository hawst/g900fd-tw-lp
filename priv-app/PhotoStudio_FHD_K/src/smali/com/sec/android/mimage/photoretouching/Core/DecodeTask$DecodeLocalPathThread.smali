.class Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;
.super Ljava/lang/Thread;
.source "DecodeTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DecodeLocalPathThread"
.end annotation


# instance fields
.field mQuramOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

.field optsResolution:Landroid/graphics/BitmapFactory$Options;

.field path:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/BitmapFactory$Options;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Ljava/lang/String;I)V
    .locals 1
    .param p2, "opts"    # Landroid/graphics/BitmapFactory$Options;
    .param p3, "quramoptions"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p4, "str"    # Ljava/lang/String;
    .param p5, "mode"    # I

    .prologue
    const/4 v0, 0x0

    .line 634
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    .line 633
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 630
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->mQuramOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .line 631
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    .line 632
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->path:Ljava/lang/String;

    .line 635
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    .line 636
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->path:Ljava/lang/String;

    .line 637
    invoke-static {p1, p5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$0(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;I)V

    .line 638
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->mQuramOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .line 639
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 643
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 644
    .local v1, "time":Ljava/lang/Long;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeMode:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$1(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    .line 648
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mSkiaMode:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$2(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 650
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->path:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$3(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 663
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_2

    .line 665
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v2, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$7(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    .line 666
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    .line 674
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bigheadk, DecodePath mBitmap = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 675
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$8(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc2:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$9(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 677
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # invokes: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->makeImageHandlerAboutLocalPath()V
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$15(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)V

    .line 698
    :cond_0
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIdx:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$14(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]decode bitmap time:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 699
    return-void

    .line 654
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->path:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->mQuramOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-static {v3, v4}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeFile(Ljava/lang/String;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$3(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 657
    :catch_0
    move-exception v0

    .line 659
    .local v0, "e":Ljava/lang/NullPointerException;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v2, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$7(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    .line 660
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 661
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    goto/16 :goto_0

    .line 670
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v2, v6}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$7(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    goto :goto_1

    .line 684
    :cond_3
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->path:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    .line 692
    :goto_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v2, v6}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$11(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    .line 693
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$8(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc1:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$12(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 694
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    iget v3, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    iget v4, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    # invokes: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->preDecodingAboutLocalPath(II)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$16(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;II)V

    goto :goto_2

    .line 686
    :catch_1
    move-exception v0

    .line 688
    .restart local v0    # "e":Ljava/lang/NullPointerException;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v2, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$11(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    .line 689
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 690
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    goto :goto_3
.end method
