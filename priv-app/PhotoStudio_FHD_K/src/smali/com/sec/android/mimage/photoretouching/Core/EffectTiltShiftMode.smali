.class public Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;
.super Lcom/sec/android/mimage/photoretouching/Core/EffectInfoInterface;
.source "EffectTiltShiftMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;,
        Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;,
        Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mPaint:Landroid/graphics/Paint;

.field private mStartInitializing:Z

.field private mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

.field private mutexOn:Z

.field private myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

.field private myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onCallback"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    .param p3, "onActionbarCallback"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;
    .param p4, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfoInterface;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 533
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mutexOn:Z

    .line 534
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mStartInitializing:Z

    .line 536
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mPaint:Landroid/graphics/Paint;

    .line 537
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mContext:Landroid/content/Context;

    .line 538
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    .line 539
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    .line 541
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    .line 543
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 23
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    .line 24
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    .line 25
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 26
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mContext:Landroid/content/Context;

    .line 27
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mPaint:Landroid/graphics/Paint;

    .line 28
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 29
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;Z)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->changeTiltShiftBuffer(Z)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method private applyTiltShiftPreview(Landroid/view/MotionEvent;)V
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 246
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 247
    .local v2, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 248
    .local v3, "y":F
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-boolean v4, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isConfigurationChange:Z

    if-eqz v4, :cond_1

    .line 342
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v4

    float-to-int v5, v2

    float-to-int v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 252
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v4, v4, 0xff

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 255
    :pswitch_1
    const-string v4, "Tilt event down"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 256
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iput v8, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSTouchState:I

    .line 257
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v5

    int-to-float v5, v5

    sub-float v5, v3, v5

    float-to-int v5, v5

    int-to-float v5, v5

    iput v5, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSCenter:F

    .line 258
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSCenter:F

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v6

    div-float/2addr v5, v6

    iput v5, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSOrgCenter:F

    .line 259
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iput-boolean v7, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isTiltFirstTouch:Z

    .line 261
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iput-boolean v7, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->showOriginal:Z

    .line 262
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltEventThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltEventThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->isAlive()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 265
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltEventThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->interrupt()V

    .line 268
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    new-instance v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)V

    iput-object v5, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltEventThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;

    .line 269
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltEventThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->start()V

    .line 270
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->invalidate()V

    goto :goto_0

    .line 274
    :pswitch_2
    const-string v4, "Tilt event ACTION_POINTER_DOWN"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 275
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iput v7, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSTouchState:I

    .line 276
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->invalidate()V

    goto/16 :goto_0

    .line 279
    :pswitch_3
    const-string v4, "Tilt event ACTION_POINTER_UP"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 280
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    const/4 v5, 0x2

    iput v5, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSTouchState:I

    .line 281
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v4, :cond_0

    .line 283
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    invoke-interface {v4, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    goto/16 :goto_0

    .line 287
    :pswitch_4
    const-string v4, "Tilt event ACTION_UP"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 288
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iput v9, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSTouchState:I

    .line 291
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltEventThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;

    if-eqz v4, :cond_3

    .line 292
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltEventThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->workResume()V

    .line 293
    :cond_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->invalidateWithThread()V

    .line 294
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v4, :cond_0

    .line 296
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    invoke-interface {v4, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    goto/16 :goto_0

    .line 300
    :pswitch_5
    const-string v4, "Tilt event ACTION_MOVE"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 301
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v4, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSTouchState:I

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v4, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSTouchState:I

    if-ne v4, v10, :cond_6

    .line 302
    :cond_4
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iput v10, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSTouchState:I

    .line 303
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v5

    int-to-float v5, v5

    sub-float v5, v3, v5

    float-to-int v5, v5

    int-to-float v5, v5

    iput v5, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSCenter:F

    .line 304
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSCenter:F

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v6

    div-float/2addr v5, v6

    iput v5, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSOrgCenter:F

    .line 317
    :cond_5
    :goto_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->invalidate()V

    goto/16 :goto_0

    .line 306
    :cond_6
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v4, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSTouchState:I

    if-ne v4, v7, :cond_5

    .line 307
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    invoke-virtual {v4, p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->spacingY(Landroid/view/MotionEvent;)F

    move-result v4

    const/high16 v5, -0x40800000    # -1.0f

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_5

    .line 308
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    invoke-virtual {v4, p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->spacingY(Landroid/view/MotionEvent;)F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float v1, v4, v5

    .line 309
    .local v1, "newDist":F
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    invoke-virtual {v4, p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->center(Landroid/view/MotionEvent;)F

    move-result v0

    .line 311
    .local v0, "newCenter":F
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    float-to-int v5, v0

    int-to-float v5, v5

    iput v5, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSCenter:F

    .line 312
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    float-to-int v5, v1

    int-to-float v5, v5

    iput v5, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSRange:F

    .line 313
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSCenter:F

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v6

    div-float/2addr v5, v6

    iput v5, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSOrgCenter:F

    .line 314
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSRange:F

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v6

    div-float/2addr v5, v6

    iput v5, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSOrgRange:F

    goto :goto_1

    .line 323
    .end local v0    # "newCenter":F
    .end local v1    # "newDist":F
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v4, v4, 0xff

    packed-switch v4, :pswitch_data_1

    :pswitch_6
    goto/16 :goto_0

    .line 328
    :pswitch_7
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iput v9, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSTouchState:I

    .line 329
    invoke-direct {p0, v8}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->changeTiltShiftBuffer(Z)V

    .line 330
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->invalidateWithThread()V

    .line 331
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v4, :cond_8

    .line 333
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    invoke-interface {v4, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    .line 335
    :cond_8
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->invalidateWithThread()V

    goto/16 :goto_0

    .line 252
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 323
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private changeTiltShiftBuffer(Z)V
    .locals 10
    .param p1, "showOriginal"    # Z

    .prologue
    const/4 v4, 0x0

    .line 75
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v9

    .line 76
    .local v9, "viewBuffer":[I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iput-boolean p1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->showOriginal:Z

    .line 77
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->showOriginal:Z

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewIn:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewIn:[I

    .line 83
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    .line 85
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    mul-int/2addr v2, v3

    .line 81
    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 101
    :cond_0
    :goto_0
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    if-lt v7, v0, :cond_2

    .line 113
    const-string v0, "changeTiltShiftBuffer"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 114
    return-void

    .line 90
    .end local v7    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewIn:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewIn:[I

    .line 93
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    .line 94
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 95
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 96
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v4, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSCenter:F

    float-to-int v4, v4

    .line 97
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSRange:F

    float-to-int v5, v5

    .line 92
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyTiltShift([I[IIIII)V

    goto :goto_0

    .line 103
    .restart local v7    # "i":I
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewIn:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    if-eqz v0, :cond_3

    .line 105
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_3

    .line 107
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v0

    add-int/2addr v0, v7

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    mul-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    invoke-static {v9, v0, v1}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v6

    .line 108
    .local v6, "dst":Ljava/nio/IntBuffer;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    mul-int/2addr v1, v7

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    invoke-static {v0, v1, v2}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v8

    .line 109
    .local v8, "src":Ljava/nio/IntBuffer;
    invoke-virtual {v6, v8}, Ljava/nio/IntBuffer;->put(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;

    .line 101
    .end local v6    # "dst":Ljava/nio/IntBuffer;
    .end local v8    # "src":Ljava/nio/IntBuffer;
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1
.end method

.method private drawTiltShift(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->showOriginal:Z

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v6

    .line 149
    .local v6, "x_start":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSCenter:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v1, v1, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSRange:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v1

    add-int v7, v0, v1

    .line 151
    .local v7, "y_start":I
    int-to-float v1, v6

    .line 152
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v0

    int-to-float v2, v0

    .line 153
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    add-int/2addr v0, v6

    int-to-float v3, v0

    .line 154
    int-to-float v4, v7

    .line 155
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    .line 151
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 156
    int-to-float v1, v6

    .line 157
    int-to-float v0, v7

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v2, v2, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSRange:F

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    add-float/2addr v2, v0

    .line 158
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    add-int/2addr v0, v6

    int-to-float v3, v0

    .line 159
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v0

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    add-int/2addr v0, v4

    int-to-float v4, v0

    .line 160
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    .line 156
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 162
    .end local v6    # "x_start":I
    .end local v7    # "y_start":I
    :cond_0
    return-void
.end method

.method private initTiltShift()V
    .locals 8

    .prologue
    .line 33
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v4

    .line 35
    .local v4, "viewBuffer":[I
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    .line 36
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    mul-int/2addr v6, v7

    new-array v6, v6, [I

    iput-object v6, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewIn:[I

    .line 37
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    mul-int/2addr v6, v7

    new-array v6, v6, [I

    iput-object v6, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    .line 39
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    if-lt v0, v5, :cond_0

    .line 58
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    iput v6, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSCenter:F

    .line 59
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x6

    int-to-float v6, v6

    iput v6, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSRange:F

    .line 61
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v6, v6, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSCenter:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v7

    div-float/2addr v6, v7

    iput v6, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSOrgCenter:F

    .line 62
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v6, v6, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSRange:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v7

    div-float/2addr v6, v7

    iput v6, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSOrgRange:F

    .line 64
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->startTiltInitThread()V

    .line 65
    return-void

    .line 42
    :cond_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v5

    add-int/2addr v5, v0

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v6

    mul-int/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v6

    add-int/2addr v5, v6

    .line 43
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 41
    invoke-static {v4, v5, v6}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v1

    .line 44
    .local v1, "inputBuffer":Ljava/nio/IntBuffer;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewIn:[I

    .line 45
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    mul-int/2addr v6, v0

    .line 46
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    .line 44
    invoke-static {v5, v6, v7}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v2

    .line 47
    .local v2, "outputBuffer":Ljava/nio/IntBuffer;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    .line 48
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    mul-int/2addr v6, v0

    .line 49
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    .line 47
    invoke-static {v5, v6, v7}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v3

    .line 51
    .local v3, "outputBuffer1":Ljava/nio/IntBuffer;
    invoke-virtual {v2, v1}, Ljava/nio/IntBuffer;->put(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;

    .line 52
    invoke-virtual {v3, v1}, Ljava/nio/IntBuffer;->put(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;

    .line 54
    const/4 v1, 0x0

    .line 55
    const/4 v2, 0x0

    .line 39
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method

.method private startTiltInitThread()V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltInitThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;

    .line 70
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltInitThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->start()V

    .line 71
    return-void
.end method


# virtual methods
.method public applyOriginal()[I
    .locals 1

    .prologue
    .line 347
    const/4 v0, 0x0

    return-object v0
.end method

.method public applyPreview()I
    .locals 1

    .prologue
    .line 362
    const/4 v0, -0x1

    return v0
.end method

.method public configurationChange()V
    .locals 15

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v2

    .line 167
    .local v2, "w":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    .line 169
    .local v3, "h":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 170
    .local v4, "pw":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    .line 171
    .local v5, "ph":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    if-eqz v0, :cond_4

    .line 172
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isConfigurationChange:Z

    .line 173
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->showOriginal:Z

    .line 174
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltInitThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltInitThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->workStop()V

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltEventThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltEventThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->interrupt()V

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewIn:[I

    if-eqz v0, :cond_2

    .line 181
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewIn:[I

    .line 182
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    if-eqz v0, :cond_3

    .line 183
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    .line 185
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    mul-int/2addr v1, v6

    new-array v1, v1, [I

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewIn:[I

    .line 186
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    mul-int/2addr v1, v6

    new-array v1, v1, [I

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    .line 188
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewIn:[I

    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->resizeBuff([I[IIIII)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v14

    .line 192
    .local v14, "viewBuffer":[I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v1, v1, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSOrgCenter:F

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v6

    mul-float/2addr v1, v6

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSCenter:F

    .line 193
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v1, v1, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSOrgRange:F

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v6

    mul-float/2addr v1, v6

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSRange:F

    .line 195
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewIn:[I

    .line 196
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    .line 197
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v8

    .line 198
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v9

    .line 199
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSCenter:F

    float-to-int v10, v0

    .line 200
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSRange:F

    float-to-int v11, v0

    .line 195
    invoke-static/range {v6 .. v11}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyTiltShift([I[IIIII)V

    .line 202
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    if-lt v12, v0, :cond_5

    .line 209
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isConfigurationChange:Z

    .line 211
    .end local v12    # "i":I
    .end local v14    # "viewBuffer":[I
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->afterApplyPreview()V

    .line 212
    return-void

    .line 204
    .restart local v12    # "i":I
    .restart local v14    # "viewBuffer":[I
    :cond_5
    const/4 v13, 0x0

    .local v13, "j":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    if-lt v13, v0, :cond_6

    .line 202
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 206
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v0

    add-int/2addr v0, v12

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    mul-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v1

    add-int/2addr v1, v13

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    mul-int/2addr v6, v12

    add-int/2addr v6, v13

    aget v1, v1, v6

    aput v1, v14, v0

    .line 204
    add-int/lit8 v13, v13, 0x1

    goto :goto_1
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 217
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isConfigurationChange:Z

    .line 218
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isTiltFirstTouch:Z

    .line 219
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltInitThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltInitThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->workStop()V

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->destroy()V

    .line 223
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    .line 225
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 226
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->drawTiltShift(Landroid/graphics/Canvas;)V

    .line 144
    return-void
.end method

.method public getMutexOn()Z
    .locals 1

    .prologue
    .line 357
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mutexOn:Z

    return v0
.end method

.method public init(I)V
    .locals 4
    .param p1, "effectType"    # I

    .prologue
    const/4 v3, 0x0

    .line 118
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mStartInitializing:Z

    .line 120
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    invoke-interface {v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    .line 124
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 125
    .local v0, "viewBuffer":[I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 127
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->initTiltShift()V

    .line 129
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$1;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 137
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mStartInitializing:Z

    .line 138
    return-void
.end method

.method public isStartInitializing()Z
    .locals 1

    .prologue
    .line 367
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mStartInitializing:Z

    return v0
.end method

.method public setStep(I)V
    .locals 0
    .param p1, "step"    # I

    .prologue
    .line 353
    return-void
.end method

.method public touch(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 231
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v2, :cond_0

    .line 232
    const/4 v2, 0x0

    .line 241
    :goto_0
    return v2

    .line 234
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 235
    .local v1, "m":Landroid/graphics/Matrix;
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 236
    .local v0, "inversM":Landroid/graphics/Matrix;
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 237
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 239
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->applyTiltShiftPreview(Landroid/view/MotionEvent;)V

    .line 241
    const/4 v2, 0x1

    goto :goto_0
.end method
