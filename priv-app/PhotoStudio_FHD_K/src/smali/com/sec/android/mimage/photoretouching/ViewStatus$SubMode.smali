.class public Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;
.super Ljava/lang/Object;
.source "ViewStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/ViewStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SubMode"
.end annotation


# static fields
.field public static final ADJUSTMENT_BUTTON:I = 0x1000100a

.field public static final AIRBRUSHFACE_BUTTON:I = 0x17001702

.field public static final BLUEWASH_BUTTON:I = 0x1600160b

.field public static final BLUE_BUTTON:I = 0x15001609

.field public static final BRIGHTNESS_BUTTON:I = 0x15001500

.field public static final BRUSH_SELECT_DIALOG_BUTTON:I = 0x14001403

.field public static final BTN_EXPAND:I = 0x1

.field public static final BTN_NORMAL:I = 0x0

.field public static final COLLAGE_BACKGROUND_01:I = 0x1e400000

.field public static final COLLAGE_BACKGROUND_02:I = 0x1e400001

.field public static final COLLAGE_BACKGROUND_03:I = 0x1e400002

.field public static final COLLAGE_BACKGROUND_04:I = 0x1e400003

.field public static final COLLAGE_BACKGROUND_05:I = 0x1e400004

.field public static final COLLAGE_BACKGROUND_06:I = 0x1e400005

.field public static final COLLAGE_BACKGROUND_07:I = 0x1e400006

.field public static final COLLAGE_BACKGROUND_08:I = 0x1e400007

.field public static final COLLAGE_BACKGROUND_09:I = 0x1e400008

.field public static final COLLAGE_BACKGROUND_10:I = 0x1e400009

.field public static final COLLAGE_BACKGROUND_11:I = 0x1e40000a

.field public static final COLLAGE_BACKGROUND_12:I = 0x1e40000b

.field public static final COLLAGE_BACKGROUND_13:I = 0x1e40000c

.field public static final COLLAGE_BACKGROUND_14:I = 0x1e40000d

.field public static final COLLAGE_BACKGROUND_15:I = 0x1e40000e

.field public static final COLLAGE_BACKGROUND_16:I = 0x1e40000f

.field public static final COLLAGE_BACKGROUND_17:I = 0x1e400010

.field public static final COLLAGE_BACKGROUND_18:I = 0x1e400011

.field public static final COLLAGE_BACKGROUND_19:I = 0x1e400012

.field public static final COLLAGE_BACKGROUND_20:I = 0x1e400013

.field public static final COLLAGE_BACKGROUND_21:I = 0x1e400014

.field public static final COLLAGE_BACKGROUND_22:I = 0x1e400015

.field public static final COLLAGE_BACKGROUND_23:I = 0x1e400016

.field public static final COLLAGE_BACKGROUND_24:I = 0x1e400017

.field public static final COLLAGE_BACKGROUND_25:I = 0x1e400018

.field public static final COLLAGE_BACKGROUND_26:I = 0x1e400019

.field public static final COLLAGE_BACKGROUND_27:I = 0x1e40001a

.field public static final COLLAGE_BACKGROUND_28:I = 0x1e40001b

.field public static final COLLAGE_BACKGROUND_29:I = 0x1e40001c

.field public static final COLLAGE_BACKGROUND_30:I = 0x1e40001d

.field public static final COLLAGE_BOTTOM_01_PROPORTIONS:I = 0x1e300000

.field public static final COLLAGE_BOTTOM_02_LAYOUT:I = 0x1e300001

.field public static final COLLAGE_BOTTOM_03_BORDER:I = 0x1e300002

.field public static final COLLAGE_BOTTOM_04_BACKGROUND:I = 0x1e300003

.field public static final COLLAGE_PILE_A:I = 0x1e200100

.field public static final COLLAGE_PILE_B:I = 0x1e200101

.field public static final COLLAGE_PILE_C:I = 0x1e200102

.field public static final COLLAGE_PILE_D:I = 0x1e200103

.field public static final COLLAGE_PROPORTION_01:I = 0x1e500000

.field public static final COLLAGE_PROPORTION_04:I = 0x1e500001

.field public static final COLLAGE_STYLE_00:I = 0x1e200000

.field public static final COLLAGE_STYLE_01:I = 0x1e200001

.field public static final COLLAGE_STYLE_02:I = 0x1e200002

.field public static final COLLAGE_STYLE_03:I = 0x1e200003

.field public static final COLLAGE_STYLE_04:I = 0x1e200004

.field public static final COLLAGE_STYLE_05:I = 0x1e200005

.field public static final COLLAGE_STYLE_06:I = 0x1e200006

.field public static final COLLAGE_STYLE_07:I = 0x1e200007

.field public static final COLLAGE_STYLE_08:I = 0x1e200008

.field public static final COLLAGE_STYLE_09:I = 0x1e200009

.field public static final COLLAGE_STYLE_10:I = 0x1e20000a

.field public static final COLLAGE_STYLE_11:I = 0x1e20000b

.field public static final COLLAGE_STYLE_12:I = 0x1e20000c

.field public static final COLLAGE_STYLE_13:I = 0x1e20000d

.field public static final COLLAGE_STYLE_14:I = 0x1e20000e

.field public static final COLOR_BUTTON:I = 0x10001004

.field public static final COMIC_BUTTON:I = 0x16001610

.field public static final CONTRAST_BUTTON:I = 0x15001501

.field public static final COPY_TO_ANOTHER_DIALOG_BUTTON:I = 0x1d001702

.field public static final COPY_TO_ORIGIN_DIALOG_BUTTON:I = 0x1d001701

.field public static final COPY_TO_RETURN_BUTTON:I = 0x1d001703

.field public static final CROP_AUTOREFRAME:I = 0x11201307

.field public static final CROP_BUTTON:I = 0x10001003

.field public static final CROP_DYNAMIC_RATIO:I = 0x11201306

.field public static final CROP_FOURBYTHREE:I = 0x11201303

.field public static final CROP_LASSO:I = 0x11201305

.field public static final CROP_ONEBYONE:I = 0x11201302

.field public static final CROP_RETURN_BUTTON:I = 0x11201301

.field public static final CROP_SIXTEENBYNINE:I = 0x11201304

.field public static final DAWNCAST_BUTTON:I = 0x16001616

.field public static final DAWNCAST_BUTTON_2:I = 0x16001617

.field public static final DECORATION_BUTTON:I = 0x1000100b

.field public static final DOWNLIGHT_BUTTON:I = 0x1600160a

.field public static final EFFECT_BUTTON:I = 0x10001005

.field public static final EFFECT_DOWNLOAD:I = 0x160016ff

.field public static final EFFECT_MANAGER_BUTTON:I = 0x16001630

.field public static final ENHANCE_BUTTON:I = 0x15001506

.field public static final EXPOSURE_BUTTON:I = 0x15001503

.field public static final FACEBRIGHTNESS_BUTTON:I = 0x17001701

.field public static final FADEDCOLOURS_BUTTON:I = 0x16001603

.field public static final FRAME00_BUTTON:I = 0x3120006c

.field public static final FRAME01_BUTTON:I = 0x3120006e

.field public static final FRAME02_BUTTON:I = 0x3120006f

.field public static final FRAME03_BUTTON:I = 0x31200070

.field public static final FRAME04_BUTTON:I = 0x31200071

.field public static final FRAME05_BUTTON:I = 0x31200072

.field public static final FRAME06_BUTTON:I = 0x31200073

.field public static final FRAME07_BUTTON:I = 0x31200074

.field public static final FRAME08_BUTTON:I = 0x31200075

.field public static final FRAME09_BUTTON:I = 0x31200076

.field public static final FRAME10_BUTTON:I = 0x31200077

.field public static final FRAME11_BUTTON:I = 0x31200078

.field public static final FRAME12_BUTTON:I = 0x31200079

.field public static final FRAME13_BUTTON:I = 0x3120007a

.field public static final FRAME14_BUTTON:I = 0x3120007b

.field public static final FRAME15_BUTTON:I = 0x3120007c

.field public static final FRAME16_BUTTON:I = 0x3120007d

.field public static final FRAME17_BUTTON:I = 0x3120007e

.field public static final FRAME18_BUTTON:I = 0x3120007f

.field public static final FRAME19_BUTTON:I = 0x31200080

.field public static final FRAME20_BUTTON:I = 0x31200081

.field public static final FRAME21_BUTTON:I = 0x31200082

.field public static final FRAME22_BUTTON:I = 0x31200083

.field public static final FRAME23_BUTTON:I = 0x31200084

.field public static final FRAME24_BUTTON:I = 0x31200085

.field public static final FRAME25_BUTTON:I = 0x31200086

.field public static final FRAME26_BUTTON:I = 0x31200087

.field public static final FRAME27_BUTTON:I = 0x31200088

.field public static final FRAME28_BUTTON:I = 0x31200089

.field public static final FRAME29_BUTTON:I = 0x3120008a

.field public static final FRAME_BUTTON:I = 0x10001007

.field public static final FRAME_RETURN_BUTTON:I = 0x31200000

.field public static final GOTHICNOIR_BUTTON:I = 0x16001619

.field public static final GREEN_BUTTON:I = 0x15001608

.field public static final GREYSCALE_BUTTON:I = 0x16001604

.field public static final HUE_BUTTON:I = 0x15001504

.field public static final IMAGE_STICKER_BUTTON:I = 0x1000100f

.field public static final IMPRESSIONIST_BUTTON:I = 0x1600160f

.field public static final IMPRESSIONIST_BUTTON_2:I = 0x16001614

.field public static final LABEL_BUTTON:I = 0x1000100d

.field public static final LASSO_SELECT_DIALOG_BUTTON:I = 0x14001404

.field public static final LIGHT_FLARE_BUTTON:I = 0x16001608

.field public static final LIGHT_STREAK_BUTTON:I = 0x16001609

.field public static final MAGICPEN_BUTTON:I = 0x1600161a

.field public static final MAGNETIC_SELECT_DIALOG_BUTTON:I = 0x14001401

.field public static final MOSAIC_BUTTON:I = 0x1600161d

.field public static final MULTIGRID_SPLIT_2:I = 0x1e001841

.field public static final MULTIGRID_SPLIT_3:I = 0x1e001842

.field public static final MULTIGRID_SPLIT_4:I = 0x1e001843

.field public static final MULTIGRID_SPLIT_5:I = 0x1e001844

.field public static final MULTIGRID_SPLIT_6:I = 0x1e001845

.field public static final MULTIGRID_SPLIT_7:I = 0x1e001846

.field public static final NEGATIVE_BUTTON:I = 0x16001611

.field public static final NONE:I = 0x1

.field public static final NOSTALGIA_BUTTON:I = 0x16001602

.field public static final OUTOFFOCUS_BUTTON:I = 0x17001703

.field public static final PEN_BUTTON:I = 0x10001009

.field public static final PEN_ERASER_BUTTON:I = 0x1a001843

.field public static final PEN_PEN_BUTTON:I = 0x1a001842

.field public static final PEN_REDO:I = 0x1a001845

.field public static final PEN_RETURN_BUTTON:I = 0x1a001841

.field public static final PEN_UNDO:I = 0x1a001844

.field public static final POP_ART_BUTTON:I = 0x16001613

.field public static final PORTRAIT_BUTTON:I = 0x10001006

.field public static final POSTERISE_BUTTON:I = 0x16001618

.field public static final RAINBOW_BUTTON:I = 0x16001606

.field public static final REDEYEFIX_BUTTON:I = 0x17001700

.field public static final RED_BUTTON:I = 0x15001607

.field public static final RESIZE_10:I = 0x11301201

.field public static final RESIZE_100:I = 0x11301205

.field public static final RESIZE_25:I = 0x11301202

.field public static final RESIZE_50:I = 0x11301203

.field public static final RESIZE_75:I = 0x11301204

.field public static final RESIZE_BUTTON:I = 0x10001001

.field public static final RESIZE_FREE:I = 0x11301206

.field public static final ROTATE_BUTTON:I = 0x10001002

.field public static final ROTATE_FLIP_HORIZONTAL:I = 0x11101103

.field public static final ROTATE_FLIP_VERTICAL:I = 0x11101104

.field public static final ROTATE_MIRROR_BUTTON:I = 0x11001108

.field public static final ROTATE_MIRROR_UNDO:I = 0x11101107

.field public static final ROTATE_RETURN_BUTTON:I = 0x11101108

.field public static final ROTATE_STRAIGHTEN_MOVE:I = 0x11101105

.field public static final ROTATE_STRAIGHTEN_UP:I = 0x11101106

.field public static final ROTATE_TURN_LEFT:I = 0x11101101

.field public static final ROTATE_TURN_RIGHT:I = 0x11101102

.field public static final ROUND_SELECT_DIALOG_BUTTON:I = 0x14001405

.field public static final SATURATION_BUTTON:I = 0x15001502

.field public static final SELECT_ADD:I = 0x14001406

.field public static final SELECT_ALL:I = 0x1400140a

.field public static final SELECT_AREA:I = 0x1400140b

.field public static final SELECT_INVERSE:I = 0x14001409

.field public static final SELECT_MODE:I = 0x1400140d

.field public static final SELECT_NEW:I = 0x14001407

.field public static final SELECT_RETURN_BUTTON:I = 0x1400140e

.field public static final SELECT_SIZE:I = 0x1400140c

.field public static final SELECT_SUB:I = 0x14001408

.field public static final SEPIA_BUTTON:I = 0x16001605

.field public static final SHARPEN_BUTTON:I = 0x1600160c

.field public static final SKETCH_ART_BUTTON:I = 0x16001612

.field public static final SOFTGLOW_BUTTON:I = 0x1600160d

.field public static final SPHERICITY_BUTTON:I = 0x1600161b

.field public static final SQUARE_SELECT_DIALOG_BUTTON:I = 0x14001402

.field public static final STAMP01_BUTTON:I = 0x3150008b

.field public static final STAMP02_BUTTON:I = 0x3150008c

.field public static final STAMP03_BUTTON:I = 0x3150008d

.field public static final STAMP04_BUTTON:I = 0x3150008e

.field public static final STAMP05_BUTTON:I = 0x3150008f

.field public static final STAMP06_BUTTON:I = 0x31500090

.field public static final STAMP07_BUTTON:I = 0x31500091

.field public static final STAMP08_BUTTON:I = 0x31500092

.field public static final STAMP09_BUTTON:I = 0x31500093

.field public static final STAMP10_BUTTON:I = 0x31500094

.field public static final STAMP11_BUTTON:I = 0x31500095

.field public static final STAMP12_BUTTON:I = 0x31500096

.field public static final STAMP13_BUTTON:I = 0x31500097

.field public static final STAMP14_BUTTON:I = 0x31500098

.field public static final STAMP15_BUTTON:I = 0x31500099

.field public static final STAMP16_BUTTON:I = 0x3150009a

.field public static final STAMP17_BUTTON:I = 0x3150009b

.field public static final STAMP18_BUTTON:I = 0x3150009c

.field public static final STAMP19_BUTTON:I = 0x3150009d

.field public static final STAMP20_BUTTON:I = 0x3150009e

.field public static final STAMP21_BUTTON:I = 0x3150009f

.field public static final STAMP22_BUTTON:I = 0x315000a0

.field public static final STAMP23_BUTTON:I = 0x315000a1

.field public static final STAMP24_BUTTON:I = 0x315000a2

.field public static final STAMP25_BUTTON:I = 0x315000a3

.field public static final STAMP26_BUTTON:I = 0x315000a4

.field public static final STAMP27_BUTTON:I = 0x315000a5

.field public static final STAMP28_BUTTON:I = 0x315000a6

.field public static final STAMP29_BUTTON:I = 0x315000a7

.field public static final STAMP30_BUTTON:I = 0x315000a8

.field public static final STAMP31_BUTTON:I = 0x315000a9

.field public static final STAMP32_BUTTON:I = 0x315000aa

.field public static final STAMP33_BUTTON:I = 0x315000ab

.field public static final STAMP34_BUTTON:I = 0x315000ac

.field public static final STAMP_BUTTON:I = 0x1000100c

.field public static final STARDUST_BUTTON:I = 0x16001607

.field public static final STICKER_BRUSH_BUTTON:I = 0x19001822

.field public static final STICKER_BUTTON:I = 0x10001008

.field public static final STICKER_COMIC_BUTTON:I = 0x19001823

.field public static final STICKER_ICON_BUTTON:I = 0x19001825

.field public static final STICKER_PAPER_BUTTON:I = 0x19001824

.field public static final STICKER_RETURN_BUTTON:I = 0x19001821

.field public static final TEMPERATURE_BUTTON:I = 0x15001505

.field public static final TURQUOISE_BUTTON:I = 0x1600160e

.field public static final TWIRL_BUTTON:I = 0x1600161c

.field public static final VIGNETTE_BUTTON:I = 0x16001600

.field public static final VINTAGE_BUTTON:I = 0x16001601

.field public static final WATERMARK_BUTTON:I = 0x1000100e

.field public static final YELLOWGLOW_BUTTON:I = 0x16001615

.field private static mCurrentSubMode:I

.field private static mPreviousSubMode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getIconId()I
    .locals 1

    .prologue
    .line 471
    sget v0, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->mCurrentSubMode:I

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getIconId(I)I

    move-result v0

    return v0
.end method

.method public static getIconId(I)I
    .locals 5
    .param p0, "subMode"    # I

    .prologue
    const v1, 0x7f0201c2

    const v2, 0x7f02016a

    const v4, 0x7f020525

    const v3, 0x7f02009d

    const v0, 0x7f0204f0

    .line 475
    sparse-switch p0, :sswitch_data_0

    .line 661
    const/4 v0, -0x1

    :goto_0
    :sswitch_0
    return v0

    .line 477
    :sswitch_1
    const v0, 0x7f0204ea

    goto :goto_0

    .line 479
    :sswitch_2
    const v0, 0x7f020163

    goto :goto_0

    .line 480
    :sswitch_3
    const v0, 0x7f02015f

    goto :goto_0

    :sswitch_4
    move v0, v1

    .line 481
    goto :goto_0

    .line 482
    :sswitch_5
    const v0, 0x7f0204cb

    goto :goto_0

    .line 483
    :sswitch_6
    const v0, 0x7f0201d2

    goto :goto_0

    .line 484
    :sswitch_7
    const v0, 0x7f02052d

    goto :goto_0

    .line 485
    :sswitch_8
    const v0, 0x7f020367

    goto :goto_0

    .line 486
    :sswitch_9
    const v0, 0x7f020022

    goto :goto_0

    .line 487
    :sswitch_a
    const v0, 0x7f02019a

    goto :goto_0

    .line 488
    :sswitch_b
    const v0, 0x7f02052c

    goto :goto_0

    .line 489
    :sswitch_c
    const v0, 0x7f0202f7

    goto :goto_0

    .line 490
    :sswitch_d
    const v0, 0x7f020593

    goto :goto_0

    .line 491
    :sswitch_e
    const v0, 0x7f0202f6

    goto :goto_0

    .line 493
    :sswitch_f
    const v0, 0x7f0204f2

    goto :goto_0

    .line 494
    :sswitch_10
    const v0, 0x7f0204f4

    goto :goto_0

    .line 495
    :sswitch_11
    const v0, 0x7f0204f1

    goto :goto_0

    .line 496
    :sswitch_12
    const v0, 0x7f0204f5

    goto :goto_0

    .line 499
    :sswitch_13
    const v0, 0x7f0204f3

    goto :goto_0

    .line 502
    :sswitch_14
    const v0, 0x7f020169

    goto :goto_0

    .line 503
    :sswitch_15
    const v0, 0x7f020164

    goto :goto_0

    .line 504
    :sswitch_16
    const v0, 0x7f02016b

    goto :goto_0

    .line 505
    :sswitch_17
    const v0, 0x7f020165

    goto :goto_0

    :sswitch_18
    move v0, v2

    .line 506
    goto :goto_0

    :sswitch_19
    move v0, v2

    .line 507
    goto :goto_0

    .line 509
    :sswitch_1a
    const v0, 0x7f0204ee

    goto :goto_0

    .line 510
    :sswitch_1b
    const v0, 0x7f0204ef

    goto :goto_0

    .line 511
    :sswitch_1c
    const v0, 0x7f0204eb

    goto :goto_0

    .line 512
    :sswitch_1d
    const v0, 0x7f0204ed

    goto :goto_0

    .line 513
    :sswitch_1e
    const v0, 0x7f0204ea

    goto :goto_0

    .line 514
    :sswitch_1f
    const v0, 0x7f0204ec

    goto :goto_0

    :sswitch_20
    move v0, v3

    .line 516
    goto :goto_0

    .line 517
    :sswitch_21
    const v0, 0x7f02009f

    goto :goto_0

    :sswitch_22
    move v0, v3

    .line 518
    goto :goto_0

    .line 519
    :sswitch_23
    const v0, 0x7f02009e

    goto/16 :goto_0

    :sswitch_24
    move v0, v3

    .line 520
    goto/16 :goto_0

    :sswitch_25
    move v0, v4

    .line 521
    goto/16 :goto_0

    .line 522
    :sswitch_26
    const v0, 0x7f020528

    goto/16 :goto_0

    .line 523
    :sswitch_27
    const v0, 0x7f02052a

    goto/16 :goto_0

    .line 524
    :sswitch_28
    const v0, 0x7f020526

    goto/16 :goto_0

    :sswitch_29
    move v0, v4

    .line 525
    goto/16 :goto_0

    .line 526
    :sswitch_2a
    const v0, 0x7f020528

    goto/16 :goto_0

    .line 527
    :sswitch_2b
    const v0, 0x7f020529

    goto/16 :goto_0

    .line 528
    :sswitch_2c
    const v0, 0x7f020527

    goto/16 :goto_0

    :sswitch_2d
    move v0, v4

    .line 529
    goto/16 :goto_0

    .line 531
    :sswitch_2e
    const v0, 0x7f0200b2

    goto/16 :goto_0

    .line 532
    :sswitch_2f
    const v0, 0x7f0200b3

    goto/16 :goto_0

    .line 533
    :sswitch_30
    const v0, 0x7f0200b7

    goto/16 :goto_0

    .line 534
    :sswitch_31
    const v0, 0x7f0200b9

    goto/16 :goto_0

    .line 535
    :sswitch_32
    const v0, 0x7f0200b5

    goto/16 :goto_0

    .line 536
    :sswitch_33
    const v0, 0x7f0200b8

    goto/16 :goto_0

    .line 537
    :sswitch_34
    const v0, 0x7f0200b6

    goto/16 :goto_0

    .line 538
    :sswitch_35
    const v0, 0x7f0200b4

    goto/16 :goto_0

    .line 539
    :sswitch_36
    const v0, 0x7f0200b1

    goto/16 :goto_0

    :sswitch_37
    move v0, v1

    .line 541
    goto/16 :goto_0

    .line 543
    :sswitch_38
    const v0, 0x7f0200ae

    goto/16 :goto_0

    .line 544
    :sswitch_39
    const v0, 0x7f0200a8

    goto/16 :goto_0

    .line 545
    :sswitch_3a
    const v0, 0x7f0200a6

    goto/16 :goto_0

    .line 546
    :sswitch_3b
    const v0, 0x7f0200ac

    goto/16 :goto_0

    .line 583
    :sswitch_3c
    const v0, 0x7f020162

    goto/16 :goto_0

    .line 584
    :sswitch_3d
    const v0, 0x7f020160

    goto/16 :goto_0

    .line 585
    :sswitch_3e
    const v0, 0x7f020161

    goto/16 :goto_0

    .line 587
    :sswitch_3f
    const v0, 0x7f02052d

    goto/16 :goto_0

    .line 588
    :sswitch_40
    const v0, 0x7f02003a

    goto/16 :goto_0

    .line 589
    :sswitch_41
    const v0, 0x7f020039

    goto/16 :goto_0

    .line 590
    :sswitch_42
    const v0, 0x7f02003b

    goto/16 :goto_0

    .line 591
    :sswitch_43
    const v0, 0x7f02003c

    goto/16 :goto_0

    .line 593
    :sswitch_44
    const v0, 0x7f020367

    goto/16 :goto_0

    .line 594
    :sswitch_45
    const v0, 0x7f0201b2

    goto/16 :goto_0

    .line 595
    :sswitch_46
    const v0, 0x7f0201b1

    goto/16 :goto_0

    .line 596
    :sswitch_47
    const v0, 0x7f0201b4

    goto/16 :goto_0

    .line 597
    :sswitch_48
    const v0, 0x7f0201b3

    goto/16 :goto_0

    .line 475
    :sswitch_data_0
    .sparse-switch
        0x10001001 -> :sswitch_1
        0x10001002 -> :sswitch_0
        0x10001003 -> :sswitch_2
        0x10001004 -> :sswitch_3
        0x10001005 -> :sswitch_4
        0x10001006 -> :sswitch_5
        0x10001007 -> :sswitch_6
        0x10001008 -> :sswitch_7
        0x10001009 -> :sswitch_8
        0x1000100a -> :sswitch_9
        0x1000100b -> :sswitch_a
        0x1000100c -> :sswitch_b
        0x1000100d -> :sswitch_c
        0x1000100e -> :sswitch_d
        0x1000100f -> :sswitch_e
        0x11001108 -> :sswitch_13
        0x11101101 -> :sswitch_f
        0x11101102 -> :sswitch_10
        0x11101103 -> :sswitch_11
        0x11101104 -> :sswitch_12
        0x11101107 -> :sswitch_0
        0x11101108 -> :sswitch_0
        0x11201301 -> :sswitch_0
        0x11201302 -> :sswitch_14
        0x11201303 -> :sswitch_15
        0x11201304 -> :sswitch_16
        0x11201305 -> :sswitch_17
        0x11201306 -> :sswitch_18
        0x11201307 -> :sswitch_19
        0x11301201 -> :sswitch_1a
        0x11301202 -> :sswitch_1b
        0x11301203 -> :sswitch_1c
        0x11301204 -> :sswitch_1d
        0x11301205 -> :sswitch_1e
        0x11301206 -> :sswitch_1f
        0x14001401 -> :sswitch_20
        0x14001402 -> :sswitch_21
        0x14001403 -> :sswitch_22
        0x14001404 -> :sswitch_23
        0x14001405 -> :sswitch_24
        0x14001406 -> :sswitch_25
        0x14001407 -> :sswitch_26
        0x14001408 -> :sswitch_27
        0x14001409 -> :sswitch_28
        0x1400140a -> :sswitch_29
        0x1400140b -> :sswitch_2a
        0x1400140c -> :sswitch_2b
        0x1400140d -> :sswitch_2c
        0x1400140e -> :sswitch_2d
        0x15001500 -> :sswitch_2e
        0x15001501 -> :sswitch_2f
        0x15001502 -> :sswitch_30
        0x15001503 -> :sswitch_31
        0x15001504 -> :sswitch_32
        0x15001505 -> :sswitch_33
        0x15001607 -> :sswitch_34
        0x15001608 -> :sswitch_35
        0x15001609 -> :sswitch_36
        0x160016ff -> :sswitch_37
        0x17001700 -> :sswitch_38
        0x17001701 -> :sswitch_39
        0x17001702 -> :sswitch_3a
        0x17001703 -> :sswitch_3b
        0x19001821 -> :sswitch_3f
        0x19001822 -> :sswitch_40
        0x19001823 -> :sswitch_41
        0x19001824 -> :sswitch_42
        0x19001825 -> :sswitch_43
        0x1a001841 -> :sswitch_44
        0x1a001842 -> :sswitch_45
        0x1a001843 -> :sswitch_46
        0x1a001844 -> :sswitch_47
        0x1a001845 -> :sswitch_48
        0x1d001701 -> :sswitch_3c
        0x1d001702 -> :sswitch_3d
        0x1d001703 -> :sswitch_3e
    .end sparse-switch
.end method

.method public static getPreviousSubMode()I
    .locals 1

    .prologue
    .line 99
    sget v0, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->mPreviousSubMode:I

    return v0
.end method

.method public static getStringId()I
    .locals 1

    .prologue
    .line 666
    sget v0, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->mCurrentSubMode:I

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getStringId(I)I

    move-result v0

    return v0
.end method

.method public static getStringId(I)I
    .locals 5
    .param p0, "subMode"    # I

    .prologue
    const v4, 0x7f060021

    const v3, 0x7f0600b4

    const v2, 0x7f060069

    const v1, 0x7f060033

    const v0, 0x7f060032

    .line 670
    sparse-switch p0, :sswitch_data_0

    .line 789
    const/4 v0, -0x1

    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    move v0, v1

    .line 673
    goto :goto_0

    .line 674
    :sswitch_2
    const v0, 0x7f060034

    goto :goto_0

    .line 675
    :sswitch_3
    const v0, 0x7f060035

    goto :goto_0

    .line 676
    :sswitch_4
    const v0, 0x7f060043

    goto :goto_0

    .line 677
    :sswitch_5
    const v0, 0x7f0600e1

    goto :goto_0

    .line 678
    :sswitch_6
    const v0, 0x7f06006a

    goto :goto_0

    .line 679
    :sswitch_7
    const v0, 0x7f06005a

    goto :goto_0

    :sswitch_8
    move v0, v2

    .line 680
    goto :goto_0

    .line 681
    :sswitch_9
    const v0, 0x7f060185

    goto :goto_0

    .line 682
    :sswitch_a
    const v0, 0x7f060058

    goto :goto_0

    .line 683
    :sswitch_b
    const v0, 0x7f0600aa

    goto :goto_0

    .line 684
    :sswitch_c
    const v0, 0x7f0601c1

    goto :goto_0

    .line 685
    :sswitch_d
    const v0, 0x7f06016f

    goto :goto_0

    .line 687
    :sswitch_e
    const v0, 0x7f060099

    goto :goto_0

    .line 688
    :sswitch_f
    const v0, 0x7f06009a

    goto :goto_0

    .line 689
    :sswitch_10
    const v0, 0x7f060017

    goto :goto_0

    .line 690
    :sswitch_11
    const v0, 0x7f060018

    goto :goto_0

    :sswitch_12
    move v0, v1

    .line 691
    goto :goto_0

    :sswitch_13
    move v0, v1

    .line 692
    goto :goto_0

    .line 693
    :sswitch_14
    const v0, 0x7f060056

    goto :goto_0

    .line 695
    :sswitch_15
    const v0, 0x7f060034

    goto :goto_0

    .line 696
    :sswitch_16
    const v0, 0x7f060091

    goto :goto_0

    .line 697
    :sswitch_17
    const v0, 0x7f060092

    goto :goto_0

    :sswitch_18
    move v0, v3

    .line 706
    goto :goto_0

    .line 707
    :sswitch_19
    const v0, 0x7f0600b5

    goto :goto_0

    :sswitch_1a
    move v0, v3

    .line 708
    goto :goto_0

    .line 709
    :sswitch_1b
    const v0, 0x7f0601d3

    goto :goto_0

    :sswitch_1c
    move v0, v3

    .line 710
    goto :goto_0

    .line 711
    :sswitch_1d
    const v0, 0x7f0600c5

    goto :goto_0

    .line 712
    :sswitch_1e
    const v0, 0x7f0600c7

    goto :goto_0

    .line 713
    :sswitch_1f
    const v0, 0x7f0600c8

    goto :goto_0

    .line 714
    :sswitch_20
    const v0, 0x7f060025

    goto :goto_0

    :sswitch_21
    move v0, v4

    .line 715
    goto :goto_0

    .line 716
    :sswitch_22
    const v0, 0x7f0600bd

    goto :goto_0

    .line 717
    :sswitch_23
    const v0, 0x7f060029

    goto :goto_0

    .line 718
    :sswitch_24
    const v0, 0x7f06002d

    goto :goto_0

    :sswitch_25
    move v0, v4

    .line 719
    goto/16 :goto_0

    .line 721
    :sswitch_26
    const v0, 0x7f060037

    goto/16 :goto_0

    .line 722
    :sswitch_27
    const v0, 0x7f060038

    goto/16 :goto_0

    .line 723
    :sswitch_28
    const v0, 0x7f060039

    goto/16 :goto_0

    .line 724
    :sswitch_29
    const v0, 0x7f06003b

    goto/16 :goto_0

    .line 725
    :sswitch_2a
    const v0, 0x7f06003c

    goto/16 :goto_0

    .line 726
    :sswitch_2b
    const v0, 0x7f060042

    goto/16 :goto_0

    .line 727
    :sswitch_2c
    const v0, 0x7f06003a

    goto/16 :goto_0

    .line 728
    :sswitch_2d
    const v0, 0x7f06003e

    goto/16 :goto_0

    .line 729
    :sswitch_2e
    const v0, 0x7f06003f

    goto/16 :goto_0

    .line 731
    :sswitch_2f
    const v0, 0x7f060151

    goto/16 :goto_0

    .line 732
    :sswitch_30
    const v0, 0x7f060044

    goto/16 :goto_0

    .line 733
    :sswitch_31
    const v0, 0x7f0600ce

    goto/16 :goto_0

    .line 734
    :sswitch_32
    const v0, 0x7f06018c

    goto/16 :goto_0

    .line 735
    :sswitch_33
    const v0, 0x7f0601d0

    goto/16 :goto_0

    .line 736
    :sswitch_34
    const v0, 0x7f060047

    goto/16 :goto_0

    .line 737
    :sswitch_35
    const v0, 0x7f060165

    goto/16 :goto_0

    .line 738
    :sswitch_36
    const v0, 0x7f0600ef

    goto/16 :goto_0

    .line 739
    :sswitch_37
    const v0, 0x7f0600ec

    goto/16 :goto_0

    .line 740
    :sswitch_38
    const v0, 0x7f06012f

    goto/16 :goto_0

    .line 741
    :sswitch_39
    const v0, 0x7f0600cc

    goto/16 :goto_0

    .line 742
    :sswitch_3a
    const v0, 0x7f0600c9

    goto/16 :goto_0

    .line 743
    :sswitch_3b
    const v0, 0x7f06004c

    goto/16 :goto_0

    .line 744
    :sswitch_3c
    const v0, 0x7f06004d

    goto/16 :goto_0

    .line 745
    :sswitch_3d
    const v0, 0x7f06015a

    goto/16 :goto_0

    .line 746
    :sswitch_3e
    const v0, 0x7f060139

    goto/16 :goto_0

    .line 747
    :sswitch_3f
    const v0, 0x7f06015e

    goto/16 :goto_0

    .line 748
    :sswitch_40
    const v0, 0x7f0600f4

    goto/16 :goto_0

    .line 749
    :sswitch_41
    const v0, 0x7f060138

    goto/16 :goto_0

    .line 750
    :sswitch_42
    const v0, 0x7f060046

    goto/16 :goto_0

    .line 751
    :sswitch_43
    const v0, 0x7f060139

    goto/16 :goto_0

    .line 753
    :sswitch_44
    const v0, 0x7f0600d2

    goto/16 :goto_0

    .line 754
    :sswitch_45
    const v0, 0x7f06012e

    goto/16 :goto_0

    .line 755
    :sswitch_46
    const v0, 0x7f06012e

    goto/16 :goto_0

    .line 756
    :sswitch_47
    const v0, 0x7f060050

    goto/16 :goto_0

    .line 757
    :sswitch_48
    const v0, 0x7f06013b

    goto/16 :goto_0

    .line 758
    :sswitch_49
    const v0, 0x7f06004e

    goto/16 :goto_0

    .line 759
    :sswitch_4a
    const v0, 0x7f060054

    goto/16 :goto_0

    .line 760
    :sswitch_4b
    const v0, 0x7f060055

    goto/16 :goto_0

    .line 761
    :sswitch_4c
    const v0, 0x7f06018e

    goto/16 :goto_0

    .line 764
    :sswitch_4d
    const v0, 0x7f060043

    goto/16 :goto_0

    .line 766
    :sswitch_4e
    const v0, 0x7f060190

    goto/16 :goto_0

    .line 767
    :sswitch_4f
    const v0, 0x7f060191

    goto/16 :goto_0

    .line 768
    :sswitch_50
    const v0, 0x7f0600d5

    goto/16 :goto_0

    .line 769
    :sswitch_51
    const v0, 0x7f060159

    goto/16 :goto_0

    .line 771
    :sswitch_52
    const v0, 0x7f060103

    goto/16 :goto_0

    .line 772
    :sswitch_53
    const v0, 0x7f060065

    goto/16 :goto_0

    .line 773
    :sswitch_54
    const v0, 0x7f0600de

    goto/16 :goto_0

    .line 775
    :sswitch_55
    const v0, 0x7f06005a

    goto/16 :goto_0

    .line 776
    :sswitch_56
    const v0, 0x7f0600b3

    goto/16 :goto_0

    .line 777
    :sswitch_57
    const v0, 0x7f0600cb

    goto/16 :goto_0

    .line 778
    :sswitch_58
    const v0, 0x7f0600cf

    goto/16 :goto_0

    .line 779
    :sswitch_59
    const v0, 0x7f0600cd

    goto/16 :goto_0

    :sswitch_5a
    move v0, v2

    .line 781
    goto/16 :goto_0

    :sswitch_5b
    move v0, v2

    .line 782
    goto/16 :goto_0

    .line 783
    :sswitch_5c
    const v0, 0x7f060068

    goto/16 :goto_0

    .line 784
    :sswitch_5d
    const v0, 0x7f06001d

    goto/16 :goto_0

    .line 785
    :sswitch_5e
    const v0, 0x7f06001c

    goto/16 :goto_0

    .line 670
    :sswitch_data_0
    .sparse-switch
        0x10001001 -> :sswitch_0
        0x10001002 -> :sswitch_1
        0x10001003 -> :sswitch_2
        0x10001004 -> :sswitch_3
        0x10001005 -> :sswitch_4
        0x10001006 -> :sswitch_5
        0x10001007 -> :sswitch_6
        0x10001008 -> :sswitch_7
        0x10001009 -> :sswitch_8
        0x1000100a -> :sswitch_9
        0x1000100b -> :sswitch_a
        0x1000100c -> :sswitch_b
        0x1000100d -> :sswitch_c
        0x1000100f -> :sswitch_d
        0x11001108 -> :sswitch_14
        0x11101101 -> :sswitch_e
        0x11101102 -> :sswitch_f
        0x11101103 -> :sswitch_10
        0x11101104 -> :sswitch_11
        0x11101107 -> :sswitch_12
        0x11101108 -> :sswitch_13
        0x11201301 -> :sswitch_15
        0x11201305 -> :sswitch_16
        0x11201306 -> :sswitch_17
        0x11301201 -> :sswitch_0
        0x11301202 -> :sswitch_0
        0x11301203 -> :sswitch_0
        0x11301204 -> :sswitch_0
        0x11301205 -> :sswitch_0
        0x11301206 -> :sswitch_0
        0x14001401 -> :sswitch_18
        0x14001402 -> :sswitch_19
        0x14001403 -> :sswitch_1a
        0x14001404 -> :sswitch_1b
        0x14001405 -> :sswitch_1c
        0x14001406 -> :sswitch_1d
        0x14001407 -> :sswitch_1e
        0x14001408 -> :sswitch_1f
        0x14001409 -> :sswitch_20
        0x1400140a -> :sswitch_21
        0x1400140b -> :sswitch_22
        0x1400140c -> :sswitch_23
        0x1400140d -> :sswitch_24
        0x1400140e -> :sswitch_25
        0x15001500 -> :sswitch_26
        0x15001501 -> :sswitch_27
        0x15001502 -> :sswitch_28
        0x15001503 -> :sswitch_29
        0x15001504 -> :sswitch_2a
        0x15001505 -> :sswitch_2b
        0x15001607 -> :sswitch_2c
        0x15001608 -> :sswitch_2d
        0x15001609 -> :sswitch_2e
        0x16001600 -> :sswitch_2f
        0x16001601 -> :sswitch_30
        0x16001602 -> :sswitch_31
        0x16001603 -> :sswitch_32
        0x16001604 -> :sswitch_33
        0x16001605 -> :sswitch_34
        0x16001606 -> :sswitch_35
        0x16001607 -> :sswitch_36
        0x16001608 -> :sswitch_37
        0x16001609 -> :sswitch_38
        0x1600160a -> :sswitch_39
        0x1600160b -> :sswitch_3a
        0x1600160c -> :sswitch_3b
        0x1600160d -> :sswitch_3c
        0x1600160e -> :sswitch_3d
        0x1600160f -> :sswitch_3e
        0x16001610 -> :sswitch_3f
        0x16001611 -> :sswitch_40
        0x16001612 -> :sswitch_41
        0x16001613 -> :sswitch_42
        0x16001614 -> :sswitch_43
        0x16001615 -> :sswitch_44
        0x16001616 -> :sswitch_45
        0x16001617 -> :sswitch_46
        0x16001618 -> :sswitch_47
        0x16001619 -> :sswitch_48
        0x1600161a -> :sswitch_49
        0x1600161b -> :sswitch_4a
        0x1600161c -> :sswitch_4b
        0x1600161d -> :sswitch_4c
        0x160016ff -> :sswitch_4d
        0x17001700 -> :sswitch_4e
        0x17001701 -> :sswitch_4f
        0x17001702 -> :sswitch_50
        0x17001703 -> :sswitch_51
        0x19001821 -> :sswitch_55
        0x19001822 -> :sswitch_56
        0x19001823 -> :sswitch_57
        0x19001824 -> :sswitch_58
        0x19001825 -> :sswitch_59
        0x1a001841 -> :sswitch_5a
        0x1a001842 -> :sswitch_5b
        0x1a001843 -> :sswitch_5c
        0x1a001844 -> :sswitch_5d
        0x1a001845 -> :sswitch_5e
        0x1d001701 -> :sswitch_52
        0x1d001702 -> :sswitch_53
        0x1d001703 -> :sswitch_54
    .end sparse-switch
.end method

.method public static getSubMode()I
    .locals 1

    .prologue
    .line 96
    sget v0, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->mCurrentSubMode:I

    return v0
.end method

.method public static isCheckPreventRetouchButton(I)Z
    .locals 1
    .param p0, "resid"    # I

    .prologue
    .line 795
    sparse-switch p0, :sswitch_data_0

    .line 846
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 843
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 795
    nop

    :sswitch_data_0
    .sparse-switch
        0x15001500 -> :sswitch_0
        0x15001501 -> :sswitch_0
        0x15001502 -> :sswitch_0
        0x15001503 -> :sswitch_0
        0x15001504 -> :sswitch_0
        0x15001505 -> :sswitch_0
        0x15001607 -> :sswitch_0
        0x15001608 -> :sswitch_0
        0x15001609 -> :sswitch_0
        0x16001600 -> :sswitch_0
        0x16001601 -> :sswitch_0
        0x16001602 -> :sswitch_0
        0x16001603 -> :sswitch_0
        0x16001604 -> :sswitch_0
        0x16001605 -> :sswitch_0
        0x16001606 -> :sswitch_0
        0x16001607 -> :sswitch_0
        0x16001608 -> :sswitch_0
        0x16001609 -> :sswitch_0
        0x1600160a -> :sswitch_0
        0x1600160b -> :sswitch_0
        0x1600160c -> :sswitch_0
        0x1600160d -> :sswitch_0
        0x1600160e -> :sswitch_0
        0x1600160f -> :sswitch_0
        0x16001610 -> :sswitch_0
        0x16001611 -> :sswitch_0
        0x16001612 -> :sswitch_0
        0x16001613 -> :sswitch_0
        0x16001614 -> :sswitch_0
        0x16001615 -> :sswitch_0
        0x16001616 -> :sswitch_0
        0x16001617 -> :sswitch_0
        0x16001618 -> :sswitch_0
        0x16001619 -> :sswitch_0
        0x1600161a -> :sswitch_0
        0x1600161b -> :sswitch_0
        0x1600161c -> :sswitch_0
        0x1600161d -> :sswitch_0
        0x17001700 -> :sswitch_0
        0x17001701 -> :sswitch_0
        0x17001702 -> :sswitch_0
        0x17001703 -> :sswitch_0
    .end sparse-switch
.end method

.method public static setMode(I)V
    .locals 1
    .param p0, "subMode"    # I

    .prologue
    .line 103
    sget v0, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->mCurrentSubMode:I

    sput v0, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->mPreviousSubMode:I

    .line 104
    sput p0, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->mCurrentSubMode:I

    .line 105
    return-void
.end method
