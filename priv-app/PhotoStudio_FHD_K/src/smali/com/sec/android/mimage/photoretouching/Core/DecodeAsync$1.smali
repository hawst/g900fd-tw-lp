.class Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;
.super Ljava/lang/Object;
.source "DecodeAsync.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 428
    const/4 v0, -0x1

    .line 429
    .local v0, "status":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFrom:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$2(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 439
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressCancelFlag:Z
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$3(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 440
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0601b9

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;I)V

    .line 442
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$5(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 443
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$5(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;->changeViewStatus(I)V

    .line 444
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$6(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 445
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$6(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 446
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$7(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;Landroid/app/ProgressDialog;)V

    .line 448
    :cond_2
    return-void

    .line 432
    :pswitch_0
    const/high16 v0, 0x10000000

    .line 433
    goto :goto_0

    .line 436
    :pswitch_1
    const/high16 v0, 0x2e000000

    goto :goto_0

    .line 429
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
