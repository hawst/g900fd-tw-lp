.class public Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonBackground;
.super Landroid/widget/FrameLayout;
.source "BottomPenButtonBackground.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mLayout:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutResId"    # I

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 40
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonBackground;->mContext:Landroid/content/Context;

    .line 41
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonBackground;->mLayout:Landroid/view/ViewGroup;

    .line 19
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonBackground;->mContext:Landroid/content/Context;

    .line 20
    invoke-static {p1, p2, p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonBackground;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonBackground;->mLayout:Landroid/view/ViewGroup;

    .line 21
    return-void
.end method


# virtual methods
.method public isFocused()Z
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonBackground;->mLayout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    return v0
.end method
