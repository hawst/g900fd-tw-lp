.class Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;
.super Ljava/lang/Object;
.source "SubViewButtonsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->doInitialScroll()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

.field private final synthetic val$childCount:I

.field private final synthetic val$scrollBtnCount:I

.field private final synthetic val$scrollTo:I


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;III)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->val$childCount:I

    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->val$scrollBtnCount:I

    iput p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->val$scrollTo:I

    .line 1318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;)Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
    .locals 1

    .prologue
    .line 1318
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/high16 v3, 0x16000000

    const/4 v2, 0x0

    .line 1322
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->val$childCount:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->val$scrollBtnCount:I

    if-lt v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->val$scrollTo:I

    if-lez v0, :cond_1

    .line 1324
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->val$scrollTo:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->scrollTo(II)V

    .line 1325
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v0

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->val$scrollTo:I

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;I)V

    .line 1345
    const-wide/16 v2, 0x32

    .line 1325
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1364
    :cond_0
    :goto_0
    return-void

    .line 1348
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1349
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setVisibility(I)V

    .line 1350
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    if-eq v0, v3, :cond_3

    .line 1351
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1352
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;Z)V

    .line 1353
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setAnimation(Z)V

    .line 1354
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 1355
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->subMenuAnimate()V

    goto :goto_0

    .line 1358
    :cond_4
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x1e110000

    if-eq v0, v1, :cond_5

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x2c000000

    if-ne v0, v1, :cond_0

    .line 1359
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;Z)V

    .line 1360
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1361
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setAnimation(Z)V

    goto :goto_0
.end method
