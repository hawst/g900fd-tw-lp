.class Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;
.super Ljava/lang/Object;
.source "InputFileName.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private isContain:Z

.field private mBeforeCusorPos:I

.field private mBeforeString:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->isContain:Z

    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->mBeforeString:Ljava/lang/String;

    .line 117
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->mBeforeCusorPos:I

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/16 v4, 0x32

    .line 120
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;

    invoke-virtual {v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 121
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->isContain:Z

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->mBeforeString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->mBeforeCusorPos:I

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->mBeforeString:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->setSelection(I)V

    .line 124
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f060086

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;I)V

    .line 126
    :cond_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-le v1, v4, :cond_1

    .line 128
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->mBeforeString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->mBeforeCusorPos:I

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->mBeforeString:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->setSelection(I)V

    .line 131
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f060083

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "text":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 133
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 135
    .end local v0    # "text":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;

    invoke-virtual {v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 136
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->isContain:Z

    .line 143
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->mBeforeString:Ljava/lang/String;

    .line 145
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 150
    add-int v1, p2, p3

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->mBeforeCusorPos:I

    .line 152
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 153
    .local v0, "name":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 156
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 157
    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 158
    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 159
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 160
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 161
    const-string v1, "<"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 162
    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 163
    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 164
    const-string v1, "\\"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;->isContain:Z

    goto :goto_0
.end method
