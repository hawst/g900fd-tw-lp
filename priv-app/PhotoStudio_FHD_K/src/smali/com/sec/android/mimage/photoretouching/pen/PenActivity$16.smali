.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;
.super Ljava/lang/Object;
.source "PenActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->newSpenListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    .line 1221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onZoom(FFF)V
    .locals 6
    .param p1, "arg0"    # F
    .param p2, "arg1"    # F
    .param p3, "arg2"    # F

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x42c80000    # 100.0f

    .line 1225
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevTime:J
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$33(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v2, 0x1c2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 1226
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$34(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setZoom(FFF)V

    .line 1227
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$34(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$35(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;F)V

    .line 1228
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevZoomRatio:F
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$36(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$34(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$37(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;F)V

    .line 1229
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v0, v5}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$30(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V

    .line 1250
    :goto_0
    return-void

    .line 1232
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevZoomRatio:F
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$36(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$34(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 1234
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$34(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v0

    cmpl-float v0, p3, v0

    if-ltz v0, :cond_1

    .line 1235
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$34(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setZoom(FFF)V

    .line 1236
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$34(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$35(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;F)V

    .line 1237
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$38(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;J)V

    .line 1246
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v0, p3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$35(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;F)V

    .line 1247
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$34(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v1

    div-float v1, p3, v1

    mul-float/2addr v1, v4

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$37(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;F)V

    .line 1248
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v0, v5}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$30(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V

    goto :goto_0

    .line 1239
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevZoomRatio:F
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$36(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$34(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1240
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$34(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v0

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_1

    .line 1241
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$34(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setZoom(FFF)V

    .line 1242
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$34(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$35(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;F)V

    .line 1243
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$38(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;J)V

    goto :goto_1
.end method
