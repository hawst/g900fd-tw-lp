.class Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;
.super Landroid/os/AsyncTask;
.source "EffectManagerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;-><init>(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;)Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    return-object v0
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask$1;-><init>(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 83
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 108
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 113
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;Landroid/app/ProgressDialog;)V

    .line 115
    :cond_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 90
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 91
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 94
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;Landroid/app/ProgressDialog;)V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;Landroid/app/ProgressDialog;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    const v2, 0x7f060075

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 103
    :cond_1
    return-void
.end method
