.class Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;
.super Ljava/lang/Object;
.source "PortraitView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->init2DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    .line 626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 4
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 664
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 665
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewWidth()I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$27(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewHeight()I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->redoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 666
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$30(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Landroid/graphics/Bitmap;)V

    .line 667
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 629
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    invoke-static {v0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Z)V

    .line 630
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewWidth()I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$27(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewHeight()I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->redo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z

    .line 631
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 633
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 634
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 635
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableSave()V

    .line 636
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 637
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 649
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 651
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 652
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 659
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->refreshImageAndBottomButtons()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$29(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 660
    return-void

    .line 639
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_0

    .line 644
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 645
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    .line 646
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 647
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_0

    .line 656
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 657
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_1
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 672
    return-void
.end method
