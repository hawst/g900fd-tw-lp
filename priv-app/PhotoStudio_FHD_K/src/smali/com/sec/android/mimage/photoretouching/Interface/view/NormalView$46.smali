.class Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;
.super Ljava/lang/Object;
.source "NormalView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initDefaultSelectDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    .line 2627
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public TouchFunction(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 2631
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2632
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 2633
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2635
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 2636
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v4

    .line 2637
    .local v4, "roi":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v2

    invoke-virtual {v4, v6, v6, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 2638
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;-><init>()V

    .line 2639
    .local v0, "cntr":Lcom/sec/android/mimage/photoretouching/Core/ChainContour;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->init(II)V

    .line 2640
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v1

    .line 2641
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v2

    .line 2642
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    .line 2644
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawPathList()Landroid/graphics/Path;

    move-result-object v5

    .line 2640
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Z)V

    .line 2646
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->destroy()V

    .line 2647
    const/4 v0, 0x0

    .line 2648
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->invalidateViews()V

    .line 2651
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$45(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2653
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    const/4 v5, 0x0

    invoke-direct {v2, v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;)V

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;)V

    .line 2654
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$45(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;->start()V

    .line 2658
    .end local v0    # "cntr":Lcom/sec/android/mimage/photoretouching/Core/ChainContour;
    .end local v4    # "roi":Landroid/graphics/Rect;
    :cond_1
    return-void
.end method
