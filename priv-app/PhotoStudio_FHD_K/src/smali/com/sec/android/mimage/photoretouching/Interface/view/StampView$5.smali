.class Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$5;
.super Ljava/lang/Object;
.source "StampView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->init3DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .line 317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 0
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 337
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableCancel()V

    .line 322
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 323
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->doCancel()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    .line 329
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    if-nez v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->openDrawer()V

    .line 331
    :cond_0
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 343
    return-void
.end method
