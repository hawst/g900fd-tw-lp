.class Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;
.super Ljava/lang/Object;
.source "ApplyOriginalThreadManager.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ApplyThread"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public mErrorcase:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;)V
    .locals 1

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;->mErrorcase:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;)V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic run(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;->run(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public run(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;)Ljava/lang/Void;
    .locals 3
    .param p1, "jc"    # Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 155
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->access$0(Z)V

    .line 156
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;->mErrorcase:Z

    .line 157
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;)V

    .line 169
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread$1;->start()V

    .line 170
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mApplyList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_0

    .line 175
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->access$0(Z)V

    .line 176
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;->mErrorcase:Z

    .line 177
    const/4 v1, 0x0

    return-object v1

    .line 172
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mApplyList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;

    .line 173
    .local v0, "info":Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->applyData(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;)V
    invoke-static {v1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;)V

    goto :goto_0
.end method
