.class public Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;
.super Ljava/lang/Object;
.source "IndicatorListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mPager:Landroid/support/v4/view/ViewPager;

.field private mView:[Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;->mView:[Landroid/widget/LinearLayout;

    .line 13
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;->mPager:Landroid/support/v4/view/ViewPager;

    .line 16
    return-void
.end method


# virtual methods
.method public initIndicatorListener([Landroid/widget/LinearLayout;Landroid/support/v4/view/ViewPager;)V
    .locals 3
    .param p1, "iv"    # [Landroid/widget/LinearLayout;
    .param p2, "pager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 20
    array-length v1, p1

    new-array v1, v1, [Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;->mView:[Landroid/widget/LinearLayout;

    .line 21
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    .line 26
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;->mPager:Landroid/support/v4/view/ViewPager;

    .line 27
    return-void

    .line 23
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;->mView:[Landroid/widget/LinearLayout;

    aget-object v2, p1, v0

    aput-object v2, v1, v0

    .line 21
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 32
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;->mView:[Landroid/widget/LinearLayout;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 40
    :goto_1
    return-void

    .line 34
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;->mView:[Landroid/widget/LinearLayout;

    aget-object v1, v1, v0

    if-ne p1, v1, :cond_1

    .line 36
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_1

    .line 32
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
