.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;
.super Ljava/lang/Object;
.source "PenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StoredSettingManager"
.end annotation


# instance fields
.field private final PEN_PATH:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 1

    .prologue
    .line 2240
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    .line 2239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2353
    const-string v0, "com.samsung.android.sdk.pen.pen.preload."

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->PEN_PATH:Ljava/lang/String;

    .line 2241
    return-void
.end method

.method private getDefaultColor(I)I
    .locals 1
    .param p1, "type"    # I

    .prologue
    const/high16 v0, -0x1000000

    .line 2384
    packed-switch p1, :pswitch_data_0

    .line 2392
    :goto_0
    :pswitch_0
    return v0

    .line 2389
    :pswitch_1
    const/high16 v0, 0x7e000000

    goto :goto_0

    .line 2390
    :pswitch_2
    const/high16 v0, 0x7f000000

    goto :goto_0

    .line 2384
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getDefaultName(I)Ljava/lang/String;
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 2357
    packed-switch p1, :pswitch_data_0

    .line 2365
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2358
    :pswitch_0
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    goto :goto_0

    .line 2359
    :pswitch_1
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    goto :goto_0

    .line 2360
    :pswitch_2
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Brush"

    goto :goto_0

    .line 2361
    :pswitch_3
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    goto :goto_0

    .line 2362
    :pswitch_4
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.Marker"

    goto :goto_0

    .line 2363
    :pswitch_5
    const-string v0, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    goto :goto_0

    .line 2357
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private getDefaultSize(I)F
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 2369
    const/high16 v0, 0x41f00000    # 30.0f

    .line 2370
    .local v0, "ret":F
    const v1, 0x40aaaaaa

    .line 2371
    .local v1, "scale":F
    packed-switch p1, :pswitch_data_0

    .line 2379
    :goto_0
    div-float v2, v0, v1

    return v2

    .line 2372
    :pswitch_0
    const/high16 v0, 0x41300000    # 11.0f

    goto :goto_0

    .line 2373
    :pswitch_1
    const/high16 v0, 0x41800000    # 16.0f

    goto :goto_0

    .line 2374
    :pswitch_2
    const/high16 v0, 0x42700000    # 60.0f

    goto :goto_0

    .line 2375
    :pswitch_3
    const/high16 v0, 0x42700000    # 60.0f

    goto :goto_0

    .line 2376
    :pswitch_4
    const/high16 v0, 0x41300000    # 11.0f

    goto :goto_0

    .line 2377
    :pswitch_5
    const/high16 v0, 0x434b0000    # 203.0f

    goto :goto_0

    .line 2371
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private getSharedSPenSetting()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2344
    const-string v0, "sharedSPenSetting"

    return-object v0
.end method

.method private getlastSavedSettingType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2351
    const-string v0, "SPenLastSetting"

    return-object v0
.end method

.method private isSaved(I)Ljava/lang/String;
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 2345
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SPenSaved_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private savedAdvancedSetting(I)Ljava/lang/String;
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 2350
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SPenAdvancedSetting_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private savedColor(I)Ljava/lang/String;
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 2348
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SPenColor_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private savedCurvable(I)Ljava/lang/String;
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 2349
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SPenCurvable_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private savedName(I)Ljava/lang/String;
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 2346
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SPenName_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private savedSize(I)Ljava/lang/String;
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 2347
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SPenSize_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public commit()Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 2315
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getSharedSPenSetting()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v9}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 2316
    .local v4, "sharedPref":Landroid/content/SharedPreferences;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v3

    .line 2318
    .local v3, "penInfo":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v6, 0x6

    if-lt v2, v6, :cond_0

    .line 2341
    :goto_1
    const/4 v6, 0x1

    return v6

    .line 2320
    :cond_0
    iget-object v6, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedName(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-interface {v4, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2322
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2323
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedName(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2324
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedSize(I)Ljava/lang/String;

    move-result-object v6

    iget v7, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 2325
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedColor(I)Ljava/lang/String;

    move-result-object v6

    iget v7, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2326
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedCurvable(I)Ljava/lang/String;

    move-result-object v6

    iget-boolean v7, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2327
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedAdvancedSetting(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2328
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getlastSavedSettingType()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v6, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2329
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2331
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getlastSavedSettingType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v9}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 2332
    .local v5, "sharedPrefForLastSetting":Landroid/content/SharedPreferences;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2333
    .local v1, "editorForLastSetting":Landroid/content/SharedPreferences$Editor;
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getlastSavedSettingType()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-interface {v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2334
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .line 2318
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "editorForLastSetting":Landroid/content/SharedPreferences$Editor;
    .end local v5    # "sharedPrefForLastSetting":Landroid/content/SharedPreferences;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public init()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2245
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getSharedSPenSetting()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2246
    .local v2, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2247
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v3, 0x6

    if-lt v1, v3, :cond_0

    .line 2261
    return v5

    .line 2249
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->isSaved(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2251
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->isSaved(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2252
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedName(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getDefaultName(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2253
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedSize(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getDefaultSize(I)F

    move-result v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 2254
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedColor(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getDefaultColor(I)I

    move-result v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2255
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedCurvable(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2256
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedAdvancedSetting(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2257
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2247
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public load()Z
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 2266
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getSharedSPenSetting()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 2267
    .local v7, "sharedPref":Landroid/content/SharedPreferences;
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getlastSavedSettingType()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 2269
    .local v5, "lastSettingType":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v9, 0x6

    if-lt v2, v9, :cond_0

    .line 2300
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v3

    .line 2301
    .local v3, "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedName(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getDefaultName(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 2302
    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedSize(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getDefaultSize(I)F

    move-result v10

    invoke-interface {v7, v9, v10}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v9

    iput v9, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 2303
    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedColor(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getDefaultColor(I)I

    move-result v10

    invoke-interface {v7, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    iput v9, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 2304
    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedCurvable(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    iput-boolean v9, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    .line 2305
    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedAdvancedSetting(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, ""

    invoke-interface {v7, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    .line 2306
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v9

    invoke-virtual {v9, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 2307
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v9

    invoke-virtual {v9, v3}, Lcom/sec/android/pen/util/SettingView;->setSpenPenInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 2308
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->update()V

    .line 2310
    return v12

    .line 2271
    .end local v3    # "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    :cond_0
    if-ne v2, v5, :cond_1

    .line 2269
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2274
    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getDefaultName(I)Ljava/lang/String;

    move-result-object v6

    .line 2275
    .local v6, "name":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getDefaultSize(I)F

    move-result v8

    .line 2276
    .local v8, "size":F
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getDefaultColor(I)I

    move-result v1

    .line 2277
    .local v1, "color":I
    const/4 v4, 0x1

    .line 2278
    .local v4, "isCurvable":Z
    const-string v0, ""

    .line 2280
    .local v0, "advancedSetting":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->isSaved(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2282
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedName(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getDefaultName(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2283
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedSize(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getDefaultSize(I)F

    move-result v10

    invoke-interface {v7, v9, v10}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v8

    .line 2284
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedColor(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->getDefaultColor(I)I

    move-result v10

    invoke-interface {v7, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 2285
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedCurvable(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 2286
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->savedAdvancedSetting(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, ""

    invoke-interface {v7, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2289
    :cond_2
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v3

    .line 2290
    .restart local v3    # "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    iput-object v6, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 2291
    iput v8, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 2292
    iput v1, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 2293
    iput-boolean v4, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    .line 2294
    iput-object v0, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    .line 2295
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v9

    invoke-virtual {v9, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 2296
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v9

    invoke-virtual {v9, v3}, Lcom/sec/android/pen/util/SettingView;->setSpenPenInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 2297
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->update()V

    goto/16 :goto_1
.end method
