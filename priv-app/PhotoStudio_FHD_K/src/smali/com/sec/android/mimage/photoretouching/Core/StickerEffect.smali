.class public Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;
.super Ljava/lang/Object;
.source "StickerEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;
    }
.end annotation


# instance fields
.field private final STICKER_MAX_NUM:I

.field private mBm_delete:Landroid/graphics/Bitmap;

.field private mBm_delete_press:Landroid/graphics/Bitmap;

.field private mBm_lrtb:Landroid/graphics/Bitmap;

.field private mBm_lrtb_press:Landroid/graphics/Bitmap;

.field private mBm_rotate:Landroid/graphics/Bitmap;

.field private mBm_rotate_press:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mCurrentStickerCount:I

.field private mCurrentStickerIndex:I

.field mDeletePressed:Z

.field private mGreyPaint:Landroid/graphics/Paint;

.field private mHandler_LB:Landroid/graphics/Bitmap;

.field private mHandler_RB:Landroid/graphics/Bitmap;

.field private mHandler_delete:Landroid/graphics/Bitmap;

.field private mHandler_rotate:Landroid/graphics/Bitmap;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mRectPaint:Landroid/graphics/Paint;

.field private mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

.field private mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

.field mTouchPressed:Z

.field private mTouchType:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->STICKER_MAX_NUM:I

    .line 27
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    .line 29
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 30
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    .line 32
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 34
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    .line 36
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    .line 37
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    .line 40
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mDeletePressed:Z

    .line 41
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchPressed:Z

    .line 43
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_delete:Landroid/graphics/Bitmap;

    .line 44
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    .line 45
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 46
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 47
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 48
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 59
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imgInfo"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p3, "stickerCallback"    # Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->STICKER_MAX_NUM:I

    .line 27
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    .line 29
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 30
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    .line 32
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 34
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    .line 36
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    .line 37
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    .line 40
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mDeletePressed:Z

    .line 41
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchPressed:Z

    .line 43
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_delete:Landroid/graphics/Bitmap;

    .line 44
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    .line 45
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 46
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 47
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 48
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 59
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    .line 63
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    .line 64
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 66
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020374

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    .line 67
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020375

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_delete:Landroid/graphics/Bitmap;

    .line 68
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020378

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    .line 70
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020377

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    .line 71
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020376

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    .line 72
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020379

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    .line 74
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 75
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 76
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 77
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 79
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 80
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mRectPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mRectPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mRectPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 84
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    .line 85
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    const/16 v1, 0x4b

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    if-nez v0, :cond_0

    .line 96
    new-array v0, v3, [Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    .line 98
    :cond_0
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    .line 99
    return-void
.end method

.method private setHandlerIconPressed(I)V
    .locals 1
    .param p1, "touchType"    # I

    .prologue
    .line 289
    packed-switch p1, :pswitch_data_0

    .line 317
    :goto_0
    return-void

    .line 291
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchPressed:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mDeletePressed:Z

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 297
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchPressed:Z

    if-eqz v0, :cond_1

    .line 298
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 300
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 303
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchPressed:Z

    if-eqz v0, :cond_2

    .line 304
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 306
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 309
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchPressed:Z

    if-eqz v0, :cond_3

    .line 310
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 312
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 289
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public Destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 233
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    if-eqz v1, :cond_0

    .line 235
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 238
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    .line 240
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 241
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 242
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_delete:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 244
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 245
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 246
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 248
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 249
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 250
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 251
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 253
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 254
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    .line 255
    return-void

    .line 236
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 237
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->destory()V

    .line 235
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public applyOriginal()[I
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 688
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v10

    .line 689
    .local v10, "ret":[I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    .line 690
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    .line 691
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 689
    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 692
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v1

    .line 694
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    .line 697
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    .line 698
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 692
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 700
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    if-lt v8, v1, :cond_0

    .line 709
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v7

    move-object v1, v10

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 711
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 712
    return-object v10

    .line 702
    :cond_0
    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->getStickerOrderPosition(I)I

    move-result v9

    .line 703
    .local v9, "idx":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v9

    if-eqz v1, :cond_1

    .line 705
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v9

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->applyOriginal(Landroid/graphics/Bitmap;)V

    .line 700
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public applyPreview()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 663
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    .line 664
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 665
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 663
    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 666
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 668
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 671
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 672
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 666
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 674
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    if-lt v8, v1, :cond_0

    .line 682
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewBuffer(Landroid/graphics/Bitmap;)V

    .line 683
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 684
    return-void

    .line 676
    :cond_0
    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->getStickerOrderPosition(I)I

    move-result v9

    .line 677
    .local v9, "idx":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v9

    if-eqz v1, :cond_1

    .line 679
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v9

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->applyPreview(Landroid/graphics/Bitmap;)V

    .line 674
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public clearResource()V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 260
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 261
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_delete:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 263
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 264
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 265
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 267
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 268
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 269
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 270
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 271
    return-void
.end method

.method public configurationChanged()V
    .locals 0

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->resetHandlerIconPressed()V

    .line 286
    return-void
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;)V
    .locals 1
    .param p1, "sticker"    # Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    .prologue
    .line 105
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 106
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    .line 107
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    .line 108
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    .line 109
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    .line 110
    return-void
.end method

.method public deleteSticker()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 174
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    if-eq v2, v5, :cond_0

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    if-lez v2, :cond_0

    .line 176
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    .line 178
    .local v0, "curStckIdx":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->destory()V

    .line 179
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    aput-object v6, v2, v3

    .line 181
    const/4 v1, 0x0

    .line 182
    .local v1, "i":I
    move v1, v0

    :goto_0
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_2

    .line 191
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    .line 192
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    .line 194
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    if-gtz v2, :cond_4

    .line 195
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;->actionBarUnableSave()V

    .line 200
    .end local v0    # "curStckIdx":I
    .end local v1    # "i":I
    :cond_0
    :goto_1
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    if-nez v2, :cond_1

    .line 202
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    if-eqz v2, :cond_1

    .line 204
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;->set2DepthActionBar()V

    .line 208
    :cond_1
    return-void

    .line 184
    .restart local v0    # "curStckIdx":I
    .restart local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v1

    if-nez v2, :cond_3

    .line 186
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    add-int/lit8 v4, v1, 0x1

    aget-object v3, v3, v4

    aput-object v3, v2, v1

    .line 187
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    add-int/lit8 v3, v1, 0x1

    aput-object v6, v2, v3

    .line 182
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 197
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;->actionBarAbleSave()V

    goto :goto_1
.end method

.method public deleteStickerAll()V
    .locals 3

    .prologue
    .line 212
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    if-lt v0, v1, :cond_0

    .line 219
    return-void

    .line 213
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->destory()V

    .line 214
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 216
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    .line 217
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    .line 212
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public deleteStickerOrdering(I)V
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 648
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    if-lt v0, v1, :cond_0

    .line 653
    move v0, p1

    :goto_1
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_2

    .line 657
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v0, v1, -0x1

    :goto_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    array-length v1, v1

    if-lt v0, v1, :cond_3

    .line 660
    return-void

    .line 649
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getZOrder()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getZOrder()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 650
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getZOrder()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->setZOrder(I)V

    .line 648
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 654
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    add-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getZOrder()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->setZOrder(I)V

    .line 653
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 658
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v0

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->setZOrder(I)V

    .line 657
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public drawStickerBdry(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 24
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 460
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    if-nez v2, :cond_1

    .line 617
    :cond_0
    :goto_0
    return-void

    .line 463
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v22

    .line 466
    .local v22, "viewTransform":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v21

    .line 467
    .local v21, "supportViewtransMatrix":Landroid/graphics/Matrix;
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 468
    .local v15, "previewRect":Landroid/graphics/RectF;
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 469
    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 472
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    .line 473
    .local v8, "drawRoi":Landroid/graphics/RectF;
    new-instance v20, Landroid/graphics/RectF;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/RectF;-><init>()V

    .line 475
    .local v20, "src":Landroid/graphics/RectF;
    const/4 v2, 0x2

    new-array v14, v2, [F

    .line 477
    .local v14, "point":[F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 479
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    array-length v2, v2

    if-lt v11, v2, :cond_6

    .line 566
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 567
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    aget-object v2, v2, v3

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 569
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    aput v3, v14, v2

    .line 570
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->y:F

    aput v3, v14, v2

    .line 571
    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 573
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getDrawBdry()Landroid/graphics/RectF;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 574
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v8, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 576
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 577
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getAngle()I

    move-result v2

    int-to-float v2, v2

    .line 578
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 579
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 577
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 581
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 582
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 581
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 583
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 584
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 583
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 585
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 586
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 585
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 587
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 588
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 587
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 590
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    .line 591
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 592
    iget v3, v8, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 593
    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 591
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 596
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    .line 597
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 598
    iget v3, v8, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 599
    iget v4, v8, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 597
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 602
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_4

    .line 603
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 604
    iget v3, v8, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 605
    iget v4, v8, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 603
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 608
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_5

    .line 609
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 610
    iget v3, v8, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 611
    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 609
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 615
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0

    .line 481
    :cond_6
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    if-lt v12, v2, :cond_7

    .line 479
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 483
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v12

    if-eqz v2, :cond_8

    .line 484
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getZOrder()I

    move-result v2

    if-ne v2, v11, :cond_8

    .line 486
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 488
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v3, v3, v12

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    aput v3, v14, v2

    .line 489
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v3, v3, v12

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->y:F

    aput v3, v14, v2

    .line 490
    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 492
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getDrawBdry()Landroid/graphics/RectF;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 493
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v8, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 495
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 496
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getAngle()I

    move-result v2

    int-to-float v2, v2

    .line 497
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 498
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 496
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 500
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sj, SE - drawStickerBdry() - viewTransform : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 501
    const-string v3, " / mSticker[j].getDrawBdry() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v3, v3, v12

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getDrawBdry()Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 502
    const-string v3, " / drawRoi : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 500
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 504
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v23

    .line 505
    .local v23, "width":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    .line 506
    .local v10, "height":I
    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v0, v2

    move/from16 v17, v0

    .line 507
    .local v17, "resizeWidth":I
    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v0, v2

    move/from16 v16, v0

    .line 508
    .local v16, "resizeHeight":I
    move/from16 v0, v17

    int-to-float v2, v0

    move/from16 v0, v23

    int-to-float v3, v0

    div-float v19, v2, v3

    .line 509
    .local v19, "scaleWidth":F
    move/from16 v0, v16

    int-to-float v2, v0

    int-to-float v3, v10

    div-float v18, v2, v3

    .line 511
    .local v18, "scaleHeight":F
    new-instance v13, Landroid/graphics/Matrix;

    invoke-direct {v13}, Landroid/graphics/Matrix;-><init>()V

    .line 512
    .local v13, "matrix":Landroid/graphics/Matrix;
    move/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 513
    iget v2, v8, Landroid/graphics/RectF;->left:F

    iget v3, v8, Landroid/graphics/RectF;->top:F

    invoke-virtual {v13, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 515
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sj, SE - drawStickerBdry() - matrix : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / scaleWidth : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 516
    const-string v3, " / scaleHeight : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / drawRoi : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / srcRoi : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 517
    const-string v3, " / mImageData.getDrawCanvasRoiBasedOnViewTransform() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 515
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 520
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v19, v2

    if-eqz v2, :cond_9

    .line 523
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 535
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 536
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getAngle()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    .line 537
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 538
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 536
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 539
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 540
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getAngle()I

    move-result v2

    int-to-float v2, v2

    .line 541
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 542
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 540
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 543
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v19, v2

    if-eqz v2, :cond_a

    .line 546
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v13, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 556
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 559
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 481
    .end local v10    # "height":I
    .end local v13    # "matrix":Landroid/graphics/Matrix;
    .end local v16    # "resizeHeight":I
    .end local v17    # "resizeWidth":I
    .end local v18    # "scaleHeight":F
    .end local v19    # "scaleWidth":F
    .end local v23    # "width":I
    :cond_8
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2

    .line 525
    .restart local v10    # "height":I
    .restart local v13    # "matrix":Landroid/graphics/Matrix;
    .restart local v16    # "resizeHeight":I
    .restart local v17    # "resizeWidth":I
    .restart local v18    # "scaleHeight":F
    .restart local v19    # "scaleWidth":F
    .restart local v23    # "width":I
    :catch_0
    move-exception v9

    .line 526
    .local v9, "e":Ljava/lang/Exception;
    goto/16 :goto_0

    .line 531
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iget v3, v8, Landroid/graphics/RectF;->left:F

    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_3

    .line 548
    :catch_1
    move-exception v9

    .line 549
    .restart local v9    # "e":Ljava/lang/Exception;
    goto/16 :goto_0

    .line 554
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iget v3, v8, Landroid/graphics/RectF;->left:F

    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_4
.end method

.method public freeResource()V
    .locals 0

    .prologue
    .line 224
    return-void
.end method

.method public getCurStickerCount()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    return v0
.end method

.method public getCurStickerIdx()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 120
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSticker()[Lcom/sec/android/mimage/photoretouching/Core/Sticker;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    return-object v0
.end method

.method public getStickerOrderPosition(I)I
    .locals 2
    .param p1, "orderNum"    # I

    .prologue
    .line 630
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 637
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v0, v1, -0x1

    .end local v0    # "i":I
    :goto_1
    return v0

    .line 631
    .restart local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v0

    if-nez v1, :cond_2

    .line 630
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 633
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getZOrder()I

    move-result v1

    if-ne v1, p1, :cond_1

    goto :goto_1
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 115
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetHandlerIconPressed()V
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchPressed:Z

    .line 322
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 323
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 324
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 325
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 326
    return-void
.end method

.method public resetStickerOrdering()V
    .locals 2

    .prologue
    .line 641
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 645
    return-void

    .line 642
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 643
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v0

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->setZOrder(I)V

    .line 641
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setStickerMode(ILandroid/graphics/Rect;)V
    .locals 10
    .param p1, "resId"    # I
    .param p2, "targetOrgRoi"    # Landroid/graphics/Rect;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_2

    .line 138
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->destory()V

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 141
    const v0, 0x10001008

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 142
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    .line 143
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 145
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    .line 146
    const/4 v5, 0x2

    .line 147
    const/4 v6, 0x0

    move v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;IIIZ)V

    .line 142
    aput-object v0, v7, v9

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sj, StickerTestView - setStickerMode() - mCurrentStickerCount : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 150
    const-string v1, " / mSticker[mCurrentStickerCount].getZOrder() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getZOrder()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 151
    const-string v1, " / orgRoi : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getOrgDestROI()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 153
    if-eqz p2, :cond_1

    .line 155
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    aget-object v0, v0, v1

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, p2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 156
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 157
    new-instance v3, Landroid/graphics/PointF;

    iget v4, p2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    iget v5, p2, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 158
    new-instance v4, Landroid/graphics/PointF;

    iget v5, p2, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    iget v6, p2, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 159
    new-instance v5, Landroid/graphics/PointF;

    iget v6, p2, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    iget v7, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 160
    new-instance v6, Landroid/graphics/PointF;

    iget v7, p2, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    iget v9, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v9, v9

    invoke-direct {v6, v7, v9}, Landroid/graphics/PointF;-><init>(FF)V

    .line 161
    const/4 v7, 0x0

    .line 155
    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V

    .line 163
    :cond_1
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    .line 164
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    .line 170
    :goto_0
    return-void

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0601d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 168
    .local v8, "str":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/16 v3, 0x8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v8, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setStickerOrdering(I)V
    .locals 3
    .param p1, "curIdx"    # I

    .prologue
    .line 620
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    if-ge v0, v1, :cond_0

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    array-length v2, v2

    if-le v1, v2, :cond_1

    .line 626
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, p1

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->setZOrder(I)V

    .line 627
    return-void

    .line 621
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getZOrder()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getZOrder()I

    move-result v2

    if-le v1, v2, :cond_2

    .line 622
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getZOrder()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->setZOrder(I)V

    .line 623
    :cond_2
    if-ne v0, p1, :cond_3

    .line 620
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public touch(Landroid/view/MotionEvent;Z)V
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "isDrawerOpened"    # Z

    .prologue
    const/4 v10, -0x1

    const/4 v9, 0x6

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 330
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sj, SE - touch() 00 - event.x : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / event.y : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 331
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->transformVirtualPreviewCoordinate(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)V

    .line 334
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v6

    int-to-float v6, v6

    sub-float v3, v5, v6

    .line 335
    .local v3, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v6

    int-to-float v6, v6

    sub-float v4, v5, v6

    .line 336
    .local v4, "y":F
    const/4 v1, 0x1

    .line 337
    .local v1, "ret":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 456
    :goto_0
    return-void

    .line 340
    :pswitch_0
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchPressed:Z

    .line 342
    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    .line 343
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    if-eq v5, v10, :cond_0

    .line 345
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    aget-object v5, v5, v6

    invoke-virtual {v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->InitMoveObject(FF)I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    .line 346
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    if-nez v5, :cond_0

    .line 347
    iput v10, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    .line 350
    :cond_0
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    if-ge v5, v7, :cond_4

    .line 352
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    invoke-interface {v5, v7}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;->setVisibleMenu(Z)V

    .line 354
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v0, v5, -0x1

    .local v0, "i":I
    :goto_1
    if-gez v0, :cond_2

    .line 410
    .end local v0    # "i":I
    :cond_1
    :goto_2
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->setHandlerIconPressed(I)V

    goto :goto_0

    .line 356
    .restart local v0    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v5, v5, v0

    invoke-virtual {v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->InitMoveObject(FF)I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    .line 357
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    if-lt v5, v7, :cond_3

    .line 359
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    .line 361
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->setStickerOrdering(I)V

    .line 363
    if-eqz p2, :cond_1

    .line 364
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;->closeDecorationDrawer()V

    goto :goto_2

    .line 354
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 377
    .end local v0    # "i":I
    :cond_4
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->setStickerOrdering(I)V

    .line 379
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    invoke-interface {v5, v8}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;->setVisibleMenu(Z)V

    .line 381
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    if-ne v5, v9, :cond_5

    .line 383
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mDeletePressed:Z

    goto :goto_2

    .line 388
    :cond_5
    if-eqz p2, :cond_1

    .line 389
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;->closeDecorationDrawer()V

    goto :goto_2

    .line 414
    :pswitch_1
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchPressed:Z

    .line 416
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    if-eq v5, v10, :cond_6

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    if-eq v5, v9, :cond_6

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    aget-object v5, v5, v6

    if-eqz v5, :cond_6

    .line 418
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sj, SE - touch() - event.x : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / event.y : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 419
    const-string v6, " / x : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / y : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 418
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 420
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    invoke-interface {v5, v8}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;->setVisibleMenu(Z)V

    .line 421
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    aget-object v5, v5, v6

    invoke-virtual {v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->StartMoveObject(FF)V

    .line 424
    :cond_6
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    .line 425
    .local v2, "touchType":I
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mDeletePressed:Z

    if-eqz v5, :cond_7

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    if-ltz v5, :cond_7

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    array-length v6, v6

    if-ge v5, v6, :cond_7

    .line 427
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    aget-object v5, v5, v6

    invoke-virtual {v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->InitMoveObject(FF)I

    move-result v2

    .line 428
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sj, SE - touch() - MOVE - touchType : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 429
    if-eq v2, v9, :cond_7

    .line 430
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mDeletePressed:Z

    .line 434
    :cond_7
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->setHandlerIconPressed(I)V

    goto/16 :goto_0

    .line 439
    .end local v2    # "touchType":I
    :pswitch_2
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchPressed:Z

    .line 441
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    invoke-interface {v5, v7}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;->setVisibleMenu(Z)V

    .line 442
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    if-eq v5, v10, :cond_8

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    if-eq v5, v9, :cond_8

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    aget-object v5, v5, v6

    if-eqz v5, :cond_8

    .line 444
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mCurrentStickerIndex:I

    aget-object v5, v5, v6

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->EndMoveObject()V

    .line 447
    :cond_8
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    if-ne v5, v9, :cond_9

    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mDeletePressed:Z

    if-eqz v5, :cond_9

    .line 449
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->deleteSticker()V

    .line 451
    :cond_9
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mDeletePressed:Z

    .line 452
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mTouchType:I

    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->setHandlerIconPressed(I)V

    goto/16 :goto_0

    .line 337
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public updateOriginalBuffer([I)V
    .locals 1
    .param p1, "prevOriginalBuf"    # [I

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([I)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPreviewBuffer()V

    .line 229
    return-void
.end method
