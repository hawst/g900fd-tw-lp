.class public Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
.super Ljava/lang/Object;
.source "ExifTag.java"


# static fields
.field private static final LONG_MAX:J = 0x7fffffffL

.field private static final LONG_MIN:J = -0x80000000L

.field static final SIZE_UNDEFINED:I = 0x0

.field private static final TIME_FORMAT:Ljava/text/SimpleDateFormat;

.field public static final TYPE_ASCII:S = 0x2s

.field public static final TYPE_LONG:S = 0x9s

.field public static final TYPE_RATIONAL:S = 0xas

.field private static final TYPE_TO_SIZE_MAP:[I

.field public static final TYPE_UNDEFINED:S = 0x7s

.field public static final TYPE_UNSIGNED_BYTE:S = 0x1s

.field public static final TYPE_UNSIGNED_LONG:S = 0x4s

.field public static final TYPE_UNSIGNED_RATIONAL:S = 0x5s

.field public static final TYPE_UNSIGNED_SHORT:S = 0x3s

.field private static final UNSIGNED_LONG_MAX:J = 0xffffffffL

.field private static final UNSIGNED_SHORT_MAX:I = 0xffff

.field private static US_ASCII:Ljava/nio/charset/Charset;


# instance fields
.field private mComponentCountActual:I

.field private final mDataType:S

.field private mHasDefinedDefaultComponentCount:Z

.field private mIfd:I

.field private mOffset:I

.field private final mTagId:S

.field private mValue:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x2

    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 54
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->US_ASCII:Ljava/nio/charset/Charset;

    .line 55
    const/16 v0, 0xb

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->TYPE_TO_SIZE_MAP:[I

    .line 62
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->TYPE_TO_SIZE_MAP:[I

    aput v2, v0, v2

    .line 63
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->TYPE_TO_SIZE_MAP:[I

    aput v2, v0, v4

    .line 64
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->TYPE_TO_SIZE_MAP:[I

    const/4 v1, 0x3

    aput v4, v0, v1

    .line 65
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->TYPE_TO_SIZE_MAP:[I

    aput v3, v0, v3

    .line 66
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->TYPE_TO_SIZE_MAP:[I

    const/4 v1, 0x5

    aput v5, v0, v1

    .line 67
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->TYPE_TO_SIZE_MAP:[I

    const/4 v1, 0x7

    aput v2, v0, v1

    .line 68
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->TYPE_TO_SIZE_MAP:[I

    const/16 v1, 0x9

    aput v3, v0, v1

    .line 69
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->TYPE_TO_SIZE_MAP:[I

    const/16 v1, 0xa

    aput v5, v0, v1

    .line 89
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy:MM:dd kk:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->TIME_FORMAT:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method constructor <init>(SSIIZ)V
    .locals 1
    .param p1, "tagId"    # S
    .param p2, "type"    # S
    .param p3, "componentCount"    # I
    .param p4, "ifd"    # I
    .param p5, "hasDefinedComponentCount"    # Z

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-short p1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mTagId:S

    .line 114
    iput-short p2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    .line 115
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    .line 116
    iput-boolean p5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mHasDefinedDefaultComponentCount:Z

    .line 117
    iput p4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mIfd:I

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    .line 119
    return-void
.end method

.method private checkBadComponentCount(I)Z
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 867
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mHasDefinedDefaultComponentCount:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    if-eq v0, p1, :cond_0

    .line 868
    const/4 v0, 0x1

    .line 870
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkOverflowForRational([Lcom/sec/android/mimage/photoretouching/exif/Rational;)Z
    .locals 10
    .param p1, "value"    # [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    .prologue
    const-wide/32 v8, 0x7fffffff

    const-wide/32 v6, -0x80000000

    const/4 v1, 0x0

    .line 935
    array-length v3, p1

    move v2, v1

    :goto_0
    if-lt v2, v3, :cond_0

    .line 942
    :goto_1
    return v1

    .line 935
    :cond_0
    aget-object v0, p1, v2

    .line 936
    .local v0, "v":Lcom/sec/android/mimage/photoretouching/exif/Rational;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/Rational;->getNumerator()J

    move-result-wide v4

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/Rational;->getDenominator()J

    move-result-wide v4

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    .line 937
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/Rational;->getNumerator()J

    move-result-wide v4

    cmp-long v4, v4, v8

    if-gtz v4, :cond_1

    .line 938
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/Rational;->getDenominator()J

    move-result-wide v4

    cmp-long v4, v4, v8

    if-lez v4, :cond_2

    .line 939
    :cond_1
    const/4 v1, 0x1

    goto :goto_1

    .line 935
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private checkOverflowForUnsignedLong([I)Z
    .locals 4
    .param p1, "value"    # [I

    .prologue
    const/4 v1, 0x0

    .line 915
    array-length v3, p1

    move v2, v1

    :goto_0
    if-lt v2, v3, :cond_0

    .line 920
    :goto_1
    return v1

    .line 915
    :cond_0
    aget v0, p1, v2

    .line 916
    .local v0, "v":I
    if-gez v0, :cond_1

    .line 917
    const/4 v1, 0x1

    goto :goto_1

    .line 915
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private checkOverflowForUnsignedLong([J)Z
    .locals 8
    .param p1, "value"    # [J

    .prologue
    const/4 v2, 0x0

    .line 906
    array-length v4, p1

    move v3, v2

    :goto_0
    if-lt v3, v4, :cond_0

    .line 911
    :goto_1
    return v2

    .line 906
    :cond_0
    aget-wide v0, p1, v3

    .line 907
    .local v0, "v":J
    const-wide/16 v6, 0x0

    cmp-long v5, v0, v6

    if-ltz v5, :cond_1

    const-wide v6, 0xffffffffL

    cmp-long v5, v0, v6

    if-lez v5, :cond_2

    .line 908
    :cond_1
    const/4 v2, 0x1

    goto :goto_1

    .line 906
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private checkOverflowForUnsignedRational([Lcom/sec/android/mimage/photoretouching/exif/Rational;)Z
    .locals 10
    .param p1, "value"    # [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    .prologue
    const-wide v8, 0xffffffffL

    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 924
    array-length v3, p1

    move v2, v1

    :goto_0
    if-lt v2, v3, :cond_0

    .line 931
    :goto_1
    return v1

    .line 924
    :cond_0
    aget-object v0, p1, v2

    .line 925
    .local v0, "v":Lcom/sec/android/mimage/photoretouching/exif/Rational;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/Rational;->getNumerator()J

    move-result-wide v4

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/Rational;->getDenominator()J

    move-result-wide v4

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    .line 926
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/Rational;->getNumerator()J

    move-result-wide v4

    cmp-long v4, v4, v8

    if-gtz v4, :cond_1

    .line 927
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/Rational;->getDenominator()J

    move-result-wide v4

    cmp-long v4, v4, v8

    if-lez v4, :cond_2

    .line 928
    :cond_1
    const/4 v1, 0x1

    goto :goto_1

    .line 924
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private checkOverflowForUnsignedShort([I)Z
    .locals 5
    .param p1, "value"    # [I

    .prologue
    const/4 v1, 0x0

    .line 897
    array-length v3, p1

    move v2, v1

    :goto_0
    if-lt v2, v3, :cond_0

    .line 902
    :goto_1
    return v1

    .line 897
    :cond_0
    aget v0, p1, v2

    .line 898
    .local v0, "v":I
    const v4, 0xffff

    if-gt v0, v4, :cond_1

    if-gez v0, :cond_2

    .line 899
    :cond_1
    const/4 v1, 0x1

    goto :goto_1

    .line 897
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private static convertTypeToString(S)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # S

    .prologue
    .line 874
    packed-switch p0, :pswitch_data_0

    .line 892
    :pswitch_0
    const-string v0, ""

    :goto_0
    return-object v0

    .line 876
    :pswitch_1
    const-string v0, "UNSIGNED_BYTE"

    goto :goto_0

    .line 878
    :pswitch_2
    const-string v0, "ASCII"

    goto :goto_0

    .line 880
    :pswitch_3
    const-string v0, "UNSIGNED_SHORT"

    goto :goto_0

    .line 882
    :pswitch_4
    const-string v0, "UNSIGNED_LONG"

    goto :goto_0

    .line 884
    :pswitch_5
    const-string v0, "UNSIGNED_RATIONAL"

    goto :goto_0

    .line 886
    :pswitch_6
    const-string v0, "UNDEFINED"

    goto :goto_0

    .line 888
    :pswitch_7
    const-string v0, "LONG"

    goto :goto_0

    .line 890
    :pswitch_8
    const-string v0, "RATIONAL"

    goto :goto_0

    .line 874
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static getElementSize(S)I
    .locals 1
    .param p0, "type"    # S

    .prologue
    .line 134
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->TYPE_TO_SIZE_MAP:[I

    aget v0, v0, p0

    return v0
.end method

.method public static isValidIfd(I)Z
    .locals 2
    .param p0, "ifdId"    # I

    .prologue
    const/4 v0, 0x1

    .line 95
    if-eqz p0, :cond_0

    if-eq p0, v0, :cond_0

    .line 96
    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    .line 97
    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    .line 95
    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public static isValidType(S)Z
    .locals 2
    .param p0, "type"    # S

    .prologue
    const/4 v0, 0x1

    .line 104
    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    .line 105
    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    .line 106
    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    const/4 v1, 0x7

    if-eq p0, v1, :cond_0

    .line 107
    const/16 v1, 0x9

    if-eq p0, v1, :cond_0

    const/16 v1, 0xa

    if-eq p0, v1, :cond_0

    .line 104
    const/4 v0, 0x0

    :cond_0
    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 947
    if-nez p1, :cond_1

    .line 982
    :cond_0
    :goto_0
    return v1

    .line 950
    :cond_1
    instance-of v2, p1, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 951
    check-cast v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 952
    .local v0, "tag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    iget-short v2, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mTagId:S

    iget-short v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mTagId:S

    if-ne v2, v3, :cond_0

    .line 953
    iget v2, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    if-ne v2, v3, :cond_0

    .line 954
    iget-short v2, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    iget-short v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    if-ne v2, v3, :cond_0

    .line 957
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    if-eqz v2, :cond_5

    .line 958
    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 960
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v2, v2, [J

    if-eqz v2, :cond_2

    .line 961
    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v2, v2, [J

    if-eqz v2, :cond_0

    .line 964
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v1, [J

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v2, [J

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v1

    goto :goto_0

    .line 965
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v2, v2, [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    if-eqz v2, :cond_3

    .line 966
    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v2, v2, [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    if-eqz v2, :cond_0

    .line 969
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v1, [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v2, [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 970
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v2, v2, [B

    if-eqz v2, :cond_4

    .line 971
    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v2, v2, [B

    if-eqz v2, :cond_0

    .line 974
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v1, [B

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v2, [B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    goto :goto_0

    .line 976
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 979
    :cond_5
    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public forceGetValueAsLong(J)J
    .locals 9
    .param p1, "defaultValue"    # J

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 713
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAsLongs()[J

    move-result-object v1

    .line 714
    .local v1, "l":[J
    if-eqz v1, :cond_1

    array-length v3, v1

    if-lt v3, v4, :cond_1

    .line 715
    aget-wide p1, v1, v8

    .line 725
    .end local p1    # "defaultValue":J
    :cond_0
    :goto_0
    return-wide p1

    .line 717
    .restart local p1    # "defaultValue":J
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAsBytes()[B

    move-result-object v0

    .line 718
    .local v0, "b":[B
    if-eqz v0, :cond_2

    array-length v3, v0

    if-lt v3, v4, :cond_2

    .line 719
    aget-byte v3, v0, v8

    int-to-long p1, v3

    goto :goto_0

    .line 721
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAsRationals()[Lcom/sec/android/mimage/photoretouching/exif/Rational;

    move-result-object v2

    .line 722
    .local v2, "r":[Lcom/sec/android/mimage/photoretouching/exif/Rational;
    if-eqz v2, :cond_0

    array-length v3, v2

    if-lt v3, v4, :cond_0

    aget-object v3, v2, v8

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/Rational;->getDenominator()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 723
    aget-object v3, v2, v8

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/Rational;->toDouble()D

    move-result-wide v4

    double-to-long p1, v4

    goto :goto_0
.end method

.method public forceGetValueAsString()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 732
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    if-nez v1, :cond_0

    .line 733
    const-string v1, ""

    .line 758
    :goto_0
    return-object v1

    .line 734
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v1, v1, [B

    if-eqz v1, :cond_2

    .line 735
    iget-short v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 736
    new-instance v2, Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v1, [B

    sget-object v3, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v2, v1, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    move-object v1, v2

    goto :goto_0

    .line 738
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v1, [B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 740
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v1, v1, [J

    if-eqz v1, :cond_4

    .line 741
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v1, [J

    array-length v1, v1

    if-ne v1, v3, :cond_3

    .line 742
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v1, [J

    aget-wide v2, v1, v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 744
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v1, [J

    invoke-static {v1}, Ljava/util/Arrays;->toString([J)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 746
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v1, v1, [Ljava/lang/Object;

    if-eqz v1, :cond_7

    .line 747
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    array-length v1, v1

    if-ne v1, v3, :cond_6

    .line 748
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    aget-object v0, v1, v2

    .line 749
    .local v0, "val":Ljava/lang/Object;
    if-nez v0, :cond_5

    .line 750
    const-string v1, ""

    goto :goto_0

    .line 752
    :cond_5
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 755
    .end local v0    # "val":Ljava/lang/Object;
    :cond_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 758
    :cond_7
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected forceSetComponentCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 198
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    .line 199
    return-void
.end method

.method protected getBytes([B)V
    .locals 2
    .param p1, "buf"    # [B

    .prologue
    .line 821
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getBytes([BII)V

    .line 822
    return-void
.end method

.method protected getBytes([BII)V
    .locals 3
    .param p1, "buf"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 835
    iget-short v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    iget-short v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 836
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get BYTE value from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 837
    iget-short v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->convertTypeToString(S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 836
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 839
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    const/4 v1, 0x0

    .line 840
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    if-le p3, v2, :cond_1

    iget p3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    .line 839
    .end local p3    # "length":I
    :cond_1
    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 841
    return-void
.end method

.method public getComponentCount()I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    return v0
.end method

.method public getDataSize()I
    .locals 2

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataType()S

    move-result v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getElementSize(S)I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method public getDataType()S
    .locals 1

    .prologue
    .line 174
    iget-short v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    return v0
.end method

.method public getIfd()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mIfd:I

    return v0
.end method

.method protected getOffset()I
    .locals 1

    .prologue
    .line 848
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mOffset:I

    return v0
.end method

.method protected getRational(I)Lcom/sec/android/mimage/photoretouching/exif/Rational;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 810
    iget-short v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget-short v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 811
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get RATIONAL value from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 812
    iget-short v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->convertTypeToString(S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 811
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 814
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v0, [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    aget-object v0, v0, p1

    return-object v0
.end method

.method protected getString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 789
    iget-short v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 790
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get ASCII value from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 791
    iget-short v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->convertTypeToString(S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 790
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 793
    :cond_0
    new-instance v1, Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v0, [B

    sget-object v2, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v1
.end method

.method protected getStringByte()[B
    .locals 1

    .prologue
    .line 800
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v0, [B

    return-object v0
.end method

.method public getTagId()S
    .locals 1

    .prologue
    .line 158
    iget-short v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mTagId:S

    return v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 701
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    return-object v0
.end method

.method public getValueAsByte(B)B
    .locals 3
    .param p1, "defaultValue"    # B

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAsBytes()[B

    move-result-object v0

    .line 576
    .local v0, "b":[B
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 579
    .end local p1    # "defaultValue":B
    :cond_0
    :goto_0
    return p1

    .restart local p1    # "defaultValue":B
    :cond_1
    const/4 v1, 0x0

    aget-byte p1, v0, v1

    goto :goto_0
.end method

.method public getValueAsBytes()[B
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_0

    .line 560
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v0, [B

    .line 562
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getValueAsInt(I)I
    .locals 3
    .param p1, "defaultValue"    # I

    .prologue
    .line 659
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAsInts()[I

    move-result-object v0

    .line 660
    .local v0, "i":[I
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 663
    .end local p1    # "defaultValue":I
    :cond_0
    :goto_0
    return p1

    .restart local p1    # "defaultValue":I
    :cond_1
    const/4 v1, 0x0

    aget p1, v0, v1

    goto :goto_0
.end method

.method public getValueAsInts()[I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 636
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    if-nez v3, :cond_1

    .line 646
    :cond_0
    return-object v0

    .line 638
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v3, v3, [J

    if-eqz v3, :cond_0

    .line 639
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v2, [J

    .line 640
    .local v2, "val":[J
    array-length v3, v2

    new-array v0, v3, [I

    .line 641
    .local v0, "arr":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 642
    aget-wide v4, v2, v1

    long-to-int v3, v4

    aput v3, v0, v1

    .line 641
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getValueAsLong(J)J
    .locals 3
    .param p1, "defaultValue"    # J

    .prologue
    .line 690
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAsLongs()[J

    move-result-object v0

    .line 691
    .local v0, "l":[J
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 694
    .end local p1    # "defaultValue":J
    :cond_0
    :goto_0
    return-wide p1

    .restart local p1    # "defaultValue":J
    :cond_1
    const/4 v1, 0x0

    aget-wide p1, v0, v1

    goto :goto_0
.end method

.method public getValueAsLongs()[J
    .locals 1

    .prologue
    .line 674
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v0, v0, [J

    if-eqz v0, :cond_0

    .line 675
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v0, [J

    .line 677
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getValueAsRational(J)Lcom/sec/android/mimage/photoretouching/exif/Rational;
    .locals 5
    .param p1, "defaultValue"    # J

    .prologue
    .line 624
    new-instance v0, Lcom/sec/android/mimage/photoretouching/exif/Rational;

    const-wide/16 v2, 0x1

    invoke-direct {v0, p1, p2, v2, v3}, Lcom/sec/android/mimage/photoretouching/exif/Rational;-><init>(JJ)V

    .line 625
    .local v0, "defaultVal":Lcom/sec/android/mimage/photoretouching/exif/Rational;
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAsRational(Lcom/sec/android/mimage/photoretouching/exif/Rational;)Lcom/sec/android/mimage/photoretouching/exif/Rational;

    move-result-object v1

    return-object v1
.end method

.method public getValueAsRational(Lcom/sec/android/mimage/photoretouching/exif/Rational;)Lcom/sec/android/mimage/photoretouching/exif/Rational;
    .locals 3
    .param p1, "defaultValue"    # Lcom/sec/android/mimage/photoretouching/exif/Rational;

    .prologue
    .line 606
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAsRationals()[Lcom/sec/android/mimage/photoretouching/exif/Rational;

    move-result-object v0

    .line 607
    .local v0, "r":[Lcom/sec/android/mimage/photoretouching/exif/Rational;
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 610
    .end local p1    # "defaultValue":Lcom/sec/android/mimage/photoretouching/exif/Rational;
    :cond_0
    :goto_0
    return-object p1

    .restart local p1    # "defaultValue":Lcom/sec/android/mimage/photoretouching/exif/Rational;
    :cond_1
    const/4 v1, 0x0

    aget-object p1, v0, v1

    goto :goto_0
.end method

.method public getValueAsRationals()[Lcom/sec/android/mimage/photoretouching/exif/Rational;
    .locals 1

    .prologue
    .line 590
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v0, v0, [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    if-eqz v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v0, [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    .line 593
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getValueAsString()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 525
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 532
    :cond_0
    :goto_0
    return-object v0

    .line 527
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 528
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 529
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v1, v1, [B

    if-eqz v1, :cond_0

    .line 530
    new-instance v1, Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v0, [B

    sget-object v2, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public getValueAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 544
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAsString()Ljava/lang/String;

    move-result-object v0

    .line 545
    .local v0, "s":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 548
    .end local p1    # "defaultValue":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "defaultValue":Ljava/lang/String;
    :cond_0
    move-object p1, v0

    goto :goto_0
.end method

.method protected getValueAt(I)J
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 773
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v0, v0, [J

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v0, [J

    aget-wide v0, v0, p1

    .line 776
    :goto_0
    return-wide v0

    .line 775
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_1

    .line 776
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    check-cast v0, [B

    aget-byte v0, v0, p1

    int-to-long v0, v0

    goto :goto_0

    .line 778
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get integer value from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 779
    iget-short v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->convertTypeToString(S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 778
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected hasDefinedCount()Z
    .locals 1

    .prologue
    .line 863
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mHasDefinedDefaultComponentCount:Z

    return v0
.end method

.method public hasValue()Z
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setHasDefinedCount(Z)V
    .locals 0
    .param p1, "d"    # Z

    .prologue
    .line 859
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mHasDefinedDefaultComponentCount:Z

    .line 860
    return-void
.end method

.method protected setIfd(I)V
    .locals 0
    .param p1, "ifdId"    # I

    .prologue
    .line 151
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mIfd:I

    .line 152
    return-void
.end method

.method protected setOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 855
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mOffset:I

    .line 856
    return-void
.end method

.method public setTimeValue(J)Z
    .locals 3
    .param p1, "time"    # J

    .prologue
    .line 512
    sget-object v1, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->TIME_FORMAT:Ljava/text/SimpleDateFormat;

    monitor-enter v1

    .line 513
    :try_start_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->TIME_FORMAT:Ljava/text/SimpleDateFormat;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(Ljava/lang/String;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 512
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setValue(B)Z
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 426
    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    .line 427
    aput-byte p1, v0, v1

    .line 426
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([B)Z

    move-result v0

    return v0
.end method

.method public setValue(I)Z
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 255
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    .line 256
    aput p1, v0, v1

    .line 255
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([I)Z

    move-result v0

    return v0
.end method

.method public setValue(J)Z
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 292
    const/4 v0, 0x1

    new-array v0, v0, [J

    const/4 v1, 0x0

    .line 293
    aput-wide p1, v0, v1

    .line 292
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([J)Z

    move-result v0

    return v0
.end method

.method public setValue(Lcom/sec/android/mimage/photoretouching/exif/Rational;)Z
    .locals 2
    .param p1, "value"    # Lcom/sec/android/mimage/photoretouching/exif/Rational;

    .prologue
    .line 379
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    const/4 v1, 0x0

    .line 380
    aput-object p1, v0, v1

    .line 379
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([Lcom/sec/android/mimage/photoretouching/exif/Rational;)Z

    move-result v0

    return v0
.end method

.method public setValue(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const v5, 0xffff

    const/4 v4, 0x0

    .line 442
    if-nez p1, :cond_1

    .line 497
    .end local p1    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v4

    .line 444
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    instance-of v3, p1, Ljava/lang/Short;

    if-eqz v3, :cond_2

    .line 445
    check-cast p1, Ljava/lang/Short;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Short;->shortValue()S

    move-result v3

    and-int/2addr v3, v5

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(I)Z

    move-result v4

    goto :goto_0

    .line 446
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_2
    instance-of v3, p1, Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 447
    check-cast p1, Ljava/lang/String;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(Ljava/lang/String;)Z

    move-result v4

    goto :goto_0

    .line 448
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_3
    instance-of v3, p1, [I

    if-eqz v3, :cond_4

    .line 449
    check-cast p1, [I

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([I)Z

    move-result v4

    goto :goto_0

    .line 450
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_4
    instance-of v3, p1, [J

    if-eqz v3, :cond_5

    .line 451
    check-cast p1, [J

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([J)Z

    move-result v4

    goto :goto_0

    .line 452
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_5
    instance-of v3, p1, Lcom/sec/android/mimage/photoretouching/exif/Rational;

    if-eqz v3, :cond_6

    .line 453
    check-cast p1, Lcom/sec/android/mimage/photoretouching/exif/Rational;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(Lcom/sec/android/mimage/photoretouching/exif/Rational;)Z

    move-result v4

    goto :goto_0

    .line 454
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_6
    instance-of v3, p1, [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    if-eqz v3, :cond_7

    .line 455
    check-cast p1, [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([Lcom/sec/android/mimage/photoretouching/exif/Rational;)Z

    move-result v4

    goto :goto_0

    .line 456
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_7
    instance-of v3, p1, [B

    if-eqz v3, :cond_8

    .line 457
    check-cast p1, [B

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([B)Z

    move-result v4

    goto :goto_0

    .line 458
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_8
    instance-of v3, p1, Ljava/lang/Integer;

    if-eqz v3, :cond_9

    .line 459
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(I)Z

    move-result v4

    goto :goto_0

    .line 460
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_9
    instance-of v3, p1, Ljava/lang/Long;

    if-eqz v3, :cond_a

    .line 461
    check-cast p1, Ljava/lang/Long;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(J)Z

    move-result v4

    goto :goto_0

    .line 462
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_a
    instance-of v3, p1, Ljava/lang/Byte;

    if-eqz v3, :cond_b

    .line 463
    check-cast p1, Ljava/lang/Byte;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(B)Z

    move-result v4

    goto :goto_0

    .line 464
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_b
    instance-of v3, p1, [Ljava/lang/Short;

    if-eqz v3, :cond_e

    move-object v0, p1

    .line 466
    check-cast v0, [Ljava/lang/Short;

    .line 467
    .local v0, "arr":[Ljava/lang/Short;
    array-length v3, v0

    new-array v1, v3, [I

    .line 468
    .local v1, "fin":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_c

    .line 471
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([I)Z

    move-result v4

    goto/16 :goto_0

    .line 469
    :cond_c
    aget-object v3, v0, v2

    if-nez v3, :cond_d

    move v3, v4

    :goto_2
    aput v3, v1, v2

    .line 468
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 469
    :cond_d
    aget-object v3, v0, v2

    invoke-virtual {v3}, Ljava/lang/Short;->shortValue()S

    move-result v3

    and-int/2addr v3, v5

    goto :goto_2

    .line 472
    .end local v0    # "arr":[Ljava/lang/Short;
    .end local v1    # "fin":[I
    .end local v2    # "i":I
    :cond_e
    instance-of v3, p1, [Ljava/lang/Integer;

    if-eqz v3, :cond_11

    move-object v0, p1

    .line 474
    check-cast v0, [Ljava/lang/Integer;

    .line 475
    .local v0, "arr":[Ljava/lang/Integer;
    array-length v3, v0

    new-array v1, v3, [I

    .line 476
    .restart local v1    # "fin":[I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    array-length v3, v0

    if-lt v2, v3, :cond_f

    .line 479
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([I)Z

    move-result v4

    goto/16 :goto_0

    .line 477
    :cond_f
    aget-object v3, v0, v2

    if-nez v3, :cond_10

    move v3, v4

    :goto_4
    aput v3, v1, v2

    .line 476
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 477
    :cond_10
    aget-object v3, v0, v2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    goto :goto_4

    .line 480
    .end local v0    # "arr":[Ljava/lang/Integer;
    .end local v1    # "fin":[I
    .end local v2    # "i":I
    :cond_11
    instance-of v3, p1, [Ljava/lang/Long;

    if-eqz v3, :cond_14

    move-object v0, p1

    .line 482
    check-cast v0, [Ljava/lang/Long;

    .line 483
    .local v0, "arr":[Ljava/lang/Long;
    array-length v3, v0

    new-array v1, v3, [J

    .line 484
    .local v1, "fin":[J
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_5
    array-length v3, v0

    if-lt v2, v3, :cond_12

    .line 487
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([J)Z

    move-result v4

    goto/16 :goto_0

    .line 485
    :cond_12
    aget-object v3, v0, v2

    if-nez v3, :cond_13

    const-wide/16 v4, 0x0

    :goto_6
    aput-wide v4, v1, v2

    .line 484
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 485
    :cond_13
    aget-object v3, v0, v2

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    goto :goto_6

    .line 488
    .end local v0    # "arr":[Ljava/lang/Long;
    .end local v1    # "fin":[J
    .end local v2    # "i":I
    :cond_14
    instance-of v3, p1, [Ljava/lang/Byte;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 490
    check-cast v0, [Ljava/lang/Byte;

    .line 491
    .local v0, "arr":[Ljava/lang/Byte;
    array-length v3, v0

    new-array v1, v3, [B

    .line 492
    .local v1, "fin":[B
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_7
    array-length v3, v0

    if-lt v2, v3, :cond_15

    .line 495
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([B)Z

    move-result v4

    goto/16 :goto_0

    .line 493
    :cond_15
    aget-object v3, v0, v2

    if-nez v3, :cond_16

    move v3, v4

    :goto_8
    aput-byte v3, v1, v2

    .line 492
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 493
    :cond_16
    aget-object v3, v0, v2

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    goto :goto_8
.end method

.method public setValue(Ljava/lang/String;)Z
    .locals 8
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x7

    const/4 v6, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 312
    iget-short v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    if-eq v5, v6, :cond_1

    iget-short v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    if-eq v5, v7, :cond_1

    .line 330
    :cond_0
    :goto_0
    return v3

    .line 316
    :cond_1
    sget-object v5, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v5}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 317
    .local v0, "buf":[B
    move-object v2, v0

    .line 318
    .local v2, "finalBuf":[B
    array-length v5, v0

    if-lez v5, :cond_5

    .line 319
    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    aget-byte v5, v0, v5

    if-eqz v5, :cond_2

    iget-short v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    if-ne v5, v7, :cond_4

    :cond_2
    move-object v2, v0

    .line 324
    :cond_3
    :goto_1
    array-length v1, v2

    .line 325
    .local v1, "count":I
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->checkBadComponentCount(I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 328
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    .line 329
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    move v3, v4

    .line 330
    goto :goto_0

    .line 320
    .end local v1    # "count":I
    :cond_4
    array-length v5, v0

    add-int/lit8 v5, v5, 0x1

    invoke-static {v0, v5}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    goto :goto_1

    .line 321
    :cond_5
    iget-short v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    if-ne v5, v6, :cond_3

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    if-ne v5, v4, :cond_3

    .line 322
    new-array v2, v4, [B

    goto :goto_1
.end method

.method public setValue([B)Z
    .locals 2
    .param p1, "value"    # [B

    .prologue
    .line 412
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([BII)Z

    move-result v0

    return v0
.end method

.method public setValue([BII)Z
    .locals 4
    .param p1, "value"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 396
    invoke-direct {p0, p3}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->checkBadComponentCount(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 405
    :cond_0
    :goto_0
    return v0

    .line 399
    :cond_1
    iget-short v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    if-eq v2, v1, :cond_2

    iget-short v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    const/4 v3, 0x7

    if-ne v2, v3, :cond_0

    .line 402
    :cond_2
    new-array v2, p3, [B

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    .line 403
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    invoke-static {p1, p2, v2, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 404
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    move v0, v1

    .line 405
    goto :goto_0
.end method

.method public setValue([I)Z
    .locals 7
    .param p1, "value"    # [I

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v2, 0x0

    .line 221
    array-length v3, p1

    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->checkBadComponentCount(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 240
    :cond_0
    :goto_0
    return v2

    .line 224
    :cond_1
    iget-short v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    if-eq v3, v5, :cond_2

    iget-short v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    const/16 v4, 0x9

    if-eq v3, v4, :cond_2

    .line 225
    iget-short v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    if-ne v3, v6, :cond_0

    .line 228
    :cond_2
    iget-short v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    if-ne v3, v5, :cond_3

    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->checkOverflowForUnsignedShort([I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 230
    :cond_3
    iget-short v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    if-ne v3, v6, :cond_4

    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->checkOverflowForUnsignedLong([I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 234
    :cond_4
    array-length v2, p1

    new-array v0, v2, [J

    .line 235
    .local v0, "data":[J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v2, p1

    if-lt v1, v2, :cond_5

    .line 238
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    .line 239
    array-length v2, p1

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    .line 240
    const/4 v2, 0x1

    goto :goto_0

    .line 236
    :cond_5
    aget v2, p1, v1

    int-to-long v2, v2

    aput-wide v2, v0, v1

    .line 235
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public setValue([J)Z
    .locals 3
    .param p1, "value"    # [J

    .prologue
    const/4 v0, 0x0

    .line 271
    array-length v1, p1

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->checkBadComponentCount(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-short v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    .line 279
    :cond_0
    :goto_0
    return v0

    .line 274
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->checkOverflowForUnsignedLong([J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 277
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    .line 278
    array-length v0, p1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    .line 279
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setValue([Lcom/sec/android/mimage/photoretouching/exif/Rational;)Z
    .locals 4
    .param p1, "value"    # [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    .prologue
    const/16 v3, 0xa

    const/4 v2, 0x5

    const/4 v0, 0x0

    .line 348
    array-length v1, p1

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->checkBadComponentCount(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 362
    :cond_0
    :goto_0
    return v0

    .line 351
    :cond_1
    iget-short v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    if-eq v1, v2, :cond_2

    iget-short v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    if-ne v1, v3, :cond_0

    .line 354
    :cond_2
    iget-short v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    if-ne v1, v2, :cond_3

    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->checkOverflowForUnsignedRational([Lcom/sec/android/mimage/photoretouching/exif/Rational;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 356
    :cond_3
    iget-short v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    if-ne v1, v3, :cond_4

    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->checkOverflowForRational([Lcom/sec/android/mimage/photoretouching/exif/Rational;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 360
    :cond_4
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mValue:Ljava/lang/Object;

    .line 361
    array-length v0, p1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    .line 362
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 987
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "tag id: %04X\n"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-short v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mTagId:S

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "ifd id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mIfd:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\ntype: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 988
    iget-short v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mDataType:S

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->convertTypeToString(S)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\ncount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mComponentCountActual:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 989
    const-string v1, "\noffset: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->mOffset:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nvalue: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->forceGetValueAsString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 987
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
