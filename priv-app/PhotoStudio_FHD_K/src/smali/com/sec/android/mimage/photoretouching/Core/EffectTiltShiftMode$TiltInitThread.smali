.class Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;
.super Ljava/lang/Thread;
.source "EffectTiltShiftMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TiltInitThread"
.end annotation


# instance fields
.field isExit:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 371
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 370
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->isExit:Z

    .line 372
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->setDaemon(Z)V

    .line 373
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSTouchState:I

    .line 374
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isTiltFirstTouch:Z

    .line 375
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isTSWorking:Z

    .line 376
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->showOriginal:Z

    .line 377
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isConfigurationChange:Z

    .line 378
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 382
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    iput-boolean v3, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isTSWorking:Z

    .line 387
    const-wide/16 v0, 0xbb8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 391
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->isExit:Z

    if-eqz v0, :cond_1

    .line 402
    :cond_0
    :goto_1
    return-void

    .line 395
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    const/4 v1, 0x3

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSTouchState:I

    .line 396
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # invokes: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->changeTiltShiftBuffer(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$1(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;Z)V

    .line 397
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->invalidateWithThread()V

    .line 398
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 399
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isTSWorking:Z

    .line 400
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$3(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    goto :goto_1

    .line 389
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public workStop()V
    .locals 2

    .prologue
    .line 405
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->isExit:Z

    .line 406
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isTSWorking:Z

    .line 407
    monitor-enter p0

    .line 408
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 407
    monitor-exit p0

    .line 410
    return-void

    .line 407
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
