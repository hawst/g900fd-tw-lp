.class Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$7;
.super Ljava/lang/Object;
.source "AssistantLayoutManager.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setSeekbarListener(ILcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

.field private final synthetic val$seekbar_listener2:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$7;->val$seekbar_listener2:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 841
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$7;->val$seekbar_listener2:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    invoke-interface {v0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;->onMyProgressChanged(Landroid/widget/SeekBar;IZ)V

    .line 843
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 834
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$7;->val$seekbar_listener2:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 835
    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;->onMyStartTrackingTouch(Landroid/widget/SeekBar;)V

    .line 836
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 828
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$7;->val$seekbar_listener2:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 829
    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;->onMyStopTrackingTouch(Landroid/widget/SeekBar;)V

    .line 830
    return-void
.end method
