.class Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;
.super Ljava/lang/Object;
.source "EffectView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->init2DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    .line 1057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 6
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 1077
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$38(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Z)V

    .line 1078
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getButtonPosition(I)Landroid/graphics/Point;

    move-result-object v0

    .line 1079
    .local v0, "point":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f06000d

    iget v4, v0, Landroid/graphics/Point;->x:I

    iget v5, v0, Landroid/graphics/Point;->y:I

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, 0x2

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastActionbar(Landroid/content/Context;III)V

    .line 1081
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "vibrator"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    .line 1082
    .local v1, "vibrator":Landroid/os/Vibrator;
    const-wide/16 v2, 0xd

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 1083
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$38(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Z)V

    .line 1084
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x9

    .line 1061
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mLongpressButton:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$39(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1062
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$41(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;I)V

    .line 1067
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    const v1, 0x7a1200

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$42(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;I)V

    .line 1068
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1069
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1071
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$38(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Z)V

    .line 1072
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1090
    return-void
.end method
