.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;
.super Ljava/lang/Object;
.source "PenActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->createManager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    .line 367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onModeChanged(Z)V
    .locals 5
    .param p1, "isMultiwindow"    # Z

    .prologue
    .line 410
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$19(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v1

    .line 411
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 412
    .local v2, "width":I
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 413
    .local v0, "height":I
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onModeChanged mSMultiWindowActivity.isMultiWindow():"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$19(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 414
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$19(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 416
    add-int/lit8 v0, v0, -0x19

    .line 424
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v3, v2, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setCanvasSize(II)V

    .line 426
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->fitPopupIntoMotherLayout(II)V
    invoke-static {v3, v2, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$0(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;II)V

    .line 435
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->fitSPenIntoMotherLayout()V
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$20(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 436
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$21(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 438
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$22(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 439
    const-string v3, "onModeChanged"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 440
    return-void

    .line 420
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v2, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 421
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_0
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 5
    .param p1, "curWindowRect"    # Landroid/graphics/Rect;

    .prologue
    .line 390
    move-object v1, p1

    .line 391
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 392
    .local v2, "width":I
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 393
    .local v0, "height":I
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onSizeChanged mSMultiWindowActivity.isMultiWindow():"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$19(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 394
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$19(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 396
    add-int/lit8 v0, v0, -0x19

    .line 399
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v3, v2, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setCanvasSize(II)V

    .line 400
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->fitPopupIntoMotherLayout(II)V
    invoke-static {v3, v2, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$0(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;II)V

    .line 401
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->fitSPenIntoMotherLayout()V
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$20(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 402
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$21(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 404
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$22(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 405
    const-string v3, "onSizeChanged"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 406
    return-void
.end method

.method public onZoneChanged(I)V
    .locals 5
    .param p1, "zone"    # I

    .prologue
    .line 371
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$19(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v1

    .line 372
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 373
    .local v2, "width":I
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 374
    .local v0, "height":I
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onZoneChanged mSMultiWindowActivity.isMultiWindow():"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$19(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 375
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$19(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 377
    add-int/lit8 v0, v0, -0x19

    .line 380
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v3, v2, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setCanvasSize(II)V

    .line 381
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->fitPopupIntoMotherLayout(II)V
    invoke-static {v3, v2, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$0(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;II)V

    .line 382
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->fitSPenIntoMotherLayout()V
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$20(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 383
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$21(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 385
    const-string v3, "onZoneChanged"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 386
    return-void
.end method
