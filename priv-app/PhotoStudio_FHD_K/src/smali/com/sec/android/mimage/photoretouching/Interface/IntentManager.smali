.class public Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;
.super Ljava/lang/Object;
.source "IntentManager.java"


# static fields
.field public static final REQUEST_CODE_CAMERA_OPEN:I = 0x30000

.field public static final REQUEST_CODE_EFFECTMANAGER:I = 0x80000

.field public static final REQUEST_CODE_GALLERY_OPEN:I = 0x20000

.field public static final REQUEST_CODE_PEN:I = 0x50000

.field public static final REQUEST_CODE_PHOTORETOUCHING:I = 0x60000

.field public static final REQUEST_CODE_PICK_IMAGE_MULTIGRID:I = 0x70000

.field public static final REQUEST_CODE_TOOL_CAMERA:I = 0x10001

.field public static final REQUEST_CODE_TOOL_GALLERY:I = 0x10000

.field public static final REQUEST_CODE_TRAY_GALLERY_OPEN:I = 0x40000

.field public static final REQUEST_CODE_TRAY_GALLERY_OPEN_MULTIPLE:I = 0x40001

.field public static final RESULT_PEN_DONE_OK:I = 0x50002

.field public static final RESULT_PEN_SAVE_OK:I = 0x50001

.field public static mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

.field private static mFileNameFromCamera:Ljava/lang/String;

.field private static mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 634
    sput-object v0, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    .line 636
    sput-object v0, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    .line 642
    sput-object v0, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mFileNameFromCamera:Ljava/lang/String;

    .line 654
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static activityResult(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;Ljava/util/ArrayList;IILandroid/content/Intent;)V
    .locals 28
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p2, "srcItemList"    # Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    .param p4, "requestCode"    # I
    .param p5, "resultCode"    # I
    .param p6, "data"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;",
            "Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;",
            ">;II",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    .prologue
    .line 131
    .local p3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cheus, IntentManager : activityResult : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 132
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sj, IntentManager - activityResult() : requestCode : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / resultCode : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 133
    sparse-switch p4, :sswitch_data_0

    .line 544
    .end local p0    # "context":Landroid/content/Context;
    :cond_0
    :goto_0
    return-void

    .line 136
    .restart local p0    # "context":Landroid/content/Context;
    :sswitch_0
    const/4 v5, -0x1

    move/from16 v0, p5

    if-ne v0, v5, :cond_3

    .line 138
    if-eqz p6, :cond_2

    if-eqz p1, :cond_2

    .line 140
    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 141
    .local v18, "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    const/4 v5, 0x1

    move-object/from16 v0, v18

    iput-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 142
    invoke-virtual/range {p6 .. p6}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 144
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    const-string v6, "content"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 146
    const/4 v5, 0x0

    move-object/from16 v0, v18

    iput-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 147
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 153
    :goto_1
    new-instance v9, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 156
    const/4 v5, 0x4

    .line 153
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    invoke-direct {v9, v0, v1, v2, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;I)V

    .line 157
    .local v9, "decode":Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->start()V

    goto :goto_0

    .line 151
    .end local v9    # "decode":Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    :cond_1
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    goto :goto_1

    .line 161
    .end local v18    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    :cond_2
    const-string v5, "REQUEST_CODE_GALLERY data null"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_0

    .line 166
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "REQUEST_CODE_GALLERY result not ok : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_0

    .line 170
    :sswitch_1
    const/4 v5, -0x1

    move/from16 v0, p5

    if-ne v0, v5, :cond_5

    .line 172
    sget-object v5, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mFileNameFromCamera:Ljava/lang/String;

    if-eqz v5, :cond_4

    if-eqz p1, :cond_4

    .line 174
    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 175
    .restart local v18    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    const/4 v5, 0x1

    move-object/from16 v0, v18

    iput-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isCamera:Z

    .line 176
    const/4 v5, 0x0

    move-object/from16 v0, v18

    iput-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 177
    new-instance v5, Ljava/lang/StringBuilder;

    sget-object v6, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mFileNameFromCamera:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 178
    sget-object v5, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mFileNameFromCamera:Ljava/lang/String;

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->fileName:Ljava/lang/String;

    .line 180
    new-instance v9, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 183
    const/4 v5, 0x4

    .line 180
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    invoke-direct {v9, v0, v1, v2, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;I)V

    .line 184
    .restart local v9    # "decode":Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->start()V

    goto/16 :goto_0

    .line 188
    .end local v9    # "decode":Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    .end local v18    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    :cond_4
    const-string v5, "REQUEST_CODE_CAMERA data null"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 193
    :cond_5
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "REQUEST_CODE_CAMERA result not ok : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 197
    :sswitch_2
    const/4 v5, -0x1

    move/from16 v0, p5

    if-ne v0, v5, :cond_9

    .line 199
    if-eqz p6, :cond_8

    .line 201
    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 202
    .restart local v18    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    const/4 v5, 0x1

    move-object/from16 v0, v18

    iput-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 203
    invoke-virtual/range {p6 .. p6}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 204
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    if-nez v5, :cond_6

    .line 206
    const/4 v5, 0x0

    move-object/from16 v0, v18

    iput-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 226
    :goto_2
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    goto/16 :goto_0

    .line 210
    :cond_6
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    const-string v6, "content"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 212
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 219
    :goto_3
    const-string v5, "activityResult REQUEST_CODE_TOOL_GALLERY"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 220
    new-instance v9, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 222
    sget-object v5, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    .line 223
    const/4 v6, 0x1

    .line 220
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v9, v0, v1, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;I)V

    .line 224
    .restart local v9    # "decode":Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->start()V

    goto :goto_2

    .line 216
    .end local v9    # "decode":Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    :cond_7
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    goto :goto_3

    .line 230
    .end local v18    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    :cond_8
    const-string v5, "REQUEST_CODE_GALLERY data null"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 235
    :cond_9
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "REQUEST_CODE_TOOL_GALLERY result not ok : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 236
    move-object/from16 v26, p0

    .line 238
    .local v26, "tmpContext":Landroid/content/Context;
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    new-instance v6, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager$1;

    move-object/from16 v0, v26

    invoke-direct {v6, v0}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager$1;-><init>(Landroid/content/Context;)V

    .line 244
    const-wide/16 v10, 0x12c

    .line 238
    invoke-virtual {v5, v6, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 250
    .end local v26    # "tmpContext":Landroid/content/Context;
    :sswitch_3
    const/4 v5, -0x1

    move/from16 v0, p5

    if-ne v0, v5, :cond_b

    .line 252
    sget-object v5, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mFileNameFromCamera:Ljava/lang/String;

    if-eqz v5, :cond_a

    if-eqz p1, :cond_a

    .line 254
    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 255
    .restart local v18    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    const/4 v5, 0x1

    move-object/from16 v0, v18

    iput-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isCamera:Z

    .line 256
    const/4 v5, 0x0

    move-object/from16 v0, v18

    iput-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 257
    new-instance v5, Ljava/lang/StringBuilder;

    sget-object v6, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mFileNameFromCamera:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 258
    sget-object v5, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mFileNameFromCamera:Ljava/lang/String;

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->fileName:Ljava/lang/String;

    .line 260
    new-instance v9, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 262
    sget-object v5, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    .line 263
    const/4 v6, 0x1

    .line 260
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v9, v0, v1, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;I)V

    .line 264
    .restart local v9    # "decode":Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->start()V

    goto/16 :goto_0

    .line 268
    .end local v9    # "decode":Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    .end local v18    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    :cond_a
    const-string v5, "REQUEST_CODE_CAMERA data null"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 273
    :cond_b
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "REQUEST_CODE_TOOL_GALLERY result not ok : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 277
    :sswitch_4
    const/4 v5, -0x1

    move/from16 v0, p5

    if-ne v0, v5, :cond_f

    .line 279
    if-eqz p6, :cond_e

    if-eqz p1, :cond_e

    .line 281
    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 282
    .restart local v18    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    const/4 v5, 0x1

    move-object/from16 v0, v18

    iput-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 283
    invoke-virtual/range {p6 .. p6}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 284
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->contain(Landroid/net/Uri;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 286
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    const-string v6, "content"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 288
    const/4 v5, 0x0

    move-object/from16 v0, v18

    iput-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 289
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 295
    :goto_4
    const/4 v5, 0x3

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->newImage(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V

    .line 296
    new-instance v9, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 299
    const/4 v5, 0x3

    .line 296
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    invoke-direct {v9, v0, v1, v2, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;I)V

    .line 300
    .restart local v9    # "decode":Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->start()V

    goto/16 :goto_0

    .line 293
    .end local v9    # "decode":Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    :cond_c
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    goto :goto_4

    .line 304
    :cond_d
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton()V

    goto/16 :goto_0

    .line 309
    .end local v18    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    :cond_e
    const-string v5, "REQUEST_CODE_GALLERY data null"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 310
    if-eqz p1, :cond_0

    .line 311
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton()V

    goto/16 :goto_0

    .line 316
    :cond_f
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "REQUEST_CODE_TOOL_GALLERY result not ok : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 317
    if-eqz p1, :cond_0

    .line 318
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton()V

    goto/16 :goto_0

    .line 322
    :sswitch_5
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 323
    .local v23, "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;>;"
    const/4 v5, -0x1

    move/from16 v0, p5

    if-ne v0, v5, :cond_14

    .line 325
    invoke-virtual/range {p6 .. p6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v15

    .line 326
    .local v15, "bundle":Landroid/os/Bundle;
    const-string v5, "selectedItems"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v27

    .line 328
    .local v27, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v27, :cond_13

    if-eqz p1, :cond_13

    .line 330
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_5
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v5

    move/from16 v0, v20

    if-lt v0, v5, :cond_10

    .line 358
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 360
    new-instance v9, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 363
    const/4 v5, 0x3

    .line 360
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, p1

    invoke-direct {v9, v0, v1, v2, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;I)V

    .line 364
    .restart local v9    # "decode":Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->start()V

    goto/16 :goto_0

    .line 332
    .end local v9    # "decode":Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    :cond_10
    new-instance v18, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    invoke-direct/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;-><init>()V

    .line 334
    .restart local v18    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    const/4 v5, 0x1

    move-object/from16 v0, v18

    iput-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 335
    move-object/from16 v0, v27

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 337
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->contain(Landroid/net/Uri;)Z

    move-result v5

    if-nez v5, :cond_12

    .line 345
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    const-string v6, "content"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 346
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 350
    :goto_6
    const/4 v5, 0x3

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->newImage(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V

    .line 351
    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 330
    :goto_7
    add-int/lit8 v20, v20, 0x1

    goto :goto_5

    .line 348
    :cond_11
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    goto :goto_6

    .line 355
    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton()V

    goto :goto_7

    .line 369
    .end local v18    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .end local v20    # "i":I
    :cond_13
    const-string v5, "REQUEST_CODE_GALLERY data null"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 370
    if-eqz p1, :cond_0

    .line 371
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton()V

    goto/16 :goto_0

    .line 376
    .end local v15    # "bundle":Landroid/os/Bundle;
    .end local v27    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_14
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "REQUEST_CODE_TOOL_GALLERY result not ok : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 377
    if-eqz p1, :cond_0

    .line 378
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton()V

    goto/16 :goto_0

    .line 382
    .end local v23    # "infoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;>;"
    :sswitch_6
    if-eqz p1, :cond_0

    .line 383
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v21

    .line 385
    .local v21, "imageData":Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    if-eqz v21, :cond_0

    .line 388
    const v5, 0x50002

    move/from16 v0, p5

    if-ne v0, v5, :cond_15

    .line 390
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v7

    const/4 v10, 0x0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v11

    array-length v11, v11

    invoke-static {v5, v6, v7, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 391
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v4

    .line 393
    .local v4, "hManager":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    new-instance v24, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 394
    .local v24, "peneffect":Lcom/sec/android/mimage/photoretouching/Core/PenEffect;
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    move-object/from16 v0, v24

    invoke-direct {v8, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/PenEffect;)V

    .line 396
    .local v8, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v5

    .line 397
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 398
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 399
    sget-boolean v9, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->enhanceFlag:Z

    .line 396
    invoke-virtual/range {v4 .. v9}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([IIILcom/sec/android/mimage/photoretouching/Core/EffectInfo;Z)V

    .line 401
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateDrawing()V

    .line 403
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/CommonInputData;->removeData([I)V

    .line 404
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    .line 405
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    .line 404
    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 407
    move-object/from16 v0, p0

    instance-of v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v5, :cond_0

    .line 408
    check-cast p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->refreshView()V

    goto/16 :goto_0

    .line 411
    .end local v4    # "hManager":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .end local v8    # "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    .end local v24    # "peneffect":Lcom/sec/android/mimage/photoretouching/Core/PenEffect;
    .restart local p0    # "context":Landroid/content/Context;
    :cond_15
    if-nez p5, :cond_16

    .line 413
    move-object/from16 v0, p0

    instance-of v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v5, :cond_0

    .line 414
    check-cast p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->refreshView()V

    goto/16 :goto_0

    .line 422
    .restart local p0    # "context":Landroid/content/Context;
    :cond_16
    const-string v5, "ysjeong, else, IntentManager"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 423
    if-eqz v21, :cond_17

    .line 424
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/CommonInputData;->removeData([I)V

    .line 426
    :cond_17
    move-object/from16 v0, p0

    instance-of v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v5, :cond_0

    .line 427
    check-cast p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->refreshView()V

    goto/16 :goto_0

    .line 434
    .end local v21    # "imageData":Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .restart local p0    # "context":Landroid/content/Context;
    :sswitch_7
    const/4 v5, -0x1

    move/from16 v0, p5

    if-ne v0, v5, :cond_1e

    .line 436
    if-eqz p6, :cond_1d

    .line 439
    invoke-virtual/range {p6 .. p6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v15

    .line 440
    .restart local v15    # "bundle":Landroid/os/Bundle;
    const-string v5, "selectedItems"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v25

    .line 441
    .local v25, "selectedUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 442
    .local v19, "decodeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;>;"
    const-string v5, "selectedCount"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v16

    .line 444
    .local v16, "count":I
    const/16 v22, 0x0

    .local v22, "index":I
    :goto_8
    move/from16 v0, v22

    move/from16 v1, v16

    if-lt v0, v1, :cond_19

    .line 493
    move-object/from16 v17, p0

    .line 494
    .local v17, "ctx":Landroid/content/Context;
    sget-object v5, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    if-nez v5, :cond_18

    .line 495
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager$2;

    move-object/from16 v0, v17

    invoke-direct {v5, v0}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager$2;-><init>(Landroid/content/Context;)V

    sput-object v5, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    .line 503
    :cond_18
    new-instance v9, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 506
    sget-object v13, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    .line 507
    const/4 v14, 0x7

    move-object/from16 v10, p0

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    .line 503
    invoke-direct/range {v9 .. v14}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;I)V

    .line 508
    .restart local v9    # "decode":Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager$3;

    move-object/from16 v0, v17

    invoke-direct {v5, v0}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager$3;-><init>(Landroid/content/Context;)V

    invoke-virtual {v9, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->setChangeViewCallback(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;)V

    .line 519
    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 520
    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->start()V

    .line 523
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    goto/16 :goto_0

    .line 447
    .end local v9    # "decode":Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    .end local v17    # "ctx":Landroid/content/Context;
    :cond_19
    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 448
    .restart local v18    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    if-eqz v18, :cond_1a

    .line 450
    const/4 v5, 0x1

    move-object/from16 v0, v18

    iput-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 451
    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 452
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cheus, IntentManager : activityResult : REQUEST_CODE_PICK_IMAGE_MULTIGRID : 03 : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 453
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    if-nez v5, :cond_1b

    .line 455
    const/4 v5, 0x0

    move-object/from16 v0, v18

    iput-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 444
    :cond_1a
    :goto_9
    add-int/lit8 v22, v22, 0x1

    goto :goto_8

    .line 459
    :cond_1b
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    const-string v6, "content"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 461
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    goto :goto_9

    .line 465
    :cond_1c
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    goto :goto_9

    .line 528
    .end local v15    # "bundle":Landroid/os/Bundle;
    .end local v16    # "count":I
    .end local v18    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .end local v19    # "decodeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;>;"
    .end local v22    # "index":I
    .end local v25    # "selectedUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_1d
    const-string v5, "REQUEST_CODE_GALLERY data null"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 533
    :cond_1e
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "REQUEST_CODE_TOOL_GALLERY result not ok : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 537
    :sswitch_8
    const/4 v5, -0x1

    move/from16 v0, p5

    if-eq v0, v5, :cond_1f

    if-nez p5, :cond_0

    .line 539
    :cond_1f
    move-object/from16 v0, p0

    instance-of v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v5, :cond_0

    .line 540
    check-cast p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .end local p0    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->refreshViewFromEffectManger(I)V

    goto/16 :goto_0

    .line 133
    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_2
        0x10001 -> :sswitch_3
        0x20000 -> :sswitch_0
        0x30000 -> :sswitch_1
        0x40000 -> :sswitch_4
        0x40001 -> :sswitch_5
        0x50000 -> :sswitch_6
        0x70000 -> :sswitch_7
        0x80000 -> :sswitch_8
    .end sparse-switch
.end method

.method public static clear()V
    .locals 1

    .prologue
    .line 639
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    .line 640
    return-void
.end method

.method public static getMultiGridImageFromGallery(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "addImgCnt"    # I
    .param p2, "multiGridDecodeCallback"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    .prologue
    .line 72
    sput-object p2, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    .line 73
    const/high16 v0, 0x70000

    invoke-static {p0, v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->sendGalleryMultipleCollage(Landroid/content/Context;II)V

    .line 74
    return-void
.end method

.method public static getMultiGridImageFromGallery(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "multiGridDecodeCallback"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    .prologue
    .line 79
    sput-object p1, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    .line 80
    const/high16 v0, 0x70000

    invoke-static {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->sendGallery(Landroid/content/Context;I)V

    .line 81
    return-void
.end method

.method public static getMultipleImageForTray(Landroid/content/Context;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "count"    # I

    .prologue
    .line 44
    const v0, 0x40001

    invoke-static {p0, v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->sendGalleryMultiple(Landroid/content/Context;II)V

    .line 45
    return-void
.end method

.method public static makeSetAsIntent(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 105
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.ATTACH_DATA"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 106
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "image/*"

    .line 107
    .local v1, "mimeType":Ljava/lang/String;
    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    const-string v2, "mimeType"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 110
    return-object v0
.end method

.method public static makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 114
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 116
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 118
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 120
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 121
    return-object v0
.end method

.method public static openCamera(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    const/high16 v0, 0x30000

    invoke-static {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->sendCamera(Landroid/content/Context;I)V

    .line 53
    return-void
.end method

.method public static openGallery(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    const/high16 v0, 0x20000

    invoke-static {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->sendGallery(Landroid/content/Context;I)V

    .line 49
    return-void
.end method

.method public static openGalleryForTray(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const/high16 v0, 0x40000

    invoke-static {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->sendGallery(Landroid/content/Context;I)V

    .line 41
    return-void
.end method

.method public static openPen(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "viewWidth"    # I
    .param p3, "viewHeight"    # I

    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "openPen w,h:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 85
    const/high16 v0, 0x50000

    invoke-static {p0, p1, v0, p2, p3}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->sendPen(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;III)V

    .line 86
    return-void
.end method

.method public static openToolCamera(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "copyToDecodeCallback"    # Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    .prologue
    .line 61
    sput-object p1, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    .line 62
    const v0, 0x10001

    invoke-static {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->sendCamera(Landroid/content/Context;I)V

    .line 63
    return-void
.end method

.method public static openToolGallery(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "copyToDecodeCallback"    # Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    .prologue
    .line 56
    sput-object p1, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    .line 57
    const/high16 v0, 0x10000

    invoke-static {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->sendGallery(Landroid/content/Context;I)V

    .line 58
    return-void
.end method

.method private static sendCamera(Landroid/content/Context;I)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requestCode"    # I

    .prologue
    .line 606
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/Image;->getSaveFileName(I)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mFileNameFromCamera:Ljava/lang/String;

    .line 607
    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    sget-object v4, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mFileNameFromCamera:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    .local v2, "out":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    sget-object v3, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 609
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 610
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 612
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 613
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "android.media.action.IMAGE_CAPTURE"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 614
    const-string v3, "output"

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 615
    const-string v3, "android.intent.extra.videoQuality"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 616
    check-cast p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, v1, p1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->startActivityForResult(Landroid/content/Intent;I)V

    .line 617
    return-void
.end method

.method private static sendGallery(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requestCode"    # I

    .prologue
    .line 547
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 548
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "com.sec.android.gallery3d"

    const-string v3, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 549
    const-string v2, "android.intent.action.PICK"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 550
    const-string v2, "image/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 551
    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "vnd.android.cursor.dir/image"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 552
    const/high16 v2, 0x70000

    if-eq p1, v2, :cond_0

    .line 553
    const/high16 v2, 0x10000

    if-ne p1, v2, :cond_1

    .line 554
    :cond_0
    const-string v2, "android.intent.extra.LOCAL_ONLY"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 558
    :cond_1
    :try_start_0
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, v1, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 564
    :goto_0
    return-void

    .line 560
    :catch_0
    move-exception v0

    .line 562
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static sendGalleryMultiple(Landroid/content/Context;II)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requestCode"    # I
    .param p2, "count"    # I

    .prologue
    .line 567
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MULTIPLE_PICK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 568
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "image/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 569
    const-string v2, "android.intent.extra.LOCAL_ONLY"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 570
    const-string v2, "pick-max-item"

    rsub-int/lit8 v3, p2, 0x5

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 574
    :try_start_0
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, v1, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 580
    :goto_0
    return-void

    .line 576
    :catch_0
    move-exception v0

    .line 578
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static sendGalleryMultipleCollage(Landroid/content/Context;II)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requestCode"    # I
    .param p2, "count"    # I

    .prologue
    const/4 v4, 0x1

    .line 584
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 585
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "com.sec.android.gallery3d"

    const-string v3, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 586
    const-string v2, "android.intent.action.GET_CONTENT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 587
    const-string v2, "multi-pick"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 588
    const-string v2, "image/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 589
    const-string v2, "pick-max-item"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 590
    const-string v2, "pick-min-item"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 591
    const-string v2, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 596
    :try_start_0
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, v1, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 602
    :goto_0
    return-void

    .line 598
    :catch_0
    move-exception v0

    .line 600
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendMultigrid(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 125
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MULTIPLE_PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 126
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    const/high16 v1, 0x70000

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 128
    return-void
.end method

.method private static sendPen(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;III)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "requstCode"    # I
    .param p3, "viewWidth"    # I
    .param p4, "viewHeight"    # I

    .prologue
    .line 620
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 621
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 622
    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 623
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/CommonInputData;->setData([I)I

    move-result v0

    .line 624
    .local v0, "index":I
    const-string v2, "imageIndex"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 626
    const-string v2, "width"

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 627
    const-string v2, "height"

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 628
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendPen w,h:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 629
    const-string v2, "viewWidth"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 630
    const-string v2, "viewHeight"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 631
    const-string v2, "draw_canvas_roi"

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 632
    check-cast p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, v1, p2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->startActivityForResult(Landroid/content/Intent;I)V

    .line 633
    return-void
.end method

.method public static sendSetAs(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 89
    if-nez p1, :cond_0

    .line 94
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeSetAsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 93
    .local v0, "intent":Landroid/content/Intent;
    const v1, 0x7f0600a3

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static sendShareVia(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 97
    if-nez p1, :cond_0

    .line 102
    :goto_0
    return-void

    .line 99
    :cond_0
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 101
    .local v0, "intent":Landroid/content/Intent;
    const v1, 0x7f06000f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 100
    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
