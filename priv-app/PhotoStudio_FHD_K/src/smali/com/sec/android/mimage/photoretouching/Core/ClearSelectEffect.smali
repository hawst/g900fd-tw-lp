.class public Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;
.super Ljava/lang/Object;
.source "ClearSelectEffect.java"


# instance fields
.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 28
    return-void
.end method


# virtual methods
.method public applyOriginal()[I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 40
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawPathList()Landroid/graphics/Path;

    move-result-object v0

    .line 41
    .local v0, "path":Landroid/graphics/Path;
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 42
    invoke-virtual {v0}, Landroid/graphics/Path;->rewind()V

    .line 43
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v1

    invoke-static {v1, v5}, Ljava/util/Arrays;->fill([BB)V

    .line 44
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v4

    invoke-direct {v2, v3, v4, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setOriginalMaskRoi(Landroid/graphics/Rect;)V

    .line 45
    const/4 v1, 0x0

    return-object v1
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;)V
    .locals 1
    .param p1, "clearselecteffect"    # Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;

    .prologue
    .line 35
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 36
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 51
    return-void
.end method

.method public init(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 0
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 32
    return-void
.end method
