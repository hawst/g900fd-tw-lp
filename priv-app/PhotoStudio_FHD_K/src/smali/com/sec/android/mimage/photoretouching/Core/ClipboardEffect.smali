.class public Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;
.super Ljava/lang/Object;
.source "ClipboardEffect.java"


# instance fields
.field private mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

.field private mContext:Landroid/content/Context;

.field private mCurrentStickerCount:I

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mContext:Landroid/content/Context;

    .line 95
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 96
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mCurrentStickerCount:I

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p3, "clipboard"    # [Lcom/sec/android/mimage/photoretouching/Core/Clipboard;
    .param p4, "count"    # I

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mContext:Landroid/content/Context;

    .line 95
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 96
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mCurrentStickerCount:I

    .line 28
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mContext:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 30
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    .line 31
    iput p4, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mCurrentStickerCount:I

    .line 32
    return-void
.end method


# virtual methods
.method public applyOriginal()[I
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 64
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    mul-int/2addr v1, v3

    new-array v10, v1, [I

    .line 65
    .local v10, "ret":[I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    .line 66
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    .line 67
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 65
    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 68
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v1

    .line 70
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    .line 73
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    .line 74
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 68
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 75
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    array-length v1, v1

    if-lt v8, v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v7

    move-object v1, v10

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1, v10}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([I)V

    .line 90
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 91
    return-object v10

    .line 77
    :cond_0
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mCurrentStickerCount:I

    if-lt v9, v1, :cond_1

    .line 75
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 79
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v1, v1, v9

    if-eqz v1, :cond_2

    .line 80
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v1, v1, v9

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getZOrder()I

    move-result v1

    if-ne v1, v8, :cond_2

    .line 82
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v1, v1, v9

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->applyOriginal(Landroid/graphics/Bitmap;)V

    .line 77
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;)V
    .locals 1
    .param p1, "effectEffect"    # Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    .prologue
    .line 35
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 36
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mContext:Landroid/content/Context;

    .line 37
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    .line 38
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mCurrentStickerCount:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mCurrentStickerCount:I

    .line 39
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 61
    return-void
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 48
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 43
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
