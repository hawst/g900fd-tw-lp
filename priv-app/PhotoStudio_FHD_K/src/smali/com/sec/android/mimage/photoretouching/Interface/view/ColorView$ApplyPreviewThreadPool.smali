.class Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;
.super Ljava/lang/Object;
.source "ColorView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ApplyPreviewThreadPool"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field mCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ThreadCallback;

.field mStep:I

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ThreadCallback;I)V
    .locals 1
    .param p2, "callback"    # Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ThreadCallback;
    .param p3, "step"    # I

    .prologue
    .line 2960
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    .line 2959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2957
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;->mCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ThreadCallback;

    .line 2958
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;->mStep:I

    .line 2961
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;->mCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ThreadCallback;

    .line 2962
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;->mStep:I

    .line 2963
    return-void
.end method


# virtual methods
.method public bridge synthetic run(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;->run(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public run(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;)Ljava/lang/Void;
    .locals 3
    .param p1, "jc"    # Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;

    .prologue
    const/4 v2, 0x0

    .line 2966
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentEffectType:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)I

    move-result v0

    const v1, 0x15001506

    if-eq v0, v1, :cond_0

    .line 2968
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;->mStep:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->applyPreview(I)I

    .line 2969
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;->mCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ThreadCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ThreadCallback;->onFinished()V

    .line 2970
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;->mCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ThreadCallback;

    .line 2972
    :cond_0
    return-object v2
.end method
