.class Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
.super Ljava/lang/Object;
.source "EffectTiltShiftMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TiltShiftInfo"
.end annotation


# static fields
.field static final DRAG:I = 0x0

.field static final DRAG_MOVE:I = 0x4

.field static final HOLD:I = 0x2

.field static final NONE:I = 0x3

.field static final ZOOM:I = 0x1


# instance fields
.field isConfigurationChange:Z

.field isTSWorking:Z

.field isTiltFirstTouch:Z

.field mTSCenter:F

.field mTSOrgCenter:F

.field mTSOrgRange:F

.field mTSRange:F

.field mTSTouchState:I

.field mTempPreviewIn:[I

.field mTempPreviewOut:[I

.field mTiltEventThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;

.field mTiltInitThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;

.field showOriginal:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 489
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSCenter:F

    .line 502
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSRange:F

    .line 503
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewIn:[I

    .line 504
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    .line 505
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltInitThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltInitThread;

    .line 506
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltEventThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;

    .line 507
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSOrgCenter:F

    .line 508
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSOrgRange:F

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;)V
    .locals 0

    .prologue
    .line 489
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)V

    return-void
.end method


# virtual methods
.method center(Landroid/view/MotionEvent;)F
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 515
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float v0, v1, v2

    .line 517
    .local v0, "c":F
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$4(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 518
    return v0
.end method

.method destroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 511
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewIn:[I

    .line 512
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTempPreviewOut:[I

    .line 513
    return-void
.end method

.method spacingY(Landroid/view/MotionEvent;)F
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 523
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 524
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    sub-float v0, v1, v2

    .line 525
    .local v0, "y":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 528
    .end local v0    # "y":F
    :goto_0
    return v1

    :cond_0
    const/high16 v1, -0x40800000    # -1.0f

    goto :goto_0
.end method
