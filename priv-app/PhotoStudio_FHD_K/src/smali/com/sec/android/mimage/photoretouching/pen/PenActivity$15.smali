.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;
.super Ljava/lang/Object;
.source "PenActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->newSpenListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    .line 1175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1183
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/pen/util/SettingView;->isSettingViewVisible(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/pen/util/SettingView;->isSettingViewVisible(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1184
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/pen/util/SettingView;->closeSettingView()V

    .line 1186
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1211
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$25(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isUndoable()Z

    move-result v0

    .line 1212
    .local v0, "undoable":Z
    if-nez v0, :cond_2

    .line 1213
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$21(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 1217
    :goto_1
    return v4

    .line 1188
    .end local v0    # "undoable":Z
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$29(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V

    .line 1189
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v1, v4}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$30(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V

    .line 1190
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$31(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V

    .line 1191
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v1, p2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$32(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 1194
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$31(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V

    .line 1195
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v1, p2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$32(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 1198
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$29(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V

    .line 1199
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v1, p2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$32(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 1205
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v1, v4}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$29(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V

    .line 1206
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v1, v4}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$31(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V

    .line 1207
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v1, p2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$32(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 1215
    .restart local v0    # "undoable":Z
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$21(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    goto :goto_1

    .line 1186
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_3
        0x2 -> :sswitch_1
        0x3 -> :sswitch_3
        0x5 -> :sswitch_2
        0x6 -> :sswitch_3
        0x106 -> :sswitch_3
    .end sparse-switch
.end method
