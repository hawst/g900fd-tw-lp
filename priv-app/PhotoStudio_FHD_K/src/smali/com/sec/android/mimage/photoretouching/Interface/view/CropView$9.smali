.class Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$9;
.super Ljava/lang/Object;
.source "CropView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->initButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    .line 428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 454
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 437
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->onButtonClicked(Landroid/view/View;)V
    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;Landroid/view/View;)V

    .line 438
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->resetZoom()V

    .line 439
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->pinchZoom()V

    .line 441
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v0

    const v1, 0x11201305

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->init(I)V

    .line 443
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(Landroid/view/View;Z)V

    .line 445
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 446
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->invalidateViews()V

    .line 447
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 448
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 433
    return-void
.end method
