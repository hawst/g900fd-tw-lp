.class Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;
.super Ljava/lang/Thread;
.source "EffectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EffectAnimationThread"
.end annotation


# instance fields
.field private AnimationDstPaint:Landroid/graphics/Paint;

.field private AnimationSrcPaint:Landroid/graphics/Paint;

.field private mDuration:I

.field private mKillThread:Z

.field private mRunningAnimation:Z

.field private mStartTime:J

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2845
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    .line 2844
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2838
    const/16 v0, 0x2a8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->mDuration:I

    .line 2839
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->mStartTime:J

    .line 2840
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->mKillThread:Z

    .line 2841
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->mRunningAnimation:Z

    .line 2842
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->AnimationSrcPaint:Landroid/graphics/Paint;

    .line 2843
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->AnimationDstPaint:Landroid/graphics/Paint;

    .line 2846
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->AnimationSrcPaint:Landroid/graphics/Paint;

    .line 2847
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->AnimationDstPaint:Landroid/graphics/Paint;

    .line 2848
    return-void
.end method

.method private declared-synchronized wakeUp()V
    .locals 1

    .prologue
    .line 2902
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2903
    monitor-exit p0

    return-void

    .line 2902
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public drawCanvas(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2857
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->mKillThread:Z

    if-nez v0, :cond_0

    .line 2859
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->AnimationSrcPaint:Landroid/graphics/Paint;

    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 2860
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->AnimationDstPaint:Landroid/graphics/Paint;

    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 2862
    :cond_0
    return-void
.end method

.method public free()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2851
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->mRunningAnimation:Z

    .line 2852
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->AnimationSrcPaint:Landroid/graphics/Paint;

    .line 2853
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->AnimationDstPaint:Landroid/graphics/Paint;

    .line 2854
    return-void
.end method

.method public killThread()V
    .locals 1

    .prologue
    .line 2893
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->mKillThread:Z

    .line 2894
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->wakeUp()V

    .line 2895
    return-void
.end method

.method public run()V
    .locals 10

    .prologue
    const/high16 v9, 0x437f0000    # 255.0f

    const/high16 v8, 0x3f800000    # 1.0f

    .line 2866
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 2867
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->mRunningAnimation:Z

    .line 2868
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->mStartTime:J

    .line 2869
    const/4 v0, 0x0

    .line 2872
    .local v0, "diff":I
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->mStartTime:J

    sub-long/2addr v4, v6

    long-to-int v0, v4

    .line 2873
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->mDuration:I

    if-lt v3, v0, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->mKillThread:Z

    if-eqz v3, :cond_1

    .line 2888
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->invalidateViewsWithThread()V

    .line 2889
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->free()V

    .line 2890
    return-void

    .line 2875
    :cond_1
    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->mDuration:I

    int-to-float v4, v4

    div-float v2, v3, v4

    .line 2876
    .local v2, "input":F
    cmpl-float v3, v2, v8

    if-lez v3, :cond_2

    .line 2877
    const/high16 v2, 0x3f800000    # 1.0f

    .line 2878
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->AnimationSrcPaint:Landroid/graphics/Paint;

    sub-float v4, v8, v2

    mul-float/2addr v4, v9

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2879
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->AnimationDstPaint:Landroid/graphics/Paint;

    mul-float v4, v9, v2

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2881
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->invalidateViewsWithThread()V

    .line 2883
    const-wide/16 v4, 0x14

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2884
    :catch_0
    move-exception v1

    .line 2885
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public runningAnimation()Z
    .locals 1

    .prologue
    .line 2898
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->mRunningAnimation:Z

    return v0
.end method
