.class Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$1;
.super Ljava/lang/Object;
.source "StampListView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public applyEffect(I)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->applyPreview(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getBGColor()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x0

    .line 64
    .local v0, "ret":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)I

    move-result v1

    const v2, 0x3150008b

    if-lt v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)I

    move-result v1

    const v2, 0x3150009f

    if-gt v1, v2, :cond_1

    .line 65
    sget v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE1:I

    .line 68
    :cond_0
    :goto_0
    return v0

    .line 66
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)I

    move-result v1

    const v2, 0x315000a0

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)I

    move-result v1

    const v2, 0x315000ac

    if-gt v1, v2, :cond_0

    .line 67
    sget v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE2:I

    goto :goto_0
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->invalidate()V

    .line 86
    return-void
.end method
