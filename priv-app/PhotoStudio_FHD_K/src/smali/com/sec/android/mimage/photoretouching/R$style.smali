.class public final Lcom/sec/android/mimage/photoretouching/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final PenTheme_Light:I = 0x7f070003

.field public static final PhotoeditorActionBarBackground:I = 0x7f070006

.field public static final PhotoeditorActionBarTextStyle:I = 0x7f070007

.field public static final PhotoeditorOverflowButton:I = 0x7f070004

.field public static final PhotoeditorTheme:I = 0x7f070000

.field public static final PhotoeditorTheme_Light:I = 0x7f070001

.field public static final arrow_right_button_image_style:I = 0x7f070016

.field public static final back_button_focus:I = 0x7f070010

.field public static final collage_modify_button:I = 0x7f070046

.field public static final collage_modify_icon:I = 0x7f070047

.field public static final collage_modify_text:I = 0x7f070048

.field public static final color_icon:I = 0x7f070049

.field public static final color_picker_color_icon:I = 0x7f070045

.field public static final color_picker_color_icon_sel:I = 0x7f070044

.field public static final deco_label_text_style_tab1_1:I = 0x7f070017

.field public static final deco_label_text_style_tab1_10:I = 0x7f070020

.field public static final deco_label_text_style_tab1_11:I = 0x7f070021

.field public static final deco_label_text_style_tab1_12:I = 0x7f070022

.field public static final deco_label_text_style_tab1_2:I = 0x7f070018

.field public static final deco_label_text_style_tab1_3:I = 0x7f070019

.field public static final deco_label_text_style_tab1_4:I = 0x7f07001a

.field public static final deco_label_text_style_tab1_5:I = 0x7f07001b

.field public static final deco_label_text_style_tab1_6:I = 0x7f07001c

.field public static final deco_label_text_style_tab1_7:I = 0x7f07001d

.field public static final deco_label_text_style_tab1_8:I = 0x7f07001e

.field public static final deco_label_text_style_tab1_9:I = 0x7f07001f

.field public static final deco_label_text_style_tab2_1:I = 0x7f070023

.field public static final deco_label_text_style_tab2_10:I = 0x7f07002c

.field public static final deco_label_text_style_tab2_11:I = 0x7f07002d

.field public static final deco_label_text_style_tab2_12:I = 0x7f07002e

.field public static final deco_label_text_style_tab2_2:I = 0x7f070024

.field public static final deco_label_text_style_tab2_3:I = 0x7f070025

.field public static final deco_label_text_style_tab2_4:I = 0x7f070026

.field public static final deco_label_text_style_tab2_5:I = 0x7f070027

.field public static final deco_label_text_style_tab2_6:I = 0x7f070028

.field public static final deco_label_text_style_tab2_7:I = 0x7f070029

.field public static final deco_label_text_style_tab2_8:I = 0x7f07002a

.field public static final deco_label_text_style_tab2_9:I = 0x7f07002b

.field public static final deco_tab_button_image_style:I = 0x7f07000f

.field public static final deco_tab_item_style:I = 0x7f07000a

.field public static final decoration_button_item_style:I = 0x7f070015

.field public static final disable_multitouch_style:I = 0x7f07004e

.field public static final drawing_button_style:I = 0x7f070005

.field public static final effect_manager_text_style:I = 0x7f07004d

.field public static final effectmanage_menu_image_style_land:I = 0x7f07003b

.field public static final effectmanage_menu_image_style_port:I = 0x7f07003a

.field public static final extra_menu_background_frameimage_style_h:I = 0x7f07003e

.field public static final extra_menu_background_stampimage_style_h:I = 0x7f070040

.field public static final extra_menu_frame_button_h:I = 0x7f070037

.field public static final extra_menu_frameimage_style_h:I = 0x7f07003d

.field public static final extra_menu_image_button_h:I = 0x7f070033

.field public static final extra_menu_image_button_h_portrait:I = 0x7f070034

.field public static final extra_menu_image_style_h:I = 0x7f070039

.field public static final extra_menu_image_style_h_portrait:I = 0x7f07003c

.field public static final extra_menu_stamp_button_h:I = 0x7f070038

.field public static final extra_menu_stampimage_style_h:I = 0x7f07003f

.field public static final extra_menu_text_style:I = 0x7f070041

.field public static final extra_menu_thumbnail_button_h:I = 0x7f070035

.field public static final extra_menu_thumbnail_button_h_portrait:I = 0x7f070036

.field public static final help_popup_text_style:I = 0x7f07004c

.field public static final indication_icon:I = 0x7f07004a

.field public static final main_bottom_button_image_style:I = 0x7f07000e

.field public static final main_bottom_button_image_style_on_framelayout:I = 0x7f070011

.field public static final main_bottom_button_layout_style:I = 0x7f070012

.field public static final main_bottom_button_layout_style_on_framelayout:I = 0x7f070013

.field public static final main_bottom_button_style:I = 0x7f070008

.field public static final main_bottom_button_text_style:I = 0x7f070014

.field public static final main_bottom_multi_grid_button_style:I = 0x7f070009

.field public static final middle_button_style:I = 0x7f07000d

.field public static final mySeekBar:I = 0x7f070002

.field public static final pen_bottom_button_style:I = 0x7f07000b

.field public static final progress_text_style_1:I = 0x7f07004b

.field public static final selection_button_text_style:I = 0x7f07002f

.field public static final selection_button_text_style_1:I = 0x7f070031

.field public static final selection_button_text_style_2:I = 0x7f070032

.field public static final selection_button_text_style_on_framelayout:I = 0x7f070030

.field public static final sticker_bottom_button_style:I = 0x7f07000c

.field public static final sticker_frame_layout:I = 0x7f070042

.field public static final sticker_view_icon:I = 0x7f070043


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
