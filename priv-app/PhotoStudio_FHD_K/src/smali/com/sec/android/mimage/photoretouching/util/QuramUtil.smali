.class public Lcom/sec/android/mimage/photoretouching/util/QuramUtil;
.super Ljava/lang/Object;
.source "QuramUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;
    }
.end annotation


# static fields
.field public static final AVAILABLE_TOUCH_AREA_OFFSET:I = 0x28

.field public static final BOTTOM_BUTTON_DATABASE_NAME:Ljava/lang/String; = "common_button_table.db"

.field public static final BOTTOM_BUTTON_DATABASE_NAME_EFFECT:Ljava/lang/String; = "effect_button_table.db"

.field public static final BUTTON_SHOW_HIDE_ANIMATION_DURATION:I = 0x190

.field public static final DB_DIR:Ljava/lang/String;

.field public static final DEVICE_NAME:Ljava/lang/String;

.field public static Debug:Z = false

.field public static DebugTag:Ljava/lang/String; = null

.field public static final DrawBitmap:Z = true

.field static final ERROR:I = -0x1

.field public static final EXTERNAL_STORAGE:Ljava/lang/String;

.field public static final HISTORY_PATH:Ljava/lang/String;

.field public static final INTENT_BITMAP_PARCEL_NAME:Ljava/lang/String; = "bitmap_parcel"

.field public static final INTENT_CAMERA_NAME:Ljava/lang/String; = "camera"

.field public static final INTENT_FILE_NAME:Ljava/lang/String; = "filename"

.field public static final INTENT_MULTI_SELECT_NAME:Ljava/lang/String; = "multi_select"

.field public static final INTENT_NO_IMAGE:Ljava/lang/String; = "no"

.field public static final INTENT_PATH_LIST_NAME:Ljava/lang/String; = "path_list"

.field public static final INTENT_PATH_NAME:Ljava/lang/String; = "path"

.field public static final INTENT_PEN_BUFFER_DRAW_RECT:Ljava/lang/String; = "draw_rect"

.field public static final INTENT_PEN_BUFFER_HEIGHT:Ljava/lang/String; = "height"

.field public static final INTENT_PEN_BUFFER_INDEX:Ljava/lang/String; = "imageIndex"

.field public static final INTENT_PEN_BUFFER_MATRIX_VALUES:Ljava/lang/String; = "matrix_values"

.field public static final INTENT_PEN_BUFFER_VIEWHEIGHT:Ljava/lang/String; = "viewHeight"

.field public static final INTENT_PEN_BUFFER_VIEWWIDTH:Ljava/lang/String; = "viewWidth"

.field public static final INTENT_PEN_BUFFER_WIDTH:Ljava/lang/String; = "width"

.field public static final INTENT_PEN_BUTTON_ID:Ljava/lang/String; = "button_id"

.field public static final INTENT_PEN_DRAWCANVAS_ROI:Ljava/lang/String; = "draw_canvas_roi"

.field public static final INTENT_URI_LIST_NAME:Ljava/lang/String; = "uri_list"

.field public static final INVALIDATE_TIME:I = 0x1f4

.field public static final K_MODEL_G800:Ljava/lang/String; = "G800"

.field public static final K_MODEL_G900:Ljava/lang/String; = "G900"

.field public static final K_MODEL_G905:Ljava/lang/String; = "G905"

.field public static final MAX_DECODE_TIME:I = 0x258

.field public static final MAX_GET_INTENT_TIME:I = 0x32

.field public static final MAX_IMAGE_SIZE:I = 0x7a1200

.field public static final MAX_MULTIGRID_IMAGE_SIZE:I = 0x2dc6c0

.field public static final MAX_PREVIEW_SIZE:I = 0x1e8480

.field public static final MAX_ZOOM_RATIO:F = 10.0f

.field public static final MINUS_ZOOM_RATIO:F = 0.75f

.field public static final MIN_AVAILABLE_MEM_SIZE:I = 0xa00000

.field public static final MIN_ZOOM_RATIO:F = 1.0f

.field public static final MORPHING_ANIMATION_DURATION:I = 0x2bc

.field public static final MULTI_GRID_RESULT_WIDTH:I = 0x640

.field public static final PACKAGE:Ljava/lang/String; = "com.sec.android.mimage.photoretouching"

.field public static final SAVE_DIR:Ljava/lang/String;

.field public static final SAVE_GOOD_SIZE:I = 0x4c4b40

.field public static final SAVE_MAX_SIZE:I = 0x7a1200

.field public static final STATE_MINUS_ZOOM:I = 0x1

.field public static final STATE_NOT_MINUS_ZOOM:I = 0x2

.field public static final TEMP_CAMERA_PATH:Ljava/lang/String;

.field public static enhanceFlag:Z

.field private static mToast:Landroid/widget/Toast;

.field private static mToastEndTime:J

.field private static mToastStartTime:J

.field private static mToast_ac:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 69
    sput-boolean v1, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->Debug:Z

    .line 70
    const-string v0, "140630_QURAM_K_MR_v1"

    sput-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->DebugTag:Ljava/lang/String;

    .line 71
    sput-object v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    .line 72
    sput-object v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast_ac:Landroid/widget/Toast;

    .line 73
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->DEVICE_NAME:Ljava/lang/String;

    .line 76
    sput-boolean v1, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->enhanceFlag:Z

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->EXTERNAL_STORAGE:Ljava/lang/String;

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->EXTERNAL_STORAGE:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "Studio"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->EXTERNAL_STORAGE:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".Studio"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->TEMP_CAMERA_PATH:Ljava/lang/String;

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/.db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->DB_DIR:Ljava/lang/String;

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->TEMP_CAMERA_PATH:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/tempHistoryInfo/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->HISTORY_PATH:Ljava/lang/String;

    .line 131
    sput-wide v4, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastStartTime:J

    sput-wide v4, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastEndTime:J

    .line 810
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetAngleBetweenTwoPoints1(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)I
    .locals 20
    .param p0, "centerPt"    # Landroid/graphics/PointF;
    .param p1, "startPt"    # Landroid/graphics/PointF;
    .param p2, "endPt"    # Landroid/graphics/PointF;

    .prologue
    .line 424
    const/4 v4, 0x0

    .line 426
    .local v4, "angle":I
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 427
    .local v2, "a":Landroid/graphics/Point;
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 429
    .local v5, "b":Landroid/graphics/Point;
    move-object/from16 v0, p1

    iget v13, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v16, v0

    sub-float v13, v13, v16

    float-to-int v13, v13

    iput v13, v2, Landroid/graphics/Point;->x:I

    .line 430
    move-object/from16 v0, p1

    iget v13, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    sub-float v13, v13, v16

    float-to-int v13, v13

    iput v13, v2, Landroid/graphics/Point;->y:I

    .line 432
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v16, v0

    sub-float v13, v13, v16

    float-to-int v13, v13

    iput v13, v5, Landroid/graphics/Point;->x:I

    .line 433
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    sub-float v13, v13, v16

    float-to-int v13, v13

    iput v13, v5, Landroid/graphics/Point;->y:I

    .line 435
    iget v13, v2, Landroid/graphics/Point;->x:I

    iget v0, v2, Landroid/graphics/Point;->x:I

    move/from16 v16, v0

    mul-int v13, v13, v16

    iget v0, v2, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    iget v0, v2, Landroid/graphics/Point;->y:I

    move/from16 v17, v0

    mul-int v16, v16, v17

    add-int v13, v13, v16

    int-to-double v0, v13

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    .line 436
    .local v6, "dist_a":D
    iget v13, v5, Landroid/graphics/Point;->x:I

    iget v0, v5, Landroid/graphics/Point;->x:I

    move/from16 v16, v0

    mul-int v13, v13, v16

    iget v0, v5, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    iget v0, v5, Landroid/graphics/Point;->y:I

    move/from16 v17, v0

    mul-int v16, v16, v17

    add-int v13, v13, v16

    int-to-double v0, v13

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    .line 437
    .local v8, "dist_b":D
    iget v13, v2, Landroid/graphics/Point;->x:I

    iget v0, v5, Landroid/graphics/Point;->x:I

    move/from16 v16, v0

    mul-int v13, v13, v16

    iget v0, v2, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    iget v0, v5, Landroid/graphics/Point;->y:I

    move/from16 v17, v0

    mul-int v16, v16, v17

    add-int v3, v13, v16

    .line 438
    .local v3, "a_b":I
    int-to-double v0, v3

    move-wide/from16 v16, v0

    mul-double v18, v6, v8

    div-double v14, v16, v18

    .line 440
    .local v14, "value":D
    iget v13, v2, Landroid/graphics/Point;->x:I

    iget v0, v5, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    mul-int v13, v13, v16

    iget v0, v2, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    iget v0, v5, Landroid/graphics/Point;->x:I

    move/from16 v17, v0

    mul-int v16, v16, v17

    sub-int v12, v13, v16

    .line 442
    .local v12, "temp":I
    invoke-static {v14, v15}, Ljava/lang/Math;->acos(D)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v10

    .line 443
    .local v10, "fangle":D
    double-to-int v4, v10

    .line 444
    if-gez v12, :cond_0

    .line 445
    rsub-int v4, v4, 0x168

    .line 447
    :cond_0
    const/16 v13, 0x168

    if-le v4, v13, :cond_1

    .line 448
    add-int/lit16 v4, v4, -0x168

    .line 450
    :cond_1
    const/4 v2, 0x0

    .line 451
    const/4 v5, 0x0

    .line 453
    return v4
.end method

.method public static GetExternalStorageMount()Z
    .locals 5

    .prologue
    .line 916
    const/4 v2, 0x0

    .line 918
    .local v2, "ret":Z
    const/4 v0, 0x0

    .line 919
    .local v0, "ExternalStorageAvailable":Z
    const/4 v1, 0x0

    .line 921
    .local v1, "ExternalStorageWriteable":Z
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v3

    .line 923
    .local v3, "state":Ljava/lang/String;
    const-string v4, "mounted"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 925
    const/4 v1, 0x1

    move v0, v1

    .line 936
    .end local v0    # "ExternalStorageAvailable":Z
    :goto_0
    if-eqz v1, :cond_2

    .line 937
    const/4 v2, 0x0

    .line 941
    :goto_1
    return v2

    .line 927
    .restart local v0    # "ExternalStorageAvailable":Z
    :cond_0
    const-string v4, "mounted_ro"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 929
    const/4 v0, 0x1

    .line 930
    const/4 v1, 0x0

    .line 931
    goto :goto_0

    .line 933
    :cond_1
    const/4 v1, 0x0

    move v0, v1

    .local v0, "ExternalStorageAvailable":I
    goto :goto_0

    .line 939
    .end local v0    # "ExternalStorageAvailable":I
    :cond_2
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public static LogD(Ljava/lang/String;)V
    .locals 1
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 133
    sget-boolean v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->Debug:Z

    if-eqz v0, :cond_0

    .line 134
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->DebugTag:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    :cond_0
    return-void
.end method

.method public static LogE(Ljava/lang/String;)V
    .locals 1
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 142
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->DebugTag:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    return-void
.end method

.method public static LogI(Ljava/lang/String;)V
    .locals 1
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 145
    sget-boolean v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->Debug:Z

    if-eqz v0, :cond_0

    .line 146
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->DebugTag:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_0
    return-void
.end method

.method public static LogV(Ljava/lang/String;)V
    .locals 1
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 137
    sget-boolean v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->Debug:Z

    if-eqz v0, :cond_0

    .line 138
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->DebugTag:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_0
    return-void
.end method

.method public static LogW(Ljava/lang/String;)V
    .locals 1
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 149
    sget-boolean v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->Debug:Z

    if-eqz v0, :cond_0

    .line 150
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->DebugTag:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :cond_0
    return-void
.end method

.method public static ccw(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F
    .locals 4
    .param p0, "p1"    # Landroid/graphics/PointF;
    .param p1, "p2"    # Landroid/graphics/PointF;
    .param p2, "p3"    # Landroid/graphics/PointF;

    .prologue
    .line 515
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/PointF;->y:F

    iget v2, p0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    iget v1, p1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0
.end method

.method public static checkOrMakeFileDirectory(Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p0, "directory"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 272
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 274
    .local v1, "nf":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 276
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v3

    if-nez v3, :cond_0

    .line 277
    const-string v3, "make directory!!, fail"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    .line 286
    .end local v1    # "nf":Ljava/io/File;
    :cond_0
    :goto_0
    return-object v1

    .line 281
    .restart local v1    # "nf":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 282
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v1, v2

    .line 283
    goto :goto_0
.end method

.method public static checkPointInRect(FFLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 3
    .param p0, "x"    # F
    .param p1, "y"    # F
    .param p2, "P1"    # Landroid/graphics/PointF;
    .param p3, "P2"    # Landroid/graphics/PointF;
    .param p4, "P3"    # Landroid/graphics/PointF;
    .param p5, "P4"    # Landroid/graphics/PointF;

    .prologue
    const/4 v2, 0x0

    .line 498
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 499
    .local v0, "currentPoint":Landroid/graphics/PointF;
    invoke-virtual {v0, p0, p1}, Landroid/graphics/PointF;->set(FF)V

    .line 501
    invoke-static {p2, p3, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->ccw(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    invoke-static {p3, p4, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->ccw(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 502
    invoke-static {p4, p5, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->ccw(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    invoke-static {p5, p2, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->ccw(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 504
    const/4 v0, 0x0

    .line 505
    const/4 v1, 0x1

    .line 510
    :goto_0
    return v1

    .line 509
    :cond_0
    const/4 v0, 0x0

    .line 510
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static convertRectToVector(Landroid/graphics/Rect;)Ljava/util/Vector;
    .locals 4
    .param p0, "rect"    # Landroid/graphics/Rect;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/Vector",
            "<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation

    .prologue
    .line 676
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 677
    .local v0, "ret":Ljava/util/Vector;, "Ljava/util/Vector<Landroid/graphics/Point;>;"
    new-instance v1, Landroid/graphics/Point;

    iget v2, p0, Landroid/graphics/Rect;->left:I

    iget v3, p0, Landroid/graphics/Rect;->top:I

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 678
    new-instance v1, Landroid/graphics/Point;

    iget v2, p0, Landroid/graphics/Rect;->right:I

    iget v3, p0, Landroid/graphics/Rect;->top:I

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 679
    new-instance v1, Landroid/graphics/Point;

    iget v2, p0, Landroid/graphics/Rect;->right:I

    iget v3, p0, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 680
    new-instance v1, Landroid/graphics/Point;

    iget v2, p0, Landroid/graphics/Rect;->left:I

    iget v3, p0, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 681
    new-instance v1, Landroid/graphics/Point;

    iget v2, p0, Landroid/graphics/Rect;->left:I

    iget v3, p0, Landroid/graphics/Rect;->top:I

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 682
    return-object v0
.end method

.method public static copyBuffer([BILandroid/graphics/Rect;[BILandroid/graphics/Rect;)V
    .locals 5
    .param p0, "src"    # [B
    .param p1, "srcWidthStep"    # I
    .param p2, "srcRoi"    # Landroid/graphics/Rect;
    .param p3, "dst"    # [B
    .param p4, "dstWidthStep"    # I
    .param p5, "dstRoi"    # Landroid/graphics/Rect;

    .prologue
    .line 1119
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {p5}, Landroid/graphics/Rect;->width()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 1121
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p5}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-lt v0, v3, :cond_1

    .line 1136
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 1124
    .restart local v0    # "i":I
    :cond_1
    iget v3, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v0

    mul-int/2addr v3, p1

    iget v4, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    .line 1125
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v4

    .line 1123
    invoke-static {p0, v3, v4}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1127
    .local v1, "inputBuffer":Ljava/nio/ByteBuffer;
    iget v3, p5, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v0

    mul-int/2addr v3, p4

    iget v4, p5, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    .line 1128
    invoke-virtual {p5}, Landroid/graphics/Rect;->width()I

    move-result v4

    .line 1126
    invoke-static {p3, v3, v4}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1130
    .local v2, "outputBuffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 1132
    const/4 v1, 0x0

    .line 1121
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static copyBuffer([IILandroid/graphics/Rect;[IILandroid/graphics/Rect;)V
    .locals 5
    .param p0, "src"    # [I
    .param p1, "srcWidthStep"    # I
    .param p2, "srcRoi"    # Landroid/graphics/Rect;
    .param p3, "dst"    # [I
    .param p4, "dstWidthStep"    # I
    .param p5, "dstRoi"    # Landroid/graphics/Rect;

    .prologue
    .line 1139
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {p5}, Landroid/graphics/Rect;->width()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 1141
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p5}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-lt v0, v3, :cond_1

    .line 1156
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 1144
    .restart local v0    # "i":I
    :cond_1
    iget v3, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v0

    mul-int/2addr v3, p1

    iget v4, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    .line 1145
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v4

    .line 1143
    invoke-static {p0, v3, v4}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v1

    .line 1147
    .local v1, "inputBuffer":Ljava/nio/IntBuffer;
    iget v3, p5, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v0

    mul-int/2addr v3, p4

    iget v4, p5, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    .line 1148
    invoke-virtual {p5}, Landroid/graphics/Rect;->width()I

    move-result v4

    .line 1146
    invoke-static {p3, v3, v4}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v2

    .line 1150
    .local v2, "outputBuffer":Ljava/nio/IntBuffer;
    invoke-virtual {v2, v1}, Ljava/nio/IntBuffer;->put(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;

    .line 1152
    const/4 v1, 0x0

    .line 1141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "i"    # Ljava/io/InputStream;
    .param p1, "r"    # Landroid/graphics/Rect;
    .param p2, "o"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 1265
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/MemoryStatus;->getAvailableExternalMemorySize()J

    move-result-wide v0

    .line 1266
    .local v0, "available_memsize":J
    const-wide/32 v2, 0x6e45000

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 1267
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1269
    :cond_0
    invoke-static {p0, p1, p2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    return-object v2
.end method

.method public static doNeedUppercaseString(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1420
    const v8, 0x7f0601b2

    invoke-static {p0, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 1421
    .local v2, "discard":Ljava/lang/String;
    const v8, 0x7f06000d

    invoke-static {p0, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    .line 1422
    .local v6, "save":Ljava/lang/String;
    const v8, 0x7f060009

    invoke-static {p0, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 1423
    .local v0, "cancel":Ljava/lang/String;
    const v8, 0x7f060008

    invoke-static {p0, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 1424
    .local v3, "done":Ljava/lang/String;
    const v8, 0x7f0601b8

    invoke-static {p0, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    .line 1425
    .local v4, "enhance":Ljava/lang/String;
    const v8, 0x7f0601c0

    invoke-static {p0, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1426
    .local v5, "marquee":Ljava/lang/String;
    const v8, 0x7f060193

    invoke-static {p0, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 1427
    .local v1, "deselect":Ljava/lang/String;
    const v8, 0x7f060198

    invoke-static {p0, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    .line 1429
    .local v7, "shuffle":Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {p1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1430
    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {p1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1431
    :cond_0
    const/4 v8, 0x1

    .line 1434
    :goto_0
    return v8

    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public static existsFile(Ljava/lang/String;)Z
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 1312
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1313
    .local v0, "file":Ljava/io/File;
    if-nez v0, :cond_0

    .line 1314
    const/4 v1, 0x0

    .line 1315
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    goto :goto_0
.end method

.method public static externalMemoryAvailable()Z
    .locals 2

    .prologue
    .line 814
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 815
    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 814
    return v0
.end method

.method public static findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;
    .locals 4
    .param p1, "resId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;I)",
            "Landroid/widget/LinearLayout;"
        }
    .end annotation

    .prologue
    .line 336
    .local p0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/LinearLayout;>;"
    const/4 v2, 0x0

    .line 337
    .local v2, "ret":Landroid/widget/LinearLayout;
    if-eqz p0, :cond_0

    .line 339
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_1

    .line 349
    .end local v0    # "i":I
    :cond_0
    :goto_1
    return-object v2

    .line 341
    .restart local v0    # "i":I
    :cond_1
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 342
    .local v1, "l":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    if-ne v3, p1, :cond_2

    .line 344
    move-object v2, v1

    .line 345
    goto :goto_1

    .line 339
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static findCaller(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 5
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    const/4 v4, 0x1

    .line 1092
    move-object v1, p1

    .line 1093
    .local v1, "temp":Ljava/lang/Throwable;
    invoke-virtual {v1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    .line 1094
    .local v0, "elements":[Ljava/lang/StackTraceElement;
    new-instance v2, Ljava/lang/StringBuilder;

    aget-object v3, v0, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->removeAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "() ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v0, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v0, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static findCaller(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1
    .param p0, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 1086
    const-string v0, ""

    invoke-static {v0, p0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findCaller(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static findCallerWholeStack(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    const/4 v5, 0x0

    .line 1097
    move-object v2, p1

    .line 1098
    .local v2, "temp":Ljava/lang/Throwable;
    invoke-virtual {v2}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    .line 1099
    .local v0, "elements":[Ljava/lang/StackTraceElement;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "============ Caller stack of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->removeAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "() ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1100
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_0

    aget-object v3, v0, v1

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.sec.android.mimage.photoretouching"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1104
    :cond_0
    return-void

    .line 1102
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->removeAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->removeAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1100
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static findCallerWholeStack(Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 1089
    const-string v0, ""

    invoke-static {v0, p0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findCallerWholeStack(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1090
    return-void
.end method

.method public static formatSize(J)Ljava/lang/String;
    .locals 6
    .param p0, "size"    # J

    .prologue
    const-wide/16 v4, 0x400

    .line 886
    const/4 v2, 0x0

    .line 888
    .local v2, "suffix":Ljava/lang/String;
    cmp-long v3, p0, v4

    if-ltz v3, :cond_0

    .line 890
    const-string v2, "KiB"

    .line 891
    div-long/2addr p0, v4

    .line 892
    cmp-long v3, p0, v4

    if-ltz v3, :cond_0

    .line 894
    const-string v2, "MiB"

    .line 895
    div-long/2addr p0, v4

    .line 899
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 901
    .local v1, "resultBuffer":Ljava/lang/StringBuilder;
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v0, v3, -0x3

    .line 902
    .local v0, "commaOffset":I
    :goto_0
    if-gtz v0, :cond_2

    .line 908
    if-eqz v2, :cond_1

    .line 909
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 911
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 904
    :cond_2
    const/16 v3, 0x2c

    invoke-virtual {v1, v0, v3}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 905
    add-int/lit8 v0, v0, -0x3

    goto :goto_0
.end method

.method public static getAngle(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)I
    .locals 20
    .param p0, "centerPt"    # Landroid/graphics/PointF;
    .param p1, "startPt"    # Landroid/graphics/PointF;
    .param p2, "endPt"    # Landroid/graphics/PointF;

    .prologue
    .line 359
    const/4 v4, 0x0

    .line 361
    .local v4, "angle":I
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    .line 362
    .local v2, "a":Landroid/graphics/PointF;
    new-instance v5, Landroid/graphics/PointF;

    invoke-direct {v5}, Landroid/graphics/PointF;-><init>()V

    .line 364
    .local v5, "b":Landroid/graphics/PointF;
    move-object/from16 v0, p1

    iget v13, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v16, v0

    sub-float v13, v13, v16

    iput v13, v2, Landroid/graphics/PointF;->x:F

    .line 365
    move-object/from16 v0, p1

    iget v13, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    sub-float v13, v13, v16

    iput v13, v2, Landroid/graphics/PointF;->y:F

    .line 367
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v16, v0

    sub-float v13, v13, v16

    iput v13, v5, Landroid/graphics/PointF;->x:F

    .line 368
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    sub-float v13, v13, v16

    iput v13, v5, Landroid/graphics/PointF;->y:F

    .line 370
    iget v13, v2, Landroid/graphics/PointF;->x:F

    iget v0, v2, Landroid/graphics/PointF;->x:F

    move/from16 v16, v0

    mul-float v13, v13, v16

    iget v0, v2, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    iget v0, v2, Landroid/graphics/PointF;->y:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    add-float v13, v13, v16

    float-to-double v0, v13

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    .line 371
    .local v6, "dist_a":D
    iget v13, v5, Landroid/graphics/PointF;->x:F

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v16, v0

    mul-float v13, v13, v16

    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    add-float v13, v13, v16

    float-to-double v0, v13

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    .line 372
    .local v8, "dist_b":D
    iget v13, v2, Landroid/graphics/PointF;->x:F

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v16, v0

    mul-float v13, v13, v16

    iget v0, v2, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    add-float v3, v13, v16

    .line 373
    .local v3, "a_b":F
    float-to-double v0, v3

    move-wide/from16 v16, v0

    mul-double v18, v6, v8

    div-double v14, v16, v18

    .line 375
    .local v14, "value":D
    iget v13, v2, Landroid/graphics/PointF;->x:F

    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    mul-float v13, v13, v16

    iget v0, v2, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v12, v13, v16

    .line 377
    .local v12, "temp":F
    invoke-static {v14, v15}, Ljava/lang/Math;->acos(D)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v10

    .line 378
    .local v10, "fangle":D
    double-to-int v4, v10

    .line 379
    const/4 v13, 0x0

    cmpg-float v13, v12, v13

    if-gez v13, :cond_0

    .line 380
    rsub-int v4, v4, 0x168

    .line 382
    :cond_0
    const/16 v13, 0x168

    if-le v4, v13, :cond_1

    .line 383
    add-int/lit16 v4, v4, -0x168

    .line 385
    :cond_1
    const/4 v2, 0x0

    .line 386
    const/4 v5, 0x0

    .line 388
    return v4
.end method

.method public static getAvailableExternalMemorySize()J
    .locals 8

    .prologue
    .line 846
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->externalMemoryAvailable()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 848
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    .line 849
    .local v4, "path":Ljava/io/File;
    new-instance v5, Landroid/os/StatFs;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 850
    .local v5, "stat":Landroid/os/StatFs;
    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v2, v6

    .line 851
    .local v2, "blockSize":J
    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v0, v6

    .line 853
    .local v0, "availableBlocks":J
    const/4 v4, 0x0

    .line 854
    const/4 v5, 0x0

    .line 856
    mul-long v6, v0, v2

    .line 860
    :goto_0
    return-wide v6

    .end local v0    # "availableBlocks":J
    .end local v2    # "blockSize":J
    .end local v4    # "path":Ljava/io/File;
    .end local v5    # "stat":Landroid/os/StatFs;
    :cond_0
    const-wide/16 v6, -0x1

    goto :goto_0
.end method

.method public static getAvailableInternalMemorySize()J
    .locals 8

    .prologue
    .line 820
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v4

    .line 821
    .local v4, "path":Ljava/io/File;
    new-instance v5, Landroid/os/StatFs;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 822
    .local v5, "stat":Landroid/os/StatFs;
    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v2, v6

    .line 823
    .local v2, "blockSize":J
    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v0, v6

    .line 825
    .local v0, "availableBlocks":J
    const/4 v4, 0x0

    .line 826
    const/4 v5, 0x0

    .line 828
    mul-long v6, v0, v2

    return-wide v6
.end method

.method public static getBitmapFromUri(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 22
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "viewWidth"    # I
    .param p2, "viewHeight"    # I

    .prologue
    .line 1159
    const-string v3, "Bitmap getBitmapFromUri, else, 251 "

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1160
    const/4 v9, 0x0

    .line 1161
    .local v9, "b":Landroid/graphics/Bitmap;
    new-instance v12, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v12}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1162
    .local v12, "optsResolution":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    iput-boolean v3, v12, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1164
    move-object/from16 v0, p0

    invoke-static {v0, v12}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1166
    iget v5, v12, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 1167
    .local v5, "imageWidth":I
    iget v6, v12, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 1169
    .local v6, "imageHeight":I
    mul-int v3, v5, v6

    int-to-float v3, v3

    mul-int v4, p1, p2

    int-to-float v4, v4

    div-float/2addr v3, v4

    float-to-double v0, v3

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v16, v0

    .line 1170
    .local v16, "sampleSize":I
    if-nez v16, :cond_0

    .line 1171
    const/16 v16, 0x1

    .line 1172
    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, v12, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1173
    move/from16 v0, v16

    iput v0, v12, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1174
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v3, v12, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1175
    const/4 v3, 0x1

    iput-boolean v3, v12, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 1177
    move-object/from16 v0, p0

    invoke-static {v0, v12}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1179
    .local v2, "temp":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1180
    :cond_1
    const-string v3, "Bitmap getBitmapFromUri, if(temp == null || temp.isRecycled()), 272 "

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1181
    const/4 v3, 0x0

    .line 1261
    :goto_0
    return-object v3

    .line 1183
    :cond_2
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1185
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 1186
    .local v10, "bm":Landroid/graphics/Bitmap;
    const-string v3, "Bitmap getBitmapFromUri, if(!temp.hasAlpha()), 278 : "

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1187
    new-instance v11, Landroid/graphics/Canvas;

    invoke-direct {v11, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1188
    .local v11, "canvas":Landroid/graphics/Canvas;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x0

    invoke-virtual {v11, v2, v3, v4, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1189
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1190
    move-object v2, v10

    .line 1191
    const-string v3, "Bitmap getBitmapFromUri, if(!temp.hasAlpha()), temp, 283 : "

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1194
    .end local v10    # "bm":Landroid/graphics/Bitmap;
    .end local v11    # "canvas":Landroid/graphics/Canvas;
    :cond_3
    if-nez v2, :cond_4

    .line 1195
    const-string v3, "Bitmap getBitmapFromUri, if(temp == null), 287 : "

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1196
    const/4 v3, 0x0

    goto :goto_0

    .line 1199
    :cond_4
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 1200
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 1201
    if-lez v5, :cond_5

    if-gtz v6, :cond_6

    .line 1202
    :cond_5
    const-string v3, "Bitmap getBitmapFromUri, if(imageWidth <= 0 || imageHeight <= 0), 294 "

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1203
    const/4 v3, 0x0

    goto :goto_0

    .line 1206
    :cond_6
    const/4 v15, -0x1

    .line 1207
    .local v15, "rotate":I
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 1208
    .local v7, "m":Landroid/graphics/Matrix;
    invoke-static/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotateDegree(Ljava/lang/String;)I

    move-result v15

    .line 1209
    if-eqz v15, :cond_7

    .line 1211
    const/16 v3, 0x5a

    if-ne v15, v3, :cond_a

    .line 1212
    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1224
    :cond_7
    :goto_1
    if-eqz v15, :cond_8

    .line 1225
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1226
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1227
    move-object v2, v9

    .line 1228
    const-string v3, "Bitmap getBitmapFromUri, if(rotate != 0), 320 "

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1231
    :cond_8
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 1232
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 1235
    move/from16 v0, p1

    if-eq v5, v0, :cond_9

    move/from16 v0, p2

    if-ne v6, v0, :cond_d

    .line 1237
    :cond_9
    const-string v3, "Bitmap getBitmapFromUri, iif(imageWidth == viewWidth || imageHeight == viewHeight), 329"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    move-object v3, v2

    .line 1238
    goto/16 :goto_0

    .line 1214
    :cond_a
    const/16 v3, 0xb4

    if-ne v15, v3, :cond_b

    .line 1215
    const/high16 v3, 0x43340000    # 180.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_1

    .line 1217
    :cond_b
    const/16 v3, 0x10e

    if-ne v15, v3, :cond_c

    .line 1218
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_1

    .line 1222
    :cond_c
    const/4 v15, 0x0

    goto :goto_1

    .line 1241
    :cond_d
    move/from16 v0, p1

    int-to-float v3, v0

    int-to-float v4, v5

    div-float v19, v3, v4

    .line 1242
    .local v19, "scaleW":F
    move/from16 v0, p2

    int-to-float v3, v0

    int-to-float v4, v6

    div-float v18, v3, v4

    .line 1245
    .local v18, "scaleH":F
    move/from16 v0, p1

    if-lt v5, v0, :cond_e

    move/from16 v0, p2

    if-ge v6, v0, :cond_f

    .line 1247
    :cond_e
    move/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v17

    .line 1248
    .local v17, "scale":F
    const-string v3, "Bitmap getBitmapFromUri, iif(imageWidth == viewWidth || imageHeight == viewHeight), 340"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1254
    :goto_2
    int-to-float v3, v5

    mul-float v3, v3, v17

    float-to-int v14, v3

    .line 1255
    .local v14, "resizedWdt":I
    int-to-float v3, v6

    mul-float v3, v3, v17

    float-to-int v13, v3

    .line 1257
    .local v13, "resizedHgt":I
    const/4 v3, 0x0

    invoke-static {v2, v14, v13, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1258
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1259
    const/4 v2, 0x0

    .line 1260
    const-string v3, "Bitmap getBitmapFromUri, end, 352"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    move-object v3, v9

    .line 1261
    goto/16 :goto_0

    .line 1252
    .end local v13    # "resizedHgt":I
    .end local v14    # "resizedWdt":I
    .end local v17    # "scale":F
    :cond_f
    move/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v17

    .restart local v17    # "scale":F
    goto :goto_2
.end method

.method public static getCurrentFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 197
    move-object v2, p0

    .line 198
    .local v2, "ret":Ljava/lang/String;
    const/4 v0, 0x0

    .line 199
    .local v0, "canSaveFileName":Z
    const/4 v1, 0x1

    .line 200
    .local v1, "fileNumber":I
    const-string v5, "/"

    invoke-virtual {p0, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 201
    .local v3, "startFileName":I
    add-int/lit8 v5, v3, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 202
    .local v4, "subFileName":Ljava/lang/String;
    return-object v4
.end method

.method public static getDpToPixel(Landroid/content/Context;I)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dpVal"    # I

    .prologue
    .line 353
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    .line 354
    .local v0, "density":F
    int-to-float v1, p1

    mul-float/2addr v1, v0

    float-to-double v2, v1

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v4

    double-to-int v1, v2

    return v1
.end method

.method public static getEffectButtonInfoList(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 7
    .param p0, "cs"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1373
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1374
    .local v4, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;>;"
    if-eqz p0, :cond_1

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_1

    .line 1379
    :cond_0
    :try_start_0
    const-string v5, "button_id"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1380
    .local v0, "buttonId":I
    const-string v5, "button_idx"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 1381
    .local v1, "buttonIdx":I
    const-string v5, "use"

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1383
    .local v2, "buttonUse":I
    new-instance v5, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;

    invoke-direct {v5, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;-><init>(III)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1384
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "moveToNext:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1388
    .end local v0    # "buttonId":I
    .end local v1    # "buttonIdx":I
    .end local v2    # "buttonUse":I
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1390
    :cond_1
    return-object v4

    .line 1385
    :catch_0
    move-exception v3

    .line 1386
    .local v3, "e":Landroid/database/CursorIndexOutOfBoundsException;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "QuramUtil - getEffectButtonInfoList() : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/database/CursorIndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 207
    move-object v7, p0

    .line 208
    .local v7, "ret":Ljava/lang/String;
    const/4 v0, 0x0

    .line 209
    .local v0, "canSaveFileName":Z
    const/4 v3, 0x1

    .line 210
    .local v3, "fileNumber":I
    const-string v12, "/"

    invoke-virtual {p0, v12}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    .line 211
    .local v10, "startFileName":I
    move-object v11, p0

    .line 212
    .local v11, "subFileName":Ljava/lang/String;
    const/4 v12, -0x1

    if-eq v10, v12, :cond_0

    .line 213
    invoke-virtual {p0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 214
    :cond_0
    new-instance v12, Ljava/lang/StringBuilder;

    sget-object v13, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 215
    .local v5, "newFileName":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 217
    .local v2, "file":Ljava/io/File;
    const-string v12, "("

    invoke-virtual {v5, v12}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    .line 218
    .local v9, "st":I
    const-string v12, ")"

    invoke-virtual {v5, v12}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 220
    .local v1, "ed":I
    const/4 v12, -0x1

    if-eq v9, v12, :cond_1

    const/4 v12, -0x1

    if-eq v1, v12, :cond_1

    sub-int v12, v1, v9

    if-lez v12, :cond_1

    .line 222
    add-int/lit8 v12, v9, 0x1

    invoke-virtual {v5, v12, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 224
    .local v6, "number":Ljava/lang/String;
    :try_start_0
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 228
    .end local v6    # "number":Ljava/lang/String;
    :cond_1
    :goto_0
    if-eqz v0, :cond_3

    .line 254
    const/4 v2, 0x0

    .line 255
    const-string v12, "/"

    invoke-virtual {v7, v12}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    .line 256
    .local v8, "separator":I
    const/4 v12, -0x1

    if-eq v8, v12, :cond_2

    .line 257
    add-int/lit8 v12, v8, 0x1

    invoke-virtual {v7, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 259
    :cond_2
    return-object v7

    .line 230
    .end local v8    # "separator":I
    :cond_3
    const/4 v12, -0x1

    if-eq v9, v12, :cond_4

    const/4 v12, -0x1

    if-eq v1, v12, :cond_4

    .line 232
    new-instance v12, Ljava/lang/StringBuilder;

    const/4 v13, 0x0

    invoke-virtual {v5, v13, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    add-int/lit8 v13, v1, 0x1

    invoke-virtual {v5, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 244
    :goto_1
    new-instance v2, Ljava/io/File;

    .end local v2    # "file":Ljava/io/File;
    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 245
    .restart local v2    # "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_6

    .line 247
    const/4 v0, 0x1

    .line 248
    goto :goto_0

    .line 236
    :cond_4
    const-string v12, "."

    invoke-virtual {v5, v12}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 237
    .local v4, "idx":I
    const/4 v12, -0x1

    if-ne v4, v12, :cond_5

    .line 239
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ".jpg"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 240
    const-string v12, "."

    invoke-virtual {v5, v12}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 242
    :cond_5
    new-instance v12, Ljava/lang/StringBuilder;

    const/4 v13, 0x0

    invoke-virtual {v5, v13, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    add-int/lit8 v13, v4, 0x1

    invoke-virtual {v5, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 251
    .end local v4    # "idx":I
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 225
    .restart local v6    # "number":Ljava/lang/String;
    :catch_0
    move-exception v12

    goto/16 :goto_0
.end method

.method public static getFilePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 186
    const/4 v1, 0x0

    .line 187
    .local v1, "ret":Ljava/lang/String;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 188
    const-string v2, "/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 189
    .local v0, "endIndex":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 190
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 193
    .end local v0    # "endIndex":I
    :cond_0
    return-object v1
.end method

.method public static getGLRendererMaxSize()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 165
    const/4 v4, 0x0

    .line 167
    .local v4, "val":Ljava/nio/IntBuffer;
    :try_start_0
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v2

    check-cast v2, Ljavax/microedition/khronos/egl/EGL10;

    .line 168
    .local v2, "egl":Ljavax/microedition/khronos/egl/EGL10;
    invoke-interface {v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentContext()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    .line 169
    .local v0, "ctx":Ljavax/microedition/khronos/egl/EGLContext;
    invoke-virtual {v0}, Ljavax/microedition/khronos/egl/EGLContext;->getGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v3

    check-cast v3, Ljavax/microedition/khronos/opengles/GL10;

    .line 170
    .local v3, "gl":Ljavax/microedition/khronos/opengles/GL10;
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v4

    .line 171
    const/16 v6, 0xd33

    invoke-interface {v3, v6, v4}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(ILjava/nio/IntBuffer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    if-eqz v4, :cond_0

    .line 178
    invoke-virtual {v4}, Ljava/nio/IntBuffer;->get()I

    move-result v5

    .line 180
    .end local v0    # "ctx":Ljavax/microedition/khronos/egl/EGLContext;
    .end local v2    # "egl":Ljavax/microedition/khronos/egl/EGL10;
    .end local v3    # "gl":Ljavax/microedition/khronos/opengles/GL10;
    :cond_0
    :goto_0
    return v5

    .line 172
    :catch_0
    move-exception v1

    .line 174
    .local v1, "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getGLRendererMaxSize() : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getImageContentUri(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "imageFile"    # Ljava/io/File;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1272
    const/4 v10, 0x0

    .line 1273
    .local v10, "ret":Landroid/net/Uri;
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    .line 1274
    .local v8, "filePath":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1275
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1276
    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v5

    .line 1277
    const-string v3, "_data=? "

    .line 1278
    new-array v4, v4, [Ljava/lang/String;

    aput-object v8, v4, v5

    const/4 v5, 0x0

    .line 1274
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1279
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1281
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1280
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 1282
    .local v9, "id":I
    const-string v0, "content://media/external/images/media"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 1283
    .local v6, "baseUri":Landroid/net/Uri;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 1296
    .end local v6    # "baseUri":Landroid/net/Uri;
    .end local v9    # "id":I
    :cond_0
    :goto_0
    if-eqz v7, :cond_1

    .line 1297
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1298
    :cond_1
    const/4 v7, 0x0

    .line 1299
    return-object v10

    .line 1286
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1287
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 1288
    .local v11, "values":Landroid/content/ContentValues;
    const-string v0, "_data"

    invoke-virtual {v11, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1289
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1290
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1289
    invoke-virtual {v0, v1, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v10

    goto :goto_0
.end method

.method public static getIntersectPoint(Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 10
    .param p0, "AP1"    # Landroid/graphics/Point;
    .param p1, "AP2"    # Landroid/graphics/Point;
    .param p2, "BP1"    # Landroid/graphics/Point;
    .param p3, "BP2"    # Landroid/graphics/Point;

    .prologue
    .line 458
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 461
    .local v2, "ret":Landroid/graphics/Point;
    iget v6, p3, Landroid/graphics/Point;->y:I

    iget v7, p2, Landroid/graphics/Point;->y:I

    sub-int/2addr v6, v7

    iget v7, p1, Landroid/graphics/Point;->x:I

    iget v8, p0, Landroid/graphics/Point;->x:I

    sub-int/2addr v7, v8

    mul-int/2addr v6, v7

    iget v7, p3, Landroid/graphics/Point;->x:I

    iget v8, p2, Landroid/graphics/Point;->x:I

    sub-int/2addr v7, v8

    iget v8, p1, Landroid/graphics/Point;->y:I

    iget v9, p0, Landroid/graphics/Point;->y:I

    sub-int/2addr v8, v9

    mul-int/2addr v7, v8

    sub-int/2addr v6, v7

    int-to-float v5, v6

    .line 462
    .local v5, "under":F
    const/4 v6, 0x0

    cmpl-float v6, v5, v6

    if-nez v6, :cond_0

    const/4 v2, 0x0

    .line 476
    .end local v2    # "ret":Landroid/graphics/Point;
    :goto_0
    return-object v2

    .line 464
    .restart local v2    # "ret":Landroid/graphics/Point;
    :cond_0
    iget v6, p3, Landroid/graphics/Point;->x:I

    iget v7, p2, Landroid/graphics/Point;->x:I

    sub-int/2addr v6, v7

    iget v7, p0, Landroid/graphics/Point;->y:I

    iget v8, p2, Landroid/graphics/Point;->y:I

    sub-int/2addr v7, v8

    mul-int/2addr v6, v7

    iget v7, p3, Landroid/graphics/Point;->y:I

    iget v8, p2, Landroid/graphics/Point;->y:I

    sub-int/2addr v7, v8

    iget v8, p0, Landroid/graphics/Point;->x:I

    iget v9, p2, Landroid/graphics/Point;->x:I

    sub-int/2addr v8, v9

    mul-int/2addr v7, v8

    sub-int/2addr v6, v7

    int-to-float v1, v6

    .line 465
    .local v1, "_t":F
    iget v6, p1, Landroid/graphics/Point;->x:I

    iget v7, p0, Landroid/graphics/Point;->x:I

    sub-int/2addr v6, v7

    iget v7, p0, Landroid/graphics/Point;->y:I

    iget v8, p2, Landroid/graphics/Point;->y:I

    sub-int/2addr v7, v8

    mul-int/2addr v6, v7

    iget v7, p1, Landroid/graphics/Point;->y:I

    iget v8, p0, Landroid/graphics/Point;->y:I

    sub-int/2addr v7, v8

    iget v8, p0, Landroid/graphics/Point;->x:I

    iget v9, p2, Landroid/graphics/Point;->x:I

    sub-int/2addr v8, v9

    mul-int/2addr v7, v8

    sub-int/2addr v6, v7

    int-to-float v0, v6

    .line 467
    .local v0, "_s":F
    div-float v4, v1, v5

    .line 468
    .local v4, "t":F
    div-float v3, v0, v5

    .line 470
    .local v3, "s":F
    float-to-double v6, v4

    const-wide/16 v8, 0x0

    cmpg-double v6, v6, v8

    if-ltz v6, :cond_1

    float-to-double v6, v4

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v6, v6, v8

    if-gtz v6, :cond_1

    float-to-double v6, v3

    const-wide/16 v8, 0x0

    cmpg-double v6, v6, v8

    if-ltz v6, :cond_1

    float-to-double v6, v3

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v6, v6, v8

    if-lez v6, :cond_2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 471
    :cond_2
    const/4 v6, 0x0

    cmpl-float v6, v1, v6

    if-nez v6, :cond_3

    const/4 v6, 0x0

    cmpl-float v6, v0, v6

    if-nez v6, :cond_3

    const/4 v2, 0x0

    goto :goto_0

    .line 473
    :cond_3
    iget v6, p0, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    iget v7, p1, Landroid/graphics/Point;->x:I

    iget v8, p0, Landroid/graphics/Point;->x:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    mul-float/2addr v7, v4

    add-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v2, Landroid/graphics/Point;->x:I

    .line 474
    iget v6, p0, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    iget v7, p1, Landroid/graphics/Point;->y:I

    iget v8, p0, Landroid/graphics/Point;->y:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    mul-float/2addr v7, v4

    add-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v2, Landroid/graphics/Point;->y:I

    goto :goto_0
.end method

.method public static getPackageInfo(Landroid/content/Context;)Landroid/content/pm/PackageInfo;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 693
    const/4 v1, 0x0

    .line 694
    .local v1, "ret":Landroid/content/pm/PackageInfo;
    if-eqz p0, :cond_0

    .line 697
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 703
    :cond_0
    :goto_0
    return-object v1

    .line 698
    :catch_0
    move-exception v0

    .line 700
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x0

    .line 547
    if-eqz p1, :cond_2

    .line 548
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, p1, v6}, Landroid/provider/MediaStore$Images$Media;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 549
    .local v1, "cur":Landroid/database/Cursor;
    if-nez v1, :cond_0

    .line 551
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPathIfHiddenFile(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 570
    .end local v1    # "cur":Landroid/database/Cursor;
    :goto_0
    return-object v3

    .line 554
    .restart local v1    # "cur":Landroid/database/Cursor;
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-nez v5, :cond_1

    move-object v3, v4

    .line 556
    goto :goto_0

    .line 559
    :cond_1
    const-string v5, "_data"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 560
    .local v0, "columnIndex":I
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 562
    .local v3, "path":Ljava/lang/String;
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 568
    .end local v0    # "columnIndex":I
    .end local v1    # "cur":Landroid/database/Cursor;
    .end local v3    # "path":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 569
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    move-object v3, v4

    .line 570
    goto :goto_0

    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    move-object v3, v4

    .line 566
    goto :goto_0
.end method

.method public static getPathIfHiddenFile(Landroid/net/Uri;)Ljava/lang/String;
    .locals 6
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 575
    const/4 v2, 0x0

    .line 576
    .local v2, "ret":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 577
    .local v3, "str":Ljava/lang/String;
    const/4 v0, 0x0

    .line 578
    .local v0, "chkHiddenFile":C
    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 580
    const/16 v4, 0x2e

    if-ne v0, v4, :cond_0

    .line 582
    const-string v4, "s"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 583
    .local v1, "firstIndex":I
    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 585
    .end local v1    # "firstIndex":I
    :cond_0
    return-object v2
.end method

.method public static getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 156
    if-nez p0, :cond_0

    .line 157
    const/4 v0, 0x0

    .line 158
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/samsung/android/secretmode/SecretModeManager;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getRotateDegree(Landroid/content/ContentResolver;Landroid/net/Uri;)I
    .locals 14
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 589
    if-eqz p0, :cond_b

    if-eqz p1, :cond_b

    .line 592
    const/4 v10, -0x1

    .line 594
    .local v10, "exifOrientation":I
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/mnt/sdcard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v11

    .line 647
    .local v11, "path":Ljava/lang/String;
    :goto_0
    if-nez v11, :cond_7

    .line 648
    const/4 v0, -0x1

    .line 672
    .end local v10    # "exifOrientation":I
    .end local v11    # "path":Ljava/lang/String;
    :goto_1
    return v0

    .line 600
    .restart local v10    # "exifOrientation":I
    :cond_0
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 601
    .local v2, "proj":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 606
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v3, 0x0

    .line 607
    const/4 v4, 0x0

    .line 608
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    .line 604
    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 613
    if-nez v7, :cond_1

    .line 615
    const/4 v11, 0x0

    .line 616
    .restart local v11    # "path":Ljava/lang/String;
    const/4 v0, -0x1

    goto :goto_1

    .line 609
    .end local v11    # "path":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 610
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 611
    const/4 v0, -0x1

    goto :goto_1

    .line 618
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_1
    const-string v0, "_data"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 619
    .local v6, "column_index":I
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 620
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 622
    if-eqz v7, :cond_2

    .line 623
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 624
    :cond_2
    const/4 v7, 0x0

    .line 625
    const/4 v0, -0x1

    goto :goto_1

    .line 628
    :cond_3
    invoke-interface {v7, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 630
    if-eqz v7, :cond_4

    .line 631
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 632
    :cond_4
    const/4 v7, 0x0

    .line 634
    const-string v13, "Unknown Jpeg"

    .line 635
    .local v13, "temp":Ljava/lang/String;
    move-object v11, v13

    .line 636
    .restart local v11    # "path":Ljava/lang/String;
    goto :goto_0

    .line 639
    .end local v11    # "path":Ljava/lang/String;
    .end local v13    # "temp":Ljava/lang/String;
    :cond_5
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 640
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 641
    .local v12, "ret":Ljava/lang/String;
    if-eqz v7, :cond_6

    .line 642
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 643
    :cond_6
    const/4 v7, 0x0

    .line 644
    move-object v11, v12

    .restart local v11    # "path":Ljava/lang/String;
    goto :goto_0

    .line 650
    .end local v2    # "proj":[Ljava/lang/String;
    .end local v6    # "column_index":I
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v12    # "ret":Ljava/lang/String;
    :cond_7
    :try_start_1
    new-instance v9, Landroid/media/ExifInterface;

    invoke-direct {v9, v11}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 651
    .local v9, "exif":Landroid/media/ExifInterface;
    if-eqz v9, :cond_b

    .line 652
    const-string v0, "Orientation"

    const/4 v1, 0x1

    invoke-virtual {v9, v0, v1}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v10

    .line 653
    const/4 v0, 0x6

    if-ne v10, v0, :cond_8

    .line 655
    const/16 v0, 0x5a

    goto :goto_1

    .line 657
    :cond_8
    const/4 v0, 0x3

    if-ne v10, v0, :cond_9

    .line 659
    const/16 v0, 0xb4

    goto :goto_1

    .line 661
    :cond_9
    const/16 v0, 0x8

    if-ne v10, v0, :cond_a

    .line 663
    const/16 v0, 0x10e

    goto :goto_1

    .line 665
    :cond_a
    const/4 v0, 0x0

    goto :goto_1

    .line 667
    .end local v9    # "exif":Landroid/media/ExifInterface;
    :catch_1
    move-exception v8

    .line 668
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 669
    const/4 v0, -0x1

    goto :goto_1

    .line 672
    .end local v8    # "e":Ljava/io/IOException;
    .end local v10    # "exifOrientation":I
    .end local v11    # "path":Ljava/lang/String;
    :cond_b
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static getRotateDegree(Ljava/lang/String;)I
    .locals 6
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 519
    const/4 v3, 0x0

    .line 523
    .local v3, "rotation":I
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 525
    .local v1, "exif":Landroid/media/ExifInterface;
    if-eqz v1, :cond_0

    .line 527
    const-string v4, "Orientation"

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 529
    .local v2, "exifOrientation":I
    const/4 v4, 0x6

    if-ne v2, v4, :cond_1

    .line 530
    const/16 v3, 0x5a

    .line 541
    .end local v1    # "exif":Landroid/media/ExifInterface;
    .end local v2    # "exifOrientation":I
    :cond_0
    :goto_0
    return v3

    .line 531
    .restart local v1    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exifOrientation":I
    :cond_1
    const/4 v4, 0x3

    if-ne v2, v4, :cond_2

    .line 532
    const/16 v3, 0xb4

    goto :goto_0

    .line 533
    :cond_2
    const/16 v4, 0x8

    if-ne v2, v4, :cond_0

    .line 534
    const/16 v3, 0x10e

    goto :goto_0

    .line 537
    .end local v1    # "exif":Landroid/media/ExifInterface;
    .end local v2    # "exifOrientation":I
    :catch_0
    move-exception v0

    .line 539
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getRotationPoint(FFFFF)Landroid/graphics/PointF;
    .locals 12
    .param p0, "srcX"    # F
    .param p1, "srcY"    # F
    .param p2, "angle"    # F
    .param p3, "pivotX"    # F
    .param p4, "pivotY"    # F

    .prologue
    .line 408
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    .line 409
    .local v3, "retPt":Landroid/graphics/PointF;
    move v6, p0

    .line 410
    .local v6, "x":F
    move v7, p1

    .line 411
    .local v7, "y":F
    float-to-double v8, p2

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    double-to-float v2, v8

    .line 412
    .local v2, "radian":F
    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 413
    .local v0, "cos":D
    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    .line 415
    .local v4, "sin":D
    sub-float v8, v6, p3

    float-to-double v8, v8

    mul-double/2addr v8, v0

    sub-float v10, v7, p4

    float-to-double v10, v10

    mul-double/2addr v10, v4

    sub-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    int-to-float v8, v8

    iput v8, v3, Landroid/graphics/PointF;->x:F

    .line 416
    sub-float v8, v6, p3

    float-to-double v8, v8

    mul-double/2addr v8, v4

    sub-float v10, v7, p4

    float-to-double v10, v10

    mul-double/2addr v10, v0

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    int-to-float v8, v8

    iput v8, v3, Landroid/graphics/PointF;->y:F

    .line 418
    iget v8, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v8, p3

    float-to-int v8, v8

    int-to-float v8, v8

    iput v8, v3, Landroid/graphics/PointF;->x:F

    .line 419
    iget v8, v3, Landroid/graphics/PointF;->y:F

    add-float v8, v8, p4

    float-to-int v8, v8

    int-to-float v8, v8

    iput v8, v3, Landroid/graphics/PointF;->y:F

    .line 420
    return-object v3
.end method

.method public static getRotationPoint(Landroid/graphics/PointF;IFF)Landroid/graphics/PointF;
    .locals 12
    .param p0, "srcPoint"    # Landroid/graphics/PointF;
    .param p1, "angle"    # I
    .param p2, "pivotX"    # F
    .param p3, "pivotY"    # F

    .prologue
    .line 392
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    .line 393
    .local v3, "retPt":Landroid/graphics/PointF;
    iget v6, p0, Landroid/graphics/PointF;->x:F

    .line 394
    .local v6, "x":F
    iget v7, p0, Landroid/graphics/PointF;->y:F

    .line 395
    .local v7, "y":F
    int-to-double v8, p1

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    double-to-float v2, v8

    .line 396
    .local v2, "radian":F
    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 397
    .local v0, "cos":D
    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    .line 399
    .local v4, "sin":D
    sub-float v8, v6, p2

    float-to-double v8, v8

    mul-double/2addr v8, v0

    sub-float v10, v7, p3

    float-to-double v10, v10

    mul-double/2addr v10, v4

    sub-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    int-to-float v8, v8

    iput v8, v3, Landroid/graphics/PointF;->x:F

    .line 400
    sub-float v8, v6, p2

    float-to-double v8, v8

    mul-double/2addr v8, v4

    sub-float v10, v7, p3

    float-to-double v10, v10

    mul-double/2addr v10, v0

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    int-to-float v8, v8

    iput v8, v3, Landroid/graphics/PointF;->y:F

    .line 402
    iget v8, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v8, p2

    iput v8, v3, Landroid/graphics/PointF;->x:F

    .line 403
    iget v8, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v8, p3

    iput v8, v3, Landroid/graphics/PointF;->y:F

    .line 404
    return-object v3
.end method

.method public static getSharedPreferences(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # I

    .prologue
    .line 686
    const/4 v0, 0x0

    .line 687
    .local v0, "ret":Landroid/content/SharedPreferences;
    if-eqz p0, :cond_0

    .line 688
    check-cast p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 689
    :cond_0
    return-object v0
.end method

.method public static getSimpleDate()Ljava/lang/String;
    .locals 6

    .prologue
    .line 263
    const/4 v0, 0x0

    .line 265
    .local v0, "ret":Ljava/lang/String;
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd HH.mm.ss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 266
    .local v1, "sdfNow":Ljava/text/SimpleDateFormat;
    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 267
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 268
    return-object v0
.end method

.method public static getString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    .line 800
    if-gtz p1, :cond_0

    .line 801
    const/4 v0, 0x0

    .line 802
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getTexture(IIILandroid/content/Context;)[I
    .locals 9
    .param p0, "rscId"    # I
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 1058
    const/4 v8, 0x0

    .line 1059
    .local v8, "mTexture":Landroid/graphics/Bitmap;
    const/4 v0, 0x0

    .line 1061
    .local v0, "mResizedTexture":Landroid/graphics/Bitmap;
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, p0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1063
    if-nez v8, :cond_0

    .line 1064
    const/4 v1, 0x0

    .line 1077
    :goto_0
    return-object v1

    .line 1066
    :cond_0
    invoke-static {v8, p1, p2, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1068
    mul-int v3, p1, p2

    new-array v1, v3, [I

    .local v1, "returnBuffer":[I
    move v3, p1

    move v4, v2

    move v5, v2

    move v6, p1

    move v7, p2

    .line 1069
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1071
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 1072
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1074
    const/4 v8, 0x0

    .line 1075
    const/4 v0, 0x0

    .line 1077
    goto :goto_0
.end method

.method public static getTotalExternalMemorySize()J
    .locals 8

    .prologue
    .line 866
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->externalMemoryAvailable()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 868
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    .line 869
    .local v2, "path":Ljava/io/File;
    new-instance v3, Landroid/os/StatFs;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 870
    .local v3, "stat":Landroid/os/StatFs;
    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v0, v6

    .line 871
    .local v0, "blockSize":J
    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockCount()I

    move-result v6

    int-to-long v4, v6

    .line 873
    .local v4, "totalBlocks":J
    const/4 v2, 0x0

    .line 874
    const/4 v3, 0x0

    .line 876
    mul-long v6, v4, v0

    .line 880
    :goto_0
    return-wide v6

    .end local v0    # "blockSize":J
    .end local v2    # "path":Ljava/io/File;
    .end local v3    # "stat":Landroid/os/StatFs;
    .end local v4    # "totalBlocks":J
    :cond_0
    const-wide/16 v6, -0x1

    goto :goto_0
.end method

.method public static getTotalInternalMemorySize()J
    .locals 8

    .prologue
    .line 833
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v2

    .line 834
    .local v2, "path":Ljava/io/File;
    new-instance v3, Landroid/os/StatFs;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 835
    .local v3, "stat":Landroid/os/StatFs;
    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v0, v6

    .line 836
    .local v0, "blockSize":J
    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockCount()I

    move-result v6

    int-to-long v4, v6

    .line 838
    .local v4, "totalBlocks":J
    const/4 v2, 0x0

    .line 839
    const/4 v3, 0x0

    .line 841
    mul-long v6, v4, v0

    return-wide v6
.end method

.method public static getViewGroup(Landroid/content/Context;I)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    .line 322
    const/4 v0, 0x0

    .line 323
    .local v0, "vg":Landroid/view/ViewGroup;
    check-cast p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    .line 324
    return-object v0
.end method

.method public static getZoomStateForZoomInOut(Landroid/graphics/Matrix;)I
    .locals 4
    .param p0, "supMatrix"    # Landroid/graphics/Matrix;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1320
    const/4 v0, -0x1

    .line 1322
    .local v0, "ret":I
    const/16 v2, 0x9

    new-array v1, v2, [F

    .line 1323
    .local v1, "values":[F
    invoke-virtual {p0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1324
    const/4 v2, 0x0

    aget v2, v1, v2

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    const/4 v2, 0x4

    aget v2, v1, v2

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 1326
    const/4 v0, 0x1

    .line 1333
    :goto_0
    return v0

    .line 1330
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static isDevice(Ljava/lang/String;)Z
    .locals 1
    .param p0, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 1393
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->DEVICE_NAME:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isHoveringUI(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 969
    if-nez p0, :cond_0

    .line 970
    const/4 v0, 0x0

    .line 972
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 973
    const-string v1, "com.sec.feature.hovering_ui"

    .line 972
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isInButton(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p0, "v"    # Landroid/view/View;
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 480
    const/4 v4, 0x0

    .line 481
    .local v4, "ret":Z
    const/4 v1, 0x0

    .line 482
    .local v1, "left":I
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 483
    .local v5, "right":I
    const/4 v6, 0x0

    .line 484
    .local v6, "top":I
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 485
    .local v0, "bottom":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v2, v7

    .line 486
    .local v2, "posX":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v3, v7

    .line 490
    .local v3, "posY":I
    if-lt v2, v1, :cond_0

    if-gt v2, v5, :cond_0

    if-lt v3, v6, :cond_0

    if-gt v3, v0, :cond_0

    .line 491
    const/4 v4, 0x1

    .line 494
    :goto_0
    return v4

    .line 493
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static isLOSdevices()Z
    .locals 1

    .prologue
    .line 1409
    const/4 v0, 0x1

    return v0
.end method

.method public static isLightThemeRequired()Z
    .locals 1

    .prologue
    .line 1397
    const-string v0, "tr"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1398
    const-string v0, "tb"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "a5"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "a7"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SCL"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SC"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLOSdevices()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "hestia"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1399
    :cond_0
    const/4 v0, 0x1

    .line 1400
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNewSpenJarSupported()Z
    .locals 1

    .prologue
    .line 1404
    const/4 v0, 0x1

    return v0
.end method

.method public static isToastShowing()Z
    .locals 4

    .prologue
    .line 707
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 708
    .local v0, "curr":J
    sget-wide v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastStartTime:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    sget-wide v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastEndTime:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static makeScaleMaskBuff([BII[BII)V
    .locals 5
    .param p0, "src"    # [B
    .param p1, "srcWidth"    # I
    .param p2, "srcHeight"    # I
    .param p3, "dst"    # [B
    .param p4, "dstWidth"    # I
    .param p5, "dstHeight"    # I

    .prologue
    .line 981
    if-eqz p0, :cond_0

    if-eqz p3, :cond_0

    .line 983
    sget-object v4, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 985
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 986
    .local v1, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 987
    const/4 v1, 0x0

    .line 988
    const/4 v4, 0x1

    invoke-static {v0, p4, p5, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 990
    .local v2, "scaledViewMask":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 991
    const/4 v0, 0x0

    .line 992
    invoke-static {p3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 993
    .local v3, "viewMask":Ljava/nio/ByteBuffer;
    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 994
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 995
    const/4 v2, 0x0

    .line 998
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "buffer":Ljava/nio/ByteBuffer;
    .end local v2    # "scaledViewMask":Landroid/graphics/Bitmap;
    .end local v3    # "viewMask":Ljava/nio/ByteBuffer;
    :cond_0
    return-void
.end method

.method public static makeStringWithIDnString(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I
    .param p2, "string"    # Ljava/lang/String;

    .prologue
    .line 1304
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1305
    .local v0, "text":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1307
    return-object v0
.end method

.method public static recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 291
    if-eqz p0, :cond_1

    .line 293
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 295
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 297
    :cond_0
    const/4 p0, 0x0

    .line 299
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public static recycleBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "view"    # Landroid/widget/ImageView;

    .prologue
    .line 303
    const/4 v0, 0x0

    .line 304
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p0, :cond_0

    .line 306
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 308
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    instance-of v2, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_0

    .line 310
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 311
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 313
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 314
    const/4 v0, 0x0

    .line 318
    :cond_0
    return-object v0
.end method

.method public static removeAppName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "org"    # Ljava/lang/String;

    .prologue
    .line 1081
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 1082
    .local v1, "temp":Ljava/lang/String;
    const-string v2, "com.sec.android.mimage.photoretouching."

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1083
    .local v0, "ret":Ljava/lang/String;
    return-object v0
.end method

.method public static removeHovering(Landroid/content/Context;Landroid/view/View;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 945
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isHoveringUI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 946
    invoke-virtual {p1, v1}, Landroid/view/View;->setHovered(Z)V

    .line 947
    invoke-virtual {p1, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 949
    :cond_0
    return-void
.end method

.method public static removeList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 328
    .local p0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/LinearLayout;>;"
    if-eqz p0, :cond_0

    .line 330
    invoke-virtual {p0}, Ljava/util/ArrayList;->clear()V

    .line 331
    const/4 p0, 0x0

    .line 333
    :cond_0
    return-void
.end method

.method public static resizeBuff([I[IIIII)V
    .locals 26
    .param p0, "in"    # [I
    .param p1, "out"    # [I
    .param p2, "in_width"    # I
    .param p3, "in_height"    # I
    .param p4, "out_width"    # I
    .param p5, "out_height"    # I

    .prologue
    .line 1003
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v23, v0

    move/from16 v0, p4

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v19, v23, v24

    .line 1004
    .local v19, "scale":F
    const-string v23, "resizeBuff float:0.326 to int:21364"

    invoke-static/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1005
    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "resizeBuff Float.floatToIntBits float:0.326 to int:"

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v24, 0x3ea6e979    # 0.326f

    invoke-static/range {v24 .. v24}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1006
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_0
    move/from16 v0, p5

    if-lt v12, v0, :cond_0

    .line 1054
    return-void

    .line 1007
    :cond_0
    int-to-float v0, v12

    move/from16 v23, v0

    mul-float v22, v23, v19

    .line 1009
    .local v22, "ypos":F
    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->floor(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-int v10, v0

    .line 1010
    .local v10, "floorY":I
    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-int v8, v0

    .line 1012
    .local v8, "ceilY":I
    move/from16 v0, p3

    if-ge v8, v0, :cond_2

    .line 1013
    :goto_1
    if-gez v10, :cond_1

    const/4 v10, 0x0

    .line 1015
    :cond_1
    int-to-float v0, v8

    move/from16 v23, v0

    sub-float v23, v23, v22

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    const/high16 v24, 0x10000

    mul-int v20, v23, v24

    .line 1016
    .local v20, "topRatio":I
    const/high16 v23, 0x10000

    sub-int v6, v23, v20

    .line 1018
    .local v6, "bottomRatio":I
    mul-int v10, v10, p2

    .line 1019
    mul-int v8, v8, p2

    .line 1020
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    move/from16 v0, p4

    if-lt v11, v0, :cond_3

    .line 1006
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 1012
    .end local v6    # "bottomRatio":I
    .end local v11    # "i":I
    .end local v20    # "topRatio":I
    :cond_2
    add-int/lit8 v8, p3, -0x1

    goto :goto_1

    .line 1021
    .restart local v6    # "bottomRatio":I
    .restart local v11    # "i":I
    .restart local v20    # "topRatio":I
    :cond_3
    int-to-float v0, v11

    move/from16 v23, v0

    mul-float v21, v23, v19

    .line 1022
    .local v21, "xpos":F
    const/16 v23, 0x0

    cmpl-float v23, v21, v23

    if-ltz v23, :cond_5

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v23, v0

    cmpg-float v23, v21, v23

    if-gez v23, :cond_5

    const/16 v23, 0x0

    cmpl-float v23, v22, v23

    if-ltz v23, :cond_5

    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v23, v0

    cmpg-float v23, v22, v23

    if-gez v23, :cond_5

    .line 1023
    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->floor(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-int v9, v0

    .line 1024
    .local v9, "floorX":I
    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-int v7, v0

    .line 1028
    .local v7, "ceilX":I
    int-to-float v0, v7

    move/from16 v23, v0

    sub-float v14, v23, v21

    .line 1029
    .local v14, "leftRatio":F
    const/high16 v23, 0x3f800000    # 1.0f

    sub-float v17, v23, v14

    .line 1031
    .local v17, "rightRatio":F
    move/from16 v0, p2

    if-ge v7, v0, :cond_6

    .line 1032
    :goto_3
    if-gez v9, :cond_4

    const/4 v9, 0x0

    .line 1033
    :cond_4
    add-int v15, v10, v9

    .line 1034
    .local v15, "leftTop":I
    add-int v18, v10, v7

    .line 1035
    .local v18, "rightTop":I
    add-int v13, v8, v9

    .line 1036
    .local v13, "leftBottom":I
    add-int v16, v8, v7

    .line 1039
    .local v16, "rightBottom":I
    aget v23, p0, v15

    invoke-static/range {v23 .. v23}, Landroid/graphics/Color;->alpha(I)I

    move-result v23

    mul-int v23, v23, v20

    aget v24, p0, v13

    invoke-static/range {v24 .. v24}, Landroid/graphics/Color;->alpha(I)I

    move-result v24

    mul-int v24, v24, v6

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v14

    .line 1040
    aget v24, p0, v18

    invoke-static/range {v24 .. v24}, Landroid/graphics/Color;->alpha(I)I

    move-result v24

    mul-int v24, v24, v20

    aget v25, p0, v16

    invoke-static/range {v25 .. v25}, Landroid/graphics/Color;->alpha(I)I

    move-result v25

    mul-int v25, v25, v6

    add-int v24, v24, v25

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v24, v24, v17

    .line 1039
    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    shr-int/lit8 v2, v23, 0x10

    .line 1042
    .local v2, "A":I
    aget v23, p0, v15

    invoke-static/range {v23 .. v23}, Landroid/graphics/Color;->red(I)I

    move-result v23

    mul-int v23, v23, v20

    aget v24, p0, v13

    invoke-static/range {v24 .. v24}, Landroid/graphics/Color;->red(I)I

    move-result v24

    mul-int v24, v24, v6

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v14

    .line 1043
    aget v24, p0, v18

    invoke-static/range {v24 .. v24}, Landroid/graphics/Color;->red(I)I

    move-result v24

    mul-int v24, v24, v20

    aget v25, p0, v16

    invoke-static/range {v25 .. v25}, Landroid/graphics/Color;->red(I)I

    move-result v25

    mul-int v25, v25, v6

    add-int v24, v24, v25

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v24, v24, v17

    .line 1042
    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    shr-int/lit8 v5, v23, 0x10

    .line 1045
    .local v5, "R":I
    aget v23, p0, v15

    invoke-static/range {v23 .. v23}, Landroid/graphics/Color;->green(I)I

    move-result v23

    mul-int v23, v23, v20

    aget v24, p0, v13

    invoke-static/range {v24 .. v24}, Landroid/graphics/Color;->green(I)I

    move-result v24

    mul-int v24, v24, v6

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v14

    .line 1046
    aget v24, p0, v18

    invoke-static/range {v24 .. v24}, Landroid/graphics/Color;->green(I)I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v24, v24, v14

    aget v25, p0, v16

    invoke-static/range {v25 .. v25}, Landroid/graphics/Color;->green(I)I

    move-result v25

    mul-int v25, v25, v6

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    add-float v24, v24, v25

    mul-float v24, v24, v17

    .line 1045
    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    shr-int/lit8 v4, v23, 0x10

    .line 1048
    .local v4, "G":I
    aget v23, p0, v15

    invoke-static/range {v23 .. v23}, Landroid/graphics/Color;->blue(I)I

    move-result v23

    mul-int v23, v23, v20

    aget v24, p0, v13

    invoke-static/range {v24 .. v24}, Landroid/graphics/Color;->blue(I)I

    move-result v24

    mul-int v24, v24, v6

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v14

    .line 1049
    aget v24, p0, v18

    invoke-static/range {v24 .. v24}, Landroid/graphics/Color;->blue(I)I

    move-result v24

    mul-int v24, v24, v20

    aget v25, p0, v16

    invoke-static/range {v25 .. v25}, Landroid/graphics/Color;->blue(I)I

    move-result v25

    mul-int v25, v25, v6

    add-int v24, v24, v25

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v24, v24, v17

    .line 1048
    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    shr-int/lit8 v3, v23, 0x10

    .line 1050
    .local v3, "B":I
    mul-int v23, v12, p4

    add-int v23, v23, v11

    invoke-static {v2, v5, v4, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v24

    aput v24, p1, v23

    .line 1020
    .end local v2    # "A":I
    .end local v3    # "B":I
    .end local v4    # "G":I
    .end local v5    # "R":I
    .end local v7    # "ceilX":I
    .end local v9    # "floorX":I
    .end local v13    # "leftBottom":I
    .end local v14    # "leftRatio":F
    .end local v15    # "leftTop":I
    .end local v16    # "rightBottom":I
    .end local v17    # "rightRatio":F
    .end local v18    # "rightTop":I
    :cond_5
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_2

    .line 1031
    .restart local v7    # "ceilX":I
    .restart local v9    # "floorX":I
    .restart local v14    # "leftRatio":F
    .restart local v17    # "rightRatio":F
    :cond_6
    add-int/lit8 v7, p2, -0x1

    goto/16 :goto_3
.end method

.method public static setHovering(Landroid/content/Context;Landroid/view/View;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 952
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isHoveringUI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 953
    invoke-virtual {p1, v1}, Landroid/view/View;->setHovered(Z)V

    .line 954
    invoke-virtual {p1, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 957
    :cond_0
    return-void
.end method

.method public static setHoveringWithoutPopUpDisplay(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 960
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isHoveringUI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 961
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setHovered(Z)V

    .line 962
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setHoverPopupType(I)V

    .line 965
    :cond_0
    return-void
.end method

.method public static showToast(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    .line 713
    if-nez p0, :cond_0

    .line 728
    :goto_0
    return-void

    .line 716
    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_3

    .line 717
    if-eqz p0, :cond_1

    .line 719
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    .line 724
    :cond_1
    :goto_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_2

    .line 725
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 726
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastStartTime:J

    .line 727
    sget-wide v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastStartTime:J

    const-wide/16 v2, 0xdac

    add-long/2addr v0, v2

    sput-wide v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastEndTime:J

    goto :goto_0

    .line 722
    :cond_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_1
.end method

.method public static showToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 753
    if-nez p0, :cond_0

    .line 767
    :goto_0
    return-void

    .line 756
    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_2

    .line 757
    if-eqz p0, :cond_1

    .line 759
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    .line 764
    :cond_1
    :goto_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 765
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastStartTime:J

    .line 766
    sget-wide v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastStartTime:J

    const-wide/16 v2, 0xdac

    add-long/2addr v0, v2

    sput-wide v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastEndTime:J

    goto :goto_0

    .line 762
    :cond_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public static showToastActionbar(Landroid/content/Context;III)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 732
    if-nez p0, :cond_1

    .line 749
    :cond_0
    :goto_0
    return-void

    .line 735
    :cond_1
    sget-object v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast_ac:Landroid/widget/Toast;

    if-nez v2, :cond_3

    .line 736
    if-eqz p0, :cond_2

    .line 738
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->twMakeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    sput-object v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast_ac:Landroid/widget/Toast;

    .line 744
    :cond_2
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 745
    .local v1, "widthPixels":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05024c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 746
    .local v0, "shifting":I
    sget-object v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast_ac:Landroid/widget/Toast;

    const/16 v3, 0x35

    sub-int v4, v1, p2

    sub-int/2addr v4, v0

    invoke-virtual {v2, v3, v4, p3}, Landroid/widget/Toast;->setGravity(III)V

    .line 747
    sget-object v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast_ac:Landroid/widget/Toast;

    if-eqz v2, :cond_0

    .line 748
    sget-object v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast_ac:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 741
    .end local v0    # "shifting":I
    .end local v1    # "widthPixels":I
    :cond_3
    sget-object v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast_ac:Landroid/widget/Toast;

    invoke-virtual {v2, p1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_1
.end method

.method public static showToastShort(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    .line 770
    if-nez p0, :cond_0

    .line 783
    :goto_0
    return-void

    .line 773
    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_2

    .line 774
    if-eqz p0, :cond_1

    .line 775
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    .line 780
    :cond_1
    :goto_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 781
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastStartTime:J

    .line 782
    sget-wide v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastStartTime:J

    const-wide/16 v2, 0x7d0

    add-long/2addr v0, v2

    sput-wide v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastEndTime:J

    goto :goto_0

    .line 778
    :cond_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_1
.end method

.method public static showToastShort(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 786
    if-nez p0, :cond_0

    .line 797
    :goto_0
    return-void

    .line 789
    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_1

    .line 790
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    .line 794
    :goto_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 795
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastStartTime:J

    .line 796
    sget-wide v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastStartTime:J

    const-wide/16 v2, 0x7d0

    add-long/2addr v0, v2

    sput-wide v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToastEndTime:J

    goto :goto_0

    .line 792
    :cond_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public static toUppercaseString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 1415
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 1416
    .local v0, "upperCaseString":Ljava/lang/String;
    return-object v0
.end method

.method public static whoIsCallMe(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 9
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1106
    move-object v5, p1

    .line 1107
    .local v5, "throwable":Ljava/lang/Throwable;
    invoke-virtual {v5}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    .line 1109
    .local v4, "elements":[Ljava/lang/StackTraceElement;
    aget-object v6, v4, v7

    invoke-virtual {v6}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    .line 1110
    .local v1, "calleeMethodName":Ljava/lang/String;
    aget-object v6, v4, v7

    invoke-virtual {v6}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 1111
    .local v0, "calleeClassName":Ljava/lang/String;
    aget-object v6, v4, v8

    invoke-virtual {v6}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    .line 1112
    .local v3, "callerMethodName":Ljava/lang/String;
    aget-object v6, v4, v8

    invoke-virtual {v6}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 1114
    .local v2, "callerClassName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, ", whoIsCallMe() - calleeMethodName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / calleeClassName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / callerMethodName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / callerClassName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1115
    return-void
.end method
