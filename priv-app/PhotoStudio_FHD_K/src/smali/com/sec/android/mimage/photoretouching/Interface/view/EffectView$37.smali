.class Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$37;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "EffectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initGesture()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$37;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    .line 2611
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2647
    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 2615
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$37;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isReverseAnimRunning:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$45(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2617
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$37;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$46(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Z)V

    .line 2625
    :goto_0
    return v2

    .line 2622
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$37;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    const/16 v1, 0x32

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$47(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;I)V

    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 2631
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2635
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2643
    :goto_0
    return-void

    .line 2637
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$37;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$48(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Z)V

    .line 2640
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$37;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->invalidateViews()V

    goto :goto_0

    .line 2635
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 2652
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2658
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2662
    const/4 v0, 0x0

    return v0
.end method
