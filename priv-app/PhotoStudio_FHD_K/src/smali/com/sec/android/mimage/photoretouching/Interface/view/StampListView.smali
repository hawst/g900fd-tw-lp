.class public Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;
.super Landroid/widget/LinearLayout;
.source "StampListView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;
    }
.end annotation


# static fields
.field public static final FRAME_RECENT:I = 0x2004

.field public static final FRAME_TYPE1:I = 0x2001

.field public static final FRAME_TYPE2:I = 0x2002

.field public static final FRAME_TYPE3:I = 0x2003


# instance fields
.field public final FRAME_THREAD_MAX_NUM:I

.field public final FRAME_TYPE1_MAX_NUM:I

.field public final FRAME_TYPE2_MAX_NUM:I

.field public final FRAME_TYPE3_MAX_NUM:I

.field id:[I

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;

.field private mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

.field private mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

.field mContext:Landroid/content/Context;

.field private mFirstTouchPoint:Landroid/graphics/PointF;

.field private mFlipperLinearLayout:Landroid/widget/FrameLayout;

.field public mPageImage:[Landroid/widget/ImageView;

.field public mPageLayout:[Landroid/widget/LinearLayout;

.field mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

.field private mResId:I

.field private mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

.field private mStampView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

.field public mViewPager:Landroid/support/v4/view/ViewPager;

.field private mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

.field public mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 242
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 243
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;

    .line 244
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mFirstTouchPoint:Landroid/graphics/PointF;

    .line 246
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 248
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->id:[I

    .line 250
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 251
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mPageLayout:[Landroid/widget/LinearLayout;

    .line 252
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mPageImage:[Landroid/widget/ImageView;

    .line 254
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mStampView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .line 256
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mContext:Landroid/content/Context;

    .line 262
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->FRAME_TYPE1_MAX_NUM:I

    .line 263
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->FRAME_TYPE2_MAX_NUM:I

    .line 264
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->FRAME_TYPE3_MAX_NUM:I

    .line 265
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->FRAME_THREAD_MAX_NUM:I

    .line 266
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I

    .line 267
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .line 269
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    .line 271
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    .line 272
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    .line 273
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    .line 274
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    .line 35
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mContext:Landroid/content/Context;

    .line 36
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->initView()V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/StampEffect;Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "stampEffect"    # Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    .param p3, "stampview"    # Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 242
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 243
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;

    .line 244
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mFirstTouchPoint:Landroid/graphics/PointF;

    .line 246
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 248
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->id:[I

    .line 250
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 251
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mPageLayout:[Landroid/widget/LinearLayout;

    .line 252
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mPageImage:[Landroid/widget/ImageView;

    .line 254
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mStampView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .line 256
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mContext:Landroid/content/Context;

    .line 262
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->FRAME_TYPE1_MAX_NUM:I

    .line 263
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->FRAME_TYPE2_MAX_NUM:I

    .line 264
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->FRAME_TYPE3_MAX_NUM:I

    .line 265
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->FRAME_THREAD_MAX_NUM:I

    .line 266
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I

    .line 267
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .line 269
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    .line 271
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    .line 272
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    .line 273
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    .line 274
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    .line 40
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mContext:Landroid/content/Context;

    .line 41
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mStampView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .line 42
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    .line 43
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->initView()V

    .line 44
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)I
    .locals 1

    .prologue
    .line 266
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    return-object v0
.end method

.method private getCurrentPage()I
    .locals 4

    .prologue
    const v3, 0x3150008b

    .line 101
    const/4 v0, 0x0

    .line 102
    .local v0, "pageNum":I
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I

    if-lt v1, v3, :cond_1

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I

    const v2, 0x3150009f

    if-gt v1, v2, :cond_1

    .line 104
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I

    sub-int v0, v1, v3

    .line 110
    :cond_0
    :goto_0
    return v0

    .line 106
    :cond_1
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I

    const v2, 0x315000a0

    if-lt v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I

    const v2, 0x315000ac

    if-gt v1, v2, :cond_0

    .line 108
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I

    sub-int/2addr v1, v3

    add-int/lit8 v0, v1, -0x15

    goto :goto_0
.end method

.method private initView()V
    .locals 6

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mStampView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getResId()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "KHJ initView() mResId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    .line 88
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->getCurrentPage()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mResId:I

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;-><init>(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;ILcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    .line 89
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getLayout()Landroid/widget/FrameLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    .line 90
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 91
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->addView(Landroid/view/View;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getBitmapListClass()Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    .line 94
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mStampView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mStampView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getActionBar()Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 96
    :cond_0
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;

    .line 97
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mFirstTouchPoint:Landroid/graphics/PointF;

    .line 98
    return-void
.end method


# virtual methods
.method public Release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 178
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->Release()V

    .line 180
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    .line 181
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 183
    :cond_1
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    .line 184
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_2

    .line 185
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->removeAllViews()V

    .line 186
    :cond_2
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 187
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 188
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 115
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 145
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1

    .line 117
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    if-eqz v1, :cond_1

    .line 118
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->actionDown()V

    .line 120
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mFirstTouchPoint:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 125
    :pswitch_2
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {v0, v1, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 126
    .local v0, "curPt":Landroid/graphics/PointF;
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mFirstTouchPoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v4

    float-to-double v4, v1

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    iget v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mFirstTouchPoint:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v6

    float-to-double v6, v1

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 127
    .local v2, "distance":D
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    .line 129
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;

    if-eqz v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 132
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->hide()V

    goto :goto_0

    .line 134
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->show()V

    goto :goto_0

    .line 139
    .end local v0    # "curPt":Landroid/graphics/PointF;
    .end local v2    # "distance":D
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mFirstTouchPoint:Landroid/graphics/PointF;

    invoke-virtual {v1, v4, v4}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 115
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public getFrameAdapter()Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    return-object v0
.end method

.method public getmViewPager()Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method public hideColorpicker()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 198
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v0, v1, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->show(ZZ)Z

    .line 202
    :cond_0
    return-void
.end method

.method public onConfigurationChanged()V
    .locals 3

    .prologue
    .line 161
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 162
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->showColorpicker()V

    .line 166
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 168
    .local v0, "curPage":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getPagerAdapter()Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 169
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 174
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 150
    const/4 v0, 0x0

    return v0
.end method
