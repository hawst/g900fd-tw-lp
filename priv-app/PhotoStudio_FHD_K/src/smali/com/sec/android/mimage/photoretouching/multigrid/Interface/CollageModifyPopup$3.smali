.class Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$3;
.super Ljava/lang/Object;
.source "CollageModifyPopup.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->show(ZZ)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    .line 228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const/16 v2, 0x8

    .line 244
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;)Landroid/widget/LinearLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 246
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 248
    .local v0, "viewGroup":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 250
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 253
    .end local v0    # "viewGroup":Landroid/view/ViewGroup;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->setVisibility(I)V

    .line 254
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 255
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->access$1(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;Z)V

    .line 256
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 240
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->access$1(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;Z)V

    .line 234
    return-void
.end method
