.class public interface abstract Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$ExposureMode;
.super Ljava/lang/Object;
.source "GalleryExifInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ExposureMode"
.end annotation


# static fields
.field public static final AUTO_BRACKET:S = 0x2s

.field public static final AUTO_EXPOSURE:S = 0x0s

.field public static final MANUAL_EXPOSURE:S = 0x1s
