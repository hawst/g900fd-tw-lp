.class public Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;
.super Landroid/widget/LinearLayout;
.source "FrameButtonLayout.java"


# instance fields
.field private mBackground:Landroid/graphics/Bitmap;

.field private mButtonId:I

.field private mButtonType:I

.field private mContext:Landroid/content/Context;

.field private mDecoManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

.field private mId:I

.field private mLayout:Landroid/view/ViewGroup;

.field private mSelected:Z

.field private mStampThumb:[I

.field private mTempThumb:[I

.field private mThumbnail:Landroid/graphics/Bitmap;

.field private mThumbnailHeight:I

.field private mThumbnailWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v3, 0x7f03006c

    const v2, 0x7f030043

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 538
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mSelected:Z

    .line 539
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mButtonId:I

    .line 540
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mButtonType:I

    .line 541
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mContext:Landroid/content/Context;

    .line 542
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mBackground:Landroid/graphics/Bitmap;

    .line 543
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    .line 544
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mTempThumb:[I

    .line 545
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mStampThumb:[I

    .line 546
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailWidth:I

    .line 547
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailHeight:I

    .line 548
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 550
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mId:I

    .line 551
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mDecoManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 34
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mContext:Landroid/content/Context;

    .line 35
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 48
    :goto_0
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setGravity(I)V

    .line 51
    return-void

    .line 38
    :sswitch_0
    invoke-static {p1, v2, p0}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 39
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mButtonId:I

    goto :goto_0

    .line 42
    :sswitch_1
    invoke-static {p1, v3, p0}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 43
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mButtonId:I

    goto :goto_0

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x31200000 -> :sswitch_0
        0x31500000 -> :sswitch_1
    .end sparse-switch
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mDecoManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getFrameResId()I
    .locals 2

    .prologue
    .line 407
    const/4 v0, 0x0

    .line 408
    .local v0, "resId":I
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mButtonType:I

    packed-switch v1, :pswitch_data_0

    .line 501
    :goto_0
    return v0

    .line 414
    :pswitch_0
    const v0, 0x7f0201d3

    .line 415
    goto :goto_0

    .line 417
    :pswitch_1
    const v0, 0x7f0201d4

    .line 418
    goto :goto_0

    .line 420
    :pswitch_2
    const v0, 0x7f0201d5

    .line 421
    goto :goto_0

    .line 423
    :pswitch_3
    const v0, 0x7f0201d6

    .line 424
    goto :goto_0

    .line 426
    :pswitch_4
    const v0, 0x7f0201d7

    .line 427
    goto :goto_0

    .line 429
    :pswitch_5
    const v0, 0x7f0201d8

    .line 430
    goto :goto_0

    .line 432
    :pswitch_6
    const v0, 0x7f0201d9

    .line 433
    goto :goto_0

    .line 435
    :pswitch_7
    const v0, 0x7f0201da

    .line 436
    goto :goto_0

    .line 438
    :pswitch_8
    const v0, 0x7f0201db

    .line 439
    goto :goto_0

    .line 441
    :pswitch_9
    const v0, 0x7f0201dc

    .line 442
    goto :goto_0

    .line 444
    :pswitch_a
    const v0, 0x7f0203af

    .line 445
    goto :goto_0

    .line 447
    :pswitch_b
    const v0, 0x7f0203ae

    .line 448
    goto :goto_0

    .line 450
    :pswitch_c
    const v0, 0x7f0203ab

    .line 451
    goto :goto_0

    .line 453
    :pswitch_d
    const v0, 0x7f0203ac

    .line 454
    goto :goto_0

    .line 456
    :pswitch_e
    const v0, 0x7f0203ad

    .line 457
    goto :goto_0

    .line 459
    :pswitch_f
    const v0, 0x7f0203aa

    .line 460
    goto :goto_0

    .line 462
    :pswitch_10
    const v0, 0x7f0203a9

    .line 463
    goto :goto_0

    .line 465
    :pswitch_11
    const v0, 0x7f0203b1

    .line 466
    goto :goto_0

    .line 468
    :pswitch_12
    const v0, 0x7f0203b3

    .line 469
    goto :goto_0

    .line 471
    :pswitch_13
    const v0, 0x7f0203b5

    .line 472
    goto :goto_0

    .line 474
    :pswitch_14
    const v0, 0x7f0203b7

    .line 475
    goto :goto_0

    .line 477
    :pswitch_15
    const v0, 0x7f0203b9

    .line 478
    goto :goto_0

    .line 480
    :pswitch_16
    const v0, 0x7f0203bb

    .line 481
    goto :goto_0

    .line 483
    :pswitch_17
    const v0, 0x7f0203bd

    .line 484
    goto :goto_0

    .line 486
    :pswitch_18
    const v0, 0x7f0203bf

    .line 487
    goto :goto_0

    .line 489
    :pswitch_19
    const v0, 0x7f0203c1

    .line 490
    goto :goto_0

    .line 492
    :pswitch_1a
    const v0, 0x7f0203c3

    .line 493
    goto :goto_0

    .line 495
    :pswitch_1b
    const v0, 0x7f0203c5

    .line 496
    goto :goto_0

    .line 498
    :pswitch_1c
    const v0, 0x7f0203c7

    goto :goto_0

    .line 408
    nop

    :pswitch_data_0
    .packed-switch 0x3120006e
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch
.end method


# virtual methods
.method public applyEffectThumbnail([III)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "in"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 347
    const/4 v0, 0x0

    .line 349
    .local v0, "ret":Landroid/graphics/Bitmap;
    mul-int v3, p2, p3

    new-array v1, v3, [I

    .line 350
    .local v1, "out":[I
    mul-int v3, p2, p3

    new-array v8, v3, [B

    .line 352
    .local v8, "mask":[B
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 353
    .local v9, "roi":Landroid/graphics/Rect;
    invoke-virtual {v9, p2, p3, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 354
    const-string v3, "applyEffectThumbnail"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 355
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "JW mButtonType="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mButtonType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 385
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 394
    :goto_0
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    move v3, p2

    move v4, v2

    move v5, v2

    move v6, p2

    move v7, p3

    .line 395
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 397
    const/4 p1, 0x0

    .line 398
    const/4 v1, 0x0

    .line 399
    const/4 v8, 0x0

    .line 400
    const/4 v9, 0x0

    .line 402
    return-object v0

    .line 388
    :sswitch_0
    mul-int v3, p2, p3

    invoke-static {p1, v2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 391
    :sswitch_1
    mul-int v3, p2, p3

    invoke-static {p1, v2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 385
    nop

    :sswitch_data_0
    .sparse-switch
        0x31200000 -> :sswitch_0
        0x31500000 -> :sswitch_1
    .end sparse-switch
.end method

.method public configurationChanged()V
    .locals 2

    .prologue
    .line 528
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->removeAllViews()V

    .line 529
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mButtonId:I

    invoke-static {v0, v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 530
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setFocusable(Z)V

    .line 531
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mSelected:Z

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setSelected(Z)V

    .line 532
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mSelected:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mSelected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 533
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mButtonType:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->initFrame(I)Ljava/util/ArrayList;

    .line 535
    const-string v0, "JW FrameButtonLayout configChanged"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 536
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 287
    const v1, 0x7f090004

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 288
    .local v0, "icon":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 290
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 292
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 294
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 296
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 297
    const-string v1, "JW mThumbnail.recycle()"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 300
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mBackground:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 302
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 304
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 305
    const-string v1, "JW mBackground.recycle()"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 309
    :cond_2
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    .line 310
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mBackground:Landroid/graphics/Bitmap;

    .line 311
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mTempThumb:[I

    .line 312
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mStampThumb:[I

    .line 313
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 314
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 316
    const-string v1, "JW frameButton destroy"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 317
    return-void
.end method

.method public getLayoutHeight()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 329
    const/4 v1, 0x1

    .line 330
    .local v1, "ret":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 331
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 332
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 334
    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    return v1
.end method

.method public getLayoutWidth()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 320
    const/4 v1, 0x1

    .line 321
    .local v1, "ret":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 322
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 323
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 325
    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    return v1
.end method

.method public getThumbnail()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getThumbnailBuffer()[I
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mTempThumb:[I

    return-object v0
.end method

.method public getThumbnailHeight()I
    .locals 1

    .prologue
    .line 342
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailHeight:I

    return v0
.end method

.method public getThumbnailId()I
    .locals 1

    .prologue
    .line 509
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mId:I

    return v0
.end method

.method public getThumbnailWidth()I
    .locals 1

    .prologue
    .line 338
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailWidth:I

    return v0
.end method

.method public initFrame(I)Ljava/util/ArrayList;
    .locals 11
    .param p1, "buttonType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 86
    const-string v6, "JW initFrame"

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 87
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mButtonType:I

    .line 88
    const/4 v0, 0x0

    .line 89
    .local v0, "bm":Landroid/graphics/Bitmap;
    const/4 v5, 0x0

    .line 91
    .local v5, "temp":Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 105
    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mTempThumb:[I

    if-eqz v6, :cond_1

    .line 107
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mTempThumb:[I

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailWidth:I

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailHeight:I

    invoke-virtual {p0, v6, v7, v8}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->applyEffectThumbnail([III)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 111
    :cond_1
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailWidth:I

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailHeight:I

    const/4 v8, 0x1

    invoke-static {v0, v6, v7, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 112
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailWidth:I

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailHeight:I

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 113
    .local v2, "mBitmap":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 114
    .local v3, "mTempCanvas":Landroid/graphics/Canvas;
    invoke-virtual {v3, v5, v9, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 115
    invoke-virtual {v3, v0, v9, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 116
    iput-object v10, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mTempThumb:[I

    .line 117
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 118
    const/4 v5, 0x0

    .line 119
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 120
    const/4 v0, 0x0

    .line 121
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 122
    .local v1, "mArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    new-instance v7, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;

    invoke-direct {v7, p0}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;)V

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 242
    return-object v1

    .line 95
    .end local v1    # "mArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    .end local v2    # "mBitmap":Landroid/graphics/Bitmap;
    .end local v3    # "mTempCanvas":Landroid/graphics/Canvas;
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mTempThumb:[I

    if-eqz v6, :cond_0

    .line 97
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->getFrameResId()I

    move-result v4

    .line 98
    .local v4, "resId":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v6, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x31200000
        :pswitch_0
    .end packed-switch
.end method

.method public initStamp(I[I)Ljava/util/ArrayList;
    .locals 13
    .param p1, "buttonType"    # I
    .param p2, "bitmap"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[I)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 247
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mButtonType:I

    .line 248
    const/4 v10, 0x0

    .line 251
    .local v10, "bm":Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 272
    :cond_0
    :goto_0
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 273
    .local v11, "mArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailWidth:I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 274
    .local v12, "mBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 275
    .local v0, "mTempCanvas":Landroid/graphics/Canvas;
    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailWidth:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailWidth:I

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailHeight:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 276
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v10, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 277
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mStampThumb:[I

    .line 278
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 279
    const/4 v10, 0x0

    .line 280
    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 282
    return-object v11

    .line 254
    .end local v0    # "mTempCanvas":Landroid/graphics/Canvas;
    .end local v11    # "mArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    .end local v12    # "mBitmap":Landroid/graphics/Bitmap;
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mStampThumb:[I

    if-eqz v1, :cond_0

    .line 257
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mStampThumb:[I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailWidth:I

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailHeight:I

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->applyEffectThumbnail([III)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 260
    const-string v1, "JW applyEffectThumbnail done"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_0

    .line 251
    :pswitch_data_0
    .packed-switch 0x31500000
        :pswitch_0
    .end packed-switch
.end method

.method public isFocused()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 523
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 524
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    .line 525
    :cond_0
    return v0
.end method

.method public setBackgroundBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 68
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mBackground:Landroid/graphics/Bitmap;

    .line 69
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 70
    return-void
.end method

.method public setSelected(Z)V
    .locals 2
    .param p1, "selected"    # Z

    .prologue
    const/4 v1, 0x0

    .line 513
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mSelected:Z

    .line 515
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 516
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setSelected(Z)V

    .line 518
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 519
    return-void
.end method

.method public setStampThumbnail([III)V
    .locals 0
    .param p1, "thumbnail"    # [I
    .param p2, "thumbnailWidth"    # I
    .param p3, "thumbnailHeight"    # I

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mStampThumb:[I

    .line 62
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailWidth:I

    .line 63
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailHeight:I

    .line 64
    return-void
.end method

.method public setThumbnail([III)V
    .locals 0
    .param p1, "thumbnail"    # [I
    .param p2, "thumbnailWidth"    # I
    .param p3, "thumbnailHeight"    # I

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mTempThumb:[I

    .line 56
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailWidth:I

    .line 57
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnailHeight:I

    .line 58
    return-void
.end method

.method public setThumbnailBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 73
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    .line 74
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 75
    return-void
.end method

.method public setThumbnailId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 505
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mId:I

    .line 506
    return-void
.end method
