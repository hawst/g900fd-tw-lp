.class Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;
.super Ljava/lang/Object;
.source "MyButton.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->onkey()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 87
    const/16 v0, 0x42

    if-eq p2, v0, :cond_0

    const/16 v0, 0x17

    if-ne p2, v0, :cond_1

    .line 89
    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 104
    :cond_1
    :goto_0
    return v1

    .line 91
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isSelected()Z

    move-result v3

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;Z)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setPressed(Z)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mBtnThumnail:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setPressed(Z)V

    goto :goto_0

    .line 96
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/widget/CheckBox;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mIsSelected:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setSelected(Z)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/widget/CheckBox;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mIsSelected:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setPressed(Z)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mDoneBtn:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$4(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mDoneBtn:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$4(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 96
    goto :goto_1

    :cond_3
    move v0, v2

    .line 97
    goto :goto_2

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
