.class public abstract Lcom/sec/android/mimage/photoretouching/Core/EffectInfoInterface;
.super Ljava/lang/Object;
.source "EffectInfoInterface.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onCallback"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    .param p3, "onActionbarCallback"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;
    .param p4, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method


# virtual methods
.method public abstract applyOriginal()[I
.end method

.method public abstract applyPreview()I
.end method

.method public abstract configurationChange()V
.end method

.method public abstract destroy()V
.end method

.method public abstract draw(Landroid/graphics/Canvas;)V
.end method

.method public abstract getMutexOn()Z
.end method

.method public abstract init(I)V
.end method

.method public abstract isStartInitializing()Z
.end method

.method public abstract setStep(I)V
.end method

.method public abstract touch(Landroid/view/MotionEvent;)Z
.end method
