.class public Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;
.super Ljava/lang/Object;
.source "SeekbarManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;
    }
.end annotation


# instance fields
.field private final AIRBRUSHFACE_INIT:I

.field private final AIRBRUSHFACE_MAX:I

.field private final AIRBRUSHFACE_MIN:I

.field private final COLOR_INIT:I

.field private final COLOR_INIT_RGB:I

.field private final COLOR_MAX:I

.field private final COLOR_MAX_RGB:I

.field private final COLOR_MIN:I

.field private final COLOR_MIN_RGB:I

.field private final EFFECT_INIT:I

.field private final EFFECT_MAX:I

.field private final EFFECT_MIN:I

.field private final FACEBRIGHTNESS_INIT:I

.field private final FACEBRIGHTNESS_MAX:I

.field private final FACEBRIGHTNESS_MIN:I

.field private final NORMAL_MAX:I

.field private final NORMAL_MIN:I

.field private final OUTOFFOCUS_INIT:I

.field private final OUTOFFOCUS_MAX:I

.field private final OUTOFFOCUS_MIN:I

.field private final PORTRAIT_INIT:I

.field private final PORTRAIT_MAX:I

.field private final PORTRAIT_MIN:I

.field private final RATIO:F

.field private final START_SEEK:I

.field private final STOP_SEEK:I

.field private mAlphaPaint:Landroid/graphics/Paint;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmap_Divider:Landroid/graphics/Bitmap;

.field private mBitmap_bg:Landroid/graphics/Bitmap;

.field private mBitmap_masking:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mCurrentStep:F

.field private mEffectType:I

.field private mGauge_progress:Landroid/graphics/Bitmap;

.field private mGauge_progress_r:Landroid/graphics/Bitmap;

.field private mMax:I

.field private mMin:I

.field private mMode:I

.field private mPreX:F

.field private mProgressInterface:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;

.field private mProgressLeftMagine:F

.field private mType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I
    .param p3, "progressInterface"    # Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;

    .prologue
    const/4 v4, 0x5

    const/high16 v3, 0x42c80000    # 100.0f

    const/4 v0, 0x0

    const/16 v2, 0x64

    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 834
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    .line 835
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 836
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_masking:Landroid/graphics/Bitmap;

    .line 837
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_Divider:Landroid/graphics/Bitmap;

    .line 838
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress:Landroid/graphics/Bitmap;

    .line 839
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    .line 841
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mPreX:F

    .line 843
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 844
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 845
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mType:I

    .line 847
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMode:I

    .line 849
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mEffectType:I

    .line 851
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->RATIO:F

    .line 853
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->NORMAL_MAX:I

    .line 854
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->NORMAL_MIN:I

    .line 856
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->COLOR_MAX:I

    .line 857
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->COLOR_MIN:I

    .line 858
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->COLOR_INIT:I

    .line 860
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->COLOR_MAX_RGB:I

    .line 861
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->COLOR_MIN_RGB:I

    .line 862
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->COLOR_INIT_RGB:I

    .line 864
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->EFFECT_MAX:I

    .line 865
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->EFFECT_MIN:I

    .line 866
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->EFFECT_INIT:I

    .line 868
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->PORTRAIT_MAX:I

    .line 869
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->PORTRAIT_MIN:I

    .line 870
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->PORTRAIT_INIT:I

    .line 872
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->FACEBRIGHTNESS_MAX:I

    .line 873
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->FACEBRIGHTNESS_MIN:I

    .line 874
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->FACEBRIGHTNESS_INIT:I

    .line 876
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->AIRBRUSHFACE_MAX:I

    .line 877
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->AIRBRUSHFACE_MIN:I

    .line 878
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->AIRBRUSHFACE_INIT:I

    .line 880
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->OUTOFFOCUS_MAX:I

    .line 881
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->OUTOFFOCUS_MIN:I

    .line 882
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->OUTOFFOCUS_INIT:I

    .line 884
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->STOP_SEEK:I

    .line 885
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->START_SEEK:I

    .line 887
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressLeftMagine:F

    .line 56
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    .line 57
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressInterface:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;

    .line 58
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mType:I

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    .line 62
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMode:I

    .line 64
    sparse-switch p2, :sswitch_data_0

    .line 91
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->setGaugeBitmap()V

    .line 92
    return-void

    .line 67
    :sswitch_0
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 68
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 69
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0

    .line 72
    :sswitch_1
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 73
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 74
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0

    .line 77
    :sswitch_2
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 78
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 79
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0

    .line 84
    :sswitch_3
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 85
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 86
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    .line 87
    const-string v0, "JW create seekbarManager normalView/cropView"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_0

    .line 64
    nop

    :sswitch_data_0
    .sparse-switch
        0x10000000 -> :sswitch_3
        0x11000000 -> :sswitch_3
        0x11200000 -> :sswitch_3
        0x15000000 -> :sswitch_0
        0x16000000 -> :sswitch_1
        0x18000000 -> :sswitch_2
    .end sparse-switch
.end method

.method private calcStep(F)V
    .locals 4
    .param p1, "x"    # F

    .prologue
    .line 739
    const/high16 v1, 0x42c80000    # 100.0f

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 740
    .local v0, "ratio_margin":F
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    .line 741
    const/high16 v0, 0x3f800000    # 1.0f

    .line 742
    :cond_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mPreX:F

    sub-float v2, p1, v2

    const/high16 v3, 0x41200000    # 10.0f

    mul-float/2addr v3, v0

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    .line 744
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 745
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    .line 748
    :cond_1
    :goto_0
    return-void

    .line 746
    :cond_2
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 747
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0
.end method

.method private calcZoomStep(F)V
    .locals 2
    .param p1, "scale"    # F

    .prologue
    .line 752
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    .line 754
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 755
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    .line 760
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 761
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    add-int/lit8 v0, v0, 0x1

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    .line 765
    :cond_1
    return-void

    .line 756
    :cond_2
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 757
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0
.end method

.method private drawColorGauge(Landroid/graphics/Canvas;I)V
    .locals 15
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "effectmode"    # I

    .prologue
    .line 539
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f050255

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 540
    .local v10, "topMargine":I
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f050256

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 541
    .local v7, "progressWidth":I
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f050257

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 542
    .local v6, "progressHeight":I
    int-to-float v11, v7

    const/high16 v12, 0x42c80000    # 100.0f

    div-float v9, v11, v12

    .line 543
    .local v9, "stepRatio":F
    const/4 v4, 0x0

    .line 544
    .local v4, "dstWidth":F
    move/from16 v0, p2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mEffectType:I

    .line 546
    new-instance v5, Landroid/graphics/Paint;

    const/4 v11, 0x1

    invoke-direct {v5, v11}, Landroid/graphics/Paint;-><init>(I)V

    .line 547
    .local v5, "paint":Landroid/graphics/Paint;
    const/4 v11, 0x1

    invoke-virtual {v5, v11}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 549
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 550
    new-instance v1, Landroid/graphics/Canvas;

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v11}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 552
    .local v1, "colorCanvas":Landroid/graphics/Canvas;
    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mEffectType:I

    sparse-switch v11, :sswitch_data_0

    .line 613
    :goto_0
    return-void

    .line 559
    :sswitch_0
    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    const/high16 v12, 0x42480000    # 50.0f

    sub-float/2addr v11, v12

    mul-float v4, v11, v9

    .line 562
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_Divider:Landroid/graphics/Bitmap;

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    int-to-float v12, v12

    int-to-float v13, v10

    iget-object v14, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12, v13, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 565
    new-instance v8, Landroid/graphics/Rect;

    const/4 v11, 0x0

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    iget-object v14, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    invoke-direct {v8, v11, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 567
    .local v8, "src":Landroid/graphics/Rect;
    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    const/high16 v12, 0x42480000    # 50.0f

    cmpl-float v11, v11, v12

    if-lez v11, :cond_0

    .line 568
    new-instance v3, Landroid/graphics/RectF;

    div-int/lit8 v11, v7, 0x2

    int-to-float v11, v11

    const/4 v12, 0x0

    div-int/lit8 v13, v7, 0x2

    int-to-float v13, v13

    add-float/2addr v13, v4

    int-to-float v14, v6

    invoke-direct {v3, v11, v12, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 569
    .local v3, "dst":Landroid/graphics/RectF;
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v11, v8, v3, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 581
    :goto_1
    new-instance v11, Landroid/graphics/PorterDuffXfermode;

    sget-object v12, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v11, v12}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v11}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 582
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_masking:Landroid/graphics/Bitmap;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v1, v11, v12, v13, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 585
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    iget v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressLeftMagine:F

    int-to-float v13, v10

    iget-object v14, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12, v13, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 587
    const/4 v1, 0x0

    .line 588
    const/16 p1, 0x0

    .line 589
    goto :goto_0

    .line 571
    .end local v3    # "dst":Landroid/graphics/RectF;
    :cond_0
    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    const/high16 v12, 0x42480000    # 50.0f

    cmpg-float v11, v11, v12

    if-gez v11, :cond_1

    .line 572
    new-instance v3, Landroid/graphics/RectF;

    div-int/lit8 v11, v7, 0x2

    int-to-float v11, v11

    add-float/2addr v11, v4

    const/4 v12, 0x0

    div-int/lit8 v13, v7, 0x2

    int-to-float v13, v13

    int-to-float v14, v6

    invoke-direct {v3, v11, v12, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 573
    .restart local v3    # "dst":Landroid/graphics/RectF;
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v11, v8, v3, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 576
    .end local v3    # "dst":Landroid/graphics/RectF;
    :cond_1
    new-instance v3, Landroid/graphics/RectF;

    div-int/lit8 v11, v7, 0x2

    int-to-float v11, v11

    const/4 v12, 0x0

    div-int/lit8 v13, v7, 0x2

    int-to-float v13, v13

    int-to-float v14, v6

    invoke-direct {v3, v11, v12, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 577
    .restart local v3    # "dst":Landroid/graphics/RectF;
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v11, v8, v3, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 593
    .end local v3    # "dst":Landroid/graphics/RectF;
    .end local v8    # "src":Landroid/graphics/Rect;
    :sswitch_1
    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    mul-float v4, v11, v9

    .line 594
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 595
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v11}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 598
    .local v2, "colorRGBCanvas":Landroid/graphics/Canvas;
    new-instance v8, Landroid/graphics/Rect;

    const/4 v11, 0x0

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    iget-object v14, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    invoke-direct {v8, v11, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 599
    .restart local v8    # "src":Landroid/graphics/Rect;
    new-instance v3, Landroid/graphics/RectF;

    const/4 v11, 0x0

    const/4 v12, 0x0

    int-to-float v13, v6

    invoke-direct {v3, v11, v12, v4, v13}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 600
    .restart local v3    # "dst":Landroid/graphics/RectF;
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    const/4 v12, 0x0

    invoke-virtual {v2, v11, v8, v3, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 603
    new-instance v11, Landroid/graphics/PorterDuffXfermode;

    sget-object v12, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v11, v12}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v11}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 604
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_masking:Landroid/graphics/Bitmap;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v2, v11, v12, v13, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 607
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    iget v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressLeftMagine:F

    int-to-float v13, v10

    iget-object v14, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12, v13, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 608
    const/4 v2, 0x0

    .line 609
    const/16 p1, 0x0

    goto/16 :goto_0

    .line 552
    :sswitch_data_0
    .sparse-switch
        0x15001500 -> :sswitch_0
        0x15001501 -> :sswitch_0
        0x15001502 -> :sswitch_0
        0x15001503 -> :sswitch_0
        0x15001504 -> :sswitch_0
        0x15001505 -> :sswitch_0
        0x15001607 -> :sswitch_1
        0x15001608 -> :sswitch_1
        0x15001609 -> :sswitch_1
    .end sparse-switch
.end method

.method private drawEffetGauge(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 616
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f050255

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 617
    .local v8, "topMargine":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f050256

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 618
    .local v5, "progressWidth":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f050257

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 619
    .local v4, "progressHeight":I
    int-to-float v9, v5

    const/high16 v10, 0x42c80000    # 100.0f

    div-float v7, v9, v10

    .line 620
    .local v7, "stepRatio":F
    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    mul-float v1, v9, v7

    .line 622
    .local v1, "dstWidth":F
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9, v12}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 623
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 625
    .local v2, "effectCanvas":Landroid/graphics/Canvas;
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v13}, Landroid/graphics/Paint;-><init>(I)V

    .line 626
    .local v3, "paint":Landroid/graphics/Paint;
    invoke-virtual {v3, v13}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 629
    new-instance v6, Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-direct {v6, v12, v12, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 631
    .local v6, "src":Landroid/graphics/Rect;
    new-instance v0, Landroid/graphics/RectF;

    int-to-float v9, v4

    invoke-direct {v0, v11, v11, v1, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 632
    .local v0, "dst":Landroid/graphics/RectF;
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    invoke-virtual {v2, v9, v6, v0, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 635
    new-instance v9, Landroid/graphics/PorterDuffXfermode;

    sget-object v10, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v9, v10}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 636
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_masking:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v9, v11, v11, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 639
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    iget v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressLeftMagine:F

    int-to-float v11, v8

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 641
    const/4 v2, 0x0

    .line 642
    return-void
.end method

.method private drawPortraitGauge(Landroid/graphics/Canvas;)V
    .locals 16
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 645
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f050255

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 646
    .local v11, "topMargine":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f050256

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 647
    .local v8, "progressWidth":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f050257

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 648
    .local v7, "progressHeight":I
    const/4 v4, 0x0

    .line 650
    .local v4, "maxStep":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v12

    packed-switch v12, :pswitch_data_0

    .line 670
    :cond_0
    int-to-float v12, v8

    int-to-float v13, v4

    div-float v10, v12, v13

    .line 671
    .local v10, "stepRatio":F
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    mul-float v2, v12, v10

    .line 673
    .local v2, "dstWidth":F
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 674
    new-instance v6, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v6, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 676
    .local v6, "portraitCanvas":Landroid/graphics/Canvas;
    new-instance v5, Landroid/graphics/Paint;

    const/4 v12, 0x1

    invoke-direct {v5, v12}, Landroid/graphics/Paint;-><init>(I)V

    .line 677
    .local v5, "paint":Landroid/graphics/Paint;
    const/4 v12, 0x1

    invoke-virtual {v5, v12}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 680
    new-instance v9, Landroid/graphics/Rect;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    invoke-direct {v9, v12, v13, v14, v15}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 682
    .local v9, "src":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/RectF;

    const/4 v12, 0x0

    const/4 v13, 0x0

    int-to-float v14, v7

    invoke-direct {v1, v12, v13, v2, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 683
    .local v1, "dst":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    const/4 v13, 0x0

    invoke-virtual {v6, v12, v9, v1, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 686
    new-instance v12, Landroid/graphics/PorterDuffXfermode;

    sget-object v13, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v12, v13}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v12}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 687
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_masking:Landroid/graphics/Bitmap;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v6, v12, v13, v14, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 690
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressLeftMagine:F

    int-to-float v14, v11

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13, v14, v15}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 692
    return-void

    .line 652
    .end local v1    # "dst":Landroid/graphics/RectF;
    .end local v2    # "dstWidth":F
    .end local v5    # "paint":Landroid/graphics/Paint;
    .end local v6    # "portraitCanvas":Landroid/graphics/Canvas;
    .end local v9    # "src":Landroid/graphics/Rect;
    .end local v10    # "stepRatio":F
    :pswitch_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/4 v12, 0x4

    if-ge v3, v12, :cond_0

    .line 654
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mEffectType:I

    packed-switch v12, :pswitch_data_1

    .line 652
    :goto_1
    :pswitch_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 658
    :pswitch_2
    const/16 v4, 0x14

    .line 659
    goto :goto_1

    .line 661
    :pswitch_3
    const/4 v4, 0x5

    .line 662
    goto :goto_1

    .line 664
    :pswitch_4
    const/4 v4, 0x7

    goto :goto_1

    .line 650
    :pswitch_data_0
    .packed-switch 0x18000000
        :pswitch_0
    .end packed-switch

    .line 654
    :pswitch_data_1
    .packed-switch 0x17001700
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private drawProgressText(Landroid/graphics/Canvas;I)V
    .locals 18
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "effectmode"    # I

    .prologue
    .line 356
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f05025a

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 357
    .local v10, "textTopMagine":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f050259

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 358
    .local v4, "progressbarLeftMagine":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f05025b

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 359
    .local v5, "progressbarTextSize":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f05025d

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 360
    .local v6, "progressbarTextwidth":I
    const/4 v11, 0x1

    .line 361
    .local v11, "textViewMaxLine":I
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mEffectType:I

    .line 362
    const/4 v8, 0x0

    .line 364
    .local v8, "text":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mEffectType:I

    sparse-switch v14, :sswitch_data_0

    .line 490
    :goto_0
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 491
    .local v3, "paint":Landroid/graphics/Paint;
    const/4 v14, 0x1

    invoke-virtual {v3, v14}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 493
    new-instance v14, Landroid/graphics/BlurMaskFilter;

    const/high16 v15, 0x40000000    # 2.0f

    sget-object v16, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct/range {v14 .. v16}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    invoke-virtual {v3, v14}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 494
    new-instance v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-direct {v13, v14}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 496
    .local v13, "tv":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mType:I

    sparse-switch v14, :sswitch_data_1

    .line 514
    :goto_1
    invoke-virtual {v13, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 515
    const/4 v14, -0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 516
    const/4 v14, 0x0

    int-to-float v15, v5

    invoke-virtual {v13, v14, v15}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 517
    const-string v14, "sec-roboto-light"

    const/4 v15, 0x0

    invoke-static {v14, v15}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    .line 518
    .local v2, "font":Landroid/graphics/Typeface;
    invoke-virtual {v13, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 519
    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v15, 0x0

    const/high16 v16, 0x3f800000    # 1.0f

    const/high16 v17, -0x41000000    # -0.5f

    invoke-virtual/range {v13 .. v17}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 521
    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    .line 523
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v14

    const/high16 v15, -0x80000000

    invoke-static {v14, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    .line 524
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v15

    const/high16 v16, -0x80000000

    invoke-static/range {v15 .. v16}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    .line 523
    invoke-virtual {v13, v14, v15}, Landroid/widget/TextView;->measure(II)V

    .line 526
    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v16

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v17

    invoke-virtual/range {v13 .. v17}, Landroid/widget/TextView;->layout(IIII)V

    .line 527
    const/4 v14, 0x3

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setGravity(I)V

    .line 529
    invoke-virtual {v13}, Landroid/widget/TextView;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v9

    .line 531
    .local v9, "textBitmap":Landroid/graphics/Bitmap;
    if-eqz v9, :cond_0

    .line 532
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressLeftMagine:F

    int-to-float v15, v4

    add-float/2addr v14, v15

    int-to-float v15, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v9, v14, v15, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 535
    :cond_0
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 536
    return-void

    .line 366
    .end local v2    # "font":Landroid/graphics/Typeface;
    .end local v3    # "paint":Landroid/graphics/Paint;
    .end local v9    # "textBitmap":Landroid/graphics/Bitmap;
    .end local v13    # "tv":Landroid/widget/TextView;
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060037

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 367
    goto/16 :goto_0

    .line 369
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060038

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 370
    goto/16 :goto_0

    .line 372
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060039

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 373
    goto/16 :goto_0

    .line 375
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f06003c

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 376
    goto/16 :goto_0

    .line 378
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060042

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 379
    goto/16 :goto_0

    .line 381
    :sswitch_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060093

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 382
    goto/16 :goto_0

    .line 384
    :sswitch_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060095

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 385
    goto/16 :goto_0

    .line 387
    :sswitch_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060096

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 388
    goto/16 :goto_0

    .line 391
    :sswitch_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060044

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 392
    goto/16 :goto_0

    .line 394
    :sswitch_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060046

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 395
    goto/16 :goto_0

    .line 397
    :sswitch_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0601d0

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 398
    goto/16 :goto_0

    .line 400
    :sswitch_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060047

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 401
    goto/16 :goto_0

    .line 403
    :sswitch_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060138

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 404
    goto/16 :goto_0

    .line 406
    :sswitch_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f06004d

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 407
    goto/16 :goto_0

    .line 409
    :sswitch_e
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0600f4

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 410
    goto/16 :goto_0

    .line 412
    :sswitch_f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f06015e

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 413
    goto/16 :goto_0

    .line 415
    :sswitch_10
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060139

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 416
    goto/16 :goto_0

    .line 418
    :sswitch_11
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f06012f

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 419
    goto/16 :goto_0

    .line 421
    :sswitch_12
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0600ec

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 422
    goto/16 :goto_0

    .line 424
    :sswitch_13
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0600ef

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 425
    goto/16 :goto_0

    .line 427
    :sswitch_14
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060165

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 428
    goto/16 :goto_0

    .line 430
    :sswitch_15
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0600cc

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 431
    goto/16 :goto_0

    .line 433
    :sswitch_16
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0600c9

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 434
    goto/16 :goto_0

    .line 436
    :sswitch_17
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0600ce

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 437
    goto/16 :goto_0

    .line 439
    :sswitch_18
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f06018c

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 440
    goto/16 :goto_0

    .line 442
    :sswitch_19
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f06004c

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 443
    goto/16 :goto_0

    .line 445
    :sswitch_1a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060151

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 446
    goto/16 :goto_0

    .line 448
    :sswitch_1b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f06015a

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 449
    goto/16 :goto_0

    .line 451
    :sswitch_1c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060190

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 452
    goto/16 :goto_0

    .line 454
    :sswitch_1d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060191

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 455
    goto/16 :goto_0

    .line 457
    :sswitch_1e
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0600d5

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 458
    goto/16 :goto_0

    .line 460
    :sswitch_1f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060159

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 461
    goto/16 :goto_0

    .line 464
    :sswitch_20
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0600d2

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 465
    goto/16 :goto_0

    .line 467
    :sswitch_21
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f06012e

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 468
    goto/16 :goto_0

    .line 470
    :sswitch_22
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060050

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 471
    goto/16 :goto_0

    .line 473
    :sswitch_23
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f06013b

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 474
    goto/16 :goto_0

    .line 476
    :sswitch_24
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f06004e

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 477
    goto/16 :goto_0

    .line 479
    :sswitch_25
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f0601fb

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 480
    goto/16 :goto_0

    .line 482
    :sswitch_26
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f060055

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 483
    goto/16 :goto_0

    .line 485
    :sswitch_27
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    const v15, 0x7f06018e

    invoke-static {v14, v15}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 499
    .restart local v3    # "paint":Landroid/graphics/Paint;
    .restart local v13    # "tv":Landroid/widget/TextView;
    :sswitch_28
    invoke-virtual {v13, v6}, Landroid/widget/TextView;->setWidth(I)V

    .line 500
    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 501
    sget-object v14, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto/16 :goto_1

    .line 504
    :sswitch_29
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f050256

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 505
    .local v7, "progressbarWidth":I
    int-to-double v14, v7

    const-wide v16, 0x3fe6666666666666L    # 0.7

    mul-double v14, v14, v16

    double-to-int v12, v14

    .line 506
    .local v12, "textWidth":I
    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 507
    invoke-virtual {v13}, Landroid/widget/TextView;->setSingleLine()V

    .line 508
    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    goto/16 :goto_1

    .line 364
    nop

    :sswitch_data_0
    .sparse-switch
        0x15001500 -> :sswitch_0
        0x15001501 -> :sswitch_1
        0x15001502 -> :sswitch_2
        0x15001504 -> :sswitch_3
        0x15001505 -> :sswitch_4
        0x15001607 -> :sswitch_5
        0x15001608 -> :sswitch_6
        0x15001609 -> :sswitch_7
        0x16001600 -> :sswitch_1a
        0x16001601 -> :sswitch_8
        0x16001602 -> :sswitch_17
        0x16001603 -> :sswitch_18
        0x16001604 -> :sswitch_a
        0x16001605 -> :sswitch_b
        0x16001606 -> :sswitch_14
        0x16001607 -> :sswitch_13
        0x16001608 -> :sswitch_12
        0x16001609 -> :sswitch_11
        0x1600160a -> :sswitch_15
        0x1600160b -> :sswitch_16
        0x1600160c -> :sswitch_19
        0x1600160d -> :sswitch_d
        0x1600160e -> :sswitch_1b
        0x1600160f -> :sswitch_10
        0x16001610 -> :sswitch_f
        0x16001611 -> :sswitch_e
        0x16001612 -> :sswitch_c
        0x16001613 -> :sswitch_9
        0x16001615 -> :sswitch_20
        0x16001616 -> :sswitch_21
        0x16001618 -> :sswitch_22
        0x16001619 -> :sswitch_23
        0x1600161a -> :sswitch_24
        0x1600161b -> :sswitch_25
        0x1600161c -> :sswitch_26
        0x1600161d -> :sswitch_27
        0x17001700 -> :sswitch_1c
        0x17001701 -> :sswitch_1d
        0x17001702 -> :sswitch_1e
        0x17001703 -> :sswitch_1f
    .end sparse-switch

    .line 496
    :sswitch_data_1
    .sparse-switch
        0x15000000 -> :sswitch_28
        0x16000000 -> :sswitch_29
    .end sparse-switch
.end method

.method private setGaugeBitmap()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 696
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f050257

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 697
    .local v4, "progressHeight":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f050256

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 700
    .local v5, "progressWidth":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    .line 701
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_bg:Landroid/graphics/Bitmap;

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 702
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_masking:Landroid/graphics/Bitmap;

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_masking:Landroid/graphics/Bitmap;

    .line 703
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_Divider:Landroid/graphics/Bitmap;

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_Divider:Landroid/graphics/Bitmap;

    .line 704
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress:Landroid/graphics/Bitmap;

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress:Landroid/graphics/Bitmap;

    .line 705
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    .line 708
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v4, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    .line 711
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 712
    .local v6, "res":Landroid/content/res/Resources;
    if-eqz v6, :cond_2

    .line 714
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0203c8

    invoke-static {v7, v8}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 715
    .local v0, "gauge_bg":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 717
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 720
    :cond_0
    const v7, 0x7f0203ca

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 721
    .local v1, "gauge_bg_masking":Landroid/graphics/drawable/BitmapDrawable;
    if-eqz v1, :cond_1

    .line 722
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v7

    if-nez v7, :cond_1

    .line 723
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_masking:Landroid/graphics/Bitmap;

    .line 726
    :cond_1
    const v7, 0x7f0203c9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .line 727
    .local v2, "gauge_divider":Landroid/graphics/drawable/BitmapDrawable;
    if-eqz v2, :cond_2

    .line 728
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v7

    if-nez v7, :cond_2

    .line 729
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_Divider:Landroid/graphics/Bitmap;

    .line 731
    .end local v0    # "gauge_bg":Landroid/graphics/Bitmap;
    .end local v1    # "gauge_bg_masking":Landroid/graphics/drawable/BitmapDrawable;
    .end local v2    # "gauge_divider":Landroid/graphics/drawable/BitmapDrawable;
    :cond_2
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 732
    .local v3, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v7, 0x0

    iput-boolean v7, v3, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 733
    const v7, 0x7f0203cb

    invoke-static {v6, v7, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress:Landroid/graphics/Bitmap;

    .line 734
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress:Landroid/graphics/Bitmap;

    invoke-static {v7, v5, v4, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    .line 735
    return-void
.end method


# virtual methods
.method public Destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 809
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap:Landroid/graphics/Bitmap;

    .line 810
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_bg:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 811
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_masking:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_masking:Landroid/graphics/Bitmap;

    .line 812
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_Divider:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_Divider:Landroid/graphics/Bitmap;

    .line 813
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress:Landroid/graphics/Bitmap;

    .line 814
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mGauge_progress_r:Landroid/graphics/Bitmap;

    .line 815
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressInterface:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;

    .line 816
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    .line 825
    return-void
.end method

.method public drawProgress(Landroid/graphics/Canvas;IZI)V
    .locals 22
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "effectmode"    # I
    .param p3, "isBeforeMode"    # Z
    .param p4, "mAlpha"    # I

    .prologue
    .line 218
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMode:I

    move/from16 v18, v0

    if-nez v18, :cond_1

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    if-eqz p3, :cond_2

    .line 222
    const/16 p4, 0xff

    .line 224
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 225
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f05025b

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 226
    .local v5, "TEXT_SIZE":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f050255

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 227
    .local v16, "topMargine":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f05025a

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    .line 228
    .local v15, "textTopMagine":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f050259

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 229
    .local v9, "progressbarRightMagine":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f050256

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 230
    .local v10, "progressbarWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f050259

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 231
    .local v8, "progressbarLeftMagine":I
    const/4 v14, 0x0

    .line 232
    .local v14, "textPosRatio":I
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mEffectType:I

    .line 233
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v18

    sub-int v18, v18, v10

    div-int/lit8 v18, v18, 0x2

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressLeftMagine:F

    .line 235
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v18

    if-eqz v18, :cond_7

    .line 236
    const/4 v14, 0x6

    .line 240
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_bg:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_bg:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 242
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->setGaugeBitmap()V

    .line 246
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mPreX:F

    move/from16 v18, v0

    const/high16 v19, -0x40800000    # -1.0f

    cmpl-float v18, v18, v19

    if-eqz v18, :cond_0

    .line 249
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-int v11, v0

    .line 256
    .local v11, "step":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mEffectType:I

    move/from16 v18, v0

    sparse-switch v18, :sswitch_data_0

    .line 272
    :goto_2
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 273
    .local v7, "paint":Landroid/graphics/Paint;
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 275
    new-instance v18, Landroid/graphics/BlurMaskFilter;

    const/high16 v19, 0x40000000    # 2.0f

    sget-object v20, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct/range {v18 .. v20}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 276
    new-instance v17, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-direct/range {v17 .. v18}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 278
    .local v17, "tv":Landroid/widget/TextView;
    if-eqz p3, :cond_8

    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const v19, 0x7f0601b1

    invoke-static/range {v18 .. v19}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    .line 281
    .local v12, "tempBeforeReceiveString":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    .end local v12    # "tempBeforeReceiveString":Ljava/lang/String;
    :goto_3
    const/16 v18, -0x1

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setTextColor(I)V

    .line 293
    const/16 v18, 0x0

    int-to-float v0, v5

    move/from16 v19, v0

    invoke-virtual/range {v17 .. v19}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 294
    const-string v18, "sec-roboto-light"

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v6

    .line 295
    .local v6, "font":Landroid/graphics/Typeface;
    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 297
    const/high16 v18, 0x3f800000    # 1.0f

    const/16 v19, 0x0

    const/high16 v20, 0x3f800000    # 1.0f

    const/high16 v21, -0x41000000    # -0.5f

    invoke-virtual/range {v17 .. v21}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 298
    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    .line 300
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v18

    const/high16 v19, -0x80000000

    invoke-static/range {v18 .. v19}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    .line 301
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v19

    const/high16 v20, -0x80000000

    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    .line 300
    invoke-virtual/range {v17 .. v19}, Landroid/widget/TextView;->measure(II)V

    .line 302
    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v20

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v21

    invoke-virtual/range {v17 .. v21}, Landroid/widget/TextView;->layout(IIII)V

    .line 303
    const/16 v18, 0x11

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setGravity(I)V

    .line 304
    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v13

    .line 306
    .local v13, "textBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_bg:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_bg:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v18

    if-nez v18, :cond_5

    .line 307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mBitmap_bg:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressLeftMagine:F

    move/from16 v19, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move-object/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 308
    :cond_5
    if-eqz p3, :cond_b

    .line 310
    if-eqz v13, :cond_6

    .line 311
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v19

    div-int/lit8 v19, v19, 0x2

    sub-int v18, v18, v19

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    int-to-float v0, v15

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 350
    :cond_6
    :goto_4
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 238
    .end local v6    # "font":Landroid/graphics/Typeface;
    .end local v7    # "paint":Landroid/graphics/Paint;
    .end local v11    # "step":I
    .end local v13    # "textBitmap":Landroid/graphics/Bitmap;
    .end local v17    # "tv":Landroid/widget/TextView;
    :cond_7
    const/4 v14, 0x3

    goto/16 :goto_1

    .line 263
    .restart local v11    # "step":I
    :sswitch_0
    add-int/lit8 v11, v11, -0x32

    .line 264
    goto/16 :goto_2

    .line 268
    :sswitch_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-int v11, v0

    goto/16 :goto_2

    .line 286
    .restart local v7    # "paint":Landroid/graphics/Paint;
    .restart local v17    # "tv":Landroid/widget/TextView;
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mType:I

    move/from16 v18, v0

    const/high16 v19, 0x10000000

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mType:I

    move/from16 v18, v0

    const/high16 v19, 0x20000000

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mType:I

    move/from16 v18, v0

    const/high16 v19, 0x11200000

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mType:I

    move/from16 v18, v0

    const/high16 v19, 0x11000000

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_a

    .line 287
    :cond_9
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "%"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 289
    :cond_a
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 316
    .restart local v6    # "font":Landroid/graphics/Typeface;
    .restart local v13    # "textBitmap":Landroid/graphics/Bitmap;
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 317
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v18

    sparse-switch v18, :sswitch_data_1

    goto/16 :goto_4

    .line 341
    :sswitch_2
    if-eqz v13, :cond_6

    .line 342
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    sub-int v18, v18, v8

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    int-to-float v0, v15

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 319
    :sswitch_3
    invoke-direct/range {p0 .. p2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawColorGauge(Landroid/graphics/Canvas;I)V

    .line 320
    invoke-direct/range {p0 .. p2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgressText(Landroid/graphics/Canvas;I)V

    .line 321
    if-eqz v13, :cond_6

    .line 322
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressLeftMagine:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    int-to-float v0, v9

    move/from16 v19, v0

    sub-float v18, v18, v19

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    sub-float v18, v18, v19

    int-to-float v0, v15

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 325
    :sswitch_4
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawEffetGauge(Landroid/graphics/Canvas;)V

    .line 326
    invoke-direct/range {p0 .. p2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgressText(Landroid/graphics/Canvas;I)V

    .line 327
    if-eqz v13, :cond_6

    .line 328
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressLeftMagine:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    int-to-float v0, v9

    move/from16 v19, v0

    sub-float v18, v18, v19

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    sub-float v18, v18, v19

    int-to-float v0, v15

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 331
    :sswitch_5
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawPortraitGauge(Landroid/graphics/Canvas;)V

    .line 332
    invoke-direct/range {p0 .. p2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgressText(Landroid/graphics/Canvas;I)V

    .line 333
    if-eqz v13, :cond_6

    .line 334
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressLeftMagine:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    int-to-float v0, v9

    move/from16 v19, v0

    sub-float v18, v18, v19

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    sub-float v18, v18, v19

    int-to-float v0, v15

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mAlphaPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 256
    nop

    :sswitch_data_0
    .sparse-switch
        0x15001500 -> :sswitch_0
        0x15001501 -> :sswitch_0
        0x15001502 -> :sswitch_0
        0x15001503 -> :sswitch_0
        0x15001504 -> :sswitch_0
        0x15001505 -> :sswitch_0
        0x15001607 -> :sswitch_1
        0x15001608 -> :sswitch_1
        0x15001609 -> :sswitch_1
    .end sparse-switch

    .line 317
    :sswitch_data_1
    .sparse-switch
        0x10000000 -> :sswitch_2
        0x11000000 -> :sswitch_2
        0x11200000 -> :sswitch_2
        0x15000000 -> :sswitch_3
        0x16000000 -> :sswitch_4
        0x18000000 -> :sswitch_5
        0x20000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public getCurrentStep()F
    .locals 1

    .prologue
    .line 805
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    return v0
.end method

.method public init()V
    .locals 4

    .prologue
    const/high16 v3, 0x42c80000    # 100.0f

    const/16 v2, 0x64

    const/4 v1, 0x0

    .line 102
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMode:I

    .line 103
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mType:I

    sparse-switch v0, :sswitch_data_0

    .line 158
    :goto_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mType:I

    const v1, 0x15001506

    if-eq v0, v1, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressInterface:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    float-to-int v1, v1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;->onProgressChanged(I)V

    .line 160
    :cond_0
    return-void

    .line 116
    :sswitch_0
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 117
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 118
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0

    .line 123
    :sswitch_1
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 124
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 125
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0

    .line 128
    :sswitch_2
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 129
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 130
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0

    .line 133
    :sswitch_3
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 134
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 135
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0

    .line 138
    :sswitch_4
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 139
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 140
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0

    .line 143
    :sswitch_5
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 144
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 145
    const/high16 v0, 0x40e00000    # 7.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0

    .line 150
    :sswitch_6
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 151
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 152
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    .line 153
    const-string v0, "JW seekbarManager init normalView/cropView"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :sswitch_data_0
    .sparse-switch
        0x10000000 -> :sswitch_6
        0x11000000 -> :sswitch_6
        0x11200000 -> :sswitch_6
        0x15001500 -> :sswitch_0
        0x15001501 -> :sswitch_0
        0x15001502 -> :sswitch_0
        0x15001503 -> :sswitch_0
        0x15001504 -> :sswitch_0
        0x15001505 -> :sswitch_0
        0x15001607 -> :sswitch_1
        0x15001608 -> :sswitch_1
        0x15001609 -> :sswitch_1
        0x16000000 -> :sswitch_2
        0x17001701 -> :sswitch_3
        0x17001702 -> :sswitch_4
        0x17001703 -> :sswitch_5
    .end sparse-switch
.end method

.method public init(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 96
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mType:I

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->init()V

    .line 98
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 173
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMode:I

    if-eqz v1, :cond_0

    .line 175
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 176
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 192
    .end local v0    # "x":F
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 179
    .restart local v0    # "x":F
    :pswitch_1
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mPreX:F

    goto :goto_0

    .line 182
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->calcStep(F)V

    .line 183
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressInterface:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    float-to-int v2, v2

    invoke-interface {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;->onProgressChanged(I)V

    .line 184
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mPreX:F

    goto :goto_0

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEventForSeekbar(Landroid/view/MotionEvent;F)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "scale"    # F

    .prologue
    .line 196
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMode:I

    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 214
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 201
    :pswitch_1
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mPreX:F

    goto :goto_0

    .line 204
    :pswitch_2
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mPreX:F

    .line 205
    invoke-direct {p0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->calcZoomStep(F)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressInterface:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    float-to-int v1, v1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;->onProgressChanged(I)V

    goto :goto_0

    .line 198
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public resetPreProgressValue()V
    .locals 1

    .prologue
    .line 168
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mPreX:F

    .line 169
    return-void
.end method

.method public setStep(F)V
    .locals 3
    .param p1, "step"    # F

    .prologue
    const/16 v2, 0x64

    const/4 v1, 0x0

    .line 769
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMode:I

    .line 770
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mType:I

    sparse-switch v0, :sswitch_data_0

    .line 799
    :goto_0
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    .line 800
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mProgressInterface:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    float-to-int v1, v1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;->onProgressChanged(I)V

    .line 801
    return-void

    .line 773
    :sswitch_0
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 774
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 775
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0

    .line 778
    :sswitch_1
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 779
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 780
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0

    .line 783
    :sswitch_2
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 784
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 785
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0

    .line 788
    :sswitch_3
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 789
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 790
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0

    .line 793
    :sswitch_4
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMax:I

    .line 794
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMin:I

    .line 795
    const/high16 v0, 0x40e00000    # 7.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mCurrentStep:F

    goto :goto_0

    .line 770
    nop

    :sswitch_data_0
    .sparse-switch
        0x15000000 -> :sswitch_0
        0x16000000 -> :sswitch_1
        0x17001701 -> :sswitch_2
        0x17001702 -> :sswitch_3
        0x17001703 -> :sswitch_4
    .end sparse-switch
.end method

.method public startseek()V
    .locals 1

    .prologue
    .line 828
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMode:I

    .line 829
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mPreX:F

    .line 830
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->mMode:I

    .line 165
    return-void
.end method
