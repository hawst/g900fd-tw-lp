.class Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$15;
.super Ljava/lang/Object;
.source "MultigridStyleView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initPinchZoomCallback()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$15;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    .line 1256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invalidate()V
    .locals 0

    .prologue
    .line 1268
    return-void
.end method

.method public onPinchZoom(Landroid/view/MotionEvent;I)V
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;
    .param p2, "diffDst"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1273
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$15;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$21(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    move-result-object v2

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getTouchedIndex(FF)I

    move-result v0

    .line 1274
    .local v0, "index0":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$15;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$21(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    move-result-object v2

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getTouchedIndex(FF)I

    move-result v1

    .line 1276
    .local v1, "index1":I
    if-ne v0, v1, :cond_0

    .line 1278
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$15;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$24(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    move-result-object v2

    invoke-virtual {v2, v0, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->pinchZoom(II)V

    .line 1280
    :cond_0
    return-void
.end method

.method public onSingleTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1261
    const/4 v0, 0x0

    return v0
.end method
