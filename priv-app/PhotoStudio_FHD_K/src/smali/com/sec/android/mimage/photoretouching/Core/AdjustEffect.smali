.class public Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;
.super Ljava/lang/Object;
.source "AdjustEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;
    }
.end annotation


# static fields
.field public static final ADJUST_BLUE:I = 0x2

.field public static final ADJUST_GREEN:I = 0x1

.field public static final ADJUST_RED:I = 0x0

.field public static final ORGINALVIEW_OFF:I = 0x0

.field public static final ORGINALVIEW_ON:I = 0x1

.field public static isFirstTimeDnL:Z

.field private static myCallback:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;


# instance fields
.field private MAX_BRUSH_SIZE:I

.field private mBrushSize:I

.field private mEffectType:I

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mPaint:Landroid/graphics/Paint;

.field private mStep:I

.field private mStep_b:I

.field private mStep_g:I

.field private mStep_r:I

.field private typeRGB:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;

    .line 172
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->isFirstTimeDnL:Z

    .line 176
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 163
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mEffectType:I

    .line 164
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mStep:I

    .line 165
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mStep_r:I

    .line 166
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mStep_g:I

    .line 167
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mStep_b:I

    .line 168
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->typeRGB:I

    .line 177
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mPaint:Landroid/graphics/Paint;

    .line 179
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mBrushSize:I

    .line 180
    const/16 v0, 0x8c

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->MAX_BRUSH_SIZE:I

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 3
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 163
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mEffectType:I

    .line 164
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mStep:I

    .line 165
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mStep_r:I

    .line 166
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mStep_g:I

    .line 167
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mStep_b:I

    .line 168
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->typeRGB:I

    .line 177
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mPaint:Landroid/graphics/Paint;

    .line 179
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mBrushSize:I

    .line 180
    const/16 v0, 0x8c

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->MAX_BRUSH_SIZE:I

    .line 25
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 27
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mPaint:Landroid/graphics/Paint;

    .line 28
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 29
    return-void
.end method

.method private declared-synchronized applyAutoEffect(Z)[I
    .locals 14
    .param p1, "ispreview"    # Z

    .prologue
    const/4 v10, 0x0

    const/16 v13, 0x1770

    const/16 v12, 0x20

    .line 52
    monitor-enter p0

    if-eqz p1, :cond_2

    .line 54
    :try_start_0
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v11, :cond_0

    .line 57
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v5

    .line 58
    .local v5, "previewInput":[I
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v6

    .line 60
    .local v6, "previewOutput":[I
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    .line 61
    .local v7, "previewWidth":I
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    .line 62
    .local v4, "previewHeight":I
    if-le v7, v12, :cond_1

    if-le v4, v12, :cond_1

    if-ge v7, v13, :cond_1

    if-ge v4, v13, :cond_1

    .line 63
    const/4 v11, 0x0

    invoke-static {v5, v6, v7, v4, v11}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->autoBLCInit([I[IIII)I

    .line 69
    const/4 v11, 0x0

    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->DeInitBLC(I)I

    .line 70
    sget-object v11, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;

    invoke-interface {v11}, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;->afterApplyPreview()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .end local v4    # "previewHeight":I
    .end local v5    # "previewInput":[I
    .end local v6    # "previewOutput":[I
    .end local v7    # "previewWidth":I
    :cond_0
    move-object v1, v10

    .line 104
    :goto_0
    monitor-exit p0

    return-object v1

    .restart local v4    # "previewHeight":I
    .restart local v5    # "previewInput":[I
    .restart local v6    # "previewOutput":[I
    .restart local v7    # "previewWidth":I
    :cond_1
    move-object v1, v10

    .line 65
    goto :goto_0

    .line 77
    .end local v4    # "previewHeight":I
    .end local v5    # "previewInput":[I
    .end local v6    # "previewOutput":[I
    .end local v7    # "previewWidth":I
    :cond_2
    :try_start_1
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v11, :cond_4

    .line 79
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v2

    .line 80
    .local v2, "mask":[B
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v1

    .line 81
    .local v1, "input":[I
    const/4 v3, 0x0

    .line 82
    .local v3, "output":[I
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v9

    .line 83
    .local v9, "w":I
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 84
    .local v0, "h":I
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v8

    .line 87
    .local v8, "r":Landroid/graphics/Rect;
    if-le v9, v12, :cond_3

    if-le v0, v12, :cond_3

    if-ge v9, v13, :cond_3

    if-ge v0, v13, :cond_3

    .line 88
    const/4 v10, 0x1

    invoke-static {v1, v1, v9, v0, v10}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->autoBLCInit([I[IIII)I

    .line 93
    const/4 v10, 0x1

    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->DeInitBLC(I)I

    .line 97
    const/4 v3, 0x0

    .line 99
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 101
    const-string v10, "applyOriginal end"

    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 52
    .end local v0    # "h":I
    .end local v1    # "input":[I
    .end local v2    # "mask":[B
    .end local v3    # "output":[I
    .end local v8    # "r":Landroid/graphics/Rect;
    .end local v9    # "w":I
    :catchall_0
    move-exception v10

    monitor-exit p0

    throw v10

    .restart local v0    # "h":I
    .restart local v1    # "input":[I
    .restart local v2    # "mask":[B
    .restart local v3    # "output":[I
    .restart local v8    # "r":Landroid/graphics/Rect;
    .restart local v9    # "w":I
    :cond_3
    move-object v1, v10

    .line 90
    goto :goto_0

    .end local v0    # "h":I
    .end local v1    # "input":[I
    .end local v2    # "mask":[B
    .end local v3    # "output":[I
    .end local v8    # "r":Landroid/graphics/Rect;
    .end local v9    # "w":I
    :cond_4
    move-object v1, v10

    .line 104
    goto :goto_0
.end method


# virtual methods
.method public applyOriginal()[I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->applyAutoEffect(Z)[I

    move-result-object v0

    return-object v0
.end method

.method public applyPreview()V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->applyAutoEffect(Z)[I

    .line 116
    return-void
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;)V
    .locals 1
    .param p1, "AdjustEffect"    # Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    .prologue
    .line 32
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 35
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 39
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mPaint:Landroid/graphics/Paint;

    .line 40
    return-void
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 157
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 152
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 44
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 45
    .local v0, "previewInput":[I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 47
    .local v1, "previewOutput":[I
    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 48
    return-void
.end method

.method public setOnCallback(Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;

    .prologue
    .line 147
    sput-object p1, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;

    .line 148
    return-void
.end method

.method public showOrginalView(I)V
    .locals 6
    .param p1, "state"    # I

    .prologue
    const/4 v5, 0x0

    .line 120
    if-nez p1, :cond_1

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->applyPreview()V

    .line 123
    sget-object v4, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;

    if-eqz v4, :cond_0

    .line 125
    sget-object v4, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;->invalidate()V

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    const/4 v4, 0x1

    if-ne p1, v4, :cond_0

    .line 130
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 131
    .local v1, "previewInput":[I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v2

    .line 133
    .local v2, "previewOutput":[I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 134
    .local v3, "previewWidth":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    .line 136
    .local v0, "previewHeight":I
    mul-int v4, v3, v0

    invoke-static {v1, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 138
    sget-object v4, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;

    if-eqz v4, :cond_0

    .line 140
    sget-object v4, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;->invalidate()V

    goto :goto_0
.end method
