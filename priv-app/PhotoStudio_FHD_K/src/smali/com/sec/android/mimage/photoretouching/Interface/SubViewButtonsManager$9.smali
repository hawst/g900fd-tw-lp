.class Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$9;
.super Ljava/lang/Object;
.source "SubViewButtonsManager.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->subMenuAnimate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    .line 508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v1, 0x0

    .line 524
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 525
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 526
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setAnimation(Z)V

    .line 527
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;Z)V

    .line 528
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 520
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v1, 0x1

    .line 512
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;Z)V

    .line 513
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setAnimation(Z)V

    .line 514
    return-void
.end method
