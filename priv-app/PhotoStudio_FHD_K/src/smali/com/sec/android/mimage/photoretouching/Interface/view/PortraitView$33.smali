.class Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$33;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "PortraitView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initGesture()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$33;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    .line 2407
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2445
    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 2411
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$33;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->isReverseAnimRunning:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$40(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2413
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$33;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$41(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Z)V

    .line 2421
    :goto_0
    return v2

    .line 2418
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$33;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    const/16 v1, 0x32

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$42(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;I)V

    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 2428
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2432
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2441
    :goto_0
    return-void

    .line 2434
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$33;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->isSeekbarProgressChanged()Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$43(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2435
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$33;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$44(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Z)V

    .line 2438
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$33;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->invalidateViews()V

    goto :goto_0

    .line 2432
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 2450
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2456
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2460
    const/4 v0, 0x0

    return v0
.end method
