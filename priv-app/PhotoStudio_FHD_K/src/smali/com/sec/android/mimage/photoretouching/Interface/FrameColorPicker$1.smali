.class Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;
.super Ljava/lang/Object;
.source "FrameColorPicker.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->initListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "button"    # Landroid/view/View;

    .prologue
    .line 106
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 154
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->setSelected(I)V
    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const/4 v1, 0x0

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->setPickerPos(Landroid/view/MotionEvent;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;Landroid/view/MotionEvent;Z)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;)Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mCurrentColor:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;->setBackgroundColor(I)V

    .line 157
    return-void

    .line 109
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto :goto_0

    .line 112
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const v1, -0x200d3

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto :goto_0

    .line 115
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const/16 v1, -0x7ca3

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto :goto_0

    .line 118
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const v1, -0xc4a5

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto :goto_0

    .line 121
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const v1, -0xb637

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto :goto_0

    .line 124
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const v1, -0x357a01

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto :goto_0

    .line 127
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const v1, -0x85c822

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto :goto_0

    .line 130
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const v1, -0xfe6bd2

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto :goto_0

    .line 133
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const v1, -0xc75701

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto :goto_0

    .line 136
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const v1, -0xcc9803

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto :goto_0

    .line 139
    :pswitch_a
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const v1, -0x595a5b

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto :goto_0

    .line 142
    :pswitch_b
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const v1, -0xc9c9ca

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto/16 :goto_0

    .line 145
    :pswitch_c
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const/high16 v1, -0x1000000

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto/16 :goto_0

    .line 148
    :pswitch_d
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const/16 v1, -0x199e

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto/16 :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x7f09007a
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
