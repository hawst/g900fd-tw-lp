.class public Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;
.super Ljava/lang/Object;
.source "MirrorEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;
    }
.end annotation


# instance fields
.field private mApplyMirror:Z

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mInitialCanvasRoi:Landroid/graphics/Rect;

.field private mInitialPreHeight:I

.field private mInitialPreImageBuf:[I

.field private mInitialPreWidth:I

.field private mMirrorAxis:I

.field private mMirrorCallback:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;

.field private mMirrorHandler:Landroid/graphics/Bitmap;

.field private mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

.field private mMirrorHandlerLeftPressed:Landroid/graphics/Bitmap;

.field private mMirrorHandlerPressed:Z

.field private mMirrorHandlerRightNormal:Landroid/graphics/Bitmap;

.field private mMirrorHandlerRightPressed:Landroid/graphics/Bitmap;

.field private mMirrorHandlerRoi:Landroid/graphics/RectF;

.field private mMirrorMatrix:Landroid/graphics/Matrix;

.field private mMirrorPreviewHeight:I

.field private mMirrorPreviewWidth:I

.field private mMirrorSelection:Landroid/graphics/drawable/NinePatchDrawable;

.field private mMirrorSelectionRoi:Landroid/graphics/Rect;

.field private mMirrorSide:I

.field private mPrevX:F

.field private mPreviousMirrorHandler:Landroid/graphics/Bitmap;

.field private mResultPreviewWdt:I

.field private mScale:F

.field private mTempMirrorBuff:[I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 489
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    .line 490
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorCallback:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;

    .line 492
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    .line 493
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    .line 495
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mPrevX:F

    .line 497
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreWidth:I

    .line 498
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreHeight:I

    .line 500
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreImageBuf:[I

    .line 501
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mTempMirrorBuff:[I

    .line 503
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    .line 504
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    .line 506
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerPressed:Z

    .line 507
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mApplyMirror:Z

    .line 509
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    .line 510
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mPreviousMirrorHandler:Landroid/graphics/Bitmap;

    .line 511
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftPressed:Landroid/graphics/Bitmap;

    .line 512
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    .line 513
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightPressed:Landroid/graphics/Bitmap;

    .line 514
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightNormal:Landroid/graphics/Bitmap;

    .line 515
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelection:Landroid/graphics/drawable/NinePatchDrawable;

    .line 516
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    .line 517
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mScale:F

    .line 518
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorAxis:I

    .line 519
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSide:I

    .line 521
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mResultPreviewWdt:I

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 489
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    .line 490
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorCallback:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;

    .line 492
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    .line 493
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    .line 495
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mPrevX:F

    .line 497
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreWidth:I

    .line 498
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreHeight:I

    .line 500
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreImageBuf:[I

    .line 501
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mTempMirrorBuff:[I

    .line 503
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    .line 504
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    .line 506
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerPressed:Z

    .line 507
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mApplyMirror:Z

    .line 509
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    .line 510
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mPreviousMirrorHandler:Landroid/graphics/Bitmap;

    .line 511
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftPressed:Landroid/graphics/Bitmap;

    .line 512
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    .line 513
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightPressed:Landroid/graphics/Bitmap;

    .line 514
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightNormal:Landroid/graphics/Bitmap;

    .line 515
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelection:Landroid/graphics/drawable/NinePatchDrawable;

    .line 516
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    .line 517
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mScale:F

    .line 518
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorAxis:I

    .line 519
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSide:I

    .line 521
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mResultPreviewWdt:I

    .line 45
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 46
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    .line 48
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02032c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelection:Landroid/graphics/drawable/NinePatchDrawable;

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020328

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020329

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftPressed:Landroid/graphics/Bitmap;

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02032a

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightNormal:Landroid/graphics/Bitmap;

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02032b

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightPressed:Landroid/graphics/Bitmap;

    .line 54
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightNormal:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    .line 55
    return-void
.end method

.method private declared-synchronized applyMirror(Z)[I
    .locals 38
    .param p1, "isOrg"    # Z

    .prologue
    .line 350
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 352
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v5

    mul-int v36, v4, v5

    .line 353
    .local v36, "size":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    div-float v35, v4, v5

    .line 354
    .local v35, "scale":F
    new-instance v28, Landroid/graphics/RectF;

    invoke-direct/range {v28 .. v28}, Landroid/graphics/RectF;-><init>()V

    .line 355
    .local v28, "dst":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    move-object/from16 v0, v28

    invoke-virtual {v4, v0, v5}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 356
    invoke-virtual/range {v28 .. v28}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    mul-float v4, v4, v35

    float-to-int v10, v4

    .line 357
    .local v10, "mirrorAxis":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSide:I

    .line 358
    .local v11, "mirrorSide":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v4

    .line 361
    const v5, 0xe002

    .line 358
    invoke-static {v4, v10, v11, v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->getMirrorOutwidth(IIII)I

    move-result v8

    .line 363
    .local v8, "MirrorOriginalWidth":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v4

    mul-int/2addr v4, v8

    int-to-float v4, v4

    move/from16 v0, v36

    int-to-float v5, v0

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v34, v0

    .line 365
    .local v34, "retScale":F
    int-to-float v4, v8

    div-float v4, v4, v34

    float-to-int v0, v4

    move/from16 v16, v0

    .line 366
    .local v16, "scaledWidth":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v4, v4, v34

    float-to-int v0, v4

    move/from16 v17, v0

    .line 368
    .local v17, "scaledHeight":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v4

    mul-int/2addr v4, v8

    new-array v7, v4, [I

    .line 369
    .local v7, "OriginalBuffer":[I
    mul-int v4, v16, v17

    new-array v13, v4, [I

    .line 371
    .local v13, "retResultBuffer":[I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v4

    .line 372
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    .line 373
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v6

    .line 376
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v9

    .line 379
    const v12, 0xe002

    .line 371
    invoke-static/range {v4 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runMirror([III[IIIIII)I

    move-result v33

    .line 381
    .local v33, "ret":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v15

    move-object v12, v7

    move v14, v8

    invoke-static/range {v12 .. v17}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->resizeBuff([I[IIIII)V

    .line 385
    const/4 v7, 0x0

    .line 387
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v4, v13, v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([III)V

    .line 390
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mResultPreviewWdt:I

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOrgToPreviewRatio(I)V

    .line 391
    const/4 v13, 0x0

    .line 392
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 452
    .end local v7    # "OriginalBuffer":[I
    .end local v8    # "MirrorOriginalWidth":I
    .end local v10    # "mirrorAxis":I
    .end local v11    # "mirrorSide":I
    .end local v13    # "retResultBuffer":[I
    .end local v16    # "scaledWidth":I
    .end local v17    # "scaledHeight":I
    .end local v28    # "dst":Landroid/graphics/RectF;
    .end local v34    # "retScale":F
    .end local v36    # "size":I
    :goto_0
    monitor-exit p0

    return-object v4

    .line 396
    .end local v33    # "ret":I
    .end local v35    # "scale":F
    :cond_0
    const/4 v4, 0x0

    :try_start_1
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mTempMirrorBuff:[I

    .line 408
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreWidth:I

    .line 409
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorAxis:I

    .line 410
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSide:I

    .line 411
    const v9, 0xe002

    .line 408
    invoke-static {v4, v5, v6, v9}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->getMirrorOutwidth(IIII)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    .line 413
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreHeight:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    .line 415
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    mul-int/2addr v4, v5

    new-array v4, v4, [I

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mTempMirrorBuff:[I

    .line 416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreImageBuf:[I

    move-object/from16 v18, v0

    .line 417
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreWidth:I

    move/from16 v19, v0

    .line 418
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreHeight:I

    move/from16 v20, v0

    .line 419
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mTempMirrorBuff:[I

    move-object/from16 v21, v0

    .line 420
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    move/from16 v22, v0

    .line 421
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    move/from16 v23, v0

    .line 422
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorAxis:I

    move/from16 v24, v0

    .line 423
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSide:I

    move/from16 v25, v0

    .line 424
    const v26, 0xe002

    .line 416
    invoke-static/range {v18 .. v26}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runMirror([III[IIIIII)I

    move-result v33

    .line 426
    .restart local v33    # "ret":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v5

    int-to-float v5, v5

    div-float v37, v4, v5

    .line 427
    .local v37, "viewRatio":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    int-to-float v5, v5

    div-float v32, v4, v5

    .line 428
    .local v32, "previewRatio":F
    const/high16 v35, 0x3f800000    # 1.0f

    .line 430
    .restart local v35    # "scale":F
    cmpl-float v4, v37, v32

    if-lez v4, :cond_1

    .line 432
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    int-to-float v5, v5

    div-float v35, v4, v5

    .line 438
    :goto_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    int-to-float v4, v4

    mul-float v4, v4, v35

    float-to-int v0, v4

    move/from16 v31, v0

    .line 439
    .local v31, "newWidth":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    int-to-float v4, v4

    mul-float v4, v4, v35

    float-to-int v0, v4

    move/from16 v30, v0

    .line 440
    .local v30, "newHeight":I
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v31

    move/from16 v1, v30

    invoke-static {v0, v1, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v29

    .line 441
    .local v29, "mirror":Landroid/graphics/Bitmap;
    new-instance v18, Landroid/graphics/Canvas;

    move-object/from16 v0, v18

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 442
    .local v18, "canvas":Landroid/graphics/Canvas;
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Canvas;->save()I

    .line 443
    move-object/from16 v0, v18

    move/from16 v1, v35

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->scale(FF)V

    .line 444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mTempMirrorBuff:[I

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    move/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    move/from16 v25, v0

    const/16 v26, 0x1

    const/16 v27, 0x0

    invoke-virtual/range {v18 .. v27}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 445
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Canvas;->restore()V

    .line 446
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mTempMirrorBuff:[I

    .line 447
    mul-int v4, v31, v30

    new-array v4, v4, [I

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mTempMirrorBuff:[I

    .line 448
    move/from16 v0, v30

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    .line 449
    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    .line 450
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mTempMirrorBuff:[I

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v19, v29

    move/from16 v22, v31

    move/from16 v25, v31

    move/from16 v26, v30

    invoke-virtual/range {v19 .. v26}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 451
    invoke-static/range {v29 .. v29}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 452
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 436
    .end local v18    # "canvas":Landroid/graphics/Canvas;
    .end local v29    # "mirror":Landroid/graphics/Bitmap;
    .end local v30    # "newHeight":I
    .end local v31    # "newWidth":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-float v5, v5

    div-float v35, v4, v5

    goto/16 :goto_1

    .line 350
    .end local v32    # "previewRatio":F
    .end local v33    # "ret":I
    .end local v35    # "scale":F
    .end local v37    # "viewRatio":F
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method private getMirrorMatrix()Landroid/graphics/Matrix;
    .locals 3

    .prologue
    .line 179
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 180
    .local v0, "matrix":Landroid/graphics/Matrix;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 181
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 183
    return-object v0
.end method

.method public static resizeBuff([I[IIIII)V
    .locals 9
    .param p0, "in"    # [I
    .param p1, "out"    # [I
    .param p2, "in_width"    # I
    .param p3, "in_height"    # I
    .param p4, "out_width"    # I
    .param p5, "out_height"    # I

    .prologue
    .line 526
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 527
    .local v0, "original":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    move v3, p2

    move v6, p2

    move v7, p3

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 528
    const/4 v2, 0x1

    invoke-static {v0, p4, p5, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 529
    .local v1, "newBmp":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 530
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p1

    move v4, p4

    move v7, p4

    move v8, p5

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 531
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 532
    return-void
.end method


# virtual methods
.method public applyOriginal()[I
    .locals 1

    .prologue
    .line 483
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->applyMirror(Z)[I

    move-result-object v0

    return-object v0
.end method

.method public applyToPreviewMirror()V
    .locals 1

    .prologue
    .line 479
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->applyMirror(Z)[I

    .line 480
    return-void
.end method

.method public changeDoneCancelStatus(I)V
    .locals 1
    .param p1, "input"    # I

    .prologue
    .line 486
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorCallback:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;->ableDone()V

    .line 487
    return-void
.end method

.method public changeLayoutSize(Landroid/graphics/Rect;)V
    .locals 11
    .param p1, "newCanvasRoi"    # Landroid/graphics/Rect;

    .prologue
    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 457
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 458
    .local v3, "scaleHorizontal":F
    const/16 v5, 0x9

    new-array v4, v5, [F

    .line 459
    .local v4, "values":[F
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5, v4}, Landroid/graphics/Matrix;->getValues([F)V

    .line 460
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5}, Landroid/graphics/Matrix;->reset()V

    .line 461
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v5, v8, v8, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 462
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    sub-float/2addr v5, v6

    mul-float v1, v5, v10

    .line 463
    .local v1, "dx":F
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    sub-float/2addr v5, v6

    mul-float v2, v5, v10

    .line 464
    .local v2, "dy":F
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    invoke-virtual {v5, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 465
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    const/4 v6, 0x2

    aget v6, v4, v6

    mul-float/2addr v6, v3

    invoke-virtual {v5, v6, v8}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 466
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v5, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 468
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 469
    .local v0, "center":Landroid/graphics/RectF;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    invoke-virtual {v5, v0, v6}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 471
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    if-eq v5, v6, :cond_0

    .line 472
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftPressed:Landroid/graphics/Bitmap;

    if-ne v5, v6, :cond_1

    .line 473
    :cond_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v6

    float-to-int v6, v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v7

    invoke-virtual {v5, v9, v9, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 476
    :goto_0
    return-void

    .line 475
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v6

    float-to-int v6, v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v8

    invoke-virtual {v5, v6, v9, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;)V
    .locals 1
    .param p1, "mirrorEffect"    # Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    .prologue
    .line 62
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 63
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSide:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSide:I

    .line 64
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    .line 65
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    .line 66
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    .line 67
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mResultPreviewWdt:I

    .line 68
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreImageBuf:[I

    if-eqz v0, :cond_0

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreImageBuf:[I

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    .line 84
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftPressed:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftPressed:Landroid/graphics/Bitmap;

    .line 85
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightNormal:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightNormal:Landroid/graphics/Bitmap;

    .line 86
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightPressed:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightPressed:Landroid/graphics/Bitmap;

    .line 87
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 88
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 24
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "viewBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .param p4, "linePaint"    # Landroid/graphics/Paint;

    .prologue
    .line 115
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mTempMirrorBuff:[I

    if-eqz v3, :cond_2

    .line 117
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    .line 118
    .local v13, "clearPaint":Landroid/graphics/Paint;
    new-instance v23, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, v23

    invoke-direct {v0, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    .line 119
    .local v23, "xmode":Landroid/graphics/Xfermode;
    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 120
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 121
    const/high16 v3, -0x1000000

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 122
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 123
    new-instance v19, Landroid/graphics/Matrix;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Matrix;-><init>()V

    .line 124
    .local v19, "matrix":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v22, v3, v4

    .line 125
    .local v22, "viewRatio":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    int-to-float v4, v4

    div-float v17, v3, v4

    .line 126
    .local v17, "imgRatio":F
    const/high16 v21, 0x3f800000    # 1.0f

    .line 127
    .local v21, "scale":F
    cmpl-float v3, v22, v17

    if-lez v3, :cond_1

    .line 129
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    int-to-float v4, v4

    div-float v21, v3, v4

    .line 135
    :goto_0
    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 136
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    int-to-float v4, v4

    mul-float v4, v4, v21

    sub-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    .line 137
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    int-to-float v5, v5

    mul-float v5, v5, v21

    sub-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    .line 136
    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 138
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 139
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 140
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mTempMirrorBuff:[I

    .line 141
    const/4 v5, 0x0

    .line 142
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    .line 143
    const/4 v7, 0x0

    .line 144
    const/4 v8, 0x0

    .line 145
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    .line 146
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    .line 147
    const/4 v11, 0x1

    .line 148
    const/4 v12, 0x0

    move-object/from16 v3, p1

    .line 140
    invoke-virtual/range {v3 .. v12}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 149
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 176
    .end local v13    # "clearPaint":Landroid/graphics/Paint;
    .end local v17    # "imgRatio":F
    .end local v19    # "matrix":Landroid/graphics/Matrix;
    .end local v21    # "scale":F
    .end local v22    # "viewRatio":F
    .end local v23    # "xmode":Landroid/graphics/Xfermode;
    :cond_0
    :goto_1
    return-void

    .line 133
    .restart local v13    # "clearPaint":Landroid/graphics/Paint;
    .restart local v17    # "imgRatio":F
    .restart local v19    # "matrix":Landroid/graphics/Matrix;
    .restart local v21    # "scale":F
    .restart local v22    # "viewRatio":F
    .restart local v23    # "xmode":Landroid/graphics/Xfermode;
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    int-to-float v4, v4

    div-float v21, v3, v4

    goto :goto_0

    .line 153
    .end local v13    # "clearPaint":Landroid/graphics/Paint;
    .end local v17    # "imgRatio":F
    .end local v19    # "matrix":Landroid/graphics/Matrix;
    .end local v21    # "scale":F
    .end local v22    # "viewRatio":F
    .end local v23    # "xmode":Landroid/graphics/Xfermode;
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 155
    new-instance v20, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    move-object/from16 v0, v20

    invoke-direct {v0, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 156
    .local v20, "s":Landroid/graphics/RectF;
    new-instance v14, Landroid/graphics/RectF;

    invoke-direct {v14}, Landroid/graphics/RectF;-><init>()V

    .line 157
    .local v14, "d":Landroid/graphics/RectF;
    new-instance v18, Landroid/graphics/Matrix;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Matrix;-><init>()V

    .line 158
    .local v18, "m":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 159
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 160
    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v14, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 161
    new-instance v15, Landroid/graphics/Rect;

    invoke-direct {v15}, Landroid/graphics/Rect;-><init>()V

    .line 162
    .local v15, "dd":Landroid/graphics/Rect;
    iget v3, v14, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    iget v4, v14, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    iget v5, v14, Landroid/graphics/RectF;->right:F

    float-to-int v5, v5

    iget v6, v14, Landroid/graphics/RectF;->bottom:F

    float-to-int v6, v6

    invoke-virtual {v15, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 163
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelection:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v3, v15}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 164
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelection:Landroid/graphics/drawable/NinePatchDrawable;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 166
    .end local v14    # "d":Landroid/graphics/RectF;
    .end local v15    # "dd":Landroid/graphics/Rect;
    .end local v18    # "m":Landroid/graphics/Matrix;
    .end local v20    # "s":Landroid/graphics/RectF;
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->getMirrorMatrix()Landroid/graphics/Matrix;

    move-result-object v18

    .line 167
    .restart local v18    # "m":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 168
    new-instance v16, Landroid/graphics/RectF;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/RectF;-><init>()V

    .line 169
    .local v16, "dst":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 172
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 173
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    sub-float/2addr v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1
.end method

.method public getApplyMirrorHeight()I
    .locals 1

    .prologue
    .line 346
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewHeight:I

    return v0
.end method

.method public getApplyMirrorPreviewBuff()[I
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mTempMirrorBuff:[I

    return-object v0
.end method

.method public getApplyMirrorWidth()I
    .locals 1

    .prologue
    .line 342
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorPreviewWidth:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 77
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 72
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;)V
    .locals 7
    .param p1, "info"    # Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;
    .param p2, "callback"    # Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v5, 0x0

    .line 92
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->getRotatedBuff()[I

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreImageBuf:[I

    .line 93
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->getRotatedWidth()I

    move-result v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreWidth:I

    .line 94
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->getRotatedHeight()I

    move-result v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreHeight:I

    .line 95
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorCallback:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;

    .line 96
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->getRotatedRoi()Landroid/graphics/Rect;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    .line 97
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    .line 98
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    .line 99
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mTempMirrorBuff:[I

    .line 100
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 101
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 102
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    sub-float/2addr v2, v3

    mul-float v0, v2, v6

    .line 103
    .local v0, "dx":F
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v2, v3

    mul-float v1, v2, v6

    .line 105
    .local v1, "dy":F
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 107
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    float-to-int v3, v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 109
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorCallback:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;->invalidate()V

    .line 110
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorCallback:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;->unableDone()V

    .line 111
    return-void
.end method

.method public touch(Landroid/view/MotionEvent;)Z
    .locals 18
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 187
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 188
    .local v8, "m":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->left:I

    int-to-float v13, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->top:I

    int-to-float v14, v14

    invoke-virtual {v8, v13, v14}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 189
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v13

    invoke-virtual {v8, v13}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 190
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 191
    .local v7, "im":Landroid/graphics/Matrix;
    invoke-virtual {v8, v7}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 192
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 193
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    .line 196
    .local v12, "x":F
    const/16 v13, 0x9

    new-array v11, v13, [F

    .line 197
    .local v11, "value":[F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v13

    invoke-virtual {v13, v11}, Landroid/graphics/Matrix;->getValues([F)V

    .line 199
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 200
    .local v6, "handlerRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    invoke-virtual {v13}, Landroid/graphics/RectF;->centerX()F

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    int-to-float v14, v14

    const/4 v15, 0x0

    aget v15, v11, v15

    div-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    sub-float/2addr v13, v14

    iput v13, v6, Landroid/graphics/RectF;->left:F

    .line 201
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    invoke-virtual {v13}, Landroid/graphics/RectF;->centerX()F

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    int-to-float v14, v14

    const/4 v15, 0x0

    aget v15, v11, v15

    div-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    add-float/2addr v13, v14

    iput v13, v6, Landroid/graphics/RectF;->right:F

    .line 202
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    iget v13, v13, Landroid/graphics/RectF;->top:F

    iput v13, v6, Landroid/graphics/RectF;->top:F

    .line 203
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    iget v13, v13, Landroid/graphics/RectF;->bottom:F

    iput v13, v6, Landroid/graphics/RectF;->bottom:F

    .line 206
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v13

    packed-switch v13, :pswitch_data_0

    .line 334
    :cond_0
    :goto_0
    const/4 v13, 0x1

    return v13

    .line 208
    :pswitch_0
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 209
    .local v4, "dst":Landroid/graphics/RectF;
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    .line 210
    .local v5, "dst_t":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v13, v4, v6}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 211
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    invoke-virtual {v13, v5, v14}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 212
    iget v13, v4, Landroid/graphics/RectF;->left:F

    cmpg-float v13, v13, v12

    if-gez v13, :cond_3

    iget v13, v4, Landroid/graphics/RectF;->right:F

    cmpl-float v13, v13, v12

    if-lez v13, :cond_3

    .line 214
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerPressed:Z

    .line 215
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mPreviousMirrorHandler:Landroid/graphics/Bitmap;

    .line 216
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mPreviousMirrorHandler:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    if-ne v13, v14, :cond_2

    .line 217
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftPressed:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    .line 220
    :cond_1
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mPrevX:F

    goto :goto_0

    .line 218
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mPreviousMirrorHandler:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightNormal:Landroid/graphics/Bitmap;

    if-ne v13, v14, :cond_1

    .line 219
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightPressed:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 224
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mTempMirrorBuff:[I

    if-nez v13, :cond_0

    .line 226
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    float-to-int v14, v12

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    invoke-virtual {v15}, Landroid/graphics/Rect;->centerY()I

    move-result v15

    invoke-virtual {v13, v14, v15}, Landroid/graphics/Rect;->contains(II)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 228
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mApplyMirror:Z

    goto/16 :goto_0

    .line 232
    :cond_4
    iget v13, v4, Landroid/graphics/RectF;->left:F

    cmpg-float v13, v12, v13

    if-gez v13, :cond_5

    .line 234
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    .line 235
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerX()F

    move-result v16

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v17

    invoke-virtual/range {v13 .. v17}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 237
    :cond_5
    iget v13, v4, Landroid/graphics/RectF;->right:F

    cmpl-float v13, v12, v13

    if-lez v13, :cond_0

    .line 239
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightNormal:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    .line 240
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerX()F

    move-result v14

    float-to-int v14, v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v17

    invoke-virtual/range {v13 .. v17}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 247
    .end local v4    # "dst":Landroid/graphics/RectF;
    .end local v5    # "dst_t":Landroid/graphics/RectF;
    :pswitch_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerPressed:Z

    if-eqz v13, :cond_0

    .line 249
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mPrevX:F

    sub-float v3, v13, v14

    .line 251
    .local v3, "diff":F
    new-instance v8, Landroid/graphics/Matrix;

    .end local v8    # "m":Landroid/graphics/Matrix;
    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 252
    .restart local v8    # "m":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v8, v13}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 253
    const/4 v13, 0x0

    invoke-virtual {v8, v3, v13}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 254
    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10, v6}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 255
    .local v10, "src":Landroid/graphics/RectF;
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 257
    .restart local v4    # "dst":Landroid/graphics/RectF;
    invoke-virtual {v8, v4, v10}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 258
    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v13

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-lez v13, :cond_6

    .line 259
    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v14

    int-to-float v14, v14

    cmpg-float v13, v13, v14

    if-gez v13, :cond_6

    .line 261
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    const/4 v14, 0x0

    invoke-virtual {v13, v3, v14}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 263
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mPrevX:F

    .line 265
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 266
    .local v1, "center":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    invoke-virtual {v13, v1, v14}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 268
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftPressed:Landroid/graphics/Bitmap;

    if-ne v13, v14, :cond_7

    .line 270
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v16

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v17

    invoke-virtual/range {v13 .. v17}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 274
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v14

    float-to-int v14, v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v17

    invoke-virtual/range {v13 .. v17}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 280
    .end local v1    # "center":Landroid/graphics/RectF;
    .end local v3    # "diff":F
    .end local v4    # "dst":Landroid/graphics/RectF;
    .end local v10    # "src":Landroid/graphics/RectF;
    :pswitch_2
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerPressed:Z

    if-eqz v13, :cond_a

    .line 282
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mPreviousMirrorHandler:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    if-ne v13, v14, :cond_9

    .line 283
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    .line 331
    :cond_8
    :goto_2
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerPressed:Z

    goto/16 :goto_0

    .line 284
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mPreviousMirrorHandler:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightNormal:Landroid/graphics/Bitmap;

    if-ne v13, v14, :cond_8

    .line 285
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRightNormal:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 289
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mApplyMirror:Z

    if-eqz v13, :cond_8

    .line 291
    new-instance v9, Landroid/graphics/RectF;

    invoke-direct {v9}, Landroid/graphics/RectF;-><init>()V

    .line 292
    .local v9, "mirrorRoi":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreWidth:I

    int-to-float v13, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v14

    int-to-float v14, v14

    div-float/2addr v13, v14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mScale:F

    .line 293
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 294
    .local v2, "d":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerRoi:Landroid/graphics/RectF;

    invoke-virtual {v13, v2, v14}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 295
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mScale:F

    mul-float/2addr v13, v14

    float-to-int v13, v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorAxis:I

    .line 297
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandlerLeftNormal:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorHandler:Landroid/graphics/Bitmap;

    if-ne v13, v14, :cond_b

    .line 299
    const v13, 0xe003

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSide:I

    .line 300
    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorAxis:I

    int-to-float v15, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreHeight:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v9, v13, v14, v15, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 307
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSelectionRoi:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->setEmpty()V

    .line 325
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->applyToPreviewMirror()V

    .line 326
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorCallback:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;

    invoke-interface {v13}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;->changeMirrorButtonTOUndoButton()V

    .line 327
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorCallback:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;

    invoke-interface {v13}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;->ableDone()V

    .line 328
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mApplyMirror:Z

    goto/16 :goto_2

    .line 304
    :cond_b
    const v13, 0xe004

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorSide:I

    .line 305
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mMirrorAxis:I

    int-to-float v13, v13

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreWidth:I

    int-to-float v15, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->mInitialPreHeight:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v9, v13, v14, v15, v0}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_3

    .line 206
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
