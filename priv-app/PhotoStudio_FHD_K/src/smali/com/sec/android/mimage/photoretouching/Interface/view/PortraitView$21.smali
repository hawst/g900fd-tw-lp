.class Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;
.super Ljava/lang/Object;
.source "PortraitView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initUndoRedoAllDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    .line 1658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 1662
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Z)V

    .line 1663
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewWidth()I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$27(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewHeight()I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->redoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 1664
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->refreshImageAndBottomButtons()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$29(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 1665
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1667
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1668
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 1669
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableSave()V

    .line 1677
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1679
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1680
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 1687
    :cond_1
    :goto_1
    return-void

    .line 1674
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1675
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    goto :goto_0

    .line 1684
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1685
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_1
.end method
