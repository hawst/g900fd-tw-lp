.class public interface abstract Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
.super Ljava/lang/Object;
.source "MyButtonInterface.java"


# virtual methods
.method public abstract copyButtonsNPosition(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V
.end method

.method public abstract getCurrentPosition()Landroid/graphics/Rect;
.end method

.method public abstract getHeight()I
.end method

.method public abstract getLeftButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
.end method

.method public abstract getRightButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
.end method

.method public abstract getWidth()I
.end method

.method public abstract moveToLeft()V
.end method

.method public abstract moveToRight()V
.end method

.method public abstract reloadLayout()V
.end method

.method public abstract runningAnimation()Z
.end method

.method public abstract setCurrentPosition(Landroid/graphics/Rect;)V
.end method

.method public abstract setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V
.end method

.method public abstract setMovePosition(Landroid/graphics/Rect;)V
.end method

.method public abstract setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V
.end method

.method public abstract startAnimation()V
.end method
