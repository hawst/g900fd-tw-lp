.class public final Lcom/sec/android/mimage/photoretouching/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final actionbar_background_color:I = 0x7f04003e

.field public static final actionbar_button_text_color:I = 0x7f040041

.field public static final add_more_pictures_text_disable:I = 0x7f040001

.field public static final add_more_pictures_text_enable:I = 0x7f040000

.field public static final auto_shadow_color:I = 0x7f040033

.field public static final bottom_button_thumb_text_color:I = 0x7f040042

.field public static final bright_text_dark_focused:I = 0x7f040043

.field public static final bright_text_dark_focused_decoration:I = 0x7f040044

.field public static final bright_text_dark_focused_popup:I = 0x7f040045

.field public static final btn_bottom_shadow_color:I = 0x7f040003

.field public static final btn_effect_manager_text:I = 0x7f040039

.field public static final btn_effect_manager_text_dim:I = 0x7f04003a

.field public static final btn_submenu_middle_btn_text_dim:I = 0x7f040009

.field public static final btn_submenu_middle_btn_text_foucs:I = 0x7f04000a

.field public static final btn_submenu_middle_btn_text_normal:I = 0x7f04000c

.field public static final btn_submenu_middle_btn_text_press:I = 0x7f04000b

.field public static final btn_submenu_popup_icon_text:I = 0x7f040008

.field public static final btn_text:I = 0x7f040004

.field public static final btn_text_dim:I = 0x7f040007

.field public static final btn_text_focus:I = 0x7f040005

.field public static final btn_text_press:I = 0x7f040006

.field public static final clipboard_panel_toggle_light:I = 0x7f04003f

.field public static final clipboard_ripple_color_mask:I = 0x7f040040

.field public static final color_white:I = 0x7f040046

.field public static final deco_tab_selector_yellow:I = 0x7f04003c

.field public static final editbox_popup_save_text:I = 0x7f040047

.field public static final effect_manager_button_text:I = 0x7f040048

.field public static final face_circle_color:I = 0x7f04003b

.field public static final help_popup_text_color:I = 0x7f040037

.field public static final help_popup_text_shadow_color:I = 0x7f040038

.field public static final middle_button_text_color:I = 0x7f040049

.field public static final rect_controller_line_color:I = 0x7f040035

.field public static final ripple_material_light:I = 0x7f04003d

.field public static final selection_shadow_color:I = 0x7f040034

.field public static final stamp_middle_layer_color:I = 0x7f04000d

.field public static final stamp_shadow_color:I = 0x7f040032

.field public static final stamp_style11_1st_text_color:I = 0x7f040015

.field public static final stamp_style12_3rd_text_color:I = 0x7f040016

.field public static final stamp_style13_1st_text_color:I = 0x7f040017

.field public static final stamp_style14_1st_text_color:I = 0x7f040018

.field public static final stamp_style15_1st_text_color:I = 0x7f040019

.field public static final stamp_style16_1st_text_color:I = 0x7f04001a

.field public static final stamp_style17_1st_text_color:I = 0x7f04001b

.field public static final stamp_style18_1st_text_color:I = 0x7f04001c

.field public static final stamp_style19_1st_text_color:I = 0x7f04001d

.field public static final stamp_style1_1st_text_color:I = 0x7f04000e

.field public static final stamp_style20_1st_text_color:I = 0x7f04001e

.field public static final stamp_style20_3rd_text_color:I = 0x7f04001f

.field public static final stamp_style21_1st_text_color:I = 0x7f040020

.field public static final stamp_style22_1st_text_color:I = 0x7f040021

.field public static final stamp_style23_1st_text_color:I = 0x7f040022

.field public static final stamp_style24_1st_text_color:I = 0x7f040023

.field public static final stamp_style25_1st_text_color:I = 0x7f040024

.field public static final stamp_style26_1st_text_color:I = 0x7f040025

.field public static final stamp_style26_4th_text_color:I = 0x7f040026

.field public static final stamp_style27_1st_text_color:I = 0x7f040027

.field public static final stamp_style27_4th_text_color:I = 0x7f040028

.field public static final stamp_style28_1st_text_color:I = 0x7f040029

.field public static final stamp_style29_1st_text_color:I = 0x7f04002a

.field public static final stamp_style2_1st_text_color:I = 0x7f04000f

.field public static final stamp_style30_1st_text_color:I = 0x7f04002b

.field public static final stamp_style30_2nd_text_color:I = 0x7f04002c

.field public static final stamp_style31_1st_text_color:I = 0x7f04002d

.field public static final stamp_style31_2nd_text_color:I = 0x7f04002e

.field public static final stamp_style32_1st_text_color:I = 0x7f04002f

.field public static final stamp_style33_1st_text_color:I = 0x7f040030

.field public static final stamp_style34_1st_text_color:I = 0x7f040031

.field public static final stamp_style4_1st_text_color:I = 0x7f040010

.field public static final stamp_style7_1st_text_color:I = 0x7f040011

.field public static final stamp_style8_1st_text_color:I = 0x7f040012

.field public static final stamp_style9_1st_text_color:I = 0x7f040013

.field public static final stamp_style9_4th_text_color:I = 0x7f040014

.field public static final temp_red:I = 0x7f040036

.field public static final transparent:I = 0x7f040002


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
