.class public Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
.super Landroid/widget/LinearLayout;
.source "BottomIconButtonLayout.java"


# static fields
.field public static final LEFT:I = 0x1

.field public static final MIDDLE:I = 0x0

.field public static final RIGHT:I = 0x2


# instance fields
.field private mButtonId:I

.field private mButtonType:I

.field private mContext:Landroid/content/Context;

.field private mDivider:Landroid/widget/LinearLayout;

.field private mEnabled:Z

.field private mIconId:I

.field private mIsSubmenuButton:Z

.field private mLayout:Landroid/view/ViewGroup;

.field private mRecent:Z

.field private mSelected:Z

.field private mSideButton:I

.field private mTextId:Ljava/lang/String;

.field private mWithoutDimed:Z

.field private penColorLine:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutResId"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 23
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mSelected:Z

    .line 24
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mEnabled:Z

    .line 25
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mWithoutDimed:Z

    .line 26
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mRecent:Z

    .line 27
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mButtonId:I

    .line 28
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mIconId:I

    .line 29
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mButtonType:I

    .line 30
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mTextId:Ljava/lang/String;

    .line 32
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mIsSubmenuButton:Z

    .line 33
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mSideButton:I

    .line 373
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 374
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mContext:Landroid/content/Context;

    .line 375
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 376
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->penColorLine:Landroid/widget/LinearLayout;

    .line 42
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mContext:Landroid/content/Context;

    .line 43
    invoke-static {p1, p2, p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 47
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mButtonId:I

    .line 49
    return-void
.end method


# virtual methods
.method public changeRes(ILjava/lang/String;)V
    .locals 5
    .param p1, "iconId"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 200
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mIconId:I

    .line 201
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mTextId:Ljava/lang/String;

    .line 203
    const v3, 0x7f090004

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 204
    .local v1, "icon":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 206
    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 208
    :cond_0
    const v3, 0x7f090005

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 209
    .local v2, "tView":Landroid/widget/TextView;
    const-string v3, "sans-serif-regular"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 210
    .local v0, "font":Landroid/graphics/Typeface;
    if-eqz v2, :cond_1

    .line 212
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 213
    if-eqz p2, :cond_1

    .line 215
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    :cond_1
    return-void
.end method

.method public configurationChanged()V
    .locals 6

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->removeAllViews()V

    .line 358
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mButtonId:I

    invoke-static {v0, v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 360
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mButtonType:I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mIconId:I

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mTextId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mIsSubmenuButton:Z

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mSideButton:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    .line 361
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mSelected:Z

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setSelected(Z)V

    .line 362
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setPressed(Z)V

    .line 363
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mEnabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mWithoutDimed:Z

    if-nez v0, :cond_1

    .line 365
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mEnabled:Z

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 371
    :cond_0
    :goto_0
    return-void

    .line 367
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mEnabled:Z

    if-nez v0, :cond_0

    .line 369
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mEnabled:Z

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setEnabledWidthChildren(Z)V

    goto :goto_0
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mTextId:Ljava/lang/String;

    .line 53
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mContext:Landroid/content/Context;

    .line 54
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 55
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 58
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 61
    :cond_0
    return-void
.end method

.method public getButtonType()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mButtonType:I

    return v0
.end method

.method public init(IILjava/lang/String;ZI)V
    .locals 10
    .param p1, "buttonType"    # I
    .param p2, "iconId"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "isSubmenu"    # Z
    .param p5, "side"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mButtonType:I

    .line 65
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mIconId:I

    .line 66
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mTextId:Ljava/lang/String;

    .line 68
    iput-boolean p4, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mIsSubmenuButton:Z

    .line 69
    iput p5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mSideButton:I

    .line 71
    const/4 v0, 0x3

    .line 72
    .local v0, "SIDE_ADDITIONAL_WIDTH":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f05024a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 74
    .local v1, "btnDividerHeight":I
    const v7, 0x7f090004

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 75
    .local v3, "icon":Landroid/widget/ImageView;
    if-eqz v3, :cond_0

    .line 77
    invoke-virtual {v3, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 78
    if-eqz p4, :cond_0

    .line 79
    invoke-virtual {v3, p3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mContext:Landroid/content/Context;

    invoke-static {v7, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 86
    :cond_0
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/ViewGroup;->measure(II)V

    .line 88
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f05024b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 91
    .local v4, "padding":I
    if-nez p4, :cond_1

    .line 92
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mSideButton:I

    packed-switch v7, :pswitch_data_0

    .line 108
    :cond_1
    :goto_0
    const v7, 0x7f090005

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 110
    .local v6, "tView":Landroid/widget/TextView;
    const-string v7, "sans-serif-regular"

    const/4 v8, 0x0

    invoke-static {v7, v8}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    .line 111
    .local v2, "font":Landroid/graphics/Typeface;
    if-eqz v6, :cond_3

    .line 113
    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 114
    if-eqz p3, :cond_3

    .line 116
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p3

    .line 118
    const-string v7, " "

    invoke-virtual {p3, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 120
    const-string v7, " "

    invoke-virtual {p3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 121
    .local v5, "subs":[Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    const/4 v8, 0x0

    aget-object v8, v5, v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x1

    aget-object v8, v5, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 123
    .end local v5    # "subs":[Ljava/lang/String;
    :cond_2
    invoke-virtual {v6, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setId(I)V

    .line 130
    iget-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mIsSubmenuButton:Z

    if-eqz v7, :cond_4

    .line 132
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 134
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mContext:Landroid/content/Context;

    check-cast v7, Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f030021

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 135
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    if-eqz v7, :cond_4

    .line 136
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    const/4 v8, 0x1

    invoke-virtual {p0, v7, v8, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->addView(Landroid/view/View;II)V

    .line 138
    :cond_4
    return-void

    .line 95
    .end local v2    # "font":Landroid/graphics/Typeface;
    .end local v6    # "tView":Landroid/widget/TextView;
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v7, v4, v4, v4, v4}, Landroid/view/ViewGroup;->setPadding(IIII)V

    goto :goto_0

    .line 98
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v4, v9, v4}, Landroid/view/ViewGroup;->setPadding(IIII)V

    goto :goto_0

    .line 101
    :pswitch_2
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    const/4 v8, 0x0

    invoke-virtual {v7, v8, v4, v4, v4}, Landroid/view/ViewGroup;->setPadding(IIII)V

    goto/16 :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public initCollageButtonsWithoutTextDisplay(IILjava/lang/String;ZI)V
    .locals 5
    .param p1, "buttonType"    # I
    .param p2, "iconId"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "isSubmenu"    # Z
    .param p5, "side"    # I

    .prologue
    const/4 v4, 0x0

    .line 141
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mButtonType:I

    .line 142
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mIconId:I

    .line 143
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mTextId:Ljava/lang/String;

    .line 144
    iput-boolean p4, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mIsSubmenuButton:Z

    .line 145
    iput p5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mSideButton:I

    .line 146
    const v2, 0x7f090004

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 147
    .local v0, "icon":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 150
    if-eqz p4, :cond_0

    .line 151
    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mContext:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 155
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4, v4}, Landroid/view/ViewGroup;->measure(II)V

    .line 156
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05024b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 157
    .local v1, "padding":I
    if-eqz p4, :cond_1

    .line 158
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mSideButton:I

    packed-switch v2, :pswitch_data_0

    .line 173
    :cond_1
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setId(I)V

    .line 174
    return-void

    .line 161
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    goto :goto_0

    .line 164
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4, v1, v4, v1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    goto :goto_0

    .line 167
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4, v1, v1, v1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    goto :goto_0

    .line 158
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isFocused()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 335
    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mIsSubmenuButton:Z

    if-eqz v3, :cond_1

    .line 337
    const v3, 0x7f090004

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 338
    .local v0, "icon":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 339
    invoke-virtual {v0}, Landroid/widget/ImageView;->isFocused()Z

    move-result v2

    .line 353
    .end local v0    # "icon":Landroid/widget/ImageView;
    :cond_0
    :goto_0
    return v2

    .line 345
    :cond_1
    const v3, 0x7f090077

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 346
    .local v1, "icon_background":Landroid/widget/ImageView;
    if-eqz v1, :cond_2

    .line 347
    invoke-virtual {v1}, Landroid/widget/ImageView;->isFocused()Z

    move-result v2

    goto :goto_0

    .line 350
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 353
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->isFocused()Z

    move-result v2

    goto :goto_0
.end method

.method public isRecent()Z
    .locals 1

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mRecent:Z

    return v0
.end method

.method public removeColorLine()V
    .locals 2

    .prologue
    .line 221
    const v0, 0x7f090075

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->penColorLine:Landroid/widget/LinearLayout;

    .line 222
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->penColorLine:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 223
    return-void
.end method

.method public removeDivider()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 197
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mEnabled:Z

    if-eqz v0, :cond_0

    .line 264
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 265
    :cond_0
    return-void
.end method

.method public setEnabledWidthChildren(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 276
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setEnabledWidthChildren(ZI)V

    .line 277
    return-void
.end method

.method public setEnabledWidthChildren(ZI)V
    .locals 6
    .param p1, "enabled"    # Z
    .param p2, "buttonId"    # I

    .prologue
    .line 281
    const-string v4, "sj"

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->whoIsCallMe(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 282
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mEnabled:Z

    .line 285
    const v4, 0x7f090005

    :try_start_0
    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 286
    .local v3, "tView":Landroid/widget/TextView;
    if-eqz v3, :cond_0

    .line 288
    if-eqz p1, :cond_5

    .line 289
    const v4, 0x1e300002

    if-ne p2, v4, :cond_4

    .line 290
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f040049

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 295
    :goto_0
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 296
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 297
    sget-object v4, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 299
    :cond_0
    const v4, 0x7f090004

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 300
    .local v1, "icon":Landroid/widget/ImageView;
    if-eqz v1, :cond_2

    .line 303
    if-eqz p1, :cond_1

    .line 304
    const/16 v4, 0xff

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 307
    :cond_1
    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 311
    :cond_2
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 313
    .local v2, "parent":Landroid/view/View;
    if-eqz v2, :cond_3

    .line 314
    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 315
    invoke-virtual {v2, p1}, Landroid/view/View;->setFocusable(Z)V

    .line 318
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 324
    .end local v1    # "icon":Landroid/widget/ImageView;
    .end local v2    # "parent":Landroid/view/View;
    .end local v3    # "tView":Landroid/widget/TextView;
    :goto_1
    return-void

    .line 292
    .restart local v3    # "tView":Landroid/widget/TextView;
    :cond_4
    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_0
    .catch Landroid/view/ViewRootImpl$CalledFromWrongThreadException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 320
    .end local v3    # "tView":Landroid/widget/TextView;
    :catch_0
    move-exception v0

    .line 322
    .local v0, "e":Landroid/view/ViewRootImpl$CalledFromWrongThreadException;
    invoke-virtual {v0}, Landroid/view/ViewRootImpl$CalledFromWrongThreadException;->printStackTrace()V

    goto :goto_1

    .line 294
    .end local v0    # "e":Landroid/view/ViewRootImpl$CalledFromWrongThreadException;
    .restart local v3    # "tView":Landroid/widget/TextView;
    :cond_5
    const v4, 0x66ffffff

    :try_start_1
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_1
    .catch Landroid/view/ViewRootImpl$CalledFromWrongThreadException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public setEnabledWithoutDim(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 327
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mEnabled:Z

    .line 328
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mWithoutDimed:Z

    .line 329
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 330
    return-void
.end method

.method public setIconResource(I)V
    .locals 2
    .param p1, "iconId"    # I

    .prologue
    .line 227
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mIconId:I

    .line 228
    const v1, 0x7f090004

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 229
    .local v0, "icon":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 231
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 233
    :cond_0
    return-void
.end method

.method public setPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 271
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setPressed(Z)V

    .line 272
    return-void
.end method

.method public setRecent(Z)V
    .locals 0
    .param p1, "isRecent"    # Z

    .prologue
    .line 185
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mRecent:Z

    .line 186
    return-void
.end method

.method public setSelected(Z)V
    .locals 4
    .param p1, "selected"    # Z

    .prologue
    const/4 v3, 0x0

    .line 237
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mSelected:Z

    .line 239
    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 240
    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setSelected(Z)V

    .line 241
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->mSelected:Z

    invoke-super {p0, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 244
    :cond_0
    const v2, 0x7f090070

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 245
    .local v0, "iconOverlay":Landroid/widget/ImageView;
    if-eqz v0, :cond_1

    .line 249
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 252
    :cond_1
    const v2, 0x7f090005

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 253
    .local v1, "tView":Landroid/widget/TextView;
    if-eqz v1, :cond_2

    .line 254
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 255
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 256
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 258
    :cond_2
    return-void
.end method

.method public setVisibleText()V
    .locals 2

    .prologue
    .line 177
    const v1, 0x7f090005

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 178
    .local v0, "tView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 180
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    :cond_0
    return-void
.end method
