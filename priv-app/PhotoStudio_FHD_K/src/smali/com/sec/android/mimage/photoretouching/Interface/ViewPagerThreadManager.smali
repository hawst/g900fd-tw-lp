.class public Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;
.super Ljava/lang/Object;
.source "ViewPagerThreadManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$ApplyThread;,
        Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$OnCallBackForViewPager;
    }
.end annotation


# instance fields
.field private mDoing:Z

.field private mIndexList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

.field private myCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$OnCallBackForViewPager;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$OnCallBackForViewPager;)V
    .locals 1
    .param p1, "onCallBack"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$OnCallBackForViewPager;

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    .line 91
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mIndexList:Ljava/util/ArrayList;

    .line 92
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$OnCallBackForViewPager;

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mDoing:Z

    .line 28
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mIndexList:Ljava/util/ArrayList;

    .line 30
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$OnCallBackForViewPager;

    .line 31
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;Z)V
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mDoing:Z

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mIndexList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;I)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->applyData(I)V

    return-void
.end method

.method private applyData(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$OnCallBackForViewPager;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$OnCallBackForViewPager;->applyEffect(I)V

    .line 58
    return-void
.end method

.method private startThread()V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mDoing:Z

    if-nez v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$ApplyThread;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$ApplyThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$ApplyThread;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;->submit(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;)Lcom/sec/android/mimage/photoretouching/util/Future;

    .line 53
    :cond_1
    return-void
.end method


# virtual methods
.method public isDoingThread()Z
    .locals 2

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW isDoingThread: mDoing="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mDoing:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 64
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mDoing:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 65
    const/4 v0, 0x0

    .line 66
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removeData(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 42
    return-void
.end method

.method public setData(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->mIndexList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->startThread()V

    .line 37
    return-void
.end method
