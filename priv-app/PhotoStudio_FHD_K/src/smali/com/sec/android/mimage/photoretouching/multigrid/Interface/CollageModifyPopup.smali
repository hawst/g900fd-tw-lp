.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;
.super Landroid/widget/LinearLayout;
.source "CollageModifyPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$CollageModifyPopupCallback;
    }
.end annotation


# instance fields
.field private mAnimate:Z

.field private mButtons:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mMainLayout:Landroid/widget/FrameLayout;

.field private mModifyPopupCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$CollageModifyPopupCallback;

.field private mTouchListener:Landroid/view/View$OnTouchListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mAnimate:Z

    .line 362
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mContext:Landroid/content/Context;

    .line 364
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mMainLayout:Landroid/widget/FrameLayout;

    .line 365
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    .line 366
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mButtons:Ljava/util/ArrayList;

    .line 368
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 369
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mModifyPopupCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$CollageModifyPopupCallback;

    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->init(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mAnimate:Z

    .line 362
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mContext:Landroid/content/Context;

    .line 364
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mMainLayout:Landroid/widget/FrameLayout;

    .line 365
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    .line 366
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mButtons:Ljava/util/ArrayList;

    .line 368
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 369
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mModifyPopupCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$CollageModifyPopupCallback;

    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->init(Landroid/content/Context;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mAnimate:Z

    .line 362
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mContext:Landroid/content/Context;

    .line 364
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mMainLayout:Landroid/widget/FrameLayout;

    .line 365
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    .line 366
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mButtons:Ljava/util/ArrayList;

    .line 368
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 369
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mModifyPopupCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$CollageModifyPopupCallback;

    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->init(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$CollageModifyPopupCallback;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mModifyPopupCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$CollageModifyPopupCallback;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;Z)V
    .locals 0

    .prologue
    .line 158
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mAnimate:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mContext:Landroid/content/Context;

    .line 67
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const v1, 0x7f09006f

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mMainLayout:Landroid/widget/FrameLayout;

    .line 68
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030037

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    .line 69
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 70
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->initTouchListener()V

    .line 71
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->initButtons()V

    .line 72
    return-void
.end method

.method private initButtons()V
    .locals 6

    .prologue
    .line 76
    const-string v4, "sans-serif"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    .line 78
    .local v1, "font":Landroid/graphics/Typeface;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mButtons:Ljava/util/ArrayList;

    if-nez v4, :cond_0

    .line 80
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mButtons:Ljava/util/ArrayList;

    .line 82
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mButtons:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 84
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v4, 0x4

    if-lt v2, v4, :cond_1

    .line 95
    return-void

    .line 86
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    const v5, 0x7f09008c

    add-int/2addr v5, v2

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 87
    .local v0, "button":Landroid/widget/FrameLayout;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 88
    const v4, 0x7f090005

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 89
    .local v3, "textView":Landroid/widget/TextView;
    if-eqz v3, :cond_2

    .line 91
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 93
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mButtons:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private initTouchListener()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$1;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 149
    :cond_0
    return-void
.end method


# virtual methods
.method public configurationChanged()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 152
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 154
    invoke-virtual {p0, v1, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->show(ZZ)Z

    .line 156
    :cond_0
    return-void
.end method

.method public isAnimating()Z
    .locals 1

    .prologue
    .line 372
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mAnimate:Z

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 301
    const/4 v0, 0x1

    .line 303
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setModifyPopupCallback(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$CollageModifyPopupCallback;)V
    .locals 0
    .param p1, "modifyPopupCallback"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$CollageModifyPopupCallback;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mModifyPopupCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$CollageModifyPopupCallback;

    .line 61
    return-void
.end method

.method public setPosition(Landroid/graphics/RectF;)V
    .locals 11
    .param p1, "selectedRect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v10, -0x2

    .line 308
    if-nez p1, :cond_1

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getDisplayWidthBasedOnLand()I

    move-result v1

    .line 312
    .local v1, "displayWidth":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getDisplayHeightBasedOnLand()I

    move-result v0

    .line 313
    .local v0, "displayHeight":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f05001c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 314
    .local v6, "popupWidth":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f05001d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 316
    .local v5, "popupHeight":I
    iget v7, p1, Landroid/graphics/RectF;->bottom:F

    float-to-int v3, v7

    .line 317
    .local v3, "marginTop":I
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    div-int/lit8 v8, v6, 0x2

    int-to-float v8, v8

    sub-float/2addr v7, v8

    float-to-int v2, v7

    .line 323
    .local v2, "marginLeft":I
    add-int v7, v2, v6

    if-ge v1, v7, :cond_5

    .line 325
    int-to-float v7, v1

    iget v8, p1, Landroid/graphics/RectF;->right:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v7

    int-to-float v8, v6

    sub-float/2addr v7, v8

    float-to-int v2, v7

    .line 332
    :cond_2
    :goto_1
    if-gez v2, :cond_3

    .line 334
    const/4 v2, 0x0

    .line 338
    :cond_3
    sub-int v7, v0, v5

    add-int v8, v3, v5

    if-ge v7, v8, :cond_4

    .line 340
    iget v7, p1, Landroid/graphics/RectF;->top:F

    int-to-float v8, v5

    sub-float/2addr v7, v8

    float-to-int v3, v7

    .line 341
    if-gez v3, :cond_4

    .line 343
    iget v7, p1, Landroid/graphics/RectF;->top:F

    float-to-int v3, v7

    .line 349
    :cond_4
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v10, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 350
    .local v4, "params":Landroid/widget/FrameLayout$LayoutParams;
    iput v3, v4, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 351
    iput v2, v4, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 353
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 355
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    invoke-virtual {v7}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/ActionBar;->getHeight()I

    move-result v7

    if-ge v3, v7, :cond_0

    .line 356
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    invoke-virtual {v7}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/ActionBar;->getHeight()I

    move-result v7

    iput v7, v4, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 357
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 327
    .end local v4    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :cond_5
    if-gez v2, :cond_2

    .line 329
    const/4 v7, 0x0

    iget v8, p1, Landroid/graphics/RectF;->left:F

    float-to-int v8, v8

    div-int/lit8 v8, v8, 0x2

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_1
.end method

.method public show(ZLandroid/graphics/RectF;)Z
    .locals 2
    .param p1, "show"    # Z
    .param p2, "selectedRect"    # Landroid/graphics/RectF;

    .prologue
    .line 286
    const/4 v0, 0x0

    .line 288
    .local v0, "isHided":Z
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->show(ZZ)Z

    move-result v0

    .line 289
    if-eqz p1, :cond_0

    .line 291
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->setPosition(Landroid/graphics/RectF;)V

    .line 294
    :cond_0
    return v0
.end method

.method public show(ZZ)Z
    .locals 13
    .param p1, "show"    # Z
    .param p2, "anim"    # Z

    .prologue
    .line 162
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : show : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 163
    const/4 v11, 0x0

    .line 165
    .local v11, "isHided":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const v2, 0x7f09006f

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mMainLayout:Landroid/widget/FrameLayout;

    .line 167
    if-eqz p1, :cond_2

    .line 169
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 171
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup;

    .line 173
    .local v12, "viewGroup":Landroid/view/ViewGroup;
    if-eqz v12, :cond_0

    .line 175
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v12, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 178
    .end local v12    # "viewGroup":Landroid/view/ViewGroup;
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->setVisibility(I)V

    .line 179
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 180
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mMainLayout:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 181
    new-instance v10, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x1

    invoke-direct {v10, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 182
    .local v10, "animation":Landroid/view/animation/AnimationSet;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const v1, 0x3f666666    # 0.9f

    const/high16 v2, 0x3f800000    # 1.0f

    const v3, 0x3f666666    # 0.9f

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 183
    .local v0, "scaleAnim":Landroid/view/animation/ScaleAnimation;
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const v1, 0x3f19999a    # 0.6f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 184
    .local v9, "alphaAnim":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 185
    invoke-virtual {v10, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 186
    new-instance v1, Landroid/view/animation/interpolator/SineEaseInOut;

    invoke-direct {v1}, Landroid/view/animation/interpolator/SineEaseInOut;-><init>()V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 187
    const-wide/16 v2, 0x55

    invoke-virtual {v10, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 189
    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$2;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;)V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 209
    if-eqz p2, :cond_1

    .line 210
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->clearAnimation()V

    .line 211
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v10}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 281
    .end local v0    # "scaleAnim":Landroid/view/animation/ScaleAnimation;
    .end local v9    # "alphaAnim":Landroid/view/animation/AlphaAnimation;
    .end local v10    # "animation":Landroid/view/animation/AnimationSet;
    :cond_1
    :goto_0
    return v11

    .line 219
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 221
    new-instance v10, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x1

    invoke-direct {v10, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 222
    .restart local v10    # "animation":Landroid/view/animation/AnimationSet;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3f666666    # 0.9f

    const/high16 v3, 0x3f800000    # 1.0f

    const v4, 0x3f666666    # 0.9f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 223
    .restart local v0    # "scaleAnim":Landroid/view/animation/ScaleAnimation;
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3f19999a    # 0.6f

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 224
    .restart local v9    # "alphaAnim":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 225
    invoke-virtual {v10, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 226
    new-instance v1, Landroid/view/animation/interpolator/SineEaseInOut;

    invoke-direct {v1}, Landroid/view/animation/interpolator/SineEaseInOut;-><init>()V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 227
    const-wide/16 v2, 0x55

    invoke-virtual {v10, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 228
    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$3;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$3;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;)V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 258
    if-eqz p2, :cond_3

    .line 259
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->clearAnimation()V

    .line 260
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v10}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 277
    :goto_1
    const/4 v11, 0x1

    goto :goto_0

    .line 264
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_4

    .line 266
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup;

    .line 268
    .restart local v12    # "viewGroup":Landroid/view/ViewGroup;
    if-eqz v12, :cond_4

    .line 270
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v12, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 273
    .end local v12    # "viewGroup":Landroid/view/ViewGroup;
    :cond_4
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->setVisibility(I)V

    .line 274
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mCollageModifyPopupLayout:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method
