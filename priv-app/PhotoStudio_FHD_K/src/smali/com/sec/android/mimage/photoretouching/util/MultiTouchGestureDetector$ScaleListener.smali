.class Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$ScaleListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "MultiTouchGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$ScaleListener;->this$0:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$ScaleListener;)V
    .locals 0

    .prologue
    .line 202
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$ScaleListener;-><init>(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$ScaleListener;->this$0:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    # invokes: Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->removeMotion()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->access$0(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$ScaleListener;->this$0:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    # getter for: Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mListener:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->access$1(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;->onMultiTouchScale(FFF)Z

    .line 207
    const/4 v0, 0x1

    return v0
.end method
