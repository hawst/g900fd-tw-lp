.class public interface abstract Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$GpsSpeedRef;
.super Ljava/lang/Object;
.source "GalleryExifInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GpsSpeedRef"
.end annotation


# static fields
.field public static final KILOMETERS:Ljava/lang/String; = "K"

.field public static final KNOTS:Ljava/lang/String; = "N"

.field public static final MILES:Ljava/lang/String; = "M"
