.class public Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;
.super Landroid/widget/ImageView;
.source "ImageEditView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;,
        Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;,
        Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnDrawCallback;,
        Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnLayoutCallback;
    }
.end annotation


# instance fields
.field final LIGHTING_WIDTH_HEIGHT:I

.field private Testthread:Ljava/lang/Thread;

.field private isRunning:Z

.field private mBackgroundFrame:Landroid/graphics/drawable/NinePatchDrawable;

.field private mClearPaint:Landroid/graphics/Paint;

.field private mContext:Landroid/content/Context;

.field private mDensity:F

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

.field private mInitViewCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

.field private mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

.field private mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

.field private mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

.field private mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

.field private mOnDrawCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnDrawCallback;

.field private mOnLayoutCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnLayoutCallback;

.field private mPaint:Landroid/graphics/Paint;

.field private mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

.field private mPreviousOnLayout:Z

.field private mViewHeight:I

.field private mViewWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 655
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mViewWidth:I

    .line 656
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mViewHeight:I

    .line 660
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mDensity:F

    .line 661
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mOnDrawCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnDrawCallback;

    .line 662
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mInitViewCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

    .line 663
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mOnLayoutCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnLayoutCallback;

    .line 664
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;

    .line 665
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPaint:Landroid/graphics/Paint;

    .line 666
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mClearPaint:Landroid/graphics/Paint;

    .line 667
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .line 668
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mBackgroundFrame:Landroid/graphics/drawable/NinePatchDrawable;

    .line 669
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPreviousOnLayout:Z

    .line 670
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->isRunning:Z

    .line 671
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->Testthread:Ljava/lang/Thread;

    .line 673
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 674
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 675
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 676
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 677
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 678
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->LIGHTING_WIDTH_HEIGHT:I

    .line 59
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;

    .line 60
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setResource()V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 63
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 655
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mViewWidth:I

    .line 656
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mViewHeight:I

    .line 660
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mDensity:F

    .line 661
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mOnDrawCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnDrawCallback;

    .line 662
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mInitViewCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

    .line 663
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mOnLayoutCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnLayoutCallback;

    .line 664
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;

    .line 665
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPaint:Landroid/graphics/Paint;

    .line 666
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mClearPaint:Landroid/graphics/Paint;

    .line 667
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .line 668
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mBackgroundFrame:Landroid/graphics/drawable/NinePatchDrawable;

    .line 669
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPreviousOnLayout:Z

    .line 670
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->isRunning:Z

    .line 671
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->Testthread:Ljava/lang/Thread;

    .line 673
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 674
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 675
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 676
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 677
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 678
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->LIGHTING_WIDTH_HEIGHT:I

    .line 64
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;

    .line 65
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setResource()V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 69
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 655
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mViewWidth:I

    .line 656
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mViewHeight:I

    .line 660
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mDensity:F

    .line 661
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mOnDrawCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnDrawCallback;

    .line 662
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mInitViewCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

    .line 663
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mOnLayoutCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnLayoutCallback;

    .line 664
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;

    .line 665
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPaint:Landroid/graphics/Paint;

    .line 666
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mClearPaint:Landroid/graphics/Paint;

    .line 667
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .line 668
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mBackgroundFrame:Landroid/graphics/drawable/NinePatchDrawable;

    .line 669
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPreviousOnLayout:Z

    .line 670
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->isRunning:Z

    .line 671
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->Testthread:Ljava/lang/Thread;

    .line 673
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 674
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 675
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 676
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 677
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 678
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->LIGHTING_WIDTH_HEIGHT:I

    .line 70
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;

    .line 71
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setResource()V

    .line 72
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;)V
    .locals 0

    .prologue
    .line 676
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;
    .locals 1

    .prologue
    .line 677
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    .locals 1

    .prologue
    .line 673
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    .locals 1

    .prologue
    .line 674
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;)V
    .locals 0

    .prologue
    .line 673
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;)V
    .locals 0

    .prologue
    .line 674
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;)V
    .locals 0

    .prologue
    .line 675
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    return-void
.end method

.method private drawBackgroundFrame(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 302
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    if-eqz v2, :cond_1

    .line 304
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v1

    .line 305
    .local v1, "roi":Landroid/graphics/Rect;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    const/high16 v3, 0x1d000000

    if-ne v2, v3, :cond_0

    .line 307
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getCopyToDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 308
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getCopyToDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v1

    .line 310
    :cond_0
    if-eqz v1, :cond_1

    .line 312
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mBackgroundFrame:Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v2, :cond_1

    .line 314
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 315
    .local v0, "r":Landroid/graphics/Rect;
    iget v2, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v2, v2, -0x9

    .line 316
    iget v3, v1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v3, v3, -0x6

    .line 317
    iget v4, v1, Landroid/graphics/Rect;->right:I

    add-int/lit8 v4, v4, 0x9

    .line 318
    iget v5, v1, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v5, v5, 0xd

    .line 315
    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 319
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mBackgroundFrame:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 320
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mBackgroundFrame:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 324
    .end local v0    # "r":Landroid/graphics/Rect;
    .end local v1    # "roi":Landroid/graphics/Rect;
    :cond_1
    return-void
.end method

.method private drawLightingForTouchMovement(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 328
    const/16 v2, 0x9

    new-array v1, v2, [F

    .line 329
    .local v1, "values":[F
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    if-eqz v2, :cond_0

    .line 331
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 332
    .local v0, "m":Landroid/graphics/Matrix;
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 335
    .end local v0    # "m":Landroid/graphics/Matrix;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    if-eqz v2, :cond_5

    aget v2, v1, v3

    cmpl-float v2, v2, v4

    if-lez v2, :cond_5

    const/4 v2, 0x4

    aget v2, v1, v2

    cmpl-float v2, v2, v4

    if-lez v2, :cond_5

    .line 337
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v2, :cond_1

    .line 338
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-virtual {v2, p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawLighting(Landroid/graphics/Canvas;)V

    .line 339
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v2, :cond_2

    .line 340
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-virtual {v2, p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawLighting(Landroid/graphics/Canvas;)V

    .line 341
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v2, :cond_3

    .line 342
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-virtual {v2, p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawLighting(Landroid/graphics/Canvas;)V

    .line 343
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v2, :cond_4

    .line 344
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-virtual {v2, p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawLighting(Landroid/graphics/Canvas;)V

    .line 358
    :cond_4
    :goto_0
    return-void

    .line 348
    :cond_5
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v2, :cond_6

    .line 349
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;I)V

    .line 350
    :cond_6
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v2, :cond_7

    .line 351
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;I)V

    .line 352
    :cond_7
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v2, :cond_8

    .line 353
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;I)V

    .line 354
    :cond_8
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v2, :cond_4

    .line 355
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;I)V

    goto :goto_0
.end method

.method private setResource()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 388
    sget-object v2, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 389
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mDensity:F

    .line 390
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPaint:Landroid/graphics/Paint;

    .line 391
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 393
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mClearPaint:Landroid/graphics/Paint;

    .line 394
    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    .line 395
    .local v1, "xmode":Landroid/graphics/Xfermode;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mClearPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 396
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020497

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mBackgroundFrame:Landroid/graphics/drawable/NinePatchDrawable;

    .line 398
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    .line 399
    .local v0, "mode":I
    const/high16 v2, 0x20000000

    if-ne v0, v2, :cond_1

    .line 400
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$2;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->Testthread:Ljava/lang/Thread;

    .line 418
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->Testthread:Ljava/lang/Thread;

    if-eqz v2, :cond_0

    .line 419
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->Testthread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 434
    :cond_0
    :goto_0
    return-void

    .line 421
    :cond_1
    const/high16 v2, 0x2c000000

    if-ne v0, v2, :cond_2

    .line 422
    const-string v2, "JW lighting not created"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_0

    .line 425
    :cond_2
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    const/4 v3, 0x2

    invoke-direct {v2, p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;I)V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 426
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    const/4 v3, 0x3

    invoke-direct {v2, p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;I)V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 427
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-direct {v2, p0, v4}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;I)V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 428
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    const/4 v3, 0x1

    invoke-direct {v2, p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;I)V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    goto :goto_0
.end method


# virtual methods
.method public changeImageData(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;)V
    .locals 1
    .param p1, "iData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .line 76
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->initLighting()V

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->initLighting()V

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v0, :cond_2

    .line 81
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->initLighting()V

    .line 82
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v0, :cond_3

    .line 83
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->initLighting()V

    .line 84
    :cond_3
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 138
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->Testthread:Ljava/lang/Thread;

    .line 140
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPaint:Landroid/graphics/Paint;

    .line 141
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mBackgroundFrame:Landroid/graphics/drawable/NinePatchDrawable;

    .line 142
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .line 143
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->destroy()V

    .line 145
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->destroy()V

    .line 148
    :cond_1
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 149
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v0, :cond_2

    .line 150
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->destroy()V

    .line 151
    :cond_2
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 152
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    if-eqz v0, :cond_3

    .line 153
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->destroy()V

    .line 154
    :cond_3
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 155
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;

    .line 156
    return-void
.end method

.method public getViewBufferHeight()I
    .locals 3

    .prologue
    .line 128
    const/4 v0, 0x0

    .line 130
    .local v0, "ret":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->getHeight()I

    move-result v0

    .line 131
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mInitViewCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mInitViewCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

    invoke-interface {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;->getActionHeight()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mInitViewCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;->getBottomButtonHeight()I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 134
    :cond_0
    return v0
.end method

.method public getViewBufferWidth()I
    .locals 2

    .prologue
    .line 117
    const/4 v0, 0x0

    .line 120
    .local v0, "ret":I
    if-nez v0, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 124
    :cond_0
    return v0
.end method

.method public invalidate()V
    .locals 0

    .prologue
    .line 161
    invoke-super {p0}, Landroid/widget/ImageView;->invalidate()V

    .line 162
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mOnDrawCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnDrawCallback;

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 109
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mOnDrawCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnDrawCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnDrawCallback;->onDraw(Landroid/graphics/Canvas;)V

    .line 110
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 111
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->drawLightingForTouchMovement(Landroid/graphics/Canvas;)V

    .line 114
    :cond_0
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 207
    const-string v2, "JW ImageEditView: onLayout"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 208
    if-eqz p1, :cond_2

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mViewHeight:I

    .line 211
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mViewWidth:I

    .line 224
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mInitViewCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;->getActionHeight()I

    move-result v1

    .line 225
    .local v1, "topPos":I
    if-gtz v1, :cond_0

    .line 226
    const/4 v1, 0x1

    .line 227
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setTop(I)V

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->getViewBufferHeight()I

    move-result v2

    add-int v0, v2, v1

    .line 230
    .local v0, "bottomPos":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_2

    .line 232
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v0, v2, :cond_1

    .line 233
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 234
    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setBottom(I)V

    .line 236
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mOnLayoutCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnLayoutCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnLayoutCallback;->setViewSize()V

    .line 240
    .end local v0    # "bottomPos":I
    .end local v1    # "topPos":I
    :cond_2
    return-void
.end method

.method public setInitViewCallback(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;)V
    .locals 0
    .param p1, "initViewCallback"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mInitViewCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

    .line 100
    return-void
.end method

.method public setOnDrawCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnDrawCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnDrawCallback;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mOnDrawCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnDrawCallback;

    .line 92
    return-void
.end method

.method public setOnLayoutCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnLayoutCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnLayoutCallback;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mOnLayoutCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnLayoutCallback;

    .line 96
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnTouchListener;

    .prologue
    .line 246
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;Landroid/view/View$OnTouchListener;)V

    invoke-super {p0, v0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 299
    return-void
.end method

.method public setPinchZoomCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;)V
    .locals 0
    .param p1, "pinchZoomCallback"    # Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 88
    return-void
.end method
