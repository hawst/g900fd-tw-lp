.class public Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;
.super Landroid/widget/EditText;
.source "InputFileName.java"


# instance fields
.field final INPUT_TEXT_LENGTH:I

.field private mChangeFileName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 174
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mContext:Landroid/content/Context;

    .line 175
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mChangeFileName:Ljava/lang/String;

    .line 177
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->INPUT_TEXT_LENGTH:I

    .line 21
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mContext:Landroid/content/Context;

    .line 22
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->init()V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 174
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mContext:Landroid/content/Context;

    .line 175
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mChangeFileName:Ljava/lang/String;

    .line 177
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->INPUT_TEXT_LENGTH:I

    .line 28
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mContext:Landroid/content/Context;

    .line 29
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->init()V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 174
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mContext:Landroid/content/Context;

    .line 175
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mChangeFileName:Ljava/lang/String;

    .line 177
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->INPUT_TEXT_LENGTH:I

    .line 35
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mContext:Landroid/content/Context;

    .line 36
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->init()V

    .line 37
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private findFileNumberInString(Ljava/lang/String;)I
    .locals 7
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 77
    const/4 v3, 0x0

    .line 78
    .local v3, "number":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 79
    .local v1, "fileLength":I
    add-int/lit8 v5, v1, -0x1

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x29

    if-eq v5, v6, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v4

    .line 83
    :cond_1
    add-int/lit8 v2, v1, -0x2

    .local v2, "i":I
    :goto_1
    if-gez v2, :cond_2

    :goto_2
    move v4, v3

    .line 101
    goto :goto_0

    .line 85
    :cond_2
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 86
    .local v0, "dight":C
    const/16 v5, 0x30

    if-lt v0, v5, :cond_3

    const/16 v5, 0x39

    if-gt v0, v5, :cond_3

    .line 88
    shl-int/lit8 v3, v3, 0x1

    .line 89
    add-int/lit8 v5, v0, -0x30

    or-int/2addr v3, v5

    .line 83
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 91
    :cond_3
    const/16 v5, 0x28

    if-ne v0, v5, :cond_0

    .line 93
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mChangeFileName:Ljava/lang/String;

    goto :goto_2
.end method

.method private init()V
    .locals 5

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->selectAll()V

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->requestFocus()Z

    .line 108
    const/4 v2, 0x1

    new-array v0, v2, [Landroid/text/InputFilter;

    .line 109
    .local v0, "FilterArray":[Landroid/text/InputFilter;
    const/4 v2, 0x0

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    const/16 v4, 0x33

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v0, v2

    .line 110
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->setFilters([Landroid/text/InputFilter;)V

    .line 113
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;)V

    .line 171
    .local v1, "textWatcher":Landroid/text/TextWatcher;
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 172
    return-void
.end method


# virtual methods
.method public setCurEditTextContext(Ljava/lang/String;I)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "pos"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->setText(Ljava/lang/CharSequence;)V

    .line 41
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->setSelection(I)V

    .line 42
    return-void
.end method

.method public setNewFile(Ljava/lang/String;)V
    .locals 5
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 45
    const/4 v0, 0x0

    .line 46
    .local v0, "bCanSaveFileName":Z
    const/4 v2, 0x1

    .line 48
    .local v2, "fileNumber":I
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".jpg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 49
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->findFileNumberInString(Ljava/lang/String;)I

    move-result v2

    .line 52
    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 54
    const/4 v2, 0x1

    .line 60
    :goto_0
    if-eqz v0, :cond_2

    .line 71
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 73
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 74
    return-void

    .line 58
    :cond_1
    iget-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->mChangeFileName:Ljava/lang/String;

    .line 60
    goto :goto_0

    .line 62
    :cond_2
    new-instance v1, Ljava/io/File;

    .end local v1    # "file":Ljava/io/File;
    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ").jpg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 63
    .restart local v1    # "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 64
    const/4 v0, 0x1

    .line 65
    goto :goto_0

    .line 68
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
