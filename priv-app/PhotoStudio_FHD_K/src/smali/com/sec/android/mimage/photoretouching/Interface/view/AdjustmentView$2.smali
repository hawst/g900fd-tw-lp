.class Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$2;
.super Ljava/lang/Object;
.source "AdjustmentView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isBottomMax()Z
    .locals 2

    .prologue
    .line 161
    const/4 v0, 0x0

    .line 162
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->isBottomMax()Z

    move-result v0

    .line 164
    :cond_0
    return v0
.end method

.method public isLeftMax()Z
    .locals 2

    .prologue
    .line 152
    const/4 v0, 0x0

    .line 153
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->isLeftMax()Z

    move-result v0

    .line 155
    :cond_0
    return v0
.end method

.method public isRightMax()Z
    .locals 2

    .prologue
    .line 143
    const/4 v0, 0x0

    .line 144
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 145
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->isRightMax()Z

    move-result v0

    .line 146
    :cond_0
    return v0
.end method

.method public isTopMax()Z
    .locals 2

    .prologue
    .line 134
    const/4 v0, 0x0

    .line 135
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 136
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->isTopMax()Z

    move-result v0

    .line 137
    :cond_0
    return v0
.end method
