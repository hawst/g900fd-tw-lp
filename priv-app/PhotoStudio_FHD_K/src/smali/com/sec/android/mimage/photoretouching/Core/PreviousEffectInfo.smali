.class public Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;
.super Ljava/lang/Object;
.source "PreviousEffectInfo.java"


# instance fields
.field private mPreviousEffectValue:I

.field private mPreviousStatus:I

.field private mPreviousSubStatus:I


# direct methods
.method public constructor <init>(III)V
    .locals 1
    .param p1, "previousStatus"    # I
    .param p2, "previousSubStatus"    # I
    .param p3, "previousEffectValue"    # I

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->mPreviousStatus:I

    .line 5
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->mPreviousSubStatus:I

    .line 6
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->mPreviousEffectValue:I

    .line 10
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->mPreviousStatus:I

    .line 11
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->mPreviousSubStatus:I

    .line 12
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->mPreviousEffectValue:I

    .line 13
    return-void
.end method


# virtual methods
.method public getPreviousEffectValue()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->mPreviousEffectValue:I

    return v0
.end method

.method public getPreviousStatus()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->mPreviousStatus:I

    return v0
.end method

.method public getPreviousSubStatus()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->mPreviousSubStatus:I

    return v0
.end method
