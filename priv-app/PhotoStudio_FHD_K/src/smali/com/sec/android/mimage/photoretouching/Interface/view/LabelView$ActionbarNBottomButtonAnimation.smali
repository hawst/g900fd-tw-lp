.class Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;
.super Ljava/lang/Object;
.source "LabelView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionbarNBottomButtonAnimation"
.end annotation


# instance fields
.field private mIsShowing:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1325
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    .line 1324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1323
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 1326
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 1327
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 1

    .prologue
    .line 1346
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 1347
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1349
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->hide()V

    .line 1351
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1353
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->drawerHide()V

    .line 1355
    :cond_1
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 1330
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    return v0
.end method

.method public show()V
    .locals 1

    .prologue
    .line 1334
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 1335
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1337
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->show()V

    .line 1339
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1341
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->drawerShow()V

    .line 1343
    :cond_1
    return-void
.end method
