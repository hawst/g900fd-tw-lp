.class public Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
.super Ljava/lang/Object;
.source "RotateEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;,
        Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;
    }
.end annotation


# instance fields
.field private ANIM_HIDE:I

.field private ANIM_NONE:I

.field private ANIM_SHOW:I

.field private mAlpha:I

.field private mAlphaAnim:I

.field private mAngle:I

.field private mAngleTextBitmap:Landroid/graphics/Bitmap;

.field private mBitmap_bg:Landroid/graphics/Bitmap;

.field private mBitmap_degree:Landroid/graphics/Bitmap;

.field private mCanvasRoi:Landroid/graphics/Rect;

.field private mConfigChanged:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentSaveBtnState:Z

.field private mDrawAnimRunnable:Ljava/lang/Runnable;

.field private mFlipH:I

.field private mFlipV:I

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHandler:Landroid/os/Handler;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mInitialCanvasRoi:Landroid/graphics/Rect;

.field private mInitialPrevImageBuf:[I

.field private mPreX:F

.field private mPrevStraightenAngle:I

.field private mPreviewHeight:I

.field private mPreviewWidth:I

.field private mProgressLeftMagine:F

.field private mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

.field private mRotateMatrix:Landroid/graphics/Matrix;

.field private mStartX:F

.field private mStartY:F

.field private mStraightenAngle:I

.field private mStraightenAngleOnMove:I

.field private mStraightenEnable:Z

.field private mStraightenLines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[F>;"
        }
    .end annotation
.end field

.field private mStraightenStart:Z

.field private mTextBitmap:Landroid/graphics/Bitmap;

.field private mTouchMatrixForFlip:Landroid/graphics/Matrix;

.field private mViewHeight:I

.field private mViewWidth:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 878
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    .line 879
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 880
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    .line 881
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTouchMatrixForFlip:Landroid/graphics/Matrix;

    .line 882
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mGestureDetector:Landroid/view/GestureDetector;

    .line 883
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    .line 884
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    .line 885
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    .line 886
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    .line 887
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenEnable:Z

    .line 888
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenStart:Z

    .line 889
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    .line 890
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    .line 891
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPrevStraightenAngle:I

    .line 892
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStartX:F

    .line 893
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStartY:F

    .line 894
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    .line 896
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mViewWidth:I

    .line 897
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mViewHeight:I

    .line 898
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewWidth:I

    .line 899
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewHeight:I

    .line 901
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialPrevImageBuf:[I

    .line 903
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    .line 904
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    .line 906
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mConfigChanged:Z

    .line 907
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCurrentSaveBtnState:Z

    .line 909
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mProgressLeftMagine:F

    .line 910
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 911
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_degree:Landroid/graphics/Bitmap;

    .line 912
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTextBitmap:Landroid/graphics/Bitmap;

    .line 913
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    .line 914
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreX:F

    .line 917
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->ANIM_SHOW:I

    .line 918
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->ANIM_HIDE:I

    .line 919
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->ANIM_NONE:I

    .line 922
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    .line 923
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mDrawAnimRunnable:Ljava/lang/Runnable;

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 878
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    .line 879
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 880
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    .line 881
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTouchMatrixForFlip:Landroid/graphics/Matrix;

    .line 882
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mGestureDetector:Landroid/view/GestureDetector;

    .line 883
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    .line 884
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    .line 885
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    .line 886
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    .line 887
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenEnable:Z

    .line 888
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenStart:Z

    .line 889
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    .line 890
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    .line 891
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPrevStraightenAngle:I

    .line 892
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStartX:F

    .line 893
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStartY:F

    .line 894
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    .line 896
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mViewWidth:I

    .line 897
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mViewHeight:I

    .line 898
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewWidth:I

    .line 899
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewHeight:I

    .line 901
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialPrevImageBuf:[I

    .line 903
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    .line 904
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    .line 906
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mConfigChanged:Z

    .line 907
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCurrentSaveBtnState:Z

    .line 909
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mProgressLeftMagine:F

    .line 910
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 911
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_degree:Landroid/graphics/Bitmap;

    .line 912
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTextBitmap:Landroid/graphics/Bitmap;

    .line 913
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    .line 914
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreX:F

    .line 917
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->ANIM_SHOW:I

    .line 918
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->ANIM_HIDE:I

    .line 919
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->ANIM_NONE:I

    .line 922
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    .line 923
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mDrawAnimRunnable:Ljava/lang/Runnable;

    .line 55
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mHandler:Landroid/os/Handler;

    .line 56
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    .line 57
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 58
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    .line 59
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 60
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTouchMatrixForFlip:Landroid/graphics/Matrix;

    .line 61
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->initGesture()V

    .line 62
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;
    .locals 1

    .prologue
    .line 883
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)I
    .locals 1

    .prologue
    .line 920
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlphaAnim:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)I
    .locals 1

    .prologue
    .line 917
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->ANIM_SHOW:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;I)V
    .locals 0

    .prologue
    .line 920
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlphaAnim:I

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;I)V
    .locals 0

    .prologue
    .line 922
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;I)V
    .locals 0

    .prologue
    .line 890
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    return-void
.end method

.method private applyToCanvasStraighten([IIIILandroid/graphics/Bitmap;Z)V
    .locals 15
    .param p1, "previewBuff"    # [I
    .param p2, "previewWidth"    # I
    .param p3, "previewHeight"    # I
    .param p4, "angle"    # I
    .param p5, "viewBitmap"    # Landroid/graphics/Bitmap;
    .param p6, "isMove"    # Z

    .prologue
    .line 534
    if-eqz p1, :cond_0

    if-eqz p5, :cond_0

    .line 536
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewWidth:I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewHeight:I

    mul-int/2addr v1, v2

    new-array v4, v1, [I

    .line 537
    .local v4, "tempPreview":[I
    if-eqz p6, :cond_1

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    .line 539
    invoke-static/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runStraightenAngleOnMove([III[IIII)V

    .line 549
    :goto_0
    new-instance v3, Landroid/graphics/Canvas;

    move-object/from16 v0, p5

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 551
    .local v3, "canvas":Landroid/graphics/Canvas;
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    .line 552
    .local v13, "clearPaint":Landroid/graphics/Paint;
    new-instance v14, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v14, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    .line 553
    .local v14, "xmode":Landroid/graphics/Xfermode;
    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 555
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v3, v0, v1, v2, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 557
    const/4 v5, 0x0

    .line 559
    const/4 v7, 0x0

    .line 560
    const/4 v8, 0x0

    .line 563
    const/4 v11, 0x1

    .line 564
    const/4 v12, 0x0

    move/from16 v6, p2

    move/from16 v9, p2

    move/from16 v10, p3

    .line 556
    invoke-virtual/range {v3 .. v12}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 565
    const/4 v4, 0x0

    .line 567
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    invoke-interface {v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;->invalidate()V

    .line 570
    .end local v3    # "canvas":Landroid/graphics/Canvas;
    .end local v4    # "tempPreview":[I
    .end local v13    # "clearPaint":Landroid/graphics/Paint;
    .end local v14    # "xmode":Landroid/graphics/Xfermode;
    :cond_0
    return-void

    .restart local v4    # "tempPreview":[I
    :cond_1
    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    .line 545
    invoke-static/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runStraightenAngleOnUp([III[IIII)V

    goto :goto_0
.end method

.method private configurationRotate(Landroid/graphics/Bitmap;)V
    .locals 13
    .param p1, "viewBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v12, 0x3

    const/4 v6, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/high16 v10, -0x40800000    # -1.0f

    const/4 v9, 0x1

    .line 426
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_5

    .line 428
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    if-eqz v0, :cond_0

    .line 430
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialPrevImageBuf:[I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewWidth:I

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewHeight:I

    .line 431
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    move-object v0, p0

    move-object v5, p1

    .line 430
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->applyToCanvasStraighten([IIIILandroid/graphics/Bitmap;Z)V

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 436
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    if-ne v0, v9, :cond_1

    .line 437
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v11, v10, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 439
    :cond_1
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    if-ne v0, v9, :cond_2

    .line 440
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v10, v11, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 443
    :cond_2
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    if-eq v0, v9, :cond_3

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    if-ne v0, v12, :cond_7

    .line 445
    :cond_3
    const/16 v0, 0x5a

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->getScaleForFittedImage(I)F

    move-result v8

    .line 446
    .local v8, "scale":F
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "configurationRotate scale:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 447
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v8, v8, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 448
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    if-ne v0, v9, :cond_6

    .line 449
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    const/high16 v1, 0x42b40000    # 90.0f

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 462
    :cond_4
    :goto_0
    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    .line 463
    .local v7, "dst":Landroid/graphics/RectF;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v7, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 464
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v7}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 465
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "configurationRotate dst:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 466
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    iget v1, v7, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget v2, v7, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget v3, v7, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    iget v4, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 477
    .end local v7    # "dst":Landroid/graphics/RectF;
    .end local v8    # "scale":F
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->makeStraightenLines()V

    .line 478
    return-void

    .line 450
    .restart local v8    # "scale":F
    :cond_6
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    if-ne v0, v12, :cond_4

    .line 451
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    const/high16 v1, 0x43870000    # 270.0f

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    goto :goto_0

    .line 456
    .end local v8    # "scale":F
    :cond_7
    invoke-virtual {p0, v6}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->getScaleForFittedImage(I)F

    move-result v8

    .line 457
    .restart local v8    # "scale":F
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v8, v8, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 458
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 459
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    const/high16 v1, 0x43340000    # 180.0f

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    goto/16 :goto_0
.end method

.method private initGesture()V
    .locals 3

    .prologue
    .line 645
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$2;-><init>(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)V

    .line 701
    .local v0, "gesture":Landroid/view/GestureDetector$OnGestureListener;
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mGestureDetector:Landroid/view/GestureDetector;

    .line 702
    return-void
.end method

.method private setBitmapBG(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 840
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 841
    .local v2, "res":Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0203c8

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 842
    .local v1, "gauge_bg":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02036e

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 843
    .local v0, "degree":Landroid/graphics/Bitmap;
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 844
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_degree:Landroid/graphics/Bitmap;

    .line 846
    return-void
.end method


# virtual methods
.method public applyOriginal()[I
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 380
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v0

    .line 381
    .local v0, "input":[I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    .line 382
    .local v1, "OriginalWidth":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v2

    .line 383
    .local v2, "OriginalHeight":I
    array-length v5, v0

    new-array v3, v5, [I

    .line 384
    .local v3, "output":[I
    array-length v5, v0

    invoke-static {v0, v6, v3, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 385
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    if-eqz v5, :cond_0

    .line 388
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    move v4, v1

    move v5, v2

    .line 387
    invoke-static/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runStraightenAngleOnUp([III[IIII)V

    .line 390
    :cond_0
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    move v4, v1

    move v5, v2

    invoke-static/range {v3 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runRotateFlip([IIIIII)V

    .line 391
    const/4 v7, 0x0

    .line 392
    .local v7, "rotatedWidth":I
    const/4 v8, 0x0

    .line 393
    .local v8, "rotatedHeight":I
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    const/4 v6, 0x1

    if-eq v5, v6, :cond_1

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_2

    .line 395
    :cond_1
    move v7, v2

    .line 396
    move v8, v1

    .line 403
    :goto_0
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    move-object v5, p0

    move-object v6, v3

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;[IIILandroid/graphics/Rect;)V

    .line 405
    .local v4, "ret":Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->getRotatedBuff()[I

    move-result-object v6

    .line 406
    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->getRotatedWidth()I

    move-result v9

    .line 407
    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->getRotatedHeight()I

    move-result v10

    .line 405
    invoke-virtual {v5, v6, v9, v10}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([III)V

    .line 408
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v5

    return-object v5

    .line 400
    .end local v4    # "ret":Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;
    :cond_2
    move v7, v1

    .line 401
    move v8, v2

    goto :goto_0
.end method

.method public applyToPreview()Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 353
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 354
    .local v0, "input":[I
    array-length v2, v0

    new-array v3, v2, [I

    .line 355
    .local v3, "output":[I
    array-length v2, v0

    invoke-static {v0, v6, v3, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 356
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    if-eqz v2, :cond_0

    .line 358
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewWidth:I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewHeight:I

    .line 359
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewWidth:I

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewHeight:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    .line 358
    invoke-static/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runStraightenAngleOnUp([III[IIII)V

    .line 361
    :cond_0
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewWidth:I

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewHeight:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    invoke-static/range {v3 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runRotateFlip([IIIIII)V

    .line 362
    const/4 v4, 0x0

    .line 363
    .local v4, "rotatedWidth":I
    const/4 v5, 0x0

    .line 364
    .local v5, "rotatedHeight":I
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    const/4 v6, 0x1

    if-eq v2, v6, :cond_1

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    const/4 v6, 0x3

    if-ne v2, v6, :cond_2

    .line 366
    :cond_1
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewHeight:I

    .line 367
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewWidth:I

    .line 374
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;->invalidate()V

    .line 375
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;[IIILandroid/graphics/Rect;)V

    .line 376
    .local v1, "ret":Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;
    return-object v1

    .line 371
    .end local v1    # "ret":Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;
    :cond_2
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewWidth:I

    .line 372
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewHeight:I

    goto :goto_0
.end method

.method public changeDoneCancelStatus(I)V
    .locals 3
    .param p1, "input"    # I

    .prologue
    const/4 v2, 0x1

    .line 572
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenEnable:Z

    if-nez v0, :cond_0

    const v0, 0x11101106

    if-eq p1, v0, :cond_0

    .line 574
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    if-eqz v0, :cond_1

    .line 576
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;->ableDone()V

    .line 594
    :cond_0
    :goto_0
    return-void

    .line 580
    :cond_1
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    add-int/2addr v0, v1

    if-nez v0, :cond_2

    .line 582
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;->unableDone()V

    goto :goto_0

    .line 584
    :cond_2
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    if-ne v0, v2, :cond_3

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    if-ne v0, v2, :cond_3

    .line 586
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;->unableDone()V

    goto :goto_0

    .line 590
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;->ableDone()V

    goto :goto_0
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)V
    .locals 1
    .param p1, "rotateEffect"    # Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    .prologue
    .line 83
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 84
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    .line 85
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    .line 86
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    .line 87
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    .line 88
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    .line 89
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 92
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 95
    :cond_0
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    .line 96
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTouchMatrixForFlip:Landroid/graphics/Matrix;

    .line 98
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_degree:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_degree:Landroid/graphics/Bitmap;

    .line 99
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 100
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTextBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTextBitmap:Landroid/graphics/Bitmap;

    .line 101
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    .line 103
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialPrevImageBuf:[I

    if-eqz v0, :cond_1

    .line 104
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialPrevImageBuf:[I

    .line 105
    :cond_1
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 106
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "viewBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .param p4, "linePaint"    # Landroid/graphics/Paint;

    .prologue
    .line 115
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 135
    :cond_0
    return-void

    .line 117
    :cond_1
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 118
    .local v2, "m":Landroid/graphics/Matrix;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 119
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v4}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 121
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 122
    invoke-virtual {p1, p2, v2, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 123
    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenStart:Z

    if-eqz v4, :cond_0

    .line 125
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    .line 126
    .local v3, "viewTranformMatrix":Landroid/graphics/Matrix;
    const/4 v4, 0x4

    new-array v0, v4, [F

    .line 127
    .local v0, "dst":[F
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 128
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 130
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [F

    invoke-virtual {v3, v0, v4}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 131
    invoke-virtual {p1, v0, p4}, Landroid/graphics/Canvas;->drawLines([FLandroid/graphics/Paint;)V

    .line 128
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public drawProgress(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 711
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlphaAnim:I

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->ANIM_HIDE:I

    if-ne v8, v9, :cond_7

    .line 713
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    if-lez v8, :cond_6

    .line 715
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    add-int/lit8 v8, v8, -0xf

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    .line 717
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mHandler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mDrawAnimRunnable:Ljava/lang/Runnable;

    invoke-virtual {v8, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 746
    :goto_0
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    const/16 v9, 0xff

    if-le v8, v9, :cond_0

    .line 748
    const/16 v8, 0xff

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    .line 750
    :cond_0
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    if-gez v8, :cond_1

    .line 751
    const/4 v8, 0x0

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    .line 754
    :cond_1
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f05025a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 755
    .local v5, "textTopMagine":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050259

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 756
    .local v2, "progressbarRightMagine":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050256

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 757
    .local v4, "progressbarWidth":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f05025b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 758
    .local v3, "progressbarTextSize":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050255

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 760
    .local v6, "topMargine":I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v8

    sub-int/2addr v8, v4

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mProgressLeftMagine:F

    .line 761
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreX:F

    const/high16 v9, -0x40800000    # -1.0f

    cmpl-float v8, v8, v9

    if-eqz v8, :cond_5

    .line 763
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 764
    .local v1, "paint":Landroid/graphics/Paint;
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 765
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 766
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 768
    new-instance v7, Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 770
    .local v7, "tv":Landroid/widget/TextView;
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPrevStraightenAngle:I

    const/16 v9, 0x19

    if-lt v8, v9, :cond_2

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPrevStraightenAngle:I

    const/16 v9, 0x14f

    if-le v8, v9, :cond_3

    .line 771
    :cond_2
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPrevStraightenAngle:I

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 772
    :cond_3
    const/4 v8, -0x1

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 773
    const/4 v8, 0x0

    int-to-float v9, v3

    invoke-virtual {v7, v8, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 774
    const-string v8, "sec-roboto-light"

    const/4 v9, 0x0

    invoke-static {v8, v9}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 775
    .local v0, "font":Landroid/graphics/Typeface;
    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 776
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v11, -0x41000000    # -0.5f

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 777
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    .line 779
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v8

    const/high16 v9, -0x80000000

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 780
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v9

    const/high16 v10, -0x80000000

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 779
    invoke-virtual {v7, v8, v9}, Landroid/widget/TextView;->measure(II)V

    .line 782
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v10

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v11

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->layout(IIII)V

    .line 783
    const/16 v8, 0x11

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 784
    invoke-virtual {v7}, Landroid/widget/TextView;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    .line 786
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->setBitmapBG(Landroid/graphics/Canvas;)V

    .line 787
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_degree:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_4

    .line 788
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_degree:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-nez v8, :cond_4

    .line 789
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_degree:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v9

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mProgressLeftMagine:F

    sub-float/2addr v9, v10

    int-to-float v10, v2

    sub-float/2addr v9, v10

    int-to-float v10, v5

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v11

    iget v11, v11, Landroid/util/DisplayMetrics;->density:F

    add-float/2addr v10, v11

    invoke-virtual {p1, v8, v9, v10, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 790
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mProgressLeftMagine:F

    int-to-float v10, v6

    invoke-virtual {p1, v8, v9, v10, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 791
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v9

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mProgressLeftMagine:F

    sub-float/2addr v9, v10

    int-to-float v10, v2

    sub-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    int-to-float v10, v10

    sub-float/2addr v9, v10

    int-to-float v10, v5

    invoke-virtual {p1, v8, v9, v10, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 794
    :cond_4
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    invoke-virtual {p0, p1, v8}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->showResizeText(Landroid/graphics/Canvas;I)V

    .line 796
    .end local v0    # "font":Landroid/graphics/Typeface;
    .end local v1    # "paint":Landroid/graphics/Paint;
    .end local v7    # "tv":Landroid/widget/TextView;
    :cond_5
    return-void

    .line 722
    .end local v2    # "progressbarRightMagine":I
    .end local v3    # "progressbarTextSize":I
    .end local v4    # "progressbarWidth":I
    .end local v5    # "textTopMagine":I
    .end local v6    # "topMargine":I
    :cond_6
    const/4 v8, 0x0

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    .line 723
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->ANIM_NONE:I

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlphaAnim:I

    .line 724
    const/high16 v8, 0x3f800000    # 1.0f

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreX:F

    .line 725
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    invoke-interface {v8}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;->invalidate()V

    goto/16 :goto_0

    .line 728
    :cond_7
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlphaAnim:I

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->ANIM_SHOW:I

    if-ne v8, v9, :cond_9

    .line 731
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    const/16 v9, 0xff

    if-ge v8, v9, :cond_8

    .line 733
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    add-int/lit8 v8, v8, 0xf

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    goto/16 :goto_0

    .line 737
    :cond_8
    const/16 v8, 0xff

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    goto/16 :goto_0

    .line 743
    :cond_9
    const/4 v8, 0x0

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    goto/16 :goto_0
.end method

.method public flipHorizontal()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    const/4 v3, 0x1

    .line 195
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 196
    :cond_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    if-ne v0, v3, :cond_1

    .line 197
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    .line 207
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v4, v5, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 208
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTouchMatrixForFlip:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v4, v5, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 209
    const v0, 0x11101103

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->changeDoneCancelStatus(I)V

    .line 210
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenEnable:Z

    .line 211
    return-void

    .line 199
    :cond_1
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    goto :goto_0

    .line 202
    :cond_2
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    if-ne v0, v3, :cond_3

    .line 203
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    goto :goto_0

    .line 205
    :cond_3
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    goto :goto_0
.end method

.method public flipVertical()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    const/4 v3, 0x1

    .line 214
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 215
    :cond_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    if-ne v0, v3, :cond_1

    .line 216
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    .line 226
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v5, v4, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 227
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTouchMatrixForFlip:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v5, v4, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 228
    const v0, 0x11101104

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->changeDoneCancelStatus(I)V

    .line 229
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenEnable:Z

    .line 230
    return-void

    .line 218
    :cond_1
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipV:I

    goto :goto_0

    .line 221
    :cond_2
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    if-ne v0, v3, :cond_3

    .line 222
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    goto :goto_0

    .line 224
    :cond_3
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mFlipH:I

    goto :goto_0
.end method

.method public getAngle()I
    .locals 1

    .prologue
    .line 481
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    return v0
.end method

.method public getCanvasRoi()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 530
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method public getDrawRoi()Landroid/graphics/RectF;
    .locals 4

    .prologue
    .line 521
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 525
    .local v0, "ret":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    invoke-direct {v2, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 526
    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 79
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScaleForFittedImage(I)F
    .locals 8
    .param p1, "angle"    # I

    .prologue
    const/4 v7, 0x0

    .line 485
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 486
    .local v5, "viewRoi":Landroid/graphics/Rect;
    iput v7, v5, Landroid/graphics/Rect;->left:I

    .line 487
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v6

    iput v6, v5, Landroid/graphics/Rect;->right:I

    .line 488
    iput v7, v5, Landroid/graphics/Rect;->top:I

    .line 489
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v6

    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    .line 490
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getScaleForFittedImage angle:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 491
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getScaleForFittedImage viewRoi:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 492
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 493
    .local v2, "rotateM":Landroid/graphics/Matrix;
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 494
    .local v0, "dst":Landroid/graphics/RectF;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v0, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 495
    int-to-float v6, p1

    invoke-virtual {v2, v6}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 496
    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 497
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getScaleForFittedImage mCanvasRoi:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 498
    const/high16 v3, 0x3f800000    # 1.0f

    .line 499
    .local v3, "scale":F
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    div-float v4, v6, v7

    .line 500
    .local v4, "viewRatio":F
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v7

    div-float v1, v6, v7

    .line 501
    .local v1, "dstRatio":F
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getScaleForFittedImage dst:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/graphics/RectF;->toShortString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 502
    cmpl-float v6, v4, v1

    if-lez v6, :cond_0

    .line 504
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v7

    div-float v3, v6, v7

    .line 505
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getScaleForFittedImage if(viewRatio > dstRatio):"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/graphics/RectF;->toShortString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 506
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getScaleForFittedImage viewRoi.height(),dst.height():"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 507
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getScaleForFittedImage scale:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 517
    :goto_0
    return v3

    .line 511
    :cond_0
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v7

    div-float v3, v6, v7

    .line 512
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getScaleForFittedImage if(viewRatio > dstRatio) false:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/graphics/RectF;->toShortString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 513
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getScaleForFittedImage viewRoi.width(),dst.width():"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 514
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getScaleForFittedImage scale:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 74
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(I)V
    .locals 0
    .param p1, "effectType"    # I

    .prologue
    .line 111
    return-void
.end method

.method public makeStraightenLines()V
    .locals 14

    .prologue
    .line 597
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iput-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    .line 599
    const/4 v5, 0x0

    .local v5, "left":I
    const/4 v6, 0x0

    .local v6, "top":I
    const/4 v4, 0x0

    .local v4, "canvasWidth":I
    const/4 v3, 0x0

    .line 601
    .local v3, "canvasHeight":I
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    iget v5, v10, Landroid/graphics/Rect;->left:I

    .line 602
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    iget v6, v10, Landroid/graphics/Rect;->top:I

    .line 603
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v4

    .line 604
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v3

    .line 606
    const/4 v10, 0x4

    new-array v7, v10, [F

    const/4 v10, 0x0

    int-to-float v11, v5

    int-to-float v12, v4

    const/high16 v13, 0x3e800000    # 0.25f

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    aput v11, v7, v10

    const/4 v10, 0x1

    .line 607
    int-to-float v11, v6

    aput v11, v7, v10

    const/4 v10, 0x2

    .line 608
    int-to-float v11, v5

    int-to-float v12, v4

    const/high16 v13, 0x3e800000    # 0.25f

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    aput v11, v7, v10

    const/4 v10, 0x3

    .line 609
    add-int v11, v6, v3

    int-to-float v11, v11

    aput v11, v7, v10

    .line 610
    .local v7, "veticalLine1":[F
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 611
    const/4 v7, 0x0

    .line 612
    const/4 v10, 0x4

    new-array v8, v10, [F

    const/4 v10, 0x0

    int-to-float v11, v5

    int-to-float v12, v4

    const/high16 v13, 0x3f000000    # 0.5f

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    aput v11, v8, v10

    const/4 v10, 0x1

    .line 613
    int-to-float v11, v6

    aput v11, v8, v10

    const/4 v10, 0x2

    .line 614
    int-to-float v11, v5

    int-to-float v12, v4

    const/high16 v13, 0x3f000000    # 0.5f

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    aput v11, v8, v10

    const/4 v10, 0x3

    .line 615
    add-int v11, v6, v3

    int-to-float v11, v11

    aput v11, v8, v10

    .line 616
    .local v8, "veticalLine2":[F
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 617
    const/4 v8, 0x0

    .line 618
    const/4 v10, 0x4

    new-array v9, v10, [F

    const/4 v10, 0x0

    int-to-float v11, v5

    int-to-float v12, v4

    const/high16 v13, 0x3f400000    # 0.75f

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    aput v11, v9, v10

    const/4 v10, 0x1

    .line 619
    int-to-float v11, v6

    aput v11, v9, v10

    const/4 v10, 0x2

    .line 620
    int-to-float v11, v5

    int-to-float v12, v4

    const/high16 v13, 0x3f400000    # 0.75f

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    aput v11, v9, v10

    const/4 v10, 0x3

    .line 621
    add-int v11, v6, v3

    int-to-float v11, v11

    aput v11, v9, v10

    .line 622
    .local v9, "veticalLine3":[F
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 623
    const/4 v9, 0x0

    .line 624
    const/4 v10, 0x4

    new-array v0, v10, [F

    const/4 v10, 0x0

    int-to-float v11, v5

    aput v11, v0, v10

    const/4 v10, 0x1

    .line 625
    int-to-float v11, v6

    int-to-float v12, v3

    const/high16 v13, 0x3e800000    # 0.25f

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    aput v11, v0, v10

    const/4 v10, 0x2

    .line 626
    add-int v11, v5, v4

    int-to-float v11, v11

    aput v11, v0, v10

    const/4 v10, 0x3

    .line 627
    int-to-float v11, v6

    int-to-float v12, v3

    const/high16 v13, 0x3e800000    # 0.25f

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    aput v11, v0, v10

    .line 628
    .local v0, "HorizontalLine1":[F
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 629
    const/4 v0, 0x0

    .line 630
    const/4 v10, 0x4

    new-array v1, v10, [F

    const/4 v10, 0x0

    int-to-float v11, v5

    aput v11, v1, v10

    const/4 v10, 0x1

    .line 631
    int-to-float v11, v6

    int-to-float v12, v3

    const/high16 v13, 0x3f000000    # 0.5f

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    aput v11, v1, v10

    const/4 v10, 0x2

    .line 632
    add-int v11, v5, v4

    int-to-float v11, v11

    aput v11, v1, v10

    const/4 v10, 0x3

    .line 633
    int-to-float v11, v6

    int-to-float v12, v3

    const/high16 v13, 0x3f000000    # 0.5f

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    aput v11, v1, v10

    .line 634
    .local v1, "HorizontalLine2":[F
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 635
    const/4 v1, 0x0

    .line 636
    const/4 v10, 0x4

    new-array v2, v10, [F

    const/4 v10, 0x0

    int-to-float v11, v5

    aput v11, v2, v10

    const/4 v10, 0x1

    .line 637
    int-to-float v11, v6

    int-to-float v12, v3

    const/high16 v13, 0x3f400000    # 0.75f

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    aput v11, v2, v10

    const/4 v10, 0x2

    .line 638
    add-int v11, v5, v4

    int-to-float v11, v11

    aput v11, v2, v10

    const/4 v10, 0x3

    .line 639
    int-to-float v11, v6

    int-to-float v12, v3

    const/high16 v13, 0x3f400000    # 0.75f

    mul-float/2addr v12, v13

    add-float/2addr v11, v12

    aput v11, v2, v10

    .line 640
    .local v2, "HorizontalLine3":[F
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641
    const/4 v2, 0x0

    .line 642
    return-void
.end method

.method public onConfigurationChanged(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "viewBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 412
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->configurationRotate(Landroid/graphics/Bitmap;)V

    .line 413
    return-void
.end method

.method public onLayoutRotateEffect(IIIIII)V
    .locals 5
    .param p1, "viewWidth"    # I
    .param p2, "viewHeight"    # I
    .param p3, "orgWidth"    # I
    .param p4, "orgHeight"    # I
    .param p5, "previewWidth"    # I
    .param p6, "previewHeight"    # I

    .prologue
    const/4 v4, 0x0

    .line 138
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mViewWidth:I

    .line 139
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mViewHeight:I

    .line 140
    iput p5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewWidth:I

    .line 141
    iput p6, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewHeight:I

    .line 143
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    .line 144
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->makeStraightenLines()V

    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialPrevImageBuf:[I

    if-eqz v0, :cond_0

    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialPrevImageBuf:[I

    .line 148
    :cond_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewWidth:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewHeight:I

    mul-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialPrevImageBuf:[I

    .line 149
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialPrevImageBuf:[I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewWidth:I

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewHeight:I

    mul-int/2addr v2, v3

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 150
    return-void
.end method

.method public removeStraightenLines()V
    .locals 2

    .prologue
    .line 416
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 418
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 423
    .end local v0    # "i":I
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    .line 424
    return-void

    .line 420
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenLines:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method public setCallback(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    .line 70
    return-void
.end method

.method public setStraightenEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 191
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenEnable:Z

    .line 192
    return-void
.end method

.method public showResizeText(Landroid/graphics/Canvas;I)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "alpha"    # I

    .prologue
    .line 799
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f05025b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 800
    .local v3, "progressbarTextSize":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f05025a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 801
    .local v6, "textTopMagine":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050256

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 802
    .local v4, "progressbarWidth":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050259

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 804
    .local v2, "progressbarLeftMagine":I
    const/4 v5, 0x0

    .line 805
    .local v5, "text":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v8

    sub-int/2addr v8, v4

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mProgressLeftMagine:F

    .line 807
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 808
    .local v1, "paint":Landroid/graphics/Paint;
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 809
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 810
    invoke-virtual {v1, p2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 812
    new-instance v7, Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 813
    .local v7, "tv":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mContext:Landroid/content/Context;

    const v9, 0x7f060033

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 814
    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 815
    const/4 v8, -0x1

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 816
    const/4 v8, 0x0

    int-to-float v9, v3

    invoke-virtual {v7, v8, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 817
    const-string v8, "sec-roboto-light"

    const/4 v9, 0x0

    invoke-static {v8, v9}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 818
    .local v0, "font":Landroid/graphics/Typeface;
    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 819
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v11, -0x41000000    # -0.5f

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 821
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    .line 823
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v8

    const/high16 v9, -0x80000000

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 824
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v9

    const/high16 v10, -0x80000000

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 823
    invoke-virtual {v7, v8, v9}, Landroid/widget/TextView;->measure(II)V

    .line 826
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v10

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v11

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->layout(IIII)V

    .line 827
    const/16 v8, 0x11

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 828
    invoke-virtual {v7}, Landroid/widget/TextView;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTextBitmap:Landroid/graphics/Bitmap;

    .line 830
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTextBitmap:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_0

    .line 831
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTextBitmap:Landroid/graphics/Bitmap;

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mProgressLeftMagine:F

    int-to-float v10, v2

    add-float/2addr v9, v10

    .line 832
    int-to-float v10, v6

    .line 831
    invoke-virtual {p1, v8, v9, v10, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 834
    :cond_0
    return-void
.end method

.method public touch(Landroid/view/MotionEvent;Landroid/graphics/Bitmap;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "viewBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenEnable:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTouchMatrixForFlip:Landroid/graphics/Matrix;

    if-eqz v0, :cond_10

    .line 236
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mTouchMatrixForFlip:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    const/4 v0, 0x0

    .line 348
    :goto_0
    return v0

    .line 241
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mConfigChanged:Z

    if-eqz v0, :cond_3

    .line 243
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v10, v0

    .line 244
    .local v10, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v11, v0

    .line 245
    .local v11, "y":I
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    new-instance v1, Landroid/graphics/PointF;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mViewWidth:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mViewHeight:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 246
    new-instance v2, Landroid/graphics/PointF;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStartX:F

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStartY:F

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 247
    new-instance v3, Landroid/graphics/PointF;

    int-to-float v4, v10

    int-to-float v5, v11

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 245
    invoke-static {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getAngle(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    .line 249
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    const/16 v1, 0x168

    if-le v0, v1, :cond_1

    .line 250
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    add-int/lit16 v0, v0, -0x168

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    .line 252
    :cond_1
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    const/16 v1, 0x19

    if-lt v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    const/16 v1, 0x14f

    if-le v0, v1, :cond_4

    .line 253
    :cond_2
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    .line 257
    :goto_1
    const v0, 0x11101106

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->changeDoneCancelStatus(I)V

    .line 258
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mConfigChanged:Z

    .line 260
    .end local v10    # "x":I
    .end local v11    # "y":I
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v10, v0

    .line 261
    .restart local v10    # "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v11, v0

    .line 263
    .restart local v11    # "y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 343
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 255
    :cond_4
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPrevStraightenAngle:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    goto :goto_1

    .line 265
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;->actionBarIsEnableDone()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCurrentSaveBtnState:Z

    .line 266
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStartX:F

    .line 267
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStartY:F

    .line 268
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;->unableDone()V

    .line 269
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenStart:Z

    .line 270
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;->setEnabledWithChildren(Z)V

    .line 271
    int-to-float v0, v10

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreX:F

    goto :goto_2

    .line 275
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenStart:Z

    if-nez v0, :cond_5

    .line 276
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenStart:Z

    .line 278
    :cond_5
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    new-instance v1, Landroid/graphics/PointF;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mViewWidth:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mViewHeight:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 279
    new-instance v2, Landroid/graphics/PointF;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStartX:F

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStartY:F

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 280
    new-instance v3, Landroid/graphics/PointF;

    int-to-float v4, v10

    int-to-float v5, v11

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 278
    invoke-static {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getAngle(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    .line 281
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    const/16 v1, 0x168

    if-lt v0, v1, :cond_6

    .line 282
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    add-int/lit16 v0, v0, -0x168

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    .line 283
    :cond_6
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    const/16 v1, 0x19

    if-lt v0, v1, :cond_7

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    const/16 v1, 0x14f

    if-le v0, v1, :cond_8

    .line 285
    :cond_7
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialPrevImageBuf:[I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewWidth:I

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewHeight:I

    .line 286
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    const/4 v6, 0x1

    move-object v0, p0

    move-object v5, p2

    .line 285
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->applyToCanvasStraighten([IIIILandroid/graphics/Bitmap;Z)V

    .line 288
    const v0, 0x11101105

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->changeDoneCancelStatus(I)V

    .line 289
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPrevStraightenAngle:I

    .line 292
    :cond_8
    int-to-float v0, v10

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreX:F

    goto/16 :goto_2

    .line 297
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;->setEnabledWithChildren(Z)V

    .line 299
    new-instance v0, Landroid/graphics/PointF;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mViewWidth:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mViewHeight:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 300
    new-instance v1, Landroid/graphics/PointF;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStartX:F

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStartY:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 301
    new-instance v2, Landroid/graphics/PointF;

    int-to-float v3, v10

    int-to-float v4, v11

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 299
    invoke-static {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getAngle(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)I

    move-result v7

    .line 302
    .local v7, "rotateAngle":I
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    add-int/2addr v0, v7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    .line 304
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    const/16 v1, 0x168

    if-lt v0, v1, :cond_9

    .line 305
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    add-int/lit16 v0, v0, -0x168

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    .line 307
    :cond_9
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    const/16 v1, 0x19

    if-lt v0, v1, :cond_a

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    const/16 v1, 0x14f

    if-le v0, v1, :cond_f

    .line 309
    :cond_a
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngleOnMove:I

    .line 314
    .local v8, "tempAngle":I
    :goto_3
    if-nez v8, :cond_b

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCurrentSaveBtnState:Z

    if-eqz v0, :cond_c

    .line 316
    :cond_b
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;->ableDone()V

    .line 319
    :cond_c
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    sub-int v9, v8, v0

    .line 320
    .local v9, "undoRedoAngle":I
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    add-int/2addr v0, v9

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    .line 321
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    if-eqz v0, :cond_d

    .line 322
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialPrevImageBuf:[I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewWidth:I

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPreviewHeight:I

    .line 323
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenAngle:I

    const/4 v6, 0x0

    move-object v0, p0

    move-object v5, p2

    .line 322
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->applyToCanvasStraighten([IIIILandroid/graphics/Bitmap;Z)V

    .line 325
    :cond_d
    const v0, 0x11101106

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->changeDoneCancelStatus(I)V

    .line 327
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenStart:Z

    .line 328
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;->invalidate()V

    .line 331
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->ANIM_HIDE:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlphaAnim:I

    .line 332
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    const/16 v1, 0xff

    if-le v0, v1, :cond_e

    .line 334
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlpha:I

    .line 336
    :cond_e
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mDrawAnimRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_2

    .line 312
    .end local v8    # "tempAngle":I
    .end local v9    # "undoRedoAngle":I
    :cond_f
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mPrevStraightenAngle:I

    .restart local v8    # "tempAngle":I
    goto :goto_3

    .line 348
    .end local v7    # "rotateAngle":I
    .end local v8    # "tempAngle":I
    .end local v10    # "x":I
    .end local v11    # "y":I
    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 263
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public turnLeft(F)V
    .locals 7
    .param p1, "toScale"    # F

    .prologue
    const/4 v3, 0x0

    .line 153
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    .line 154
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    if-gez v2, :cond_0

    .line 155
    const/4 v2, 0x3

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    .line 156
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v2, p1, p1, v4, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 157
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    const/high16 v4, -0x3d4c0000    # -90.0f

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v2, v4, v5, v6}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 159
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 160
    .local v0, "dst":Landroid/graphics/RectF;
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 161
    .local v1, "src":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 162
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 163
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    move v2, v3

    :goto_0
    float-to-int v2, v2

    iput v2, v4, Landroid/graphics/Rect;->left:I

    .line 164
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v4, v4, v3

    if-gez v4, :cond_2

    :goto_1
    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 165
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v2

    int-to-float v2, v2

    :goto_2
    float-to-int v2, v2

    iput v2, v3, Landroid/graphics/Rect;->right:I

    .line 166
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    int-to-float v2, v2

    :goto_3
    float-to-int v2, v2

    iput v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 167
    const v2, 0x11101101

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->changeDoneCancelStatus(I)V

    .line 168
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenEnable:Z

    .line 169
    return-void

    .line 163
    :cond_1
    iget v2, v0, Landroid/graphics/RectF;->left:F

    goto :goto_0

    .line 164
    :cond_2
    iget v3, v0, Landroid/graphics/RectF;->top:F

    goto :goto_1

    .line 165
    :cond_3
    iget v2, v0, Landroid/graphics/RectF;->right:F

    goto :goto_2

    .line 166
    :cond_4
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    goto :goto_3
.end method

.method public turnRight(F)V
    .locals 7
    .param p1, "toScale"    # F

    .prologue
    const/4 v3, 0x0

    .line 172
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    .line 173
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    const/4 v4, 0x3

    if-le v2, v4, :cond_0

    .line 174
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAngle:I

    .line 175
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v2, p1, p1, v4, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 176
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    const/high16 v4, 0x42b40000    # 90.0f

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v2, v4, v5, v6}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 178
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 179
    .local v0, "dst":Landroid/graphics/RectF;
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 180
    .local v1, "src":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mInitialCanvasRoi:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 181
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 182
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    move v2, v3

    :goto_0
    float-to-int v2, v2

    iput v2, v4, Landroid/graphics/Rect;->left:I

    .line 183
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v4, v4, v3

    if-gez v4, :cond_2

    :goto_1
    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 184
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v2

    int-to-float v2, v2

    :goto_2
    float-to-int v2, v2

    iput v2, v3, Landroid/graphics/Rect;->right:I

    .line 185
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mCanvasRoi:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    int-to-float v2, v2

    :goto_3
    float-to-int v2, v2

    iput v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 186
    const v2, 0x11101102

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->changeDoneCancelStatus(I)V

    .line 187
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mStraightenEnable:Z

    .line 188
    return-void

    .line 182
    :cond_1
    iget v2, v0, Landroid/graphics/RectF;->left:F

    goto :goto_0

    .line 183
    :cond_2
    iget v3, v0, Landroid/graphics/RectF;->top:F

    goto :goto_1

    .line 184
    :cond_3
    iget v2, v0, Landroid/graphics/RectF;->right:F

    goto :goto_2

    .line 185
    :cond_4
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    goto :goto_3
.end method
