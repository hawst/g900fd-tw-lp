.class public Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "RotateView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;
    }
.end annotation


# instance fields
.field private final Key:Ljava/lang/String;

.field private REDOALL_DIALOG:I

.field private UNDOALL_DIALOG:I

.field private anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mContext:Landroid/content/Context;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mIsMinimum:Z

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

.field private mMirrorMode:Z

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mOnFilp3dAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;

.field private mPaint:Landroid/graphics/Paint;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p3, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p4, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p5, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/16 v5, 0x64

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 113
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 1454
    const-string v0, "RotateView"

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->Key:Ljava/lang/String;

    .line 1455
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1457
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1458
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    .line 1459
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 1460
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1461
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1462
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1463
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1464
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1465
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 1466
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mOnFilp3dAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;

    .line 1467
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mPaint:Landroid/graphics/Paint;

    .line 1468
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mLinePaint:Landroid/graphics/Paint;

    .line 1469
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    .line 1471
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1473
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    .line 1474
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    .line 1475
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1476
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorMode:Z

    .line 1477
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->UNDOALL_DIALOG:I

    .line 1478
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->REDOALL_DIALOG:I

    .line 1479
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mIsMinimum:Z

    .line 115
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    .line 116
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 117
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 118
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 119
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 120
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->setInterface(Ljava/lang/Object;)V

    .line 122
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 123
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 124
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mPaint:Landroid/graphics/Paint;

    .line 125
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mLinePaint:Landroid/graphics/Paint;

    .line 126
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mLinePaint:Landroid/graphics/Paint;

    const v1, -0x7f000001

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 128
    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->setViewLayerType(I)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 132
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    .line 133
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    if-lt v0, v5, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    if-ge v0, v5, :cond_1

    .line 134
    :cond_0
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mIsMinimum:Z

    .line 135
    :cond_1
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    const/16 v5, 0x64

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 1454
    const-string v0, "RotateView"

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->Key:Ljava/lang/String;

    .line 1455
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1457
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1458
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    .line 1459
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 1460
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1461
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1462
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1463
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1464
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1465
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 1466
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mOnFilp3dAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;

    .line 1467
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mPaint:Landroid/graphics/Paint;

    .line 1468
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mLinePaint:Landroid/graphics/Paint;

    .line 1469
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    .line 1471
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1473
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    .line 1474
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    .line 1475
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1476
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorMode:Z

    .line 1477
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->UNDOALL_DIALOG:I

    .line 1478
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->REDOALL_DIALOG:I

    .line 1479
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mIsMinimum:Z

    .line 84
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    .line 85
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 86
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 87
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 88
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 89
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->setInterface(Ljava/lang/Object;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 92
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 93
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mPaint:Landroid/graphics/Paint;

    .line 94
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mLinePaint:Landroid/graphics/Paint;

    .line 95
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mLinePaint:Landroid/graphics/Paint;

    const v1, -0x7f000001

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 97
    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->setViewLayerType(I)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 101
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    .line 102
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    if-lt v0, v5, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    if-ge v0, v5, :cond_1

    .line 103
    :cond_0
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mIsMinimum:Z

    .line 104
    :cond_1
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 1457
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1458
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
    .locals 1

    .prologue
    .line 1473
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->startImageEditViewAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewHeight()I

    move-result v0

    return v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)V
    .locals 0

    .prologue
    .line 1469
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;
    .locals 1

    .prologue
    .line 1469
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1475
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;
    .locals 1

    .prologue
    .line 1474
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;)V
    .locals 0

    .prologue
    .line 1474
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;I)V
    .locals 0

    .prologue
    .line 998
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->checkHelpPopup(I)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 1455
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Z)V
    .locals 0

    .prologue
    .line 1476
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorMode:Z

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;I)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->setViewLayerType(I)V

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;ILandroid/graphics/Bitmap;ILandroid/graphics/RectF;Landroid/graphics/RectF;Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual/range {p0 .. p6}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->startMirrorAnimation(ILandroid/graphics/Bitmap;ILandroid/graphics/RectF;Landroid/graphics/RectF;Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;)V

    return-void
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;
    .locals 1

    .prologue
    .line 1466
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mOnFilp3dAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;

    return-object v0
.end method

.method static synthetic access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V
    .locals 0

    .prologue
    .line 1465
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 1460
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 1461
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 1457
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 1471
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 1471
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 1462
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .locals 1

    .prologue
    .line 1464
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    return-object v0
.end method

.method private checkHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1000
    const/4 v0, 0x0

    .line 1001
    .local v0, "isNoShow":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->isDoNotShowPopup(Landroid/content/Context;I)Z

    move-result v0

    .line 1003
    if-nez v0, :cond_0

    .line 1005
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-nez v1, :cond_0

    .line 1007
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->initHelpPopup(I)V

    .line 1008
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->showCurrentModeHelpPopup()V

    .line 1011
    :cond_0
    return-void
.end method

.method private initHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1014
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 1015
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$10;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->setHelpPopupCallback(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;)V

    .line 1022
    return-void
.end method

.method private initSaveAsDialog()V
    .locals 14

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1298
    const/4 v10, 0x0

    .line 1299
    .local v10, "fileName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v13

    .line 1300
    .local v13, "path":Ljava/lang/String;
    if-eqz v13, :cond_0

    .line 1302
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1303
    if-nez v10, :cond_0

    .line 1305
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1309
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1310
    const v2, 0x7f090156

    .line 1311
    const v3, 0x7f0601cd

    .line 1314
    const v6, 0x103012e

    .line 1309
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1316
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1317
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v7, 0x600

    move v8, v4

    move v9, v4

    move-object v11, v5

    move-object v12, v5

    invoke-virtual/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIILjava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1324
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000a

    .line 1325
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    .line 1324
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnShowListener;)V

    .line 1385
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1386
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$13;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    .line 1385
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1400
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1402
    return-void
.end method

.method private registerReceiverToActivity()V
    .locals 3

    .prologue
    .line 1024
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 1073
    :cond_0
    :goto_0
    return-void

    .line 1029
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1030
    .local v0, "theFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TWIST_RIGHT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1031
    const-string v1, "android.intent.action.TWIST_LEFT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1039
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$11;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 1071
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    instance-of v1, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v1, :cond_0

    .line 1072
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private unregisterReceiverFromActivity()V
    .locals 2

    .prologue
    .line 1075
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v0, :cond_0

    .line 1077
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1078
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 1080
    :cond_0
    return-void
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 553
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorMode:Z

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-virtual {v0, p2}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->touch(Landroid/view/MotionEvent;)Z

    .line 560
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 557
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->transformVirtualPreviewCoordinate(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)V

    .line 558
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, p2, v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->touch(Landroid/view/MotionEvent;Landroid/graphics/Bitmap;)Z

    goto :goto_0
.end method

.method public backPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 309
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v0, :cond_2

    .line 311
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x11000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 322
    :cond_1
    :goto_0
    return-void

    .line 317
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v0, :cond_1

    .line 319
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const/high16 v1, 0x1e110000

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->changeViewStatus(II)V

    goto :goto_0
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 648
    return-void
.end method

.method protected doDone()V
    .locals 8

    .prologue
    const/high16 v7, 0x42c80000    # 100.0f

    .line 418
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->applyToPreview()Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;

    move-result-object v6

    .line 419
    .local v6, "info":Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->getRotatedBuff()[I

    move-result-object v1

    .line 420
    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->getRotatedWidth()I

    move-result v2

    .line 421
    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->getRotatedHeight()I

    move-result v3

    .line 419
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewBuffer([III)V

    .line 423
    const/4 v4, 0x0

    .line 424
    .local v4, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorMode:Z

    if-eqz v0, :cond_3

    .line 426
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->getApplyMirrorPreviewBuff()[I

    move-result-object v1

    .line 427
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->getApplyMirrorWidth()I

    move-result v2

    .line 428
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->getApplyMirrorHeight()I

    move-result v3

    .line 426
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewBufferForMirror([III)V

    .line 429
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    .end local v4    # "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-direct {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;)V

    .line 430
    .restart local v4    # "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->destroy()V

    .line 437
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 440
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 441
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 442
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getEnhance()Z

    move-result v5

    .line 439
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([IIILcom/sec/android/mimage/photoretouching/Core/EffectInfo;Z)V

    .line 445
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v0, :cond_5

    .line 447
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v7

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v7

    if-gez v0, :cond_4

    .line 448
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 452
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->loadTextures(Landroid/content/Context;)V

    .line 473
    :cond_2
    :goto_2
    return-void

    .line 434
    :cond_3
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    .end local v4    # "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    invoke-direct {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)V

    .restart local v4    # "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    goto :goto_0

    .line 450
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x11000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_1

    .line 455
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v0, :cond_2

    .line 458
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->applyOriginal()[I

    .line 460
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorMode:Z

    if-eqz v0, :cond_6

    .line 462
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->applyOriginal()[I

    .line 469
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->changeImageData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 470
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const/high16 v1, 0x1e110000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->changeViewStatus(II)V

    goto :goto_2
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 659
    const/4 v0, 0x0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 663
    const/4 v0, 0x0

    return v0
.end method

.method public getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 823
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    return-object v0
.end method

.method public getStatusHeight()I
    .locals 1

    .prologue
    .line 670
    const/4 v0, 0x0

    return v0
.end method

.method public initActionbar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 325
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 330
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    .line 327
    invoke-virtual {v0, v4, v2, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 350
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 353
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$6;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    .line 350
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 387
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 390
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$7;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    .line 387
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 411
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 413
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 415
    :cond_0
    return-void
.end method

.method public initButtons()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 249
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v2, :cond_0

    .line 252
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setClearSelectedButton()V

    .line 253
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 255
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 303
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 257
    .restart local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 258
    .local v1, "v":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x11101108

    if-ne v2, v3, :cond_3

    .line 260
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$3;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 255
    .end local v1    # "v":Landroid/view/View;
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 283
    .restart local v1    # "v":Landroid/view/View;
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x11001108

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEnableDecoration()Z

    move-result v2

    if-nez v2, :cond_4

    .line 284
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v5, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 285
    instance-of v2, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v2, :cond_2

    .line 286
    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .end local v1    # "v":Landroid/view/View;
    invoke-virtual {v1, v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setEnabledWidthChildren(Z)V

    .line 287
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$4;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 297
    .restart local v1    # "v":Landroid/view/View;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_1
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 479
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->initSaveAsDialog()V

    .line 481
    :cond_0
    return-void
.end method

.method public initEffect()V
    .locals 2

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->registerReceiverToActivity()V

    .line 142
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->setCallback(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;)V

    .line 202
    return-void
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 499
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 636
    return-void
.end method

.method public initTrayLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 640
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 642
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 644
    :cond_0
    return-void
.end method

.method public initView()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 209
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mOnFilp3dAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;

    .line 245
    :cond_0
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 582
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 583
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledSave()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 597
    :goto_0
    return-void

    .line 589
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_1

    .line 591
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 592
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 593
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 595
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 565
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->unregisterReceiverFromActivity()V

    .line 566
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->destroy()V

    .line 567
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mPaint:Landroid/graphics/Paint;

    .line 568
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeCancelPressState(Z)V

    .line 569
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    .line 570
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->destroy()V

    .line 574
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 575
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 578
    :cond_1
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 485
    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 486
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 488
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->draw(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    .line 489
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->drawProgress(Landroid/graphics/Canvas;)V

    .line 490
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorMode:Z

    if-eqz v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->draw(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    .line 493
    :cond_0
    return-void
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 600
    const/16 v0, 0x17

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-ne p1, v0, :cond_3

    .line 602
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 603
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    .line 606
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_2

    .line 607
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 629
    :cond_2
    :goto_0
    return v1

    .line 611
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 613
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->backPressed()V

    goto :goto_0
.end method

.method public onLayout()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 1406
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 1407
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1408
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 1409
    const-string v0, "rotateview onLayout"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1410
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    .line 1411
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    .line 1412
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    .line 1413
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v4

    .line 1414
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    .line 1415
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    .line 1410
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->onLayoutRotateEffect(IIIIII)V

    .line 1420
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1422
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1425
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1426
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v2, v10

    move v4, v10

    move v5, v10

    .line 1420
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1428
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->onConfigurationChanged(Landroid/graphics/Bitmap;)V

    .line 1429
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    if-eqz v0, :cond_0

    .line 1430
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->getCanvasRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->changeLayoutSize(Landroid/graphics/Rect;)V

    .line 1431
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setEnabledWithChildren(Z)V

    .line 1433
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v8, v0, :cond_2

    .line 1450
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    if-eqz v0, :cond_1

    .line 1451
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->releaseAnimation()V

    .line 1452
    :cond_1
    return-void

    .line 1434
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 1435
    .local v9, "v":Landroid/widget/LinearLayout;
    instance-of v0, v9, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v0, :cond_3

    .line 1436
    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getId()I

    move-result v0

    const v1, 0x11001108

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEnableDecoration()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1437
    check-cast v9, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .end local v9    # "v":Landroid/widget/LinearLayout;
    invoke-virtual {v9, v10}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setEnabledWidthChildren(Z)V

    .line 1438
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$14;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1433
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 656
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 652
    return-void
.end method

.method public refreshView()V
    .locals 0

    .prologue
    .line 1485
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 2

    .prologue
    .line 504
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 508
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 509
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 510
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_1

    .line 511
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 513
    :cond_1
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mOnFilp3dAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;

    .line 547
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->setStraightenEnable(Z)V

    .line 548
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->enableButtons()V

    .line 549
    return-void
.end method
