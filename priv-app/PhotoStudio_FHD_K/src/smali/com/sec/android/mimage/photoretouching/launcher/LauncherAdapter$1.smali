.class Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$1;
.super Ljava/lang/Object;
.source "LauncherAdapter.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    .line 373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    const/16 v5, 0x2d

    .line 377
    const/4 v1, 0x0

    .line 378
    .local v1, "nIndex":I
    const/4 v2, 0x0

    .line 380
    .local v2, "nResourceId":I
    if-eqz p2, :cond_5

    .line 382
    const/4 v3, 0x0

    .line 383
    .local v3, "uri":Landroid/net/Uri;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v5, :cond_2

    .line 428
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v4

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    if-eqz v4, :cond_1

    .line 429
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v4

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 432
    :cond_1
    return-void

    .line 385
    .restart local v3    # "uri":Landroid/net/Uri;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v4

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-eqz v4, :cond_4

    .line 388
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v4

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-ne p1, v4, :cond_4

    .line 390
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v4

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    if-nez v4, :cond_3

    .line 391
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$1(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;Z)V

    .line 395
    :goto_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    invoke-static {v4, v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$2(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;I)V

    .line 396
    move v1, v0

    .line 398
    goto :goto_1

    .line 393
    :cond_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$1(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;Z)V

    goto :goto_2

    .line 383
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 405
    .end local v0    # "i":I
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    if-ge v0, v5, :cond_0

    .line 407
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v4

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-eqz v4, :cond_6

    .line 410
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v4

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-ne p1, v4, :cond_6

    .line 412
    move v1, v0

    .line 413
    goto :goto_1

    .line 405
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method
