.class public interface abstract Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;
.super Ljava/lang/Object;
.source "MultiTouchGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnMultiTouchGestureListener"
.end annotation


# virtual methods
.method public abstract onMultiTouchDown()Z
.end method

.method public abstract onMultiTouchMove()Z
.end method

.method public abstract onMultiTouchScale(FFF)Z
.end method

.method public abstract onMultiTouchScroll(FF)Z
.end method

.method public abstract onMultiTouchUp()Z
.end method
