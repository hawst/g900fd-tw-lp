.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21$1;
.super Ljava/lang/Object;
.source "PenActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21$1;->this$1:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;

    .line 1554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1558
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21$1;->this$1:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;->access$1(Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;)Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/pen/util/SettingView;->isSettingViewVisible(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1559
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21$1;->this$1:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;->access$1(Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;)Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$40(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V

    .line 1563
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21$1;->this$1:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;->access$1(Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;)Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/pen/util/SettingView;->setVisibility(I)V

    .line 1564
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21$1;->this$1:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;->access$1(Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;)Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    move-result-object v0

    const/16 v1, 0x10

    # invokes: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->refreshPopupState(IZ)V
    invoke-static {v0, v1, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$42(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;IZ)V

    .line 1566
    return-void

    .line 1560
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21$1;->this$1:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;->access$1(Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;)Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/pen/util/SettingView;->isSettingViewVisible(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1561
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21$1;->this$1:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;->access$1(Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;)Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$41(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V

    goto :goto_0
.end method
