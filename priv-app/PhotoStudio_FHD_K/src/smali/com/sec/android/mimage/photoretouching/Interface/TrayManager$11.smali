.class Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$11;
.super Ljava/lang/Object;
.source "TrayManager.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->addButton(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$11;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 888
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 927
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 894
    const/4 v3, 0x1

    .line 895
    .local v3, "runDelete":Z
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayTouchFunction:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 897
    const/4 v1, -0x1

    .line 898
    .local v1, "idx":I
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 899
    .local v2, "parent":Landroid/view/ViewGroup;
    if-eqz v2, :cond_0

    .line 901
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 902
    .local v0, "grandParent":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 904
    .end local v0    # "grandParent":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayTouchFunction:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    move-result-object v5

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 905
    new-instance v6, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$11$1;

    invoke-direct {v6, p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$11$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$11;Landroid/view/View;)V

    .line 904
    invoke-interface {v5, v4, v6}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;->deleteButtonTouch(Landroid/view/View;Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$TrayDeleteFunction;)Z

    move-result v4

    .line 911
    if-eqz v4, :cond_3

    .line 904
    const/4 v3, 0x0

    .line 913
    .end local v1    # "idx":I
    .end local v2    # "parent":Landroid/view/ViewGroup;
    :cond_1
    :goto_0
    if-eqz v3, :cond_2

    .line 914
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteFunction(Landroid/view/View;)I
    invoke-static {v4, p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Landroid/view/View;)I

    .line 915
    :cond_2
    return-void

    .line 904
    .restart local v1    # "idx":I
    .restart local v2    # "parent":Landroid/view/ViewGroup;
    :cond_3
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 921
    return-void
.end method
