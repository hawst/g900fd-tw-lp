.class Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;
.super Ljava/lang/Object;
.source "SelectView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initActionbar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    .line 337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 0
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 349
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 353
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getPreviewRoi()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPreviewMaskRoi(Landroid/graphics/Rect;)V

    .line 354
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getPreviewMaskBuffer()[B

    move-result-object v1

    array-length v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskBuffer()[B

    move-result-object v2

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 355
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getPreviewMaskBuffer()[B

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskBuffer()[B

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskBuffer()[B

    move-result-object v3

    array-length v3, v3

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 356
    :cond_0
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)V

    .line 357
    .local v0, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addMaskBuffer(Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;)V

    .line 358
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 360
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->getPreviousStatus()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(ILcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;)V

    .line 362
    :cond_1
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 343
    return-void
.end method
