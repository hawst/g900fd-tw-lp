.class Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;
.super Ljava/lang/Object;
.source "LauncherAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->setThumbnailBitmap(ILandroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

.field private final synthetic val$bitmap:Landroid/graphics/Bitmap;

.field private final synthetic val$idx:I


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;ILandroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    iput p2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$bitmap:Landroid/graphics/Bitmap;

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 191
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    if-eqz v0, :cond_4

    .line 193
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 196
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 197
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    .line 199
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameView:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 205
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 206
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_3

    .line 207
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 208
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->isFlagData:Z

    .line 224
    :cond_4
    :goto_0
    return-void

    .line 212
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    .line 213
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 214
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_7

    .line 215
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 216
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameView:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 217
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 218
    :cond_8
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_9

    .line 219
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 220
    :cond_9
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;->val$idx:I

    aget-object v0, v0, v1

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->isFlagData:Z

    goto/16 :goto_0
.end method
