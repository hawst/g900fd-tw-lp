.class Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;
.super Ljava/lang/Object;
.source "EffectRedEyeMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RedEyeInfo"
.end annotation


# instance fields
.field mFixedRedEye:Z

.field mFixedRedEyeThread:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;

.field mPosX:F

.field mPosY:F

.field mTempOriginal:[I

.field mTempPreview:[I

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 379
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 381
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mPosX:F

    .line 382
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mPosY:F

    .line 383
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mFixedRedEye:Z

    .line 384
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempOriginal:[I

    .line 385
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempPreview:[I

    .line 386
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mFixedRedEyeThread:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;)V
    .locals 0

    .prologue
    .line 379
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)V

    return-void
.end method


# virtual methods
.method destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 389
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempOriginal:[I

    .line 390
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempPreview:[I

    .line 391
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mFixedRedEyeThread:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mFixedRedEyeThread:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->stopThread()V

    .line 393
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mFixedRedEyeThread:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;

    .line 394
    return-void
.end method
