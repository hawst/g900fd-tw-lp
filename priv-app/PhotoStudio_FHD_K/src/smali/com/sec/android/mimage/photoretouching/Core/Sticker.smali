.class public Lcom/sec/android/mimage/photoretouching/Core/Sticker;
.super Lcom/sec/android/mimage/photoretouching/Core/RectController;
.source "Sticker.java"


# static fields
.field public static final STICKER_MIMINUM_SIZE:I = 0x38


# instance fields
.field private OrgObjectBmp:Landroid/graphics/Bitmap;

.field private mBoundaryType:I

.field private mContext:Landroid/content/Context;

.field private mCurrentResId:I

.field private mFreeRect:Z

.field private mZOrder:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 96
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;-><init>()V

    .line 24
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mCurrentResId:I

    .line 25
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mZOrder:I

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mContext:Landroid/content/Context;

    .line 27
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mBoundaryType:I

    .line 28
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mFreeRect:Z

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;IIIZ)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imgData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p3, "resId"    # I
    .param p4, "zOrder"    # I
    .param p5, "boundaryType"    # I
    .param p6, "freeRect"    # Z

    .prologue
    .line 33
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p5

    move/from16 v4, p6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/RectController;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;IZ)V

    .line 24
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mCurrentResId:I

    .line 25
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mZOrder:I

    .line 26
    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mContext:Landroid/content/Context;

    .line 27
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mBoundaryType:I

    .line 28
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mFreeRect:Z

    .line 34
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mContext:Landroid/content/Context;

    .line 35
    move/from16 v0, p5

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mBoundaryType:I

    .line 36
    move/from16 v0, p6

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mFreeRect:Z

    .line 37
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mCurrentResId:I

    .line 38
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mZOrder:I

    .line 40
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v20

    div-int/lit8 v18, v20, 0x2

    .line 41
    .local v18, "x_center":I
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v20

    div-int/lit8 v19, v20, 0x2

    .line 44
    .local v19, "y_center":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v20, v0

    if-eqz v20, :cond_1

    .line 45
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v17

    .line 46
    .local v17, "viewTransform":Landroid/graphics/Matrix;
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 47
    .local v9, "invertMatrix":Landroid/graphics/Matrix;
    const/16 v20, 0x9

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v16, v0

    .line 49
    .local v16, "val":[F
    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 50
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 52
    const/16 v20, 0x0

    aget v11, v16, v20

    .line 53
    .local v11, "matrixScaleX":F
    const/16 v20, 0x4

    aget v12, v16, v20

    .line 55
    .local v12, "matrixScaleY":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_2

    const/4 v10, 0x1

    .line 56
    .local v10, "isImageLandscape":Z
    :goto_0
    const/high16 v13, 0x3f800000    # 1.0f

    .line 57
    .local v13, "orientationScale":F
    if-eqz v10, :cond_3

    .line 58
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v13, v20, v21

    .line 62
    :goto_1
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v20

    move/from16 v0, v20

    if-ne v10, v0, :cond_0

    .line 63
    const/high16 v13, 0x3f800000    # 1.0f

    .line 65
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, p3

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 67
    .local v14, "resBitmap":Landroid/graphics/Bitmap;
    if-eqz v14, :cond_4

    .line 68
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v21, v11, v13

    mul-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v8, v0

    .line 69
    .local v8, "bitmapWidth":I
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v21, v12, v13

    mul-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v7, v0

    .line 72
    .local v7, "bitmapHeight":I
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-static {v14, v8, v7, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 73
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_1

    .line 74
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->recycle()V

    .line 82
    .end local v7    # "bitmapHeight":I
    .end local v8    # "bitmapWidth":I
    .end local v9    # "invertMatrix":Landroid/graphics/Matrix;
    .end local v10    # "isImageLandscape":Z
    .end local v11    # "matrixScaleX":F
    .end local v12    # "matrixScaleY":F
    .end local v13    # "orientationScale":F
    .end local v14    # "resBitmap":Landroid/graphics/Bitmap;
    .end local v16    # "val":[F
    .end local v17    # "viewTransform":Landroid/graphics/Matrix;
    :cond_1
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 83
    .local v6, "ObjectWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 85
    .local v5, "ObjectHeight":I
    new-instance v15, Landroid/graphics/Rect;

    invoke-direct {v15}, Landroid/graphics/Rect;-><init>()V

    .line 86
    .local v15, "roi":Landroid/graphics/Rect;
    div-int/lit8 v20, v6, 0x2

    sub-int v20, v18, v20

    move/from16 v0, v20

    iput v0, v15, Landroid/graphics/Rect;->left:I

    .line 87
    iget v0, v15, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    add-int v20, v20, v6

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v20

    iput v0, v15, Landroid/graphics/Rect;->right:I

    .line 88
    div-int/lit8 v20, v5, 0x2

    sub-int v20, v19, v20

    move/from16 v0, v20

    iput v0, v15, Landroid/graphics/Rect;->top:I

    .line 89
    iget v0, v15, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    add-int v20, v20, v5

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v20

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    .line 90
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->setRectRoi(Landroid/graphics/Rect;)V

    .line 92
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const/16 v21, 0x38

    invoke-static/range {v20 .. v21}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getDpToPixel(Landroid/content/Context;I)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->setMinimumSize(I)V

    .line 94
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "sj, Sticker - init() - roi : "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 95
    return-void

    .line 55
    .end local v5    # "ObjectHeight":I
    .end local v6    # "ObjectWidth":I
    .end local v15    # "roi":Landroid/graphics/Rect;
    .restart local v9    # "invertMatrix":Landroid/graphics/Matrix;
    .restart local v11    # "matrixScaleX":F
    .restart local v12    # "matrixScaleY":F
    .restart local v16    # "val":[F
    .restart local v17    # "viewTransform":Landroid/graphics/Matrix;
    :cond_2
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 60
    .restart local v10    # "isImageLandscape":Z
    .restart local v13    # "orientationScale":F
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v13, v20, v21

    goto/16 :goto_1

    .line 76
    .restart local v14    # "resBitmap":Landroid/graphics/Bitmap;
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, p3

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    goto/16 :goto_2
.end method


# virtual methods
.method public EndMoveObject()V
    .locals 0

    .prologue
    .line 463
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->EndMoveObject()V

    .line 464
    return-void
.end method

.method public GetObjectBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public InitMoveObject(FF)I
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 454
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->InitMoveObject(FF)I

    move-result v0

    return v0
.end method

.method public StartMoveObject(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 459
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->StartMoveObject(FF)V

    .line 460
    return-void
.end method

.method public applyOriginal(Landroid/graphics/Bitmap;)V
    .locals 16
    .param p1, "originalBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 265
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v12}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalToPreviewRatio()F

    move-result v8

    .line 266
    .local v8, "scale":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getRoi()Landroid/graphics/RectF;

    move-result-object v5

    .line 268
    .local v5, "rectRoi":Landroid/graphics/RectF;
    iget v12, v5, Landroid/graphics/RectF;->left:F

    div-float/2addr v12, v8

    iput v12, v5, Landroid/graphics/RectF;->left:F

    .line 269
    iget v12, v5, Landroid/graphics/RectF;->top:F

    div-float/2addr v12, v8

    iput v12, v5, Landroid/graphics/RectF;->top:F

    .line 270
    iget v12, v5, Landroid/graphics/RectF;->right:F

    div-float/2addr v12, v8

    iput v12, v5, Landroid/graphics/RectF;->right:F

    .line 271
    iget v12, v5, Landroid/graphics/RectF;->bottom:F

    div-float/2addr v12, v8

    iput v12, v5, Landroid/graphics/RectF;->bottom:F

    .line 273
    new-instance v1, Landroid/graphics/Canvas;

    move-object/from16 v0, p1

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 274
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 276
    invoke-virtual {v1}, Landroid/graphics/Canvas;->save()I

    .line 277
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getAngle()I

    move-result v12

    int-to-float v12, v12

    .line 278
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v13

    iget v13, v13, Landroid/graphics/PointF;->x:F

    div-float/2addr v13, v8

    .line 279
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v14

    iget v14, v14, Landroid/graphics/PointF;->y:F

    div-float/2addr v14, v8

    .line 277
    invoke-virtual {v1, v12, v13, v14}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 281
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    .line 282
    .local v11, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 283
    .local v3, "height":I
    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v12

    float-to-int v7, v12

    .line 284
    .local v7, "resizeWidth":I
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v12

    float-to-int v6, v12

    .line 285
    .local v6, "resizeHeight":I
    int-to-float v12, v7

    int-to-float v13, v11

    div-float v10, v12, v13

    .line 286
    .local v10, "scaleWidth":F
    int-to-float v12, v6

    int-to-float v13, v3

    div-float v9, v12, v13

    .line 288
    .local v9, "scaleHeight":F
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 289
    .local v4, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v4, v10, v9}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 290
    iget v12, v5, Landroid/graphics/RectF;->left:F

    iget v13, v5, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v12, v13}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 292
    const/high16 v12, 0x3f800000    # 1.0f

    cmpl-float v12, v10, v12

    if-eqz v12, :cond_1

    .line 295
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v1, v12, v4, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 305
    :goto_0
    invoke-virtual {v1}, Landroid/graphics/Canvas;->restore()V

    .line 307
    .end local v3    # "height":I
    .end local v4    # "matrix":Landroid/graphics/Matrix;
    .end local v6    # "resizeHeight":I
    .end local v7    # "resizeWidth":I
    .end local v9    # "scaleHeight":F
    .end local v10    # "scaleWidth":F
    .end local v11    # "width":I
    :cond_0
    :goto_1
    return-void

    .line 297
    .restart local v3    # "height":I
    .restart local v4    # "matrix":Landroid/graphics/Matrix;
    .restart local v6    # "resizeHeight":I
    .restart local v7    # "resizeWidth":I
    .restart local v9    # "scaleHeight":F
    .restart local v10    # "scaleWidth":F
    .restart local v11    # "width":I
    :catch_0
    move-exception v2

    .line 298
    .local v2, "e":Ljava/lang/Exception;
    goto :goto_1

    .line 303
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    iget v13, v5, Landroid/graphics/RectF;->left:F

    iget v14, v5, Landroid/graphics/RectF;->top:F

    const/4 v15, 0x0

    invoke-virtual {v1, v12, v13, v14, v15}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public applyPreview(Landroid/graphics/Bitmap;)V
    .locals 14
    .param p1, "preview"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v13, 0x0

    .line 311
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getRoi()Landroid/graphics/RectF;

    move-result-object v4

    .line 312
    .local v4, "rectRoi":Landroid/graphics/RectF;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 313
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 315
    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 316
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getAngle()I

    move-result v10

    int-to-float v10, v10

    .line 317
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v11

    iget v11, v11, Landroid/graphics/PointF;->x:F

    .line 318
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v12

    iget v12, v12, Landroid/graphics/PointF;->y:F

    .line 316
    invoke-virtual {v0, v10, v11, v12}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 320
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    .line 321
    .local v9, "width":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 322
    .local v2, "height":I
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v10

    float-to-int v6, v10

    .line 323
    .local v6, "resizeWidth":I
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v10

    float-to-int v5, v10

    .line 324
    .local v5, "resizeHeight":I
    int-to-float v10, v6

    int-to-float v11, v9

    div-float v8, v10, v11

    .line 325
    .local v8, "scaleWidth":F
    int-to-float v10, v5

    int-to-float v11, v2

    div-float v7, v10, v11

    .line 327
    .local v7, "scaleHeight":F
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 328
    .local v3, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v3, v8, v7}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 329
    iget v10, v4, Landroid/graphics/RectF;->left:F

    iget v11, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v3, v10, v11}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 331
    const/high16 v10, 0x3f800000    # 1.0f

    cmpl-float v10, v8, v10

    if-eqz v10, :cond_1

    .line 334
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v0, v10, v3, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    :goto_0
    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 346
    .end local v2    # "height":I
    .end local v3    # "matrix":Landroid/graphics/Matrix;
    .end local v5    # "resizeHeight":I
    .end local v6    # "resizeWidth":I
    .end local v7    # "scaleHeight":F
    .end local v8    # "scaleWidth":F
    .end local v9    # "width":I
    :cond_0
    :goto_1
    return-void

    .line 336
    .restart local v2    # "height":I
    .restart local v3    # "matrix":Landroid/graphics/Matrix;
    .restart local v5    # "resizeHeight":I
    .restart local v6    # "resizeWidth":I
    .restart local v7    # "scaleHeight":F
    .restart local v8    # "scaleWidth":F
    .restart local v9    # "width":I
    :catch_0
    move-exception v1

    .line 337
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_1

    .line 342
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v10

    iget v11, v4, Landroid/graphics/RectF;->left:F

    iget v12, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v10, v11, v12, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/Sticker;)V
    .locals 2
    .param p1, "sticker"    # Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW sticker="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 103
    if-nez p1, :cond_0

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mCurrentResId:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mCurrentResId:I

    .line 107
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mZOrder:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mZOrder:I

    goto :goto_0
.end method

.method public destory()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 112
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->destroy()V

    .line 113
    return-void
.end method

.method public getAngle()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mAngle:I

    return v0
.end method

.method public getCurrentResId()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mCurrentResId:I

    return v0
.end method

.method public getDrawBdry()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getDrawCenterPT()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 148
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 149
    .local v0, "ret":Landroid/graphics/PointF;
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewPaddingX()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 150
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewPaddingY()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 151
    return-object v0
.end method

.method public getOrgDestROI()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mOriginalROI:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getZOrder()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mZOrder:I

    return v0
.end method

.method public setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V
    .locals 0
    .param p1, "roiPoint"    # Landroid/graphics/RectF;
    .param p2, "mOrgCenterPt"    # Landroid/graphics/PointF;
    .param p3, "mOrgDestPt1"    # Landroid/graphics/PointF;
    .param p4, "mOrgDestPt2"    # Landroid/graphics/PointF;
    .param p5, "mOrgDestPt3"    # Landroid/graphics/PointF;
    .param p6, "mOrgDestPt4"    # Landroid/graphics/PointF;
    .param p7, "angle"    # I

    .prologue
    .line 120
    invoke-super/range {p0 .. p7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V

    .line 121
    return-void
.end method

.method public setZOrder(I)V
    .locals 0
    .param p1, "idx"    # I

    .prologue
    .line 116
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->mZOrder:I

    .line 117
    return-void
.end method
