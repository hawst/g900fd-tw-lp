.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$20;
.super Ljava/lang/Object;
.source "PenActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->newSpenListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$20;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    .line 1279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClearAll()V
    .locals 2

    .prologue
    .line 1282
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$20;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$25(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeAllObject()V

    .line 1283
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$20;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->update()V

    .line 1284
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$20;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/pen/util/SettingView;->getSpenSettingEraserLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    move-result-object v0

    .line 1285
    .local v0, "layout":Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 1286
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$20;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->selectPenButton()V
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$39(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 1287
    return-void
.end method
