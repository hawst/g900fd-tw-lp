.class Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;
.super Ljava/lang/Object;
.source "SelectView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->onShow(Landroid/content/DialogInterface;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

.field private final synthetic val$arg0:Landroid/content/DialogInterface;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->val$arg0:Landroid/content/DialogInterface;

    .line 883
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v7, 0x7f0600e5

    const/4 v6, 0x0

    .line 887
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->val$arg0:Landroid/content/DialogInterface;

    check-cast v4, Landroid/app/Dialog;

    const v5, 0x7f09001f

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;

    .line 888
    .local v0, "eView":Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/content/Context;

    move-result-object v4

    .line 889
    const-string v5, "input_method"

    .line 888
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 890
    .local v2, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 891
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 892
    .local v1, "fileName":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-direct {v3, v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    .local v3, "saveFile":Ljava/io/File;
    if-eqz v0, :cond_0

    .line 895
    if-nez v1, :cond_1

    .line 896
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 897
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 899
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 900
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 901
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    invoke-static {v4, v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    .line 929
    :cond_0
    :goto_0
    return-void

    .line 904
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-gtz v4, :cond_2

    .line 905
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 906
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 908
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 909
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 910
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    invoke-static {v4, v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    goto :goto_0

    .line 913
    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 914
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0600da

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 915
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 917
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 918
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 919
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-result-object v4

    invoke-static {v4, v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    goto/16 :goto_0
.end method
