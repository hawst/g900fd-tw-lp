.class Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;
.super Ljava/lang/Thread;
.source "EffectRedEyeMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->applyRedEyePreview(Landroid/view/MotionEvent;[I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

.field private final synthetic val$fradius:I

.field private final synthetic val$output:[I

.field private final synthetic val$point:Landroid/graphics/Point;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;Landroid/graphics/Point;I[I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->val$point:Landroid/graphics/Point;

    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->val$fradius:I

    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->val$output:[I

    .line 182
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempOriginal:[I

    .line 185
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$4(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    .line 186
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$4(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v2

    .line 187
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempPreview:[I

    .line 188
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$4(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 189
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$4(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    .line 190
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->val$point:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    .line 191
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->val$point:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    .line 192
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->val$fradius:I

    .line 184
    invoke-static/range {v0 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->removeRedeyePreview([III[IIIIII)I

    .line 194
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$4(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    if-lt v9, v0, :cond_0

    .line 206
    return-void

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempPreview:[I

    .line 197
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$4(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    mul-int/2addr v1, v9

    .line 198
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$4(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 196
    invoke-static {v0, v1, v2}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v10

    .line 199
    .local v10, "inputBuffer":Ljava/nio/IntBuffer;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->val$output:[I

    .line 200
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$4(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v1

    add-int/2addr v1, v9

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$4(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v2

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$4(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v2

    add-int/2addr v1, v2

    .line 201
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$4(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 199
    invoke-static {v0, v1, v2}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v11

    .line 202
    .local v11, "outputBuffer":Ljava/nio/IntBuffer;
    invoke-virtual {v11, v10}, Ljava/nio/IntBuffer;->put(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;

    .line 203
    const/4 v10, 0x0

    .line 194
    add-int/lit8 v9, v9, 0x1

    goto :goto_0
.end method
