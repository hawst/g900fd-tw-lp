.class Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$6;
.super Ljava/lang/Object;
.source "ActionBarManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->enableBtnWithTextColor(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private final synthetic val$text:Landroid/widget/TextView;

.field private final synthetic val$v:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Landroid/view/View;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$6;->val$v:Landroid/view/View;

    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$6;->val$text:Landroid/widget/TextView;

    .line 941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 946
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$6;->val$v:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 947
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$6;->val$v:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 950
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$6;->val$text:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 951
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$6;->val$text:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040049

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 953
    :cond_1
    return-void
.end method
