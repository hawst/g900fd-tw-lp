.class public Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;
.super Landroid/widget/FrameLayout;
.source "SelectedButtonLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$ActivityLayoutTouchUpCallback;
    }
.end annotation


# instance fields
.field private mActivityLayoutTouchUpCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$ActivityLayoutTouchUpCallback;

.field private mIsIntercept:Z

.field private mPosX:F

.field private mPosY:F

.field private mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 126
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mIsIntercept:Z

    .line 132
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mActivityLayoutTouchUpCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$ActivityLayoutTouchUpCallback;

    .line 20
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->initLayout()V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 126
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mIsIntercept:Z

    .line 132
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mActivityLayoutTouchUpCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$ActivityLayoutTouchUpCallback;

    .line 26
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->initLayout()V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 126
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mIsIntercept:Z

    .line 132
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mActivityLayoutTouchUpCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$ActivityLayoutTouchUpCallback;

    .line 33
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->initLayout()V

    .line 34
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;)Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mIsIntercept:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;F)V
    .locals 0

    .prologue
    .line 129
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mPosX:F

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;F)V
    .locals 0

    .prologue
    .line 130
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mPosY:F

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;)Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;)F
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mPosX:F

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;)F
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mPosY:F

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;)Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$ActivityLayoutTouchUpCallback;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mActivityLayoutTouchUpCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$ActivityLayoutTouchUpCallback;

    return-object v0
.end method

.method private initLayout()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$1;-><init>(Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 121
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    .line 39
    return-void
.end method

.method public getPressedButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    return-object v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 43
    const/4 v0, 0x0

    .line 44
    .local v0, "ret":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mPosX:F

    .line 45
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mPosY:F

    .line 46
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mIsIntercept:Z

    if-eqz v1, :cond_0

    .line 48
    const/4 v0, 0x1

    .line 49
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 61
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 53
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mPosX:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setTranslationX(F)V

    .line 54
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mPosY:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setTranslationY(F)V

    goto :goto_0

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setDisableIntercept(FF)V
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mIsIntercept:Z

    .line 78
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->removeView(Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mActivityLayoutTouchUpCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$ActivityLayoutTouchUpCallback;

    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getLeft()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mPosX:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    .line 82
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getTop()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mPosY:F

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    .line 83
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getRight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mPosX:F

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    float-to-int v4, v4

    .line 84
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getBottom()I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mPosY:F

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    sub-float/2addr v5, v6

    float-to-int v5, v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 85
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    .line 81
    invoke-interface {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$ActivityLayoutTouchUpCallback;->touchUp(Landroid/graphics/Rect;Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)V

    .line 87
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    .line 88
    return-void
.end method

.method public setEnableIntercept(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$ActivityLayoutTouchUpCallback;)V
    .locals 3
    .param p1, "button"    # Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;
    .param p2, "callback"    # Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$ActivityLayoutTouchUpCallback;

    .prologue
    .line 65
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mActivityLayoutTouchUpCallback:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$ActivityLayoutTouchUpCallback;

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mIsIntercept:Z

    .line 68
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    .line 69
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->addView(Landroid/view/View;)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mPosX:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setTranslationX(F)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mPosY:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->mTempButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setTranslationY(F)V

    .line 72
    return-void
.end method
