.class public Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
.super Landroid/widget/ScrollView;
.source "TogglableScrollView.java"


# instance fields
.field private mScrollable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->mScrollable:Z

    .line 24
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->mContext:Landroid/content/Context;

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->mScrollable:Z

    .line 18
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->mContext:Landroid/content/Context;

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 12
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->mScrollable:Z

    .line 13
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->mContext:Landroid/content/Context;

    .line 14
    return-void
.end method


# virtual methods
.method public isScrollable()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->mScrollable:Z

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->mScrollable:Z

    if-eqz v0, :cond_0

    .line 48
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 50
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->mScrollable:Z

    if-eqz v0, :cond_0

    .line 40
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 42
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setScrollable(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->mScrollable:Z

    .line 30
    return-void
.end method
