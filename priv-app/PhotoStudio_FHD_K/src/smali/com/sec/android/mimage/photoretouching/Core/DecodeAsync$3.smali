.class Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$3;
.super Ljava/lang/Object;
.source "DecodeAsync.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$3;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 69
    const-string v2, "mProgressListener onCancel"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 70
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$3;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$8(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;Z)V

    .line 71
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$3;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$9(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 77
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$3;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$3;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFailRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$10(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 78
    return-void

    .line 73
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$3;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$9(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 74
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->task:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->task:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->isFinish()Z

    move-result v2

    if-nez v2, :cond_1

    .line 75
    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->task:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    .line 71
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
