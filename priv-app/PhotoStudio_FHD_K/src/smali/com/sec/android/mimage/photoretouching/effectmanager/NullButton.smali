.class public Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
.super Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;
.source "NullButton.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentPos:Landroid/graphics/Rect;

.field private mLeftBar:Landroid/widget/LinearLayout;

.field private mLeftButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

.field private mMoveToPos:Landroid/graphics/Rect;

.field private mRightButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;-><init>(Landroid/content/Context;)V

    .line 147
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mContext:Landroid/content/Context;

    .line 148
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mLeftButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 149
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mRightButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 152
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mCurrentPos:Landroid/graphics/Rect;

    .line 153
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mMoveToPos:Landroid/graphics/Rect;

    .line 155
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mLeftBar:Landroid/widget/LinearLayout;

    .line 16
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mContext:Landroid/content/Context;

    .line 17
    return-void
.end method


# virtual methods
.method public configurationChanged()V
    .locals 0

    .prologue
    .line 43
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->configurationChanged()V

    .line 44
    return-void
.end method

.method public copyButtonsNPosition(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V
    .locals 1
    .param p1, "button"    # Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .prologue
    .line 68
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getLeftButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 69
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getRightButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 70
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getCurrentPosition()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setCurrentPosition(Landroid/graphics/Rect;)V

    .line 71
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mCurrentPos:Landroid/graphics/Rect;

    .line 36
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mMoveToPos:Landroid/graphics/Rect;

    .line 37
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mLeftButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 38
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mRightButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 39
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->destroy()V

    .line 40
    return-void
.end method

.method public getCurrentPosition()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 145
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mCurrentPos:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method public getLeftButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mLeftButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    return-object v0
.end method

.method public getRightButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mRightButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    return-object v0
.end method

.method public initButton(ILandroid/graphics/Bitmap;I)V
    .locals 1
    .param p1, "buttonId"    # I
    .param p2, "icon"    # Landroid/graphics/Bitmap;
    .param p3, "textId"    # I

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->initButton(ILandroid/graphics/Bitmap;I)V

    .line 29
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mCurrentPos:Landroid/graphics/Rect;

    .line 30
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mMoveToPos:Landroid/graphics/Rect;

    .line 31
    const v0, 0x7f0900c9

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mLeftBar:Landroid/widget/LinearLayout;

    .line 32
    return-void
.end method

.method public moveToLeft()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mLeftButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mMoveToPos:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mLeftButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    invoke-interface {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getCurrentPosition()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 136
    :cond_0
    return-void
.end method

.method public moveToRight()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mRightButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mMoveToPos:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mRightButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    invoke-interface {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getCurrentPosition()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 143
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 49
    if-eqz p1, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mCurrentPos:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setLeft(I)V

    .line 52
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mCurrentPos:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setTop(I)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mCurrentPos:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setRight(I)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mCurrentPos:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setBottom(I)V

    .line 56
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->onLayout(ZIIII)V

    .line 57
    return-void
.end method

.method public reloadLayout()V
    .locals 0

    .prologue
    .line 21
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->init()V

    .line 22
    return-void
.end method

.method public runningAnimation()Z
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return v0
.end method

.method public setCurrentPosition(Landroid/graphics/Rect;)V
    .locals 5
    .param p1, "pos"    # Landroid/graphics/Rect;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mCurrentPos:Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 94
    return-void
.end method

.method public setLeftBarColor_Move(I)V
    .locals 2
    .param p1, "left"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mLeftBar:Landroid/widget/LinearLayout;

    const v1, 0x7f0201c4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 61
    return-void
.end method

.method public setLeftBarColor_Up()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mLeftBar:Landroid/widget/LinearLayout;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 65
    return-void
.end method

.method public setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V
    .locals 0
    .param p1, "left"    # Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mLeftButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 125
    return-void
.end method

.method public setMovePosition(Landroid/graphics/Rect;)V
    .locals 5
    .param p1, "pos"    # Landroid/graphics/Rect;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mMoveToPos:Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 98
    return-void
.end method

.method public setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V
    .locals 0
    .param p1, "right"    # Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mRightButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 129
    return-void
.end method

.method public startAnimation()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mMoveToPos:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setLeft(I)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mMoveToPos:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setTop(I)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mMoveToPos:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setRight(I)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mMoveToPos:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setBottom(I)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mCurrentPos:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->mMoveToPos:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 106
    return-void
.end method
