.class Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$10;
.super Ljava/lang/Object;
.source "AdjustmentView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->setMainBtnListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    .line 399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 422
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(Landroid/view/View;Z)V

    .line 407
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x31000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 410
    :cond_0
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 416
    return-void
.end method
