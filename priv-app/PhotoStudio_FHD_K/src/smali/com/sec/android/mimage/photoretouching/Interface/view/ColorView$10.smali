.class Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$10;
.super Ljava/lang/Object;
.source "ColorView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->init2DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    .line 744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 0
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 770
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v1, 0x8

    .line 751
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 753
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 754
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 764
    :cond_0
    :goto_0
    return-void

    .line 756
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    goto :goto_0

    .line 759
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 761
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x2e000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 776
    return-void
.end method
