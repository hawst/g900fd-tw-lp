.class public Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "LauncherAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;,
        Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;
    }
.end annotation


# instance fields
.field private FCL:Landroid/view/View$OnFocusChangeListener;

.field private final MAX_MULTIPLE_SELECTION:I

.field private mCurrentFocusIndex:I

.field private mCurrentLinearLayout:Landroid/widget/LinearLayout;

.field private mFrameViewRes:[I

.field private mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

.field private mImageViewRes:[I

.field private mImagebackgroundRes:[I

.field private mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

.field private mLongPress:Z

.field private mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

.field private mNoImageState:Z

.field private mNumThumbCount:I

.field private mThumbnailFrameLayout:[I


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;IZ)V
    .locals 5
    .param p1, "activity"    # Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;
    .param p2, "count"    # I
    .param p3, "refreshThumbnail"    # Z

    .prologue
    const/4 v0, 0x0

    const/16 v4, 0x2d

    const/16 v3, 0x8

    const/16 v2, 0xf

    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 22
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->MAX_MULTIPLE_SELECTION:I

    .line 23
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    .line 24
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    .line 25
    new-array v0, v4, [Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    .line 26
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mNumThumbCount:I

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mCurrentFocusIndex:I

    .line 29
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mNoImageState:Z

    .line 31
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLongPress:Z

    .line 32
    new-array v0, v3, [Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    .line 34
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    .line 49
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageViewRes:[I

    .line 52
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    .line 67
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mFrameViewRes:[I

    .line 70
    new-array v0, v2, [I

    fill-array-data v0, :array_2

    .line 85
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImagebackgroundRes:[I

    .line 87
    new-array v0, v2, [I

    fill-array-data v0, :array_3

    .line 102
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mThumbnailFrameLayout:[I

    .line 373
    new-instance v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$1;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->FCL:Landroid/view/View$OnFocusChangeListener;

    .line 107
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    .line 108
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mNumThumbCount:I

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->initImageData()V

    .line 110
    if-eqz p3, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getThumbnail(II)V

    .line 112
    :cond_0
    return-void

    .line 34
    :array_0
    .array-data 4
        0x7f0900eb
        0x7f0900ef
        0x7f0900f3
        0x7f0900f7
        0x7f0900fb
        0x7f0900ff
        0x7f090103
        0x7f090107
        0x7f09010b
        0x7f09010f
        0x7f090113
        0x7f090117
        0x7f09011b
        0x7f09011f
        0x7f090123
    .end array-data

    .line 52
    :array_1
    .array-data 4
        0x7f0900ec
        0x7f0900f0
        0x7f0900f4
        0x7f0900f8
        0x7f0900fc
        0x7f090100
        0x7f090104
        0x7f090108
        0x7f09010c
        0x7f090110
        0x7f090114
        0x7f090118
        0x7f09011c
        0x7f090120
        0x7f090124
    .end array-data

    .line 70
    :array_2
    .array-data 4
        0x7f0900ed
        0x7f0900f1
        0x7f0900f5
        0x7f0900f9
        0x7f0900fd
        0x7f090101
        0x7f090105
        0x7f090109
        0x7f09010d
        0x7f090111
        0x7f090115
        0x7f090119
        0x7f09011d
        0x7f090121
        0x7f090125
    .end array-data

    .line 87
    :array_3
    .array-data 4
        0x7f0900ea
        0x7f0900ee
        0x7f0900f2
        0x7f0900f6
        0x7f0900fa
        0x7f0900fe
        0x7f090102
        0x7f090106
        0x7f09010a
        0x7f09010e
        0x7f090112
        0x7f090116
        0x7f09011a
        0x7f09011e
        0x7f090122
    .end array-data
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;)[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;Z)V
    .locals 0

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mNoImageState:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;I)V
    .locals 0

    .prologue
    .line 27
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mCurrentFocusIndex:I

    return-void
.end method

.method private lockThumbnails(Landroid/view/View;)V
    .locals 3
    .param p1, "clickedView"    # Landroid/view/View;

    .prologue
    .line 663
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x2d

    if-lt v0, v1, :cond_0

    .line 670
    return-void

    .line 665
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 667
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 663
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public cancelMultiSelection()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 320
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLongPress:Z

    .line 322
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->resetMultiSelection()V

    .line 323
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setActionBarLayout(I)V

    .line 324
    const-string v0, "Cancel Multi Selection"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 325
    return-void
.end method

.method public configurationChanged(I)V
    .locals 0
    .param p1, "currentNum"    # I

    .prologue
    .line 355
    return-void
.end method

.method public destroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 358
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x2d

    if-lt v0, v1, :cond_0

    .line 371
    return-void

    .line 360
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 362
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 363
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v1, v1, v0

    iput-object v3, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    .line 364
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v1, v1, v0

    iput-object v3, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    .line 365
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v1, v1, v0

    iput-object v3, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameView:Landroid/widget/ImageView;

    .line 366
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v1, v1, v0

    iput-object v3, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    .line 367
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->isFlagData:Z

    .line 368
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aput-object v3, v1, v0

    .line 358
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public destroyItem(Landroid/view/View;ILjava/lang/Object;)V
    .locals 0
    .param p1, "collection"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "view"    # Ljava/lang/Object;

    .prologue
    .line 457
    return-void
.end method

.method public doneMultiSelection()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 300
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLongPress:Z

    .line 301
    const-string v2, "Done Multi Selection"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 302
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setActionBarLayout(I)V

    .line 303
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 305
    .local v1, "path":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x8

    if-lt v0, v2, :cond_0

    .line 315
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->startMainActivity(Ljava/util/ArrayList;)V

    .line 316
    return-void

    .line 307
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    if-eqz v2, :cond_1

    .line 309
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v2, v2, v0

    iput-object v4, v2, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->view:Landroid/view/View;

    .line 312
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v2, v2, v0

    iput-object v4, v2, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    .line 305
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public finishUpdate(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 547
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 530
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mNumThumbCount:I

    if-nez v0, :cond_0

    .line 531
    const/4 v0, 0x1

    .line 535
    :goto_0
    return v0

    .line 532
    :cond_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mNumThumbCount:I

    rem-int/lit8 v0, v0, 0xf

    if-nez v0, :cond_1

    .line 533
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mNumThumbCount:I

    div-int/lit8 v0, v0, 0xf

    goto :goto_0

    .line 535
    :cond_1
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mNumThumbCount:I

    div-int/lit8 v0, v0, 0xf

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getCurrentFocusIndex()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mCurrentFocusIndex:I

    return v0
.end method

.method public getCurrentLinearLayout()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getImageData(I)Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    if-nez v0, :cond_0

    .line 148
    const/4 v0, 0x0

    .line 149
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public getMultiSelectCount()I
    .locals 4

    .prologue
    .line 125
    const/4 v2, 0x0

    .line 126
    .local v2, "ret":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v3, 0x8

    if-lt v0, v3, :cond_0

    .line 131
    return v2

    .line 127
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v1, v3, v0

    .line 128
    .local v1, "multiData":Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;
    iget-object v3, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    if-eqz v3, :cond_1

    .line 129
    add-int/lit8 v2, v2, 0x1

    .line 126
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getNoImageState()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mNoImageState:Z

    return v0
.end method

.method public initImageData()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 115
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    const/16 v0, 0x2d

    if-lt v7, v0, :cond_0

    .line 120
    const/4 v7, 0x0

    :goto_1
    const/16 v0, 0x8

    if-lt v7, v0, :cond_1

    .line 122
    return-void

    .line 116
    :cond_0
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    new-instance v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-object v1, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/net/Uri;Landroid/widget/ImageView;Landroid/widget/FrameLayout;)V

    aput-object v0, v8, v7

    .line 117
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v0, v0, v7

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->isFlagData:Z

    .line 115
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    invoke-direct {v1, p0, v2, v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;Landroid/view/View;Landroid/net/Uri;)V

    aput-object v1, v0, v7

    .line 120
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method

.method public instantiateItem(Landroid/view/View;I)Ljava/lang/Object;
    .locals 4
    .param p1, "page"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    const/4 v3, 0x0

    const v2, 0x7f03004e

    .line 437
    const/4 v0, 0x0

    .line 439
    .local v0, "view":Landroid/view/View;
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mNumThumbCount:I

    if-lez v1, :cond_0

    .line 441
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 442
    const v1, 0x7f0900e9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    .line 444
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->setPageView(I)V

    :goto_0
    move-object v1, p1

    .line 450
    check-cast v1, Landroid/support/v4/view/ViewPager;

    move-object v2, p1

    check-cast v2, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v2

    if-le v2, p2, :cond_1

    .end local p1    # "page":Landroid/view/View;
    .end local p2    # "position":I
    :goto_1
    invoke-virtual {v1, v0, p2}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;I)V

    .line 451
    return-object v0

    .line 448
    .restart local p1    # "page":Landroid/view/View;
    .restart local p2    # "position":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 450
    :cond_1
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "page":Landroid/view/View;
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result p2

    goto :goto_1
.end method

.method public isSetBitmap(I)Z
    .locals 2
    .param p1, "idx"    # I

    .prologue
    const/4 v0, 0x1

    .line 343
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v1, v1, p1

    if-eqz v1, :cond_0

    .line 345
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v1, v1, p1

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 346
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v0, v0, p1

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->isFlagData:Z

    .line 350
    :cond_0
    return v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 540
    check-cast p2, Landroid/view/View;

    .end local p2    # "object":Ljava/lang/Object;
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    .line 578
    const/4 v0, 0x0

    .line 579
    .local v0, "i":I
    const/4 v1, 0x0

    .line 581
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x0

    :goto_0
    const/16 v4, 0x2d

    if-lt v0, v4, :cond_1

    .line 591
    :goto_1
    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLongPress:Z

    if-nez v4, :cond_3

    .line 593
    if-eqz v1, :cond_0

    .line 595
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->lockThumbnails(Landroid/view/View;)V

    .line 596
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getStartNormalView()Z

    move-result v4

    if-nez v4, :cond_0

    .line 597
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 598
    .local v2, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 599
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v4, v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->startMainActivity(Ljava/util/ArrayList;)V

    .line 632
    .end local v2    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_0
    :goto_2
    return-void

    .line 583
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    if-eqz v4, :cond_2

    .line 585
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    if-ne p1, v4, :cond_2

    .line 586
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v4, v4, v0

    iget-object v1, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->uri:Landroid/net/Uri;

    .line 587
    goto :goto_1

    .line 581
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 605
    :cond_3
    const/4 v3, 0x0

    .line 606
    .local v3, "uriexist":Z
    const/4 v0, 0x0

    :goto_3
    if-lt v0, v5, :cond_6

    .line 616
    :goto_4
    if-nez v3, :cond_4

    .line 618
    const/4 v0, 0x0

    :goto_5
    if-lt v0, v5, :cond_8

    .line 628
    :cond_4
    :goto_6
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->getMultiSelectCount()I

    move-result v4

    if-ne v4, v5, :cond_5

    .line 629
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    const v5, 0x7f0600ff

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;I)V

    .line 630
    :cond_5
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setMultiSelectDropText()V

    goto :goto_2

    .line 607
    :cond_6
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    invoke-virtual {v4, v1}, Landroid/net/Uri;->compareTo(Landroid/net/Uri;)I

    move-result v4

    if-nez v4, :cond_7

    .line 609
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v4, v4, v0

    iput-object v6, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    .line 611
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v4, v4, v0

    iput-object v6, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->view:Landroid/view/View;

    .line 612
    const/4 v3, 0x1

    .line 613
    goto :goto_4

    .line 606
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 619
    :cond_8
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    if-nez v4, :cond_9

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->view:Landroid/view/View;

    if-nez v4, :cond_9

    .line 621
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v4, v4, v0

    iput-object v1, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    .line 622
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v4, v4, v0

    iput-object p1, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->view:Landroid/view/View;

    goto :goto_6

    .line 618
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method public onKeyEnter()V
    .locals 4

    .prologue
    .line 555
    const/4 v1, 0x0

    .line 556
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v3, 0x2d

    if-lt v0, v3, :cond_1

    .line 567
    :goto_1
    if-eqz v1, :cond_0

    .line 569
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 570
    .local v2, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 571
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v3, v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->startMainActivity(Ljava/util/ArrayList;)V

    .line 573
    .end local v2    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_0
    return-void

    .line 558
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-eqz v3, :cond_2

    .line 560
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->isFocused()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 561
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v3, v3, v0

    iget-object v1, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->uri:Landroid/net/Uri;

    .line 562
    goto :goto_1

    .line 556
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/16 v4, 0x8

    .line 462
    const/4 v0, 0x0

    .line 464
    .local v0, "i":I
    const/4 v1, 0x0

    .line 465
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x0

    :goto_0
    const/16 v3, 0x2d

    if-lt v0, v3, :cond_3

    .line 477
    :goto_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    if-eqz v3, :cond_9

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLongPress:Z

    if-eqz v3, :cond_9

    .line 479
    const/4 v2, 0x0

    .line 480
    .local v2, "uriexist":Z
    const/4 v0, 0x0

    :goto_2
    if-lt v0, v4, :cond_5

    .line 490
    :goto_3
    if-nez v2, :cond_0

    .line 492
    const/4 v0, 0x0

    :goto_4
    if-lt v0, v4, :cond_7

    .line 503
    :cond_0
    :goto_5
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->getMultiSelectCount()I

    move-result v3

    if-ne v3, v4, :cond_1

    .line 504
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    const v4, 0x7f0600ff

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;I)V

    .line 505
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setMultiSelectDropText()V

    .line 521
    .end local v2    # "uriexist":Z
    :cond_2
    :goto_6
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLongPress:Z

    .line 522
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setActionBarLayout(I)V

    .line 523
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setMultiSelectDropText()V

    .line 524
    const-string v3, "LongPress"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 525
    return v5

    .line 467
    :cond_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    if-eqz v3, :cond_4

    .line 469
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    if-ne p1, v3, :cond_4

    .line 470
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v3, v3, v0

    iget-object v1, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->uri:Landroid/net/Uri;

    .line 471
    goto :goto_1

    .line 465
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 481
    .restart local v2    # "uriexist":Z
    :cond_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    invoke-virtual {v3, v1}, Landroid/net/Uri;->compareTo(Landroid/net/Uri;)I

    move-result v3

    if-nez v3, :cond_6

    .line 483
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v3, v3, v0

    iput-object v6, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    .line 485
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v3, v3, v0

    iput-object v6, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->view:Landroid/view/View;

    .line 486
    const/4 v2, 0x1

    .line 487
    goto :goto_3

    .line 480
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 493
    :cond_7
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->view:Landroid/view/View;

    if-nez v3, :cond_8

    .line 495
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v3, v3, v0

    iput-object v1, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    .line 496
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v3, v3, v0

    iput-object p1, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->view:Landroid/view/View;

    goto :goto_5

    .line 492
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_4

    .line 508
    .end local v2    # "uriexist":Z
    :cond_9
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    if-eqz v3, :cond_2

    .line 510
    const/4 v0, 0x0

    :goto_7
    if-ge v0, v4, :cond_2

    .line 511
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->view:Landroid/view/View;

    if-nez v3, :cond_a

    .line 513
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v3, v3, v0

    iput-object v1, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    .line 514
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v3, v3, v0

    iput-object p1, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->view:Landroid/view/View;

    goto/16 :goto_6

    .line 510
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_7
.end method

.method public resetMultiSelection()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 328
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    if-eqz v1, :cond_0

    .line 330
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x8

    if-lt v0, v1, :cond_1

    .line 340
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 332
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->view:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 335
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v1, v1, v0

    iput-object v2, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->view:Landroid/view/View;

    .line 336
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v1, v1, v0

    iput-object v2, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    .line 330
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0
    .param p1, "pc"    # Landroid/os/Parcelable;
    .param p2, "cl"    # Ljava/lang/ClassLoader;

    .prologue
    .line 549
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 544
    const/4 v0, 0x0

    return-object v0
.end method

.method public setImageUri(ILandroid/net/Uri;)V
    .locals 1
    .param p1, "idx"    # I
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v0, v0, p1

    iput-object p2, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->uri:Landroid/net/Uri;

    .line 158
    :cond_0
    return-void
.end method

.method public setPageView(I)V
    .locals 13
    .param p1, "viewPosition"    # I

    .prologue
    .line 231
    mul-int/lit8 v8, p1, 0xf

    .line 232
    .local v8, "startIndex":I
    add-int/lit8 v2, v8, 0xf

    .line 234
    .local v2, "endIndex":I
    iget v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mNumThumbCount:I

    if-le v2, v10, :cond_0

    .line 236
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mNumThumbCount:I

    .line 238
    :cond_0
    move v5, v8

    .local v5, "i":I
    :goto_0
    if-lt v5, v2, :cond_1

    .line 296
    return-void

    .line 240
    :cond_1
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v10, v10, v5

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    if-eqz v10, :cond_2

    .line 241
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v10, v10, v5

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 242
    :cond_2
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v10, v10, v5

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameView:Landroid/widget/ImageView;

    if-eqz v10, :cond_3

    .line 243
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v10, v10, v5

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameView:Landroid/widget/ImageView;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 244
    :cond_3
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v10, v10, v5

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    if-eqz v10, :cond_4

    .line 245
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v10, v10, v5

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 246
    :cond_4
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v10, v10, v5

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-eqz v10, :cond_5

    .line 247
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v10, v10, v5

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 249
    :cond_5
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageViewRes:[I

    rem-int/lit8 v12, v5, 0xf

    aget v11, v11, v12

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 250
    .local v6, "imageView":Landroid/widget/ImageView;
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mFrameViewRes:[I

    rem-int/lit8 v12, v5, 0xf

    aget v11, v11, v12

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 251
    .local v4, "frameView":Landroid/widget/ImageView;
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImagebackgroundRes:[I

    rem-int/lit8 v12, v5, 0xf

    aget v11, v11, v12

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 252
    .local v0, "background":Landroid/widget/ImageView;
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mThumbnailFrameLayout:[I

    rem-int/lit8 v12, v5, 0xf

    aget v11, v11, v12

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    .line 254
    .local v3, "fl":Landroid/widget/FrameLayout;
    if-eqz v0, :cond_6

    .line 255
    const/4 v10, 0x0

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 257
    :cond_6
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 258
    const/4 v10, 0x1

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 260
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v10, v10, v5

    iput-object v6, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    .line 261
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v10, v10, v5

    iput-object v4, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameView:Landroid/widget/ImageView;

    .line 262
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v10, v10, v5

    iput-object v0, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    .line 263
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v10, v10, v5

    iput-object v3, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    .line 265
    sget-object v10, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 266
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v10, v5}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getThumbnailBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 267
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 269
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->FCL:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v3, v10}, Landroid/widget/FrameLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 271
    if-eqz v1, :cond_7

    .line 273
    invoke-virtual {v6, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 274
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v10, v10, v5

    const/4 v11, 0x1

    iput-boolean v11, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->isFlagData:Z

    .line 275
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 277
    :cond_7
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v10, v5}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getThumbnailUri(I)Landroid/net/Uri;

    move-result-object v9

    .line 278
    .local v9, "uri":Landroid/net/Uri;
    if-eqz v9, :cond_8

    .line 280
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v10, v10, v5

    iput-object v9, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->uri:Landroid/net/Uri;

    .line 284
    :cond_8
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    if-eqz v10, :cond_9

    .line 286
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_1
    const/16 v10, 0x8

    if-lt v7, v10, :cond_a

    .line 238
    .end local v7    # "j":I
    :cond_9
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 287
    .restart local v7    # "j":I
    :cond_a
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v10, v10, v7

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    if-eqz v10, :cond_b

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v10, v10, v7

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->uri:Landroid/net/Uri;

    invoke-virtual {v10, v9}, Landroid/net/Uri;->compareTo(Landroid/net/Uri;)I

    move-result v10

    if-nez v10, :cond_b

    .line 290
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mMultiData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;

    aget-object v10, v10, v7

    iput-object v0, v10, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$MultiSelectionData;->view:Landroid/view/View;

    goto :goto_2

    .line 286
    :cond_b
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method

.method public setThumbnailBitmap(ILandroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "idx"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 161
    const/4 v1, 0x0

    .line 162
    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    if-eqz v2, :cond_1

    .line 163
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v2, v2, p1

    if-eqz v2, :cond_1

    .line 164
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v2, v2, p1

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 165
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v2, v2, p1

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->backgroundView:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 166
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v2, v2, p1

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameView:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 167
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v2, v2, p1

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_1

    .line 182
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v2, v2, p1

    if-eqz v2, :cond_0

    .line 184
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    aget-object v2, v2, p1

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->imageView:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    .line 186
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->mLauncherActivity:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;

    invoke-direct {v3, p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$2;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;ILandroid/graphics/Bitmap;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 228
    :cond_0
    return-void

    .line 169
    :cond_1
    const/16 v2, 0xa

    if-gt v1, v2, :cond_0

    .line 174
    const-wide/16 v2, 0x1e

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 175
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method public startUpdate(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 551
    return-void
.end method
