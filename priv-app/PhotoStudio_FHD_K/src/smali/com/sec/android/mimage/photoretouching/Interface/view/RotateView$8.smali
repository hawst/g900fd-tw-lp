.class Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;
.super Ljava/lang/Object;
.source "RotateView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->setConfigurationChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    .line 513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public startAnimation(I)V
    .locals 26
    .param p1, "animType"    # I

    .prologue
    .line 517
    packed-switch p1, :pswitch_data_0

    .line 540
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    move-result-object v2

    new-instance v3, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v3}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 541
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->setFillAfter(Z)V

    .line 542
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->setAnimationListenerCallback()V

    .line 544
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    move-result-object v3

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->startImageEditViewAnimation(Landroid/view/animation/Animation;)V
    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Landroid/view/animation/Animation;)V

    .line 545
    return-void

    .line 519
    :pswitch_0
    const/16 v25, 0x5a

    .line 520
    .local v25, "angle":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    move-result-object v2

    move/from16 v0, v25

    invoke-virtual {v2, v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->getScaleForFittedImage(I)F

    move-result v11

    .line 521
    .local v11, "scale":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move/from16 v0, v25

    int-to-float v9, v0

    const/high16 v10, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewWidth()I
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    int-to-float v12, v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewHeight()I
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)I

    move-result v13

    div-int/lit8 v13, v13, 0x2

    int-to-float v13, v13

    move/from16 v14, p1

    invoke-direct/range {v2 .. v14}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;FFFFFFFFFFI)V

    invoke-static {v15, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)V

    .line 522
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    move-result-object v2

    const-wide/16 v4, 0x15e

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->setDuration(J)V

    goto/16 :goto_0

    .line 525
    .end local v11    # "scale":F
    .end local v25    # "angle":I
    :pswitch_1
    const/16 v25, -0x5a

    .line 526
    .restart local v25    # "angle":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    move-result-object v2

    move/from16 v0, v25

    invoke-virtual {v2, v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->getScaleForFittedImage(I)F

    move-result v11

    .line 527
    .restart local v11    # "scale":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move/from16 v0, v25

    int-to-float v9, v0

    const/high16 v10, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewWidth()I
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    int-to-float v12, v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewHeight()I
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)I

    move-result v13

    div-int/lit8 v13, v13, 0x2

    int-to-float v13, v13

    move/from16 v14, p1

    invoke-direct/range {v2 .. v14}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;FFFFFFFFFFI)V

    invoke-static {v15, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)V

    .line 528
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    move-result-object v2

    const-wide/16 v4, 0x15e

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->setDuration(J)V

    goto/16 :goto_0

    .line 531
    .end local v11    # "scale":F
    .end local v25    # "angle":I
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    new-instance v12, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/high16 v17, -0x3ccc0000    # -180.0f

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/high16 v20, 0x3f800000    # 1.0f

    const/high16 v21, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewWidth()I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v0, v3

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewHeight()I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v0, v3

    move/from16 v23, v0

    move/from16 v24, p1

    invoke-direct/range {v12 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;FFFFFFFFFFI)V

    invoke-static {v2, v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)V

    .line 532
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    move-result-object v2

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->setDuration(J)V

    goto/16 :goto_0

    .line 535
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    new-instance v12, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    const/4 v14, 0x0

    const/high16 v15, 0x43340000    # 180.0f

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/high16 v20, 0x3f800000    # 1.0f

    const/high16 v21, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewWidth()I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v0, v3

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewHeight()I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v0, v3

    move/from16 v23, v0

    move/from16 v24, p1

    invoke-direct/range {v12 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;FFFFFFFFFFI)V

    invoke-static {v2, v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)V

    .line 536
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    move-result-object v2

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->setDuration(J)V

    goto/16 :goto_0

    .line 517
    nop

    :pswitch_data_0
    .packed-switch 0x11101101
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
