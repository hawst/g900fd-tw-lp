.class Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;
.super Ljava/lang/Object;
.source "CollageColorPicker.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->initListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "button"    # Landroid/view/View;

    .prologue
    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : mOnClickColorButton : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 190
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const/4 v1, 0x0

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setPickerPos(Landroid/view/MotionEvent;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Landroid/view/MotionEvent;Z)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColor:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$4(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;->setBackgroundColor(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setSelected(Landroid/view/View;)V
    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$5(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Landroid/view/View;)V

    .line 194
    :cond_0
    return-void

    .line 144
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V

    goto :goto_0

    .line 147
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const v1, -0x200d3

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V

    goto :goto_0

    .line 150
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const/16 v1, -0x7ca3

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V

    goto :goto_0

    .line 153
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const v1, -0xc4a5

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V

    goto :goto_0

    .line 156
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const v1, -0xb637

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V

    goto :goto_0

    .line 159
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const v1, -0x357a01

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V

    goto :goto_0

    .line 162
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const v1, -0x85c822

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V

    goto :goto_0

    .line 165
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const v1, -0xfe6bd2

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V

    goto :goto_0

    .line 168
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const v1, -0xc75701

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V

    goto :goto_0

    .line 171
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const v1, -0xcc9803

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V

    goto :goto_0

    .line 174
    :pswitch_a
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const v1, -0x595a5b

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V

    goto :goto_0

    .line 177
    :pswitch_b
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const v1, -0xc9c9ca

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V

    goto/16 :goto_0

    .line 180
    :pswitch_c
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const/high16 v1, -0x1000000

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V

    goto/16 :goto_0

    .line 183
    :pswitch_d
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColorBtn:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$1(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V

    goto/16 :goto_0

    .line 141
    :pswitch_data_0
    .packed-switch 0x7f09007a
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
