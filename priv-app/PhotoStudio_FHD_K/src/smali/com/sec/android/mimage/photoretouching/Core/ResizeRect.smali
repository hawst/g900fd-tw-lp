.class public Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;
.super Lcom/sec/android/mimage/photoretouching/Core/RectController;
.source "ResizeRect.java"


# static fields
.field public static final START_SCALE_FACTOR:F = 0.75f


# instance fields
.field private mOrgObjectBmp:Landroid/graphics/Bitmap;

.field private mScale:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;IZ)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imgData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p3, "boundaryType"    # I
    .param p4, "freeRect"    # Z

    .prologue
    .line 20
    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-direct {p0, p1, p2, v7, v8}, Lcom/sec/android/mimage/photoretouching/Core/RectController;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;IZ)V

    .line 98
    const/high16 v7, 0x3f800000    # 1.0f

    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mScale:F

    .line 22
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mOrgObjectBmp:Landroid/graphics/Bitmap;

    .line 24
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v7

    div-int/lit8 v5, v7, 0x2

    .line 25
    .local v5, "x_center":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v7

    div-int/lit8 v6, v7, 0x2

    .line 26
    .local v6, "y_center":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v1

    .line 27
    .local v1, "objWidth":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v0

    .line 28
    .local v0, "objHeight":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mOrgObjectBmp:Landroid/graphics/Bitmap;

    .line 29
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 30
    .local v2, "roi":Landroid/graphics/Rect;
    int-to-float v7, v1

    const/high16 v8, 0x3f400000    # 0.75f

    mul-float/2addr v7, v8

    float-to-int v4, v7

    .line 31
    .local v4, "roiWidth":I
    int-to-float v7, v0

    const/high16 v8, 0x3f400000    # 0.75f

    mul-float/2addr v7, v8

    float-to-int v3, v7

    .line 32
    .local v3, "roiHeight":I
    div-int/lit8 v7, v4, 0x2

    sub-int v7, v5, v7

    iput v7, v2, Landroid/graphics/Rect;->left:I

    .line 33
    iget v7, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v7, v4

    add-int/lit8 v7, v7, -0x1

    iput v7, v2, Landroid/graphics/Rect;->right:I

    .line 34
    div-int/lit8 v7, v3, 0x2

    sub-int v7, v6, v7

    iput v7, v2, Landroid/graphics/Rect;->top:I

    .line 35
    iget v7, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, v3

    add-int/lit8 v7, v7, -0x1

    iput v7, v2, Landroid/graphics/Rect;->bottom:I

    .line 36
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->setRectRoi(Landroid/graphics/Rect;)V

    .line 37
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->setRightTopRotate(Z)V

    .line 38
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->setCheckBoundary(Z)V

    .line 39
    const v7, 0x3dcccccd    # 0.1f

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->setCustomMinSize(F)V

    .line 40
    const/16 v7, 0x3e8

    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->limit:I

    .line 41
    return-void
.end method


# virtual methods
.method public EndMoveObject()V
    .locals 0

    .prologue
    .line 92
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->EndMoveObject()V

    .line 93
    return-void
.end method

.method public InitMoveObject(FF)I
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 79
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->InitMoveObject(FF)I

    move-result v0

    return v0
.end method

.method public StartMoveObject(FF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 84
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->StartMoveObject(FF)V

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 87
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->setResizeScale(F)V

    .line 89
    :cond_0
    return-void
.end method

.method public destory()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mOrgObjectBmp:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mOrgObjectBmp:Landroid/graphics/Bitmap;

    .line 46
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->destroy()V

    .line 47
    return-void
.end method

.method public getObjectBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mOrgObjectBmp:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mScale:F

    return v0
.end method

.method public run_blending_org()V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method public setResizeScale(F)V
    .locals 12
    .param p1, "scale"    # F

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 51
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mScale:F

    .line 52
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v9

    .line 53
    .local v9, "originalWidth":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v8

    .line 54
    .local v8, "originalHeight":I
    int-to-float v0, v9

    mul-float v11, v0, p1

    .line 55
    .local v11, "tempWidth":F
    int-to-float v0, v8

    mul-float v10, v0, p1

    .line 56
    .local v10, "tempHeight":F
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 57
    .local v1, "roiPoint":Landroid/graphics/RectF;
    int-to-float v0, v9

    sub-float/2addr v0, v11

    div-float/2addr v0, v2

    iput v0, v1, Landroid/graphics/RectF;->left:F

    .line 58
    int-to-float v0, v8

    sub-float/2addr v0, v10

    div-float/2addr v0, v2

    iput v0, v1, Landroid/graphics/RectF;->top:F

    .line 59
    iget v0, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v11

    iput v0, v1, Landroid/graphics/RectF;->right:F

    .line 60
    iget v0, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v10

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 61
    new-instance v3, Landroid/graphics/PointF;

    iget v0, v1, Landroid/graphics/RectF;->left:F

    iget v2, v1, Landroid/graphics/RectF;->top:F

    invoke-direct {v3, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 62
    .local v3, "dpt1":Landroid/graphics/PointF;
    new-instance v4, Landroid/graphics/PointF;

    iget v0, v1, Landroid/graphics/RectF;->right:F

    iget v2, v1, Landroid/graphics/RectF;->top:F

    invoke-direct {v4, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 63
    .local v4, "dpt2":Landroid/graphics/PointF;
    new-instance v5, Landroid/graphics/PointF;

    iget v0, v1, Landroid/graphics/RectF;->right:F

    iget v2, v1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v5, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 64
    .local v5, "dpt3":Landroid/graphics/PointF;
    new-instance v6, Landroid/graphics/PointF;

    iget v0, v1, Landroid/graphics/RectF;->left:F

    iget v2, v1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v6, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 65
    .local v6, "dpt4":Landroid/graphics/PointF;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getOriginalCenter()Landroid/graphics/PointF;

    move-result-object v2

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getAngle()I

    move-result v7

    move-object v0, p0

    .line 65
    invoke-super/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V

    .line 68
    return-void
.end method
