.class Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$36;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ColorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initGesture()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$36;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    .line 2743
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2780
    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 2747
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$36;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->isReverseAnimRunning:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$51(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2749
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$36;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$52(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Z)V

    .line 2757
    :goto_0
    return v2

    .line 2754
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$36;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    const/16 v1, 0x32

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$53(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;I)V

    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 2764
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2768
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2776
    :goto_0
    return-void

    .line 2770
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$36;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$36(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Z)V

    .line 2773
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$36;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->invalidateViews()V

    goto :goto_0

    .line 2768
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 2785
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2791
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2795
    const/4 v0, 0x0

    return v0
.end method
