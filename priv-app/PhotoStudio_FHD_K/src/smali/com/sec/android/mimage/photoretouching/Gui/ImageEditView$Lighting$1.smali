.class Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1;
.super Ljava/lang/Object;
.source "ImageEditView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->minusAlphaLighting()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1;->this$1:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    .line 548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1;->this$1:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 552
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1;->this$1:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->alpha:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;)I

    move-result v1

    if-gtz v1, :cond_1

    .line 584
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1;->this$1:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;I)V

    .line 585
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1;->this$1:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;Ljava/lang/Thread;)V

    .line 586
    return-void

    .line 554
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1;->this$1:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->alpha:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;)I

    move-result v2

    add-int/lit8 v2, v2, -0xa

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;I)V

    .line 555
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1;->this$1:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    # invokes: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->setDrawRoi()V
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;)V

    .line 557
    const-wide/16 v2, 0x14

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 561
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1;->this$1:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->access$4(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 563
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1;->this$1:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->access$4(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1$1;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 558
    :catch_0
    move-exception v0

    .line 559
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
