.class public interface abstract Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
.super Ljava/lang/Object;
.source "ImageDataInterface.java"


# virtual methods
.method public abstract destroy()V
.end method

.method public abstract getCopyToDrawCanvasRoi()Landroid/graphics/Rect;
.end method

.method public abstract getDrawCanvasRoi()Landroid/graphics/Rect;
.end method

.method public abstract getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;
.end method

.method public abstract getDrawPathList()Landroid/graphics/Path;
.end method

.method public abstract getMagnifyMaxRatio()F
.end method

.method public abstract getMagnifyMinRatio()F
.end method

.method public abstract getOrgSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;
.end method

.method public abstract getOriginalHeight()I
.end method

.method public abstract getOriginalInputBitmap()Landroid/graphics/Bitmap;
.end method

.method public abstract getOriginalInputData()[I
.end method

.method public abstract getOriginalMaskBuffer()[B
.end method

.method public abstract getOriginalMaskRoi()Landroid/graphics/Rect;
.end method

.method public abstract getOriginalPaddingX()F
.end method

.method public abstract getOriginalPaddingY()F
.end method

.method public abstract getOriginalToPreviewMatrix()Landroid/graphics/Matrix;
.end method

.method public abstract getOriginalToPreviewMatrixBasedOnViewTransform()Landroid/graphics/Matrix;
.end method

.method public abstract getOriginalToPreviewRatio()F
.end method

.method public abstract getOriginalWidth()I
.end method

.method public abstract getPath()Ljava/lang/String;
.end method

.method public abstract getPathBitmap()Landroid/graphics/Bitmap;
.end method

.method public abstract getPreviewBitmap()Landroid/graphics/Bitmap;
.end method

.method public abstract getPreviewHeight()I
.end method

.method public abstract getPreviewInputBuffer()[I
.end method

.method public abstract getPreviewMaskBuffer()[B
.end method

.method public abstract getPreviewMaskRoi()Landroid/graphics/Rect;
.end method

.method public abstract getPreviewOutputBuffer()[I
.end method

.method public abstract getPreviewPaddingX()I
.end method

.method public abstract getPreviewPaddingY()I
.end method

.method public abstract getPreviewWidth()I
.end method

.method public abstract getSupMatrix()Landroid/graphics/Matrix;
.end method

.method public abstract getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;
.end method

.method public abstract getUri()Landroid/net/Uri;
.end method

.method public abstract getViewHeight()I
.end method

.method public abstract getViewTransformMatrix()Landroid/graphics/Matrix;
.end method

.method public abstract getViewWidth()I
.end method

.method public abstract isAtLeastSaved()Z
.end method

.method public abstract isEdited()Z
.end method

.method public abstract isSaved()Z
.end method

.method public abstract setDecodedImage(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/net/Uri;I)V
.end method

.method public abstract setSupMatrix(Landroid/graphics/Matrix;)V
.end method

.method public abstract updateOriginalBuffer([I)V
.end method

.method public abstract updatePinchZoomMatrix(Landroid/graphics/Matrix;)V
.end method
