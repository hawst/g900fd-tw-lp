.class Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$48;
.super Ljava/lang/Object;
.source "NormalView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initTrayLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$48;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    .line 2744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public deleteButtonTouch(Landroid/view/View;Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$TrayDeleteFunction;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "delete"    # Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$TrayDeleteFunction;

    .prologue
    .line 2756
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$48;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2758
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$48;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2759
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$48;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    invoke-static {v0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$TrayDeleteFunction;)V

    .line 2766
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2763
    :cond_0
    invoke-interface {p2}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$TrayDeleteFunction;->delete()I

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2771
    const/4 v0, 0x0

    return v0
.end method

.method public showButtonTouch(Z)V
    .locals 1
    .param p1, "isShow"    # Z

    .prologue
    .line 2748
    if-eqz p1, :cond_0

    .line 2749
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$48;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setAllDim()V

    .line 2752
    :goto_0
    return-void

    .line 2751
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$48;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setAllNormal()V

    goto :goto_0
.end method
