.class public Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;
.super Landroid/widget/ImageView;
.source "StickerThumbnailView.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mLauncherThView:Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 13
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 9
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;->mContext:Landroid/content/Context;

    .line 10
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;->mLauncherThView:Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;

    .line 15
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;->mContext:Landroid/content/Context;

    .line 16
    iput-object p0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;->mLauncherThView:Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 9
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;->mContext:Landroid/content/Context;

    .line 10
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;->mLauncherThView:Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;

    .line 25
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;->mContext:Landroid/content/Context;

    .line 26
    iput-object p0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;->mLauncherThView:Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 9
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;->mContext:Landroid/content/Context;

    .line 10
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;->mLauncherThView:Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;

    .line 20
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;->mContext:Landroid/content/Context;

    .line 21
    iput-object p0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;->mLauncherThView:Lcom/sec/android/mimage/photoretouching/Core/StickerThumbnailView;

    .line 22
    return-void
.end method
