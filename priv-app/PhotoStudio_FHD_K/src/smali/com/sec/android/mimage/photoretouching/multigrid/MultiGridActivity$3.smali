.class Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$3;
.super Ljava/lang/Object;
.source "MultiGridActivity.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->getImage(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    .line 427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public changeViewStatus(I)V
    .locals 3
    .param p1, "viewStatus"    # I

    .prologue
    const/4 v2, 0x0

    .line 431
    if-gez p1, :cond_1

    .line 433
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mKillViewHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->access$4(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 448
    :cond_0
    :goto_0
    return-void

    .line 437
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->access$5(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->access$5(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v0

    sget v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->MIN_ITEM_CNT:I

    if-ge v0, v1, :cond_2

    .line 442
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mKillViewHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->access$4(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 446
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->access$5(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->reloadLayoutStyle()V

    goto :goto_0
.end method

.method public invalidateViews()V
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->invalidateViews()V

    .line 453
    return-void
.end method
