.class public Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;
.super Ljava/lang/Object;
.source "ColorEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;
    }
.end annotation


# static fields
.field public static final ADJUST_BLUE:I = 0x2

.field public static final ADJUST_GREEN:I = 0x1

.field public static final ADJUST_RED:I = 0x0

.field public static final ORGINALVIEW_OFF:I = 0x0

.field public static final ORGINALVIEW_ON:I = 0x1

.field public static isFirstTimeDnL:Z

.field private static myCallback:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;


# instance fields
.field private MAX_BRUSH_SIZE:I

.field private mBrushSize:I

.field private mEffectType:I

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mPaint:Landroid/graphics/Paint;

.field private mStep:I

.field private mStep_b:I

.field private mStep_g:I

.field private mStep_r:I

.field private typeRGB:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 304
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;

    .line 316
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->isFirstTimeDnL:Z

    .line 320
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 305
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 307
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mEffectType:I

    .line 308
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    .line 309
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep_r:I

    .line 310
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep_g:I

    .line 311
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep_b:I

    .line 312
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->typeRGB:I

    .line 321
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mPaint:Landroid/graphics/Paint;

    .line 323
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mBrushSize:I

    .line 324
    const/16 v0, 0x8c

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->MAX_BRUSH_SIZE:I

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 3
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 305
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 307
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mEffectType:I

    .line 308
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    .line 309
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep_r:I

    .line 310
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep_g:I

    .line 311
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep_b:I

    .line 312
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->typeRGB:I

    .line 321
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mPaint:Landroid/graphics/Paint;

    .line 323
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mBrushSize:I

    .line 324
    const/16 v0, 0x8c

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->MAX_BRUSH_SIZE:I

    .line 24
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 26
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mPaint:Landroid/graphics/Paint;

    .line 27
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 28
    return-void
.end method


# virtual methods
.method public addEvent(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 269
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->applyPreview(I)I

    .line 270
    return-void
.end method

.method public applyOriginal()[I
    .locals 18

    .prologue
    .line 54
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v6, :cond_3

    .line 56
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v2

    .line 57
    .local v2, "mask":[B
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v1

    .line 58
    .local v1, "input":[I
    const/4 v3, 0x0

    .line 59
    .local v3, "output":[I
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v4

    .line 60
    .local v4, "w":I
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v5

    .line 61
    .local v5, "h":I
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v17

    .line 63
    .local v17, "rect":Landroid/graphics/Rect;
    mul-int v6, v4, v5

    new-array v3, v6, [I

    .line 66
    new-instance v7, Landroid/graphics/Rect;

    move-object/from16 v0, v17

    invoke-direct {v7, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 68
    .local v7, "r":Landroid/graphics/Rect;
    invoke-virtual {v7}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 69
    const/16 v6, 0x64

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v6, v8, v9, v10}, Landroid/graphics/Rect;->set(IIII)V

    .line 71
    :cond_0
    iget v6, v7, Landroid/graphics/Rect;->right:I

    add-int/lit8 v8, v4, -0x1

    if-le v6, v8, :cond_1

    .line 72
    add-int/lit8 v6, v4, -0x1

    iput v6, v7, Landroid/graphics/Rect;->right:I

    .line 73
    :cond_1
    iget v6, v7, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v8, v5, -0x1

    if-le v6, v8, :cond_2

    .line 74
    add-int/lit8 v6, v5, -0x1

    iput v6, v7, Landroid/graphics/Rect;->bottom:I

    .line 77
    :cond_2
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mEffectType:I

    sparse-switch v6, :sswitch_data_0

    .line 104
    :goto_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([I)V

    .line 105
    const/4 v3, 0x0

    .line 107
    const-string v6, "applyOriginal end"

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 110
    .end local v1    # "input":[I
    .end local v2    # "mask":[B
    .end local v3    # "output":[I
    .end local v4    # "w":I
    .end local v5    # "h":I
    .end local v7    # "r":Landroid/graphics/Rect;
    .end local v17    # "rect":Landroid/graphics/Rect;
    :goto_1
    return-object v1

    .line 79
    .restart local v1    # "input":[I
    .restart local v2    # "mask":[B
    .restart local v3    # "output":[I
    .restart local v4    # "w":I
    .restart local v5    # "h":I
    .restart local v7    # "r":Landroid/graphics/Rect;
    .restart local v17    # "rect":Landroid/graphics/Rect;
    :sswitch_0
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    invoke-static/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlBrightnessRange100([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 82
    :sswitch_1
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    invoke-static/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlContrastRange100([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 85
    :sswitch_2
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    invoke-static/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlSaturationRange100([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 88
    :sswitch_3
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    invoke-static/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlHueRange100([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 91
    :sswitch_4
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    invoke-static/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlTemperatureRange100([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 94
    :sswitch_5
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v8, v1

    move-object v9, v2

    move-object v10, v3

    move v11, v4

    move v12, v5

    move-object/from16 v16, v7

    invoke-static/range {v8 .. v16}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->adjustRGBRange100([I[B[IIIIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 97
    :sswitch_6
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    const/4 v15, 0x0

    move-object v8, v1

    move-object v9, v2

    move-object v10, v3

    move v11, v4

    move v12, v5

    move-object/from16 v16, v7

    invoke-static/range {v8 .. v16}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->adjustRGBRange100([I[B[IIIIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 100
    :sswitch_7
    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    move-object v8, v1

    move-object v9, v2

    move-object v10, v3

    move v11, v4

    move v12, v5

    move-object/from16 v16, v7

    invoke-static/range {v8 .. v16}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->adjustRGBRange100([I[B[IIIIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 110
    .end local v1    # "input":[I
    .end local v2    # "mask":[B
    .end local v3    # "output":[I
    .end local v4    # "w":I
    .end local v5    # "h":I
    .end local v7    # "r":Landroid/graphics/Rect;
    .end local v17    # "rect":Landroid/graphics/Rect;
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 77
    :sswitch_data_0
    .sparse-switch
        0x15001500 -> :sswitch_0
        0x15001501 -> :sswitch_1
        0x15001502 -> :sswitch_2
        0x15001504 -> :sswitch_3
        0x15001505 -> :sswitch_4
        0x15001607 -> :sswitch_5
        0x15001608 -> :sswitch_6
        0x15001609 -> :sswitch_7
    .end sparse-switch
.end method

.method public declared-synchronized applyPreview(I)I
    .locals 19
    .param p1, "step"    # I

    .prologue
    .line 115
    monitor-enter p0

    const/16 v18, -0x1

    .line 116
    .local v18, "ret":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v7, :cond_1

    .line 119
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    .line 121
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskBuffer()[B

    move-result-object v3

    .line 122
    .local v3, "previewMask":[B
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    .line 123
    .local v2, "previewInput":[I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v4

    .line 125
    .local v4, "previewOutput":[I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    .line 126
    .local v5, "previewWidth":I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    .line 127
    .local v6, "previewHeight":I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v8

    .line 128
    .local v8, "r":Landroid/graphics/Rect;
    invoke-virtual {v8}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 129
    new-instance v8, Landroid/graphics/Rect;

    .end local v8    # "r":Landroid/graphics/Rect;
    const/16 v7, 0x64

    const/16 v9, 0x64

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v8, v7, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 130
    .restart local v8    # "r":Landroid/graphics/Rect;
    :cond_0
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mEffectType:I

    sparse-switch v7, :sswitch_data_0

    .line 157
    :goto_0
    sget-object v7, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;

    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;->afterApplyPreview()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    .end local v2    # "previewInput":[I
    .end local v3    # "previewMask":[B
    .end local v4    # "previewOutput":[I
    .end local v5    # "previewWidth":I
    .end local v6    # "previewHeight":I
    .end local v8    # "r":Landroid/graphics/Rect;
    :cond_1
    monitor-exit p0

    return v18

    .line 132
    .restart local v2    # "previewInput":[I
    .restart local v3    # "previewMask":[B
    .restart local v4    # "previewOutput":[I
    .restart local v5    # "previewWidth":I
    .restart local v6    # "previewHeight":I
    .restart local v8    # "r":Landroid/graphics/Rect;
    :sswitch_0
    :try_start_1
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    invoke-static/range {v2 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlBrightnessRange100([I[B[IIIILandroid/graphics/Rect;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 115
    .end local v2    # "previewInput":[I
    .end local v3    # "previewMask":[B
    .end local v4    # "previewOutput":[I
    .end local v5    # "previewWidth":I
    .end local v6    # "previewHeight":I
    .end local v8    # "r":Landroid/graphics/Rect;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 135
    .restart local v2    # "previewInput":[I
    .restart local v3    # "previewMask":[B
    .restart local v4    # "previewOutput":[I
    .restart local v5    # "previewWidth":I
    .restart local v6    # "previewHeight":I
    .restart local v8    # "r":Landroid/graphics/Rect;
    :sswitch_1
    :try_start_2
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    invoke-static/range {v2 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlContrastRange100([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 138
    :sswitch_2
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    invoke-static/range {v2 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlSaturationRange100([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 141
    :sswitch_3
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    invoke-static/range {v2 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlHueRange100([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 144
    :sswitch_4
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    invoke-static/range {v2 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlTemperatureRange100([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 147
    :sswitch_5
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v9, v2

    move-object v10, v3

    move-object v11, v4

    move v12, v5

    move v13, v6

    move-object/from16 v17, v8

    invoke-static/range {v9 .. v17}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->adjustRGBRange100([I[B[IIIIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 150
    :sswitch_6
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    const/16 v16, 0x0

    move-object v9, v2

    move-object v10, v3

    move-object v11, v4

    move v12, v5

    move v13, v6

    move-object/from16 v17, v8

    invoke-static/range {v9 .. v17}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->adjustRGBRange100([I[B[IIIIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 153
    :sswitch_7
    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    move/from16 v16, v0

    move-object v9, v2

    move-object v10, v3

    move-object v11, v4

    move v12, v5

    move v13, v6

    move-object/from16 v17, v8

    invoke-static/range {v9 .. v17}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->adjustRGBRange100([I[B[IIIIIILandroid/graphics/Rect;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 130
    :sswitch_data_0
    .sparse-switch
        0x15001500 -> :sswitch_0
        0x15001501 -> :sswitch_1
        0x15001502 -> :sswitch_2
        0x15001504 -> :sswitch_3
        0x15001505 -> :sswitch_4
        0x15001607 -> :sswitch_5
        0x15001608 -> :sswitch_6
        0x15001609 -> :sswitch_7
    .end sparse-switch
.end method

.method public declared-synchronized applyPreview(I[I[I[BIILandroid/graphics/Rect;)I
    .locals 18
    .param p1, "step"    # I
    .param p2, "previewInput"    # [I
    .param p3, "previewOutput"    # [I
    .param p4, "previewMask"    # [B
    .param p5, "previewWidth"    # I
    .param p6, "previewHeight"    # I
    .param p7, "roi"    # Landroid/graphics/Rect;

    .prologue
    .line 165
    monitor-enter p0

    const/16 v17, -0x1

    .line 166
    .local v17, "ret":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_1

    .line 168
    move-object/from16 v7, p7

    .line 169
    .local v7, "r":Landroid/graphics/Rect;
    invoke-virtual {v7}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    new-instance v7, Landroid/graphics/Rect;

    .end local v7    # "r":Landroid/graphics/Rect;
    const/16 v1, 0x64

    const/16 v2, 0x64

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v7, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 171
    .restart local v7    # "r":Landroid/graphics/Rect;
    :cond_0
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mEffectType:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sparse-switch v1, :sswitch_data_0

    .line 201
    .end local v7    # "r":Landroid/graphics/Rect;
    :cond_1
    :goto_0
    monitor-exit p0

    return v17

    .restart local v7    # "r":Landroid/graphics/Rect;
    :sswitch_0
    move-object/from16 v1, p2

    move-object/from16 v2, p4

    move-object/from16 v3, p3

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p1

    .line 173
    :try_start_1
    invoke-static/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlBrightnessRange100([I[B[IIIILandroid/graphics/Rect;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 165
    .end local v7    # "r":Landroid/graphics/Rect;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .restart local v7    # "r":Landroid/graphics/Rect;
    :sswitch_1
    move-object/from16 v1, p2

    move-object/from16 v2, p4

    move-object/from16 v3, p3

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p1

    .line 176
    :try_start_2
    invoke-static/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlContrastRange100([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    :sswitch_2
    move-object/from16 v1, p2

    move-object/from16 v2, p4

    move-object/from16 v3, p3

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p1

    .line 179
    invoke-static/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlSaturationRange100([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    :sswitch_3
    move-object/from16 v1, p2

    move-object/from16 v2, p4

    move-object/from16 v3, p3

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p1

    .line 182
    invoke-static/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlHueRange100([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    :sswitch_4
    move-object/from16 v1, p2

    move-object/from16 v2, p4

    move-object/from16 v3, p3

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p1

    .line 185
    invoke-static/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlTemperatureRange100([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 188
    :sswitch_5
    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v8, p2

    move-object/from16 v9, p4

    move-object/from16 v10, p3

    move/from16 v11, p5

    move/from16 v12, p6

    move/from16 v13, p1

    move-object/from16 v16, v7

    invoke-static/range {v8 .. v16}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->adjustRGBRange100([I[B[IIIIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 191
    :sswitch_6
    const/4 v13, 0x0

    const/4 v15, 0x0

    move-object/from16 v8, p2

    move-object/from16 v9, p4

    move-object/from16 v10, p3

    move/from16 v11, p5

    move/from16 v12, p6

    move/from16 v14, p1

    move-object/from16 v16, v7

    invoke-static/range {v8 .. v16}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->adjustRGBRange100([I[B[IIIIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 194
    :sswitch_7
    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v8, p2

    move-object/from16 v9, p4

    move-object/from16 v10, p3

    move/from16 v11, p5

    move/from16 v12, p6

    move/from16 v15, p1

    move-object/from16 v16, v7

    invoke-static/range {v8 .. v16}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->adjustRGBRange100([I[B[IIIIIILandroid/graphics/Rect;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 171
    nop

    :sswitch_data_0
    .sparse-switch
        0x15001500 -> :sswitch_0
        0x15001501 -> :sswitch_1
        0x15001502 -> :sswitch_2
        0x15001504 -> :sswitch_3
        0x15001505 -> :sswitch_4
        0x15001607 -> :sswitch_5
        0x15001608 -> :sswitch_6
        0x15001609 -> :sswitch_7
    .end sparse-switch
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;)V
    .locals 1
    .param p1, "colorEffect"    # Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    .prologue
    .line 31
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 32
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    .line 33
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mEffectType:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mEffectType:I

    .line 34
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mPaint:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mPaint:Landroid/graphics/Paint;

    .line 35
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 39
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mPaint:Landroid/graphics/Paint;

    .line 40
    return-void
.end method

.method public getBrushSize()I
    .locals 1

    .prologue
    .line 274
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mBrushSize:I

    return v0
.end method

.method public getCurrentBStep()I
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep_b:I

    return v0
.end method

.method public getCurrentGStep()I
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep_g:I

    return v0
.end method

.method public getCurrentRGBType()I
    .locals 1

    .prologue
    .line 264
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->typeRGB:I

    return v0
.end method

.method public getCurrentRStep()I
    .locals 1

    .prologue
    .line 252
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep_r:I

    return v0
.end method

.method public getCurrentStep()I
    .locals 1

    .prologue
    .line 248
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    return v0
.end method

.method public getEffectType()I
    .locals 1

    .prologue
    .line 282
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mEffectType:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 301
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMaxBrushSize()I
    .locals 1

    .prologue
    .line 278
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->MAX_BRUSH_SIZE:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 296
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(I)V
    .locals 4
    .param p1, "effectType"    # I

    .prologue
    const/4 v3, 0x0

    .line 44
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mEffectType:I

    .line 45
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 46
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 47
    .local v0, "previewInput":[I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 49
    .local v1, "previewOutput":[I
    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 50
    return-void
.end method

.method public onConfigurationChanged()V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public setBrushSize(I)V
    .locals 2
    .param p1, "brushsize"    # I

    .prologue
    .line 286
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mBrushSize:I

    .line 287
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mBrushSize:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mBrushSize:I

    div-int/lit8 v1, v1, 0x8

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/jni/Util;->set_brush_size(II)V

    .line 288
    return-void
.end method

.method public setOnCallback(Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;

    .prologue
    .line 291
    sput-object p1, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;

    .line 292
    return-void
.end method

.method public setRGBInitValue(III)V
    .locals 0
    .param p1, "R"    # I
    .param p2, "G"    # I
    .param p3, "B"    # I

    .prologue
    .line 241
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep_r:I

    .line 242
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep_g:I

    .line 243
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep_b:I

    .line 244
    return-void
.end method

.method public setTypeRGB(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 237
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->typeRGB:I

    .line 238
    return-void
.end method

.method public showOrginalView(I)V
    .locals 6
    .param p1, "state"    # I

    .prologue
    const/4 v5, 0x0

    .line 206
    if-nez p1, :cond_1

    .line 208
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mStep:I

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->applyPreview(I)I

    .line 209
    sget-object v4, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;

    if-eqz v4, :cond_0

    .line 211
    sget-object v4, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;->invalidate()V

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    const/4 v4, 0x1

    if-ne p1, v4, :cond_0

    .line 216
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 217
    .local v1, "previewInput":[I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v2

    .line 219
    .local v2, "previewOutput":[I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 220
    .local v3, "previewWidth":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    .line 222
    .local v0, "previewHeight":I
    mul-int v4, v3, v0

    invoke-static {v1, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 224
    sget-object v4, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;

    if-eqz v4, :cond_0

    .line 226
    sget-object v4, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect$OnCallback;->invalidate()V

    goto :goto_0
.end method
