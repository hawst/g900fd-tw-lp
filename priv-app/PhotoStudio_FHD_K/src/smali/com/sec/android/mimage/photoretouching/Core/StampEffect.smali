.class public Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
.super Ljava/lang/Object;
.source "StampEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/StampEffect$OnMyStampCallback;
    }
.end annotation


# static fields
.field public static final DUPLICATED_BACKGROUND:I = 0x0

.field public static final EXIF_CAMERA_EXPOSURE_PREFIX:Ljava/lang/String; = "exposure:"

.field public static final EXIF_CAMERA_EXPOSURE_TIME:I = 0x2

.field public static final EXIF_CAMERA_FNUMBER:I = 0x3

.field public static final EXIF_CAMERA_FNUMBER_PREFIX:Ljava/lang/String; = "fnumber:"

.field public static final EXIF_CAMERA_ISO:I = 0x1

.field public static final EXIF_CAMERA_ISO_PREFIX:Ljava/lang/String; = "iso:"

.field public static final EXIF_LOC_FEATURE_PREFIX:Ljava/lang/String; = "feature:"

.field public static final EXIF_LOC_LOCALITY_PREFIX:Ljava/lang/String; = "locality:"

.field public static final EXIF_LOC_SUBLOCALITY_PREFIX:Ljava/lang/String; = "sublocality:"

.field public static final EXIF_LOC_THOROUGHFARE_PREFIX:Ljava/lang/String; = "thoroughfare:"

.field public static final FIFTH_STAMP:I = 0x5

.field public static final FIRST_STAMP:I = 0x1

.field public static final FOURTH_STAMP:I = 0x4

.field public static final MATCH_PARENT:I = -0x1

.field public static final NON_BACKGROUND:I = 0x1

.field public static final ORIGINAL:I = 0x2

.field public static final PREVIEW:I = 0x1

.field public static final SECOND_STAMP:I = 0x2

.field public static final THIRD_STAMP:I = 0x3

.field public static final THUMBNAIL:I


# instance fields
.field i:I

.field private mBaseH:I

.field private mBaseW:I

.field private mBottomAlign:Z

.field private mCityIsMaster:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentLinearLayout:Landroid/widget/LinearLayout;

.field public mDone:Z

.field private mExifCameraInfo:Ljava/lang/String;

.field private mExifInfo:Ljava/lang/String;

.field private mExifLocationCityInfo:Ljava/lang/String;

.field private mExifLocationThroughfareInfo:Ljava/lang/String;

.field private mFlipperLayout:Landroid/widget/FrameLayout;

.field private mId:I

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mInitialPos:I

.field private mMiddleLayer:Landroid/widget/LinearLayout;

.field private mMimimunTextSize:F

.field private mOriginalStampLayer:Landroid/widget/FrameLayout;

.field private mOriginalStampLayer2:Landroid/widget/FrameLayout;

.field private mOriginalStampLayer3:Landroid/widget/FrameLayout;

.field private mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

.field private mPaint:Landroid/graphics/Paint;

.field private mResizeRatio:F

.field private mResizedStampLayer:Landroid/widget/FrameLayout;

.field private mResizedStampLayer2:Landroid/widget/FrameLayout;

.field private mResizedStampLayer3:Landroid/widget/FrameLayout;

.field private mStampLayer:Landroid/widget/FrameLayout;

.field private mStampLayer2:Landroid/widget/FrameLayout;

.field private mStampLayer3:Landroid/widget/FrameLayout;

.field private mStampParentLayer:Landroid/widget/RelativeLayout;

.field private mStart:Z

.field private mTempBitmap:Landroid/graphics/Bitmap;

.field private mThroughfareIsMaster:Z

.field private mViewPager:Landroid/support/v4/view/ViewPager;

.field private mViewPagerParent:Landroid/widget/FrameLayout;

.field private myCallback:Lcom/sec/android/mimage/photoretouching/Core/StampEffect$OnMyStampCallback;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4689
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->i:I

    .line 7984
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    .line 7985
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 7986
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    .line 8015
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    .line 8016
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    .line 8017
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    .line 8018
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    .line 8019
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    .line 8020
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    .line 8021
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    .line 8022
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    .line 8023
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    .line 8024
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    .line 8025
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    .line 8027
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    .line 8028
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    .line 8031
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    .line 8032
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mDone:Z

    .line 8033
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/StampEffect$OnMyStampCallback;

    .line 8034
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mPaint:Landroid/graphics/Paint;

    .line 8035
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 8037
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mViewPagerParent:Landroid/widget/FrameLayout;

    .line 8038
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mFlipperLayout:Landroid/widget/FrameLayout;

    .line 8039
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 8040
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 8041
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    .line 8042
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mInitialPos:I

    .line 8043
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStart:Z

    .line 8048
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    .line 8049
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationThroughfareInfo:Ljava/lang/String;

    .line 8050
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    .line 8051
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    .line 8052
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    .line 8053
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    .line 8054
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    .line 8056
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMimimunTextSize:F

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4689
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->i:I

    .line 7984
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    .line 7985
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 7986
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    .line 8015
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    .line 8016
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    .line 8017
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    .line 8018
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    .line 8019
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    .line 8020
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    .line 8021
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    .line 8022
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    .line 8023
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    .line 8024
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    .line 8025
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    .line 8027
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    .line 8028
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    .line 8031
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    .line 8032
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mDone:Z

    .line 8033
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/StampEffect$OnMyStampCallback;

    .line 8034
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mPaint:Landroid/graphics/Paint;

    .line 8035
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 8037
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mViewPagerParent:Landroid/widget/FrameLayout;

    .line 8038
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mFlipperLayout:Landroid/widget/FrameLayout;

    .line 8039
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 8040
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 8041
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    .line 8042
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mInitialPos:I

    .line 8043
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStart:Z

    .line 8048
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    .line 8049
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationThroughfareInfo:Ljava/lang/String;

    .line 8050
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    .line 8051
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    .line 8052
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    .line 8053
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    .line 8054
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    .line 8056
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMimimunTextSize:F

    .line 55
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 56
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    .line 58
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mPaint:Landroid/graphics/Paint;

    .line 59
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setDither(Z)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050040

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMimimunTextSize:F

    .line 64
    return-void
.end method

.method private getToday()Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 1039
    const/4 v8, 0x0

    .line 1040
    .local v8, "today":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1041
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, v11}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 1042
    .local v2, "curYear":I
    const/4 v9, 0x2

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 1043
    .local v3, "curtMonth":Ljava/lang/String;
    const/4 v9, 0x5

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 1044
    .local v1, "curDay":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    .line 1045
    .local v4, "date":Ljava/util/Date;
    invoke-virtual {v4}, Ljava/util/Date;->getHours()I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 1046
    .local v5, "hour":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/Date;->getMinutes()I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 1047
    .local v6, "minute":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/Date;->getSeconds()I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .line 1049
    .local v7, "seconds":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    if-ne v9, v11, :cond_0

    .line 1050
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1051
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    if-ne v9, v11, :cond_1

    .line 1052
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1053
    :cond_1
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    if-ne v9, v11, :cond_2

    .line 1054
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1055
    :cond_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-ne v9, v11, :cond_3

    .line 1056
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1057
    :cond_3
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-ne v9, v11, :cond_4

    .line 1058
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "0"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1060
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1061
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "JW getToday: today="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1062
    return-object v8
.end method

.method private makeStamp(IIZ)V
    .locals 28
    .param p1, "type"    # I
    .param p2, "id"    # I
    .param p3, "isPageNum"    # Z

    .prologue
    .line 4692
    if-eqz p3, :cond_0

    .line 4694
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    const v3, 0x3150008b

    if-lt v2, v3, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    const v3, 0x3150009f

    if-gt v2, v3, :cond_1

    .line 4696
    packed-switch p2, :pswitch_data_0

    .line 4813
    :cond_0
    :goto_0
    packed-switch p2, :pswitch_data_1

    .line 7595
    :goto_1
    return-void

    .line 4699
    :pswitch_0
    const p2, 0x3150008b

    .line 4700
    goto :goto_0

    .line 4702
    :pswitch_1
    const p2, 0x3150008c

    .line 4703
    goto :goto_0

    .line 4705
    :pswitch_2
    const p2, 0x3150008d

    .line 4706
    goto :goto_0

    .line 4708
    :pswitch_3
    const p2, 0x3150008e

    .line 4709
    goto :goto_0

    .line 4711
    :pswitch_4
    const p2, 0x3150008f

    .line 4712
    goto :goto_0

    .line 4714
    :pswitch_5
    const p2, 0x31500090

    .line 4715
    goto :goto_0

    .line 4717
    :pswitch_6
    const p2, 0x31500091

    .line 4718
    goto :goto_0

    .line 4720
    :pswitch_7
    const p2, 0x31500092

    .line 4721
    goto :goto_0

    .line 4723
    :pswitch_8
    const p2, 0x31500093

    .line 4724
    goto :goto_0

    .line 4726
    :pswitch_9
    const p2, 0x31500094

    .line 4727
    goto :goto_0

    .line 4729
    :pswitch_a
    const p2, 0x31500095

    .line 4730
    goto :goto_0

    .line 4732
    :pswitch_b
    const p2, 0x31500096

    .line 4733
    goto :goto_0

    .line 4735
    :pswitch_c
    const p2, 0x31500097

    .line 4736
    goto :goto_0

    .line 4738
    :pswitch_d
    const p2, 0x31500098

    .line 4739
    goto :goto_0

    .line 4741
    :pswitch_e
    const p2, 0x31500099

    .line 4742
    goto :goto_0

    .line 4744
    :pswitch_f
    const p2, 0x3150009a

    .line 4745
    goto :goto_0

    .line 4747
    :pswitch_10
    const p2, 0x3150009b

    .line 4748
    goto :goto_0

    .line 4750
    :pswitch_11
    const p2, 0x3150009c

    .line 4751
    goto :goto_0

    .line 4753
    :pswitch_12
    const p2, 0x3150009d

    .line 4754
    goto :goto_0

    .line 4756
    :pswitch_13
    const p2, 0x3150009e

    .line 4757
    goto :goto_0

    .line 4759
    :pswitch_14
    const p2, 0x3150009f

    goto :goto_0

    .line 4763
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    const v3, 0x315000a0

    if-lt v2, v3, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    const v3, 0x315000ac

    if-gt v2, v3, :cond_0

    .line 4765
    packed-switch p2, :pswitch_data_2

    goto :goto_0

    .line 4768
    :pswitch_15
    const p2, 0x315000a0

    .line 4769
    goto :goto_0

    .line 4771
    :pswitch_16
    const p2, 0x315000a1

    .line 4772
    goto :goto_0

    .line 4774
    :pswitch_17
    const p2, 0x315000a2

    .line 4775
    goto :goto_0

    .line 4777
    :pswitch_18
    const p2, 0x315000a3

    .line 4778
    goto :goto_0

    .line 4780
    :pswitch_19
    const p2, 0x315000a4

    .line 4781
    goto/16 :goto_0

    .line 4783
    :pswitch_1a
    const p2, 0x315000a5

    .line 4784
    goto/16 :goto_0

    .line 4786
    :pswitch_1b
    const p2, 0x315000a6

    .line 4787
    goto/16 :goto_0

    .line 4789
    :pswitch_1c
    const p2, 0x315000a7

    .line 4790
    goto/16 :goto_0

    .line 4792
    :pswitch_1d
    const p2, 0x315000a8

    .line 4793
    goto/16 :goto_0

    .line 4795
    :pswitch_1e
    const p2, 0x315000a9

    .line 4796
    goto/16 :goto_0

    .line 4798
    :pswitch_1f
    const p2, 0x315000aa

    .line 4799
    goto/16 :goto_0

    .line 4801
    :pswitch_20
    const p2, 0x315000ab

    .line 4802
    goto/16 :goto_0

    .line 4804
    :pswitch_21
    const p2, 0x315000ac

    goto/16 :goto_0

    .line 4817
    :pswitch_22
    const v4, 0x7f020189

    .line 4818
    const/4 v5, 0x1

    .line 4819
    const/4 v6, 0x1

    .line 4820
    const/16 v7, 0x30

    .line 4821
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050043

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 4822
    const/4 v9, -0x1

    .line 4823
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050041

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 4824
    const/4 v11, 0x0

    .line 4825
    const/16 v12, 0x11

    .line 4826
    const/4 v13, 0x0

    .line 4827
    const/16 v14, 0xa

    .line 4828
    const/16 v15, 0xe

    .line 4829
    const/16 v16, 0x0

    .line 4830
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050045

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 4831
    const/16 v18, 0x0

    .line 4832
    const/16 v19, 0x0

    .line 4833
    const/16 v20, 0x0

    .line 4834
    const/16 v21, 0x0

    .line 4835
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 4836
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 4837
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 4838
    const/16 v25, 0x0

    .line 4839
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 4816
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 4842
    const/4 v4, 0x1

    .line 4843
    const/4 v5, 0x2

    .line 4844
    const/4 v6, 0x1

    .line 4845
    const/16 v7, 0x50

    .line 4846
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050044

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 4847
    const/4 v9, -0x1

    .line 4848
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050042

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 4849
    const/4 v11, 0x0

    .line 4850
    const/16 v12, 0x11

    .line 4851
    const/4 v13, 0x0

    .line 4852
    const/16 v14, 0xc

    .line 4853
    const/16 v15, 0xe

    .line 4854
    const/16 v16, 0x0

    .line 4855
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050046

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 4856
    const/16 v18, 0x0

    .line 4857
    const/16 v19, 0x0

    .line 4858
    const/16 v20, 0x0

    .line 4859
    const/16 v21, 0x0

    .line 4860
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 4861
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 4862
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/4 v3, 0x1

    const/16 v24, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 4863
    const/16 v25, 0x0

    .line 4864
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 4841
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 4868
    :pswitch_23
    const/4 v4, 0x1

    .line 4869
    const/4 v5, 0x1

    .line 4870
    const/4 v6, 0x1

    .line 4871
    const/16 v7, 0x30

    .line 4872
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050049

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 4873
    const/4 v9, -0x1

    .line 4874
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050047

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 4875
    const/4 v11, 0x0

    .line 4876
    const/4 v12, 0x1

    .line 4877
    const/4 v13, 0x0

    .line 4878
    const/4 v14, 0x0

    .line 4879
    const/16 v15, 0xd

    .line 4880
    const/16 v16, 0x0

    .line 4881
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05004b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 4882
    const/16 v18, 0x0

    .line 4883
    const/16 v19, 0x0

    .line 4884
    const/16 v20, 0x0

    .line 4885
    const/16 v21, 0x0

    .line 4886
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 4887
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 4888
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 4889
    const/16 v25, 0x0

    .line 4890
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 4867
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 4893
    const/4 v4, 0x0

    .line 4894
    const/4 v5, 0x1

    .line 4895
    const/4 v6, 0x1

    .line 4896
    const/16 v7, 0x50

    .line 4897
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05004a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 4898
    const/4 v9, -0x1

    .line 4899
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050048

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 4900
    const/4 v11, 0x0

    .line 4901
    const/4 v12, 0x1

    .line 4902
    const/4 v13, 0x0

    .line 4903
    const/4 v14, 0x0

    .line 4904
    const/16 v15, 0xd

    .line 4905
    const/16 v16, 0x0

    .line 4906
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05004c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 4907
    const/16 v18, 0x0

    .line 4908
    const/16 v19, 0x0

    .line 4909
    const/16 v20, 0x0

    .line 4910
    const/16 v21, 0x0

    .line 4911
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 4912
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 4913
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/4 v3, 0x2

    const/16 v24, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 4914
    const/16 v25, 0x0

    .line 4915
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 4892
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 4919
    :pswitch_24
    const/4 v4, 0x1

    .line 4920
    const/4 v5, 0x1

    .line 4921
    const/4 v6, 0x1

    .line 4922
    const/16 v7, 0x30

    .line 4923
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050051

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 4924
    const/4 v9, -0x1

    .line 4925
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05004d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 4926
    const/4 v11, 0x0

    .line 4927
    const/4 v12, 0x1

    .line 4928
    const/4 v13, 0x0

    .line 4929
    const/16 v14, 0xa

    .line 4930
    const/16 v15, 0xe

    .line 4931
    const/16 v16, 0x0

    .line 4932
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050055

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 4933
    const/16 v18, 0x0

    .line 4934
    const/16 v19, 0x0

    .line 4935
    const/16 v20, 0x0

    .line 4936
    const/16 v21, 0x0

    .line 4937
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 4938
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 4939
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/4 v3, 0x3

    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 4940
    const/16 v25, 0x0

    .line 4941
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 4918
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 4944
    const/4 v4, 0x1

    .line 4945
    const/4 v5, 0x2

    .line 4946
    const/4 v6, 0x1

    .line 4947
    const/16 v7, 0x30

    .line 4948
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050052

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 4949
    const/4 v9, -0x1

    .line 4950
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05004e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 4951
    const/4 v11, 0x0

    .line 4952
    const/4 v12, 0x1

    .line 4953
    const/4 v13, 0x0

    .line 4954
    const/16 v14, 0xd

    .line 4955
    const/16 v15, 0xe

    .line 4956
    const/16 v16, 0x0

    .line 4957
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050056

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 4958
    const/16 v18, 0x0

    .line 4959
    const/16 v19, 0x0

    .line 4960
    const/16 v20, 0x0

    .line 4961
    const/16 v21, 0x0

    .line 4962
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 4963
    const-string v2, "sans-serif-condensed"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 4964
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/4 v3, 0x3

    const/16 v24, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 4965
    const/16 v25, 0x0

    .line 4966
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 4943
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 4969
    const/4 v4, 0x0

    .line 4970
    const/4 v5, 0x2

    .line 4971
    const/4 v6, 0x1

    .line 4972
    const/16 v7, 0x50

    .line 4973
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050053

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 4974
    const/4 v9, -0x1

    .line 4975
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05004f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 4976
    const/4 v11, 0x0

    .line 4977
    const/4 v12, 0x1

    .line 4978
    const/4 v13, 0x0

    .line 4979
    const/16 v14, 0xd

    .line 4980
    const/16 v15, 0xe

    .line 4981
    const/16 v16, 0x0

    .line 4982
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050057

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 4983
    const/16 v18, 0x0

    .line 4984
    const/16 v19, 0x0

    .line 4985
    const/16 v20, 0x0

    .line 4986
    const/16 v21, 0x0

    .line 4987
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 4988
    const-string v2, "sans-serif-condensed"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 4989
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/4 v3, 0x3

    const/16 v24, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 4990
    const/16 v25, 0x0

    .line 4991
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 4968
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 4994
    const/4 v4, 0x1

    .line 4995
    const/4 v5, 0x3

    .line 4996
    const/4 v6, 0x1

    .line 4997
    const/16 v7, 0x50

    .line 4998
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050054

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 4999
    const/4 v9, -0x1

    .line 5000
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050050

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5001
    const/4 v11, 0x0

    .line 5002
    const/4 v12, 0x1

    .line 5003
    const/4 v13, 0x0

    .line 5004
    const/16 v14, 0xc

    .line 5005
    const/16 v15, 0xe

    .line 5006
    const/16 v16, 0x0

    .line 5007
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050058

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5008
    const/16 v18, 0x0

    .line 5009
    const/16 v19, 0x0

    .line 5010
    const/16 v20, 0x0

    .line 5011
    const/16 v21, 0x0

    .line 5012
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5013
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5014
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 5015
    const/16 v25, 0x0

    .line 5016
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 4993
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 5021
    :pswitch_25
    const v4, 0x7f02018b

    .line 5022
    const/4 v5, 0x1

    .line 5023
    const/4 v6, 0x1

    .line 5024
    const/16 v7, 0x30

    .line 5025
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05005d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5026
    const/4 v9, -0x1

    .line 5027
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050059

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5028
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05005a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 5029
    const/4 v12, 0x1

    .line 5030
    const/4 v13, 0x0

    .line 5031
    const/16 v14, 0xd

    .line 5032
    const/16 v15, 0xe

    .line 5033
    const/16 v16, 0x0

    .line 5034
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050061

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5035
    const/16 v18, 0x0

    .line 5036
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050065

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 5037
    const v20, 0x7f02032e

    .line 5038
    const/16 v21, 0xe

    .line 5039
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5040
    const-string v2, "sans-serif-thin"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5041
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 5042
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    move/from16 v25, v0

    .line 5043
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5020
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5046
    const/4 v4, 0x0

    .line 5047
    const/4 v5, 0x1

    .line 5048
    const/4 v6, 0x1

    .line 5049
    const/16 v7, 0x30

    .line 5050
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05005e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5051
    const/4 v9, -0x1

    .line 5052
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05005a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5053
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050059

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 5054
    const/4 v12, 0x1

    .line 5055
    const/4 v13, 0x0

    .line 5056
    const/16 v14, 0xd

    .line 5057
    const/16 v15, 0xe

    .line 5058
    const/16 v16, 0x0

    .line 5059
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050062

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5060
    const/16 v18, 0x0

    .line 5061
    const/16 v19, 0x0

    .line 5062
    const/16 v20, 0x0

    .line 5063
    const/16 v21, 0x0

    .line 5064
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5065
    const-string v2, "sans-serif-thin"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5066
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationThroughfareInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 5067
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    move/from16 v25, v0

    .line 5068
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5045
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5071
    const/4 v4, 0x0

    .line 5072
    const/4 v5, 0x1

    .line 5073
    const/4 v6, 0x1

    .line 5074
    const/16 v7, 0x50

    .line 5075
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05005f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5076
    const/4 v9, -0x1

    .line 5077
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05005b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5078
    const/4 v11, 0x0

    .line 5079
    const/4 v12, 0x1

    .line 5080
    const/4 v13, 0x0

    .line 5081
    const/16 v14, 0xd

    .line 5082
    const/16 v15, 0xe

    .line 5083
    const/16 v16, 0x0

    .line 5084
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5085
    const/16 v18, 0x0

    .line 5086
    const/16 v19, 0x0

    .line 5087
    const/16 v20, 0x0

    .line 5088
    const/16 v21, 0x0

    .line 5089
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5090
    const-string v2, "sans-serif-thin"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5091
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/4 v3, 0x4

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5092
    const/16 v25, 0x0

    .line 5093
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5070
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5096
    const/4 v4, 0x0

    .line 5097
    const/4 v5, 0x1

    .line 5098
    const/4 v6, 0x1

    .line 5099
    const/16 v7, 0x50

    .line 5100
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050060

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5101
    const/4 v9, -0x1

    .line 5102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05005c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5103
    const/4 v11, 0x0

    .line 5104
    const/4 v12, 0x1

    .line 5105
    const/4 v13, 0x0

    .line 5106
    const/16 v14, 0xd

    .line 5107
    const/16 v15, 0xe

    .line 5108
    const/16 v16, 0x0

    .line 5109
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050064

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5110
    const/16 v18, 0x0

    .line 5111
    const/16 v19, 0x0

    .line 5112
    const/16 v20, 0x0

    .line 5113
    const/16 v21, 0x0

    .line 5114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5115
    const-string v2, "sans-serif-thin"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5116
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 5117
    const/16 v25, 0x0

    .line 5118
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5095
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 5122
    :pswitch_26
    const v4, 0x7f02018d

    .line 5123
    const/4 v5, 0x1

    .line 5124
    const/4 v6, 0x1

    .line 5125
    const/16 v7, 0x30

    .line 5126
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05006c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5127
    const/4 v9, -0x1

    .line 5128
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050066

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5129
    const/4 v11, 0x0

    .line 5130
    const/4 v12, 0x1

    .line 5131
    const/4 v13, 0x0

    .line 5132
    const/16 v14, 0xd

    .line 5133
    const/16 v15, 0xe

    .line 5134
    const/16 v16, 0x0

    .line 5135
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050070

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5136
    const/16 v18, 0x0

    .line 5137
    const/16 v19, 0x0

    .line 5138
    const/16 v20, 0x0

    .line 5139
    const/16 v21, 0x0

    .line 5140
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5141
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5142
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 5143
    const/16 v25, 0x0

    .line 5144
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5121
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5147
    const/4 v4, 0x0

    .line 5148
    const/4 v5, 0x1

    .line 5149
    const/4 v6, 0x1

    .line 5150
    const/16 v7, 0x30

    .line 5151
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05006d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050067

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5153
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050068

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5154
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05006a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 5155
    const/4 v12, 0x1

    .line 5156
    const/4 v13, 0x0

    .line 5157
    const/16 v14, 0xd

    .line 5158
    const/16 v15, 0xe

    .line 5159
    const/16 v16, 0x0

    .line 5160
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050071

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5161
    const/16 v18, 0x0

    .line 5162
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050074

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 5163
    const v20, 0x7f02032e

    .line 5164
    const/16 v21, 0xe

    .line 5165
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5166
    const-string v2, "sans-serif-thin"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5167
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 5168
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    move/from16 v25, v0

    .line 5169
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5146
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5172
    const/4 v4, 0x0

    .line 5173
    const/4 v5, 0x1

    .line 5174
    const/4 v6, 0x1

    .line 5175
    const/16 v7, 0x50

    .line 5176
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05006e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5177
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050069

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5178
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05006a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5179
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050068

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 5180
    const/4 v12, 0x1

    .line 5181
    const/4 v13, 0x0

    .line 5182
    const/16 v14, 0xd

    .line 5183
    const/16 v15, 0xe

    .line 5184
    const/16 v16, 0x0

    .line 5185
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050072

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5186
    const/16 v18, 0x0

    .line 5187
    const/16 v19, 0x0

    .line 5188
    const/16 v20, 0x0

    .line 5189
    const/16 v21, 0x0

    .line 5190
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5191
    const-string v2, "sans-serif-thin"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationThroughfareInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 5193
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    move/from16 v25, v0

    .line 5194
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5171
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5197
    const/4 v4, 0x0

    .line 5198
    const/4 v5, 0x1

    .line 5199
    const/4 v6, 0x1

    .line 5200
    const/16 v7, 0x50

    .line 5201
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05006f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5202
    const/4 v9, -0x1

    .line 5203
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05006b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5204
    const/4 v11, 0x0

    .line 5205
    const/4 v12, 0x1

    .line 5206
    const/4 v13, 0x0

    .line 5207
    const/16 v14, 0xd

    .line 5208
    const/16 v15, 0xe

    .line 5209
    const/16 v16, 0x0

    .line 5210
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050073

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5211
    const/16 v18, 0x0

    .line 5212
    const/16 v19, 0x0

    .line 5213
    const/16 v20, 0x0

    .line 5214
    const/16 v21, 0x0

    .line 5215
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5216
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/4 v3, 0x5

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5218
    const/16 v25, 0x0

    .line 5219
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5196
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 5223
    :pswitch_27
    const v4, 0x7f02018f

    .line 5224
    const/4 v5, 0x1

    .line 5225
    const/4 v6, 0x1

    .line 5226
    const/16 v7, 0x50

    .line 5227
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05007b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5228
    const/4 v9, -0x1

    .line 5229
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050077

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5230
    const/4 v11, 0x0

    .line 5231
    const/4 v12, 0x1

    .line 5232
    const/4 v13, 0x0

    .line 5233
    const/16 v14, 0xc

    .line 5234
    const/16 v15, 0xe

    .line 5235
    const/16 v16, 0x0

    .line 5236
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05007f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5237
    const/16 v18, 0x0

    .line 5238
    const/16 v19, 0x0

    .line 5239
    const/16 v20, 0x0

    .line 5240
    const/16 v21, 0x0

    .line 5241
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5242
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5243
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 5244
    const/16 v25, 0x0

    .line 5245
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5222
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5248
    const/4 v4, 0x0

    .line 5249
    const/4 v5, 0x1

    .line 5250
    const/4 v6, 0x1

    .line 5251
    const/16 v7, 0x50

    .line 5252
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05007c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5253
    const/4 v9, -0x1

    .line 5254
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050078

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5255
    const/4 v11, 0x0

    .line 5256
    const/4 v12, 0x1

    .line 5257
    const/4 v13, 0x0

    .line 5258
    const/16 v14, 0xc

    .line 5259
    const/16 v15, 0xe

    .line 5260
    const/16 v16, 0x0

    .line 5261
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050080

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5262
    const/16 v18, 0x0

    .line 5263
    const/16 v19, 0x0

    .line 5264
    const/16 v20, 0x0

    .line 5265
    const/16 v21, 0x0

    .line 5266
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5267
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/4 v3, 0x6

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5269
    const/16 v25, 0x0

    .line 5270
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5247
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5273
    const/4 v4, 0x1

    .line 5274
    const/4 v5, 0x2

    .line 5275
    const/4 v6, 0x1

    .line 5276
    const/16 v7, 0x30

    .line 5277
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050079

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5278
    const/4 v9, -0x1

    .line 5279
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050075

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5280
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050076

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 5281
    const/4 v12, 0x1

    .line 5282
    const/4 v13, 0x0

    .line 5283
    const/16 v14, 0xd

    .line 5284
    const/16 v15, 0xe

    .line 5285
    const/16 v16, 0x0

    .line 5286
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05007d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5287
    const/16 v18, 0x0

    .line 5288
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050081

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 5289
    const v20, 0x7f02032e

    .line 5290
    const/16 v21, 0xe

    .line 5291
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5292
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 5294
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    move/from16 v25, v0

    .line 5295
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5272
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5298
    const/4 v4, 0x0

    .line 5299
    const/4 v5, 0x2

    .line 5300
    const/4 v6, 0x1

    .line 5301
    const/16 v7, 0x30

    .line 5302
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05007a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5303
    const/4 v9, -0x1

    .line 5304
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050076

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5305
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050075

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 5306
    const/4 v12, 0x1

    .line 5307
    const/4 v13, 0x0

    .line 5308
    const/16 v14, 0xd

    .line 5309
    const/16 v15, 0xe

    .line 5310
    const/16 v16, 0x0

    .line 5311
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05007e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5312
    const/16 v18, 0x0

    .line 5313
    const/16 v19, 0x0

    .line 5314
    const/16 v20, 0x0

    .line 5315
    const/16 v21, 0x0

    .line 5316
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5317
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationThroughfareInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 5319
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    move/from16 v25, v0

    .line 5320
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5297
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 5324
    :pswitch_28
    const/4 v4, 0x1

    .line 5325
    const/4 v5, 0x1

    .line 5326
    const/4 v6, 0x5

    .line 5327
    const/16 v7, 0x50

    .line 5328
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050083

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5329
    const/4 v9, -0x1

    .line 5330
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050082

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5331
    const/4 v11, 0x0

    .line 5332
    const/4 v12, 0x5

    .line 5333
    const/4 v13, 0x0

    .line 5334
    const/16 v14, 0xc

    .line 5335
    const/16 v15, 0xb

    .line 5336
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050086

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5337
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050085

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5338
    const/16 v18, 0x0

    .line 5339
    const/16 v19, 0x0

    .line 5340
    const/16 v20, 0x0

    .line 5341
    const/16 v21, 0x0

    .line 5342
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040011

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5343
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5344
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/4 v3, 0x7

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5345
    const/16 v25, 0x0

    .line 5346
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5323
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 5351
    :pswitch_29
    const v4, 0x7f020175

    .line 5352
    const/4 v5, 0x1

    .line 5353
    const/4 v6, 0x3

    .line 5354
    const/16 v7, 0x30

    .line 5355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05008d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050087

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050088

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5358
    const/4 v11, 0x0

    .line 5359
    const/4 v12, 0x1

    .line 5360
    const/4 v13, 0x0

    .line 5361
    const/16 v14, 0xc

    .line 5362
    const/16 v15, 0xe

    .line 5363
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050092

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5364
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050091

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5365
    const/16 v18, 0x0

    .line 5366
    const/16 v19, 0x0

    .line 5367
    const/16 v20, 0x0

    .line 5368
    const/16 v21, 0x0

    .line 5369
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040012

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5370
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5371
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x8

    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5372
    const/16 v25, 0x0

    .line 5373
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5350
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5376
    const/4 v4, 0x0

    .line 5377
    const/4 v5, 0x1

    .line 5378
    const/4 v6, 0x3

    .line 5379
    const/16 v7, 0x30

    .line 5380
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05008e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5381
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050087

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5382
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050089

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5383
    const/4 v11, 0x0

    .line 5384
    const/4 v12, 0x1

    .line 5385
    const/4 v13, 0x0

    .line 5386
    const/16 v14, 0xc

    .line 5387
    const/16 v15, 0xe

    .line 5388
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050092

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050093

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5390
    const/16 v18, 0x0

    .line 5391
    const/16 v19, 0x0

    .line 5392
    const/16 v20, 0x0

    .line 5393
    const/16 v21, 0x0

    .line 5394
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040012

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5395
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5396
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x8

    const/16 v24, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5397
    const/16 v25, 0x0

    .line 5398
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5375
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5401
    const/4 v4, 0x0

    .line 5402
    const/4 v5, 0x1

    .line 5403
    const/4 v6, 0x3

    .line 5404
    const/16 v7, 0x30

    .line 5405
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05008f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5406
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050087

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5407
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05008a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5408
    const/4 v11, 0x0

    .line 5409
    const/4 v12, 0x1

    .line 5410
    const/4 v13, 0x0

    .line 5411
    const/16 v14, 0xc

    .line 5412
    const/16 v15, 0xe

    .line 5413
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050092

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5414
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050094

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5415
    const/16 v18, 0x0

    .line 5416
    const/16 v19, 0x0

    .line 5417
    const/16 v20, 0x0

    .line 5418
    const/16 v21, 0x0

    .line 5419
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040012

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5420
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5421
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 5422
    const/16 v25, 0x0

    .line 5423
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5400
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 5453
    :pswitch_2a
    const v4, 0x7f020177

    .line 5454
    const/4 v5, 0x1

    .line 5455
    const/4 v6, 0x1

    .line 5456
    const/16 v7, 0x30

    .line 5457
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05009d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5458
    const/4 v9, -0x1

    .line 5459
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050099

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5460
    const/4 v11, 0x0

    .line 5461
    const/4 v12, 0x1

    .line 5462
    const/4 v13, 0x0

    .line 5463
    const/16 v14, 0xc

    .line 5464
    const/16 v15, 0xe

    .line 5465
    const/16 v16, 0x0

    .line 5466
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500a1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5467
    const/16 v18, 0x0

    .line 5468
    const/16 v19, 0x0

    .line 5469
    const/16 v20, 0x0

    .line 5470
    const/16 v21, 0x0

    .line 5471
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040013

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5472
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5473
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x9

    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5474
    const/16 v25, 0x0

    .line 5475
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5452
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5477
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x9

    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v27

    .line 5479
    .local v27, "temp_str":Ljava/lang/String;
    const/4 v4, 0x0

    .line 5480
    const/4 v5, 0x1

    .line 5481
    const/4 v6, 0x5

    .line 5482
    const/16 v7, 0x30

    .line 5483
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05009e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5484
    const/4 v9, -0x1

    .line 5485
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05009a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5486
    const/4 v11, 0x0

    .line 5487
    const/4 v12, 0x1

    .line 5488
    const/4 v13, 0x0

    .line 5489
    const/16 v14, 0xc

    .line 5490
    const/16 v15, 0xe

    .line 5492
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->arrangeMonth(Ljava/lang/String;)I

    move-result v16

    .line 5493
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500a2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5494
    const/16 v18, 0x0

    .line 5495
    const/16 v19, 0x0

    .line 5496
    const/16 v20, 0x0

    .line 5497
    const/16 v21, 0x0

    .line 5498
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040013

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5499
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5500
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x9

    const/16 v24, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5501
    const/16 v25, 0x0

    .line 5502
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5478
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5505
    const/4 v4, 0x0

    .line 5506
    const/4 v5, 0x1

    .line 5507
    const/4 v6, 0x3

    .line 5508
    const/16 v7, 0x30

    .line 5509
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05009f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5510
    const/4 v9, -0x1

    .line 5511
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05009b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5512
    const/4 v11, 0x0

    .line 5513
    const/4 v12, 0x1

    .line 5514
    const/4 v13, 0x0

    .line 5515
    const/16 v14, 0xc

    .line 5516
    const/16 v15, 0xe

    .line 5517
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500a5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5518
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500a4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5519
    const/16 v18, 0x0

    .line 5520
    const/16 v19, 0x0

    .line 5521
    const/16 v20, 0x0

    .line 5522
    const/16 v21, 0x0

    .line 5523
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040013

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5524
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5525
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x9

    const/16 v24, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5526
    const/16 v25, 0x0

    .line 5527
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5504
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5530
    const/4 v4, 0x0

    .line 5531
    const/4 v5, 0x1

    .line 5532
    const/4 v6, 0x1

    .line 5533
    const/16 v7, 0x30

    .line 5534
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500a0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5535
    const/4 v9, -0x1

    .line 5536
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05009c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5537
    const/4 v11, 0x0

    .line 5538
    const/4 v12, 0x1

    .line 5539
    const/4 v13, 0x0

    .line 5540
    const/16 v14, 0xc

    .line 5541
    const/16 v15, 0xe

    .line 5542
    const/16 v16, 0x0

    .line 5543
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500a6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5544
    const/16 v18, 0x0

    .line 5545
    const/16 v19, 0x0

    .line 5546
    const/16 v20, 0x0

    .line 5547
    const/16 v21, 0x0

    .line 5548
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040014

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5549
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5550
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 5551
    const/16 v25, 0x0

    .line 5552
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5529
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 5556
    .end local v27    # "temp_str":Ljava/lang/String;
    :pswitch_2b
    const v4, 0x7f020179

    .line 5557
    const/4 v5, 0x1

    .line 5558
    const/4 v6, 0x3

    .line 5559
    const/16 v7, 0x30

    .line 5560
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500b4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5561
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5562
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500af

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5563
    const/4 v11, 0x0

    .line 5564
    const/4 v12, 0x1

    .line 5565
    const/4 v13, 0x0

    .line 5566
    const/16 v14, 0xc

    .line 5567
    const/16 v15, 0x9

    .line 5568
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500b8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5569
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500b7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5570
    const/16 v18, 0x0

    .line 5571
    const/16 v19, 0x0

    .line 5572
    const/16 v20, 0x0

    .line 5573
    const/16 v21, 0x0

    .line 5574
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5575
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5576
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 5577
    const/16 v25, 0x0

    .line 5578
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5555
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5581
    const/4 v4, 0x0

    .line 5582
    const/4 v5, 0x1

    .line 5583
    const/4 v6, 0x3

    .line 5584
    const/16 v7, 0x30

    .line 5585
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500b5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5586
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500b0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5587
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500b1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5588
    const/4 v11, 0x0

    .line 5589
    const/4 v12, 0x1

    .line 5590
    const/4 v13, 0x0

    .line 5591
    const/16 v14, 0xc

    .line 5592
    const/16 v15, 0x9

    .line 5593
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500ba

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5594
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5595
    const/16 v18, 0x0

    .line 5596
    const/16 v19, 0x0

    .line 5597
    const/16 v20, 0x0

    .line 5598
    const/16 v21, 0x0

    .line 5599
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5600
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5601
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0xa

    const/16 v24, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5602
    const/16 v25, 0x0

    .line 5603
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5580
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5606
    const/4 v4, 0x0

    .line 5607
    const/4 v5, 0x1

    .line 5608
    const/4 v6, 0x3

    .line 5609
    const/16 v7, 0x30

    .line 5610
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5611
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5612
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500b3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5613
    const/4 v11, 0x0

    .line 5614
    const/4 v12, 0x1

    .line 5615
    const/high16 v13, -0x3d4c0000    # -90.0f

    .line 5616
    const/16 v14, 0xc

    .line 5617
    const/16 v15, 0x9

    .line 5618
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5619
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500bb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5620
    const/16 v18, 0x0

    .line 5621
    const/16 v19, 0x0

    .line 5622
    const/16 v20, 0x0

    .line 5623
    const/16 v21, 0x0

    .line 5624
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5625
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5626
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0xa

    const/16 v24, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5627
    const/16 v25, 0x0

    .line 5628
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5605
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 5632
    :pswitch_2c
    const v4, 0x7f02017b

    .line 5633
    const/4 v5, 0x1

    .line 5634
    const/4 v6, 0x3

    .line 5635
    const/16 v7, 0x30

    .line 5636
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500c3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5637
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500bd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5638
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500be

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5639
    const/4 v11, 0x0

    .line 5640
    const/4 v12, 0x1

    .line 5641
    const/4 v13, 0x0

    .line 5642
    const/16 v14, 0xc

    .line 5643
    const/16 v15, 0xe

    .line 5644
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500c7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5645
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5646
    const/16 v18, 0x0

    .line 5647
    const/16 v19, 0x0

    .line 5648
    const/16 v20, 0x0

    .line 5649
    const/16 v21, 0x0

    .line 5650
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5651
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5652
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0xb

    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5653
    const/16 v25, 0x0

    .line 5654
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5631
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5657
    const/4 v4, 0x0

    .line 5658
    const/4 v5, 0x1

    .line 5659
    const/4 v6, 0x3

    .line 5660
    const/16 v7, 0x30

    .line 5661
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500c4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5662
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500bf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5663
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500c0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5664
    const/4 v11, 0x0

    .line 5665
    const/4 v12, 0x1

    .line 5666
    const/4 v13, 0x0

    .line 5667
    const/16 v14, 0xc

    .line 5668
    const/16 v15, 0xe

    .line 5669
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500c9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5670
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500c8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5671
    const/16 v18, 0x0

    .line 5672
    const/16 v19, 0x0

    .line 5673
    const/16 v20, 0x0

    .line 5674
    const/16 v21, 0x0

    .line 5675
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5676
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5677
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0xb

    const/16 v24, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5678
    const/16 v25, 0x0

    .line 5679
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5656
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5683
    const/4 v4, 0x0

    .line 5684
    const/4 v5, 0x1

    .line 5685
    const/4 v6, 0x3

    .line 5686
    const/16 v7, 0x30

    .line 5687
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5688
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500c1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5689
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500c2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5690
    const/4 v11, 0x0

    .line 5691
    const/4 v12, 0x1

    .line 5692
    const/4 v13, 0x0

    .line 5693
    const/16 v14, 0xc

    .line 5694
    const/16 v15, 0xe

    .line 5695
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500cb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5696
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500ca

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5697
    const/16 v18, 0x0

    .line 5698
    const/16 v19, 0x0

    .line 5699
    const/16 v20, 0x0

    .line 5700
    const/16 v21, 0x0

    .line 5701
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5702
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5703
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 5704
    const/16 v25, 0x0

    .line 5705
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5682
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 5709
    :pswitch_2d
    const v4, 0x7f02017d

    .line 5710
    const/4 v5, 0x1

    .line 5711
    const/4 v6, 0x3

    .line 5712
    const/16 v7, 0x30

    .line 5713
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5714
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500cc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5715
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500cd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5716
    const/4 v11, 0x0

    .line 5717
    const/4 v12, 0x1

    .line 5718
    const/4 v13, 0x0

    .line 5719
    const/16 v14, 0xa

    .line 5720
    const/16 v15, 0x9

    .line 5721
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500d6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5722
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5723
    const/16 v18, 0x0

    .line 5724
    const/16 v19, 0x0

    .line 5725
    const/16 v20, 0x0

    .line 5726
    const/16 v21, 0x0

    .line 5727
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5728
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5729
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 5730
    const/16 v25, 0x0

    .line 5731
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5708
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5734
    const/4 v4, 0x0

    .line 5735
    const/4 v5, 0x1

    .line 5736
    const/4 v6, 0x3

    .line 5737
    const/16 v7, 0x30

    .line 5738
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500d3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5739
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500ce

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5740
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500cf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5741
    const/4 v11, 0x0

    .line 5742
    const/4 v12, 0x1

    .line 5743
    const/4 v13, 0x0

    .line 5744
    const/16 v14, 0xa

    .line 5745
    const/16 v15, 0x9

    .line 5746
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500d8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5747
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500d7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5748
    const/16 v18, 0x0

    .line 5749
    const/16 v19, 0x0

    .line 5750
    const/16 v20, 0x0

    .line 5751
    const/16 v21, 0x0

    .line 5752
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5753
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5754
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0xc

    const/16 v24, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5755
    const/16 v25, 0x0

    .line 5756
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5733
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5759
    const/4 v4, 0x0

    .line 5760
    const/4 v5, 0x1

    .line 5761
    const/4 v6, 0x3

    .line 5762
    const/16 v7, 0x30

    .line 5763
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500d4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5764
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500d0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5765
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500d1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5766
    const/4 v11, 0x0

    .line 5767
    const/4 v12, 0x1

    .line 5768
    const/4 v13, 0x0

    .line 5769
    const/16 v14, 0xa

    .line 5770
    const/16 v15, 0x9

    .line 5771
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500da

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5772
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500d9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5773
    const/16 v18, 0x0

    .line 5774
    const/16 v19, 0x0

    .line 5775
    const/16 v20, 0x0

    .line 5776
    const/16 v21, 0x0

    .line 5777
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040016

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5778
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5779
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0xc

    const/16 v24, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5780
    const/16 v25, 0x0

    .line 5781
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5758
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 5784
    :pswitch_2e
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    .line 5786
    const v4, 0x7f02017f

    .line 5787
    const/4 v5, 0x1

    .line 5788
    const/4 v6, 0x3

    .line 5789
    const/16 v7, 0x30

    .line 5790
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500e0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5791
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500db

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5792
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500dc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5793
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500dd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 5794
    const/4 v12, 0x3

    .line 5795
    const/4 v13, 0x0

    .line 5796
    const/16 v14, 0xc

    .line 5797
    const/16 v15, 0xe

    .line 5798
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5799
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5800
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500ea

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 5801
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500e9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 5802
    const v20, 0x7f02032f

    .line 5803
    const/16 v21, 0x5

    .line 5804
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5805
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5806
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 5807
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    move/from16 v25, v0

    .line 5808
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5785
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5811
    const/4 v4, 0x0

    .line 5812
    const/4 v5, 0x1

    .line 5813
    const/4 v6, 0x3

    .line 5814
    const/16 v7, 0x30

    .line 5815
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500e1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5816
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500db

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5817
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500dd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5818
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500dc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 5819
    const/4 v12, 0x3

    .line 5820
    const/4 v13, 0x0

    .line 5821
    const/16 v14, 0xc

    .line 5822
    const/16 v15, 0xe

    .line 5823
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5824
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5825
    const/16 v18, 0x0

    .line 5826
    const/16 v19, 0x0

    .line 5827
    const/16 v20, 0x0

    .line 5828
    const/16 v21, 0x0

    .line 5829
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5830
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5831
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationThroughfareInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 5832
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    move/from16 v25, v0

    .line 5833
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5810
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5834
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    .line 5837
    const/4 v4, 0x0

    .line 5838
    const/4 v5, 0x1

    .line 5839
    const/4 v6, 0x3

    .line 5840
    const/16 v7, 0x30

    .line 5841
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500e2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5842
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500db

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5843
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500de

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5844
    const/4 v11, 0x0

    .line 5845
    const/4 v12, 0x3

    .line 5846
    const/4 v13, 0x0

    .line 5847
    const/16 v14, 0xc

    .line 5848
    const/16 v15, 0xe

    .line 5849
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5850
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500e7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5851
    const/16 v18, 0x0

    .line 5852
    const/16 v19, 0x0

    .line 5853
    const/16 v20, 0x0

    .line 5854
    const/16 v21, 0x0

    .line 5855
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5856
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5857
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0xd

    const/16 v24, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5858
    const/16 v25, 0x0

    .line 5859
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5836
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5862
    const/4 v4, 0x0

    .line 5863
    const/4 v5, 0x1

    .line 5864
    const/4 v6, 0x3

    .line 5865
    const/16 v7, 0x30

    .line 5866
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500e3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5867
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500db

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5868
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500df

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5869
    const/4 v11, 0x0

    .line 5870
    const/4 v12, 0x3

    .line 5871
    const/4 v13, 0x0

    .line 5872
    const/16 v14, 0xc

    .line 5873
    const/16 v15, 0xe

    .line 5874
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 5875
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500e8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5876
    const/16 v18, 0x0

    .line 5877
    const/16 v19, 0x0

    .line 5878
    const/16 v20, 0x0

    .line 5879
    const/16 v21, 0x0

    .line 5880
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5881
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5882
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0xd

    const/16 v24, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5883
    const/16 v25, 0x0

    .line 5884
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5861
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 5887
    :pswitch_2f
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    .line 5889
    const v4, 0x7f020181

    .line 5890
    const/4 v5, 0x1

    .line 5891
    const/4 v6, 0x1

    .line 5892
    const/16 v7, 0x30

    .line 5893
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500f1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5894
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500eb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5895
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500ec

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5896
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500ee

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 5897
    const/4 v12, 0x1

    .line 5898
    const/4 v13, 0x0

    .line 5899
    const/16 v14, 0xc

    .line 5900
    const/16 v15, 0xe

    .line 5901
    const/16 v16, 0x0

    .line 5902
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500f5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5903
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500fa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 5904
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500f9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 5905
    const v20, 0x7f02032f

    .line 5906
    const/16 v21, 0xe

    .line 5907
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5908
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5909
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 5910
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    move/from16 v25, v0

    .line 5911
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5888
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5914
    const/4 v4, 0x0

    .line 5915
    const/4 v5, 0x1

    .line 5916
    const/4 v6, 0x1

    .line 5917
    const/16 v7, 0x30

    .line 5918
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500f2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5919
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500ed

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5920
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500ee

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5921
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500ec

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 5922
    const/4 v12, 0x1

    .line 5923
    const/4 v13, 0x0

    .line 5924
    const/16 v14, 0xc

    .line 5925
    const/16 v15, 0xe

    .line 5926
    const/16 v16, 0x0

    .line 5927
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500f6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5928
    const/16 v18, 0x0

    .line 5929
    const/16 v19, 0x0

    .line 5930
    const/16 v20, 0x0

    .line 5931
    const/16 v21, 0x0

    .line 5932
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5933
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5934
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationThroughfareInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 5935
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    move/from16 v25, v0

    .line 5936
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5913
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5937
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    .line 5940
    const/4 v4, 0x0

    .line 5941
    const/4 v5, 0x1

    .line 5942
    const/4 v6, 0x1

    .line 5943
    const/16 v7, 0x30

    .line 5944
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500f3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5945
    const/4 v9, -0x1

    .line 5946
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500ef

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5947
    const/4 v11, 0x0

    .line 5948
    const/4 v12, 0x1

    .line 5949
    const/4 v13, 0x0

    .line 5950
    const/16 v14, 0xc

    .line 5951
    const/16 v15, 0xe

    .line 5952
    const/16 v16, 0x0

    .line 5953
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500f7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5954
    const/16 v18, 0x0

    .line 5955
    const/16 v19, 0x0

    .line 5956
    const/16 v20, 0x0

    .line 5957
    const/16 v21, 0x0

    .line 5958
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5959
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5960
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0xe

    const/16 v24, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5961
    const/16 v25, 0x0

    .line 5962
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5939
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 5965
    const/4 v4, 0x0

    .line 5966
    const/4 v5, 0x1

    .line 5967
    const/4 v6, 0x1

    .line 5968
    const/16 v7, 0x30

    .line 5969
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500f4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5970
    const/4 v9, -0x1

    .line 5971
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500f0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5972
    const/4 v11, 0x0

    .line 5973
    const/4 v12, 0x1

    .line 5974
    const/4 v13, 0x0

    .line 5975
    const/16 v14, 0xc

    .line 5976
    const/16 v15, 0xe

    .line 5977
    const/16 v16, 0x0

    .line 5978
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500f8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 5979
    const/16 v18, 0x0

    .line 5980
    const/16 v19, 0x0

    .line 5981
    const/16 v20, 0x0

    .line 5982
    const/16 v21, 0x0

    .line 5983
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 5984
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 5985
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0xe

    const/16 v24, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 5986
    const/16 v25, 0x0

    .line 5987
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5964
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 5990
    :pswitch_30
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    .line 5992
    const v4, 0x7f020183

    .line 5993
    const/4 v5, 0x1

    .line 5994
    const/4 v6, 0x3

    .line 5995
    const/16 v7, 0x30

    .line 5996
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050100

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 5997
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5998
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500fc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5999
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 6000
    const/4 v12, 0x3

    .line 6001
    const/4 v13, 0x0

    .line 6002
    const/16 v14, 0xc

    .line 6003
    const/16 v15, 0x9

    .line 6004
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050104

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6005
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050105

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6006
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05010a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 6007
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050109

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 6008
    const v20, 0x7f020330

    .line 6009
    const/16 v21, 0x5

    .line 6010
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040019

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6011
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6012
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 6013
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    move/from16 v25, v0

    .line 6014
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 5991
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6017
    const/4 v4, 0x0

    .line 6018
    const/4 v5, 0x1

    .line 6019
    const/4 v6, 0x3

    .line 6020
    const/16 v7, 0x30

    .line 6021
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050101

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6022
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6023
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6024
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500fc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 6025
    const/4 v12, 0x3

    .line 6026
    const/4 v13, 0x0

    .line 6027
    const/16 v14, 0xc

    .line 6028
    const/16 v15, 0x9

    .line 6029
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050104

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6030
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050106

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6031
    const/16 v18, 0x0

    .line 6032
    const/16 v19, 0x0

    .line 6033
    const/16 v20, 0x0

    .line 6034
    const/16 v21, 0x0

    .line 6035
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040019

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6036
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6037
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationThroughfareInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 6038
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    move/from16 v25, v0

    .line 6039
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6016
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6040
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    .line 6043
    const/4 v4, 0x0

    .line 6044
    const/4 v5, 0x1

    .line 6045
    const/4 v6, 0x3

    .line 6046
    const/16 v7, 0x30

    .line 6047
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050102

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6048
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6049
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500fe

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6050
    const/4 v11, 0x0

    .line 6051
    const/4 v12, 0x3

    .line 6052
    const/4 v13, 0x0

    .line 6053
    const/16 v14, 0xc

    .line 6054
    const/16 v15, 0x9

    .line 6055
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050104

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6056
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050107

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6057
    const/16 v18, 0x0

    .line 6058
    const/16 v19, 0x0

    .line 6059
    const/16 v20, 0x0

    .line 6060
    const/16 v21, 0x0

    .line 6061
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040019

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6062
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6063
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0xf

    const/16 v24, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6064
    const/16 v25, 0x0

    .line 6065
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6042
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6068
    const/4 v4, 0x0

    .line 6069
    const/4 v5, 0x1

    .line 6070
    const/4 v6, 0x3

    .line 6071
    const/16 v7, 0x30

    .line 6072
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050103

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6073
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6074
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0500ff

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6075
    const/4 v11, 0x0

    .line 6076
    const/4 v12, 0x3

    .line 6077
    const/4 v13, 0x0

    .line 6078
    const/16 v14, 0xc

    .line 6079
    const/16 v15, 0x9

    .line 6080
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050104

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6081
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050108

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6082
    const/16 v18, 0x0

    .line 6083
    const/16 v19, 0x0

    .line 6084
    const/16 v20, 0x0

    .line 6085
    const/16 v21, 0x0

    .line 6086
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040019

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6087
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6088
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0xf

    const/16 v24, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6089
    const/16 v25, 0x0

    .line 6090
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6067
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 6093
    :pswitch_31
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    .line 6095
    const v4, 0x7f020185

    .line 6096
    const/4 v5, 0x1

    .line 6097
    const/4 v6, 0x3

    .line 6098
    const/16 v7, 0x30

    .line 6099
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050110

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6100
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05010b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05010c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05010d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 6103
    const/4 v12, 0x3

    .line 6104
    const/4 v13, 0x0

    .line 6105
    const/16 v14, 0xc

    .line 6106
    const/16 v15, 0xe

    .line 6107
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050114

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050115

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6109
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05011a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 6110
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050119

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 6111
    const v20, 0x7f020330

    .line 6112
    const/16 v21, 0x5

    .line 6113
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6114
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6115
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 6116
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    move/from16 v25, v0

    .line 6117
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6094
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6120
    const/4 v4, 0x0

    .line 6121
    const/4 v5, 0x1

    .line 6122
    const/4 v6, 0x3

    .line 6123
    const/16 v7, 0x30

    .line 6124
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050111

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6125
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05010b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6126
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05010d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6127
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05010c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 6128
    const/4 v12, 0x3

    .line 6129
    const/4 v13, 0x0

    .line 6130
    const/16 v14, 0xc

    .line 6131
    const/16 v15, 0xe

    .line 6132
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050114

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6133
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050116

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6134
    const/16 v18, 0x0

    .line 6135
    const/16 v19, 0x0

    .line 6136
    const/16 v20, 0x0

    .line 6137
    const/16 v21, 0x0

    .line 6138
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6139
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationThroughfareInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 6141
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    move/from16 v25, v0

    .line 6142
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6119
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6143
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    .line 6146
    const/4 v4, 0x0

    .line 6147
    const/4 v5, 0x1

    .line 6148
    const/4 v6, 0x3

    .line 6149
    const/16 v7, 0x30

    .line 6150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050112

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6151
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05010b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05010e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6153
    const/4 v11, 0x0

    .line 6154
    const/4 v12, 0x3

    .line 6155
    const/4 v13, 0x0

    .line 6156
    const/16 v14, 0xc

    .line 6157
    const/16 v15, 0xe

    .line 6158
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050114

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6159
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050117

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6160
    const/16 v18, 0x0

    .line 6161
    const/16 v19, 0x0

    .line 6162
    const/16 v20, 0x0

    .line 6163
    const/16 v21, 0x0

    .line 6164
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6165
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6166
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x10

    const/16 v24, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6167
    const/16 v25, 0x0

    .line 6168
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6145
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6171
    const/4 v4, 0x0

    .line 6172
    const/4 v5, 0x1

    .line 6173
    const/4 v6, 0x3

    .line 6174
    const/16 v7, 0x30

    .line 6175
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050113

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6176
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05010b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6177
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05010f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6178
    const/4 v11, 0x0

    .line 6179
    const/4 v12, 0x3

    .line 6180
    const/4 v13, 0x0

    .line 6181
    const/16 v14, 0xc

    .line 6182
    const/16 v15, 0xe

    .line 6183
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050114

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6184
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050118

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6185
    const/16 v18, 0x0

    .line 6186
    const/16 v19, 0x0

    .line 6187
    const/16 v20, 0x0

    .line 6188
    const/16 v21, 0x0

    .line 6189
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6190
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x10

    const/16 v24, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6192
    const/16 v25, 0x0

    .line 6193
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6170
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 6196
    :pswitch_32
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    .line 6198
    const v4, 0x7f020187

    .line 6199
    const/4 v5, 0x1

    .line 6200
    const/4 v6, 0x3

    .line 6201
    const/16 v7, 0x30

    .line 6202
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050123

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6203
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05011b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6204
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05011c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6205
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05011e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 6206
    const/4 v12, 0x1

    .line 6207
    const/4 v13, 0x0

    .line 6208
    const/16 v14, 0xa

    .line 6209
    const/16 v15, 0x9

    .line 6210
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050128

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6211
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050129

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6212
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050130

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 6213
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05012f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 6214
    const v20, 0x7f020330

    .line 6215
    const/16 v21, 0xe

    .line 6216
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6217
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 6219
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    move/from16 v25, v0

    .line 6220
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6197
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6223
    const/4 v4, 0x0

    .line 6224
    const/4 v5, 0x1

    .line 6225
    const/4 v6, 0x3

    .line 6226
    const/16 v7, 0x30

    .line 6227
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050124

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6228
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05011d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6229
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05011e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6230
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05011c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 6231
    const/4 v12, 0x1

    .line 6232
    const/4 v13, 0x0

    .line 6233
    const/16 v14, 0xa

    .line 6234
    const/16 v15, 0x9

    .line 6235
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050128

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6236
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05012a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6237
    const/16 v18, 0x0

    .line 6238
    const/16 v19, 0x0

    .line 6239
    const/16 v20, 0x0

    .line 6240
    const/16 v21, 0x0

    .line 6241
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6242
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationThroughfareInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 6244
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    move/from16 v25, v0

    .line 6245
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6222
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6246
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    .line 6249
    const/4 v4, 0x0

    .line 6250
    const/4 v5, 0x1

    .line 6251
    const/4 v6, 0x3

    .line 6252
    const/16 v7, 0x30

    .line 6253
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050125

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6254
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05011f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6255
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050120

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6256
    const/4 v11, 0x0

    .line 6257
    const/4 v12, 0x1

    .line 6258
    const/4 v13, 0x0

    .line 6259
    const/16 v14, 0xa

    .line 6260
    const/16 v15, 0x9

    .line 6261
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05012b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05012c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6263
    const/16 v18, 0x0

    .line 6264
    const/16 v19, 0x0

    .line 6265
    const/16 v20, 0x0

    .line 6266
    const/16 v21, 0x0

    .line 6267
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6268
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6269
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x11

    const/16 v24, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6270
    const/16 v25, 0x0

    .line 6271
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6248
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6274
    const/4 v4, 0x0

    .line 6275
    const/4 v5, 0x1

    .line 6276
    const/4 v6, 0x3

    .line 6277
    const/16 v7, 0x30

    .line 6278
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050126

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6279
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05011f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6280
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050121

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6281
    const/4 v11, 0x0

    .line 6282
    const/4 v12, 0x1

    .line 6283
    const/4 v13, 0x0

    .line 6284
    const/16 v14, 0xa

    .line 6285
    const/16 v15, 0x9

    .line 6286
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05012b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6287
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05012d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6288
    const/16 v18, 0x0

    .line 6289
    const/16 v19, 0x0

    .line 6290
    const/16 v20, 0x0

    .line 6291
    const/16 v21, 0x0

    .line 6292
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6293
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6294
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x11

    const/16 v24, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6295
    const/16 v25, 0x0

    .line 6296
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6273
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 6319
    :pswitch_33
    const v4, 0x7f02016d

    .line 6320
    const/4 v5, 0x1

    .line 6321
    const/4 v6, 0x3

    .line 6322
    const/16 v7, 0x30

    .line 6323
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050137

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6324
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050131

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050132

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6326
    const/4 v11, 0x0

    .line 6327
    const/16 v12, 0x11

    .line 6328
    const/4 v13, 0x0

    .line 6329
    const/16 v14, 0xc

    .line 6330
    const/16 v15, 0xe

    .line 6331
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05013a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05013b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6333
    const/16 v18, 0x0

    .line 6334
    const/16 v19, 0x0

    .line 6335
    const/16 v20, 0x0

    .line 6336
    const/16 v21, 0x0

    .line 6337
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6338
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6339
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x12

    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6340
    const/16 v25, 0x0

    .line 6341
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6318
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6344
    const/4 v4, 0x0

    .line 6345
    const/4 v5, 0x1

    .line 6346
    const/4 v6, 0x3

    .line 6347
    const/16 v7, 0x30

    .line 6348
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050138

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6349
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050133

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6350
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050134

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6351
    const/4 v11, 0x0

    .line 6352
    const/16 v12, 0x11

    .line 6353
    const/4 v13, 0x0

    .line 6354
    const/16 v14, 0xc

    .line 6355
    const/16 v15, 0xe

    .line 6356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05013a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05013c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6358
    const/16 v18, 0x0

    .line 6359
    const/16 v19, 0x0

    .line 6360
    const/16 v20, 0x0

    .line 6361
    const/16 v21, 0x0

    .line 6362
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6363
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6364
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x12

    const/16 v24, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6365
    const/16 v25, 0x0

    .line 6366
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6343
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6369
    const/4 v4, 0x0

    .line 6370
    const/4 v5, 0x1

    .line 6371
    const/4 v6, 0x3

    .line 6372
    const/16 v7, 0x30

    .line 6373
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050139

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6374
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050135

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6375
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050136

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6376
    const/4 v11, 0x0

    .line 6377
    const/16 v12, 0x11

    .line 6378
    const/4 v13, 0x0

    .line 6379
    const/16 v14, 0xc

    .line 6380
    const/16 v15, 0xe

    .line 6381
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05013d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6382
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05013e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6383
    const/16 v18, 0x0

    .line 6384
    const/16 v19, 0x0

    .line 6385
    const/16 v20, 0x0

    .line 6386
    const/16 v21, 0x0

    .line 6387
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6388
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x12

    const/16 v24, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6390
    const/16 v25, 0x0

    .line 6391
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6368
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 6395
    :pswitch_34
    const v4, 0x7f02016f

    .line 6396
    const/4 v5, 0x1

    .line 6397
    const/4 v6, 0x3

    .line 6398
    const/16 v7, 0x30

    .line 6399
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050145

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6400
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05013f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6401
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050140

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6402
    const/4 v11, 0x0

    .line 6403
    const/16 v12, 0x11

    .line 6404
    const/4 v13, 0x0

    .line 6405
    const/16 v14, 0xa

    .line 6406
    const/16 v15, 0x9

    .line 6407
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050148

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6408
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050149

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6409
    const/16 v18, 0x0

    .line 6410
    const/16 v19, 0x0

    .line 6411
    const/16 v20, 0x0

    .line 6412
    const/16 v21, 0x0

    .line 6413
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6414
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6415
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 6416
    const/16 v25, 0x0

    .line 6417
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6394
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6420
    const/4 v4, 0x0

    .line 6421
    const/4 v5, 0x1

    .line 6422
    const/4 v6, 0x3

    .line 6423
    const/16 v7, 0x30

    .line 6424
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050146

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6425
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050141

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6426
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050142

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6427
    const/4 v11, 0x0

    .line 6428
    const/4 v12, 0x1

    .line 6429
    const/4 v13, 0x0

    .line 6430
    const/16 v14, 0xa

    .line 6431
    const/16 v15, 0x9

    .line 6432
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05014a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6433
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05014b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6434
    const/16 v18, 0x0

    .line 6435
    const/16 v19, 0x0

    .line 6436
    const/16 v20, 0x0

    .line 6437
    const/16 v21, 0x0

    .line 6438
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6439
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6440
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x13

    const/16 v24, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6441
    const/16 v25, 0x0

    .line 6442
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6419
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6445
    const/4 v4, 0x0

    .line 6446
    const/4 v5, 0x1

    .line 6447
    const/4 v6, 0x3

    .line 6448
    const/16 v7, 0x30

    .line 6449
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050147

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6450
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050143

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6451
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050144

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6452
    const/4 v11, 0x0

    .line 6453
    const/4 v12, 0x1

    .line 6454
    const/4 v13, 0x0

    .line 6455
    const/16 v14, 0xa

    .line 6456
    const/16 v15, 0x9

    .line 6457
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05014c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6458
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05014d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6459
    const/16 v18, 0x0

    .line 6460
    const/16 v19, 0x0

    .line 6461
    const/16 v20, 0x0

    .line 6462
    const/16 v21, 0x0

    .line 6463
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6464
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6465
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x13

    const/16 v24, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6466
    const/16 v25, 0x0

    .line 6467
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6444
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 6471
    :pswitch_35
    const v4, 0x7f020171

    .line 6472
    const/4 v5, 0x1

    .line 6473
    const/4 v6, 0x3

    .line 6474
    const/16 v7, 0x30

    .line 6475
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050154

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6476
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05014e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6477
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05014f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6478
    const/4 v11, 0x0

    .line 6479
    const/4 v12, 0x1

    .line 6480
    const/4 v13, 0x0

    .line 6481
    const/16 v14, 0xc

    .line 6482
    const/16 v15, 0xb

    .line 6483
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050157

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6484
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050158

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6485
    const/16 v18, 0x0

    .line 6486
    const/16 v19, 0x0

    .line 6487
    const/16 v20, 0x0

    .line 6488
    const/16 v21, 0x0

    .line 6489
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6490
    const-string v2, "sans-serif-light"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6491
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x14

    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6492
    const/16 v25, 0x0

    .line 6493
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6470
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6496
    const/4 v4, 0x0

    .line 6497
    const/4 v5, 0x1

    .line 6498
    const/4 v6, 0x3

    .line 6499
    const/16 v7, 0x30

    .line 6500
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050155

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6501
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050150

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6502
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050151

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6503
    const/4 v11, 0x0

    .line 6504
    const/4 v12, 0x1

    .line 6505
    const/4 v13, 0x0

    .line 6506
    const/16 v14, 0xc

    .line 6507
    const/16 v15, 0xb

    .line 6508
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050159

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6509
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05015a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6510
    const/16 v18, 0x0

    .line 6511
    const/16 v19, 0x0

    .line 6512
    const/16 v20, 0x0

    .line 6513
    const/16 v21, 0x0

    .line 6514
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6515
    const-string v2, "sans-serif-light"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6516
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x14

    const/16 v24, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6517
    const/16 v25, 0x0

    .line 6518
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6495
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6521
    const/4 v4, 0x0

    .line 6522
    const/4 v5, 0x1

    .line 6523
    const/4 v6, 0x3

    .line 6524
    const/16 v7, 0x30

    .line 6525
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050156

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6526
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050152

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6527
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050153

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6528
    const/4 v11, 0x0

    .line 6529
    const/4 v12, 0x1

    .line 6530
    const/4 v13, 0x0

    .line 6531
    const/16 v14, 0xc

    .line 6532
    const/16 v15, 0xb

    .line 6533
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05015b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6534
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05015c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6535
    const/16 v18, 0x0

    .line 6536
    const/16 v19, 0x0

    .line 6537
    const/16 v20, 0x0

    .line 6538
    const/16 v21, 0x0

    .line 6539
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04001f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6540
    const-string v2, "sans-serif-light"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6541
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x14

    const/16 v24, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6542
    const/16 v25, 0x0

    .line 6543
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6520
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 6547
    :pswitch_36
    const v4, 0x7f020173

    .line 6548
    const/4 v5, 0x1

    .line 6549
    const/4 v6, 0x3

    .line 6550
    const/16 v7, 0x30

    .line 6551
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050161

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6552
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05015d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6553
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05015e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6554
    const/4 v11, 0x0

    .line 6555
    const/4 v12, 0x1

    .line 6556
    const/4 v13, 0x0

    .line 6557
    const/16 v14, 0xc

    .line 6558
    const/16 v15, 0x9

    .line 6559
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050163

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6560
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050164

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6561
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050168

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 6562
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050167

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 6563
    const v20, 0x7f020331

    .line 6564
    const/16 v21, 0xe

    .line 6565
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040020

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6566
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6567
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 6568
    const/16 v25, 0x0

    .line 6569
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6546
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6572
    const/4 v4, 0x0

    .line 6573
    const/4 v5, 0x1

    .line 6574
    const/4 v6, 0x3

    .line 6575
    const/16 v7, 0x30

    .line 6576
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050162

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6577
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05015f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6578
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050160

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6579
    const/4 v11, 0x0

    .line 6580
    const/4 v12, 0x1

    .line 6581
    const/4 v13, 0x0

    .line 6582
    const/16 v14, 0xc

    .line 6583
    const/16 v15, 0x9

    .line 6584
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050165

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6585
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050166

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6586
    const/16 v18, 0x0

    .line 6587
    const/16 v19, 0x0

    .line 6588
    const/16 v20, 0x0

    .line 6589
    const/16 v21, 0x0

    .line 6590
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040020

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6591
    const-string v2, "sans-serif"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6592
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x15

    const/16 v24, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6593
    const/16 v25, 0x0

    .line 6594
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6571
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 6599
    :pswitch_37
    const v4, 0x7f0200cd

    .line 6600
    const/4 v5, 0x1

    .line 6601
    const/4 v6, 0x1

    const/16 v7, 0x30

    .line 6602
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05016f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6603
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050169

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6604
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05016a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6605
    const/4 v11, 0x0

    .line 6606
    const/4 v12, 0x1

    .line 6607
    const/4 v13, 0x0

    .line 6608
    const/16 v14, 0xd

    .line 6609
    const/16 v15, 0xd

    .line 6610
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050172

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6611
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050173

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6612
    const/16 v18, 0x0

    .line 6613
    const/16 v19, 0x0

    .line 6614
    const/16 v20, 0x0

    .line 6615
    const/16 v21, 0x0

    .line 6616
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040021

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6617
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6618
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6619
    const/16 v25, 0x0

    .line 6620
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6598
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6623
    const/4 v4, 0x0

    .line 6624
    const/4 v5, 0x1

    .line 6625
    const/4 v6, 0x1

    const/16 v7, 0x30

    .line 6626
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050170

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6627
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05016b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6628
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05016c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6629
    const/4 v11, 0x0

    .line 6630
    const/4 v12, 0x1

    .line 6631
    const/4 v13, 0x0

    .line 6632
    const/16 v14, 0xd

    .line 6633
    const/16 v15, 0xd

    .line 6634
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050172

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6635
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050174

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6636
    const/16 v18, 0x0

    .line 6637
    const/16 v19, 0x0

    .line 6638
    const/16 v20, 0x0

    .line 6639
    const/16 v21, 0x0

    .line 6640
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040021

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6641
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6642
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6643
    const/16 v25, 0x0

    .line 6644
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6622
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6647
    const/4 v4, 0x0

    .line 6648
    const/4 v5, 0x1

    .line 6649
    const/4 v6, 0x1

    const/16 v7, 0x30

    .line 6650
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050171

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6651
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05016d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6652
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05016e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6653
    const/4 v11, 0x0

    .line 6654
    const/4 v12, 0x1

    .line 6655
    const/4 v13, 0x0

    .line 6656
    const/16 v14, 0xd

    .line 6657
    const/16 v15, 0xd

    .line 6658
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050172

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6659
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050175

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6660
    const/16 v18, 0x0

    .line 6661
    const/16 v19, 0x0

    .line 6662
    const/16 v20, 0x0

    .line 6663
    const/16 v21, 0x0

    .line 6664
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040021

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6665
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6666
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6667
    const/16 v25, 0x0

    .line 6668
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6646
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 6672
    :pswitch_38
    const v4, 0x7f0200cf

    .line 6673
    const/4 v5, 0x1

    .line 6674
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 6675
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05017c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6676
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050176

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6677
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050177

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6678
    const/4 v11, 0x0

    .line 6679
    const/4 v12, 0x3

    .line 6680
    const/4 v13, 0x0

    .line 6681
    const/16 v14, 0xa

    .line 6682
    const/16 v15, 0x9

    .line 6683
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05017d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6684
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05017e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6685
    const/16 v18, 0x0

    .line 6686
    const/16 v19, 0x0

    .line 6687
    const/16 v20, 0x0

    .line 6688
    const/16 v21, 0x0

    .line 6689
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040022

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6690
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6691
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6692
    const/16 v25, 0x0

    .line 6693
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6671
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6696
    const/4 v4, 0x0

    .line 6697
    const/4 v5, 0x1

    .line 6698
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 6699
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05017c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6700
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050178

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6701
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050179

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6702
    const/4 v11, 0x0

    .line 6703
    const/4 v12, 0x3

    .line 6704
    const/4 v13, 0x0

    .line 6705
    const/16 v14, 0xa

    .line 6706
    const/16 v15, 0x9

    .line 6707
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05017d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6708
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05017f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6709
    const/16 v18, 0x0

    .line 6710
    const/16 v19, 0x0

    .line 6711
    const/16 v20, 0x0

    .line 6712
    const/16 v21, 0x0

    .line 6713
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040022

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6714
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6715
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6716
    const/16 v25, 0x0

    .line 6717
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6695
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6720
    const/4 v4, 0x0

    .line 6721
    const/4 v5, 0x1

    .line 6722
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 6723
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05017c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6724
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05017a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6725
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05017b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6726
    const/4 v11, 0x0

    .line 6727
    const/4 v12, 0x3

    .line 6728
    const/4 v13, 0x0

    .line 6729
    const/16 v14, 0xa

    .line 6730
    const/16 v15, 0x9

    .line 6731
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05017d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6732
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050180

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6733
    const/16 v18, 0x0

    .line 6734
    const/16 v19, 0x0

    .line 6735
    const/16 v20, 0x0

    .line 6736
    const/16 v21, 0x0

    .line 6737
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040022

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6738
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6739
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6740
    const/16 v25, 0x0

    .line 6741
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6719
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 6745
    :pswitch_39
    const v4, 0x7f0200d1

    .line 6746
    const/4 v5, 0x1

    .line 6747
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 6748
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050187

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6749
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050181

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6750
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050182

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6751
    const/4 v11, 0x0

    .line 6752
    const/16 v12, 0x11

    .line 6753
    const/4 v13, 0x0

    .line 6754
    const/16 v14, 0xc

    .line 6755
    const/16 v15, 0xb

    .line 6756
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050188

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6757
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050189

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6758
    const/16 v18, 0x0

    .line 6759
    const/16 v19, 0x0

    .line 6760
    const/16 v20, 0x0

    .line 6761
    const/16 v21, 0x0

    .line 6762
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040023

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6763
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6764
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6765
    const/16 v25, 0x0

    .line 6766
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6744
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6769
    const/4 v4, 0x0

    .line 6770
    const/4 v5, 0x1

    .line 6771
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 6772
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050187

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6773
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050183

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6774
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050184

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6775
    const/4 v11, 0x0

    .line 6776
    const/16 v12, 0x11

    .line 6777
    const/4 v13, 0x0

    .line 6778
    const/16 v14, 0xc

    .line 6779
    const/16 v15, 0xb

    .line 6780
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05018a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6781
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05018b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6782
    const/16 v18, 0x0

    .line 6783
    const/16 v19, 0x0

    .line 6784
    const/16 v20, 0x0

    .line 6785
    const/16 v21, 0x0

    .line 6786
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040023

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6787
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6788
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6789
    const/16 v25, 0x0

    .line 6790
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6768
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6793
    const/4 v4, 0x0

    .line 6794
    const/4 v5, 0x1

    .line 6795
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 6796
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050187

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6797
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050185

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6798
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050186

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6799
    const/4 v11, 0x0

    .line 6800
    const/16 v12, 0x11

    .line 6801
    const/4 v13, 0x0

    .line 6802
    const/16 v14, 0xc

    .line 6803
    const/16 v15, 0xb

    .line 6804
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05018c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6805
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05018d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6806
    const/16 v18, 0x0

    .line 6807
    const/16 v19, 0x0

    .line 6808
    const/16 v20, 0x0

    .line 6809
    const/16 v21, 0x0

    .line 6810
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040023

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6811
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6812
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6813
    const/16 v25, 0x0

    .line 6814
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6792
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 6818
    :pswitch_3a
    const v4, 0x7f0200d3

    .line 6819
    const/4 v5, 0x1

    .line 6820
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 6821
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050194

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6822
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05018e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6823
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05018f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6824
    const/4 v11, 0x0

    .line 6825
    const/4 v12, 0x3

    .line 6826
    const/4 v13, 0x0

    .line 6827
    const/16 v14, 0xc

    .line 6828
    const/16 v15, 0xe

    .line 6829
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050195

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6830
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050196

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6831
    const/16 v18, 0x0

    .line 6832
    const/16 v19, 0x0

    .line 6833
    const/16 v20, 0x0

    .line 6834
    const/16 v21, 0x0

    .line 6835
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040024

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6836
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6837
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6838
    const/16 v25, 0x0

    .line 6839
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6817
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6842
    const/4 v4, 0x0

    .line 6843
    const/4 v5, 0x1

    .line 6844
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 6845
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050194

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6846
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050190

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6847
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050191

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6848
    const/4 v11, 0x0

    .line 6849
    const/4 v12, 0x3

    .line 6850
    const/4 v13, 0x0

    .line 6851
    const/16 v14, 0xc

    .line 6852
    const/16 v15, 0xe

    .line 6853
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050197

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6854
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050198

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6855
    const/16 v18, 0x0

    .line 6856
    const/16 v19, 0x0

    .line 6857
    const/16 v20, 0x0

    .line 6858
    const/16 v21, 0x0

    .line 6859
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040024

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6860
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6861
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6862
    const/16 v25, 0x0

    .line 6863
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6841
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6866
    const/4 v4, 0x0

    .line 6867
    const/4 v5, 0x1

    .line 6868
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 6869
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050194

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6870
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050192

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6871
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050193

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6872
    const/4 v11, 0x0

    .line 6873
    const/4 v12, 0x3

    .line 6874
    const/4 v13, 0x0

    .line 6875
    const/16 v14, 0xc

    .line 6876
    const/16 v15, 0xe

    .line 6877
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050199

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6878
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05019a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6879
    const/16 v18, 0x0

    .line 6880
    const/16 v19, 0x0

    .line 6881
    const/16 v20, 0x0

    .line 6882
    const/16 v21, 0x0

    .line 6883
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040024

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6884
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6885
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6886
    const/16 v25, 0x0

    .line 6887
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6865
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 6891
    :pswitch_3b
    const v4, 0x7f0200c1

    .line 6892
    const/4 v5, 0x1

    .line 6893
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 6894
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501a3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6895
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05019b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6896
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05019c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6897
    const/4 v11, 0x0

    .line 6898
    const/4 v12, 0x1

    .line 6899
    const/4 v13, 0x0

    .line 6900
    const/16 v14, 0xc

    .line 6901
    const/16 v15, 0xe

    .line 6902
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501a5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6903
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501a6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6904
    const/16 v18, 0x0

    .line 6905
    const/16 v19, 0x0

    .line 6906
    const/16 v20, 0x0

    .line 6907
    const/16 v21, 0x0

    .line 6908
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040025

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6909
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6910
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6911
    const/16 v25, 0x0

    .line 6912
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6890
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6915
    const/4 v4, 0x0

    .line 6916
    const/4 v5, 0x1

    .line 6917
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 6918
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501a3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6919
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05019d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6920
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05019e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6921
    const/4 v11, 0x0

    .line 6922
    const/4 v12, 0x1

    .line 6923
    const/4 v13, 0x0

    .line 6924
    const/16 v14, 0xc

    .line 6925
    const/16 v15, 0xe

    .line 6926
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501a7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6927
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501a8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6928
    const/16 v18, 0x0

    .line 6929
    const/16 v19, 0x0

    .line 6930
    const/16 v20, 0x0

    .line 6931
    const/16 v21, 0x0

    .line 6932
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040025

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6933
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6934
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6935
    const/16 v25, 0x0

    .line 6936
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6914
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6939
    const/4 v4, 0x0

    .line 6940
    const/4 v5, 0x1

    .line 6941
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 6942
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501a3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6943
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05019f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6944
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501a0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6945
    const/4 v11, 0x0

    .line 6946
    const/4 v12, 0x1

    .line 6947
    const/4 v13, 0x0

    .line 6948
    const/16 v14, 0xc

    .line 6949
    const/16 v15, 0xe

    .line 6950
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501a9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6951
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6952
    const/16 v18, 0x0

    .line 6953
    const/16 v19, 0x0

    .line 6954
    const/16 v20, 0x0

    .line 6955
    const/16 v21, 0x0

    .line 6956
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040025

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6957
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6958
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 6959
    const/16 v25, 0x0

    .line 6960
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6938
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 6963
    const/4 v4, 0x0

    .line 6964
    const/4 v5, 0x1

    .line 6965
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 6966
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501a4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6967
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501a1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6968
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501a2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6969
    const/4 v11, 0x0

    .line 6970
    const/4 v12, 0x1

    .line 6971
    const/4 v13, 0x0

    .line 6972
    const/16 v14, 0xc

    .line 6973
    const/16 v15, 0xe

    .line 6974
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501ab

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 6975
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501ac

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 6976
    const/16 v18, 0x0

    .line 6977
    const/16 v19, 0x0

    .line 6978
    const/16 v20, 0x0

    .line 6979
    const/16 v21, 0x0

    .line 6980
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040026

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 6981
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 6982
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x1a

    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 6983
    const/16 v25, 0x0

    .line 6984
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6962
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 6988
    :pswitch_3c
    const v4, 0x7f0200c3

    .line 6989
    const/4 v5, 0x1

    .line 6990
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 6991
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501b5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 6992
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501ad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 6993
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 6994
    const/4 v11, 0x0

    .line 6995
    const/4 v12, 0x1

    .line 6996
    const/4 v13, 0x0

    .line 6997
    const/16 v14, 0xc

    .line 6998
    const/16 v15, 0xe

    .line 6999
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501b7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7000
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501b8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7001
    const/16 v18, 0x0

    .line 7002
    const/16 v19, 0x0

    .line 7003
    const/16 v20, 0x0

    .line 7004
    const/16 v21, 0x0

    .line 7005
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040027

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7006
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7007
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7008
    const/16 v25, 0x0

    .line 7009
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 6987
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7012
    const/4 v4, 0x0

    .line 7013
    const/4 v5, 0x1

    .line 7014
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7015
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501b5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7016
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501af

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7017
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501b0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7018
    const/4 v11, 0x0

    .line 7019
    const/4 v12, 0x1

    .line 7020
    const/4 v13, 0x0

    .line 7021
    const/16 v14, 0xc

    .line 7022
    const/16 v15, 0xe

    .line 7023
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7024
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501ba

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7025
    const/16 v18, 0x0

    .line 7026
    const/16 v19, 0x0

    .line 7027
    const/16 v20, 0x0

    .line 7028
    const/16 v21, 0x0

    .line 7029
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040027

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7030
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7031
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7032
    const/16 v25, 0x0

    .line 7033
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7011
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7036
    const/4 v4, 0x0

    .line 7037
    const/4 v5, 0x1

    .line 7038
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7039
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501b5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7040
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501b1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7041
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7042
    const/4 v11, 0x0

    .line 7043
    const/4 v12, 0x1

    .line 7044
    const/4 v13, 0x0

    .line 7045
    const/16 v14, 0xc

    .line 7046
    const/16 v15, 0xe

    .line 7047
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501bb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7048
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7049
    const/16 v18, 0x0

    .line 7050
    const/16 v19, 0x0

    .line 7051
    const/16 v20, 0x0

    .line 7052
    const/16 v21, 0x0

    .line 7053
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040027

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7054
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7055
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7056
    const/16 v25, 0x0

    .line 7057
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7035
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7060
    const/4 v4, 0x0

    .line 7061
    const/4 v5, 0x1

    .line 7062
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7063
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7064
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501b3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7065
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501b4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7066
    const/4 v11, 0x0

    .line 7067
    const/4 v12, 0x3

    .line 7068
    const/4 v13, 0x0

    .line 7069
    const/16 v14, 0xc

    .line 7070
    const/16 v15, 0xe

    .line 7071
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501bd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7072
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501be

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7073
    const/16 v18, 0x0

    .line 7074
    const/16 v19, 0x0

    .line 7075
    const/16 v20, 0x0

    .line 7076
    const/16 v21, 0x0

    .line 7077
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040028

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7078
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7079
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    const/16 v3, 0x1b

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v24

    .line 7080
    const/16 v25, 0x0

    .line 7081
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7059
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 7085
    :pswitch_3d
    const v4, 0x7f0200c5

    .line 7086
    const/4 v5, 0x1

    .line 7087
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7088
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7089
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501bf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7090
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501c0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7091
    const/4 v11, 0x0

    .line 7092
    const/4 v12, 0x1

    .line 7093
    const/4 v13, 0x0

    .line 7094
    const/16 v14, 0xc

    .line 7095
    const/16 v15, 0x9

    .line 7096
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7097
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501c7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7098
    const/16 v18, 0x0

    .line 7099
    const/16 v19, 0x0

    .line 7100
    const/16 v20, 0x0

    .line 7101
    const/16 v21, 0x0

    .line 7102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040029

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7103
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7104
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7105
    const/16 v25, 0x0

    .line 7106
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7084
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7109
    const/4 v4, 0x0

    .line 7110
    const/4 v5, 0x1

    .line 7111
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7113
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501c1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501c2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7115
    const/4 v11, 0x0

    .line 7116
    const/4 v12, 0x1

    .line 7117
    const/4 v13, 0x0

    .line 7118
    const/16 v14, 0xc

    .line 7119
    const/16 v15, 0x9

    .line 7120
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501c8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7121
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501c9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7122
    const/16 v18, 0x0

    .line 7123
    const/16 v19, 0x0

    .line 7124
    const/16 v20, 0x0

    .line 7125
    const/16 v21, 0x0

    .line 7126
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040029

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7127
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7128
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7129
    const/16 v25, 0x0

    .line 7130
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7108
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7133
    const/4 v4, 0x0

    .line 7134
    const/4 v5, 0x1

    .line 7135
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7137
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501c3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7138
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501c4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7139
    const/4 v11, 0x0

    .line 7140
    const/4 v12, 0x1

    .line 7141
    const/4 v13, 0x0

    .line 7142
    const/16 v14, 0xc

    .line 7143
    const/16 v15, 0x9

    .line 7144
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501ca

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7145
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501cb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7146
    const/16 v18, 0x0

    .line 7147
    const/16 v19, 0x0

    .line 7148
    const/16 v20, 0x0

    .line 7149
    const/16 v21, 0x0

    .line 7150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040029

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7151
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7153
    const/16 v25, 0x0

    .line 7154
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7132
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 7158
    :pswitch_3e
    const v4, 0x7f0200c7

    .line 7159
    const/4 v5, 0x1

    .line 7160
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7161
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7162
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501cc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7163
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501cd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7164
    const/4 v11, 0x0

    .line 7165
    const/4 v12, 0x1

    .line 7166
    const/4 v13, 0x0

    .line 7167
    const/16 v14, 0xc

    .line 7168
    const/16 v15, 0xe

    .line 7169
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501d3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7170
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501d4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7171
    const/16 v18, 0x0

    .line 7172
    const/16 v19, 0x0

    .line 7173
    const/16 v20, 0x0

    .line 7174
    const/16 v21, 0x0

    .line 7175
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04002a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7176
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7177
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7178
    const/16 v25, 0x0

    .line 7179
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7157
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7182
    const/4 v4, 0x0

    .line 7183
    const/4 v5, 0x1

    .line 7184
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7185
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7186
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501ce

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7187
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501cf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7188
    const/4 v11, 0x0

    .line 7189
    const/4 v12, 0x1

    .line 7190
    const/4 v13, 0x0

    .line 7191
    const/16 v14, 0xc

    .line 7192
    const/16 v15, 0xe

    .line 7193
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501d6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7195
    const/16 v18, 0x0

    .line 7196
    const/16 v19, 0x0

    .line 7197
    const/16 v20, 0x0

    .line 7198
    const/16 v21, 0x0

    .line 7199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04002a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7200
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7201
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7202
    const/16 v25, 0x0

    .line 7203
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7181
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7206
    const/4 v4, 0x0

    .line 7207
    const/4 v5, 0x1

    .line 7208
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7209
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7210
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501d0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7211
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501d1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7212
    const/4 v11, 0x0

    .line 7213
    const/4 v12, 0x1

    .line 7214
    const/4 v13, 0x0

    .line 7215
    const/16 v14, 0xc

    .line 7216
    const/16 v15, 0xe

    .line 7217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501d7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7218
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501d8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7219
    const/16 v18, 0x0

    .line 7220
    const/16 v19, 0x0

    .line 7221
    const/16 v20, 0x0

    .line 7222
    const/16 v21, 0x0

    .line 7223
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04002a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7224
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7225
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7226
    const/16 v25, 0x0

    .line 7227
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7205
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 7231
    :pswitch_3f
    const v4, 0x7f0200c9

    .line 7232
    const/4 v5, 0x1

    .line 7233
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7234
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501df

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7235
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501d9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7236
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501da

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7237
    const/4 v11, 0x0

    .line 7238
    const/4 v12, 0x1

    .line 7239
    const/4 v13, 0x0

    .line 7240
    const/16 v14, 0xa

    .line 7241
    const/16 v15, 0x9

    .line 7242
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501e1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7243
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501e2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7244
    const/16 v18, 0x0

    .line 7245
    const/16 v19, 0x0

    .line 7246
    const/16 v20, 0x0

    .line 7247
    const/16 v21, 0x0

    .line 7248
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04002b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7249
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7250
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7251
    const/16 v25, 0x0

    .line 7252
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7230
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7255
    const/4 v4, 0x0

    .line 7256
    const/4 v5, 0x1

    .line 7257
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7258
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501e0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7259
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501db

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7260
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501dc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7261
    const/4 v11, 0x0

    .line 7262
    const/4 v12, 0x1

    .line 7263
    const/4 v13, 0x0

    .line 7264
    const/16 v14, 0xa

    .line 7265
    const/16 v15, 0x9

    .line 7266
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501e3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7267
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7268
    const/16 v18, 0x0

    .line 7269
    const/16 v19, 0x0

    .line 7270
    const/16 v20, 0x0

    .line 7271
    const/16 v21, 0x0

    .line 7272
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04002c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7273
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7274
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7275
    const/16 v25, 0x0

    .line 7276
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7254
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7279
    const/4 v4, 0x0

    .line 7280
    const/4 v5, 0x1

    .line 7281
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7282
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501df

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7283
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501dd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7284
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501de

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7285
    const/4 v11, 0x0

    .line 7286
    const/4 v12, 0x1

    .line 7287
    const/4 v13, 0x0

    .line 7288
    const/16 v14, 0xa

    .line 7289
    const/16 v15, 0x9

    .line 7290
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7291
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7292
    const/16 v18, 0x0

    .line 7293
    const/16 v19, 0x0

    .line 7294
    const/16 v20, 0x0

    .line 7295
    const/16 v21, 0x0

    .line 7296
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04002b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7297
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7298
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7299
    const/16 v25, 0x0

    .line 7300
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7278
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 7304
    :pswitch_40
    const v4, 0x7f0200cb

    .line 7305
    const/4 v5, 0x1

    .line 7306
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7307
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501ed

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7308
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501e7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7309
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501e8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7310
    const/4 v11, 0x0

    .line 7311
    const/4 v12, 0x1

    .line 7312
    const/4 v13, 0x0

    .line 7313
    const/16 v14, 0xa

    .line 7314
    const/16 v15, 0xb

    .line 7315
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501ef

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7316
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501f0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7317
    const/16 v18, 0x0

    .line 7318
    const/16 v19, 0x0

    .line 7319
    const/16 v20, 0x0

    .line 7320
    const/16 v21, 0x0

    .line 7321
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04002d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7322
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7323
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7324
    const/16 v25, 0x0

    .line 7325
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7303
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7328
    const/4 v4, 0x0

    .line 7329
    const/4 v5, 0x1

    .line 7330
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7331
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501ee

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501e9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501ea

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7334
    const/4 v11, 0x0

    .line 7335
    const/4 v12, 0x1

    .line 7336
    const/4 v13, 0x0

    .line 7337
    const/16 v14, 0xa

    .line 7338
    const/16 v15, 0xb

    .line 7339
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501f1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7340
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501f2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7341
    const/16 v18, 0x0

    .line 7342
    const/16 v19, 0x0

    .line 7343
    const/16 v20, 0x0

    .line 7344
    const/16 v21, 0x0

    .line 7345
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04002e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7346
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7347
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7348
    const/16 v25, 0x0

    .line 7349
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7327
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7352
    const/4 v4, 0x0

    .line 7353
    const/4 v5, 0x1

    .line 7354
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501ee

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501eb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501ec

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7358
    const/4 v11, 0x0

    .line 7359
    const/4 v12, 0x1

    .line 7360
    const/4 v13, 0x0

    .line 7361
    const/16 v14, 0xa

    .line 7362
    const/16 v15, 0xb

    .line 7363
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501f3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7364
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501f4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7365
    const/16 v18, 0x0

    .line 7366
    const/16 v19, 0x0

    .line 7367
    const/16 v20, 0x0

    .line 7368
    const/16 v21, 0x0

    .line 7369
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04002e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7370
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7371
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7372
    const/16 v25, 0x0

    .line 7373
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7351
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 7377
    :pswitch_41
    const v4, 0x7f0200bb

    .line 7378
    const/4 v5, 0x1

    .line 7379
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7380
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7381
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501f5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7382
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501f6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7383
    const/4 v11, 0x0

    .line 7384
    const/4 v12, 0x1

    .line 7385
    const/4 v13, 0x0

    .line 7386
    const/16 v14, 0xc

    .line 7387
    const/16 v15, 0xe

    .line 7388
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501fc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7390
    const/16 v18, 0x0

    .line 7391
    const/16 v19, 0x0

    .line 7392
    const/16 v20, 0x0

    .line 7393
    const/16 v21, 0x0

    .line 7394
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04002f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7395
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7396
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7397
    const/16 v25, 0x0

    .line 7398
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7376
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7401
    const/4 v4, 0x0

    .line 7402
    const/4 v5, 0x1

    .line 7403
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7404
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7405
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501f7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7406
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501f8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7407
    const/4 v11, 0x0

    .line 7408
    const/4 v12, 0x1

    .line 7409
    const/4 v13, 0x0

    .line 7410
    const/16 v14, 0xc

    .line 7411
    const/16 v15, 0xe

    .line 7412
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501fe

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7413
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501ff

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7414
    const/16 v18, 0x0

    .line 7415
    const/16 v19, 0x0

    .line 7416
    const/16 v20, 0x0

    .line 7417
    const/16 v21, 0x0

    .line 7418
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04002f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7419
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7420
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7421
    const/16 v25, 0x0

    .line 7422
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7400
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7425
    const/4 v4, 0x0

    .line 7426
    const/4 v5, 0x1

    .line 7427
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7428
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7429
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501f9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7430
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0501fa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7431
    const/4 v11, 0x0

    .line 7432
    const/4 v12, 0x1

    .line 7433
    const/4 v13, 0x0

    .line 7434
    const/16 v14, 0xc

    .line 7435
    const/16 v15, 0xe

    .line 7436
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050200

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7437
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050201

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7438
    const/16 v18, 0x0

    .line 7439
    const/16 v19, 0x0

    .line 7440
    const/16 v20, 0x0

    .line 7441
    const/16 v21, 0x0

    .line 7442
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04002f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7443
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7444
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7445
    const/16 v25, 0x0

    .line 7446
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7424
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 7450
    :pswitch_42
    const v4, 0x7f0200bd

    .line 7451
    const/4 v5, 0x1

    .line 7452
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7453
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050208

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7454
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050202

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7455
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050203

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7456
    const/4 v11, 0x0

    .line 7457
    const/4 v12, 0x3

    .line 7458
    const/4 v13, 0x0

    .line 7459
    const/16 v14, 0xc

    .line 7460
    const/16 v15, 0x9

    .line 7461
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050209

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7462
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05020a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7463
    const/16 v18, 0x0

    .line 7464
    const/16 v19, 0x0

    .line 7465
    const/16 v20, 0x0

    .line 7466
    const/16 v21, 0x0

    .line 7467
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040030

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7468
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7469
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7470
    const/16 v25, 0x0

    .line 7471
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7449
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7474
    const/4 v4, 0x0

    .line 7475
    const/4 v5, 0x1

    .line 7476
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7477
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050208

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7478
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050204

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7479
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050205

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7480
    const/4 v11, 0x0

    .line 7481
    const/4 v12, 0x3

    .line 7482
    const/4 v13, 0x0

    .line 7483
    const/16 v14, 0xc

    .line 7484
    const/16 v15, 0x9

    .line 7485
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7486
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05020c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7487
    const/16 v18, 0x0

    .line 7488
    const/16 v19, 0x0

    .line 7489
    const/16 v20, 0x0

    .line 7490
    const/16 v21, 0x0

    .line 7491
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040030

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7492
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7493
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7494
    const/16 v25, 0x0

    .line 7495
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7473
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7498
    const/4 v4, 0x0

    .line 7499
    const/4 v5, 0x1

    .line 7500
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7501
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050208

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7502
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050206

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7503
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050207

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7504
    const/4 v11, 0x0

    .line 7505
    const/4 v12, 0x3

    .line 7506
    const/4 v13, 0x0

    .line 7507
    const/16 v14, 0xc

    .line 7508
    const/16 v15, 0x9

    .line 7509
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05020d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7510
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05020e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7511
    const/16 v18, 0x0

    .line 7512
    const/16 v19, 0x0

    .line 7513
    const/16 v20, 0x0

    .line 7514
    const/16 v21, 0x0

    .line 7515
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040030

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7516
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7517
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7518
    const/16 v25, 0x0

    .line 7519
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7497
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 7523
    :pswitch_43
    const v4, 0x7f0200bf

    .line 7524
    const/4 v5, 0x1

    .line 7525
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7526
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050215

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7527
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05020f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7528
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050210

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7529
    const/4 v11, 0x0

    .line 7530
    const/4 v12, 0x3

    .line 7531
    const/4 v13, 0x0

    .line 7532
    const/16 v14, 0xc

    .line 7533
    const/16 v15, 0xb

    .line 7534
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050216

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7535
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050217

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7536
    const/16 v18, 0x0

    .line 7537
    const/16 v19, 0x0

    .line 7538
    const/16 v20, 0x0

    .line 7539
    const/16 v21, 0x0

    .line 7540
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040031

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7541
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7542
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7543
    const/16 v25, 0x0

    .line 7544
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7522
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7547
    const/4 v4, 0x0

    .line 7548
    const/4 v5, 0x1

    .line 7549
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7550
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050215

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7551
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050211

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7552
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050212

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7553
    const/4 v11, 0x0

    .line 7554
    const/4 v12, 0x3

    .line 7555
    const/4 v13, 0x0

    .line 7556
    const/16 v14, 0xc

    .line 7557
    const/16 v15, 0xb

    .line 7558
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050218

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7559
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050219

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7560
    const/16 v18, 0x0

    .line 7561
    const/16 v19, 0x0

    .line 7562
    const/16 v20, 0x0

    .line 7563
    const/16 v21, 0x0

    .line 7564
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040031

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7565
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7566
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7567
    const/16 v25, 0x0

    .line 7568
    const/16 v26, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7546
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    .line 7571
    const/4 v4, 0x0

    .line 7572
    const/4 v5, 0x1

    .line 7573
    const/4 v6, 0x3

    const/16 v7, 0x30

    .line 7574
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050215

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v8, v2

    .line 7575
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050213

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 7576
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050214

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 7577
    const/4 v11, 0x0

    .line 7578
    const/4 v12, 0x3

    .line 7579
    const/4 v13, 0x0

    .line 7580
    const/16 v14, 0xc

    .line 7581
    const/16 v15, 0xb

    .line 7582
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05021a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 7583
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05021b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 7584
    const/16 v18, 0x0

    .line 7585
    const/16 v19, 0x0

    .line 7586
    const/16 v20, 0x0

    .line 7587
    const/16 v21, 0x0

    .line 7588
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040031

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    .line 7589
    const-string v2, "sans-serif"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v23

    .line 7590
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v24

    .line 7591
    const/16 v25, 0x0

    .line 7592
    const/16 v26, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    .line 7570
    invoke-virtual/range {v2 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 4696
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch

    .line 4813
    :pswitch_data_1
    .packed-switch 0x3150008b
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
    .end packed-switch

    .line 4765
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
    .end packed-switch
.end method

.method private rearrangeDateInfo(Ljava/util/Date;)Ljava/lang/String;
    .locals 10
    .param p1, "mediaDateInfo"    # Ljava/util/Date;

    .prologue
    const/4 v9, 0x1

    .line 889
    const/4 v4, 0x0

    .line 890
    .local v4, "rearrangedInfo":Ljava/lang/String;
    if-eqz p1, :cond_5

    .line 893
    invoke-virtual {p1}, Ljava/util/Date;->getMonth()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 894
    .local v3, "month":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/Date;->getDate()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 895
    .local v0, "day":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/Date;->getYear()I

    move-result v7

    add-int/lit16 v6, v7, 0x76c

    .line 896
    .local v6, "year":I
    invoke-virtual {p1}, Ljava/util/Date;->getHours()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 897
    .local v1, "hour":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/Date;->getMinutes()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 898
    .local v2, "minute":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/Date;->getSeconds()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 900
    .local v5, "seconds":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v7, v9, :cond_0

    .line 901
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "0"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 902
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v7, v9, :cond_1

    .line 903
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "0"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 904
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v7, v9, :cond_2

    .line 905
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "0"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 906
    :cond_2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v7, v9, :cond_3

    .line 907
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "0"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 908
    :cond_3
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v7, v9, :cond_4

    .line 909
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "0"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 911
    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 912
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "JW rearrangedInfo="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 915
    .end local v0    # "day":Ljava/lang/String;
    .end local v1    # "hour":Ljava/lang/String;
    .end local v2    # "minute":Ljava/lang/String;
    .end local v3    # "month":Ljava/lang/String;
    .end local v5    # "seconds":Ljava/lang/String;
    .end local v6    # "year":I
    :cond_5
    return-object v4
.end method


# virtual methods
.method public applyOriginal()[I
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 1839
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW applyOriginal: mId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1840
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampLayer(II)V

    .line 1841
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    const/4 v1, 0x0

    invoke-direct {p0, v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->makeStamp(IIZ)V

    .line 1842
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized applyPreview(IZ)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "id"    # I
    .param p2, "done"    # Z

    .prologue
    .line 1846
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->resetStampLayer()V

    .line 1847
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->changeId(I)I

    move-result p1

    .line 1848
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampLayer(II)V

    .line 1849
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampViewVisibility(I)V

    .line 1850
    iput-boolean p2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mDone:Z

    .line 1851
    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->makeStamp(IIZ)V

    .line 1853
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mTempBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1846
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public arrangeMonth(Ljava/lang/String;)I
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const v2, 0x7f0500a8

    .line 7953
    const/4 v0, 0x0

    .line 7955
    .local v0, "ret":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 7978
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 7981
    :goto_0
    return v0

    .line 7957
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0500a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 7958
    goto :goto_0

    .line 7960
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 7961
    goto :goto_0

    .line 7963
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0500a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 7964
    goto :goto_0

    .line 7966
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0500aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 7967
    goto :goto_0

    .line 7969
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0500ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 7970
    goto :goto_0

    .line 7972
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0500ac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 7973
    goto :goto_0

    .line 7975
    :pswitch_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0500ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 7976
    goto :goto_0

    .line 7955
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public changeId(I)I
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 920
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    const v1, 0x3150008b

    if-lt v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    const v1, 0x3150009f

    if-gt v0, v1, :cond_1

    .line 922
    packed-switch p1, :pswitch_data_0

    .line 1034
    :cond_0
    :goto_0
    return p1

    .line 925
    :pswitch_0
    const p1, 0x3150008b

    .line 926
    goto :goto_0

    .line 928
    :pswitch_1
    const p1, 0x3150008c

    .line 929
    goto :goto_0

    .line 931
    :pswitch_2
    const p1, 0x3150008d

    .line 932
    goto :goto_0

    .line 934
    :pswitch_3
    const p1, 0x3150008e

    .line 935
    goto :goto_0

    .line 937
    :pswitch_4
    const p1, 0x3150008f

    .line 938
    goto :goto_0

    .line 940
    :pswitch_5
    const p1, 0x31500090

    .line 941
    goto :goto_0

    .line 943
    :pswitch_6
    const p1, 0x31500091

    .line 944
    goto :goto_0

    .line 946
    :pswitch_7
    const p1, 0x31500092

    .line 947
    goto :goto_0

    .line 949
    :pswitch_8
    const p1, 0x31500093

    .line 950
    goto :goto_0

    .line 952
    :pswitch_9
    const p1, 0x31500094

    .line 953
    goto :goto_0

    .line 955
    :pswitch_a
    const p1, 0x31500095

    .line 956
    goto :goto_0

    .line 958
    :pswitch_b
    const p1, 0x31500096

    .line 959
    goto :goto_0

    .line 961
    :pswitch_c
    const p1, 0x31500097

    .line 962
    goto :goto_0

    .line 964
    :pswitch_d
    const p1, 0x31500098

    .line 965
    goto :goto_0

    .line 967
    :pswitch_e
    const p1, 0x31500099

    .line 968
    goto :goto_0

    .line 970
    :pswitch_f
    const p1, 0x3150009a

    .line 971
    goto :goto_0

    .line 973
    :pswitch_10
    const p1, 0x3150009b

    .line 974
    goto :goto_0

    .line 976
    :pswitch_11
    const p1, 0x3150009c

    .line 977
    goto :goto_0

    .line 979
    :pswitch_12
    const p1, 0x3150009d

    .line 980
    goto :goto_0

    .line 982
    :pswitch_13
    const p1, 0x3150009e

    .line 983
    goto :goto_0

    .line 985
    :pswitch_14
    const p1, 0x3150009f

    goto :goto_0

    .line 989
    :cond_1
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    const v1, 0x315000a0

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    const v1, 0x315000ac

    if-gt v0, v1, :cond_0

    .line 991
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 994
    :pswitch_15
    const p1, 0x315000a0

    .line 995
    goto :goto_0

    .line 997
    :pswitch_16
    const p1, 0x315000a1

    .line 998
    goto :goto_0

    .line 1000
    :pswitch_17
    const p1, 0x315000a2

    .line 1001
    goto :goto_0

    .line 1003
    :pswitch_18
    const p1, 0x315000a3

    .line 1004
    goto :goto_0

    .line 1006
    :pswitch_19
    const p1, 0x315000a4

    .line 1007
    goto :goto_0

    .line 1009
    :pswitch_1a
    const p1, 0x315000a5

    .line 1010
    goto :goto_0

    .line 1012
    :pswitch_1b
    const p1, 0x315000a6

    .line 1013
    goto/16 :goto_0

    .line 1015
    :pswitch_1c
    const p1, 0x315000a7

    .line 1016
    goto/16 :goto_0

    .line 1018
    :pswitch_1d
    const p1, 0x315000a8

    .line 1019
    goto/16 :goto_0

    .line 1021
    :pswitch_1e
    const p1, 0x315000a9

    .line 1022
    goto/16 :goto_0

    .line 1024
    :pswitch_1f
    const p1, 0x315000aa

    .line 1025
    goto/16 :goto_0

    .line 1027
    :pswitch_20
    const p1, 0x315000ab

    .line 1028
    goto/16 :goto_0

    .line 1030
    :pswitch_21
    const p1, 0x315000ac

    goto/16 :goto_0

    .line 922
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch

    .line 991
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
    .end packed-switch
.end method

.method public changeLocationInfo()V
    .locals 1

    .prologue
    .line 7931
    const-string v0, "None"

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    .line 7933
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    .line 7934
    return-void
.end method

.method public configurationChanged()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 7794
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 7796
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 7797
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    .line 7799
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    .line 7800
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 7801
    .local v0, "middleParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 7802
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 7811
    .end local v0    # "middleParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/StampEffect;)V
    .locals 1
    .param p1, "stampEffect"    # Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    .prologue
    .line 81
    if-eqz p1, :cond_0

    .line 83
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 85
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    .line 86
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    .line 87
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    .line 88
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    .line 89
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationThroughfareInfo:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationThroughfareInfo:Ljava/lang/String;

    .line 90
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    .line 91
    iget-boolean v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    .line 92
    iget-boolean v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    .line 93
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    .line 94
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mTempBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 95
    iget-boolean v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    .line 96
    iget-boolean v0, p1, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    .line 112
    :cond_0
    return-void
.end method

.method public getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;
    .locals 12
    .param p1, "cameraInfo"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    const/4 v11, 0x1

    const/4 v8, 0x0

    .line 1404
    const/4 v2, 0x0

    .line 1406
    .local v2, "info":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 1407
    const-string v7, "-"

    .line 1458
    :goto_0
    return-object v7

    .line 1409
    :cond_0
    const-string v7, ","

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1411
    .local v6, "values":[Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, "sj, SE - getCameraInfoByParsing() - cameraInfo : "

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " / values : "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1413
    packed-switch p2, :pswitch_data_0

    .line 1456
    :cond_1
    :goto_1
    if-nez v2, :cond_2

    .line 1457
    const-string v2, "-"

    :cond_2
    move-object v7, v2

    .line 1458
    goto :goto_0

    .line 1415
    :pswitch_0
    array-length v7, v6

    :goto_2
    if-ge v8, v7, :cond_1

    aget-object v4, v6, v8

    .line 1416
    .local v4, "str":Ljava/lang/String;
    if-eqz v4, :cond_4

    .line 1417
    const-string v9, "iso:"

    invoke-virtual {v4, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1418
    new-instance v7, Ljava/lang/String;

    const-string v8, "iso:"

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1419
    goto :goto_1

    .line 1421
    :cond_3
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "sj, SE - getCameraInfoByParsing() - iso - str : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1422
    const-string v10, " / info : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1421
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1415
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 1427
    .end local v4    # "str":Ljava/lang/String;
    :pswitch_1
    array-length v7, v6

    :goto_3
    if-ge v8, v7, :cond_1

    aget-object v4, v6, v8

    .line 1428
    .restart local v4    # "str":Ljava/lang/String;
    if-eqz v4, :cond_5

    .line 1429
    const-string v9, "exposure:"

    invoke-virtual {v4, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1430
    new-instance v7, Ljava/lang/String;

    const-string v8, "exposure:"

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1431
    goto :goto_1

    .line 1427
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 1437
    .end local v4    # "str":Ljava/lang/String;
    :pswitch_2
    array-length v9, v6

    move v7, v8

    :goto_4
    if-ge v7, v9, :cond_1

    aget-object v4, v6, v7

    .line 1438
    .restart local v4    # "str":Ljava/lang/String;
    if-eqz v4, :cond_7

    .line 1439
    const-string v10, "fnumber:"

    invoke-virtual {v4, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1440
    new-instance v7, Ljava/lang/String;

    const-string v9, "fnumber:"

    invoke-direct {v7, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1441
    const-string v7, "/"

    invoke-virtual {v2, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1442
    .local v5, "strVal":[Ljava/lang/String;
    array-length v7, v5

    const/4 v9, 0x2

    if-eq v7, v9, :cond_6

    .line 1443
    const-string v7, "-"

    goto/16 :goto_0

    .line 1444
    :cond_6
    aget-object v7, v5, v8

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1445
    .local v3, "numerator":I
    aget-object v7, v5, v11

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1446
    .local v1, "denominator":I
    int-to-float v7, v3

    int-to-float v9, v1

    div-float v0, v7, v9

    .line 1447
    .local v0, "aperture":F
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, "F"

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "%.1f"

    new-array v10, v11, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v10, v8

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1448
    goto/16 :goto_1

    .line 1437
    .end local v0    # "aperture":F
    .end local v1    # "denominator":I
    .end local v3    # "numerator":I
    .end local v5    # "strVal":[Ljava/lang/String;
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 1413
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getCurBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 7758
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mTempBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getCurPos()I
    .locals 1

    .prologue
    .line 7762
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    return v0
.end method

.method public getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;
    .locals 22
    .param p1, "dateInfo"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "id"    # I

    .prologue
    .line 1143
    const/4 v4, 0x0

    .line 1144
    .local v4, "date":Ljava/lang/String;
    const/4 v14, 0x0

    .line 1145
    .local v14, "tempyy":Ljava/lang/String;
    const/4 v15, 0x0

    .line 1146
    .local v15, "tempyy_two":Ljava/lang/String;
    const/4 v13, 0x0

    .line 1147
    .local v13, "tempmm":Ljava/lang/String;
    const/4 v10, 0x0

    .line 1148
    .local v10, "tempdd":Ljava/lang/String;
    const/4 v12, 0x0

    .line 1149
    .local v12, "temphm":Ljava/lang/String;
    const/4 v11, 0x0

    .line 1150
    .local v11, "temphh":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1151
    .local v7, "mMinutes":Ljava/lang/String;
    if-eqz p1, :cond_7

    .line 1153
    const-string v18, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_2

    .line 1155
    const/16 v18, 0x0

    const/16 v19, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 1156
    const/16 v18, 0x2

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 1157
    const/16 v18, 0x5

    const/16 v19, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 1158
    const/16 v18, 0x8

    const/16 v19, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 1159
    const/16 v18, 0xb

    const/16 v19, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 1160
    const/16 v18, 0xb

    const/16 v19, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 1161
    const/16 v18, 0xe

    const/16 v19, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 1163
    const-string v18, ":"

    const/16 v19, 0x6

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1165
    const/16 v18, 0x5

    const/16 v19, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 1166
    const/16 v18, 0x7

    const/16 v19, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 1167
    const-string v18, " "

    const/16 v19, 0x8

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1168
    const/16 v18, 0x7

    const/16 v19, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 1189
    :cond_0
    :goto_0
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 1190
    .local v17, "year":I
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 1191
    .local v9, "month":I
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1192
    .local v5, "day":I
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 1194
    .local v6, "hour":I
    const/4 v8, 0x0

    .line 1195
    .local v8, "meridiem":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 1197
    const-string v8, ""

    .line 1213
    :goto_1
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 1215
    .local v16, "two_year":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 1216
    .local v3, "calendar":Ljava/util/Calendar;
    invoke-virtual {v3}, Ljava/util/Calendar;->clear()V

    .line 1217
    add-int/lit8 v18, v9, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v3, v0, v1, v5}, Ljava/util/Calendar;->set(III)V

    .line 1218
    packed-switch p2, :pswitch_data_0

    .line 1395
    :cond_1
    :goto_2
    :pswitch_0
    invoke-virtual {v3}, Ljava/util/Calendar;->clear()V

    .line 1400
    .end local v3    # "calendar":Ljava/util/Calendar;
    .end local v5    # "day":I
    .end local v6    # "hour":I
    .end local v8    # "meridiem":Ljava/lang/String;
    .end local v9    # "month":I
    .end local v16    # "two_year":I
    .end local v17    # "year":I
    :goto_3
    return-object v4

    .line 1172
    :cond_2
    const/16 v18, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 1174
    const/16 v18, 0x0

    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 1175
    const/16 v18, 0x3

    const/16 v19, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 1176
    const/16 v18, 0x6

    const/16 v19, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 1177
    const/16 v18, 0x8

    const/16 v19, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 1178
    const/16 v18, 0xb

    const/16 v19, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 1179
    const/16 v18, 0xb

    const/16 v19, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 1180
    const/16 v18, 0xe

    const/16 v19, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 1181
    const-string v18, ":"

    const/16 v19, 0xc

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1183
    const/16 v18, 0xb

    const/16 v19, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_0

    .line 1202
    .restart local v5    # "day":I
    .restart local v6    # "hour":I
    .restart local v8    # "meridiem":Ljava/lang/String;
    .restart local v9    # "month":I
    .restart local v17    # "year":I
    :cond_3
    const/16 v18, 0xc

    move/from16 v0, v18

    if-le v6, v0, :cond_4

    .line 1203
    add-int/lit8 v6, v6, -0xc

    .line 1204
    const-string v8, "PM"

    .line 1205
    goto/16 :goto_1

    .line 1208
    :cond_4
    const-string v8, "AM"

    goto/16 :goto_1

    .line 1221
    .restart local v3    # "calendar":Ljava/util/Calendar;
    .restart local v16    # "two_year":I
    :pswitch_1
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x2

    const/16 v20, 0x2

    sget-object v21, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " / "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ":"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1222
    goto/16 :goto_2

    .line 1224
    :pswitch_2
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " / "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ":"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1225
    goto/16 :goto_2

    .line 1228
    :pswitch_3
    const/16 v18, 0x1

    move/from16 v0, p3

    move/from16 v1, v18

    if-ne v0, v1, :cond_5

    .line 1229
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x2

    const/16 v20, 0x2

    sget-object v21, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1230
    goto/16 :goto_2

    :cond_5
    const/16 v18, 0x2

    move/from16 v0, p3

    move/from16 v1, v18

    if-ne v0, v1, :cond_6

    .line 1231
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, ":"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    .line 1232
    :cond_6
    const/16 v18, 0x3

    move/from16 v0, p3

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 1234
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_1

    .line 1236
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 1237
    if-eqz v8, :cond_1

    .line 1238
    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 1241
    goto/16 :goto_2

    .line 1246
    :pswitch_4
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1247
    goto/16 :goto_2

    .line 1249
    :pswitch_5
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "\'"

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1250
    goto/16 :goto_2

    .line 1252
    :pswitch_6
    packed-switch p3, :pswitch_data_1

    goto/16 :goto_2

    .line 1254
    :pswitch_7
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1255
    goto/16 :goto_2

    .line 1257
    :pswitch_8
    new-instance v18, Ljava/lang/StringBuilder;

    const/16 v19, 0x2

    const/16 v20, 0x1

    sget-object v21, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1258
    goto/16 :goto_2

    .line 1264
    :pswitch_9
    packed-switch p3, :pswitch_data_2

    goto/16 :goto_2

    .line 1266
    :pswitch_a
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1267
    goto/16 :goto_2

    .line 1269
    :pswitch_b
    const/16 v18, 0x2

    const/16 v19, 0x2

    sget-object v20, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 1270
    goto/16 :goto_2

    .line 1272
    :pswitch_c
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1273
    goto/16 :goto_2

    .line 1279
    :pswitch_d
    packed-switch p3, :pswitch_data_3

    goto/16 :goto_2

    .line 1281
    :pswitch_e
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1282
    goto/16 :goto_2

    .line 1284
    :pswitch_f
    const/16 v18, 0x2

    const/16 v19, 0x1

    sget-object v20, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 1285
    goto/16 :goto_2

    .line 1291
    :pswitch_10
    packed-switch p3, :pswitch_data_4

    goto/16 :goto_2

    .line 1293
    :pswitch_11
    const/16 v18, 0x2

    const/16 v19, 0x1

    sget-object v20, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 1294
    goto/16 :goto_2

    .line 1296
    :pswitch_12
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1297
    goto/16 :goto_2

    .line 1303
    :pswitch_13
    packed-switch p3, :pswitch_data_5

    goto/16 :goto_2

    .line 1305
    :pswitch_14
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1306
    goto/16 :goto_2

    .line 1308
    :pswitch_15
    const/16 v18, 0x2

    const/16 v19, 0x1

    sget-object v20, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 1309
    goto/16 :goto_2

    .line 1318
    :pswitch_16
    packed-switch p3, :pswitch_data_6

    goto/16 :goto_2

    .line 1320
    :pswitch_17
    const/16 v18, 0x7

    const/16 v19, 0x2

    sget-object v20, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 1321
    goto/16 :goto_2

    .line 1323
    :pswitch_18
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x2

    const/16 v20, 0x2

    sget-object v21, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1324
    goto/16 :goto_2

    .line 1330
    :pswitch_19
    packed-switch p3, :pswitch_data_7

    goto/16 :goto_2

    .line 1332
    :pswitch_1a
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x2

    const/16 v20, 0x2

    sget-object v21, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1333
    goto/16 :goto_2

    .line 1335
    :pswitch_1b
    const/16 v18, 0x7

    const/16 v19, 0x2

    sget-object v20, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 1336
    goto/16 :goto_2

    .line 1342
    :pswitch_1c
    packed-switch p3, :pswitch_data_8

    goto/16 :goto_2

    .line 1344
    :pswitch_1d
    const/16 v18, 0x7

    const/16 v19, 0x1

    sget-object v20, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 1345
    goto/16 :goto_2

    .line 1347
    :pswitch_1e
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1348
    goto/16 :goto_2

    .line 1350
    :pswitch_1f
    const/16 v18, 0x2

    const/16 v19, 0x1

    sget-object v20, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 1351
    goto/16 :goto_2

    .line 1357
    :pswitch_20
    packed-switch p3, :pswitch_data_9

    goto/16 :goto_2

    .line 1359
    :pswitch_21
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1360
    goto/16 :goto_2

    .line 1362
    :pswitch_22
    const/16 v18, 0x2

    const/16 v19, 0x1

    sget-object v20, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 1363
    goto/16 :goto_2

    .line 1369
    :pswitch_23
    packed-switch p3, :pswitch_data_a

    goto/16 :goto_2

    .line 1371
    :pswitch_24
    const/16 v18, 0x7

    const/16 v19, 0x2

    sget-object v20, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 1372
    goto/16 :goto_2

    .line 1374
    :pswitch_25
    new-instance v18, Ljava/lang/StringBuilder;

    const/16 v19, 0x2

    const/16 v20, 0x1

    sget-object v21, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1375
    goto/16 :goto_2

    .line 1377
    :pswitch_26
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1378
    goto/16 :goto_2

    .line 1384
    :pswitch_27
    packed-switch p3, :pswitch_data_b

    goto/16 :goto_2

    .line 1386
    :pswitch_28
    new-instance v18, Ljava/lang/StringBuilder;

    const/16 v19, 0x7

    const/16 v20, 0x2

    sget-object v21, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, " / "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1387
    const/16 v19, 0x2

    const/16 v20, 0x2

    sget-object v21, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1386
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1388
    goto/16 :goto_2

    .line 1398
    .end local v3    # "calendar":Ljava/util/Calendar;
    .end local v5    # "day":I
    .end local v6    # "hour":I
    .end local v8    # "meridiem":Ljava/lang/String;
    .end local v9    # "month":I
    .end local v16    # "two_year":I
    .end local v17    # "year":I
    :cond_7
    const-string v4, "None"

    goto/16 :goto_3

    .line 1218
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_9
        :pswitch_d
        :pswitch_10
        :pswitch_13
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_19
        :pswitch_1c
        :pswitch_20
        :pswitch_23
        :pswitch_27
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 1252
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 1264
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 1279
    :pswitch_data_3
    .packed-switch 0x2
        :pswitch_e
        :pswitch_f
    .end packed-switch

    .line 1291
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_11
        :pswitch_12
    .end packed-switch

    .line 1303
    :pswitch_data_5
    .packed-switch 0x2
        :pswitch_14
        :pswitch_15
    .end packed-switch

    .line 1318
    :pswitch_data_6
    .packed-switch 0x3
        :pswitch_17
        :pswitch_18
    .end packed-switch

    .line 1330
    :pswitch_data_7
    .packed-switch 0x3
        :pswitch_1a
        :pswitch_1b
    .end packed-switch

    .line 1342
    :pswitch_data_8
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
    .end packed-switch

    .line 1357
    :pswitch_data_9
    .packed-switch 0x2
        :pswitch_21
        :pswitch_22
    .end packed-switch

    .line 1369
    :pswitch_data_a
    .packed-switch 0x1
        :pswitch_24
        :pswitch_25
        :pswitch_26
    .end packed-switch

    .line 1384
    :pswitch_data_b
    .packed-switch 0x2
        :pswitch_28
    .end packed-switch
.end method

.method public getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "dateInfo"    # Ljava/lang/String;

    .prologue
    .line 1067
    const/4 v2, 0x0

    .line 1068
    .local v2, "dayOfTheWeek":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1069
    .local v6, "tempyy":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1070
    .local v5, "tempmm":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1073
    .local v4, "tempdd":Ljava/lang/String;
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    const/16 v9, 0xa

    if-lt v8, v9, :cond_2

    .line 1075
    const-string v8, "/"

    invoke-virtual {p1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1077
    const/4 v8, 0x0

    const/4 v9, 0x4

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 1078
    const/4 v8, 0x5

    const/4 v9, 0x7

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1079
    const/16 v8, 0x8

    const/16 v9, 0xa

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1081
    const-string v8, ":"

    const/4 v9, 0x6

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1083
    const/4 v8, 0x5

    const/4 v9, 0x6

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1084
    const/4 v8, 0x7

    const/16 v9, 0x9

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1085
    const-string v8, " "

    const/16 v9, 0x8

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1086
    const/4 v8, 0x7

    const/16 v9, 0x8

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1099
    :cond_0
    :goto_0
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 1100
    .local v7, "year":I
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1101
    .local v3, "month":I
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1103
    .local v1, "day":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1104
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 1105
    add-int/lit8 v8, v3, -0x1

    invoke-virtual {v0, v7, v8, v1}, Ljava/util/Calendar;->set(III)V

    .line 1107
    const/4 v8, 0x7

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 1131
    :goto_1
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 1139
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v1    # "day":I
    .end local v3    # "month":I
    .end local v7    # "year":I
    :goto_2
    return-object v2

    .line 1092
    :cond_1
    const/16 v8, 0x2f

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_0

    .line 1094
    const/4 v8, 0x0

    const/4 v9, 0x2

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1095
    const/4 v8, 0x3

    const/4 v9, 0x5

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1096
    const/4 v8, 0x6

    const/16 v9, 0xa

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 1110
    .restart local v0    # "calendar":Ljava/util/Calendar;
    .restart local v1    # "day":I
    .restart local v3    # "month":I
    .restart local v7    # "year":I
    :pswitch_0
    const-string v2, "SUNDAY"

    .line 1111
    goto :goto_1

    .line 1113
    :pswitch_1
    const-string v2, "MONDAY"

    .line 1114
    goto :goto_1

    .line 1116
    :pswitch_2
    const-string v2, "TUESDAY"

    .line 1117
    goto :goto_1

    .line 1119
    :pswitch_3
    const-string v2, "WEDNESDAY"

    .line 1120
    goto :goto_1

    .line 1122
    :pswitch_4
    const-string v2, "THURSDAY"

    .line 1123
    goto :goto_1

    .line 1125
    :pswitch_5
    const-string v2, "FRIDAY"

    .line 1126
    goto :goto_1

    .line 1128
    :pswitch_6
    const-string v2, "SATURDAY"

    goto :goto_1

    .line 1135
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v1    # "day":I
    .end local v3    # "month":I
    .end local v7    # "year":I
    :cond_2
    const-string v2, "None"

    goto :goto_2

    .line 1107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public getExifInfo(I)Ljava/lang/String;
    .locals 6
    .param p1, "exifTagId"    # I

    .prologue
    .line 830
    const/4 v2, 0x0

    .line 831
    .local v2, "exifInfo":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v4, :cond_1

    .line 833
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 835
    .local v3, "srcPath":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 836
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 838
    :cond_0
    new-instance v1, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    invoke-direct {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;-><init>()V

    .line 841
    .local v1, "exif":Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
    :try_start_0
    invoke-virtual {v1, v3}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->readExif(Ljava/lang/String;)V

    .line 842
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p1, v4}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getExifData(ILandroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 848
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 849
    const/4 v2, 0x0

    .line 853
    .end local v1    # "exif":Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
    .end local v3    # "srcPath":Ljava/lang/String;
    :cond_1
    return-object v2

    .line 843
    .restart local v1    # "exif":Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
    .restart local v3    # "srcPath":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 844
    .local v0, "e1":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 845
    .end local v0    # "e1":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 846
    .local v0, "e1":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFlipperLayout()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 7770
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mFlipperLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 77
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId()I
    .locals 1

    .prologue
    .line 7766
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    return v0
.end method

.method public getLocationInfoByParsing(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 3
    .param p1, "locationInfo"    # Ljava/lang/String;
    .param p2, "getTail"    # Z

    .prologue
    .line 857
    const/4 v1, 0x0

    .line 858
    .local v1, "ret":Ljava/lang/String;
    if-eqz p1, :cond_3

    .line 860
    const-string v2, "-"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 861
    .local v0, "idx":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    .line 863
    if-eqz p2, :cond_0

    .line 864
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 884
    .end local v0    # "idx":I
    :goto_0
    return-object v1

    .line 866
    .restart local v0    # "idx":I
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 867
    goto :goto_0

    .line 870
    :cond_1
    if-eqz p2, :cond_2

    .line 871
    const-string v1, ""

    goto :goto_0

    .line 873
    :cond_2
    move-object v1, p1

    .line 875
    goto :goto_0

    .line 878
    .end local v0    # "idx":I
    :cond_3
    if-eqz p2, :cond_4

    .line 879
    const-string v1, ""

    goto :goto_0

    .line 881
    :cond_4
    move-object v1, p1

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 72
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCityMaster()Z
    .locals 1

    .prologue
    .line 7937
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    return v0
.end method

.method public isThroughfareMaster()Z
    .locals 1

    .prologue
    .line 7941
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    return v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 119
    return-void
.end method

.method public releaseViewPager()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7774
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mTempBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 7775
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 7776
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->removeAllViews()V

    .line 7777
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mViewPagerParent:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 7778
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mViewPagerParent:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 7779
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mFlipperLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_2

    .line 7780
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mFlipperLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 7781
    :cond_2
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mFlipperLayout:Landroid/widget/FrameLayout;

    .line 7782
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mViewPagerParent:Landroid/widget/FrameLayout;

    .line 7783
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 7784
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    .line 7785
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 7789
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 7791
    return-void
.end method

.method public resetStampLayer()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 1785
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 1787
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1788
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1791
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 1793
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 1794
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1795
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1797
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 1798
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1799
    :cond_2
    return-void
.end method

.method public setBottomAlign(Z)V
    .locals 0
    .param p1, "isBottom"    # Z

    .prologue
    .line 7945
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    .line 7946
    return-void
.end method

.method public setExifCameraInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "exifInfo"    # Ljava/lang/String;

    .prologue
    .line 7927
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifCameraInfo:Ljava/lang/String;

    .line 7928
    return-void
.end method

.method public setExifInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "exifInfo"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 7874
    if-nez p1, :cond_5

    .line 7876
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const-string v0, "datetaken"

    aput-object v0, v2, v4

    .line 7877
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 7878
    .local v1, "uri":Landroid/net/Uri;
    if-nez v1, :cond_0

    .line 7880
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 7882
    :cond_0
    const/4 v6, 0x0

    .line 7883
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 7886
    .local v7, "date":Ljava/util/Date;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    .line 7888
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const/4 v10, 0x1

    aget-object v10, v2, v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, " DESC"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 7889
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7891
    new-instance v8, Ljava/util/Date;

    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct {v8, v4, v5}, Ljava/util/Date;-><init>(J)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7892
    .end local v7    # "date":Ljava/util/Date;
    .local v8, "date":Ljava/util/Date;
    :try_start_1
    invoke-direct {p0, v8}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->rearrangeDateInfo(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v7, v8

    .line 7897
    .end local v8    # "date":Ljava/util/Date;
    .restart local v7    # "date":Ljava/util/Date;
    :cond_1
    :goto_0
    if-eqz v6, :cond_2

    .line 7898
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 7901
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    if-eqz v0, :cond_3

    if-nez v7, :cond_4

    .line 7903
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getToday()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    .line 7911
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "date":Ljava/util/Date;
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    return-object v0

    .line 7894
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "date":Ljava/util/Date;
    :catch_0
    move-exception v9

    .line 7895
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    :goto_2
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0

    .line 7908
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "date":Ljava/util/Date;
    .end local v9    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_5
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifInfo:Ljava/lang/String;

    goto :goto_1

    .line 7894
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v8    # "date":Ljava/util/Date;
    :catch_1
    move-exception v9

    move-object v7, v8

    .end local v8    # "date":Ljava/util/Date;
    .restart local v7    # "date":Ljava/util/Date;
    goto :goto_2
.end method

.method public setExifLocationCityInfo(Ljava/lang/String;)V
    .locals 1
    .param p1, "exifInfo"    # Ljava/lang/String;

    .prologue
    .line 7915
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationCityInfo:Ljava/lang/String;

    .line 7916
    if-nez p1, :cond_0

    .line 7917
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mThroughfareIsMaster:Z

    .line 7918
    :cond_0
    return-void
.end method

.method public setExifLocationThroughfareInfo(Ljava/lang/String;)V
    .locals 1
    .param p1, "exifInfo"    # Ljava/lang/String;

    .prologue
    .line 7921
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mExifLocationThroughfareInfo:Ljava/lang/String;

    .line 7922
    if-nez p1, :cond_0

    .line 7923
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mCityIsMaster:Z

    .line 7924
    :cond_0
    return-void
.end method

.method public setOnCallback(Lcom/sec/android/mimage/photoretouching/Core/StampEffect$OnMyStampCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Core/StampEffect$OnMyStampCallback;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/StampEffect$OnMyStampCallback;

    .line 116
    return-void
.end method

.method public setResId(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 7948
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mId:I

    .line 7949
    return-void
.end method

.method public setResizedStampLayer(Landroid/widget/FrameLayout;)V
    .locals 0
    .param p1, "layout"    # Landroid/widget/FrameLayout;

    .prologue
    .line 1781
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    .line 1782
    return-void
.end method

.method public setStampLayer(II)V
    .locals 12
    .param p1, "where"    # I
    .param p2, "styleNum"    # I

    .prologue
    const v11, 0x7f05003a

    const/4 v10, 0x1

    const/4 v9, -0x1

    const v8, 0x7f05003c

    const/4 v7, 0x0

    .line 1462
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1464
    .local v3, "scale":F
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    if-eqz v5, :cond_0

    .line 1466
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 1467
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    .line 1469
    :cond_0
    new-instance v5, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    .line 1471
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v9, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1472
    .local v1, "param":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v5, :cond_1

    .line 1474
    if-ne p1, v10, :cond_5

    .line 1476
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    iput v5, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1477
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    iput v5, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1491
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1494
    :cond_1
    if-ne p1, v10, :cond_b

    .line 1496
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v5, :cond_2

    .line 1498
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    if-le v5, v6, :cond_7

    .line 1499
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 1503
    :goto_1
    sparse-switch p2, :sswitch_data_0

    .line 1516
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f05003b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v3

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    .line 1517
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    .line 1521
    :goto_2
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    .line 1524
    :cond_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_3

    .line 1526
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1527
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    .line 1530
    :cond_3
    new-instance v5, Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    .line 1531
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v9, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1532
    .local v0, "middleParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1534
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_4

    .line 1536
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1537
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    .line 1539
    :cond_4
    new-instance v5, Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    .line 1540
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    invoke-direct {v2, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1541
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1543
    packed-switch p2, :pswitch_data_0

    .line 1732
    .end local v0    # "middleParams":Landroid/widget/FrameLayout$LayoutParams;
    :goto_3
    :pswitch_0
    if-ne p1, v10, :cond_19

    .line 1734
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1735
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "JW setStampLayer: styleNum="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1736
    packed-switch p2, :pswitch_data_1

    .line 1778
    :goto_4
    :pswitch_1
    return-void

    .line 1479
    .end local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_5
    if-nez p1, :cond_6

    .line 1481
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1482
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 1488
    :cond_6
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    iput v5, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1489
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v5

    iput v5, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 1501
    :cond_7
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    goto/16 :goto_1

    .line 1512
    :sswitch_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v3

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    .line 1513
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    goto/16 :goto_2

    .line 1547
    .restart local v0    # "middleParams":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_8

    .line 1549
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1550
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    .line 1552
    :cond_8
    new-instance v5, Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    .line 1553
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    invoke-direct {v2, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1554
    .restart local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_3

    .line 1557
    :pswitch_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_9

    .line 1559
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1560
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    .line 1562
    :cond_9
    new-instance v5, Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    .line 1563
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    invoke-direct {v2, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1564
    .restart local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1566
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_a

    .line 1568
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1569
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    .line 1571
    :cond_a
    new-instance v5, Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    .line 1572
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    invoke-direct {v2, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1573
    .restart local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_3

    .line 1577
    .end local v0    # "middleParams":Landroid/widget/FrameLayout$LayoutParams;
    .end local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_b
    if-nez p1, :cond_11

    .line 1579
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v5, :cond_c

    .line 1588
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1589
    .local v4, "thumbnailWidth":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    int-to-float v6, v4

    div-float v3, v5, v6

    .line 1591
    sparse-switch p2, :sswitch_data_1

    .line 1604
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f05003b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v3

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    .line 1605
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    .line 1609
    :goto_5
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    .line 1612
    .end local v4    # "thumbnailWidth":I
    :cond_c
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_d

    .line 1614
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1615
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    .line 1617
    :cond_d
    new-instance v5, Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    .line 1618
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    invoke-direct {v2, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1619
    .restart local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1621
    packed-switch p2, :pswitch_data_2

    :pswitch_4
    goto/16 :goto_3

    .line 1625
    :pswitch_5
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_e

    .line 1627
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1628
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    .line 1630
    :cond_e
    new-instance v5, Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    .line 1631
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_3

    .line 1600
    .end local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v4    # "thumbnailWidth":I
    :sswitch_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v3

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    .line 1601
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    goto :goto_5

    .line 1634
    .end local v4    # "thumbnailWidth":I
    .restart local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :pswitch_6
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_f

    .line 1636
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1637
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    .line 1639
    :cond_f
    new-instance v5, Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    .line 1640
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1642
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_10

    .line 1644
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1645
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    .line 1647
    :cond_10
    new-instance v5, Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    .line 1648
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_3

    .line 1654
    .end local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_11
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v5, :cond_12

    .line 1656
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v6

    if-le v5, v6, :cond_16

    .line 1657
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 1661
    :goto_6
    sparse-switch p2, :sswitch_data_2

    .line 1674
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f05003b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v3

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    .line 1675
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    .line 1679
    :goto_7
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    .line 1681
    :cond_12
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_13

    .line 1683
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1684
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    .line 1687
    :cond_13
    new-instance v5, Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    .line 1688
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v9, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1689
    .restart local v0    # "middleParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1690
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    const/high16 v6, 0x19000000

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 1691
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1692
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1693
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_14

    .line 1695
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1696
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    .line 1698
    :cond_14
    new-instance v5, Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    .line 1699
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    invoke-direct {v2, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1700
    .restart local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1701
    packed-switch p2, :pswitch_data_3

    :pswitch_7
    goto/16 :goto_3

    .line 1705
    :pswitch_8
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_15

    .line 1707
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1708
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    .line 1710
    :cond_15
    new-instance v5, Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    .line 1711
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_3

    .line 1659
    .end local v0    # "middleParams":Landroid/widget/FrameLayout$LayoutParams;
    .end local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_16
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    goto/16 :goto_6

    .line 1670
    :sswitch_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v3

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    .line 1671
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    goto/16 :goto_7

    .line 1714
    .restart local v0    # "middleParams":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :pswitch_9
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_17

    .line 1716
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1717
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    .line 1719
    :cond_17
    new-instance v5, Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    .line 1720
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1722
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_18

    .line 1724
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1725
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    .line 1727
    :cond_18
    new-instance v5, Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    .line 1728
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_3

    .line 1740
    .end local v0    # "middleParams":Landroid/widget/FrameLayout$LayoutParams;
    :pswitch_a
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    .line 1743
    :pswitch_b
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1744
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    .line 1748
    :cond_19
    if-nez p1, :cond_1a

    .line 1750
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1751
    packed-switch p2, :pswitch_data_4

    :pswitch_c
    goto/16 :goto_4

    .line 1755
    :pswitch_d
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    .line 1758
    :pswitch_e
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1759
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    .line 1765
    :cond_1a
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1766
    packed-switch p2, :pswitch_data_5

    :pswitch_f
    goto/16 :goto_4

    .line 1770
    :pswitch_10
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    .line 1773
    :pswitch_11
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1774
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    .line 1503
    :sswitch_data_0
    .sparse-switch
        0x31500094 -> :sswitch_0
        0x31500099 -> :sswitch_0
        0x3150009e -> :sswitch_0
        0x3150009f -> :sswitch_0
        0x315000a6 -> :sswitch_0
        0x315000aa -> :sswitch_0
        0x315000ab -> :sswitch_0
        0x315000ac -> :sswitch_0
    .end sparse-switch

    .line 1543
    :pswitch_data_0
    .packed-switch 0x3150008b
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 1736
    :pswitch_data_1
    .packed-switch 0x3150008b
        :pswitch_a
        :pswitch_1
        :pswitch_b
        :pswitch_1
        :pswitch_1
        :pswitch_a
    .end packed-switch

    .line 1591
    :sswitch_data_1
    .sparse-switch
        0xa -> :sswitch_1
        0xf -> :sswitch_1
        0x14 -> :sswitch_1
        0x15 -> :sswitch_1
        0x1c -> :sswitch_1
        0x20 -> :sswitch_1
        0x21 -> :sswitch_1
        0x22 -> :sswitch_1
    .end sparse-switch

    .line 1621
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_4
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 1661
    :sswitch_data_2
    .sparse-switch
        0x31500094 -> :sswitch_2
        0x31500099 -> :sswitch_2
        0x3150009e -> :sswitch_2
        0x3150009f -> :sswitch_2
        0x315000a6 -> :sswitch_2
        0x315000aa -> :sswitch_2
        0x315000ab -> :sswitch_2
        0x315000ac -> :sswitch_2
    .end sparse-switch

    .line 1701
    :pswitch_data_3
    .packed-switch 0x3150008b
        :pswitch_8
        :pswitch_7
        :pswitch_9
        :pswitch_7
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 1751
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_e
        :pswitch_c
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 1766
    :pswitch_data_5
    .packed-switch 0x3150008b
        :pswitch_10
        :pswitch_f
        :pswitch_11
        :pswitch_f
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I
    .locals 22
    .param p1, "type"    # I
    .param p2, "resId"    # I
    .param p3, "stampLayerIndex"    # I
    .param p4, "textGravityX"    # I
    .param p5, "textGravityY"    # I
    .param p6, "textSize"    # F
    .param p7, "textWidth"    # I
    .param p8, "textHeight"    # I
    .param p9, "textHeight2"    # I
    .param p10, "textAlign"    # I
    .param p11, "textDegree"    # F
    .param p12, "stampGravityY"    # I
    .param p13, "stampGravityX"    # I
    .param p14, "marginX"    # I
    .param p15, "marginY"    # I
    .param p16, "noInfoMarginX"    # I
    .param p17, "noInfoMarginY"    # I
    .param p18, "noInfoRrc"    # I
    .param p19, "noInfoGravity"    # I
    .param p20, "textColor"    # I
    .param p21, "typeFace"    # Landroid/graphics/Typeface;
    .param p22, "exifInfo"    # Ljava/lang/String;
    .param p23, "isMaster"    # Z
    .param p24, "isFinish"    # Z

    .prologue
    .line 572
    if-eqz p2, :cond_3

    .line 574
    new-instance v14, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    invoke-direct {v14, v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 575
    .local v14, "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xb

    move/from16 v0, p13

    if-ne v0, v2, :cond_0

    .line 577
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v6}, Landroid/widget/RelativeLayout;->measure(II)V

    .line 578
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getMeasuredWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    sub-int/2addr v2, v4

    invoke-virtual {v14, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 580
    :cond_0
    move/from16 v0, p12

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 581
    move/from16 v0, p13

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 582
    const/4 v2, 0x1

    iput-boolean v2, v14, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 584
    packed-switch p3, :pswitch_data_0

    .line 601
    :cond_1
    :goto_0
    new-instance v11, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v11, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 602
    .local v11, "iv":Landroid/widget/ImageView;
    new-instance v12, Landroid/widget/FrameLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    const/16 v6, 0x11

    invoke-direct {v12, v2, v4, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 603
    .local v12, "ivParam":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 604
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v11, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 606
    const/4 v2, 0x1

    move/from16 v0, p2

    if-eq v0, v2, :cond_2

    .line 607
    move/from16 v0, p2

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 608
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_3

    .line 609
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v11}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 613
    .end local v11    # "iv":Landroid/widget/ImageView;
    .end local v12    # "ivParam":Landroid/widget/FrameLayout$LayoutParams;
    .end local v14    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    const/4 v2, -0x1

    move/from16 v0, p7

    if-eq v0, v2, :cond_4

    .line 614
    move/from16 v0, p7

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v2, v4

    float-to-int v0, v2

    move/from16 p7, v0

    .line 615
    :cond_4
    move/from16 v0, p8

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v2, v4

    float-to-int v0, v2

    move/from16 p8, v0

    .line 616
    new-instance v17, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 618
    .local v17, "tv":Landroid/widget/TextView;
    if-eqz p22, :cond_7

    .line 620
    const/4 v14, 0x0

    .line 621
    .local v14, "param":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v15, 0x0

    .line 622
    .local v15, "stampTextView":Landroid/view/View;
    const-string v2, "None"

    move-object/from16 v0, p22

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 624
    new-instance v11, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v11, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 625
    .restart local v11    # "iv":Landroid/widget/ImageView;
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v4, -0x2

    invoke-direct {v12, v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 626
    .local v12, "ivParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 627
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v11, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 628
    move/from16 v0, p18

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 630
    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    .end local v12    # "ivParam":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v12, Landroid/widget/RelativeLayout$LayoutParams;

    .line 631
    .restart local v12    # "ivParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v11, v2, v4}, Landroid/widget/ImageView;->measure(II)V

    .line 632
    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v12, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 633
    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v12, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 634
    move/from16 v0, p19

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 635
    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 638
    new-instance v14, Landroid/widget/FrameLayout$LayoutParams;

    .end local v14    # "param":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v2, -0x2

    or-int v4, p4, p5

    move/from16 v0, p7

    invoke-direct {v14, v0, v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 639
    .restart local v14    # "param":Landroid/widget/FrameLayout$LayoutParams;
    if-eqz p17, :cond_5

    .line 641
    const/16 v2, 0x30

    move/from16 v0, p5

    if-ne v0, v2, :cond_d

    .line 642
    move/from16 v0, p17

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v14, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 647
    :cond_5
    :goto_1
    if-eqz p16, :cond_6

    .line 649
    const/4 v2, 0x3

    move/from16 v0, p4

    if-ne v0, v2, :cond_e

    .line 650
    move/from16 v0, p16

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v14, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 655
    :cond_6
    :goto_2
    new-instance v18, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 656
    .local v18, "view":Landroid/widget/RelativeLayout;
    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 657
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 659
    move-object/from16 v15, v18

    .line 660
    const-string v2, "JW setStampStyle: noExif"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 757
    .end local v11    # "iv":Landroid/widget/ImageView;
    .end local v12    # "ivParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v18    # "view":Landroid/widget/RelativeLayout;
    :goto_3
    const/4 v14, 0x0

    .line 759
    packed-switch p3, :pswitch_data_1

    .line 776
    .end local v14    # "param":Landroid/widget/FrameLayout$LayoutParams;
    .end local v15    # "stampTextView":Landroid/view/View;
    :cond_7
    :goto_4
    const/4 v3, 0x0

    .line 777
    .local v3, "data":[I
    if-eqz p24, :cond_c

    .line 781
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_8

    .line 783
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v6}, Landroid/widget/RelativeLayout;->measure(II)V

    .line 784
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getMeasuredWidth()I

    move-result v5

    .line 785
    .local v5, "thumbW":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getMeasuredHeight()I

    move-result v9

    .line 788
    .local v9, "thumbH":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v6, v5, v9}, Landroid/widget/RelativeLayout;->layout(IIII)V

    .line 789
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->buildDrawingCache()V

    .line 798
    mul-int v2, v5, v9

    new-array v3, v2, [I

    .line 799
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v8, v5

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 802
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 804
    .end local v5    # "thumbW":I
    .end local v9    # "thumbH":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_9

    .line 806
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 807
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    .line 809
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_a

    .line 811
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 812
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    .line 814
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_b

    .line 816
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 817
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    .line 820
    :cond_b
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    .line 825
    :cond_c
    return-object v3

    .line 587
    .end local v3    # "data":[I
    .end local v17    # "tv":Landroid/widget/TextView;
    .local v14, "param":Landroid/widget/RelativeLayout$LayoutParams;
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_1

    .line 588
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v14}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 592
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_1

    .line 593
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v14}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 596
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_1

    .line 597
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v14}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 643
    .restart local v11    # "iv":Landroid/widget/ImageView;
    .restart local v12    # "ivParam":Landroid/widget/RelativeLayout$LayoutParams;
    .local v14, "param":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v15    # "stampTextView":Landroid/view/View;
    .restart local v17    # "tv":Landroid/widget/TextView;
    :cond_d
    const/16 v2, 0x50

    move/from16 v0, p5

    if-ne v0, v2, :cond_5

    .line 644
    move/from16 v0, p17

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v14, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_1

    .line 651
    :cond_e
    const/4 v2, 0x5

    move/from16 v0, p4

    if-ne v0, v2, :cond_6

    .line 652
    move/from16 v0, p16

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v14, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_2

    .line 664
    .end local v11    # "iv":Landroid/widget/ImageView;
    .end local v12    # "ivParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_f
    move-object/from16 v0, v17

    move/from16 v1, p10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 665
    move-object/from16 v0, v17

    move-object/from16 v1, p22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 666
    move-object/from16 v0, v17

    move-object/from16 v1, p21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 667
    move-object/from16 v0, v17

    move/from16 v1, p20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 668
    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    div-float v2, p6, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float p6, v2, v4

    .line 669
    const/4 v2, 0x1

    move-object/from16 v0, v17

    move/from16 v1, p6

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 670
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f040032

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v4, v6, v7}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 672
    const/4 v2, 0x0

    cmpl-float v2, p11, v2

    if-eqz v2, :cond_17

    .line 673
    const/16 p7, -0x1

    .line 674
    const-string v2, "None"

    move-object/from16 v0, p22

    if-ne v0, v2, :cond_10

    .line 675
    move/from16 v0, p6

    float-to-double v6, v0

    const-wide v20, 0x3fe999999999999aL    # 0.8

    mul-double v6, v6, v20

    double-to-float v0, v6

    move/from16 p6, v0

    .line 676
    const/4 v2, 0x1

    move-object/from16 v0, v17

    move/from16 v1, p6

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 678
    :cond_10
    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v4}, Landroid/widget/TextView;->measure(II)V

    .line 679
    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result p8

    .line 680
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    move/from16 v0, p7

    move/from16 v1, p8

    invoke-direct {v2, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 684
    :goto_5
    move-object/from16 v0, v17

    move/from16 v1, p11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setRotation(F)V

    .line 685
    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 686
    const/4 v2, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLines(I)V

    .line 688
    move/from16 v0, p14

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v2, v4

    float-to-int v0, v2

    move/from16 p14, v0

    .line 689
    move/from16 v0, p15

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v2, v4

    float-to-int v0, v2

    move/from16 p15, v0

    .line 691
    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v4}, Landroid/widget/TextView;->measure(II)V

    .line 692
    :goto_6
    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    if-gt v2, v4, :cond_18

    .line 708
    :goto_7
    const/4 v2, -0x1

    move/from16 v0, p7

    if-eq v0, v2, :cond_11

    .line 710
    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v4}, Landroid/widget/TextView;->measure(II)V

    .line 711
    :goto_8
    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    move/from16 v0, p7

    if-gt v2, v0, :cond_1a

    .line 727
    :cond_11
    :goto_9
    const/4 v10, 0x0

    .line 728
    .local v10, "extraMargin":I
    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    move/from16 v0, p8

    if-gt v2, v0, :cond_12

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    move/from16 v0, p8

    if-ge v2, v0, :cond_13

    .line 729
    :cond_12
    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v2, p8

    div-int/lit8 v10, v2, 0x2

    .line 731
    :cond_13
    const/4 v13, 0x0

    .line 732
    .local v13, "mergedMargin":I
    if-eqz p23, :cond_14

    .line 734
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    if-eqz v2, :cond_1c

    .line 735
    move/from16 v13, p8

    .line 739
    :cond_14
    :goto_a
    new-instance v14, Landroid/widget/FrameLayout$LayoutParams;

    .end local v14    # "param":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v2, -0x2

    or-int v4, p4, p5

    move/from16 v0, p7

    invoke-direct {v14, v0, v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 740
    .restart local v14    # "param":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v2, 0x30

    move/from16 v0, p5

    if-ne v0, v2, :cond_1d

    .line 741
    sub-int v2, p15, v10

    add-int/2addr v2, v13

    iput v2, v14, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 745
    :cond_15
    :goto_b
    const/4 v2, 0x3

    move/from16 v0, p4

    if-ne v0, v2, :cond_1e

    .line 746
    move/from16 v0, p14

    iput v0, v14, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 750
    :cond_16
    :goto_c
    new-instance v18, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 751
    .local v18, "view":Landroid/widget/LinearLayout;
    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 752
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 754
    move-object/from16 v15, v18

    goto/16 :goto_3

    .line 683
    .end local v10    # "extraMargin":I
    .end local v13    # "mergedMargin":I
    .end local v18    # "view":Landroid/widget/LinearLayout;
    :cond_17
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    move/from16 v0, p7

    invoke-direct {v2, v0, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_5

    .line 694
    :cond_18
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    int-to-float v2, v2

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    div-float v16, v2, v4

    .line 695
    .local v16, "textScale":F
    mul-float p6, p6, v16

    .line 697
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMimimunTextSize:F

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v2, v4

    cmpg-float v2, p6, v2

    if-gez v2, :cond_19

    .line 699
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 700
    const-string v2, "JW setEllipsize"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 704
    :cond_19
    const/4 v2, 0x1

    move-object/from16 v0, v17

    move/from16 v1, p6

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 705
    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v4}, Landroid/widget/TextView;->measure(II)V

    goto/16 :goto_6

    .line 713
    .end local v16    # "textScale":F
    :cond_1a
    move/from16 v0, p7

    int-to-float v2, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    div-float v16, v2, v4

    .line 714
    .restart local v16    # "textScale":F
    mul-float p6, p6, v16

    .line 716
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMimimunTextSize:F

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v2, v4

    cmpg-float v2, p6, v2

    if-gez v2, :cond_1b

    .line 718
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 719
    const-string v2, "JW setEllipsize"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_9

    .line 723
    :cond_1b
    const/4 v2, 0x1

    move-object/from16 v0, v17

    move/from16 v1, p6

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 724
    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v4}, Landroid/widget/TextView;->measure(II)V

    goto/16 :goto_8

    .line 737
    .end local v16    # "textScale":F
    .restart local v10    # "extraMargin":I
    .restart local v13    # "mergedMargin":I
    :cond_1c
    move/from16 v0, p9

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v2, v4

    float-to-int v2, v2

    div-int/lit8 v13, v2, 0x2

    goto/16 :goto_a

    .line 742
    :cond_1d
    const/16 v2, 0x50

    move/from16 v0, p5

    if-ne v0, v2, :cond_15

    .line 743
    sub-int v2, p15, v10

    add-int/2addr v2, v13

    iput v2, v14, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_b

    .line 747
    :cond_1e
    const/4 v2, 0x5

    move/from16 v0, p4

    if-ne v0, v2, :cond_16

    .line 748
    move/from16 v0, p14

    iput v0, v14, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_c

    .line 762
    .end local v10    # "extraMargin":I
    .end local v13    # "mergedMargin":I
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_7

    .line 763
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v15}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    .line 766
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_7

    .line 767
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v15}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    .line 770
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_7

    .line 771
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizedStampLayer3:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v15}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    .line 584
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 759
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public declared-synchronized setStampStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)V
    .locals 28
    .param p1, "type"    # I
    .param p2, "resId"    # I
    .param p3, "stampLayerIndex"    # I
    .param p4, "textGravityX"    # I
    .param p5, "textGravityY"    # I
    .param p6, "textSize"    # F
    .param p7, "textWidth"    # I
    .param p8, "textHeight"    # I
    .param p9, "textHeight2"    # I
    .param p10, "textAlign"    # I
    .param p11, "textDegree"    # F
    .param p12, "stampGravityY"    # I
    .param p13, "stampGravityX"    # I
    .param p14, "marginX"    # I
    .param p15, "marginY"    # I
    .param p16, "noInfoMarginX"    # I
    .param p17, "noInfoMarginY"    # I
    .param p18, "noInfoRrc"    # I
    .param p19, "noInfoGravity"    # I
    .param p20, "textColor"    # I
    .param p21, "typeFace"    # Landroid/graphics/Typeface;
    .param p22, "exifInfo"    # Ljava/lang/String;
    .param p23, "isMaster"    # Z
    .param p24, "isFinish"    # Z

    .prologue
    .line 145
    monitor-enter p0

    if-eqz p2, :cond_5

    .line 147
    const/16 v19, 0x0

    .line 148
    .local v19, "params":Landroid/widget/FrameLayout$LayoutParams;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    if-eqz v5, :cond_0

    .line 149
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    .end local v19    # "params":Landroid/widget/FrameLayout$LayoutParams;
    check-cast v19, Landroid/widget/FrameLayout$LayoutParams;

    .line 151
    .restart local v19    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    const/16 v17, 0x0

    .line 152
    .local v17, "padding":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v5, :cond_1

    .line 154
    packed-switch p1, :pswitch_data_0

    .line 163
    :goto_0
    move/from16 v0, v17

    move-object/from16 v1, v19

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 164
    move-object/from16 v0, v19

    iget v5, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    move-object/from16 v0, v19

    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 165
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    if-eqz v5, :cond_1

    .line 166
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 169
    :cond_1
    new-instance v18, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    move-object/from16 v0, v18

    invoke-direct {v0, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 170
    .local v18, "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v5, 0xb

    move/from16 v0, p13

    if-ne v0, v5, :cond_2

    .line 172
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/widget/RelativeLayout;->measure(II)V

    .line 173
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getMeasuredWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    sub-int/2addr v5, v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 175
    :cond_2
    move-object/from16 v0, v18

    move/from16 v1, p12

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 176
    move-object/from16 v0, v18

    move/from16 v1, p13

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 177
    const/4 v5, 0x1

    move-object/from16 v0, v18

    iput-boolean v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 179
    const/4 v5, 0x1

    move/from16 v0, p3

    if-ne v0, v5, :cond_14

    .line 181
    packed-switch p1, :pswitch_data_1

    .line 224
    :cond_3
    :goto_1
    new-instance v14, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v14, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 225
    .local v14, "iv":Landroid/widget/ImageView;
    new-instance v15, Landroid/widget/FrameLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseH:I

    const/16 v7, 0x11

    invoke-direct {v15, v5, v6, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 226
    .local v15, "ivParam":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 227
    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v14, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 228
    const/4 v5, 0x1

    move/from16 v0, p2

    if-eq v0, v5, :cond_4

    .line 229
    move/from16 v0, p2

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 231
    :cond_4
    packed-switch p1, :pswitch_data_2

    .line 245
    .end local v14    # "iv":Landroid/widget/ImageView;
    .end local v15    # "ivParam":Landroid/widget/FrameLayout$LayoutParams;
    .end local v17    # "padding":I
    .end local v18    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v19    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :cond_5
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "JW mResizeRatio="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 246
    const/4 v5, -0x1

    move/from16 v0, p7

    if-eq v0, v5, :cond_6

    .line 247
    move/from16 v0, p7

    int-to-float v5, v0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v5, v6

    float-to-int v0, v5

    move/from16 p7, v0

    .line 249
    :cond_6
    move/from16 v0, p8

    int-to-float v5, v0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v5, v6

    float-to-int v0, v5

    move/from16 p8, v0

    .line 250
    new-instance v26, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-direct {v0, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 252
    .local v26, "tv":Landroid/widget/TextView;
    if-eqz p22, :cond_9

    .line 254
    const/16 v18, 0x0

    .line 255
    .local v18, "param":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v22, 0x0

    .line 256
    .local v22, "stampTextView":Landroid/view/View;
    const-string v5, "None"

    move-object/from16 v0, p22

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 258
    new-instance v14, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v14, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 259
    .restart local v14    # "iv":Landroid/widget/ImageView;
    new-instance v15, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v5, -0x2

    const/4 v6, -0x2

    invoke-direct {v15, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 260
    .local v15, "ivParam":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 261
    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v14, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 262
    move/from16 v0, p18

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 264
    invoke-virtual {v14}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    .end local v15    # "ivParam":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v15, Landroid/widget/RelativeLayout$LayoutParams;

    .line 265
    .restart local v15    # "ivParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v14, v5, v6}, Landroid/widget/ImageView;->measure(II)V

    .line 266
    invoke-virtual {v14}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v5

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v15, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 267
    invoke-virtual {v14}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v15, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 268
    move/from16 v0, p19

    invoke-virtual {v15, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 269
    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 272
    new-instance v18, Landroid/widget/FrameLayout$LayoutParams;

    .end local v18    # "param":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v5, -0x2

    or-int v6, p4, p5

    move-object/from16 v0, v18

    move/from16 v1, p7

    invoke-direct {v0, v1, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 273
    .restart local v18    # "param":Landroid/widget/FrameLayout$LayoutParams;
    if-eqz p17, :cond_7

    .line 275
    const/16 v5, 0x30

    move/from16 v0, p5

    if-ne v0, v5, :cond_16

    .line 276
    move/from16 v0, p17

    int-to-float v5, v0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    move-object/from16 v0, v18

    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 281
    :cond_7
    :goto_3
    if-eqz p16, :cond_8

    .line 283
    const/4 v5, 0x3

    move/from16 v0, p4

    if-ne v0, v5, :cond_17

    .line 284
    move/from16 v0, p16

    int-to-float v5, v0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    move-object/from16 v0, v18

    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 289
    :cond_8
    :goto_4
    new-instance v27, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    move-object/from16 v0, v27

    invoke-direct {v0, v5}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 290
    .local v27, "view":Landroid/widget/RelativeLayout;
    move-object/from16 v0, v27

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 291
    move-object/from16 v0, v27

    invoke-virtual {v0, v14}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 293
    move-object/from16 v22, v27

    .line 294
    const-string v5, "JW setStampStyle: noExif"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 399
    .end local v14    # "iv":Landroid/widget/ImageView;
    .end local v15    # "ivParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v27    # "view":Landroid/widget/RelativeLayout;
    :goto_5
    const/16 v18, 0x0

    .line 401
    const/4 v5, 0x1

    move/from16 v0, p3

    if-ne v0, v5, :cond_28

    .line 403
    packed-switch p1, :pswitch_data_3

    .line 446
    .end local v18    # "param":Landroid/widget/FrameLayout$LayoutParams;
    .end local v22    # "stampTextView":Landroid/view/View;
    :cond_9
    :goto_6
    if-eqz p24, :cond_13

    .line 448
    const/16 v21, 0x0

    .line 449
    .local v21, "stampLayerBitmap":Landroid/graphics/Bitmap;
    const/16 v20, 0x0

    .line 450
    .local v20, "stampCanvas":Landroid/graphics/Canvas;
    const/4 v4, 0x0

    .line 451
    .local v4, "viewBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v5, :cond_c

    .line 453
    packed-switch p1, :pswitch_data_4

    .line 488
    :cond_a
    :goto_7
    packed-switch p1, :pswitch_data_5

    .line 504
    :cond_b
    :goto_8
    packed-switch p1, :pswitch_data_6

    .line 516
    :cond_c
    :goto_9
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mDone:Z

    if-eqz v5, :cond_d

    .line 518
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mTempBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 519
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 520
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mDone:Z

    .line 521
    new-instance v12, Landroid/graphics/Canvas;

    invoke-direct {v12, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 522
    .local v12, "canvas":Landroid/graphics/Canvas;
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v21

    invoke-virtual {v12, v0, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 523
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 526
    .end local v12    # "canvas":Landroid/graphics/Canvas;
    :cond_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 527
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    .line 528
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 529
    invoke-static/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 530
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 532
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_e

    .line 534
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 535
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    .line 537
    :cond_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_f

    .line 539
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 540
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    .line 542
    :cond_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_10

    .line 544
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 545
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    .line 547
    :cond_10
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_11

    .line 549
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 550
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    .line 552
    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_12

    .line 554
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 555
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    .line 557
    :cond_12
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_13

    .line 559
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 560
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    .line 565
    .end local v4    # "viewBitmap":Landroid/graphics/Bitmap;
    .end local v20    # "stampCanvas":Landroid/graphics/Canvas;
    .end local v21    # "stampLayerBitmap":Landroid/graphics/Bitmap;
    :cond_13
    const-string v5, "JW setstampStyle end"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 566
    monitor-exit p0

    return-void

    .line 157
    .end local v26    # "tv":Landroid/widget/TextView;
    .restart local v17    # "padding":I
    .restart local v19    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :pswitch_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v17

    .line 158
    goto/16 :goto_0

    .line 160
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalPaddingY()F

    move-result v5

    float-to-int v0, v5

    move/from16 v17, v0

    goto/16 :goto_0

    .line 184
    .local v18, "param":Landroid/widget/RelativeLayout$LayoutParams;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_3

    .line 185
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 145
    .end local v17    # "padding":I
    .end local v18    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v19    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 189
    .restart local v17    # "padding":I
    .restart local v18    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v19    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :pswitch_3
    :try_start_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_3

    .line 190
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 194
    :cond_14
    const/4 v5, 0x2

    move/from16 v0, p3

    if-ne v0, v5, :cond_15

    .line 196
    packed-switch p1, :pswitch_data_7

    goto/16 :goto_1

    .line 199
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_3

    .line 200
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 204
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_3

    .line 205
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 211
    :cond_15
    packed-switch p1, :pswitch_data_8

    goto/16 :goto_1

    .line 214
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_3

    .line 215
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 218
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_3

    .line 219
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 234
    .restart local v14    # "iv":Landroid/widget/ImageView;
    .local v15, "ivParam":Landroid/widget/FrameLayout$LayoutParams;
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_5

    .line 235
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v14}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_2

    .line 238
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_5

    .line 239
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v14}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_2

    .line 277
    .end local v17    # "padding":I
    .end local v19    # "params":Landroid/widget/FrameLayout$LayoutParams;
    .local v15, "ivParam":Landroid/widget/RelativeLayout$LayoutParams;
    .local v18, "param":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v22    # "stampTextView":Landroid/view/View;
    .restart local v26    # "tv":Landroid/widget/TextView;
    :cond_16
    const/16 v5, 0x50

    move/from16 v0, p5

    if-ne v0, v5, :cond_7

    .line 278
    move/from16 v0, p17

    int-to-float v5, v0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    move-object/from16 v0, v18

    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_3

    .line 285
    :cond_17
    const/4 v5, 0x5

    move/from16 v0, p4

    if-ne v0, v5, :cond_8

    .line 286
    move/from16 v0, p16

    int-to-float v5, v0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    move-object/from16 v0, v18

    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_4

    .line 298
    .end local v14    # "iv":Landroid/widget/ImageView;
    .end local v15    # "ivParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_18
    move-object/from16 v0, v26

    move/from16 v1, p10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 299
    move-object/from16 v0, v26

    move-object/from16 v1, p22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 300
    move-object/from16 v0, v26

    move-object/from16 v1, p21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 301
    move-object/from16 v0, v26

    move/from16 v1, p20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 302
    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    div-float v5, p6, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float p6, v5, v6

    .line 304
    const/4 v5, 0x1

    move-object/from16 v0, v26

    move/from16 v1, p6

    invoke-virtual {v0, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 305
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f040032

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 307
    const/16 v23, 0x0

    .line 308
    .local v23, "textParam":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v5, 0x0

    cmpl-float v5, p11, v5

    if-eqz v5, :cond_20

    .line 309
    const/16 p7, -0x1

    .line 310
    const-string v5, "None"

    move-object/from16 v0, p22

    if-ne v0, v5, :cond_19

    .line 311
    move/from16 v0, p6

    float-to-double v6, v0

    const-wide v8, 0x3fe999999999999aL    # 0.8

    mul-double/2addr v6, v8

    double-to-float v0, v6

    move/from16 p6, v0

    .line 312
    const/4 v5, 0x1

    move-object/from16 v0, v26

    move/from16 v1, p6

    invoke-virtual {v0, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 314
    :cond_19
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 315
    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result p8

    .line 316
    new-instance v23, Landroid/widget/LinearLayout$LayoutParams;

    .end local v23    # "textParam":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, v23

    move/from16 v1, p7

    move/from16 v2, p8

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 322
    .restart local v23    # "textParam":Landroid/widget/LinearLayout$LayoutParams;
    :goto_a
    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 323
    move-object/from16 v0, v26

    move/from16 v1, p11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setRotation(F)V

    .line 324
    const/4 v5, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 325
    const/4 v5, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setLines(I)V

    .line 328
    move/from16 v0, p14

    int-to-float v5, v0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v5, v6

    float-to-int v0, v5

    move/from16 p14, v0

    .line 329
    move/from16 v0, p15

    int-to-float v5, v0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v5, v6

    float-to-int v0, v5

    move/from16 p15, v0

    .line 331
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 332
    :goto_b
    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    if-gt v5, v6, :cond_21

    .line 348
    :goto_c
    const/4 v5, -0x1

    move/from16 v0, p7

    if-eq v0, v5, :cond_1a

    .line 350
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 351
    :goto_d
    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    move/from16 v0, p7

    if-gt v5, v0, :cond_23

    .line 367
    :cond_1a
    :goto_e
    const/4 v13, 0x0

    .line 368
    .local v13, "extraMargin":I
    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    move/from16 v0, p8

    if-gt v5, v0, :cond_1b

    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    move/from16 v0, p8

    if-ge v5, v0, :cond_1c

    .line 369
    :cond_1b
    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v5, p8

    div-int/lit8 v13, v5, 0x2

    .line 371
    :cond_1c
    const/16 v16, 0x0

    .line 372
    .local v16, "mergedMargin":I
    if-eqz p23, :cond_1d

    .line 374
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBottomAlign:Z

    if-eqz v5, :cond_25

    .line 375
    move/from16 v16, p8

    .line 379
    :cond_1d
    :goto_f
    new-instance v18, Landroid/widget/FrameLayout$LayoutParams;

    .end local v18    # "param":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v5, -0x2

    or-int v6, p4, p5

    move-object/from16 v0, v18

    move/from16 v1, p7

    invoke-direct {v0, v1, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 380
    .restart local v18    # "param":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v5, 0x30

    move/from16 v0, p5

    if-ne v0, v5, :cond_26

    .line 381
    sub-int v5, p15, v13

    add-int v5, v5, v16

    move-object/from16 v0, v18

    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 385
    :cond_1e
    :goto_10
    const/4 v5, 0x3

    move/from16 v0, p4

    if-ne v0, v5, :cond_27

    .line 386
    move/from16 v0, p14

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 390
    :cond_1f
    :goto_11
    new-instance v27, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    move-object/from16 v0, v27

    invoke-direct {v0, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 391
    .local v27, "view":Landroid/widget/LinearLayout;
    move-object/from16 v0, v27

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 392
    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 394
    move-object/from16 v22, v27

    goto/16 :goto_5

    .line 319
    .end local v13    # "extraMargin":I
    .end local v16    # "mergedMargin":I
    .end local v27    # "view":Landroid/widget/LinearLayout;
    :cond_20
    new-instance v23, Landroid/widget/LinearLayout$LayoutParams;

    .end local v23    # "textParam":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v5, -0x2

    move-object/from16 v0, v23

    move/from16 v1, p7

    invoke-direct {v0, v1, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .restart local v23    # "textParam":Landroid/widget/LinearLayout$LayoutParams;
    goto/16 :goto_a

    .line 334
    :cond_21
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mBaseW:I

    int-to-float v5, v5

    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v24, v5, v6

    .line 335
    .local v24, "textScale":F
    mul-float p6, p6, v24

    .line 336
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "JW textSize="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p6

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 337
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMimimunTextSize:F

    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v5, v6

    cmpg-float v5, p6, v5

    if-gez v5, :cond_22

    .line 339
    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 340
    const-string v5, "JW setEllipsize"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_c

    .line 344
    :cond_22
    const/4 v5, 0x1

    move-object/from16 v0, v26

    move/from16 v1, p6

    invoke-virtual {v0, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 345
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v6}, Landroid/widget/TextView;->measure(II)V

    goto/16 :goto_b

    .line 353
    .end local v24    # "textScale":F
    :cond_23
    move/from16 v0, p7

    int-to-float v5, v0

    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v24, v5, v6

    .line 354
    .restart local v24    # "textScale":F
    mul-float p6, p6, v24

    .line 356
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMimimunTextSize:F

    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v5, v6

    cmpg-float v5, p6, v5

    if-gez v5, :cond_24

    .line 358
    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 359
    const-string v5, "JW setEllipsize"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 363
    :cond_24
    const/4 v5, 0x1

    move-object/from16 v0, v26

    move/from16 v1, p6

    invoke-virtual {v0, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 364
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v6}, Landroid/widget/TextView;->measure(II)V

    goto/16 :goto_d

    .line 377
    .end local v24    # "textScale":F
    .restart local v13    # "extraMargin":I
    .restart local v16    # "mergedMargin":I
    :cond_25
    move/from16 v0, p9

    int-to-float v5, v0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mResizeRatio:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    div-int/lit8 v16, v5, 0x2

    goto/16 :goto_f

    .line 382
    :cond_26
    const/16 v5, 0x50

    move/from16 v0, p5

    if-ne v0, v5, :cond_1e

    .line 383
    sub-int v5, p15, v13

    add-int v5, v5, v16

    move-object/from16 v0, v18

    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_10

    .line 387
    :cond_27
    const/4 v5, 0x5

    move/from16 v0, p4

    if-ne v0, v5, :cond_1f

    .line 388
    move/from16 v0, p14

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_11

    .line 406
    .end local v13    # "extraMargin":I
    .end local v16    # "mergedMargin":I
    .end local v23    # "textParam":Landroid/widget/LinearLayout$LayoutParams;
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_9

    .line 407
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 410
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_9

    .line 411
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer:Landroid/widget/FrameLayout;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 415
    :cond_28
    const/4 v5, 0x2

    move/from16 v0, p3

    if-ne v0, v5, :cond_29

    .line 417
    packed-switch p1, :pswitch_data_9

    goto/16 :goto_6

    .line 420
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_9

    .line 421
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer2:Landroid/widget/FrameLayout;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 424
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_9

    .line 425
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer2:Landroid/widget/FrameLayout;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 431
    :cond_29
    packed-switch p1, :pswitch_data_a

    goto/16 :goto_6

    .line 434
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_2a

    .line 435
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer3:Landroid/widget/FrameLayout;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 436
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    move-object/from16 v22, v0

    .line 437
    goto/16 :goto_6

    .line 439
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_9

    .line 440
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mOriginalStampLayer3:Landroid/widget/FrameLayout;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 456
    .end local v18    # "param":Landroid/widget/FrameLayout$LayoutParams;
    .end local v22    # "stampTextView":Landroid/view/View;
    .restart local v4    # "viewBitmap":Landroid/graphics/Bitmap;
    .restart local v20    # "stampCanvas":Landroid/graphics/Canvas;
    .restart local v21    # "stampLayerBitmap":Landroid/graphics/Bitmap;
    :pswitch_10
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 458
    new-instance v25, Landroid/graphics/Canvas;

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 459
    .local v25, "transparentCanvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    if-eqz v5, :cond_a

    .line 461
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/widget/RelativeLayout;->measure(II)V

    .line 462
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v9

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/widget/RelativeLayout;->layout(IIII)V

    .line 463
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_7

    .line 469
    .end local v25    # "transparentCanvas":Landroid/graphics/Canvas;
    :pswitch_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v6

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 470
    new-instance v20, Landroid/graphics/Canvas;

    .end local v20    # "stampCanvas":Landroid/graphics/Canvas;
    invoke-direct/range {v20 .. v21}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 472
    .restart local v20    # "stampCanvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_2b

    .line 474
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/widget/LinearLayout;->measure(II)V

    .line 475
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v9

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 476
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 479
    :cond_2b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    if-eqz v5, :cond_a

    .line 481
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/widget/RelativeLayout;->measure(II)V

    .line 482
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v9

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/widget/RelativeLayout;->layout(IIII)V

    .line 483
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampParentLayer:Landroid/widget/RelativeLayout;

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_7

    .line 491
    :pswitch_12
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mDone:Z

    if-eqz v5, :cond_b

    .line 494
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 496
    goto/16 :goto_8

    .line 498
    :pswitch_13
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v6

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 499
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 500
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v11

    .line 499
    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    goto/16 :goto_8

    .line 507
    :pswitch_14
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v6, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mTempBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_9

    .line 510
    :pswitch_15
    new-instance v12, Landroid/graphics/Canvas;

    invoke-direct {v12, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 511
    .restart local v12    # "canvas":Landroid/graphics/Canvas;
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v21

    invoke-virtual {v12, v0, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 512
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer(Landroid/graphics/Bitmap;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_9

    .line 154
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 181
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 231
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 403
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 453
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_10
        :pswitch_11
    .end packed-switch

    .line 488
    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_12
        :pswitch_13
    .end packed-switch

    .line 504
    :pswitch_data_6
    .packed-switch 0x1
        :pswitch_14
        :pswitch_15
    .end packed-switch

    .line 196
    :pswitch_data_7
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 211
    :pswitch_data_8
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 417
    :pswitch_data_9
    .packed-switch 0x1
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 431
    :pswitch_data_a
    .packed-switch 0x1
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public setStampViewVisibility(I)V
    .locals 6
    .param p1, "visibility"    # I

    .prologue
    const/4 v5, 0x0

    const/16 v2, 0x8

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 1806
    if-nez p1, :cond_2

    .line 1812
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 1814
    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    .line 1815
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    .line 1816
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1817
    .local v0, "middleParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1818
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    const/high16 v2, 0x19000000

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 1819
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1821
    .end local v0    # "middleParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_1

    .line 1822
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1835
    :cond_1
    :goto_0
    return-void

    .line 1826
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_3

    .line 1828
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1829
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1830
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mStampLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1832
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_1

    .line 1833
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->mMiddleLayer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
