.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$14;
.super Ljava/lang/Object;
.source "PenActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenControlListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->newSpenListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$14;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    .line 1131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClosed(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1149
    .local p1, "arg0":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onCreated(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ILandroid/graphics/PointF;)Z
    .locals 1
    .param p5, "arg4"    # I
    .param p6, "arg5"    # Landroid/graphics/PointF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Rect;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I",
            "Landroid/graphics/PointF;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1169
    .local p1, "arg0":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    .local p2, "arg1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    .local p3, "arg2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;>;"
    .local p4, "arg3":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-nez p1, :cond_0

    .line 1170
    const/4 v0, 0x0

    .line 1172
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onMenuSelected(Ljava/util/ArrayList;I)Z
    .locals 1
    .param p2, "arg1"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .line 1142
    .local p1, "arg0":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onObjectChanged(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1137
    .local p1, "arg0":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    return-void
.end method

.method public onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 0
    .param p1, "arg0"    # Landroid/graphics/RectF;
    .param p2, "arg1"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 1156
    return-void
.end method

.method public onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 0
    .param p1, "arg0"    # F
    .param p2, "arg1"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 1162
    return-void
.end method
