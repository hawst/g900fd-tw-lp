.class Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$3;
.super Ljava/lang/Object;
.source "CopyToView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->initButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 222
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 197
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v1

    const-string v2, "CopyToAnother"

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isDoNotShow(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 216
    :goto_0
    return-void

    .line 203
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 192
    return-void
.end method
