.class Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;
.super Ljava/lang/Object;
.source "RotateView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->initEffect()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    return-object v0
.end method


# virtual methods
.method public ableDone()V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 190
    return-void
.end method

.method public actionBarIsEnableDone()Z
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledDone()Z

    move-result v0

    return v0
.end method

.method public getImageEditViewHeight()I
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewHeight()I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)I

    move-result v0

    return v0
.end method

.method public getImageEditViewWidth()I
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewWidth()I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)I

    move-result v0

    return v0
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->invalidateViews()V

    .line 180
    return-void
.end method

.method public setBottomButtonEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setEnabled(Z)V

    .line 175
    return-void
.end method

.method public setEnabledWithChildren(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 151
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setEnabledWithChildren(Z)V

    .line 153
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 170
    return-void

    .line 154
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 155
    .local v1, "v":Landroid/widget/LinearLayout;
    instance-of v2, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v2, :cond_1

    .line 156
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v2

    const v3, 0x11001108

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEnableDecoration()Z

    move-result v2

    if-nez v2, :cond_1

    .line 157
    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .end local v1    # "v":Landroid/widget/LinearLayout;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setEnabledWidthChildren(Z)V

    .line 158
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1$1;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 153
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public unableDone()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 147
    return-void
.end method
