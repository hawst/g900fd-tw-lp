.class Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$3;
.super Ljava/lang/Object;
.source "DecodeAsync.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->finishTask(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;

.field private final synthetic val$idx:I


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$3;->this$1:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;

    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$3;->val$idx:I

    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : taskFinish : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$3;->val$idx:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 226
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$3;->val$idx:I

    if-gez v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$3;->this$1:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->access$0(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressListener:Landroid/content/DialogInterface$OnCancelListener;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$15(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/content/DialogInterface$OnCancelListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/content/DialogInterface$OnCancelListener;->onCancel(Landroid/content/DialogInterface;)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$3;->this$1:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->access$0(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$5(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    move-result-object v0

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;->changeViewStatus(I)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$3;->this$1:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->access$0(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->destroy()V

    .line 239
    :goto_0
    return-void

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$3;->this$1:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->access$0(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$14(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v0

    sget v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->MIN_ITEM_CNT:I

    if-lt v0, v1, :cond_1

    .line 235
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$3;->this$1:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->access$0(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$14(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$3;->val$idx:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->changeImage(I)Z

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$3;->this$1:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->access$0(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$5(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;->changeViewStatus(I)V

    goto :goto_0
.end method
