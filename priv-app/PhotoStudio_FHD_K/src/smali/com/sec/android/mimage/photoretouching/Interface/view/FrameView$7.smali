.class Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$7;
.super Ljava/lang/Object;
.source "FrameView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->init3DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    .line 720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 0
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 737
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 724
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableCancel()V

    .line 725
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 726
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->doCancel()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    .line 729
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    if-nez v0, :cond_0

    .line 730
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->openDrawer()V

    .line 731
    :cond_0
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 743
    return-void
.end method
