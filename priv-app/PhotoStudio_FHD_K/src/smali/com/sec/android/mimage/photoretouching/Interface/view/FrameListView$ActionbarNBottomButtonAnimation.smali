.class Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;
.super Ljava/lang/Object;
.source "FrameListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionbarNBottomButtonAnimation"
.end annotation


# static fields
.field public static final TOUCH_OFFSET:I = 0x64


# instance fields
.field private mIsShowing:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 317
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    .line 316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 318
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 319
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 338
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 339
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->hide()V

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    if-eqz v0, :cond_1

    .line 345
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->indicatorHide(Z)V

    .line 347
    :cond_1
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    return v0
.end method

.method public show()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 326
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 327
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->show()V

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    if-eqz v0, :cond_1

    .line 333
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->indicatorShow(Z)V

    .line 335
    :cond_1
    return-void
.end method
