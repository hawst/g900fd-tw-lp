.class Lcom/sec/android/mimage/photoretouching/PhotoRetouching$1;
.super Landroid/content/BroadcastReceiver;
.source "PhotoRetouching.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/PhotoRetouching;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$1;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .line 137
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 140
    const-string v8, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 141
    const-string v8, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 142
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$1;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 143
    .local v3, "imageIntent":Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 144
    .local v1, "extras":Landroid/os/Bundle;
    const-string v8, "selectedCount"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 145
    .local v0, "count":I
    const/4 v4, 0x0

    .line 146
    .local v4, "path":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 147
    .local v5, "personalPageRoot":Ljava/lang/String;
    if-lez v0, :cond_0

    .line 150
    const-string v8, "selectedItems"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 151
    .local v7, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v7, :cond_0

    .line 152
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v2, v8, :cond_1

    .line 174
    .end local v0    # "count":I
    .end local v1    # "extras":Landroid/os/Bundle;
    .end local v2    # "i":I
    .end local v3    # "imageIntent":Landroid/content/Intent;
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "personalPageRoot":Ljava/lang/String;
    .end local v7    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_0
    return-void

    .line 153
    .restart local v0    # "count":I
    .restart local v1    # "extras":Landroid/os/Bundle;
    .restart local v2    # "i":I
    .restart local v3    # "imageIntent":Landroid/content/Intent;
    .restart local v4    # "path":Ljava/lang/String;
    .restart local v5    # "personalPageRoot":Ljava/lang/String;
    .restart local v7    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_1
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    .line 155
    .local v6, "uri":Landroid/net/Uri;
    if-eqz v6, :cond_2

    .line 158
    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    const-string v9, "content"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 159
    invoke-static {p1, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 163
    :cond_2
    :goto_1
    if-eqz v5, :cond_3

    if-eqz v4, :cond_3

    .line 164
    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 165
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$1;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->finish()V

    .line 152
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 161
    :cond_4
    invoke-virtual {v6}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method
