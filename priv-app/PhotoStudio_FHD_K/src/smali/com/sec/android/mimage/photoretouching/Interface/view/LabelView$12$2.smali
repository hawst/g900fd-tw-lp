.class Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;
.super Ljava/lang/Object;
.source "LabelView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->onShow(Landroid/content/DialogInterface;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

.field private final synthetic val$di:Landroid/content/DialogInterface;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->val$di:Landroid/content/DialogInterface;

    .line 841
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;
    .locals 1

    .prologue
    .line 841
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 844
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->val$di:Landroid/content/DialogInterface;

    check-cast v3, Landroid/app/Dialog;

    const v4, 0x7f09001f

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;

    .line 845
    .local v0, "eView":Lcom/sec/android/mimage/photoretouching/Gui/InputText;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    move-result-object v3

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Landroid/content/Context;

    move-result-object v3

    .line 846
    const-string v4, "input_method"

    .line 845
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 847
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "\n"

    const-string v5, " "

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 848
    .local v2, "labelName":Ljava/lang/String;
    if-eqz v2, :cond_1

    const-string v3, ""

    if-eq v2, v3, :cond_1

    .line 850
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mResId:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)I

    move-result v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    move-result-object v6

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mStyleNum:I
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)I

    move-result v6

    invoke-virtual {v3, v4, v5, v2, v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->setLabelMode(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 851
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    move-result-object v3

    invoke-static {v3, v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;I)V

    .line 852
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    move-result-object v3

    invoke-static {v3, v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;I)V

    .line 853
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->val$di:Landroid/content/DialogInterface;

    invoke-interface {v3}, Landroid/content/DialogInterface;->cancel()V

    .line 860
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    move-result-object v3

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mIsResetTabLayout:Z
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 863
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 865
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2$1;

    invoke-direct {v4, p0, v1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;Landroid/view/inputmethod/InputMethodManager;Lcom/sec/android/mimage/photoretouching/Gui/InputText;)V

    .line 877
    const-wide/16 v6, 0x64

    .line 865
    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 886
    :cond_0
    :goto_1
    return-void

    .line 857
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    move-result-object v3

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    goto :goto_0

    .line 881
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getCurrnetType()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v5

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->showTabLayout(IZZ)V
    invoke-static {v3, v4, v8, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;IZZ)V

    .line 882
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    move-result-object v3

    invoke-static {v3, v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;Z)V

    goto :goto_1
.end method
