.class public interface abstract Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;
.super Ljava/lang/Object;
.source "ViewFrame.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TestInterface"
.end annotation


# virtual methods
.method public abstract OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
.end method

.method public abstract backPressed()V
.end method

.method public abstract changeImage(I)V
.end method

.method public abstract initActionbar()V
.end method

.method public abstract initButtons()V
.end method

.method public abstract initDialog()V
.end method

.method public abstract initEffect()V
.end method

.method public abstract initProgressText()V
.end method

.method public abstract initSubView()V
.end method

.method public abstract initTrayLayout()V
.end method

.method public abstract initView()V
.end method

.method public abstract newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
.end method

.method public abstract onDestroy()V
.end method

.method public abstract onDraw(Landroid/graphics/Canvas;)V
.end method

.method public abstract onFrameKeyUp(ILandroid/view/KeyEvent;)Z
.end method

.method public abstract onLayout()V
.end method

.method public abstract onOptionsItemSelected(I)V
.end method

.method public abstract onResume()V
.end method

.method public abstract refreshView()V
.end method

.method public abstract setConfigurationChanged()V
.end method
