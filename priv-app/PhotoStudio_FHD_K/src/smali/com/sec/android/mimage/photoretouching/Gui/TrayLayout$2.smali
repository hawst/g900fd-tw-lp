.class Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$2;
.super Ljava/lang/Object;
.source "TrayLayout.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->addFirstButton(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

.field private final synthetic val$list:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$2;->val$list:Ljava/util/ArrayList;

    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 228
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$2;->val$list:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_2

    .line 202
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const v1, -0xf00001

    and-int/2addr v0, v1

    const/high16 v1, 0x2c000000

    if-eq v0, v1, :cond_0

    .line 203
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const v1, -0xff0001

    and-int/2addr v0, v1

    const/high16 v1, 0x1e000000

    if-ne v0, v1, :cond_1

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$2;->val$list:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->getMultipleImageForTray(Landroid/content/Context;I)V

    .line 216
    :goto_0
    return-void

    .line 209
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->openGalleryForTray(Landroid/content/Context;)V

    goto :goto_0

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0600ff

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 222
    return-void
.end method
