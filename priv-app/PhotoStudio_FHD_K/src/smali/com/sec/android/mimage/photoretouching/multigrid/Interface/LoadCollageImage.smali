.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;
.super Landroid/os/AsyncTask;
.source "LoadCollageImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage$MultiGridLoadCollageCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private COLLAGE_BITMAP_SCALE:F

.field private final MAX_BITMAP_HEIGHT:I

.field private final MAX_BITMAP_WIDTH:I

.field private mContext:Landroid/content/Context;

.field private mIsEditMode:Z

.field private mMultiGridLoadCollageCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage$MultiGridLoadCollageCallback;

.field private mPathBitmapList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSplitCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IZLjava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage$MultiGridLoadCollageCallback;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "splitCount"    # I
    .param p3, "isEdit"    # Z
    .param p5, "callback"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage$MultiGridLoadCollageCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IZ",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;",
            ">;",
            "Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage$MultiGridLoadCollageCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .local p4, "pathBitmapList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;>;"
    const/16 v2, 0x640

    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 16
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mContext:Landroid/content/Context;

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mSplitCount:I

    .line 19
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mPathBitmapList:Ljava/util/ArrayList;

    .line 20
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->COLLAGE_BITMAP_SCALE:F

    .line 21
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->MAX_BITMAP_WIDTH:I

    .line 22
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->MAX_BITMAP_HEIGHT:I

    .line 30
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mMultiGridLoadCollageCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage$MultiGridLoadCollageCallback;

    .line 35
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mContext:Landroid/content/Context;

    .line 36
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mSplitCount:I

    .line 37
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mPathBitmapList:Ljava/util/ArrayList;

    .line 38
    iput-boolean p3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mIsEditMode:Z

    .line 39
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mMultiGridLoadCollageCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage$MultiGridLoadCollageCallback;

    .line 40
    return-void
.end method


# virtual methods
.method protected StartCollage()Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/high16 v10, 0x44c80000    # 1600.0f

    .line 67
    new-instance v4, Lsiso/AutoCollage/Collage;

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mContext:Landroid/content/Context;

    invoke-direct {v4, v8}, Lsiso/AutoCollage/Collage;-><init>(Landroid/content/Context;)V

    .line 68
    .local v4, "collage":Lsiso/AutoCollage/Collage;
    invoke-virtual {v4}, Lsiso/AutoCollage/Collage;->clear()V

    .line 70
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->COLLAGE_BITMAP_SCALE:F

    mul-float/2addr v8, v10

    float-to-int v8, v8

    .line 71
    iget v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->COLLAGE_BITMAP_SCALE:F

    mul-float/2addr v9, v10

    float-to-int v9, v9

    .line 72
    sget-object v10, Lsiso/AutoCollage/Collage$PixelFormat;->ARGB_8888:Lsiso/AutoCollage/Collage$PixelFormat;

    .line 70
    invoke-virtual {v4, v8, v9, v10}, Lsiso/AutoCollage/Collage;->setConfig(IILsiso/AutoCollage/Collage$PixelFormat;)V

    .line 74
    const v8, 0x271000

    new-array v3, v8, [I

    .line 75
    .local v3, "blackAlphaBitmap":[I
    const/high16 v8, -0x1000000

    invoke-static {v3, v8}, Ljava/util/Arrays;->fill([II)V

    .line 77
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 78
    .local v2, "bitmapList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/MemoryStatus;->getAvailableExternalMemorySize()J

    move-result-wide v0

    .line 79
    .local v0, "available_memsize":J
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "ysjeong, available_memsize = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", before copy"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 80
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mSplitCount:I

    if-lt v7, v8, :cond_0

    .line 93
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v6, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x0

    :goto_1
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mPathBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v7, v8, :cond_2

    .line 99
    invoke-virtual {v4, v6, v2}, Lsiso/AutoCollage/Collage;->create(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 101
    const-string v8, "ysjeong, success create Collage, startCollage, LoadCollageImage"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 102
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mMultiGridLoadCollageCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage$MultiGridLoadCollageCallback;

    invoke-interface {v8, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage$MultiGridLoadCollageCallback;->onCreateCollage(Lsiso/AutoCollage/Collage;)V

    .line 108
    :goto_2
    const/4 v8, 0x0

    return-object v8

    .line 83
    .end local v6    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    const/4 v5, 0x0

    .line 84
    .local v5, "copyBitmap":Landroid/graphics/Bitmap;
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mPathBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 86
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mPathBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 89
    :cond_1
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 96
    .end local v5    # "copyBitmap":Landroid/graphics/Bitmap;
    .restart local v6    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mPathBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 106
    :cond_3
    const-string v8, "ysjeong, fail create Collage, startCollage, LoadCollageImage"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected StartCollageEdit()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    .line 45
    const-string v0, "ysjeong, doInBackground, LoadCollageImage"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 46
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mIsEditMode:Z

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->StartCollageEdit()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 49
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->StartCollage()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->doInBackground([Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mMultiGridLoadCollageCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage$MultiGridLoadCollageCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage$MultiGridLoadCollageCallback;->invalidate()V

    .line 56
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->mMultiGridLoadCollageCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage$MultiGridLoadCollageCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage$MultiGridLoadCollageCallback;->updateSaveButton()V

    .line 57
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/LoadCollageImage;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
