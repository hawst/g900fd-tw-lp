.class Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;
.super Ljava/lang/Object;
.source "StampView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->init3DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .line 347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 0
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 398
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 351
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 352
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableCancel()V

    .line 354
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isRecentClicked()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 355
    new-instance v1, Ljava/util/ArrayList;

    .line 356
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->MAX_RECENTLY:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)I

    move-result v2

    .line 355
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 357
    .local v1, "recentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;>;"
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v2

    .line 358
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v1

    .line 359
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 373
    :goto_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v2

    .line 374
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isRecentClicked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 375
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v2

    .line 376
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 377
    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v4

    .line 376
    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getCurrentResId(I)I
    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setRecentButton(I)V

    .line 378
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v2

    .line 379
    invoke-virtual {v2, v6, v5, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setRecentClicked(ZII)V

    .line 387
    .end local v0    # "i":I
    .end local v1    # "recentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;>;"
    :cond_0
    :goto_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->saveToDB()V

    .line 389
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->doDone()V
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    .line 390
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .line 391
    const/high16 v3, 0x31000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 392
    return-void

    .line 360
    .restart local v0    # "i":I
    .restart local v1    # "recentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;>;"
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 361
    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    .line 360
    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getCurrentResId(I)I
    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;I)I

    move-result v3

    .line 362
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v2

    .line 360
    if-ne v3, v2, :cond_2

    .line 363
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v2

    .line 365
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 366
    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v4

    .line 365
    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getCurrentResId(I)I
    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;I)I

    move-result v3

    .line 364
    invoke-virtual {v2, v3, v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->resetRecentButton(II)V

    .line 368
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v2

    .line 369
    invoke-virtual {v2, v6, v5, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setRecentClicked(ZII)V

    goto/16 :goto_1

    .line 359
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 383
    .end local v0    # "i":I
    .end local v1    # "recentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;>;"
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v2

    .line 384
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 385
    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v4

    .line 384
    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getCurrentResId(I)I
    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setRecentButton(I)V

    goto :goto_2
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 404
    return-void
.end method
