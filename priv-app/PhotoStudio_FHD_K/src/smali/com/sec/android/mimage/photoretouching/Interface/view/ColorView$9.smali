.class Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;
.super Ljava/lang/Object;
.source "ColorView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initMiddleButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    .line 667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 670
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 671
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 672
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    and-int/2addr v0, v2

    if-eqz v0, :cond_3

    .line 674
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->clearnSelectionArea()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$35(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 675
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->getCurrentStep()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->applyPreview(I)I

    .line 677
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initMiddleButton()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 678
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->invalidateViews()V

    .line 688
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 671
    goto :goto_0

    :cond_2
    move v2, v1

    .line 672
    goto :goto_1

    .line 682
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 683
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x14000000

    .line 684
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    const/high16 v3, 0x15000000

    .line 685
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->getEffectType()I

    move-result v4

    .line 686
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$33(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->getCurrentStep()F

    move-result v5

    float-to-int v5, v5

    .line 684
    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;-><init>(III)V

    .line 683
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(ILcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;)V

    goto :goto_2
.end method
