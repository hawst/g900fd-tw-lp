.class Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;
.super Ljava/lang/Object;
.source "AssistantLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AssistantSeekbarInfo"
.end annotation


# instance fields
.field assistantId:I

.field assistantType:I

.field icon:Landroid/graphics/drawable/Drawable;

.field iconId:I

.field iconId2:I

.field seekBarBLeftListener:Landroid/view/View$OnClickListener;

.field seekBarBRightListener:Landroid/view/View$OnClickListener;

.field seekBarGLeftListener:Landroid/view/View$OnClickListener;

.field seekBarGRightListener:Landroid/view/View$OnClickListener;

.field seekBarLeftListener:Landroid/view/View$OnClickListener;

.field seekBarListener:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

.field seekBarListener1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

.field seekBarListener2:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

.field seekBarMax:I

.field seekBarMax1:I

.field seekBarMax2:I

.field seekBarRLeftListener:Landroid/view/View$OnClickListener;

.field seekBarRRightListener:Landroid/view/View$OnClickListener;

.field seekBarRightListener:Landroid/view/View$OnClickListener;

.field seekBarStartPos:I

.field seekBarStartPos1:I

.field seekBarStartPos2:I

.field text:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

.field thumbnail:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1072
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1081
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 1082
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 1083
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener2:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 1084
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarLeftListener:Landroid/view/View$OnClickListener;

    .line 1085
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarRightListener:Landroid/view/View$OnClickListener;

    .line 1086
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarRLeftListener:Landroid/view/View$OnClickListener;

    .line 1087
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarRRightListener:Landroid/view/View$OnClickListener;

    .line 1088
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarGLeftListener:Landroid/view/View$OnClickListener;

    .line 1089
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarGRightListener:Landroid/view/View$OnClickListener;

    .line 1090
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarBLeftListener:Landroid/view/View$OnClickListener;

    .line 1091
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarBRightListener:Landroid/view/View$OnClickListener;

    .line 1092
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarStartPos:I

    .line 1093
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax:I

    .line 1095
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarStartPos1:I

    .line 1096
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax1:I

    .line 1097
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarStartPos2:I

    .line 1098
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax2:I

    .line 1106
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->thumbnail:Landroid/graphics/Bitmap;

    return-void
.end method
