.class Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$2;
.super Ljava/lang/Object;
.source "CollageAnimationView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->createThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 247
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mIsLoop:Z
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 263
    return-void

    .line 248
    :cond_1
    const/4 v0, 0x0

    .line 250
    .local v0, "canvas":Landroid/graphics/Canvas;
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 252
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)Landroid/view/SurfaceHolder;

    move-result-object v2

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 253
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->drawCanvas(Landroid/graphics/Canvas;)V
    invoke-static {v1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->access$4(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;Landroid/graphics/Canvas;)V

    .line 252
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 256
    if-eqz v0, :cond_0

    .line 257
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)Landroid/view/SurfaceHolder;

    move-result-object v2

    monitor-enter v2

    .line 258
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 257
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 252
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 255
    :catchall_2
    move-exception v1

    .line 256
    if-eqz v0, :cond_2

    .line 257
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)Landroid/view/SurfaceHolder;

    move-result-object v2

    monitor-enter v2

    .line 258
    :try_start_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)Landroid/view/SurfaceHolder;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 257
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 261
    :cond_2
    throw v1

    .line 257
    :catchall_3
    move-exception v1

    :try_start_6
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v1
.end method
