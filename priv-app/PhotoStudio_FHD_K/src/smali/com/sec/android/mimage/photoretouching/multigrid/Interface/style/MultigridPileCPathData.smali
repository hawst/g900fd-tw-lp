.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;
.super Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;
.source "MultigridPileCPathData.java"


# static fields
.field static mCurrentPreset:[[[F

.field static mDrawPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation
.end field

.field private static final mPilePreset2_1to1:[[[F

.field private static final mPilePreset2_9to16:[[[F

.field private static final mPilePreset3_1to1:[[[F

.field private static final mPilePreset3_9to16:[[[F

.field private static final mPilePreset4_1to1:[[[F

.field private static final mPilePreset4_9to16:[[[F

.field private static final mPilePreset5_1to1:[[[F

.field private static final mPilePreset5_9to16:[[[F

.field private static final mPilePreset6_1to1:[[[F

.field private static final mPilePreset6_9to16:[[[F


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 15
    new-array v0, v4, [[[F

    .line 16
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_0

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_1

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_2

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_3

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 17
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_4

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_5

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_6

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_7

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 14
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset2_1to1:[[[F

    .line 21
    new-array v0, v7, [[[F

    .line 22
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_8

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_9

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_b

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 23
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_f

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 24
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_10

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_11

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_12

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_13

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 20
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset3_1to1:[[[F

    .line 28
    new-array v0, v8, [[[F

    .line 29
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_14

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_15

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_16

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_17

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 30
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_18

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_19

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_1a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_1b

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 31
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_1c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_1d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_1e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_1f

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 32
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_20

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_21

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_22

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_23

    aput-object v2, v1, v7

    aput-object v1, v0, v7

    .line 27
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset4_1to1:[[[F

    .line 36
    const/4 v0, 0x5

    new-array v0, v0, [[[F

    .line 37
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_24

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_25

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_26

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_27

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 38
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_28

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_29

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_2a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_2b

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 39
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_2c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_2d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_2e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_2f

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 40
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_30

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_31

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_32

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_33

    aput-object v2, v1, v7

    aput-object v1, v0, v7

    .line 41
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_34

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_35

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_36

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_37

    aput-object v2, v1, v7

    aput-object v1, v0, v8

    .line 35
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset5_1to1:[[[F

    .line 45
    const/4 v0, 0x6

    new-array v0, v0, [[[F

    .line 46
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_38

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_39

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_3a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_3b

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 47
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_3c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_3d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_3e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_3f

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 48
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_40

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_41

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_42

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_43

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 49
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_44

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_45

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_46

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_47

    aput-object v2, v1, v7

    aput-object v1, v0, v7

    .line 50
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_48

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_49

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_4a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_4b

    aput-object v2, v1, v7

    aput-object v1, v0, v8

    const/4 v1, 0x5

    .line 51
    new-array v2, v8, [[F

    new-array v3, v4, [F

    fill-array-data v3, :array_4c

    aput-object v3, v2, v5

    new-array v3, v4, [F

    fill-array-data v3, :array_4d

    aput-object v3, v2, v6

    new-array v3, v4, [F

    fill-array-data v3, :array_4e

    aput-object v3, v2, v4

    new-array v3, v4, [F

    fill-array-data v3, :array_4f

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    .line 44
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset6_1to1:[[[F

    .line 55
    new-array v0, v4, [[[F

    .line 56
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_50

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_51

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_52

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_53

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 57
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_54

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_55

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_56

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_57

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 54
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset2_9to16:[[[F

    .line 61
    new-array v0, v7, [[[F

    .line 62
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_58

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_59

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_5a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_5b

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 63
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_5c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_5d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_5e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_5f

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 64
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_60

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_61

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_62

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_63

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 60
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset3_9to16:[[[F

    .line 70
    new-array v0, v8, [[[F

    .line 71
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_64

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_65

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_66

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_67

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 72
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_68

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_69

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_6a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_6b

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 73
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_6c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_6d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_6e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_6f

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 74
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_70

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_71

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_72

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_73

    aput-object v2, v1, v7

    aput-object v1, v0, v7

    .line 69
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset4_9to16:[[[F

    .line 78
    const/4 v0, 0x5

    new-array v0, v0, [[[F

    .line 80
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_74

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_75

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_76

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_77

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 81
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_78

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_79

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_7a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_7b

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 82
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_7c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_7d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_7e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_7f

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 83
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_80

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_81

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_82

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_83

    aput-object v2, v1, v7

    aput-object v1, v0, v7

    .line 84
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_84

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_85

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_86

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_87

    aput-object v2, v1, v7

    aput-object v1, v0, v8

    .line 77
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset5_9to16:[[[F

    .line 88
    const/4 v0, 0x6

    new-array v0, v0, [[[F

    .line 90
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_88

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_89

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_8a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_8b

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 91
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_8c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_8d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_8e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_8f

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 92
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_90

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_91

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_92

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_93

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 94
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_94

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_95

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_96

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_97

    aput-object v2, v1, v7

    aput-object v1, v0, v7

    .line 95
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_98

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_99

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_9a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_9b

    aput-object v2, v1, v7

    aput-object v1, v0, v8

    const/4 v1, 0x5

    .line 96
    new-array v2, v8, [[F

    new-array v3, v4, [F

    fill-array-data v3, :array_9c

    aput-object v3, v2, v5

    new-array v3, v4, [F

    fill-array-data v3, :array_9d

    aput-object v3, v2, v6

    new-array v3, v4, [F

    fill-array-data v3, :array_9e

    aput-object v3, v2, v4

    new-array v3, v4, [F

    fill-array-data v3, :array_9f

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    .line 87
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset6_9to16:[[[F

    .line 213
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    .line 214
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mDrawPaths:Ljava/util/ArrayList;

    return-void

    .line 16
    :array_0
    .array-data 4
        0x3d2acd9f    # 0.0417f
        0x3e910cb3    # 0.2833f
    .end array-data

    :array_1
    .array-data 4
        0x3eddd97f    # 0.4333f
        0x3e82de01    # 0.2556f
    .end array-data

    :array_2
    .array-data 4
        0x3ef05532    # 0.4694f
        0x3f444674    # 0.7667f
    .end array-data

    :array_3
    .array-data 4
        0x3d9f559b    # 0.0778f
        0x3f4aacda    # 0.7917f
    .end array-data

    .line 17
    :array_4
    .array-data 4
        0x3f00b780    # 0.5028f
        0x3e871de7    # 0.2639f
    .end array-data

    :array_5
    .array-data 4
        0x3f7779a7    # 0.9667f
        0x3e910cb3    # 0.2833f
    .end array-data

    :array_6
    .array-data 4
        0x3f73eab3    # 0.9528f
        0x3f244674    # 0.6417f
    .end array-data

    :array_7
    .array-data 4
        0x3efa511a    # 0.4889f
        0x3f1f4880    # 0.6222f
    .end array-data

    .line 22
    :array_8
    .array-data 4
        0x3f2fa440    # 0.6861f
        0x3e93eab3    # 0.2889f
    .end array-data

    :array_9
    .array-data 4
        0x3f782a99    # 0.9694f
        0x3e9f4880    # 0.3111f
    .end array-data

    :array_a
    .array-data 4
        0x3f705bc0    # 0.9389f
        0x3f2eecc0    # 0.6833f
    .end array-data

    :array_b
    .array-data 4
        0x3f288659    # 0.6583f
        0x3f293dd9    # 0.6611f
    .end array-data

    .line 23
    :array_c
    .array-data 4
        0x3eaaa64c    # 0.3333f
        0x3ea4f766    # 0.3222f
    .end array-data

    :array_d
    .array-data 4
        0x3f3c710d    # 0.7361f
        0x3ead844d    # 0.3389f
    .end array-data

    :array_e
    .array-data 4
        0x3f38e219    # 0.7222f
        0x3f25aee6    # 0.6472f
    .end array-data

    :array_f
    .array-data 4
        0x3ea38866    # 0.3194f
        0x3f216f00    # 0.6306f
    .end array-data

    .line 24
    :array_10
    .array-data 4
        0x3c9eecc0    # 0.0194f
        0x3e9b089a    # 0.3028f
    .end array-data

    :array_11
    .array-data 4
        0x3eb4a234    # 0.3528f
        0x3e8faace    # 0.2806f
    .end array-data

    :array_12
    .array-data 4
        0x3ec43fe6    # 0.3833f
        0x3f3779a7    # 0.7167f
    .end array-data

    :array_13
    .array-data 4
        0x3d4ccccd    # 0.05f
        0x3f3d288d    # 0.7389f
    .end array-data

    .line 29
    :array_14
    .array-data 4
        0x3dd288ce    # 0.1028f
        0x3f111340    # 0.5667f
    .end array-data

    :array_15
    .array-data 4
        0x3ef4a234    # 0.4778f
        0x3f0b5dcc    # 0.5444f
    .end array-data

    :array_16
    .array-data 4
        0x3efe9100    # 0.4972f
        0x3f54a234    # 0.8306f
    .end array-data

    :array_17
    .array-data 4
        0x3dfa43fe    # 0.1222f
        0x3f5a511a    # 0.8528f
    .end array-data

    .line 30
    :array_18
    .array-data 4
        0x3f105bc0    # 0.5639f
        0x3f07d567    # 0.5306f
    .end array-data

    :array_19
    .array-data 4
        0x3f638ef3    # 0.8889f
        0x3f0b5dcc    # 0.5444f
    .end array-data

    :array_1a
    .array-data 4
        0x3f5de00d    # 0.8667f
        0x3f782a99    # 0.9694f
    .end array-data

    :array_1b
    .array-data 4
        0x3f0b5dcc    # 0.5444f
        0x3f73eab3    # 0.9528f
    .end array-data

    .line 31
    :array_1c
    .array-data 4
        0x3e000000    # 0.125f
        0x3dddcc64    # 0.1083f
    .end array-data

    :array_1d
    .array-data 4
        0x3edb089a    # 0.4278f
        0x3dcccccd    # 0.1f
    .end array-data

    :array_1e
    .array-data 4
        0x3ee0b780    # 0.4389f
        0x3efe9100    # 0.4972f
    .end array-data

    :array_1f
    .array-data 4
        0x3e0b5dcc    # 0.1361f
        0x3f016f00    # 0.5056f
    .end array-data

    .line 32
    :array_20
    .array-data 4
        0x3f05aee6    # 0.5222f
        0x3dd288ce    # 0.1028f
    .end array-data

    :array_21
    .array-data 4
        0x3f6b5dcc    # 0.9194f
        0x3de38866    # 0.1111f
    .end array-data

    :array_22
    .array-data 4
        0x3f69f55a    # 0.9139f
        0x3ed3eab3    # 0.4139f
    .end array-data

    :array_23
    .array-data 4
        0x3f044674    # 0.5167f
        0x3ecfaace    # 0.4056f
    .end array-data

    .line 37
    :array_24
    .array-data 4
        0x3f2d844d    # 0.6778f
        0x3f1b089a    # 0.6056f
    .end array-data

    :array_25
    .array-data 4
        0x3f693dd9    # 0.9111f
        0x3f14a234    # 0.5806f
    .end array-data

    :array_26
    .array-data 4
        0x3f711340    # 0.9417f
        0x3f638ef3    # 0.8889f
    .end array-data

    :array_27
    .array-data 4
        0x3f355326    # 0.7083f
        0x3f693dd9    # 0.9111f
    .end array-data

    .line 38
    :array_28
    .array-data 4
        0x3ecccccd    # 0.4f
        0x3f11c433    # 0.5694f
    .end array-data

    :array_29
    .array-data 4
        0x3f266666    # 0.65f
        0x3f16c227    # 0.5889f
    .end array-data

    :array_2a
    .array-data 4
        0x3f1f4880    # 0.6222f
        0x3f6b5dcc    # 0.9194f
    .end array-data

    :array_2b
    .array-data 4
        0x3ebe9100    # 0.3722f
        0x3f666666    # 0.9f
    .end array-data

    .line 39
    :array_2c
    .array-data 4
        0x3d35dcc6    # 0.0444f
        0x3f16c227    # 0.5889f
    .end array-data

    :array_2d
    .array-data 4
        0x3eac154d    # 0.3361f
        0x3f182a99    # 0.5944f
    .end array-data

    :array_2e
    .array-data 4
        0x3ea94467    # 0.3306f
        0x3f511340    # 0.8167f
    .end array-data

    :array_2f
    .array-data 4
        0x3d1f559b    # 0.0389f
        0x3f4fa440    # 0.8111f
    .end array-data

    .line 40
    :array_30
    .array-data 4
        0x3df487fd    # 0.1194f
        0x3e1f559b    # 0.1556f
    .end array-data

    :array_31
    .array-data 4
        0x3ecfaace    # 0.4056f
        0x3e087fcc    # 0.1333f
    .end array-data

    :array_32
    .array-data 4
        0x3edf4880    # 0.4361f
        0x3f021ff3    # 0.5083f
    .end array-data

    :array_33
    .array-data 4
        0x3e19999a    # 0.15f
        0x3f07d567    # 0.5306f
    .end array-data

    .line 41
    :array_34
    .array-data 4
        0x3f07d567    # 0.5306f
        0x3e1c779a    # 0.1528f
    .end array-data

    :array_35
    .array-data 4
        0x3f67d567    # 0.9056f
        0x3e24f766    # 0.1611f
    .end array-data

    :array_36
    .array-data 4
        0x3f666666    # 0.9f
        0x3ee4f766    # 0.4472f
    .end array-data

    :array_37
    .array-data 4
        0x3f066666    # 0.525f
        0x3ee22681    # 0.4417f
    .end array-data

    .line 46
    :array_38
    .array-data 4
        0x3f333333    # 0.7f
        0x3e087fcc    # 0.1333f
    .end array-data

    :array_39
    .array-data 4
        0x3f755326    # 0.9583f
        0x3e1f559b    # 0.1556f
    .end array-data

    :array_3a
    .array-data 4
        0x3f6e3bcd    # 0.9306f
        0x3efbc01a    # 0.4917f
    .end array-data

    :array_3b
    .array-data 4
        0x3f2ccccd    # 0.675f
        0x3ef1c433    # 0.4722f
    .end array-data

    .line 47
    :array_3c
    .array-data 4
        0x3eaaa64c    # 0.3333f
        0x3e27d567    # 0.1639f
    .end array-data

    :array_3d
    .array-data 4
        0x3f2e3bcd    # 0.6806f
        0x3e2d7732    # 0.1694f
    .end array-data

    :array_3e
    .array-data 4
        0x3f2ccccd    # 0.675f
        0x3edf4880    # 0.4361f
    .end array-data

    :array_3f
    .array-data 4
        0x3ea7d567    # 0.3278f
        0x3edb089a    # 0.4278f
    .end array-data

    .line 48
    :array_40
    .array-data 4
        0x3c9eecc0    # 0.0194f
        0x3e0b5dcc    # 0.1361f
    .end array-data

    :array_41
    .array-data 4
        0x3e9b089a    # 0.3028f
        0x3dddcc64    # 0.1083f
    .end array-data

    :array_42
    .array-data 4
        0x3ead844d    # 0.3389f
        0x3ef33333    # 0.475f
    .end array-data

    :array_43
    .array-data 4
        0x3d6ecbfb    # 0.0583f
        0x3f016f00    # 0.5056f
    .end array-data

    .line 49
    :array_44
    .array-data 4
        0x3f2d844d    # 0.6778f
        0x3f1b089a    # 0.6056f
    .end array-data

    :array_45
    .array-data 4
        0x3f693dd9    # 0.9111f
        0x3f14a234    # 0.5806f
    .end array-data

    :array_46
    .array-data 4
        0x3f711340    # 0.9417f
        0x3f638ef3    # 0.8889f
    .end array-data

    :array_47
    .array-data 4
        0x3f355326    # 0.7083f
        0x3f693dd9    # 0.9111f
    .end array-data

    .line 50
    :array_48
    .array-data 4
        0x3ecccccd    # 0.4f
        0x3f11c433    # 0.5694f
    .end array-data

    :array_49
    .array-data 4
        0x3f266666    # 0.65f
        0x3f16c227    # 0.5889f
    .end array-data

    :array_4a
    .array-data 4
        0x3f1f4880    # 0.6222f
        0x3f6b5dcc    # 0.9194f
    .end array-data

    :array_4b
    .array-data 4
        0x3ebe9100    # 0.3722f
        0x3f666666    # 0.9f
    .end array-data

    .line 51
    :array_4c
    .array-data 4
        0x3d35dcc6    # 0.0444f
        0x3f16c227    # 0.5889f
    .end array-data

    :array_4d
    .array-data 4
        0x3eac154d    # 0.3361f
        0x3f182a99    # 0.5944f
    .end array-data

    :array_4e
    .array-data 4
        0x3ea94467    # 0.3306f
        0x3f511340    # 0.8167f
    .end array-data

    :array_4f
    .array-data 4
        0x3d1f559b    # 0.0389f
        0x3f4fa440    # 0.8111f
    .end array-data

    .line 56
    :array_50
    .array-data 4
        0x3c63bcd3    # 0.0139f
        0x3e9e69ad    # 0.3094f
    .end array-data

    :array_51
    .array-data 4
        0x3f02d773    # 0.5111f
        0x3e973190    # 0.2953f
    .end array-data

    :array_52
    .array-data 4
        0x3f0b5dcc    # 0.5444f
        0x3f28ce70    # 0.6594f
    .end array-data

    :array_53
    .array-data 4
        0x3d4154ca    # 0.0472f
        0x3f2c63f1    # 0.6734f
    .end array-data

    .line 57
    :array_54
    .array-data 4
        0x3f038ef3    # 0.5139f
        0x3e933333    # 0.2875f
    .end array-data

    :array_55
    .array-data 4
        0x3f760aa6    # 0.9611f
        0x3e980347    # 0.2969f
    .end array-data

    :array_56
    .array-data 4
        0x3f727bb3    # 0.9472f
        0x3f2068dc    # 0.6266f
    .end array-data

    :array_57
    .array-data 4
        0x3efd21ff    # 0.4944f
        0x3f1e00d2    # 0.6172f
    .end array-data

    .line 62
    :array_58
    .array-data 4
        0x3e800000    # 0.25f
        0x3f18ce70    # 0.5969f
    .end array-data

    :array_59
    .array-data 4
        0x3f360aa6    # 0.7111f
        0x3f1c01a3    # 0.6094f
    .end array-data

    :array_5a
    .array-data 4
        0x3f327bb3    # 0.6972f
        0x3f4ecbfb    # 0.8078f
    .end array-data

    :array_5b
    .array-data 4
        0x3e71c433    # 0.2361f
        0x3f4c01a3    # 0.7969f
    .end array-data

    .line 63
    :array_5c
    .array-data 4
        0x3f105bc0    # 0.5639f
        0x3e467382    # 0.1938f
    .end array-data

    :array_5d
    .array-data 4
        0x3f74a234    # 0.9556f
        0x3e59999a    # 0.2125f
    .end array-data

    :array_5e
    .array-data 4
        0x3f6aacda    # 0.9167f
        0x3f000000    # 0.5f
    .end array-data

    :array_5f
    .array-data 4
        0x3f05aee6    # 0.5222f
        0x3ef73190    # 0.4828f
    .end array-data

    .line 64
    :array_60
    .array-data 4
        0x3d63bcd3    # 0.0556f
        0x3e4ff972    # 0.2031f
    .end array-data

    :array_61
    .array-data 4
        0x3eec154d    # 0.4611f
        0x3e47fcb9    # 0.1953f
    .end array-data

    :array_62
    .array-data 4
        0x3ef4a234    # 0.4778f
        0x3efc01a3    # 0.4922f
    .end array-data

    :array_63
    .array-data 4
        0x3d93dd98    # 0.0722f
        0x3f000000    # 0.5f
    .end array-data

    .line 71
    :array_64
    .array-data 4
        0x3d086595    # 0.0333f
        0x3f113405    # 0.5672f
    .end array-data

    :array_65
    .array-data 4
        0x3f04f766    # 0.5194f
        0x3f13fe5d    # 0.5781f
    .end array-data

    :array_66
    .array-data 4
        0x3f016f00    # 0.5056f
        0x3f4a0275    # 0.7891f
    .end array-data

    :array_67
    .array-data 4
        0x3c88ce70    # 0.0167f
        0x3f473190    # 0.7781f
    .end array-data

    .line 72
    :array_68
    .array-data 4
        0x3f13eab3    # 0.5778f
        0x3f13fe5d    # 0.5781f
    .end array-data

    :array_69
    .array-data 4
        0x3f6ccccd    # 0.925f
        0x3f10cb29    # 0.5656f
    .end array-data

    :array_6a
    .array-data 4
        0x3f733333    # 0.95f
        0x3f51ff2e    # 0.8203f
    .end array-data

    :array_6b
    .array-data 4
        0x3f1a511a    # 0.6028f
        0x3f54d014    # 0.8313f
    .end array-data

    .line 73
    :array_6c
    .array-data 4
        0x3cfaacda    # 0.0306f
        0x3e44d014    # 0.1922f
    .end array-data

    :array_6d
    .array-data 4
        0x3edb089a    # 0.4278f
        0x3e3e5c92    # 0.1859f
    .end array-data

    :array_6e
    .array-data 4
        0x3ee38866    # 0.4444f
        0x3ef404ea    # 0.4766f
    .end array-data

    :array_6f
    .array-data 4
        0x3d35dcc6    # 0.0444f
        0x3ef66cf4    # 0.4813f
    .end array-data

    .line 74
    :array_70
    .array-data 4
        0x3ef33333    # 0.475f
        0x3e519ce0    # 0.2047f
    .end array-data

    :array_71
    .array-data 4
        0x3f7de00d    # 0.9917f
        0x3e566cf4    # 0.2094f
    .end array-data

    :array_72
    .array-data 4
        0x3f7bb98c    # 0.9833f
        0x3edd97f6    # 0.4328f
    .end array-data

    :array_73
    .array-data 4
        0x3ef05532    # 0.4694f
        0x3edb2fec    # 0.4281f
    .end array-data

    .line 80
    :array_74
    .array-data 4
        0x3f2c154d    # 0.6722f
        0x3e4b295f    # 0.1984f
    .end array-data

    :array_75
    .array-data 4
        0x3f97d220    # 1.1861f
        0x3e3b2fec    # 0.1828f
    .end array-data

    :array_76
    .array-data 4
        0x3f9a511a    # 1.2056f
        0x3ed0068e    # 0.4063f
    .end array-data

    :array_77
    .array-data 4
        0x3f311340    # 0.6917f
        0x3ed80347    # 0.4219f
    .end array-data

    .line 81
    :array_78
    .array-data 4
        0x3eaef34d    # 0.3417f
        0x3e3e5c92    # 0.1859f
    .end array-data

    :array_79
    .array-data 4
        0x3f42d773    # 0.7611f
        0x3e59999a    # 0.2125f
    .end array-data

    :array_7a
    .array-data 4
        0x3f333333    # 0.7f
        0x3f046738    # 0.5172f
    .end array-data

    :array_7b
    .array-data 4
        0x3e910cb3    # 0.2833f
        0x3efb2fec    # 0.4906f
    .end array-data

    .line 82
    :array_7c
    .array-data 4
        -0x426c2268    # -0.0722f
        0x3e44d014    # 0.1922f
    .end array-data

    :array_7d
    .array-data 4
        0x3ea66666    # 0.325f
        0x3e3e5c92    # 0.1859f
    .end array-data

    :array_7e
    .array-data 4
        0x3ead844d    # 0.3389f
        0x3ef33333    # 0.475f
    .end array-data

    :array_7f
    .array-data 4
        -0x42913405    # -0.0583f
        0x3ef66cf4    # 0.4813f
    .end array-data

    .line 83
    :array_80
    .array-data 4
        0x3d35dcc6    # 0.0444f
        0x3f113405    # 0.5672f
    .end array-data

    :array_81
    .array-data 4
        0x3f04f766    # 0.5194f
        0x3f13fe5d    # 0.5781f
    .end array-data

    :array_82
    .array-data 4
        0x3f016f00    # 0.5056f
        0x3f4a0275    # 0.7891f
    .end array-data

    :array_83
    .array-data 4
        0x3c88ce70    # 0.0167f
        0x3f473190    # 0.7781f
    .end array-data

    .line 84
    :array_84
    .array-data 4
        0x3f13eab3    # 0.5778f
        0x3f13fe5d    # 0.5781f
    .end array-data

    :array_85
    .array-data 4
        0x3f6c154d    # 0.9222f
        0x3f10cb29    # 0.5656f
    .end array-data

    :array_86
    .array-data 4
        0x3f733333    # 0.95f
        0x3f51ff2e    # 0.8203f
    .end array-data

    :array_87
    .array-data 4
        0x3f1a511a    # 0.6028f
        0x3f54d014    # 0.8313f
    .end array-data

    .line 90
    :array_88
    .array-data 4
        0x3f2c154d    # 0.6722f
        0x3e4b295f    # 0.1984f
    .end array-data

    :array_89
    .array-data 4
        0x3f97d220    # 1.1861f
        0x3e3b2fec    # 0.1828f
    .end array-data

    :array_8a
    .array-data 4
        0x3f9a511a    # 1.2056f
        0x3ed0068e    # 0.4063f
    .end array-data

    :array_8b
    .array-data 4
        0x3f311340    # 0.6917f
        0x3ed80347    # 0.4219f
    .end array-data

    .line 91
    :array_8c
    .array-data 4
        0x3eaef34d    # 0.3417f
        0x3e3e5c92    # 0.1859f
    .end array-data

    :array_8d
    .array-data 4
        0x3f42d773    # 0.7611f
        0x3e59999a    # 0.2125f
    .end array-data

    :array_8e
    .array-data 4
        0x3f333333    # 0.7f
        0x3f046738    # 0.5172f
    .end array-data

    :array_8f
    .array-data 4
        0x3e910cb3    # 0.2833f
        0x3efb2fec    # 0.4906f
    .end array-data

    .line 92
    :array_90
    .array-data 4
        -0x426c2268    # -0.0722f
        0x3e44d014    # 0.1922f
    .end array-data

    :array_91
    .array-data 4
        0x3ea66666    # 0.325f
        0x3e3b2fec    # 0.1828f
    .end array-data

    :array_92
    .array-data 4
        0x3ead844d    # 0.3389f
        0x3ef33333    # 0.475f
    .end array-data

    :array_93
    .array-data 4
        -0x42913405    # -0.0583f
        0x3ef66cf4    # 0.4813f
    .end array-data

    .line 94
    :array_94
    .array-data 4
        0x3f39999a    # 0.725f
        0x3f13fe5d    # 0.5781f
    .end array-data

    :array_95
    .array-data 4
        0x3f893dd9    # 1.0722f
        0x3f113405    # 0.5672f
    .end array-data

    :array_96
    .array-data 4
        0x3f8c710d    # 1.0972f
        0x3f51ff2e    # 0.8203f
    .end array-data

    :array_97
    .array-data 4
        0x3f400000    # 0.75f
        0x3f54d014    # 0.8313f
    .end array-data

    .line 95
    :array_98
    .array-data 4
        0x3eaaa64c    # 0.3333f
        0x3f146738    # 0.5797f
    .end array-data

    :array_99
    .array-data 4
        0x3f333333    # 0.7f
        0x3f139c0f    # 0.5766f
    .end array-data

    :array_9a
    .array-data 4
        0x3f34a234    # 0.7056f
        0x3f586595    # 0.8453f
    .end array-data

    :array_9b
    .array-data 4
        0x3ead844d    # 0.3389f
        0x3f5930be    # 0.8484f
    .end array-data

    .line 96
    :array_9c
    .array-data 4
        -0x423eab36    # -0.0944f
        0x3f12680a    # 0.5719f
    .end array-data

    :array_9d
    .array-data 4
        0x3ec16f00    # 0.3778f
        0x3f153261    # 0.5828f
    .end array-data

    :array_9e
    .array-data 4
        0x3ec16f00    # 0.3778f
        0x3f4b367a    # 0.7938f
    .end array-data

    :array_9f
    .array-data 4
        -0x4222339c    # -0.1083f
        0x3f486595    # 0.7828f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;-><init>()V

    return-void
.end method

.method public static getPathPoints(ILcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;)[[[F
    .locals 2
    .param p0, "imgCnt"    # I
    .param p1, "proportion"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .prologue
    .line 139
    const/high16 v0, 0x1e200000

    const/4 v1, 0x2

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getMaxStyleNum(I)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    .line 141
    packed-switch p0, :pswitch_data_0

    .line 199
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    .line 203
    :goto_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    return-object v0

    .line 144
    :pswitch_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    if-ne p1, v0, :cond_0

    .line 146
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset2_1to1:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 150
    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset2_9to16:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 155
    :pswitch_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    if-ne p1, v0, :cond_1

    .line 157
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset3_1to1:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 161
    :cond_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset3_9to16:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 166
    :pswitch_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    if-ne p1, v0, :cond_2

    .line 168
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset4_1to1:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 172
    :cond_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset4_9to16:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 177
    :pswitch_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    if-ne p1, v0, :cond_3

    .line 179
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset5_1to1:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 183
    :cond_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset5_9to16:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 188
    :pswitch_4
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    if-ne p1, v0, :cond_4

    .line 190
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset6_1to1:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 194
    :cond_4
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mPilePreset6_9to16:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 141
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getPaths(ILcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;Landroid/graphics/Rect;)Ljava/util/ArrayList;
    .locals 13
    .param p0, "imgCnt"    # I
    .param p1, "proportion"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;
    .param p2, "canvasRoi"    # Landroid/graphics/Rect;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 101
    invoke-static {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->getPathPoints(ILcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;)[[[F

    move-result-object v6

    if-nez v6, :cond_0

    .line 102
    const/4 v6, 0x0

    .line 133
    :goto_0
    return-object v6

    .line 104
    :cond_0
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mDrawPaths:Ljava/util/ArrayList;

    if-nez v6, :cond_1

    .line 106
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    sput-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mDrawPaths:Ljava/util/ArrayList;

    .line 113
    :goto_1
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 114
    .local v5, "viewWidth":I
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v4

    .line 115
    .local v4, "viewHeight":I
    iget v1, p2, Landroid/graphics/Rect;->left:I

    .line 116
    .local v1, "leftPadding":I
    iget v3, p2, Landroid/graphics/Rect;->top:I

    .line 119
    .local v3, "topPadding":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    if-lt v0, p0, :cond_2

    .line 133
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mDrawPaths:Ljava/util/ArrayList;

    goto :goto_0

    .line 110
    .end local v0    # "i":I
    .end local v1    # "leftPadding":I
    .end local v3    # "topPadding":I
    .end local v4    # "viewHeight":I
    .end local v5    # "viewWidth":I
    :cond_1
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mDrawPaths:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 121
    .restart local v0    # "i":I
    .restart local v1    # "leftPadding":I
    .restart local v3    # "topPadding":I
    .restart local v4    # "viewHeight":I
    .restart local v5    # "viewWidth":I
    :cond_2
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 122
    .local v2, "path":Landroid/graphics/Path;
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    aget-object v6, v6, v0

    aget-object v6, v6, v9

    aget v6, v6, v9

    int-to-float v7, v5

    mul-float/2addr v6, v7

    int-to-float v7, v1

    add-float/2addr v6, v7

    sget-object v7, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    aget-object v7, v7, v0

    aget-object v7, v7, v9

    aget v7, v7, v10

    int-to-float v8, v4

    mul-float/2addr v7, v8

    int-to-float v8, v3

    add-float/2addr v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 123
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    aget-object v6, v6, v0

    aget-object v6, v6, v10

    aget v6, v6, v9

    int-to-float v7, v5

    mul-float/2addr v6, v7

    int-to-float v7, v1

    add-float/2addr v6, v7

    sget-object v7, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    aget-object v7, v7, v0

    aget-object v7, v7, v10

    aget v7, v7, v10

    int-to-float v8, v4

    mul-float/2addr v7, v8

    int-to-float v8, v3

    add-float/2addr v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 124
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    aget-object v6, v6, v0

    aget-object v6, v6, v11

    aget v6, v6, v9

    int-to-float v7, v5

    mul-float/2addr v6, v7

    int-to-float v7, v1

    add-float/2addr v6, v7

    sget-object v7, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    aget-object v7, v7, v0

    aget-object v7, v7, v11

    aget v7, v7, v10

    int-to-float v8, v4

    mul-float/2addr v7, v8

    int-to-float v8, v3

    add-float/2addr v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 125
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    aget-object v6, v6, v0

    aget-object v6, v6, v12

    aget v6, v6, v9

    int-to-float v7, v5

    mul-float/2addr v6, v7

    int-to-float v7, v1

    add-float/2addr v6, v7

    sget-object v7, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    aget-object v7, v7, v0

    aget-object v7, v7, v12

    aget v7, v7, v10

    int-to-float v8, v4

    mul-float/2addr v7, v8

    int-to-float v8, v3

    add-float/2addr v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 126
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    aget-object v6, v6, v0

    aget-object v6, v6, v9

    aget v6, v6, v9

    int-to-float v7, v5

    mul-float/2addr v6, v7

    int-to-float v7, v1

    add-float/2addr v6, v7

    sget-object v7, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    aget-object v7, v7, v0

    aget-object v7, v7, v9

    aget v7, v7, v10

    int-to-float v8, v4

    mul-float/2addr v7, v8

    int-to-float v8, v3

    add-float/2addr v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 127
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    aget-object v6, v6, v0

    aget-object v6, v6, v10

    aget v6, v6, v9

    int-to-float v7, v5

    mul-float/2addr v6, v7

    int-to-float v7, v1

    add-float/2addr v6, v7

    sget-object v7, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mCurrentPreset:[[[F

    aget-object v7, v7, v0

    aget-object v7, v7, v10

    aget v7, v7, v10

    int-to-float v8, v4

    mul-float/2addr v7, v8

    int-to-float v8, v3

    add-float/2addr v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 130
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->mDrawPaths:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2
.end method


# virtual methods
.method public getRotate(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 210
    const/4 v0, 0x0

    return v0
.end method
