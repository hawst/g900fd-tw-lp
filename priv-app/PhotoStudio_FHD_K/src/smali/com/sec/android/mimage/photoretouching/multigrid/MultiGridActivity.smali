.class public Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;
.super Landroid/app/Activity;
.source "MultiGridActivity.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CountGetIntentTimeThread;,
        Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;,
        Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$LocalBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final PERSONAL_PAGE_OFF:Ljava/lang/String; = "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

.field private static final PERSONAL_PAGE_ON:Ljava/lang/String; = "com.samsung.android.intent.action.PRIVATE_MODE_ON"


# instance fields
.field private final SHAKE_THRESHOLD:I

.field private final SHAKE_TIME_THRESHOLD:I

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mBottomBtnLayout:Landroid/widget/LinearLayout;

.field public mCheckOnPause:Z

.field public mCopyTodInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

.field private mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

.field private mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field public mIsEdit:Z

.field public mIsFirst:Z

.field private mIsGetIntentFinish:Z

.field private mKillViewHandler:Landroid/os/Handler;

.field private mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

.field private mReceiver:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$LocalBroadcastReceiver;

.field private mSecretModeReceiver:Landroid/content/BroadcastReceiver;

.field private mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

.field private mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

.field private m_fCurX:F

.field private m_fCurY:F

.field private m_fCurZ:F

.field private m_fLastX:F

.field private m_fLastY:F

.field private m_fLastZ:F

.field private m_fSpeed:F

.field private m_lLastTime:J

.field private m_senAccelerometer:Landroid/hardware/Sensor;

.field private m_senMng:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 124
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$1;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    .line 957
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$2;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mKillViewHandler:Landroid/os/Handler;

    .line 979
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mIsGetIntentFinish:Z

    .line 981
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    .line 984
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 985
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 986
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 987
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    .line 989
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 991
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mIsEdit:Z

    .line 992
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCheckOnPause:Z

    .line 995
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCopyTodInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 998
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mBottomBtnLayout:Landroid/widget/LinearLayout;

    .line 1001
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    .line 1002
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 1010
    const/16 v0, 0x17c

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->SHAKE_THRESHOLD:I

    .line 1011
    const/16 v0, 0x1c2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->SHAKE_TIME_THRESHOLD:I

    .line 1016
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1017
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    .line 1018
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mReceiver:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$LocalBroadcastReceiver;

    .line 52
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    .locals 1

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;Z)V
    .locals 0

    .prologue
    .line 979
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mIsGetIntentFinish:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)Z
    .locals 1

    .prologue
    .line 979
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mIsGetIntentFinish:Z

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)V
    .locals 0

    .prologue
    .line 877
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->prcessingException()V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 957
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mKillViewHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
    .locals 1

    .prologue
    .line 981
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    return-object v0
.end method

.method private createManager()V
    .locals 1

    .prologue
    .line 460
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    .line 461
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 463
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 464
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 465
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 466
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    .line 467
    return-void
.end method

.method private getImage(Landroid/content/Intent;)V
    .locals 16
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 352
    new-instance v3, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CountGetIntentTimeThread;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CountGetIntentTimeThread;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)V

    .line 353
    .local v3, "countGetIntentTimeThread":Ljava/lang/Thread;
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 355
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mIsGetIntentFinish:Z

    .line 356
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 360
    .local v8, "mDecodeInfoItemsTemp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->setIsEditMode(Landroid/content/Intent;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 362
    const-string v13, "EDIT_URI"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 364
    .local v6, "file_path":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 365
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_0

    .line 456
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "file_path":Ljava/lang/String;
    :goto_0
    return-void

    .line 367
    .restart local v5    # "file":Ljava/io/File;
    .restart local v6    # "file_path":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    invoke-virtual {v13, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->loadCollageData(Ljava/lang/String;)[I

    .line 368
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->LoadMultiGridInfo(Landroid/content/Intent;)V

    .line 369
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->getDecodeInfoList()Ljava/util/ArrayList;

    move-result-object v8

    .line 402
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "file_path":Ljava/lang/String;
    :cond_1
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 403
    .local v12, "validImageCount":I
    if-eqz v8, :cond_2

    sget v13, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->MIN_ITEM_CNT:I

    if-ge v12, v13, :cond_5

    .line 405
    :cond_2
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, " : getImage : Image count under 2"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 406
    const v13, 0x7f0601b9

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 407
    const/high16 v13, 0x2e000000

    const/4 v14, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->changeViewStatus(II)V

    goto :goto_0

    .line 373
    .end local v12    # "validImageCount":I
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 374
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 376
    const-string v13, "selectedCount"

    invoke-virtual {v1, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 377
    .local v2, "count":I
    sget v13, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->MAX_ITEM_CNT:I

    invoke-static {v2, v13}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 378
    const-string v13, "selectedItems"

    invoke-virtual {v1, v13}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v11

    .line 380
    .local v11, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v11, :cond_1

    .line 382
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v2, :cond_1

    .line 384
    new-instance v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    invoke-direct {v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;-><init>()V

    .line 386
    .local v4, "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    const/4 v13, 0x1

    iput-boolean v13, v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 387
    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/net/Uri;

    iput-object v13, v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 389
    iget-object v13, v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v13}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v13

    const-string v14, "content"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 390
    iget-object v13, v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 394
    :goto_2
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 382
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 392
    :cond_4
    iget-object v13, v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v13}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v13

    iput-object v13, v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    goto :goto_2

    .line 411
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "count":I
    .end local v4    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .end local v7    # "i":I
    .end local v11    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .restart local v12    # "validImageCount":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->getIsEditMode()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 413
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->getReloadGridStyle()I

    move-result v10

    .line 414
    .local v10, "style":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->getReloadGridSplit()I

    move-result v9

    .line 415
    .local v9, "split":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v10}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->changeViewStatus(II)V

    .line 422
    .end local v9    # "split":I
    .end local v10    # "style":I
    :goto_3
    new-instance v13, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 424
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 425
    const/4 v15, 0x5

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v8, v14, v15}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;I)V

    .line 422
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 427
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    new-instance v14, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$3;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$3;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)V

    invoke-virtual {v13, v14}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->setChangeViewCallback(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;)V

    .line 455
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->start()V

    goto/16 :goto_0

    .line 419
    :cond_6
    const/high16 v13, 0x2c000000

    const/high16 v14, 0x1e200000

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->changeViewStatus(II)V

    goto :goto_3
.end method

.method private initCircumstance()V
    .locals 3

    .prologue
    .line 229
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/MemoryStatus;->GetExternalStorageMount(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 230
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->prcessingException()V

    .line 256
    :goto_0
    return-void

    .line 233
    :cond_0
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/MemoryStatus;->checkLowExternalMemory(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 234
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->prcessingException()V

    goto :goto_0

    .line 238
    :cond_1
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->TEMP_CAMERA_PATH:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 239
    .local v0, "path":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_2

    .line 240
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 242
    :cond_2
    const/4 v0, 0x0

    .line 244
    new-instance v0, Ljava/io/File;

    .end local v0    # "path":Ljava/io/File;
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 245
    .restart local v0    # "path":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_3

    .line 246
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 248
    :cond_3
    const/4 v0, 0x0

    .line 250
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 251
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setPortrait()V

    goto :goto_0

    .line 253
    :cond_4
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setLandscape()V

    goto :goto_0
.end method

.method private prcessingException()V
    .locals 1

    .prologue
    .line 879
    const-string v0, "exception!"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 880
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->finish()V

    .line 881
    return-void
.end method

.method private registBroadcastReceiver()V
    .locals 2

    .prologue
    .line 262
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mReceiver:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$LocalBroadcastReceiver;

    if-nez v1, :cond_0

    .line 264
    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$LocalBroadcastReceiver;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$LocalBroadcastReceiver;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mReceiver:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$LocalBroadcastReceiver;

    .line 265
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 266
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 267
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mReceiver:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$LocalBroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 269
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private registerPrivateModeReceiver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 116
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 117
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 119
    new-instance v0, Landroid/content/IntentFilter;

    .end local v0    # "filter":Landroid/content/IntentFilter;
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 120
    .restart local v0    # "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 121
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 122
    return-void
.end method

.method private unregisterPrivateModeReceiver(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 162
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 164
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 168
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    goto :goto_0

    .line 167
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    .line 168
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    .line 169
    throw v1
.end method


# virtual methods
.method public backupCurrentState(IIIIII)V
    .locals 2
    .param p1, "currentTouchedIndex"    # I
    .param p2, "currentProportionID"    # I
    .param p3, "currentLayout"    # I
    .param p4, "currentBackgroundID"    # I
    .param p5, "currentBackgroundColor"    # I
    .param p6, "currentSaveSize"    # I

    .prologue
    .line 944
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    if-nez v0, :cond_0

    .line 946
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    .line 948
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    iput p1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentTouchedIndex:I

    .line 949
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    iput p2, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentProportionID:I

    .line 950
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    iput p3, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentLayout:I

    .line 951
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    iput p4, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentBackgroundID:I

    .line 952
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    iput p5, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentBackgroundColor:I

    .line 953
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    iput p6, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentSaveSize:I

    .line 954
    return-void
.end method

.method public buttonsDestroy()V
    .locals 1

    .prologue
    .line 746
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    .line 747
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->destroy()V

    .line 748
    :cond_0
    return-void
.end method

.method public changeImageData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 5
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    .line 930
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : changeImageData : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 931
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 932
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->changeCurrentIndexImage(Landroid/graphics/Bitmap;)Z

    .line 933
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->destroy()V

    .line 934
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 935
    return-void
.end method

.method public changeViewStatus(II)V
    .locals 11
    .param p1, "status"    # I
    .param p2, "style"    # I

    .prologue
    const/4 v10, 0x0

    .line 751
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 860
    :goto_0
    return-void

    .line 753
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_1

    .line 754
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 756
    :cond_1
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setCurrentMode(I)V

    .line 757
    const v0, 0xffff

    and-int/2addr v0, p1

    if-eqz v0, :cond_3

    .line 759
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->setSubMenuView()V

    .line 859
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->invalidateOptionsMenu()V

    goto :goto_0

    .line 763
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_5

    .line 765
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    if-eqz v0, :cond_4

    .line 767
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->backupCurrentState()V

    .line 770
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->destroy()V

    .line 771
    iput-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    .line 773
    :cond_5
    sparse-switch p1, :sswitch_data_0

    .line 856
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_2

    .line 857
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->setView()V

    goto :goto_1

    .line 776
    :sswitch_0
    const-string v0, "launcher"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_2

    .line 782
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_7

    .line 784
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->destroy()V

    .line 785
    iput-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 789
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    if-nez v0, :cond_8

    .line 791
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    .line 793
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 794
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 795
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 796
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    .line 797
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    .line 798
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 800
    const/4 v9, 0x0

    move-object v1, p0

    move v8, p2

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;II)V

    .line 791
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    .line 818
    :goto_3
    if-gtz p2, :cond_6

    .line 820
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    iget v1, v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentTouchedIndex:I

    .line 821
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    iget v2, v2, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentProportionID:I

    .line 822
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    iget v3, v3, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentLayout:I

    .line 823
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    iget v4, v4, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentBackgroundID:I

    .line 824
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    iget v5, v5, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentBackgroundColor:I

    .line 825
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    iget v6, v6, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentSaveSize:I

    .line 820
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setCurrentState(IIIIII)V

    .line 826
    iput-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    goto :goto_2

    .line 805
    :cond_8
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    .line 807
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 808
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 809
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 810
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    .line 811
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    .line 812
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 813
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    iget v8, v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentLayout:I

    .line 814
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCurrentCollageState:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;

    iget v9, v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentProportionID:I

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;II)V

    .line 805
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto :goto_3

    .line 832
    :sswitch_2
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    .line 834
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 835
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 836
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 837
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 832
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto/16 :goto_2

    .line 842
    :sswitch_3
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    .line 844
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 845
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 846
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 847
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 842
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto/16 :goto_2

    .line 852
    :sswitch_4
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->destroyMainData()V

    .line 853
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->finish()V

    goto/16 :goto_2

    .line 773
    nop

    :sswitch_data_0
    .sparse-switch
        0x11100000 -> :sswitch_3
        0x16000000 -> :sswitch_2
        0x1e110000 -> :sswitch_1
        0x2c000000 -> :sswitch_1
        0x2d000000 -> :sswitch_0
        0x2e000000 -> :sswitch_4
    .end sparse-switch
.end method

.method public checkIfValidData()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 480
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-nez v1, :cond_1

    .line 492
    :cond_0
    :goto_0
    return v0

    .line 484
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_0

    .line 488
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_0

    .line 492
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public destroyMainData()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 586
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->destroy()V

    .line 590
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 591
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_1

    .line 592
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->destroy()V

    .line 593
    :cond_1
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    .line 595
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 596
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->destroy()V

    .line 597
    :cond_2
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 599
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_3

    .line 600
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 601
    :cond_3
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 603
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_4

    .line 604
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->destroy()V

    .line 605
    :cond_4
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 607
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    if-eqz v0, :cond_5

    .line 608
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->destroy()V

    .line 609
    :cond_5
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 611
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    if-eqz v0, :cond_6

    .line 612
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->clear()V

    .line 613
    :cond_6
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    .line 615
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    if-eqz v0, :cond_7

    .line 616
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->destroy()V

    .line 617
    :cond_7
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mMultigridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    .line 619
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_8

    .line 621
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->destroy()V

    .line 622
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 624
    :cond_8
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->clear()V

    .line 625
    return-void
.end method

.method public getActionBarManager()Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 912
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method public getImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 916
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method public getPrivateSaveFolder()Ljava/lang/String;
    .locals 7

    .prologue
    .line 304
    const/4 v0, 0x0

    .line 305
    .local v0, "i":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 306
    .local v2, "privateRootFolder":Ljava/lang/String;
    move-object v3, v2

    .line 307
    .local v3, "saveFolder":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    if-eqz v4, :cond_2

    .line 310
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 312
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->isPersonalPage()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 314
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getPrivateSaveFolder()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Studio"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 317
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_1

    .line 318
    const/4 v0, 0x1

    .line 321
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v4

    if-lt v0, v4, :cond_3

    .line 341
    :goto_1
    if-ne v3, v2, :cond_2

    .line 342
    const/4 v3, 0x0

    .line 346
    :cond_2
    return-object v3

    .line 323
    :cond_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->isPersonalPage()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 327
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getPrivateSaveFolder()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 329
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 330
    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v1, v4, 0x1

    .line 331
    .local v1, "lastFileSeparatorIdx":I
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-le v1, v4, :cond_4

    .line 332
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    .line 333
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Studio"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 334
    goto :goto_1

    .line 321
    .end local v1    # "lastFileSeparatorIdx":I
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public initButtons()V
    .locals 1

    .prologue
    .line 739
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    .line 741
    const v0, 0x7f0900ba

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mBottomBtnLayout:Landroid/widget/LinearLayout;

    .line 743
    :cond_0
    return-void
.end method

.method public invalidateViews()V
    .locals 1

    .prologue
    .line 863
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_0

    .line 865
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->invalidateViewsWithThread()V

    .line 867
    :cond_0
    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 189
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 11
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 714
    if-eqz p3, :cond_0

    .line 716
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    .line 717
    .local v7, "bundle":Landroid/os/Bundle;
    const-string v0, "selectedCount"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 719
    .local v8, "count":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 721
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;>;"
    move v10, v8

    .local v10, "i":I
    :goto_0
    if-gtz v10, :cond_1

    .line 726
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    move-object v0, p0

    move v4, p1

    move v5, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->activityResult(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;Ljava/util/ArrayList;IILandroid/content/Intent;)V

    .line 729
    .end local v3    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;>;"
    .end local v7    # "bundle":Landroid/os/Bundle;
    .end local v8    # "count":I
    .end local v10    # "i":I
    :cond_0
    return-void

    .line 723
    .restart local v3    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;>;"
    .restart local v7    # "bundle":Landroid/os/Bundle;
    .restart local v8    # "count":I
    .restart local v10    # "i":I
    :cond_1
    new-instance v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    invoke-direct {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;-><init>()V

    .line 724
    .local v9, "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 721
    add-int/lit8 v10, v10, -0x1

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 560
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 561
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 563
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setPortrait()V

    .line 569
    :cond_0
    :goto_0
    const v0, 0x7f03004f

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->setContentView(I)V

    .line 570
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_1

    .line 571
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->onConfigurationChanged()V

    .line 573
    :cond_1
    const-string v0, "activity configurationChanged"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 574
    return-void

    .line 565
    :cond_2
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 567
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setLandscape()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v6, 0x400

    const/4 v7, 0x1

    .line 63
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 65
    const v5, 0x7f070001

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->setTheme(I)V

    .line 67
    :cond_0
    const/16 v5, 0x9

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->requestWindowFeature(I)Z

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    .line 71
    .local v4, "win":Landroid/view/Window;
    invoke-virtual {v4, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 73
    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 74
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 75
    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 76
    const/high16 v5, 0x100000

    iget v6, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/2addr v5, v6

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 77
    invoke-virtual {v4, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 84
    :try_start_0
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 85
    .local v0, "config":Landroid/view/ViewConfiguration;
    const-class v5, Landroid/view/ViewConfiguration;

    const-string v6, "sHasPermanentMenuKey"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 86
    .local v3, "menuKeyField":Ljava/lang/reflect/Field;
    if-eqz v3, :cond_1

    .line 88
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 89
    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    .end local v0    # "config":Landroid/view/ViewConfiguration;
    .end local v3    # "menuKeyField":Ljava/lang/reflect/Field;
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->initCircumstance()V

    .line 99
    const v5, 0x7f03004f

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->setContentView(I)V

    .line 101
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->createManager()V

    .line 102
    invoke-direct {p0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->registerPrivateModeReceiver(Landroid/content/Context;)V

    .line 104
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mIsFirst:Z

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->getImage(Landroid/content/Intent;)V

    .line 108
    const-string v5, "sensor"

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/hardware/SensorManager;

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_senMng:Landroid/hardware/SensorManager;

    .line 109
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_senMng:Landroid/hardware/SensorManager;

    invoke-virtual {v5, v7}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_senAccelerometer:Landroid/hardware/Sensor;

    .line 110
    return-void

    .line 92
    :catch_0
    move-exception v1

    .line 94
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 630
    const/4 v3, 0x0

    .line 631
    .local v3, "ret":Z
    const-string v4, "onCreateOptionsMenu()"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 633
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    const/high16 v5, 0x1c000000

    if-eq v4, v5, :cond_0

    .line 634
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    const/high16 v5, 0x2c000000

    if-eq v4, v5, :cond_0

    .line 635
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    const/high16 v5, 0x1e110000

    if-ne v4, v5, :cond_1

    .line 637
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 638
    .local v2, "inflater":Landroid/view/MenuInflater;
    const/high16 v4, 0x7f080000

    invoke-virtual {v2, v4, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 640
    .end local v2    # "inflater":Landroid/view/MenuInflater;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getButtonList()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getButtonList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 641
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getButtonList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_3

    .line 649
    .end local v1    # "i":I
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    .line 651
    return v3

    .line 642
    .restart local v1    # "i":I
    :cond_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getButtonList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 643
    .local v0, "button":Landroid/view/View;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v4

    const/16 v5, 0x10

    if-ne v4, v5, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_4

    .line 644
    const v4, 0x7f090157

    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_1

    .line 641
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 579
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->destroyMainData()V

    .line 580
    invoke-direct {p0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->unregisterPrivateModeReceiver(Landroid/content/Context;)V

    .line 581
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 582
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 548
    const/4 v0, 0x1

    .line 549
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v1, :cond_0

    .line 550
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->onKeyUp(ILandroid/view/KeyEvent;)Z

    .line 552
    :cond_0
    const/4 v1, 0x4

    if-eq p1, v1, :cond_1

    .line 553
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 555
    :cond_1
    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 696
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    if-eqz v0, :cond_1

    .line 698
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->onOptionsItemSelected(I)V

    .line 704
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 700
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    if-eqz v0, :cond_0

    .line 702
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->onOptionsItemSelected(I)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 525
    const-string v0, "onPause"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 526
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->pause()V

    .line 528
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCheckOnPause:Z

    .line 530
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 531
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x0

    .line 658
    const/4 v0, 0x0

    .line 659
    .local v0, "ret":Z
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x1c000000

    if-eq v1, v2, :cond_0

    .line 660
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x2c000000

    if-eq v1, v2, :cond_0

    .line 661
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x1e110000

    if-ne v1, v2, :cond_3

    .line 663
    :cond_0
    const-string v1, "preapareOptions"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 665
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 666
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->getAddImgCnt()I

    move-result v1

    if-nez v1, :cond_1

    .line 667
    const v1, 0x7f090155

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 669
    :cond_1
    if-eqz p1, :cond_2

    .line 671
    const v1, 0x7f090159

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 672
    const v1, 0x7f090156

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 679
    const v1, 0x7f090158

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 682
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_2

    .line 683
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setMenu(Landroid/view/Menu;)V

    .line 686
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->invalidateOptionsMenu()V

    .line 689
    :cond_3
    return v0
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 499
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 501
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->initCircumstance()V

    .line 502
    const-string v0, "onResume"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 503
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " : onResume"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 504
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCheckOnPause:Z

    if-eqz v0, :cond_0

    .line 506
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mCheckOnPause:Z

    .line 507
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->checkIfValidData()Z

    move-result v0

    if-nez v0, :cond_1

    .line 509
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->destroyMainData()V

    .line 510
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->reCreate()V

    .line 511
    const/high16 v0, 0x2e000000

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->changeViewStatus(II)V

    .line 512
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mIsFirst:Z

    .line 520
    :cond_0
    :goto_0
    return-void

    .line 516
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_0

    .line 517
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->resume()V

    goto :goto_0
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 9
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 194
    iget-object v4, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v4}, Landroid/hardware/Sensor;->getType()I

    move-result v4

    if-ne v4, v6, :cond_1

    .line 196
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 197
    .local v0, "lCurTime":J
    iget-wide v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_lLastTime:J

    sub-long v2, v0, v4

    .line 199
    .local v2, "lGabOfTime":J
    const-wide/16 v4, 0x1c2

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    .line 201
    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_lLastTime:J

    .line 203
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v7

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_fCurX:F

    .line 204
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v6

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_fCurY:F

    .line 205
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v8

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_fCurZ:F

    .line 207
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_fCurX:F

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_fCurY:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_fCurZ:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_fLastX:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_fLastY:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_fLastZ:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    long-to-float v5, v2

    div-float/2addr v4, v5

    const v5, 0x461c4000    # 10000.0f

    mul-float/2addr v4, v5

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_fSpeed:F

    .line 209
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_fSpeed:F

    const/high16 v5, 0x43be0000    # 380.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 211
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v4, v4, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    if-eqz v4, :cond_0

    .line 215
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v4, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->doShuffle()V

    .line 219
    :cond_0
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v7

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_fLastX:F

    .line 220
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v6

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_fLastY:F

    .line 221
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v8

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_fLastZ:F

    .line 224
    .end local v0    # "lCurTime":J
    .end local v2    # "lGabOfTime":J
    :cond_1
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 175
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 176
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_senAccelerometer:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_senMng:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_senAccelerometer:Landroid/hardware/Sensor;

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 178
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 182
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 183
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_senMng:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->m_senMng:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 185
    :cond_0
    return-void
.end method

.method public reCreate()V
    .locals 1

    .prologue
    .line 471
    const v0, 0x7f03004f

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->setContentView(I)V

    .line 472
    const-string v0, "reCreate"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 474
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->createManager()V

    .line 476
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mIsFirst:Z

    .line 477
    return-void
.end method

.method public setImageData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 1
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    .line 920
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 922
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->destroy()V

    .line 923
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 925
    :cond_0
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 926
    return-void
.end method

.method public setImageMultiGridView(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 732
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    if-eqz v0, :cond_0

    .line 733
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setImageMultiGridView(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;Landroid/graphics/Bitmap;)V

    .line 735
    :cond_0
    return-void
.end method

.method public showDialogManager(I)Z
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 875
    const/4 v0, 0x0

    return v0
.end method
