.class public Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
.super Ljava/lang/Object;
.source "PhotoRetouching.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/PhotoRetouching;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DecodeInfo"
.end annotation


# instance fields
.field public fileName:Ljava/lang/String;

.field public image:Landroid/graphics/Bitmap;

.field public index:I

.field public isCamera:Z

.field public isCanceled:Z

.field public isFinish:Z

.field public isMain:Z

.field public isUri:Z

.field public path:Ljava/lang/String;

.field public task:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

.field public thumbnail:Landroid/graphics/Bitmap;

.field public trayButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

.field public uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1196
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 1197
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 1198
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 1199
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->task:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    .line 1200
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->fileName:Ljava/lang/String;

    .line 1201
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isCamera:Z

    .line 1202
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isFinish:Z

    .line 1203
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isMain:Z

    .line 1204
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->thumbnail:Landroid/graphics/Bitmap;

    .line 1205
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    .line 1206
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->trayButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 1207
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isCanceled:Z

    .line 1215
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->index:I

    .line 1194
    return-void
.end method


# virtual methods
.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1210
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    if-eqz v0, :cond_0

    .line 1211
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 1213
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method
