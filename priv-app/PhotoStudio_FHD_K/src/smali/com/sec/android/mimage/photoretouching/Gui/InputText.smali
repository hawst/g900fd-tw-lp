.class public Lcom/sec/android/mimage/photoretouching/Gui/InputText;
.super Landroid/widget/EditText;
.source "InputText.java"


# static fields
.field public static final INPUT_TEXT_LENGTH:I = 0x14


# instance fields
.field private mChangeFileName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mPositivieButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 197
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mContext:Landroid/content/Context;

    .line 198
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mChangeFileName:Ljava/lang/String;

    .line 199
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mPositivieButton:Landroid/widget/Button;

    .line 25
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mContext:Landroid/content/Context;

    .line 26
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->init()V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 197
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mContext:Landroid/content/Context;

    .line 198
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mChangeFileName:Ljava/lang/String;

    .line 199
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mPositivieButton:Landroid/widget/Button;

    .line 32
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mContext:Landroid/content/Context;

    .line 33
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->init()V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 197
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mContext:Landroid/content/Context;

    .line 198
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mChangeFileName:Ljava/lang/String;

    .line 199
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mPositivieButton:Landroid/widget/Button;

    .line 39
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mContext:Landroid/content/Context;

    .line 40
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->init()V

    .line 41
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Gui/InputText;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Gui/InputText;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mPositivieButton:Landroid/widget/Button;

    return-object v0
.end method

.method private init()V
    .locals 4

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->selectAll()V

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->requestFocus()Z

    .line 56
    const/high16 v2, 0x10000000

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->setImeOptions(I)V

    .line 113
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/InputText$1;

    const/16 v2, 0x14

    invoke-direct {v0, p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/InputText$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/InputText;I)V

    .line 127
    .local v0, "lengthFilter":Landroid/text/InputFilter$LengthFilter;
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->setFilters([Landroid/text/InputFilter;)V

    .line 129
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/InputText$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/InputText$2;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/InputText;)V

    .line 194
    .local v1, "textWatcher":Landroid/text/TextWatcher;
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 195
    return-void
.end method


# virtual methods
.method public setCurEditTextContext(Ljava/lang/String;I)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "pos"    # I

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->setText(Ljava/lang/CharSequence;)V

    .line 45
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->setSelection(I)V

    .line 46
    return-void
.end method

.method public setPositiveButton(Landroid/widget/Button;)V
    .locals 0
    .param p1, "b"    # Landroid/widget/Button;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mPositivieButton:Landroid/widget/Button;

    .line 49
    return-void
.end method
