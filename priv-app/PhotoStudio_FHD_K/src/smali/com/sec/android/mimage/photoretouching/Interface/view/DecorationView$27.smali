.class Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;
.super Ljava/lang/Object;
.source "DecorationView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->init2DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    .line 1948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 1982
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1983
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->UNDOALL_DIALOG:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1985
    :cond_0
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1952
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1953
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewHeight()I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$29(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->undo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z

    .line 1954
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1955
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1956
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 1957
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 1958
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 1970
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1971
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1972
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 1977
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->refreshImageAndBottomButtons()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$30(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1978
    return-void

    .line 1960
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_0

    .line 1962
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    .line 1963
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableSave()V

    .line 1964
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 1965
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 1968
    :goto_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setIsEdited(Z)V

    goto :goto_0

    .line 1967
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v5, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_2

    .line 1974
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1975
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_1
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1991
    return-void
.end method
