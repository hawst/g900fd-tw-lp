.class Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$1;
.super Landroid/os/Handler;
.source "MultiTouchGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->startDoubleTapTimer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$1;->this$0:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 501
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 505
    const/4 v1, 0x0

    .line 506
    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$1;->this$0:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    # getter for: Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapFirstStart:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->access$2(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$1;->this$0:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    # getter for: Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapSecondStart:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->access$3(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 507
    :cond_0
    const/4 v2, 0x5

    .line 506
    if-lt v1, v2, :cond_3

    .line 520
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$1;->this$0:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    # getter for: Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCancelDoubleTap:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->access$4(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 521
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$1;->this$0:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    # invokes: Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->loadMotion()V
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->access$5(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)V

    .line 523
    :cond_2
    return-void

    .line 512
    :cond_3
    const-wide/16 v2, 0x1e

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 517
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 513
    :catch_0
    move-exception v0

    .line 515
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
