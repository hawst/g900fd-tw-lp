.class Lcom/sec/android/mimage/photoretouching/Gui/InputText$2;
.super Ljava/lang/Object;
.source "InputText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Gui/InputText;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private isContain:Z

.field private mBeforeCusorPos:I

.field private mBeforeString:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputText;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/InputText;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputText;

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText$2;->isContain:Z

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText$2;->mBeforeString:Ljava/lang/String;

    .line 133
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText$2;->mBeforeCusorPos:I

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputText;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mPositivieButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/InputText;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 143
    const-string v0, ""

    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputText;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mPositivieButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/InputText;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/InputText;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/InputText;->mPositivieButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/InputText;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 167
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/InputText$2;->mBeforeString:Ljava/lang/String;

    .line 169
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 191
    return-void
.end method
