.class Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$21;
.super Ljava/lang/Object;
.source "MultigridStyleView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getMultiImageFromGallary()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

.field private final synthetic val$addImgCnt:I


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$21;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    iput p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$21;->val$addImgCnt:I

    .line 2231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setImageMultiGridView(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 2236
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : setImageMultiGridView : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->index:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2237
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$21;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$24(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    move-result-object v1

    iget v2, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->index:I

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setOrgImage()V

    .line 2239
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$21;->val$addImgCnt:I

    .local v0, "i":I
    :goto_0
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$21;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$24(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v1

    if-ge v1, v0, :cond_1

    .line 2243
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$21;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$21;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$33(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)I

    move-result v2

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setLayoutStyle(I)V
    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$7(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;I)V

    .line 2244
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$21;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setSaveable(Z)Z
    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$22(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Z)Z

    .line 2245
    return-void

    .line 2241
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$21;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$24(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$21;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$24(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v2

    sub-int/2addr v2, v0

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setOrgImage()V

    .line 2239
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method
