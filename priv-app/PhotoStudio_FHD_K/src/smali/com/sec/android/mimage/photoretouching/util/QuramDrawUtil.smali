.class public Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;
.super Ljava/lang/Object;
.source "QuramDrawUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static drawCanvas(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 10
    .param p0, "imageData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    const/4 v2, 0x0

    .line 209
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewOutputBuffer()[I

    move-result-object v0

    if-eqz v0, :cond_0

    .line 211
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 213
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewWidth()I

    move-result v3

    .line 216
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewWidth()I

    move-result v6

    .line 217
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewHeight()I

    move-result v7

    .line 218
    const/4 v8, 0x1

    move-object v0, p1

    move v4, v2

    move v5, v2

    move-object v9, p2

    .line 211
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 220
    :cond_0
    return-void
.end method

.method public static drawCanvasInputBuffer(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 10
    .param p0, "imageData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    const/4 v2, 0x0

    .line 223
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 225
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewWidth()I

    move-result v3

    .line 228
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewWidth()I

    move-result v6

    .line 229
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewHeight()I

    move-result v7

    .line 230
    const/4 v8, 0x1

    move-object v0, p1

    move v4, v2

    move v5, v2

    move-object v9, p2

    .line 223
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 231
    return-void
.end method

.method public static drawCanvasWithPath(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;ILandroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 10
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "viewBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "imageData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .param p3, "color"    # I
    .param p4, "drawPaint"    # Landroid/graphics/Paint;
    .param p5, "clearPaint"    # Landroid/graphics/Paint;
    .param p6, "linePaint"    # Landroid/graphics/Paint;
    .param p7, "dashPathEffectPaint"    # Landroid/graphics/Paint;

    .prologue
    .line 59
    if-eqz p2, :cond_1

    .line 61
    invoke-interface {p2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v7

    .line 62
    .local v7, "supMatrixBasedOnViewTransform":Landroid/graphics/Matrix;
    invoke-interface {p2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v6

    .line 63
    .local v6, "supMatrix":Landroid/graphics/Matrix;
    invoke-interface {p2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v8

    .line 65
    .local v8, "viewTransform":Landroid/graphics/Matrix;
    if-eqz p1, :cond_1

    if-eqz v7, :cond_1

    .line 67
    invoke-virtual {p0, p1, v7, p4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 68
    invoke-interface {p2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPathBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 69
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 70
    .local v2, "c":Landroid/graphics/Canvas;
    invoke-virtual {v2, p5}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 72
    invoke-interface {p2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawPathList()Landroid/graphics/Path;

    move-result-object v5

    .line 76
    .local v5, "pathList":Landroid/graphics/Path;
    if-eqz v5, :cond_0

    .line 78
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 81
    .local v3, "dst":Landroid/graphics/Path;
    invoke-virtual {v5, v6, v3}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 82
    move-object/from16 v0, p6

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 83
    move-object/from16 v0, p6

    invoke-virtual {v2, v3, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 84
    const v9, 0xffffff

    xor-int/2addr v9, p3

    move-object/from16 v0, p7

    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 85
    move-object/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 87
    .end local v3    # "dst":Landroid/graphics/Path;
    :cond_0
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 89
    .local v4, "m":Landroid/graphics/Matrix;
    invoke-virtual {v4, v8}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 90
    invoke-virtual {p0, v1, v8, p4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 93
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "c":Landroid/graphics/Canvas;
    .end local v4    # "m":Landroid/graphics/Matrix;
    .end local v5    # "pathList":Landroid/graphics/Path;
    .end local v6    # "supMatrix":Landroid/graphics/Matrix;
    .end local v7    # "supMatrixBasedOnViewTransform":Landroid/graphics/Matrix;
    .end local v8    # "viewTransform":Landroid/graphics/Matrix;
    :cond_1
    return-void
.end method

.method public static drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V
    .locals 2
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "viewBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "imageData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 32
    if-eqz p2, :cond_0

    .line 34
    invoke-interface {p2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v0

    .line 35
    .local v0, "supMatrixBasedOnViewTransform":Landroid/graphics/Matrix;
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 37
    invoke-virtual {p0, p1, v0, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 42
    .end local v0    # "supMatrixBasedOnViewTransform":Landroid/graphics/Matrix;
    :cond_0
    :goto_0
    return-void

    .line 40
    .restart local v0    # "supMatrixBasedOnViewTransform":Landroid/graphics/Matrix;
    :cond_1
    const-string v1, "drawImage false"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static drawImage(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;)V
    .locals 21
    .param p0, "imageData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .prologue
    .line 97
    if-eqz p0, :cond_0

    .line 99
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalToPreviewMatrix()Landroid/graphics/Matrix;

    move-result-object v15

    .line 100
    .local v15, "m":Landroid/graphics/Matrix;
    if-eqz v15, :cond_0

    .line 102
    new-instance v14, Landroid/graphics/Matrix;

    invoke-direct {v14}, Landroid/graphics/Matrix;-><init>()V

    .line 103
    .local v14, "invert":Landroid/graphics/Matrix;
    invoke-virtual {v15, v14}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 104
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalInputData()[I

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewOutputBuffer()[I

    move-result-object v4

    if-eqz v4, :cond_1

    .line 106
    const/16 v4, 0x9

    new-array v0, v4, [F

    move-object/from16 v16, v0

    .line 107
    .local v16, "values":[F
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 109
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewOutputBuffer()[I

    move-result-object v4

    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewWidth()I

    move-result v5

    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewHeight()I

    move-result v6

    .line 110
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalInputData()[I

    move-result-object v7

    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalWidth()I

    move-result v8

    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalHeight()I

    move-result v9

    .line 111
    const/4 v10, 0x0

    aget v10, v16, v10

    const/4 v11, 0x2

    aget v11, v16, v11

    const/high16 v12, 0x3f000000    # 0.5f

    add-float/2addr v11, v12

    float-to-int v11, v11

    const/4 v12, 0x5

    aget v12, v16, v12

    const/high16 v13, 0x3f000000    # 0.5f

    add-float/2addr v12, v13

    float-to-int v12, v12

    .line 112
    new-instance v13, Landroid/graphics/Rect;

    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v17

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    .line 113
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v18

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    .line 114
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    .line 115
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v20

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    .line 112
    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v13, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 108
    invoke-static/range {v4 .. v13}, Lcom/sec/android/mimage/photoretouching/jni/Util;->native_drawImage([III[IIIFIILandroid/graphics/Rect;)Z

    .line 121
    .end local v14    # "invert":Landroid/graphics/Matrix;
    .end local v15    # "m":Landroid/graphics/Matrix;
    .end local v16    # "values":[F
    :cond_0
    :goto_0
    return-void

    .line 118
    .restart local v14    # "invert":Landroid/graphics/Matrix;
    .restart local v15    # "m":Landroid/graphics/Matrix;
    :cond_1
    const-string v4, "drawImage false"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static drawImageWithMaskContour(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;I)V
    .locals 22
    .param p0, "imageData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .param p1, "color"    # I

    .prologue
    .line 163
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalToPreviewMatrix()Landroid/graphics/Matrix;

    move-result-object v17

    .line 164
    .local v17, "m":Landroid/graphics/Matrix;
    if-eqz v17, :cond_0

    .line 166
    new-instance v16, Landroid/graphics/Matrix;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Matrix;-><init>()V

    .line 167
    .local v16, "invert":Landroid/graphics/Matrix;
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 168
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalInputData()[I

    move-result-object v3

    if-eqz v3, :cond_0

    .line 170
    const/16 v3, 0x9

    new-array v0, v3, [F

    move-object/from16 v18, v0

    .line 171
    .local v18, "values":[F
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 173
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewOutputBuffer()[I

    move-result-object v3

    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewWidth()I

    move-result v4

    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewHeight()I

    move-result v5

    .line 174
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalInputData()[I

    move-result-object v6

    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalWidth()I

    move-result v7

    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalHeight()I

    move-result v8

    .line 175
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalMaskBuffer()[B

    move-result-object v9

    .line 176
    const/4 v10, 0x0

    aget v10, v18, v10

    const/4 v11, 0x2

    aget v11, v18, v11

    float-to-int v11, v11

    const/4 v12, 0x5

    aget v12, v18, v12

    float-to-int v12, v12

    const v13, 0xffffff

    xor-int v14, p1, v13

    .line 177
    new-instance v15, Landroid/graphics/Rect;

    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v13

    iget v13, v13, Landroid/graphics/Rect;->left:I

    .line 178
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    .line 179
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v20

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    .line 180
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v21

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x1

    .line 177
    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v15, v13, v0, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    move/from16 v13, p1

    .line 172
    invoke-static/range {v3 .. v15}, Lcom/sec/android/mimage/photoretouching/jni/Util;->native_drawImageWithMaskContour([III[III[BFIIIILandroid/graphics/Rect;)Z

    .line 183
    .end local v16    # "invert":Landroid/graphics/Matrix;
    .end local v18    # "values":[F
    :cond_0
    return-void
.end method

.method public static drawOrgImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V
    .locals 2
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "viewBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "imageData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 45
    if-eqz p2, :cond_0

    .line 47
    invoke-interface {p2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOrgSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v0

    .line 48
    .local v0, "supMatrixBasedOnViewTransform":Landroid/graphics/Matrix;
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 50
    invoke-virtual {p0, p1, v0, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 55
    .end local v0    # "supMatrixBasedOnViewTransform":Landroid/graphics/Matrix;
    :cond_0
    :goto_0
    return-void

    .line 53
    .restart local v0    # "supMatrixBasedOnViewTransform":Landroid/graphics/Matrix;
    :cond_1
    const-string v1, "drawImage false"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static drawSmallImageWithMaskContour(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;[I[BI)V
    .locals 11
    .param p0, "imageData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .param p1, "preview"    # [I
    .param p2, "previewMask"    # [B
    .param p3, "color"    # I

    .prologue
    .line 191
    if-eqz p0, :cond_0

    .line 193
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewOutputBuffer()[I

    move-result-object v0

    .line 194
    .local v0, "output":[I
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 197
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewWidth()I

    move-result v1

    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewHeight()I

    move-result v2

    .line 198
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v4

    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v5

    .line 199
    const v3, 0xffffff

    xor-int v8, p3, v3

    .line 200
    new-instance v9, Landroid/graphics/Rect;

    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    .line 201
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->top:I

    .line 202
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Rect;->right:I

    add-int/lit8 v7, v7, -0x1

    .line 203
    invoke-interface {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v10, v10, -0x1

    .line 200
    invoke-direct {v9, v3, v6, v7, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v3, p1

    move-object v6, p2

    move v7, p3

    .line 196
    invoke-static/range {v0 .. v9}, Lcom/sec/android/mimage/photoretouching/jni/Util;->native_drawSmallImageWithMaskContour([III[III[BIILandroid/graphics/Rect;)Z

    .line 206
    .end local v0    # "output":[I
    :cond_0
    return-void
.end method

.method public static drawViewBufferBasedOnViewTransform(Landroid/graphics/Canvas;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V
    .locals 10
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    const/4 v2, 0x0

    .line 125
    if-eqz p1, :cond_0

    .line 127
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    .line 128
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 129
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Matrix;->toShortString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 130
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 132
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewWidth()I

    move-result v3

    .line 135
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewWidth()I

    move-result v6

    .line 136
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewHeight()I

    move-result v7

    .line 137
    const/4 v8, 0x1

    .line 138
    const/4 v9, 0x0

    move-object v0, p0

    move v4, v2

    move v5, v2

    .line 130
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 139
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    .line 141
    :cond_0
    return-void
.end method

.method public static drawViewInputBufferBasedOnViewTransform(Landroid/graphics/Canvas;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V
    .locals 10
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    const/4 v2, 0x0

    .line 144
    if-eqz p1, :cond_0

    .line 146
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    .line 147
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 148
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 150
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewWidth()I

    move-result v3

    .line 153
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewWidth()I

    move-result v6

    .line 154
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewHeight()I

    move-result v7

    .line 155
    const/4 v8, 0x1

    .line 156
    const/4 v9, 0x0

    move-object v0, p0

    move v4, v2

    move v5, v2

    .line 148
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 157
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    .line 159
    :cond_0
    return-void
.end method

.method public static transformVirtualPreviewCoordinate(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)V
    .locals 2
    .param p0, "event"    # Landroid/view/MotionEvent;
    .param p1, "applyMatrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 24
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 25
    .local v1, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v1, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 26
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 27
    .local v0, "inversM":Landroid/graphics/Matrix;
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 28
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 29
    return-void
.end method
