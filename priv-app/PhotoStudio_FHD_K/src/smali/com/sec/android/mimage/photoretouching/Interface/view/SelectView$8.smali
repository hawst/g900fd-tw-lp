.class Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$8;
.super Ljava/lang/Object;
.source "SelectView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initSelectNotiDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    .line 587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/4 v7, 0x0

    .line 591
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/widget/CheckBox;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 593
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/widget/CheckBox;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 595
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPackageInfo(Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 596
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 598
    .local v3, "versionCode":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/content/Context;

    move-result-object v5

    const-string v6, "PhotoEditor"

    invoke-static {v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 599
    .local v2, "settings":Landroid/content/SharedPreferences;
    const-string v5, "SELECT"

    invoke-interface {v2, v5, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 601
    .local v4, "viewedChangelogVersion":I
    if-ge v4, v3, :cond_0

    .line 602
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 603
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "SELECT"

    invoke-interface {v0, v5, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 604
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 608
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v2    # "settings":Landroid/content/SharedPreferences;
    .end local v3    # "versionCode":I
    .end local v4    # "viewedChangelogVersion":I
    :cond_0
    return-void
.end method
