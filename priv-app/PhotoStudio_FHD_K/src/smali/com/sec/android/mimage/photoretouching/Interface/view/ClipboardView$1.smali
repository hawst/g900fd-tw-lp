.class Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;
.super Landroid/os/Handler;
.source "ClipboardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    .line 800
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 23
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 804
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 806
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0601b9

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 909
    :goto_0
    return-void

    .line 811
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)I

    move-result v1

    const/4 v2, 0x5

    if-ge v1, v2, :cond_9

    .line 813
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 815
    .local v3, "objROI":Landroid/graphics/Rect;
    new-instance v15, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v15}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 816
    .local v15, "opt":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v15, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 818
    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v1, v15}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 820
    iget v4, v15, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 821
    .local v4, "objWidth":I
    iget v5, v15, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 824
    .local v5, "objHeight":I
    const/16 v19, 0x0

    .line 825
    .local v19, "tempWidth":I
    const/16 v18, 0x0

    .line 828
    .local v18, "tempHeight":I
    const/high16 v14, 0x3f800000    # 1.0f

    .line 830
    .local v14, "maxRatio":F
    const/high16 v16, 0x3f800000    # 1.0f

    .line 833
    .local v16, "ratio":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    if-gt v4, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v1

    if-gt v5, v1, :cond_6

    .line 835
    move/from16 v19, v4

    .line 836
    move/from16 v18, v5

    .line 851
    :goto_1
    if-ge v4, v5, :cond_8

    .line 852
    int-to-float v1, v4

    move/from16 v0, v19

    int-to-float v2, v0

    div-float v16, v1, v2

    .line 860
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    sub-int v1, v1, v19

    div-int/lit8 v12, v1, 0x2

    .line 861
    .local v12, "leftMargin":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v1

    sub-int v1, v1, v18

    div-int/lit8 v20, v1, 0x2

    .line 863
    .local v20, "topMargin":I
    iput v12, v3, Landroid/graphics/Rect;->left:I

    .line 864
    add-int v1, v12, v19

    iput v1, v3, Landroid/graphics/Rect;->right:I

    .line 866
    move/from16 v0, v20

    iput v0, v3, Landroid/graphics/Rect;->top:I

    .line 867
    add-int v1, v20, v18

    iput v1, v3, Landroid/graphics/Rect;->bottom:I

    .line 870
    iget v1, v3, Landroid/graphics/Rect;->left:I

    if-gez v1, :cond_1

    .line 871
    const/4 v1, 0x0

    iput v1, v3, Landroid/graphics/Rect;->left:I

    .line 872
    :cond_1
    iget v1, v3, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 873
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, v3, Landroid/graphics/Rect;->right:I

    .line 874
    :cond_2
    iget v1, v3, Landroid/graphics/Rect;->top:I

    if-gez v1, :cond_3

    .line 875
    const/4 v1, 0x0

    iput v1, v3, Landroid/graphics/Rect;->top:I

    .line 876
    :cond_3
    iget v1, v3, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v2

    if-lt v1, v2, :cond_4

    .line 877
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, v3, Landroid/graphics/Rect;->bottom:I

    .line 879
    :cond_4
    iget v1, v3, Landroid/graphics/Rect;->left:I

    iget v2, v3, Landroid/graphics/Rect;->right:I

    if-ge v1, v2, :cond_5

    .line 880
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewMatrix()Landroid/graphics/Matrix;

    move-result-object v13

    .line 881
    .local v13, "m":Landroid/graphics/Matrix;
    new-instance v17, Landroid/graphics/RectF;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/RectF;-><init>()V

    .line 882
    .local v17, "src":Landroid/graphics/RectF;
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 883
    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11}, Landroid/graphics/RectF;-><init>()V

    .line 884
    .local v11, "dst":Landroid/graphics/RectF;
    move-object/from16 v0, v17

    invoke-virtual {v13, v11, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 885
    iget v1, v11, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v2

    sub-int/2addr v1, v2

    .line 886
    iget v2, v11, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v6

    sub-int/2addr v2, v6

    .line 887
    iget v6, v11, Landroid/graphics/RectF;->right:F

    float-to-int v6, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v7

    sub-int/2addr v6, v7

    .line 888
    iget v7, v11, Landroid/graphics/RectF;->bottom:F

    float-to-int v7, v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v8

    sub-int/2addr v7, v8

    .line 885
    invoke-virtual {v3, v1, v2, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 890
    .end local v11    # "dst":Landroid/graphics/RectF;
    .end local v13    # "m":Landroid/graphics/Matrix;
    .end local v17    # "src":Landroid/graphics/RectF;
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)I

    move-result v22

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v2

    .line 894
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    .line 895
    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    .line 896
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)I

    move-result v8

    .line 897
    const/4 v9, 0x2

    .line 898
    const/4 v10, 0x1

    invoke-direct/range {v1 .. v10}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;-><init>(Landroid/content/Context;Landroid/graphics/Rect;IILcom/sec/android/mimage/photoretouching/Core/ImageData;Ljava/lang/String;IIZ)V

    .line 890
    aput-object v1, v21, v22

    .line 899
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;I)V

    .line 900
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;I)V

    .line 902
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 903
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->invalidateViews()V

    goto/16 :goto_0

    .line 838
    .end local v12    # "leftMargin":I
    .end local v20    # "topMargin":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    sub-int v1, v4, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v2

    sub-int v2, v5, v2

    if-le v1, v2, :cond_7

    .line 840
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v14

    float-to-int v0, v1

    move/from16 v19, v0

    .line 841
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, v4

    div-float v16, v1, v2

    .line 842
    int-to-float v1, v5

    mul-float v1, v1, v16

    float-to-int v0, v1

    move/from16 v18, v0

    .line 843
    goto/16 :goto_1

    .line 846
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v14

    float-to-int v0, v1

    move/from16 v18, v0

    .line 847
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, v5

    div-float v16, v1, v2

    .line 848
    int-to-float v1, v4

    mul-float v1, v1, v16

    float-to-int v0, v1

    move/from16 v19, v0

    goto/16 :goto_1

    .line 855
    :cond_8
    int-to-float v1, v5

    move/from16 v0, v18

    int-to-float v2, v0

    div-float v16, v1, v2

    goto/16 :goto_2

    .line 907
    .end local v3    # "objROI":Landroid/graphics/Rect;
    .end local v4    # "objWidth":I
    .end local v5    # "objHeight":I
    .end local v14    # "maxRatio":F
    .end local v15    # "opt":Landroid/graphics/BitmapFactory$Options;
    .end local v16    # "ratio":F
    .end local v18    # "tempHeight":I
    .end local v19    # "tempWidth":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0600fd

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
