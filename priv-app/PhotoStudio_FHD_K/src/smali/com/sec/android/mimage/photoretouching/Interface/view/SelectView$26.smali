.class Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;
.super Ljava/lang/Object;
.source "SelectView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initPinchZoomCallback()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    .line 1032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invalidate()V
    .locals 0

    .prologue
    .line 1077
    return-void
.end method

.method public onPinchZoom()V
    .locals 0

    .prologue
    .line 1072
    return-void
.end method

.method public onSingleTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    .line 1040
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "x,y[:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1041
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mOriginalInputImage:[I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)[I

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviewBuffer:[I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)[I

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1042
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1065
    :cond_0
    :goto_0
    return v4

    .line 1045
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    invoke-static {v0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Z)V

    .line 1046
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviewBuffer:[I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)[I

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->touchDown([ILandroid/view/MotionEvent;)V

    goto :goto_0

    .line 1049
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviewBuffer:[I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)[I

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->touchMove([ILandroid/view/MotionEvent;)V

    goto :goto_0

    .line 1053
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Z)V

    .line 1054
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviewBuffer:[I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)[I

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->touchUp([ILandroid/view/MotionEvent;)V

    .line 1056
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1058
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;)V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;)V

    .line 1059
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;->start()V

    goto :goto_0

    .line 1042
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
