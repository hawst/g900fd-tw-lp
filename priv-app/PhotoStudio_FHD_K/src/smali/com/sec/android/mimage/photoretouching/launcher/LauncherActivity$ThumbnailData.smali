.class public Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;
.super Ljava/lang/Object;
.source "LauncherActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ThumbnailData"
.end annotation


# instance fields
.field bitmap:[Landroid/graphics/Bitmap;

.field count:I

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

.field uri:[Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V
    .locals 3

    .prologue
    const/16 v2, 0x2d

    const/4 v1, 0x0

    .line 1289
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1314
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    .line 1290
    new-array v0, v2, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    .line 1291
    new-array v0, v2, [Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    .line 1292
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    .line 1293
    return-void
.end method


# virtual methods
.method public getThumbnail(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "u"    # Landroid/net/Uri;

    .prologue
    .line 1296
    const/4 v1, 0x0

    .line 1297
    .local v1, "ret":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    if-eqz v2, :cond_0

    .line 1299
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    .line 1310
    .end local v0    # "i":I
    :cond_0
    return-object v1

    .line 1301
    .restart local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    .line 1303
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1305
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v2, v0

    .line 1299
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
