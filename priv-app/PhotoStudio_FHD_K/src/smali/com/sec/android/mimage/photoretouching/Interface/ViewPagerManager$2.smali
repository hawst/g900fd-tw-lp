.class Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$2;
.super Ljava/lang/Object;
.source "ViewPagerManager.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->initColorPicker(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setBackgroundColor(I)Z
    .locals 4
    .param p1, "color"    # I

    .prologue
    .line 87
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mTime:J
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v2, 0x64

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 90
    const/4 v0, 0x0

    .line 96
    :goto_0
    return v0

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;->setBackgroundColor(I)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;I)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;J)V

    .line 96
    const/4 v0, 0x1

    goto :goto_0
.end method
