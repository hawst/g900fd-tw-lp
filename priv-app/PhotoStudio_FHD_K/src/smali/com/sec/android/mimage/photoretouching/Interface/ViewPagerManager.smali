.class public Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;
.super Ljava/lang/Object;
.source "ViewPagerManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;
    }
.end annotation


# instance fields
.field private final RANGE_BITMAP_LIST:I

.field id:[I

.field private mAnimate:Z

.field private mBackwardIndex:I

.field private mBackwardThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

.field public mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

.field mContext:Landroid/content/Context;

.field private mCurIdx:I

.field private mFlipperLinearLayout:Landroid/widget/FrameLayout;

.field private mForwardIndex:I

.field private mForwardThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

.field private mImageView:Landroid/widget/ImageView;

.field mIndicationId:[I

.field public mIndicatorLayout:Landroid/widget/FrameLayout;

.field private mIsIndicatorShowing:Z

.field private mIsShowColorPicker:Z

.field private mLastColor:I

.field private mOnCallBackForViewPager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$OnCallBackForViewPager;

.field public mPageImage:[Landroid/widget/ImageView;

.field public mPageLayout:[Landroid/widget/LinearLayout;

.field mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

.field private mResId:I

.field private mTime:J

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field public mViewPager:Landroid/support/v4/view/ViewPager;

.field private mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;ILcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "currentpage"    # I
    .param p3, "viewpagerinterface"    # Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;
    .param p4, "resid"    # I
    .param p5, "colorpicker"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mOnCallBackForViewPager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$OnCallBackForViewPager;

    .line 659
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 661
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->id:[I

    .line 662
    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 667
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIndicationId:[I

    .line 670
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 671
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPageLayout:[Landroid/widget/LinearLayout;

    .line 672
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPageImage:[Landroid/widget/ImageView;

    .line 673
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    .line 675
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIsIndicatorShowing:Z

    .line 676
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mAnimate:Z

    .line 678
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mContext:Landroid/content/Context;

    .line 680
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmap:Landroid/graphics/Bitmap;

    .line 681
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mImageView:Landroid/widget/ImageView;

    .line 683
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    .line 684
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mForwardThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    .line 685
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBackwardThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    .line 687
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->RANGE_BITMAP_LIST:I

    .line 689
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBackwardIndex:I

    .line 690
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mForwardIndex:I

    .line 691
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mCurIdx:I

    .line 692
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIndicatorLayout:Landroid/widget/FrameLayout;

    .line 693
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 694
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .line 695
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mResId:I

    .line 696
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIsShowColorPicker:Z

    .line 697
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mTime:J

    .line 698
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mLastColor:I

    .line 33
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mContext:Landroid/content/Context;

    .line 34
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mOnCallBackForViewPager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$OnCallBackForViewPager;

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$OnCallBackForViewPager;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mForwardThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    .line 35
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mOnCallBackForViewPager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$OnCallBackForViewPager;

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager$OnCallBackForViewPager;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBackwardThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    .line 36
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    .line 37
    iput p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mResId:I

    .line 38
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .line 39
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v0, v4, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->show(ZZ)Z

    .line 46
    :cond_0
    :goto_0
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->initView(I)V

    .line 47
    return-void

    .line 44
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v0, v2, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->show(ZZ)Z

    goto :goto_0

    .line 662
    nop

    :array_0
    .array-data 4
        0x7f090094
        0x7f090095
        0x7f090096
        0x7f090097
        0x7f090098
        0x7f090099
        0x7f09009a
        0x7f09009b
        0x7f09009c
        0x7f09009d
        0x7f09009e
        0x7f09009f
        0x7f0900a0
        0x7f0900a1
        0x7f0900a2
        0x7f0900a3
        0x7f0900a4
        0x7f0900a5
        0x7f0900a6
        0x7f0900a7
        0x7f0900a8
    .end array-data
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)J
    .locals 2

    .prologue
    .line 697
    iget-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mTime:J

    return-wide v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;
    .locals 1

    .prologue
    .line 688
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;I)V
    .locals 0

    .prologue
    .line 698
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mLastColor:I

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;J)V
    .locals 1

    .prologue
    .line 697
    iput-wide p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mTime:J

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;ILandroid/view/View;)V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->setBackGroundImage(ILandroid/view/View;)V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 681
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;Z)V
    .locals 0

    .prologue
    .line 676
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mAnimate:Z

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;Z)V
    .locals 0

    .prologue
    .line 675
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIsIndicatorShowing:Z

    return-void
.end method

.method private initColorPicker(I)V
    .locals 5
    .param p1, "currentpage"    # I

    .prologue
    .line 79
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    if-nez v1, :cond_0

    .line 81
    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .line 83
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$2;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setColorPickerCallback(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->DECO_BACKGROUND:I

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setBackground(I)V

    .line 102
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030038

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 103
    .local v0, "colorIconLayout":Landroid/widget/FrameLayout;
    const v1, 0x7f090091

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mImageView:Landroid/widget/ImageView;

    .line 104
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mResId:I

    const v2, 0x31200078

    if-lt v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mResId:I

    const v2, 0x3120008a

    if-le v1, v2, :cond_2

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mImageView:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 106
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 108
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mImageView:Landroid/widget/ImageView;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$3;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    return-void
.end method

.method private initIndicator(I)V
    .locals 6
    .param p1, "currentpage"    # I

    .prologue
    const/4 v5, 0x0

    .line 62
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03003a

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIndicatorLayout:Landroid/widget/FrameLayout;

    .line 63
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;->getCount()I

    move-result v1

    .line 64
    .local v1, "maxCnt":I
    new-array v2, v1, [Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPageImage:[Landroid/widget/ImageView;

    .line 65
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 74
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIndicatorLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 75
    return-void

    .line 67
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPageImage:[Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIndicatorLayout:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIndicationId:[I

    aget v4, v4, v0

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    aput-object v2, v3, v0

    .line 68
    if-ne p1, v0, :cond_1

    .line 69
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPageImage:[Landroid/widget/ImageView;

    aget-object v2, v2, v0

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 72
    :goto_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPageImage:[Landroid/widget/ImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 71
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPageImage:[Landroid/widget/ImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_1
.end method

.method private initListener()V
    .locals 1

    .prologue
    .line 383
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$5;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 400
    return-void
.end method

.method private setBackGroundImage(ILandroid/view/View;)V
    .locals 6
    .param p1, "bgImgId"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 136
    const/4 v1, 0x2

    new-array v0, v1, [I

    .line 137
    .local v0, "pos":[I
    invoke-virtual {p2, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 140
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    if-eqz v1, :cond_0

    .line 142
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIsShowColorPicker:Z

    if-eqz v1, :cond_1

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->hideColorpicker()V

    .line 145
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    aget v2, v0, v4

    aget v3, v0, v5

    invoke-virtual {v1, v5, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->show(ZIIZ)V

    .line 150
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0
.end method

.method private setThread(I)V
    .locals 10
    .param p1, "currentIdx"    # I

    .prologue
    const/4 v9, -0x1

    .line 243
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    invoke-interface {v8}, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;->getCount()I

    move-result v7

    .line 245
    .local v7, "maxPageNum":I
    add-int/lit8 v8, p1, -0x3

    if-gez v8, :cond_1

    const/4 v1, 0x0

    .line 246
    .local v1, "backwIdx":I
    :goto_0
    add-int/lit8 v8, p1, 0x3

    if-lt v8, v7, :cond_2

    add-int/lit8 v4, v7, -0x1

    .line 248
    .local v4, "forwIdx":I
    :goto_1
    const/4 v2, -0x1

    .line 249
    .local v2, "backwStart":I
    const/4 v5, -0x1

    .line 251
    .local v5, "forwStart":I
    const/4 v0, -0x1

    .line 252
    .local v0, "backwEnd":I
    const/4 v3, -0x1

    .line 254
    .local v3, "forwEnd":I
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBackwardIndex:I

    if-ne v8, v9, :cond_3

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mForwardIndex:I

    if-ne v8, v9, :cond_3

    .line 256
    add-int/lit8 v2, p1, -0x1

    .line 257
    add-int/lit8 v5, p1, 0x1

    .line 259
    move v0, v1

    .line 260
    move v3, v4

    .line 297
    :cond_0
    :goto_2
    move v6, v2

    .local v6, "i":I
    :goto_3
    if-ge v6, v0, :cond_7

    .line 304
    move v6, v5

    :goto_4
    if-le v6, v3, :cond_9

    .line 311
    return-void

    .line 245
    .end local v0    # "backwEnd":I
    .end local v1    # "backwIdx":I
    .end local v2    # "backwStart":I
    .end local v3    # "forwEnd":I
    .end local v4    # "forwIdx":I
    .end local v5    # "forwStart":I
    .end local v6    # "i":I
    :cond_1
    add-int/lit8 v1, p1, -0x3

    goto :goto_0

    .line 246
    .restart local v1    # "backwIdx":I
    :cond_2
    add-int/lit8 v4, p1, 0x3

    goto :goto_1

    .line 264
    .restart local v0    # "backwEnd":I
    .restart local v2    # "backwStart":I
    .restart local v3    # "forwEnd":I
    .restart local v4    # "forwIdx":I
    .restart local v5    # "forwStart":I
    :cond_3
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBackwardIndex:I

    if-eq v8, v1, :cond_4

    .line 266
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBackwardIndex:I

    if-ge v8, v1, :cond_5

    .line 268
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBackwardThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBackwardIndex:I

    invoke-virtual {v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->removeData(I)V

    .line 269
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBackwardIndex:I

    invoke-virtual {v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->removeBitmap(I)V

    .line 277
    :goto_5
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBackwardIndex:I

    .line 280
    :cond_4
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mForwardIndex:I

    if-eq v8, v4, :cond_0

    .line 282
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mForwardIndex:I

    if-ge v8, v4, :cond_6

    .line 284
    move v5, v4

    .line 285
    move v3, v4

    .line 293
    :goto_6
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mForwardIndex:I

    goto :goto_2

    .line 273
    :cond_5
    move v2, v1

    .line 274
    move v0, v1

    goto :goto_5

    .line 289
    :cond_6
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mForwardThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mForwardIndex:I

    invoke-virtual {v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->removeData(I)V

    .line 290
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mForwardIndex:I

    invoke-virtual {v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->removeBitmap(I)V

    goto :goto_6

    .line 299
    .restart local v6    # "i":I
    :cond_7
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    invoke-interface {v9}, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;->getBGColor()I

    move-result v9

    invoke-virtual {v8, v6, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->validBitmap(II)Z

    move-result v8

    if-nez v8, :cond_8

    .line 301
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBackwardThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    invoke-virtual {v8, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->setData(I)V

    .line 297
    :cond_8
    add-int/lit8 v6, v6, -0x1

    goto :goto_3

    .line 306
    :cond_9
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    invoke-interface {v9}, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;->getBGColor()I

    move-result v9

    invoke-virtual {v8, v6, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->validBitmap(II)Z

    move-result v8

    if-nez v8, :cond_a

    .line 308
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mForwardThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;

    invoke-virtual {v8, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerThreadManager;->setData(I)V

    .line 304
    :cond_a
    add-int/lit8 v6, v6, 0x1

    goto :goto_4
.end method


# virtual methods
.method public Release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 323
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmap:Landroid/graphics/Bitmap;

    .line 324
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmap:Landroid/graphics/Bitmap;

    .line 325
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->removeAllViews()V

    .line 327
    :cond_0
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 328
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 329
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mImageView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 330
    :cond_1
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mImageView:Landroid/widget/ImageView;

    .line 331
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 332
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    if-eqz v0, :cond_2

    .line 333
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->hideColorpicker()V

    .line 334
    :cond_2
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .line 335
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->destroy()V

    .line 336
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIndicatorLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_3

    .line 337
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIndicatorLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 338
    :cond_3
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIndicatorLayout:Landroid/widget/FrameLayout;

    .line 339
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPageImage:[Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 340
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPageImage:[Landroid/widget/ImageView;

    .line 342
    :cond_4
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 343
    return-void
.end method

.method public actionDown()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 368
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->hideColorpicker()V

    .line 373
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 374
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIsShowColorPicker:Z

    .line 379
    :cond_0
    :goto_0
    return-void

    .line 377
    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIsShowColorPicker:Z

    goto :goto_0
.end method

.method public focusViewPager()V
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->requestFocus()Z

    .line 705
    return-void
.end method

.method public getBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getBitmapListClass()Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    return-object v0
.end method

.method public getEffect(I)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 218
    const/4 v0, 0x0

    .line 219
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;->getBGColor()I

    move-result v2

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->validBitmap(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 221
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    invoke-virtual {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 223
    :cond_0
    return-object v0
.end method

.method public getLayout()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public getPagerAdapter()Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    return-object v0
.end method

.method public getViewPager()Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method public hideColorpicker()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 120
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v0, v1, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->show(ZZ)Z

    .line 124
    :cond_0
    return-void
.end method

.method public indicatorHide(Z)V
    .locals 9
    .param p1, "isConfig"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 450
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIndicatorLayout:Landroid/widget/FrameLayout;

    if-eqz v3, :cond_0

    .line 455
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .line 457
    const/high16 v8, 0x40800000    # 4.0f

    move v3, v1

    move v4, v2

    move v5, v1

    move v6, v2

    move v7, v1

    .line 455
    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 459
    .local v0, "animation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 461
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$6;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 483
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 485
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$7;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;Landroid/view/animation/Animation;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 494
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_0
    return-void
.end method

.method public indicatorShow(Z)V
    .locals 9
    .param p1, "animate"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 497
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIndicatorLayout:Landroid/widget/FrameLayout;

    if-eqz v3, :cond_0

    .line 499
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$8;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 507
    if-eqz p1, :cond_1

    .line 509
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .line 511
    const/high16 v6, 0x40800000    # 4.0f

    move v3, v1

    move v4, v2

    move v5, v1

    move v7, v1

    move v8, v2

    .line 509
    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 512
    .local v0, "animation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 513
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIsIndicatorShowing:Z

    .line 514
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$9;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 532
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 533
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$10;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;Landroid/view/animation/Animation;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 550
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_0
    :goto_0
    return-void

    .line 546
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIndicatorLayout:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_0

    .line 547
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mIndicatorLayout:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public initView(I)V
    .locals 0
    .param p1, "currentpage"    # I

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->setFlipperView(I)V

    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->initIndicator(I)V

    .line 57
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->initColorPicker(I)V

    .line 58
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->initListener()V

    .line 59
    return-void
.end method

.method public isAnimation()Z
    .locals 1

    .prologue
    .line 553
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mAnimate:Z

    return v0
.end method

.method public isFocusViewPager()Z
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->isFocused()Z

    move-result v0

    return v0
.end method

.method public removeViewPager()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 157
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->removeAllViews()V

    .line 159
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 160
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 162
    :cond_1
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 163
    return-void
.end method

.method public setEffect(I)V
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 187
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;->getBGColor()I

    move-result v2

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->validBitmap(II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 189
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    invoke-interface {v1, p1}, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;->applyEffect(I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmap:Landroid/graphics/Bitmap;

    .line 190
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 192
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;->getBGColor()I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, p1, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->setBitmap(Landroid/graphics/Bitmap;IIZ)V

    .line 194
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    invoke-virtual {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->getImageView(I)Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;

    move-result-object v0

    .line 195
    .local v0, "imageview":Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;
    if-eqz v0, :cond_0

    .line 198
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$4;

    invoke-direct {v2, p0, v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;I)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 215
    .end local v0    # "imageview":Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;
    :cond_0
    return-void
.end method

.method public setFlipperView(I)V
    .locals 4
    .param p1, "currentpage"    # I

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->removeViewPager()V

    .line 171
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    invoke-interface {v1}, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;->getCount()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    .line 172
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030075

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    .line 173
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->setEffect(I)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    const v1, 0x7f090154

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 176
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 178
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 179
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05025e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 184
    return-void
.end method

.method public showColorpicker()V
    .locals 3

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->show(ZZ)Z

    .line 132
    :cond_0
    return-void
.end method

.method public startUpdate()V
    .locals 3

    .prologue
    .line 228
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mCurIdx:I

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 230
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mCurIdx:I

    .line 231
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mCurIdx:I

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->setThread(I)V

    .line 233
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPageImage:[Landroid/widget/ImageView;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 237
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPageImage:[Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    aget-object v1, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 240
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 235
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mPageImage:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 233
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public updateBitmap(Landroid/graphics/Bitmap;I)Ljava/lang/Object;
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "position"    # I

    .prologue
    .line 427
    const/4 v2, 0x0

    .line 428
    .local v2, "view":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030044

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 429
    const v3, 0x7f0900d0

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 430
    .local v0, "currentLinearLayout":Landroid/widget/LinearLayout;
    const v3, 0x7f0900d1

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;

    .line 434
    .local v1, "imageView":Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    invoke-virtual {v3, v1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->setImageView(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;I)V

    .line 436
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;->getBGColor()I

    move-result v4

    invoke-virtual {v3, p2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->validBitmap(II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 438
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    invoke-virtual {v3, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 445
    :goto_0
    return-object v2

    .line 442
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->invalidate()V

    goto :goto_0
.end method
