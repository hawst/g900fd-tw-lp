.class public Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;
.super Ljava/lang/Object;
.source "PortraitEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FaceInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;,
        Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;
    }
.end annotation


# instance fields
.field public mEyes:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;

.field public mFacenum:I

.field public mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setFaceInfo(I[I)V
    .locals 5
    .param p1, "facenum"    # I
    .param p2, "info"    # [I

    .prologue
    .line 283
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFacenum:I

    .line 284
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFacenum:I

    new-array v3, v3, [Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    .line 285
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFacenum:I

    new-array v3, v3, [Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mEyes:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;

    .line 287
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFacenum:I

    if-lt v0, v3, :cond_0

    .line 292
    const/4 v1, 0x0

    .line 294
    .local v1, "index":I
    const/4 v0, 0x0

    :goto_1
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFacenum:I

    if-lt v0, v3, :cond_1

    .line 306
    return-void

    .line 288
    .end local v1    # "index":I
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;-><init>(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;)V

    aput-object v4, v3, v0

    .line 289
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mEyes:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;

    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;-><init>(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;)V

    aput-object v4, v3, v0

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 295
    .restart local v1    # "index":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    aget-object v3, v3, v0

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .local v2, "index":I
    aget v4, p2, v1

    iput v4, v3, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;->isValid:I

    .line 296
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    aget-object v3, v3, v0

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    aget v4, p2, v2

    iput v4, v3, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;->x:I

    .line 297
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    aget-object v3, v3, v0

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    aget v4, p2, v1

    iput v4, v3, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;->y:I

    .line 298
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    aget-object v3, v3, v0

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    aget v4, p2, v2

    iput v4, v3, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;->width:I

    .line 299
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    aget-object v3, v3, v0

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    aget v4, p2, v1

    iput v4, v3, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;->height:I

    .line 301
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mEyes:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;

    aget-object v3, v3, v0

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    aget v4, p2, v2

    iput v4, v3, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;->lx:I

    .line 302
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mEyes:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;

    aget-object v3, v3, v0

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    aget v4, p2, v1

    iput v4, v3, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;->ly:I

    .line 303
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mEyes:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;

    aget-object v3, v3, v0

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    aget v4, p2, v2

    iput v4, v3, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;->rx:I

    .line 304
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mEyes:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;

    aget-object v3, v3, v0

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    aget v4, p2, v1

    iput v4, v3, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;->ry:I

    .line 294
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    goto :goto_1
.end method
