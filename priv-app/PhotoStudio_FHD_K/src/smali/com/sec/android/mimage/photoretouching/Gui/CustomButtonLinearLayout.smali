.class public Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;
.super Landroid/widget/LinearLayout;
.source "CustomButtonLinearLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout$BottomLayoutCallback;
    }
.end annotation


# instance fields
.field mBottomLayoutCallback:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout$BottomLayoutCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->mBottomLayoutCallback:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout$BottomLayoutCallback;

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->mBottomLayoutCallback:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout$BottomLayoutCallback;

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->mBottomLayoutCallback:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout$BottomLayoutCallback;

    .line 31
    return-void
.end method


# virtual methods
.method public onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 41
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 42
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->mBottomLayoutCallback:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout$BottomLayoutCallback;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->mBottomLayoutCallback:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout$BottomLayoutCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout$BottomLayoutCallback;->onLayoutCallback()V

    .line 44
    :cond_0
    return-void
.end method

.method public setBottomLayoutCallback(Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout$BottomLayoutCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout$BottomLayoutCallback;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->mBottomLayoutCallback:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout$BottomLayoutCallback;

    .line 36
    return-void
.end method
