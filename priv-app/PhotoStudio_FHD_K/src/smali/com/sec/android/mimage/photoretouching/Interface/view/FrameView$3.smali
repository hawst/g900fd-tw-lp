.class Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$3;
.super Ljava/lang/Object;
.source "FrameView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/FrameEffect$OnCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterApplyPreview()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 126
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 127
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 130
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 133
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 134
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 135
    const/4 v8, 0x1

    .line 136
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Landroid/graphics/Paint;

    move-result-object v9

    move v4, v2

    move v5, v2

    .line 128
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 139
    const-string v2, "JW afterApplyPreview"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 140
    return-void
.end method
