.class public Lcom/sec/android/mimage/photoretouching/Core/Clipboard;
.super Lcom/sec/android/mimage/photoretouching/Core/RectController;
.source "Clipboard.java"


# instance fields
.field private OrgObjectBmp:Landroid/graphics/Bitmap;

.field private RotateObjectBmp:Landroid/graphics/Bitmap;

.field bApply:Z

.field private mCurrentResPath:Ljava/lang/String;

.field private mMyIndex:I

.field private mPreviewBmp:Landroid/graphics/Bitmap;

.field private mZOrder:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Rect;IILcom/sec/android/mimage/photoretouching/Core/ImageData;Ljava/lang/String;IIZ)V
    .locals 32
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "srcroi"    # Landroid/graphics/Rect;
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "imgData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p6, "path"    # Ljava/lang/String;
    .param p7, "zOrder"    # I
    .param p8, "boundaryType"    # I
    .param p9, "freeRect"    # Z

    .prologue
    .line 39
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    move/from16 v3, p8

    move/from16 v4, p9

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/RectController;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;IZ)V

    .line 32
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mPreviewBmp:Landroid/graphics/Bitmap;

    .line 34
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mCurrentResPath:Ljava/lang/String;

    .line 35
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mZOrder:I

    .line 36
    const/4 v5, -0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mMyIndex:I

    .line 41
    move/from16 v0, p7

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mZOrder:I

    move/from16 v0, p7

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mMyIndex:I

    .line 42
    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mCurrentResPath:Ljava/lang/String;

    .line 44
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 46
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->calcMinValueForClipboard()I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->minValueForClipboard:I

    .line 47
    const v5, 0x3dcccccd    # 0.1f

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->setCustomMinSize(F)V

    .line 49
    const/16 v27, 0x1

    .line 50
    .local v27, "resetRoi":Z
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v26

    .line 51
    .local v26, "prevWidth":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v25

    .line 53
    .local v25, "prevHeight":I
    div-int/lit8 v30, v26, 0x2

    .line 54
    .local v30, "x_center":I
    div-int/lit8 v31, v25, 0x2

    .line 55
    .local v31, "y_center":I
    new-instance v24, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 56
    .local v24, "opt":Landroid/graphics/BitmapFactory$Options;
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, v24

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 57
    .local v23, "objWidth":I
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, v24

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 59
    .local v21, "objHeight":I
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->minValueForClipboard:I

    move/from16 v0, v23

    if-lt v0, v5, :cond_0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->minValueForClipboard:I

    move/from16 v0, v21

    if-ge v0, v5, :cond_1

    .line 60
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->minValueForClipboard:I

    move/from16 v23, v0

    .line 61
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->minValueForClipboard:I

    move/from16 v21, v0

    .line 65
    :cond_1
    const/high16 v19, 0x3f400000    # 0.75f

    .line 66
    .local v19, "maxRatio":F
    move/from16 v0, v26

    move/from16 v1, v25

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    int-to-float v5, v5

    mul-float v5, v5, v19

    float-to-int v0, v5

    move/from16 v20, v0

    .line 67
    .local v20, "maximumSize":I
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->setMaximumSize(I)V

    .line 69
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    div-float v22, v5, v6

    .line 70
    .local v22, "objScale":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    move-result v6

    if-lt v5, v6, :cond_8

    .line 72
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    move/from16 v0, v26

    int-to-float v6, v0

    mul-float v6, v6, v19

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_2

    .line 74
    move/from16 v0, v26

    int-to-float v5, v0

    mul-float v5, v5, v19

    float-to-int v0, v5

    move/from16 v23, v0

    .line 75
    move/from16 v0, v23

    int-to-float v5, v0

    div-float v5, v5, v22

    float-to-int v0, v5

    move/from16 v21, v0

    .line 77
    :cond_2
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    move/from16 v0, v25

    int-to-float v6, v0

    mul-float v6, v6, v19

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_3

    .line 79
    move/from16 v0, v25

    int-to-float v5, v0

    mul-float v5, v5, v19

    float-to-int v0, v5

    move/from16 v21, v0

    .line 80
    move/from16 v0, v21

    int-to-float v5, v0

    mul-float v5, v5, v22

    float-to-int v0, v5

    move/from16 v23, v0

    .line 82
    move/from16 v0, v23

    int-to-float v5, v0

    move/from16 v0, v26

    int-to-float v6, v0

    mul-float v6, v6, v19

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_3

    .line 84
    move/from16 v0, v23

    int-to-float v5, v0

    mul-float v5, v5, v19

    float-to-int v0, v5

    move/from16 v23, v0

    .line 85
    move/from16 v0, v23

    int-to-float v5, v0

    div-float v5, v5, v22

    float-to-int v0, v5

    move/from16 v21, v0

    .line 110
    :cond_3
    :goto_0
    const/4 v5, 0x1

    move-object/from16 v0, v24

    iput-boolean v5, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 112
    move-object/from16 v0, p6

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 113
    const/16 v5, 0x7d0

    const/16 v6, 0x7d0

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v5

    move-object/from16 v0, v24

    iput v5, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 115
    const/4 v5, 0x0

    move-object/from16 v0, v24

    iput-boolean v5, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 116
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mh, "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 117
    move-object/from16 v0, p6

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 120
    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    .line 122
    .local v10, "matrix":Landroid/graphics/Matrix;
    const/16 v29, 0x0

    .line 123
    .local v29, "rotate":I
    invoke-static/range {p6 .. p6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotateDegree(Ljava/lang/String;)I

    move-result v29

    .line 125
    const/16 v5, 0x5a

    move/from16 v0, v29

    if-ne v0, v5, :cond_a

    .line 126
    const/high16 v5, 0x42b40000    # 90.0f

    invoke-virtual {v10, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 139
    :goto_1
    if-eqz v29, :cond_5

    .line 140
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    const/4 v11, 0x1

    invoke-static/range {v5 .. v11}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->RotateObjectBmp:Landroid/graphics/Bitmap;

    .line 141
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 142
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->RotateObjectBmp:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 144
    const/16 v5, 0x5a

    move/from16 v0, v29

    if-eq v0, v5, :cond_4

    const/16 v5, 0x10e

    move/from16 v0, v29

    if-ne v0, v5, :cond_5

    .line 145
    :cond_4
    new-instance v28, Landroid/graphics/Rect;

    invoke-direct/range {v28 .. v28}, Landroid/graphics/Rect;-><init>()V

    .line 146
    .local v28, "roi":Landroid/graphics/Rect;
    div-int/lit8 v5, v21, 0x2

    sub-int v5, v30, v5

    move-object/from16 v0, v28

    iput v5, v0, Landroid/graphics/Rect;->left:I

    .line 147
    move-object/from16 v0, v28

    iget v5, v0, Landroid/graphics/Rect;->left:I

    add-int v5, v5, v21

    add-int/lit8 v5, v5, -0x1

    move-object/from16 v0, v28

    iput v5, v0, Landroid/graphics/Rect;->right:I

    .line 148
    div-int/lit8 v5, v23, 0x2

    sub-int v5, v31, v5

    move-object/from16 v0, v28

    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 149
    move-object/from16 v0, v28

    iget v5, v0, Landroid/graphics/Rect;->top:I

    add-int v5, v5, v23

    add-int/lit8 v5, v5, -0x1

    move-object/from16 v0, v28

    iput v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 150
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->setRectRoi(Landroid/graphics/Rect;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    const/16 v27, 0x0

    .line 160
    .end local v28    # "roi":Landroid/graphics/Rect;
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v5

    if-nez v5, :cond_6

    .line 161
    new-instance v16, Landroid/graphics/Matrix;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Matrix;-><init>()V

    .line 162
    .local v16, "scalematrix":Landroid/graphics/Matrix;
    const/high16 v5, 0x3f000000    # 0.5f

    const/high16 v6, 0x3f000000    # 0.5f

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 163
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    const/16 v17, 0x1

    invoke-static/range {v11 .. v17}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mPreviewBmp:Landroid/graphics/Bitmap;

    .line 165
    .end local v16    # "scalematrix":Landroid/graphics/Matrix;
    :cond_6
    if-eqz v27, :cond_7

    .line 166
    new-instance v28, Landroid/graphics/Rect;

    invoke-direct/range {v28 .. v28}, Landroid/graphics/Rect;-><init>()V

    .line 167
    .restart local v28    # "roi":Landroid/graphics/Rect;
    div-int/lit8 v5, v23, 0x2

    sub-int v5, v30, v5

    move-object/from16 v0, v28

    iput v5, v0, Landroid/graphics/Rect;->left:I

    .line 168
    move-object/from16 v0, v28

    iget v5, v0, Landroid/graphics/Rect;->left:I

    add-int v5, v5, v23

    add-int/lit8 v5, v5, -0x1

    move-object/from16 v0, v28

    iput v5, v0, Landroid/graphics/Rect;->right:I

    .line 169
    div-int/lit8 v5, v21, 0x2

    sub-int v5, v31, v5

    move-object/from16 v0, v28

    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 170
    move-object/from16 v0, v28

    iget v5, v0, Landroid/graphics/Rect;->top:I

    add-int v5, v5, v21

    add-int/lit8 v5, v5, -0x1

    move-object/from16 v0, v28

    iput v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 171
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->setRectRoi(Landroid/graphics/Rect;)V

    .line 185
    .end local v28    # "roi":Landroid/graphics/Rect;
    :cond_7
    return-void

    .line 91
    .end local v10    # "matrix":Landroid/graphics/Matrix;
    .end local v29    # "rotate":I
    :cond_8
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    move/from16 v0, v25

    int-to-float v6, v0

    mul-float v6, v6, v19

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_9

    .line 93
    move/from16 v0, v25

    int-to-float v5, v0

    mul-float v5, v5, v19

    float-to-int v0, v5

    move/from16 v21, v0

    .line 94
    move/from16 v0, v21

    int-to-float v5, v0

    mul-float v5, v5, v22

    float-to-int v0, v5

    move/from16 v23, v0

    .line 96
    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    move/from16 v0, v26

    int-to-float v6, v0

    mul-float v6, v6, v19

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_3

    .line 98
    move/from16 v0, v26

    int-to-float v5, v0

    mul-float v5, v5, v19

    float-to-int v0, v5

    move/from16 v23, v0

    .line 99
    move/from16 v0, v23

    int-to-float v5, v0

    div-float v5, v5, v22

    float-to-int v0, v5

    move/from16 v21, v0

    .line 100
    move/from16 v0, v21

    int-to-float v5, v0

    move/from16 v0, v25

    int-to-float v6, v0

    mul-float v6, v6, v19

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_3

    .line 102
    move/from16 v0, v21

    int-to-float v5, v0

    mul-float v5, v5, v19

    float-to-int v0, v5

    move/from16 v21, v0

    .line 103
    move/from16 v0, v21

    int-to-float v5, v0

    mul-float v5, v5, v22

    float-to-int v0, v5

    move/from16 v23, v0

    goto/16 :goto_0

    .line 128
    .restart local v10    # "matrix":Landroid/graphics/Matrix;
    .restart local v29    # "rotate":I
    :cond_a
    const/16 v5, 0xb4

    move/from16 v0, v29

    if-ne v0, v5, :cond_b

    .line 129
    const/high16 v5, 0x43340000    # 180.0f

    invoke-virtual {v10, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto/16 :goto_1

    .line 131
    :cond_b
    const/16 v5, 0x10e

    move/from16 v0, v29

    if-ne v0, v5, :cond_c

    .line 132
    const/high16 v5, 0x43870000    # 270.0f

    invoke-virtual {v10, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto/16 :goto_1

    .line 135
    :cond_c
    const/16 v29, 0x0

    goto/16 :goto_1

    .line 156
    :catch_0
    move-exception v18

    .line 158
    .local v18, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto/16 :goto_2
.end method

.method private calcMinValueForClipboard()I
    .locals 9

    .prologue
    const/high16 v8, 0x40800000    # 4.0f

    const/high16 v7, 0x40400000    # 3.0f

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v5, 0x3fc00000    # 1.5f

    .line 622
    const/16 v1, 0x64

    .line 623
    .local v1, "minValue":I
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mPreviewWidth:I

    .line 624
    .local v0, "lesserPreviewSide":I
    const/high16 v2, 0x3f800000    # 1.0f

    .line 626
    .local v2, "ratio":F
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mPreviewHeight:I

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mPreviewWidth:I

    if-lt v3, v4, :cond_1

    .line 627
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mPreviewHeight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mPreviewWidth:I

    int-to-float v4, v4

    div-float v2, v3, v4

    .line 633
    :goto_0
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v2, v3

    if-ltz v3, :cond_2

    cmpg-float v3, v2, v5

    if-gtz v3, :cond_2

    .line 634
    div-int/lit8 v1, v0, 0x8

    .line 643
    :cond_0
    :goto_1
    return v1

    .line 630
    :cond_1
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mPreviewHeight:I

    .line 631
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mPreviewWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mPreviewHeight:I

    int-to-float v4, v4

    div-float v2, v3, v4

    goto :goto_0

    .line 635
    :cond_2
    cmpl-float v3, v2, v5

    if-lez v3, :cond_3

    cmpg-float v3, v2, v6

    if-gtz v3, :cond_3

    .line 636
    div-int/lit8 v1, v0, 0x6

    goto :goto_1

    .line 637
    :cond_3
    cmpl-float v3, v2, v6

    if-lez v3, :cond_4

    cmpg-float v3, v2, v7

    if-gtz v3, :cond_4

    .line 638
    div-int/lit8 v1, v0, 0x4

    goto :goto_1

    .line 639
    :cond_4
    cmpl-float v3, v2, v7

    if-lez v3, :cond_5

    cmpg-float v3, v2, v8

    if-gtz v3, :cond_5

    .line 640
    div-int/lit8 v1, v0, 0x3

    goto :goto_1

    .line 641
    :cond_5
    cmpl-float v3, v2, v8

    if-lez v3, :cond_0

    .line 642
    div-int/lit8 v1, v0, 0x2

    goto :goto_1
.end method

.method private getSampleSize(II)I
    .locals 3
    .param p1, "bitmapSize"    # I
    .param p2, "maxSize"    # I

    .prologue
    .line 189
    const/4 v1, 0x1

    .line 190
    .local v1, "ret":I
    if-le p1, p2, :cond_0

    .line 192
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    div-int v2, p1, v0

    if-gt v2, p2, :cond_1

    .line 196
    add-int/lit8 v1, v1, 0x1

    .line 200
    .end local v0    # "i":I
    :cond_0
    return v1

    .line 194
    .restart local v0    # "i":I
    :cond_1
    move v1, v0

    .line 192
    mul-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method public EndMoveObject()V
    .locals 0

    .prologue
    .line 545
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->EndMoveObject()V

    .line 546
    return-void
.end method

.method public GetObjectBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mPreviewBmp:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public GetOrgObjectBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public InitMoveObject(FF)I
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 536
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->InitMoveObject(FF)I

    move-result v0

    return v0
.end method

.method public StartMoveObject(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 541
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->StartMoveObject(FF)V

    .line 542
    return-void
.end method

.method public applyOriginal(Landroid/graphics/Bitmap;)V
    .locals 16
    .param p1, "originalBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 347
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v12}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalToPreviewRatio()F

    move-result v8

    .line 348
    .local v8, "scale":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getRoi()Landroid/graphics/RectF;

    move-result-object v5

    .line 350
    .local v5, "rectRoi":Landroid/graphics/RectF;
    iget v12, v5, Landroid/graphics/RectF;->left:F

    div-float/2addr v12, v8

    iput v12, v5, Landroid/graphics/RectF;->left:F

    .line 351
    iget v12, v5, Landroid/graphics/RectF;->top:F

    div-float/2addr v12, v8

    iput v12, v5, Landroid/graphics/RectF;->top:F

    .line 352
    iget v12, v5, Landroid/graphics/RectF;->right:F

    div-float/2addr v12, v8

    iput v12, v5, Landroid/graphics/RectF;->right:F

    .line 353
    iget v12, v5, Landroid/graphics/RectF;->bottom:F

    div-float/2addr v12, v8

    iput v12, v5, Landroid/graphics/RectF;->bottom:F

    .line 356
    new-instance v1, Landroid/graphics/Canvas;

    move-object/from16 v0, p1

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 357
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetOrgObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 359
    invoke-virtual {v1}, Landroid/graphics/Canvas;->save()I

    .line 360
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getAngle()I

    move-result v12

    int-to-float v12, v12

    .line 361
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v13

    iget v13, v13, Landroid/graphics/PointF;->x:F

    div-float/2addr v13, v8

    .line 362
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v14

    iget v14, v14, Landroid/graphics/PointF;->y:F

    div-float/2addr v14, v8

    .line 360
    invoke-virtual {v1, v12, v13, v14}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 364
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetOrgObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    .line 365
    .local v11, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetOrgObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 366
    .local v3, "height":I
    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v12

    float-to-int v7, v12

    .line 367
    .local v7, "resizeWidth":I
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v12

    float-to-int v6, v12

    .line 368
    .local v6, "resizeHeight":I
    int-to-float v12, v7

    int-to-float v13, v11

    div-float v10, v12, v13

    .line 369
    .local v10, "scaleWidth":F
    int-to-float v12, v6

    int-to-float v13, v3

    div-float v9, v12, v13

    .line 371
    .local v9, "scaleHeight":F
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 372
    .local v4, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v4, v10, v9}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 373
    iget v12, v5, Landroid/graphics/RectF;->left:F

    iget v13, v5, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v12, v13}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 375
    const/high16 v12, 0x3f800000    # 1.0f

    cmpl-float v12, v10, v12

    if-eqz v12, :cond_1

    .line 378
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetOrgObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v1, v12, v4, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    :goto_0
    invoke-virtual {v1}, Landroid/graphics/Canvas;->restore()V

    .line 390
    .end local v3    # "height":I
    .end local v4    # "matrix":Landroid/graphics/Matrix;
    .end local v6    # "resizeHeight":I
    .end local v7    # "resizeWidth":I
    .end local v9    # "scaleHeight":F
    .end local v10    # "scaleWidth":F
    .end local v11    # "width":I
    :cond_0
    :goto_1
    return-void

    .line 380
    .restart local v3    # "height":I
    .restart local v4    # "matrix":Landroid/graphics/Matrix;
    .restart local v6    # "resizeHeight":I
    .restart local v7    # "resizeWidth":I
    .restart local v9    # "scaleHeight":F
    .restart local v10    # "scaleWidth":F
    .restart local v11    # "width":I
    :catch_0
    move-exception v2

    .line 381
    .local v2, "e":Ljava/lang/Exception;
    goto :goto_1

    .line 386
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetOrgObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    iget v13, v5, Landroid/graphics/RectF;->left:F

    iget v14, v5, Landroid/graphics/RectF;->top:F

    const/4 v15, 0x0

    invoke-virtual {v1, v12, v13, v14, v15}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public applyPreview(Landroid/graphics/Bitmap;)V
    .locals 34
    .param p1, "preview"    # Landroid/graphics/Bitmap;

    .prologue
    .line 553
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getRoi()Landroid/graphics/RectF;

    move-result-object v19

    .line 554
    .local v19, "rectRoi":Landroid/graphics/RectF;
    new-instance v11, Landroid/graphics/Canvas;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 555
    .local v11, "canvas":Landroid/graphics/Canvas;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 557
    invoke-virtual {v11}, Landroid/graphics/Canvas;->save()I

    .line 558
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getAngle()I

    move-result v3

    if-eqz v3, :cond_7

    .line 559
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getAngle()I

    move-result v3

    int-to-double v4, v3

    const-wide v30, 0x400921fb54442d18L    # Math.PI

    mul-double v4, v4, v30

    const-wide v30, 0x4066800000000000L    # 180.0

    div-double v4, v4, v30

    double-to-float v9, v4

    .line 560
    .local v9, "angle":F
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 561
    .local v7, "mat":Landroid/graphics/Matrix;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getAngle()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 563
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->height()F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 564
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v25

    .line 565
    .local v25, "tmp_w":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->height()F

    move-result v4

    mul-float/2addr v3, v4

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 v24, v0

    .line 571
    .local v24, "tmp_h":I
    :goto_0
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v25

    move/from16 v1, v24

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 572
    .local v2, "bmp":Landroid/graphics/Bitmap;
    new-instance v10, Landroid/graphics/Canvas;

    invoke-direct {v10, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 573
    .local v10, "can":Landroid/graphics/Canvas;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    new-instance v4, Landroid/graphics/Rect;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v29

    move/from16 v0, v29

    invoke-direct {v4, v5, v6, v8, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v29

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v30

    move/from16 v0, v29

    move/from16 v1, v30

    invoke-direct {v5, v6, v8, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v10, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 574
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 575
    .local v21, "tmp":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 576
    move-object/from16 v22, v21

    .line 577
    .local v22, "tmp2":Landroid/graphics/Bitmap;
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->width()F

    move-result v4

    mul-float/2addr v3, v4

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->height()F

    move-result v5

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float v12, v3, v4

    .line 578
    .local v12, "h":F
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v20, v0

    .line 579
    .local v20, "theta":F
    const/4 v3, 0x4

    new-array v0, v3, [F

    move-object/from16 v27, v0

    .line 580
    .local v27, "x":[F
    const/4 v3, 0x4

    new-array v0, v3, [F

    move-object/from16 v28, v0

    .line 581
    .local v28, "y":[F
    const/4 v3, 0x0

    float-to-double v4, v12

    add-float v6, v20, v9

    float-to-double v0, v6

    move-wide/from16 v30, v0

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->cos(D)D

    move-result-wide v30

    mul-double v4, v4, v30

    double-to-float v4, v4

    aput v4, v27, v3

    .line 582
    const/4 v3, 0x0

    float-to-double v4, v12

    add-float v6, v20, v9

    float-to-double v0, v6

    move-wide/from16 v30, v0

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->sin(D)D

    move-result-wide v30

    mul-double v4, v4, v30

    double-to-float v4, v4

    aput v4, v28, v3

    .line 583
    const/4 v3, 0x1

    float-to-double v4, v12

    const-wide v30, 0x400921fb54442d18L    # Math.PI

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v32, v0

    sub-double v30, v30, v32

    float-to-double v0, v9

    move-wide/from16 v32, v0

    add-double v30, v30, v32

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->cos(D)D

    move-result-wide v30

    mul-double v4, v4, v30

    double-to-float v4, v4

    aput v4, v27, v3

    .line 584
    const/4 v3, 0x1

    float-to-double v4, v12

    const-wide v30, 0x400921fb54442d18L    # Math.PI

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v32, v0

    sub-double v30, v30, v32

    float-to-double v0, v9

    move-wide/from16 v32, v0

    add-double v30, v30, v32

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->sin(D)D

    move-result-wide v30

    mul-double v4, v4, v30

    double-to-float v4, v4

    aput v4, v28, v3

    .line 585
    const/4 v3, 0x2

    float-to-double v4, v12

    const-wide v30, 0x400921fb54442d18L    # Math.PI

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v32, v0

    add-double v30, v30, v32

    float-to-double v0, v9

    move-wide/from16 v32, v0

    add-double v30, v30, v32

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->cos(D)D

    move-result-wide v30

    mul-double v4, v4, v30

    double-to-float v4, v4

    aput v4, v27, v3

    .line 586
    const/4 v3, 0x2

    float-to-double v4, v12

    const-wide v30, 0x400921fb54442d18L    # Math.PI

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v32, v0

    add-double v30, v30, v32

    float-to-double v0, v9

    move-wide/from16 v32, v0

    add-double v30, v30, v32

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->sin(D)D

    move-result-wide v30

    mul-double v4, v4, v30

    double-to-float v4, v4

    aput v4, v28, v3

    .line 587
    const/4 v3, 0x3

    float-to-double v4, v12

    move/from16 v0, v20

    neg-float v6, v0

    add-float/2addr v6, v9

    float-to-double v0, v6

    move-wide/from16 v30, v0

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->cos(D)D

    move-result-wide v30

    mul-double v4, v4, v30

    double-to-float v4, v4

    aput v4, v27, v3

    .line 588
    const/4 v3, 0x3

    float-to-double v4, v12

    move/from16 v0, v20

    neg-float v6, v0

    add-float/2addr v6, v9

    float-to-double v0, v6

    move-wide/from16 v30, v0

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->sin(D)D

    move-result-wide v30

    mul-double v4, v4, v30

    double-to-float v4, v4

    aput v4, v28, v3

    .line 589
    const/4 v3, 0x0

    aget v15, v27, v3

    .local v15, "maxX":F
    const/4 v3, 0x0

    aget v17, v27, v3

    .local v17, "minX":F
    const/4 v3, 0x0

    aget v16, v28, v3

    .local v16, "maxY":F
    const/4 v3, 0x0

    aget v18, v28, v3

    .line 590
    .local v18, "minY":F
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    const/4 v3, 0x4

    if-lt v14, v3, :cond_2

    .line 604
    sub-float v26, v15, v17

    .line 605
    .local v26, "width":F
    sub-float v13, v16, v18

    .line 606
    .local v13, "height":F
    move/from16 v0, v26

    float-to-int v3, v0

    float-to-int v4, v13

    const/4 v5, 0x1

    move-object/from16 v0, v22

    invoke-static {v0, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 607
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Bitmap;->recycle()V

    .line 608
    new-instance v23, Landroid/graphics/RectF;

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v26, v4

    sub-float/2addr v3, v4

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v13, v5

    sub-float/2addr v4, v5

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->centerX()F

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v26, v6

    add-float/2addr v5, v6

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->centerY()F

    move-result v6

    const/high16 v8, 0x40000000    # 2.0f

    div-float v8, v13, v8

    add-float/2addr v6, v8

    move-object/from16 v0, v23

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 609
    .local v23, "tmp3":Landroid/graphics/RectF;
    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/RectF;->top:F

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v11, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 610
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->recycle()V

    .line 617
    .end local v2    # "bmp":Landroid/graphics/Bitmap;
    .end local v7    # "mat":Landroid/graphics/Matrix;
    .end local v9    # "angle":F
    .end local v10    # "can":Landroid/graphics/Canvas;
    .end local v12    # "h":F
    .end local v13    # "height":F
    .end local v14    # "i":I
    .end local v15    # "maxX":F
    .end local v16    # "maxY":F
    .end local v17    # "minX":F
    .end local v18    # "minY":F
    .end local v20    # "theta":F
    .end local v22    # "tmp2":Landroid/graphics/Bitmap;
    .end local v23    # "tmp3":Landroid/graphics/RectF;
    .end local v24    # "tmp_h":I
    .end local v25    # "tmp_w":I
    .end local v26    # "width":F
    .end local v27    # "x":[F
    .end local v28    # "y":[F
    :goto_2
    invoke-virtual {v11}, Landroid/graphics/Canvas;->restore()V

    .line 619
    .end local v21    # "tmp":Landroid/graphics/Bitmap;
    :cond_0
    return-void

    .line 568
    .restart local v7    # "mat":Landroid/graphics/Matrix;
    .restart local v9    # "angle":F
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    .line 569
    .restart local v24    # "tmp_h":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->width()F

    move-result v4

    mul-float/2addr v3, v4

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->height()F

    move-result v4

    div-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 v25, v0

    .restart local v25    # "tmp_w":I
    goto/16 :goto_0

    .line 591
    .restart local v2    # "bmp":Landroid/graphics/Bitmap;
    .restart local v10    # "can":Landroid/graphics/Canvas;
    .restart local v12    # "h":F
    .restart local v14    # "i":I
    .restart local v15    # "maxX":F
    .restart local v16    # "maxY":F
    .restart local v17    # "minX":F
    .restart local v18    # "minY":F
    .restart local v20    # "theta":F
    .restart local v21    # "tmp":Landroid/graphics/Bitmap;
    .restart local v22    # "tmp2":Landroid/graphics/Bitmap;
    .restart local v27    # "x":[F
    .restart local v28    # "y":[F
    :cond_2
    aget v3, v27, v14

    cmpg-float v3, v15, v3

    if-gez v3, :cond_3

    .line 592
    aget v15, v27, v14

    .line 594
    :cond_3
    aget v3, v27, v14

    cmpl-float v3, v17, v3

    if-lez v3, :cond_4

    .line 595
    aget v17, v27, v14

    .line 597
    :cond_4
    aget v3, v28, v14

    cmpg-float v3, v16, v3

    if-gez v3, :cond_5

    .line 598
    aget v16, v28, v14

    .line 600
    :cond_5
    aget v3, v28, v14

    cmpl-float v3, v18, v3

    if-lez v3, :cond_6

    .line 601
    aget v18, v28, v14

    .line 590
    :cond_6
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_1

    .line 613
    .end local v2    # "bmp":Landroid/graphics/Bitmap;
    .end local v7    # "mat":Landroid/graphics/Matrix;
    .end local v9    # "angle":F
    .end local v10    # "can":Landroid/graphics/Canvas;
    .end local v12    # "h":F
    .end local v14    # "i":I
    .end local v15    # "maxX":F
    .end local v16    # "maxY":F
    .end local v17    # "minX":F
    .end local v18    # "minY":F
    .end local v20    # "theta":F
    .end local v21    # "tmp":Landroid/graphics/Bitmap;
    .end local v22    # "tmp2":Landroid/graphics/Bitmap;
    .end local v24    # "tmp_h":I
    .end local v25    # "tmp_w":I
    .end local v27    # "x":[F
    .end local v28    # "y":[F
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/RectF;->height()F

    move-result v5

    float-to-int v5, v5

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 614
    .restart local v21    # "tmp":Landroid/graphics/Bitmap;
    move-object/from16 v0, v19

    iget v3, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, v19

    iget v4, v0, Landroid/graphics/RectF;->top:F

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v11, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 615
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_2
.end method

.method public calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 6
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "reqWidth"    # I
    .param p3, "reqHeight"    # I

    .prologue
    .line 204
    iget v2, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 205
    .local v2, "height":I
    iget v4, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 206
    .local v4, "width":I
    const/4 v3, 0x1

    .line 207
    .local v3, "inSampleSize":I
    if-gt v2, p3, :cond_0

    if-le v4, p2, :cond_1

    .line 208
    :cond_0
    div-int/lit8 v0, v2, 0x2

    .line 209
    .local v0, "halfHeight":I
    div-int/lit8 v1, v4, 0x2

    .line 210
    .local v1, "halfWidth":I
    :goto_0
    div-int v5, v0, v3

    if-gt v5, p3, :cond_2

    .line 211
    div-int v5, v1, v3

    .line 210
    if-gt v5, p2, :cond_2

    .line 215
    .end local v0    # "halfHeight":I
    .end local v1    # "halfWidth":I
    :cond_1
    return v3

    .line 212
    .restart local v0    # "halfHeight":I
    .restart local v1    # "halfWidth":I
    :cond_2
    mul-int/lit8 v3, v3, 0x2

    goto :goto_0
.end method

.method public destory()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 221
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->RotateObjectBmp:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->RotateObjectBmp:Landroid/graphics/Bitmap;

    .line 222
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mPreviewBmp:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mPreviewBmp:Landroid/graphics/Bitmap;

    .line 223
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->destroy()V

    .line 224
    return-void
.end method

.method public getAngle()I
    .locals 1

    .prologue
    .line 316
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mAngle:I

    return v0
.end method

.method public getCurrentResPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mCurrentResPath:Ljava/lang/String;

    return-object v0
.end method

.method public getDrawBdry()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getDrawCenterPT()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 340
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 341
    .local v0, "ret":Landroid/graphics/PointF;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewPaddingX()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 342
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewPaddingY()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 343
    return-object v0
.end method

.method public getOrgDestROI()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mOriginalROI:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getZOrder()I
    .locals 1

    .prologue
    .line 320
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mZOrder:I

    return v0
.end method

.method public setDestROI(IIII)V
    .locals 8
    .param p1, "left"    # I
    .param p2, "right"    # I
    .param p3, "top"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 231
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    int-to-float v4, p1

    int-to-float v5, p3

    int-to-float v6, p2

    int-to-float v7, p4

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 233
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    float-to-int v0, v3

    .line 234
    .local v0, "offset_x":I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    float-to-int v1, v3

    .line 237
    .local v1, "offset_y":I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mAngle:I

    int-to-float v5, v5

    int-to-float v6, v0

    int-to-float v7, v1

    invoke-static {v3, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v2

    .line 238
    .local v2, "pt":Landroid/graphics/PointF;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mDestPt1:Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 239
    const/4 v2, 0x0

    .line 241
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mAngle:I

    int-to-float v5, v5

    int-to-float v6, v0

    int-to-float v7, v1

    invoke-static {v3, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v2

    .line 242
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mDestPt2:Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 243
    const/4 v2, 0x0

    .line 245
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mAngle:I

    int-to-float v5, v5

    int-to-float v6, v0

    int-to-float v7, v1

    invoke-static {v3, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v2

    .line 246
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mDestPt3:Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 247
    const/4 v2, 0x0

    .line 249
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mAngle:I

    int-to-float v5, v5

    int-to-float v6, v0

    int-to-float v7, v1

    invoke-static {v3, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v2

    .line 250
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mDestPt4:Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 251
    const/4 v2, 0x0

    .line 252
    return-void
.end method

.method public setOrgDestROI(Landroid/graphics/Rect;I)V
    .locals 19
    .param p1, "roi"    # Landroid/graphics/Rect;
    .param p2, "angle"    # I

    .prologue
    .line 256
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mAngle:I

    .line 258
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v14}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalToPreviewMatrix()Landroid/graphics/Matrix;

    move-result-object v7

    .line 261
    .local v7, "m":Landroid/graphics/Matrix;
    const/16 v14, 0x9

    new-array v13, v14, [F

    .line 262
    .local v13, "values":[F
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 263
    .local v2, "applyMatrix":Landroid/graphics/Matrix;
    invoke-virtual {v7, v13}, Landroid/graphics/Matrix;->getValues([F)V

    .line 264
    const/4 v14, 0x2

    aget v14, v13, v14

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-lez v14, :cond_0

    .line 265
    const/4 v6, 0x0

    .line 268
    .local v6, "left":F
    :goto_0
    const/4 v14, 0x5

    aget v14, v13, v14

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-lez v14, :cond_1

    .line 269
    const/4 v12, 0x0

    .line 273
    .local v12, "top":F
    :goto_1
    const/4 v14, 0x0

    aget v9, v13, v14

    .line 274
    .local v9, "scaleX":F
    const/4 v14, 0x4

    aget v10, v13, v14

    .line 275
    .local v10, "scaleY":F
    invoke-virtual {v2, v9, v10}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 276
    invoke-virtual {v2, v6, v12}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 279
    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11}, Landroid/graphics/RectF;-><init>()V

    .line 280
    .local v11, "src":Landroid/graphics/RectF;
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 281
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    .line 282
    .local v5, "dst":Landroid/graphics/RectF;
    invoke-virtual {v2, v5, v11}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 283
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v15, v5, Landroid/graphics/RectF;->left:F

    float-to-int v15, v15

    int-to-float v15, v15

    .line 284
    iget v0, v5, Landroid/graphics/RectF;->top:F

    move/from16 v16, v0

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    .line 285
    iget v0, v5, Landroid/graphics/RectF;->right:F

    move/from16 v17, v0

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    .line 286
    iget v0, v5, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    .line 283
    invoke-virtual/range {v14 .. v18}, Landroid/graphics/RectF;->set(FFFF)V

    .line 287
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v14}, Landroid/graphics/RectF;->centerX()F

    move-result v14

    float-to-int v3, v14

    .line 288
    .local v3, "centerX":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v14}, Landroid/graphics/RectF;->centerY()F

    move-result v14

    float-to-int v4, v14

    .line 290
    .local v4, "centerY":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v14, v14, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mAngle:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    int-to-float v0, v3

    move/from16 v17, v0

    int-to-float v0, v4

    move/from16 v18, v0

    invoke-static/range {v14 .. v18}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v8

    .line 291
    .local v8, "pt":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mDestPt1:Landroid/graphics/PointF;

    iget v15, v8, Landroid/graphics/PointF;->x:F

    iget v0, v8, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Landroid/graphics/PointF;->set(FF)V

    .line 292
    const/4 v8, 0x0

    .line 294
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v14, v14, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mAngle:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    int-to-float v0, v3

    move/from16 v17, v0

    int-to-float v0, v4

    move/from16 v18, v0

    invoke-static/range {v14 .. v18}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v8

    .line 295
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mDestPt2:Landroid/graphics/PointF;

    iget v15, v8, Landroid/graphics/PointF;->x:F

    iget v0, v8, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Landroid/graphics/PointF;->set(FF)V

    .line 296
    const/4 v8, 0x0

    .line 298
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v14, v14, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mAngle:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    int-to-float v0, v3

    move/from16 v17, v0

    int-to-float v0, v4

    move/from16 v18, v0

    invoke-static/range {v14 .. v18}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v8

    .line 299
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mDestPt3:Landroid/graphics/PointF;

    iget v15, v8, Landroid/graphics/PointF;->x:F

    iget v0, v8, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Landroid/graphics/PointF;->set(FF)V

    .line 300
    const/4 v8, 0x0

    .line 302
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v14, v14, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mROI:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mAngle:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    int-to-float v0, v3

    move/from16 v17, v0

    int-to-float v0, v4

    move/from16 v18, v0

    invoke-static/range {v14 .. v18}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v8

    .line 303
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mDestPt4:Landroid/graphics/PointF;

    iget v15, v8, Landroid/graphics/PointF;->x:F

    iget v0, v8, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Landroid/graphics/PointF;->set(FF)V

    .line 304
    const/4 v8, 0x0

    .line 305
    return-void

    .line 267
    .end local v3    # "centerX":I
    .end local v4    # "centerY":I
    .end local v5    # "dst":Landroid/graphics/RectF;
    .end local v6    # "left":F
    .end local v8    # "pt":Landroid/graphics/PointF;
    .end local v9    # "scaleX":F
    .end local v10    # "scaleY":F
    .end local v11    # "src":Landroid/graphics/RectF;
    .end local v12    # "top":F
    :cond_0
    const/4 v14, 0x2

    aget v6, v13, v14

    .restart local v6    # "left":F
    goto/16 :goto_0

    .line 271
    :cond_1
    const/4 v14, 0x5

    aget v12, v13, v14

    .restart local v12    # "top":F
    goto/16 :goto_1
.end method

.method public setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V
    .locals 0
    .param p1, "roiPoint"    # Landroid/graphics/RectF;
    .param p2, "mOrgCenterPt"    # Landroid/graphics/PointF;
    .param p3, "mOrgDestPt1"    # Landroid/graphics/PointF;
    .param p4, "mOrgDestPt2"    # Landroid/graphics/PointF;
    .param p5, "mOrgDestPt3"    # Landroid/graphics/PointF;
    .param p6, "mOrgDestPt4"    # Landroid/graphics/PointF;
    .param p7, "angle"    # I

    .prologue
    .line 308
    invoke-super/range {p0 .. p7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V

    .line 309
    return-void
.end method

.method public setZOrder(I)V
    .locals 0
    .param p1, "idx"    # I

    .prologue
    .line 227
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->mZOrder:I

    .line 228
    return-void
.end method
