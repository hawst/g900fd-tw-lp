.class Lcom/sec/android/mimage/photoretouching/exif/ExifParser;
.super Ljava/lang/Object;
.source "ExifParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ExifTagEvent;,
        Lcom/sec/android/mimage/photoretouching/exif/ExifParser$IfdEvent;,
        Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ImageEvent;
    }
.end annotation


# static fields
.field protected static final BIG_ENDIAN_TAG:S = 0x4d4ds

.field protected static final DEFAULT_IFD0_OFFSET:I = 0x8

.field public static final EVENT_COMPRESSED_IMAGE:I = 0x3

.field public static final EVENT_END:I = 0x5

.field public static final EVENT_ERROR:I = 0x6

.field public static final EVENT_NEW_TAG:I = 0x1

.field public static final EVENT_START_OF_IFD:I = 0x0

.field public static final EVENT_UNCOMPRESSED_STRIP:I = 0x4

.field public static final EVENT_VALUE_OF_REGISTERED_TAG:I = 0x2

.field protected static final EXIF_HEADER:I = 0x45786966

.field protected static final EXIF_HEADER_TAIL:S = 0x0s

.field protected static final LITTLE_ENDIAN_TAG:S = 0x4949s

.field private static final LOGV:Z = false

.field protected static final OFFSET_SIZE:I = 0x2

.field public static final OPTION_IFD_0:I = 0x1

.field public static final OPTION_IFD_1:I = 0x2

.field public static final OPTION_IFD_EXIF:I = 0x4

.field public static final OPTION_IFD_GPS:I = 0x8

.field public static final OPTION_IFD_INTEROPERABILITY:I = 0x10

.field public static final OPTION_THUMBNAIL:I = 0x20

.field private static final TAG:Ljava/lang/String; = "ExifParser"

.field private static final TAG_EXIF_IFD:S

.field private static final TAG_GPS_IFD:S

.field private static final TAG_INTEROPERABILITY_IFD:S

.field private static final TAG_JPEG_INTERCHANGE_FORMAT:S

.field private static final TAG_JPEG_INTERCHANGE_FORMAT_LENGTH:S

.field protected static final TAG_SIZE:I = 0xc

.field private static final TAG_STRIP_BYTE_COUNTS:S

.field private static final TAG_STRIP_OFFSETS:S

.field protected static final TIFF_HEADER_TAIL:S = 0x2as

.field private static final US_ASCII:Ljava/nio/charset/Charset;


# instance fields
.field private mApp1End:I

.field private mContainExifData:Z

.field private final mCorrespondingEvent:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mDataAboveIfd0:[B

.field private mIfd0Position:I

.field private mIfdStartOffset:I

.field private mIfdType:I

.field private mImageEvent:Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ImageEvent;

.field private final mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

.field private mJpegSizeTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

.field private mNeedToParseOffsetsInCurrentIfd:Z

.field private mNumOfTagInIfd:I

.field private mOffsetToApp1EndFromSOF:I

.field private final mOptions:I

.field private mStripCount:I

.field private mStripSizeTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

.field private mTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

.field private mTiffStartPosition:I

.field private final mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->US_ASCII:Ljava/nio/charset/Charset;

    .line 155
    sget v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXIF_IFD:I

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v0

    .line 154
    sput-short v0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->TAG_EXIF_IFD:S

    .line 156
    sget v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_IFD:I

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v0

    sput-short v0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->TAG_GPS_IFD:S

    .line 158
    sget v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_INTEROPERABILITY_IFD:I

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v0

    .line 157
    sput-short v0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->TAG_INTEROPERABILITY_IFD:S

    .line 160
    sget v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT:I

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v0

    .line 159
    sput-short v0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->TAG_JPEG_INTERCHANGE_FORMAT:S

    .line 162
    sget v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT_LENGTH:I

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v0

    .line 161
    sput-short v0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->TAG_JPEG_INTERCHANGE_FORMAT_LENGTH:S

    .line 164
    sget v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_OFFSETS:I

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v0

    .line 163
    sput-short v0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->TAG_STRIP_OFFSETS:S

    .line 166
    sget v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_BYTE_COUNTS:I

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v0

    .line 165
    sput-short v0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->TAG_STRIP_BYTE_COUNTS:S

    .line 166
    return-void
.end method

.method private constructor <init>(Ljava/io/InputStream;ILcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;)V
    .locals 7
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "options"    # I
    .param p3, "iRef"    # Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfdStartOffset:I

    .line 138
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mNumOfTagInIfd:I

    .line 146
    iput-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mContainExifData:Z

    .line 148
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mOffsetToApp1EndFromSOF:I

    .line 168
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    .line 192
    if-nez p1, :cond_0

    .line 193
    new-instance v1, Ljava/io/IOException;

    const-string v4, "Null argument inputStream to ExifParser"

    invoke-direct {v1, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 198
    :cond_0
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    .line 199
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->seekTiffData(Ljava/io/InputStream;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mContainExifData:Z

    .line 200
    new-instance v1, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-direct {v1, p1}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    .line 201
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mOptions:I

    .line 202
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mContainExifData:Z

    if-nez v1, :cond_2

    .line 227
    :cond_1
    :goto_0
    return-void

    .line 206
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->parseTiffHeader()V

    .line 207
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readUnsignedInt()J

    move-result-wide v2

    .line 208
    .local v2, "offset":J
    const-wide/32 v4, 0x7fffffff

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    .line 209
    new-instance v1, Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid offset "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 211
    :cond_3
    long-to-int v1, v2

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfd0Position:I

    .line 212
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfdType:I

    .line 213
    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isIfdRequested(I)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->needToParseOffsetsInCurrentIfd()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 214
    :cond_4
    invoke-direct {p0, v6, v2, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->registerIfd(IJ)V

    .line 215
    const-wide/16 v4, 0x8

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 217
    long-to-int v1, v2

    add-int/lit8 v1, v1, -0x8

    :try_start_0
    new-array v1, v1, [B

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mDataAboveIfd0:[B

    .line 218
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mDataAboveIfd0:[B

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->read([B)I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 219
    :catch_0
    move-exception v0

    .line 220
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 221
    const-string v1, "ExifParser"

    const-string v4, "OutOfMemoryError at ExifParser.."

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method private checkAllowed(II)Z
    .locals 2
    .param p1, "ifd"    # I
    .param p2, "tagId"    # I

    .prologue
    .line 635
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagInfo()Landroid/util/SparseIntArray;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 636
    .local v0, "info":I
    if-nez v0, :cond_0

    .line 637
    const/4 v1, 0x0

    .line 639
    :goto_0
    return v1

    :cond_0
    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->isIfdAllowed(II)Z

    move-result v1

    goto :goto_0
.end method

.method private checkOffsetOrImageTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)V
    .locals 10
    .param p1, "tag"    # Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 584
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v3

    if-nez v3, :cond_1

    .line 632
    :cond_0
    :goto_0
    return-void

    .line 587
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getTagId()S

    move-result v2

    .line 588
    .local v2, "tid":S
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getIfd()I

    move-result v1

    .line 589
    .local v1, "ifd":I
    sget-short v3, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->TAG_EXIF_IFD:S

    if-ne v2, v3, :cond_3

    sget v3, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXIF_IFD:I

    invoke-direct {p0, v1, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->checkAllowed(II)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 590
    invoke-direct {p0, v8}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isIfdRequested(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 591
    invoke-direct {p0, v7}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isIfdRequested(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 592
    :cond_2
    invoke-virtual {p1, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAt(I)J

    move-result-wide v4

    invoke-direct {p0, v8, v4, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->registerIfd(IJ)V

    goto :goto_0

    .line 594
    :cond_3
    sget-short v3, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->TAG_GPS_IFD:S

    if-ne v2, v3, :cond_4

    sget v3, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_IFD:I

    invoke-direct {p0, v1, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->checkAllowed(II)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 595
    invoke-direct {p0, v9}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isIfdRequested(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 596
    invoke-virtual {p1, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAt(I)J

    move-result-wide v4

    invoke-direct {p0, v9, v4, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->registerIfd(IJ)V

    goto :goto_0

    .line 598
    :cond_4
    sget-short v3, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->TAG_INTEROPERABILITY_IFD:S

    if-ne v2, v3, :cond_5

    .line 599
    sget v3, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_INTEROPERABILITY_IFD:I

    invoke-direct {p0, v1, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->checkAllowed(II)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 600
    invoke-direct {p0, v7}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isIfdRequested(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 601
    invoke-virtual {p1, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAt(I)J

    move-result-wide v4

    invoke-direct {p0, v7, v4, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->registerIfd(IJ)V

    goto :goto_0

    .line 603
    :cond_5
    sget-short v3, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->TAG_JPEG_INTERCHANGE_FORMAT:S

    if-ne v2, v3, :cond_6

    .line 604
    sget v3, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT:I

    invoke-direct {p0, v1, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->checkAllowed(II)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 605
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isThumbnailRequested()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 606
    invoke-virtual {p1, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAt(I)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->registerCompressedImage(J)V

    goto :goto_0

    .line 608
    :cond_6
    sget-short v3, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->TAG_JPEG_INTERCHANGE_FORMAT_LENGTH:S

    if-ne v2, v3, :cond_7

    .line 609
    sget v3, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT_LENGTH:I

    invoke-direct {p0, v1, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->checkAllowed(II)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 610
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isThumbnailRequested()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 611
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mJpegSizeTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    goto/16 :goto_0

    .line 613
    :cond_7
    sget-short v3, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->TAG_STRIP_OFFSETS:S

    if-ne v2, v3, :cond_a

    sget v3, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_OFFSETS:I

    invoke-direct {p0, v1, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->checkAllowed(II)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 614
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isThumbnailRequested()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 615
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->hasValue()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 616
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 617
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataType()S

    move-result v3

    if-ne v3, v7, :cond_8

    .line 618
    invoke-virtual {p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAt(I)J

    move-result-wide v4

    invoke-direct {p0, v0, v4, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->registerUncompressedStrip(IJ)V

    .line 616
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 620
    :cond_8
    invoke-virtual {p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAt(I)J

    move-result-wide v4

    invoke-direct {p0, v0, v4, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->registerUncompressedStrip(IJ)V

    goto :goto_2

    .line 624
    .end local v0    # "i":I
    :cond_9
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getOffset()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ExifTagEvent;

    invoke-direct {v5, p1, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ExifTagEvent;-><init>(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;Z)V

    invoke-virtual {v3, v4, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 627
    :cond_a
    sget-short v3, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->TAG_STRIP_BYTE_COUNTS:S

    if-ne v2, v3, :cond_0

    .line 628
    sget v3, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_BYTE_COUNTS:I

    invoke-direct {p0, v1, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->checkAllowed(II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 629
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isThumbnailRequested()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->hasValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 630
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mStripSizeTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    goto/16 :goto_0
.end method

.method private isIfdRequested(I)Z
    .locals 3
    .param p1, "ifdType"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 171
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 183
    :cond_0
    :goto_0
    return v0

    .line 173
    :pswitch_0
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mOptions:I

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 175
    :pswitch_1
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mOptions:I

    and-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 177
    :pswitch_2
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mOptions:I

    and-int/lit8 v2, v2, 0x4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 179
    :pswitch_3
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mOptions:I

    and-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 181
    :pswitch_4
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mOptions:I

    and-int/lit8 v2, v2, 0x10

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private isThumbnailRequested()Z
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mOptions:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private needToParseOffsetsInCurrentIfd()Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 388
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfdType:I

    packed-switch v2, :pswitch_data_0

    .line 399
    :cond_0
    :goto_0
    return v0

    .line 390
    :pswitch_0
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isIfdRequested(I)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x4

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isIfdRequested(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 391
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isIfdRequested(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 392
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isIfdRequested(I)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    move v0, v1

    .line 390
    goto :goto_0

    .line 394
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isThumbnailRequested()Z

    move-result v0

    goto :goto_0

    .line 397
    :pswitch_2
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isIfdRequested(I)Z

    move-result v0

    goto :goto_0

    .line 388
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected static parse(Ljava/io/InputStream;ILcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;)Lcom/sec/android/mimage/photoretouching/exif/ExifParser;
    .locals 1
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "options"    # I
    .param p2, "iRef"    # Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;
        }
    .end annotation

    .prologue
    .line 237
    new-instance v0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;-><init>(Ljava/io/InputStream;ILcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;)V

    return-object v0
.end method

.method protected static parse(Ljava/io/InputStream;Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;)Lcom/sec/android/mimage/photoretouching/exif/ExifParser;
    .locals 2
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "iRef"    # Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;
        }
    .end annotation

    .prologue
    .line 250
    new-instance v0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;

    const/16 v1, 0x3f

    invoke-direct {v0, p0, v1, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;-><init>(Ljava/io/InputStream;ILcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;)V

    return-object v0
.end method

.method private parseTiffHeader()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;
        }
    .end annotation

    .prologue
    .line 735
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readShort()S

    move-result v0

    .line 736
    .local v0, "byteOrder":S
    const/16 v1, 0x4949

    if-ne v1, v0, :cond_0

    .line 737
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->setByteOrder(Ljava/nio/ByteOrder;)V

    .line 744
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readShort()S

    move-result v1

    const/16 v2, 0x2a

    if-eq v1, v2, :cond_2

    .line 745
    new-instance v1, Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;

    const-string v2, "Invalid TIFF header"

    invoke-direct {v1, v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 738
    :cond_0
    const/16 v1, 0x4d4d

    if-ne v1, v0, :cond_1

    .line 739
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    sget-object v2, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->setByteOrder(Ljava/nio/ByteOrder;)V

    goto :goto_0

    .line 741
    :cond_1
    new-instance v1, Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;

    const-string v2, "Invalid TIFF header"

    invoke-direct {v1, v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 747
    :cond_2
    return-void
.end method

.method private readTag()Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;
        }
    .end annotation

    .prologue
    .line 530
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readShort()S

    move-result v1

    .line 531
    .local v1, "tagId":S
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readShort()S

    move-result v2

    .line 532
    .local v2, "dataFormat":S
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readUnsignedInt()J

    move-result-wide v10

    .line 534
    .local v10, "numOfComp":J
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->isValidType(S)Z

    move-result v3

    if-nez v3, :cond_0

    .line 535
    const-string v3, "ExifParser"

    const-string v4, "Tag %04x: Invalid data type %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v14

    aput-object v14, v5, v9

    const/4 v9, 0x1

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v14

    aput-object v14, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    const-wide/16 v4, 0x4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->skip(J)J

    .line 537
    const/4 v0, 0x0

    .line 575
    :goto_0
    return-object v0

    .line 540
    :cond_0
    sget v3, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->MAX_COMPONENT_COUNT:I

    int-to-long v4, v3

    cmp-long v3, v10, v4

    if-lez v3, :cond_1

    .line 541
    const-string v3, "ExifParser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Number of component is larger then MAX_COMPONENT_COUNT : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    const-wide/16 v4, 0x4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->skip(J)J

    .line 543
    const/4 v0, 0x0

    goto :goto_0

    .line 545
    :cond_1
    new-instance v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    long-to-int v3, v10

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfdType:I

    .line 546
    long-to-int v5, v10

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    .line 545
    :goto_1
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;-><init>(SSIIZ)V

    .line 547
    .local v0, "tag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataSize()I

    move-result v7

    .line 548
    .local v7, "dataSize":I
    const/4 v3, 0x4

    if-le v7, v3, :cond_5

    .line 549
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readUnsignedInt()J

    move-result-wide v12

    .line 550
    .local v12, "offset":J
    const-wide/32 v4, 0x7fffffff

    cmp-long v3, v12, v4

    if-lez v3, :cond_3

    .line 551
    new-instance v3, Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;

    .line 552
    const-string v4, "offset is larger then Integer.MAX_VALUE"

    .line 551
    invoke-direct {v3, v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 546
    .end local v0    # "tag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .end local v7    # "dataSize":I
    .end local v12    # "offset":J
    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    .line 556
    .restart local v0    # "tag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .restart local v7    # "dataSize":I
    .restart local v12    # "offset":J
    :cond_3
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfd0Position:I

    int-to-long v4, v3

    cmp-long v3, v12, v4

    if-gez v3, :cond_4

    const/4 v3, 0x7

    if-ne v2, v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mDataAboveIfd0:[B

    if-eqz v3, :cond_4

    .line 557
    long-to-int v3, v10

    new-array v6, v3, [B

    .line 558
    .local v6, "buf":[B
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mDataAboveIfd0:[B

    long-to-int v4, v12

    add-int/lit8 v4, v4, -0x8

    .line 559
    const/4 v5, 0x0

    long-to-int v9, v10

    .line 558
    invoke-static {v3, v4, v6, v5, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 560
    invoke-virtual {v0, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([B)Z

    goto :goto_0

    .line 562
    .end local v6    # "buf":[B
    :cond_4
    long-to-int v3, v12

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setOffset(I)V

    goto :goto_0

    .line 565
    .end local v12    # "offset":J
    :cond_5
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->hasDefinedCount()Z

    move-result v8

    .line 567
    .local v8, "defCount":Z
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setHasDefinedCount(Z)V

    .line 569
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readFullTagValue(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)V

    .line 570
    invoke-virtual {v0, v8}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setHasDefinedCount(Z)V

    .line 571
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    rsub-int/lit8 v4, v7, 0x4

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->skip(J)J

    .line 573
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->getReadByteCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setOffset(I)V

    goto/16 :goto_0
.end method

.method private registerCompressedImage(J)V
    .locals 5
    .param p1, "offset"    # J

    .prologue
    .line 521
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    long-to-int v1, p1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ImageEvent;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ImageEvent;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    return-void
.end method

.method private registerIfd(IJ)V
    .locals 4
    .param p1, "ifdType"    # I
    .param p2, "offset"    # J

    .prologue
    .line 517
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    long-to-int v1, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$IfdEvent;

    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isIfdRequested(I)Z

    move-result v3

    invoke-direct {v2, p1, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$IfdEvent;-><init>(IZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 518
    return-void
.end method

.method private registerUncompressedStrip(IJ)V
    .locals 4
    .param p1, "stripIndex"    # I
    .param p2, "offset"    # J

    .prologue
    .line 525
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    long-to-int v1, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ImageEvent;

    const/4 v3, 0x4

    .line 526
    invoke-direct {v2, v3, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ImageEvent;-><init>(II)V

    .line 525
    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527
    return-void
.end method

.method private seekTiffData(Ljava/io/InputStream;)Z
    .locals 10
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 751
    new-instance v0, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-direct {v0, p1}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 752
    .local v0, "dataStream":Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readShort()S

    move-result v6

    const/16 v7, -0x28

    if-eq v6, v7, :cond_0

    .line 753
    new-instance v5, Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;

    const-string v6, "Invalid JPEG format"

    invoke-direct {v5, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 756
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readShort()S

    move-result v4

    .line 757
    .local v4, "marker":S
    :goto_0
    const/16 v6, -0x27

    if-eq v4, v6, :cond_1

    .line 758
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/exif/JpegHeader;->isSofMarker(S)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 783
    :cond_1
    :goto_1
    return v5

    .line 759
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readUnsignedShort()I

    move-result v3

    .line 762
    .local v3, "length":I
    const/16 v6, -0x1f

    if-ne v4, v6, :cond_3

    .line 763
    const/4 v1, 0x0

    .line 764
    .local v1, "header":I
    const/4 v2, 0x0

    .line 765
    .local v2, "headerTail":S
    const/16 v6, 0x8

    if-lt v3, v6, :cond_3

    .line 766
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readInt()I

    move-result v1

    .line 767
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readShort()S

    move-result v2

    .line 768
    add-int/lit8 v3, v3, -0x6

    .line 769
    const v6, 0x45786966

    if-ne v1, v6, :cond_3

    if-nez v2, :cond_3

    .line 770
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->getReadByteCount()I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStartPosition:I

    .line 771
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mApp1End:I

    .line 772
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStartPosition:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mApp1End:I

    add-int/2addr v5, v6

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mOffsetToApp1EndFromSOF:I

    .line 773
    const/4 v5, 0x1

    goto :goto_1

    .line 777
    .end local v1    # "header":I
    .end local v2    # "headerTail":S
    :cond_3
    const/4 v6, 0x2

    if-lt v3, v6, :cond_4

    add-int/lit8 v6, v3, -0x2

    int-to-long v6, v6

    add-int/lit8 v8, v3, -0x2

    int-to-long v8, v8

    invoke-virtual {v0, v8, v9}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->skip(J)J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-eqz v6, :cond_5

    .line 778
    :cond_4
    const-string v6, "ExifParser"

    const-string v7, "Invalid JPEG format."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 781
    :cond_5
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readShort()S

    move-result v4

    goto :goto_0
.end method

.method private skipTo(I)V
    .locals 4
    .param p1, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 493
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->skipTo(J)V

    .line 494
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v0, p1, :cond_1

    .line 497
    :cond_0
    return-void

    .line 495
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    goto :goto_0
.end method


# virtual methods
.method protected getByteOrder()Ljava/nio/ByteOrder;
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->getByteOrder()Ljava/nio/ByteOrder;

    move-result-object v0

    return-object v0
.end method

.method protected getCompressedImageSize()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 486
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mJpegSizeTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    if-nez v1, :cond_0

    .line 489
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mJpegSizeTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAt(I)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method protected getCurrentIfd()I
    .locals 1

    .prologue
    .line 448
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfdType:I

    return v0
.end method

.method protected getOffsetToExifEndFromSOF()I
    .locals 1

    .prologue
    .line 787
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mOffsetToApp1EndFromSOF:I

    return v0
.end method

.method protected getStripCount()I
    .locals 1

    .prologue
    .line 468
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mStripCount:I

    return v0
.end method

.method protected getStripIndex()I
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mImageEvent:Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ImageEvent;

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ImageEvent;->stripIndex:I

    return v0
.end method

.method protected getStripSize()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 476
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mStripSizeTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    if-nez v1, :cond_0

    .line 478
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mStripSizeTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAt(I)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method protected getTag()Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    return-object v0
.end method

.method protected getTagCountInCurrentIfd()I
    .locals 1

    .prologue
    .line 435
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mNumOfTagInIfd:I

    return v0
.end method

.method protected getTiffStartPosition()I
    .locals 1

    .prologue
    .line 791
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStartPosition:I

    return v0
.end method

.method protected next()I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;
        }
    .end annotation

    .prologue
    .line 268
    iget-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mContainExifData:Z

    if-nez v9, :cond_0

    .line 269
    const/4 v9, 0x5

    .line 349
    :goto_0
    return v9

    .line 271
    :cond_0
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->getReadByteCount()I

    move-result v6

    .line 272
    .local v6, "offset":I
    iget v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfdStartOffset:I

    add-int/lit8 v9, v9, 0x2

    iget v10, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mNumOfTagInIfd:I

    mul-int/lit8 v10, v10, 0xc

    add-int v1, v9, v10

    .line 273
    .local v1, "endOfTags":I
    if-ge v6, v1, :cond_3

    .line 274
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readTag()Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 275
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    if-nez v9, :cond_1

    .line 276
    const/4 v9, 0x6

    goto :goto_0

    .line 278
    :cond_1
    iget-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mNeedToParseOffsetsInCurrentIfd:Z

    if-eqz v9, :cond_2

    .line 279
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    invoke-direct {p0, v9}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->checkOffsetOrImageTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)V

    .line 281
    :cond_2
    const/4 v9, 0x1

    goto :goto_0

    .line 282
    :cond_3
    if-ne v6, v1, :cond_5

    .line 284
    iget v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfdType:I

    if-nez v9, :cond_6

    .line 285
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readUnsignedLong()J

    move-result-wide v4

    .line 286
    .local v4, "ifdOffset":J
    const/4 v9, 0x1

    invoke-direct {p0, v9}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isIfdRequested(I)Z

    move-result v9

    if-nez v9, :cond_4

    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isThumbnailRequested()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 287
    :cond_4
    const-wide/16 v10, 0x0

    cmp-long v9, v4, v10

    if-eqz v9, :cond_5

    .line 288
    const/4 v9, 0x1

    invoke-direct {p0, v9, v4, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->registerIfd(IJ)V

    .line 308
    .end local v4    # "ifdOffset":J
    :cond_5
    :goto_1
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    invoke-virtual {v9}, Ljava/util/TreeMap;->size()I

    move-result v9

    if-nez v9, :cond_9

    .line 349
    const/4 v9, 0x5

    goto :goto_0

    .line 292
    :cond_6
    const/4 v7, 0x4

    .line 294
    .local v7, "offsetSize":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    invoke-virtual {v9}, Ljava/util/TreeMap;->size()I

    move-result v9

    if-lez v9, :cond_7

    .line 295
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    invoke-virtual {v9}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 296
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->getReadByteCount()I

    move-result v10

    .line 295
    sub-int v7, v9, v10

    .line 298
    :cond_7
    const/4 v9, 0x4

    if-ge v7, v9, :cond_8

    .line 299
    const-string v9, "ExifParser"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Invalid size of link to next IFD: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 301
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readUnsignedLong()J

    move-result-wide v4

    .line 302
    .restart local v4    # "ifdOffset":J
    const-wide/16 v10, 0x0

    cmp-long v9, v4, v10

    if-eqz v9, :cond_5

    .line 303
    const-string v9, "ExifParser"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Invalid link to next IFD: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 309
    .end local v4    # "ifdOffset":J
    .end local v7    # "offsetSize":I
    :cond_9
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    invoke-virtual {v9}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    move-result-object v2

    .line 310
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Object;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    .line 312
    .local v3, "event":Ljava/lang/Object;
    :try_start_0
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->skipTo(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 318
    instance-of v9, v3, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$IfdEvent;

    if-eqz v9, :cond_c

    move-object v9, v3

    .line 319
    check-cast v9, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$IfdEvent;

    iget v9, v9, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$IfdEvent;->ifd:I

    iput v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfdType:I

    .line 320
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readUnsignedShort()I

    move-result v9

    iput v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mNumOfTagInIfd:I

    .line 321
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iput v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfdStartOffset:I

    .line 323
    iget v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mNumOfTagInIfd:I

    mul-int/lit8 v9, v9, 0xc

    iget v10, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfdStartOffset:I

    add-int/2addr v9, v10

    add-int/lit8 v9, v9, 0x2

    iget v10, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mApp1End:I

    if-le v9, v10, :cond_a

    .line 324
    const-string v9, "ExifParser"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Invalid size of IFD "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfdType:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    const/4 v9, 0x5

    goto/16 :goto_0

    .line 313
    :catch_0
    move-exception v0

    .line 314
    .local v0, "e":Ljava/io/IOException;
    const-string v9, "ExifParser"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Failed to skip to data at: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 315
    const-string v11, " for "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", the file may be broken."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 314
    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 328
    .end local v0    # "e":Ljava/io/IOException;
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->needToParseOffsetsInCurrentIfd()Z

    move-result v9

    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mNeedToParseOffsetsInCurrentIfd:Z

    .line 329
    check-cast v3, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$IfdEvent;

    .end local v3    # "event":Ljava/lang/Object;
    iget-boolean v9, v3, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$IfdEvent;->isRequested:Z

    if-eqz v9, :cond_b

    .line 330
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 332
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->skipRemainingTagsInCurrentIfd()V

    goto/16 :goto_1

    .line 334
    .restart local v3    # "event":Ljava/lang/Object;
    :cond_c
    instance-of v9, v3, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ImageEvent;

    if-eqz v9, :cond_d

    .line 335
    check-cast v3, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ImageEvent;

    .end local v3    # "event":Ljava/lang/Object;
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mImageEvent:Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ImageEvent;

    .line 336
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mImageEvent:Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ImageEvent;

    iget v9, v9, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ImageEvent;->type:I

    goto/16 :goto_0

    .restart local v3    # "event":Ljava/lang/Object;
    :cond_d
    move-object v8, v3

    .line 338
    check-cast v8, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ExifTagEvent;

    .line 339
    .local v8, "tagEvent":Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ExifTagEvent;
    iget-object v9, v8, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ExifTagEvent;->tag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 340
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataType()S

    move-result v9

    const/4 v10, 0x7

    if-eq v9, v10, :cond_e

    .line 341
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    invoke-virtual {p0, v9}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readFullTagValue(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)V

    .line 342
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    invoke-direct {p0, v9}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->checkOffsetOrImageTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)V

    .line 344
    :cond_e
    iget-boolean v9, v8, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ExifTagEvent;->isRequested:Z

    if-eqz v9, :cond_5

    .line 345
    const/4 v9, 0x2

    goto/16 :goto_0
.end method

.method protected read([B)I
    .locals 1
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 805
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->read([B)I

    move-result v0

    return v0
.end method

.method protected read([BII)I
    .locals 1
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 798
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method protected readFullTagValue(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)V
    .locals 11
    .param p1, "tag"    # Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 644
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataType()S

    move-result v6

    .line 645
    .local v6, "type":S
    const/4 v8, 0x2

    if-eq v6, v8, :cond_0

    const/4 v8, 0x7

    if-eq v6, v8, :cond_0

    .line 646
    const/4 v8, 0x1

    if-ne v6, v8, :cond_1

    .line 647
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v5

    .line 648
    .local v5, "size":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    invoke-virtual {v8}, Ljava/util/TreeMap;->size()I

    move-result v8

    if-lez v8, :cond_1

    .line 649
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    invoke-virtual {v8}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->getReadByteCount()I

    move-result v9

    .line 650
    add-int/2addr v9, v5

    if-ge v8, v9, :cond_1

    .line 651
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    invoke-virtual {v8}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    .line 652
    .local v2, "event":Ljava/lang/Object;
    instance-of v8, v2, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ImageEvent;

    if-eqz v8, :cond_2

    .line 654
    const-string v8, "ExifParser"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Thumbnail overlaps value for tag: \n"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    invoke-virtual {v8}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    move-result-object v1

    .line 656
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Object;>;"
    const-string v8, "ExifParser"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Invalid thumbnail offset: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Object;>;"
    .end local v2    # "event":Ljava/lang/Object;
    .end local v5    # "size":I
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataType()S

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 731
    :goto_1
    :pswitch_0
    return-void

    .line 659
    .restart local v2    # "event":Ljava/lang/Object;
    .restart local v5    # "size":I
    :cond_2
    instance-of v8, v2, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$IfdEvent;

    if-eqz v8, :cond_4

    .line 660
    const-string v8, "ExifParser"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Ifd "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    check-cast v2, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$IfdEvent;

    .end local v2    # "event":Ljava/lang/Object;
    iget v10, v2, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$IfdEvent;->ifd:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 661
    const-string v10, " overlaps value for tag: \n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 660
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    :cond_3
    :goto_2
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    invoke-virtual {v8}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 668
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->getReadByteCount()I

    move-result v9

    .line 667
    sub-int v5, v8, v9

    .line 669
    const-string v8, "ExifParser"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Invalid size of tag: \n"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 670
    const-string v10, " setting count to: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 669
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    invoke-virtual {p1, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->forceSetComponentCount(I)V

    goto :goto_0

    .line 662
    .restart local v2    # "event":Ljava/lang/Object;
    :cond_4
    instance-of v8, v2, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ExifTagEvent;

    if-eqz v8, :cond_3

    .line 663
    const-string v8, "ExifParser"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Tag value for tag: \n"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 664
    check-cast v2, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ExifTagEvent;

    .end local v2    # "event":Ljava/lang/Object;
    iget-object v10, v2, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ExifTagEvent;->tag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 665
    const-string v10, " overlaps value for tag: \n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 663
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 679
    .end local v5    # "size":I
    :pswitch_1
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v8

    new-array v0, v8, [B

    .line 680
    .local v0, "buf":[B
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->read([B)I

    .line 681
    invoke-virtual {p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([B)Z

    goto/16 :goto_1

    .line 685
    .end local v0    # "buf":[B
    :pswitch_2
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v8

    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(Ljava/lang/String;)Z

    goto/16 :goto_1

    .line 688
    :pswitch_3
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v8

    new-array v7, v8, [J

    .line 689
    .local v7, "value":[J
    const/4 v3, 0x0

    .local v3, "i":I
    array-length v4, v7

    .local v4, "n":I
    :goto_3
    if-lt v3, v4, :cond_5

    .line 692
    invoke-virtual {p1, v7}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([J)Z

    goto/16 :goto_1

    .line 690
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readUnsignedLong()J

    move-result-wide v8

    aput-wide v8, v7, v3

    .line 689
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 696
    .end local v3    # "i":I
    .end local v4    # "n":I
    .end local v7    # "value":[J
    :pswitch_4
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v8

    new-array v7, v8, [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    .line 697
    .local v7, "value":[Lcom/sec/android/mimage/photoretouching/exif/Rational;
    const/4 v3, 0x0

    .restart local v3    # "i":I
    array-length v4, v7

    .restart local v4    # "n":I
    :goto_4
    if-lt v3, v4, :cond_6

    .line 700
    invoke-virtual {p1, v7}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([Lcom/sec/android/mimage/photoretouching/exif/Rational;)Z

    goto/16 :goto_1

    .line 698
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readUnsignedRational()Lcom/sec/android/mimage/photoretouching/exif/Rational;

    move-result-object v8

    aput-object v8, v7, v3

    .line 697
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 704
    .end local v3    # "i":I
    .end local v4    # "n":I
    .end local v7    # "value":[Lcom/sec/android/mimage/photoretouching/exif/Rational;
    :pswitch_5
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v8

    new-array v7, v8, [I

    .line 705
    .local v7, "value":[I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    array-length v4, v7

    .restart local v4    # "n":I
    :goto_5
    if-lt v3, v4, :cond_7

    .line 708
    invoke-virtual {p1, v7}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([I)Z

    goto/16 :goto_1

    .line 706
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readUnsignedShort()I

    move-result v8

    aput v8, v7, v3

    .line 705
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 712
    .end local v3    # "i":I
    .end local v4    # "n":I
    .end local v7    # "value":[I
    :pswitch_6
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v8

    new-array v7, v8, [I

    .line 713
    .restart local v7    # "value":[I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    array-length v4, v7

    .restart local v4    # "n":I
    :goto_6
    if-lt v3, v4, :cond_8

    .line 716
    invoke-virtual {p1, v7}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([I)Z

    goto/16 :goto_1

    .line 714
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readLong()I

    move-result v8

    aput v8, v7, v3

    .line 713
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 720
    .end local v3    # "i":I
    .end local v4    # "n":I
    .end local v7    # "value":[I
    :pswitch_7
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v8

    new-array v7, v8, [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    .line 721
    .local v7, "value":[Lcom/sec/android/mimage/photoretouching/exif/Rational;
    const/4 v3, 0x0

    .restart local v3    # "i":I
    array-length v4, v7

    .restart local v4    # "n":I
    :goto_7
    if-lt v3, v4, :cond_9

    .line 724
    invoke-virtual {p1, v7}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([Lcom/sec/android/mimage/photoretouching/exif/Rational;)Z

    goto/16 :goto_1

    .line 722
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readRational()Lcom/sec/android/mimage/photoretouching/exif/Rational;

    move-result-object v8

    aput-object v8, v7, v3

    .line 721
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 676
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected readLong()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 860
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readInt()I

    move-result v0

    return v0
.end method

.method protected readRational()Lcom/sec/android/mimage/photoretouching/exif/Rational;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 867
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readLong()I

    move-result v1

    .line 868
    .local v1, "nomi":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readLong()I

    move-result v0

    .line 869
    .local v0, "denomi":I
    new-instance v2, Lcom/sec/android/mimage/photoretouching/exif/Rational;

    int-to-long v4, v1

    int-to-long v6, v0

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/exif/Rational;-><init>(JJ)V

    return-object v2
.end method

.method protected readString(I)Ljava/lang/String;
    .locals 1
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 814
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readString(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected readString(ILjava/nio/charset/Charset;)Ljava/lang/String;
    .locals 1
    .param p1, "n"    # I
    .param p2, "charset"    # Ljava/nio/charset/Charset;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 823
    if-lez p1, :cond_0

    .line 824
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readString(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    .line 826
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method protected readUnsignedLong()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 843
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readLong()I

    move-result v0

    int-to-long v0, v0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method

.method protected readUnsignedRational()Lcom/sec/android/mimage/photoretouching/exif/Rational;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 851
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readUnsignedLong()J

    move-result-wide v2

    .line 852
    .local v2, "nomi":J
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readUnsignedLong()J

    move-result-wide v0

    .line 853
    .local v0, "denomi":J
    new-instance v4, Lcom/sec/android/mimage/photoretouching/exif/Rational;

    invoke-direct {v4, v2, v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/Rational;-><init>(JJ)V

    return-object v4
.end method

.method protected readUnsignedShort()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 835
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->readShort()S

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    return v0
.end method

.method protected registerForTagValue(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)V
    .locals 4
    .param p1, "tag"    # Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .prologue
    .line 509
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getOffset()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->getReadByteCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 510
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mCorrespondingEvent:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getOffset()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ExifTagEvent;

    const/4 v3, 0x1

    invoke-direct {v2, p1, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser$ExifTagEvent;-><init>(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;Z)V

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 512
    :cond_0
    return-void
.end method

.method protected skipRemainingTagsInCurrentIfd()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 360
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfdStartOffset:I

    add-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mNumOfTagInIfd:I

    mul-int/lit8 v5, v5, 0xc

    add-int v0, v4, v5

    .line 361
    .local v0, "endOfTags":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTiffStream:Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/exif/CountedDataInputStream;->getReadByteCount()I

    move-result v1

    .line 362
    .local v1, "offset":I
    if-le v1, v0, :cond_1

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 365
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mNeedToParseOffsetsInCurrentIfd:Z

    if-eqz v4, :cond_5

    .line 366
    :cond_2
    :goto_1
    if-lt v1, v0, :cond_4

    .line 377
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readUnsignedLong()J

    move-result-wide v2

    .line 379
    .local v2, "ifdOffset":J
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mIfdType:I

    if-nez v4, :cond_0

    .line 380
    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isIfdRequested(I)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->isThumbnailRequested()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 381
    :cond_3
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 382
    invoke-direct {p0, v6, v2, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->registerIfd(IJ)V

    goto :goto_0

    .line 367
    .end local v2    # "ifdOffset":J
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readTag()Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 368
    add-int/lit8 v1, v1, 0xc

    .line 369
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    if-eqz v4, :cond_2

    .line 372
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->mTag:Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    invoke-direct {p0, v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->checkOffsetOrImageTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)V

    goto :goto_1

    .line 375
    :cond_5
    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->skipTo(I)V

    goto :goto_2
.end method
