.class Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$2;
.super Ljava/lang/Object;
.source "ActionBarManager.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initHorizontalScrollView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    .line 281
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 305
    :cond_0
    :pswitch_0
    return v5

    .line 288
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 290
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 292
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 293
    .local v2, "id":I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 290
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 293
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 295
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v4

    if-ne v2, v4, :cond_2

    .line 297
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 299
    :cond_2
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setPressed(Z)V

    goto :goto_1

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
