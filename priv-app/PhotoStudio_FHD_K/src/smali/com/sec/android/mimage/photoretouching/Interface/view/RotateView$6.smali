.class Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$6;
.super Ljava/lang/Object;
.source "RotateView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->initActionbar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    .line 353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 0
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 367
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 372
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v0, :cond_2

    .line 374
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x11000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 384
    :cond_1
    :goto_0
    return-void

    .line 379
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v0, :cond_1

    .line 381
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const/high16 v1, 0x1e110000

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->changeViewStatus(II)V

    goto :goto_0
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 361
    return-void
.end method
