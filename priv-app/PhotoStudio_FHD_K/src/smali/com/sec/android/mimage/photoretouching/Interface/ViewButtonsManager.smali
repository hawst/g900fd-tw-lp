.class public Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
.super Ljava/lang/Object;
.source "ViewButtonsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$ThumbnailAsyncCallback;
    }
.end annotation


# instance fields
.field private final COLLAGE_MAIN_BTN_NUM:I

.field private mAnimate:Z

.field private mBottomFillparentWidthButtonLayout:Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;

.field private mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

.field private mButtonWidth:I

.field private mButtonsEnableState:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentSelectedButton:Landroid/widget/LinearLayout;

.field private mHandler:Landroid/os/Handler;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mIsChange:Z

.field private mIsMiddleButtonVisible:Z

.field private mIsShowing:Z

.field private mMainButton:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mMiddleButtonLayout:Landroid/widget/RelativeLayout;

.field private mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

.field private mViewWidth:I

.field private subAnimate:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->subAnimate:Z

    .line 456
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mHandler:Landroid/os/Handler;

    .line 1691
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsMiddleButtonVisible:Z

    .line 2806
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    .line 2807
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mInflater:Landroid/view/LayoutInflater;

    .line 2808
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mCurrentSelectedButton:Landroid/widget/LinearLayout;

    .line 2809
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    .line 2810
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    .line 2812
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    .line 2813
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mAnimate:Z

    .line 2814
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsShowing:Z

    .line 2815
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mViewWidth:I

    .line 2816
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomFillparentWidthButtonLayout:Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;

    .line 2817
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    .line 2818
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mButtonWidth:I

    .line 2819
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->COLLAGE_MAIN_BTN_NUM:I

    .line 2820
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mButtonsEnableState:Ljava/util/ArrayList;

    .line 2822
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsChange:Z

    .line 67
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    .line 69
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mInflater:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mInflater:Landroid/view/LayoutInflater;

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0900ba

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    .line 72
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBottomLayoutCallback()V

    .line 73
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0900b5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initDataBase(Landroid/content/Context;)V

    .line 75
    return-void
.end method

.method private PenUndoRedoEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V
    .locals 1
    .param p1, "l"    # Landroid/widget/LinearLayout;
    .param p2, "icon"    # Landroid/widget/ImageView;
    .param p3, "text"    # Landroid/widget/TextView;
    .param p4, "flag"    # Z

    .prologue
    .line 2678
    if-eqz p4, :cond_2

    .line 2679
    if-eqz p2, :cond_0

    .line 2680
    const/16 v0, 0xff

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 2681
    invoke-virtual {p2, p4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 2687
    :cond_0
    if-eqz p1, :cond_1

    .line 2689
    invoke-virtual {p1, p4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 2690
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setSoundEffectsEnabled(Z)V

    .line 2707
    :cond_1
    :goto_0
    return-void

    .line 2695
    :cond_2
    if-eqz p2, :cond_3

    .line 2697
    const/16 v0, 0x66

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 2698
    invoke-virtual {p2, p4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 2700
    :cond_3
    if-eqz p1, :cond_1

    .line 2701
    invoke-virtual {p1, p4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 2702
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setSoundEffectsEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Z)V
    .locals 0

    .prologue
    .line 2813
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mAnimate:Z

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 2806
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Z)V
    .locals 0

    .prologue
    .line 1691
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsMiddleButtonVisible:Z

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V
    .locals 0

    .prologue
    .line 2817
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Z)V
    .locals 0

    .prologue
    .line 2814
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsShowing:Z

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Z
    .locals 1

    .prologue
    .line 2813
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mAnimate:Z

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)I
    .locals 1

    .prologue
    .line 2818
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mButtonWidth:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;II)V
    .locals 0

    .prologue
    .line 926
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->adjustBottomButtonMargin(II)V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;
    .locals 1

    .prologue
    .line 2809
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
    .locals 1

    .prologue
    .line 2817
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Z
    .locals 1

    .prologue
    .line 2422
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Z)V
    .locals 0

    .prologue
    .line 197
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->subAnimate:Z

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 2810
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Z
    .locals 1

    .prologue
    .line 1691
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsMiddleButtonVisible:Z

    return v0
.end method

.method private adjustBottomButtonMargin(II)V
    .locals 6
    .param p1, "viewWidth"    # I
    .param p2, "buttonWidth"    # I

    .prologue
    .line 927
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getActivityWidth()I

    move-result p1

    .line 928
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "tb"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 929
    const/16 p1, 0x5fc

    .line 931
    :cond_0
    const/4 v4, 0x0

    .line 932
    .local v4, "sideMargin":I
    const/4 v2, 0x0

    .line 933
    .local v2, "interMargin":I
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getBottomButtonSideMargin()I

    move-result v4

    .line 934
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getBottomButtonInterMargin(II)I

    move-result v2

    .line 937
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    .line 940
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v1, v5, :cond_2

    .line 966
    .end local v1    # "i":I
    :cond_1
    :goto_1
    return-void

    .line 941
    .restart local v1    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 943
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 944
    .local v3, "params":Landroid/widget/LinearLayout$LayoutParams;
    if-eqz v3, :cond_3

    .line 946
    iput v2, v3, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 947
    if-nez v1, :cond_4

    .line 948
    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 940
    .end local v3    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_3
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 949
    .restart local v3    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_4
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v1, v5, :cond_3

    .line 950
    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 957
    .end local v3    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :catch_0
    move-exception v0

    .line 959
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1

    .line 961
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :catch_1
    move-exception v0

    .line 963
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private changeLanguage()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const v9, 0x7f0600ad

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2209
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v4, :cond_0

    .line 2210
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->changeLanguage()V

    .line 2212
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 2330
    return-void

    .line 2213
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2214
    .local v0, "button":Landroid/view/View;
    const/4 v2, 0x0

    .line 2215
    .local v2, "text":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 2326
    :goto_1
    const v4, 0x7f090005

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 2327
    .local v3, "textView":Landroid/widget/TextView;
    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    .line 2328
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2212
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2217
    .end local v3    # "textView":Landroid/widget/TextView;
    :sswitch_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060032

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2218
    goto :goto_1

    .line 2220
    :sswitch_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060033

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2221
    goto :goto_1

    .line 2223
    :sswitch_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060034

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2224
    goto :goto_1

    .line 2227
    :sswitch_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060186

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2228
    goto :goto_1

    .line 2230
    :sswitch_4
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060043

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2231
    goto :goto_1

    .line 2233
    :sswitch_5
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060058

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2234
    goto :goto_1

    .line 2236
    :sswitch_6
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060185

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2237
    goto :goto_1

    .line 2239
    :sswitch_7
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f0600e1

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2240
    goto :goto_1

    .line 2242
    :sswitch_8
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f0600bd

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2243
    goto :goto_1

    .line 2246
    :sswitch_9
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060025

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2247
    goto :goto_1

    .line 2249
    :sswitch_a
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060029

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2250
    goto :goto_1

    .line 2252
    :sswitch_b
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f06002d

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2253
    goto/16 :goto_1

    .line 2255
    :sswitch_c
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060099

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2256
    goto/16 :goto_1

    .line 2258
    :sswitch_d
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f06009a

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2259
    goto/16 :goto_1

    .line 2261
    :sswitch_e
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060017

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2262
    goto/16 :goto_1

    .line 2264
    :sswitch_f
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060018

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2265
    goto/16 :goto_1

    .line 2267
    :sswitch_10
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060091

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2268
    goto/16 :goto_1

    .line 2270
    :sswitch_11
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060103

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2271
    goto/16 :goto_1

    .line 2273
    :sswitch_12
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060104

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2274
    goto/16 :goto_1

    .line 2276
    :sswitch_13
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f06006a

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2277
    goto/16 :goto_1

    .line 2279
    :sswitch_14
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f06016f

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2280
    goto/16 :goto_1

    .line 2282
    :sswitch_15
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f06005a

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2283
    goto/16 :goto_1

    .line 2285
    :sswitch_16
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060069

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2286
    goto/16 :goto_1

    .line 2288
    :sswitch_17
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f0600aa

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2289
    goto/16 :goto_1

    .line 2295
    :sswitch_18
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-static {v4, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    .line 2294
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2296
    goto/16 :goto_1

    .line 2299
    :sswitch_19
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-static {v4, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    .line 2298
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2300
    goto/16 :goto_1

    .line 2303
    :sswitch_1a
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-static {v4, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    .line 2302
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 2215
    :sswitch_data_0
    .sparse-switch
        0x10001001 -> :sswitch_0
        0x10001002 -> :sswitch_1
        0x10001003 -> :sswitch_2
        0x10001004 -> :sswitch_3
        0x10001005 -> :sswitch_4
        0x10001006 -> :sswitch_7
        0x10001007 -> :sswitch_13
        0x10001008 -> :sswitch_15
        0x10001009 -> :sswitch_16
        0x1000100a -> :sswitch_6
        0x1000100b -> :sswitch_5
        0x1000100c -> :sswitch_17
        0x1000100f -> :sswitch_14
        0x11101101 -> :sswitch_c
        0x11101102 -> :sswitch_d
        0x11101103 -> :sswitch_e
        0x11101104 -> :sswitch_f
        0x11201305 -> :sswitch_10
        0x14001409 -> :sswitch_9
        0x1400140b -> :sswitch_8
        0x1400140c -> :sswitch_a
        0x1400140d -> :sswitch_b
        0x1d001701 -> :sswitch_11
        0x1d001702 -> :sswitch_12
        0x1e001841 -> :sswitch_18
        0x1e001842 -> :sswitch_19
        0x1e001843 -> :sswitch_1a
    .end sparse-switch
.end method

.method private getActivityWidth()I
    .locals 3

    .prologue
    .line 2786
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2787
    .local v1, "widthPixels":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2788
    .local v0, "heightPixels":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    :goto_0
    return v2

    :cond_0
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_0
.end method

.method private getBottomButtonInterMargin(II)I
    .locals 4
    .param p1, "viewWidth"    # I
    .param p2, "buttonWidth"    # I

    .prologue
    .line 912
    const/4 v1, 0x0

    .line 913
    .local v1, "sideMargin":I
    const/4 v0, 0x0

    .line 915
    .local v0, "interMargin":I
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getBottomButtonSideMargin()I

    move-result v1

    .line 917
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 918
    mul-int/lit8 v2, v1, 0x2

    sub-int v2, p1, v2

    sub-int/2addr v2, p2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    div-int v0, v2, v3

    .line 920
    :cond_0
    if-gez v0, :cond_1

    .line 921
    const/4 v0, 0x0

    .line 923
    :cond_1
    return v0
.end method

.method private getBottomButtonSideMargin()I
    .locals 4

    .prologue
    .line 873
    const/4 v1, 0x0

    .line 874
    .local v1, "sideMargin":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    .line 875
    .local v0, "isLandscape":Z
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 877
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 899
    if-eqz v0, :cond_4

    .line 900
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050241

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 907
    :cond_0
    :goto_0
    return v1

    .line 879
    :pswitch_0
    if-eqz v0, :cond_1

    .line 880
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05023e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 882
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05023a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 883
    goto :goto_0

    .line 885
    :pswitch_1
    if-eqz v0, :cond_2

    .line 886
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05023f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 888
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05023b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 889
    goto :goto_0

    .line 891
    :pswitch_2
    if-eqz v0, :cond_3

    .line 892
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050240

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 894
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05023c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 895
    goto :goto_0

    .line 902
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05023d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 877
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private initDataBase(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 79
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->isCheckDB(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->copyDBFromAssets(Landroid/content/Context;Z)V

    .line 81
    :cond_0
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->isCheckDB(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->copyDBFromAssets(Landroid/content/Context;Z)V

    .line 83
    :cond_1
    return-void
.end method

.method private isMiddleBtnMode()Z
    .locals 2

    .prologue
    .line 2453
    const/4 v0, 0x0

    .line 2454
    .local v0, "ret":Z
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2461
    const/4 v0, 0x0

    .line 2467
    :goto_0
    return v0

    .line 2458
    :sswitch_0
    const/4 v0, 0x1

    .line 2459
    goto :goto_0

    .line 2454
    :sswitch_data_0
    .sparse-switch
        0x15000000 -> :sswitch_0
        0x16000000 -> :sswitch_0
    .end sparse-switch
.end method

.method private isSubViewAnimate()Z
    .locals 2

    .prologue
    .line 832
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x16000000

    if-eq v0, v1, :cond_1

    .line 833
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-nez v0, :cond_0

    .line 834
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x15000000

    if-eq v0, v1, :cond_1

    .line 835
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x31000000

    if-eq v0, v1, :cond_1

    .line 836
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    const v1, 0x1e300001

    if-eq v0, v1, :cond_1

    .line 837
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    const v1, 0x1e300003

    if-eq v0, v1, :cond_1

    .line 832
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSubViewMode()Z
    .locals 2

    .prologue
    .line 2424
    const/4 v0, 0x0

    .line 2425
    .local v0, "ret":Z
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2438
    :goto_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2448
    :goto_1
    :pswitch_0
    return v0

    .line 2434
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 2444
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_1

    .line 2425
    :sswitch_data_0
    .sparse-switch
        0x11000000 -> :sswitch_0
        0x15000000 -> :sswitch_0
        0x16000000 -> :sswitch_0
        0x18000000 -> :sswitch_0
        0x31000000 -> :sswitch_0
    .end sparse-switch

    .line 2438
    :pswitch_data_0
    .packed-switch 0x1e300000
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private makeThumbnail([IIIIIIIF)[I
    .locals 13
    .param p1, "bitmap"    # [I
    .param p2, "imageWidth"    # I
    .param p3, "imageHeight"    # I
    .param p4, "button_width"    # I
    .param p5, "button_height"    # I
    .param p6, "resizeWidth"    # I
    .param p7, "resizeHeight"    # I
    .param p8, "scale"    # F

    .prologue
    .line 1672
    const/4 v1, 0x0

    .line 1674
    .local v1, "thumbnail":[I
    move v12, p2

    .line 1675
    .local v12, "bitmap_w":I
    move/from16 v11, p3

    .line 1676
    .local v11, "bitmap_h":I
    mul-int v2, p6, p7

    new-array v1, v2, [I

    .line 1677
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "thumbnailINfo :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1678
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "thumbnailINfo resize:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p7

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1680
    const/high16 v2, 0x3f800000    # 1.0f

    div-float v7, v2, p8

    const/4 v8, 0x0

    .line 1681
    const/4 v9, 0x0

    new-instance v10, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    add-int/lit8 v4, p6, -0x1

    .line 1682
    add-int/lit8 v5, p7, -0x1

    invoke-direct {v10, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    move/from16 v2, p6

    move/from16 v3, p7

    move-object v4, p1

    move v5, p2

    move/from16 v6, p3

    .line 1679
    invoke-static/range {v1 .. v10}, Lcom/sec/android/mimage/photoretouching/jni/Util;->native_drawImage([III[IIIFIILandroid/graphics/Rect;)Z

    .line 1683
    return-object v1
.end method

.method private setBottomLayoutCallback()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->setBottomLayoutCallback(Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout$BottomLayoutCallback;)V

    .line 128
    return-void
.end method


# virtual methods
.method public ableRedo()V
    .locals 4

    .prologue
    .line 2655
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const v2, 0x1a001845

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .line 2656
    .local v0, "undo":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    if-eqz v0, :cond_0

    .line 2659
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2660
    const/4 v2, 0x0

    .line 2661
    const/4 v3, 0x1

    .line 2658
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->PenUndoRedoEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 2663
    :cond_0
    return-void
.end method

.method public ableUndo()V
    .locals 4

    .prologue
    .line 2634
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const v2, 0x1a001844

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .line 2635
    .local v0, "undo":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    if-eqz v0, :cond_0

    .line 2637
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2638
    const/4 v2, 0x0

    .line 2639
    const/4 v3, 0x1

    .line 2636
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->PenUndoRedoEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 2641
    :cond_0
    return-void
.end method

.method public animAndFreeSubViewButtons()V
    .locals 13

    .prologue
    const/4 v4, 0x0

    const v2, 0x3f666666    # 0.9f

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    .line 201
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v3

    if-eqz v3, :cond_1

    move v11, v4

    .line 202
    .local v11, "hideAnim":Z
    :goto_0
    if-eqz v11, :cond_2

    .line 203
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    const v6, 0x7f0900b7

    invoke-virtual {v3, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 204
    .local v12, "subMenuLayout":Landroid/view/View;
    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 205
    .local v10, "animation":Landroid/view/animation/AnimationSet;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getAnimX(Z)F

    move-result v6

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v1

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 206
    .local v0, "scaleAnim":Landroid/view/animation/ScaleAnimation;
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const v2, 0x3f19999a    # 0.6f

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 207
    .local v9, "alphaAnim":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 208
    invoke-virtual {v10, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 209
    new-instance v1, Landroid/view/animation/interpolator/SineEaseInOut;

    invoke-direct {v1}, Landroid/view/animation/interpolator/SineEaseInOut;-><init>()V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 210
    const-wide/16 v2, 0x55

    invoke-virtual {v10, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 212
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 257
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 258
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 259
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->subAnimate:Z

    .line 260
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$4;

    invoke-direct {v2, p0, v12, v10}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Landroid/view/View;Landroid/view/animation/AnimationSet;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 274
    .end local v0    # "scaleAnim":Landroid/view/animation/ScaleAnimation;
    .end local v9    # "alphaAnim":Landroid/view/animation/AlphaAnimation;
    .end local v10    # "animation":Landroid/view/animation/AnimationSet;
    .end local v12    # "subMenuLayout":Landroid/view/View;
    :cond_0
    :goto_1
    return-void

    .end local v11    # "hideAnim":Z
    :cond_1
    move v11, v5

    .line 201
    goto :goto_0

    .line 270
    .restart local v11    # "hideAnim":Z
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->hideSubBottomButton()Z

    .line 271
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v1, :cond_0

    .line 272
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->destroy()V

    goto :goto_1
.end method

.method public changeLayoutSize(I)V
    .locals 4
    .param p1, "width"    # I

    .prologue
    .line 2595
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sj, VBM - changeLayoutSize() - width : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2596
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mViewWidth:I

    .line 2599
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 2602
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2603
    .local v1, "layout":Landroid/widget/LinearLayout;
    if-eqz v1, :cond_0

    .line 2625
    .end local v1    # "layout":Landroid/widget/LinearLayout;
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2627
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v2, :cond_1

    .line 2628
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v2, p1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->changeLayoutSize(I)V

    .line 2630
    :cond_1
    return-void

    .line 2620
    :catch_0
    move-exception v0

    .line 2621
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ViewButtonsManager - changeLayoutSize() : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public changeSelectionBtnIcon(II)V
    .locals 4
    .param p1, "buttonId"    # I
    .param p2, "iconId"    # I

    .prologue
    .line 750
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 751
    const/4 v2, 0x0

    .line 752
    .local v2, "icon":Landroid/widget/ImageView;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 770
    .end local v1    # "i":I
    .end local v2    # "icon":Landroid/widget/ImageView;
    :cond_0
    :goto_1
    return-void

    .line 753
    .restart local v1    # "i":I
    .restart local v2    # "icon":Landroid/widget/ImageView;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 754
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    if-ne v3, p1, :cond_2

    .line 755
    instance-of v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v3, :cond_3

    .line 757
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .end local v0    # "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0, p2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setIconResource(I)V

    .line 752
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 761
    .restart local v0    # "button":Landroid/widget/LinearLayout;
    :cond_3
    const v3, 0x7f090004

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "icon":Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 762
    .restart local v2    # "icon":Landroid/widget/ImageView;
    if-eqz v2, :cond_0

    .line 763
    invoke-virtual {v2, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method public destroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 286
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v1, :cond_0

    .line 287
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->destroy()V

    .line 288
    :cond_0
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    .line 290
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    if-eqz v1, :cond_1

    .line 291
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->removeAllViews()V

    .line 292
    :cond_1
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    .line 293
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_4

    .line 307
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    .line 308
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 309
    :cond_3
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    .line 310
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mInflater:Landroid/view/LayoutInflater;

    .line 311
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mCurrentSelectedButton:Landroid/widget/LinearLayout;

    .line 312
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    .line 313
    return-void

    .line 293
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 295
    .local v0, "vGroup":Landroid/view/ViewGroup;
    instance-of v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    if-eqz v2, :cond_5

    .line 296
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    .end local v0    # "vGroup":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->destroy()V

    goto :goto_0

    .line 297
    .restart local v0    # "vGroup":Landroid/view/ViewGroup;
    :cond_5
    instance-of v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    if-eqz v2, :cond_6

    .line 298
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .end local v0    # "vGroup":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->destroy()V

    goto :goto_0

    .line 299
    .restart local v0    # "vGroup":Landroid/view/ViewGroup;
    :cond_6
    instance-of v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v2, :cond_7

    .line 300
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .end local v0    # "vGroup":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->destroy()V

    goto :goto_0

    .line 301
    .restart local v0    # "vGroup":Landroid/view/ViewGroup;
    :cond_7
    instance-of v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;

    if-eqz v2, :cond_8

    .line 302
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;

    .end local v0    # "vGroup":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->destroy()V

    goto :goto_0

    .line 303
    .restart local v0    # "vGroup":Landroid/view/ViewGroup;
    :cond_8
    instance-of v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;

    if-eqz v2, :cond_2

    .line 304
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;

    .end local v0    # "vGroup":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->destroy()V

    goto :goto_0
.end method

.method public free()V
    .locals 6

    .prologue
    .line 466
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->animAndFreeSubViewButtons()V

    .line 468
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    .line 469
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_1

    .line 492
    .end local v2    # "i":I
    :cond_0
    return-void

    .line 470
    .restart local v2    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 471
    .local v1, "button":Landroid/widget/LinearLayout;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    .line 472
    .local v3, "id":I
    const v5, 0xff00

    and-int v4, v3, v5

    .line 474
    .local v4, "type":I
    const/high16 v5, 0x15000000

    if-ne v4, v5, :cond_2

    move-object v0, v1

    .line 475
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .line 476
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->destroy()V

    .line 479
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    :cond_2
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 469
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :sswitch_0
    move-object v0, v1

    .line 485
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .line 486
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->destroy()V

    goto :goto_1

    .line 479
    nop

    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_0
        0x31200000 -> :sswitch_0
        0x31300000 -> :sswitch_0
        0x31500000 -> :sswitch_0
    .end sparse-switch
.end method

.method public getAnimX(Z)F
    .locals 5
    .param p1, "isShowAnim"    # Z

    .prologue
    .line 133
    const/4 v2, 0x0

    .line 134
    .local v2, "position":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v3

    const/high16 v4, 0x1e110000

    if-eq v3, v4, :cond_2

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v3

    const/high16 v4, 0x2c000000

    if-eq v3, v4, :cond_2

    const/4 v1, 0x0

    .line 135
    .local v1, "isCollage":Z
    :goto_0
    if-nez v1, :cond_0

    if-eqz p1, :cond_3

    :cond_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v3

    :goto_1
    sparse-switch v3, :sswitch_data_0

    .line 185
    :goto_2
    const/4 v0, 0x0

    .line 186
    .local v0, "animX":F
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_1

    .line 188
    int-to-float v3, v2

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 189
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v4, v4

    .line 188
    div-float v0, v3, v4

    .line 193
    :cond_1
    return v0

    .line 134
    .end local v0    # "animX":F
    .end local v1    # "isCollage":Z
    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    .line 135
    .restart local v1    # "isCollage":Z
    :cond_3
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getPreviousMode()I

    move-result v3

    goto :goto_1

    .line 138
    :sswitch_0
    const v3, 0x10001002

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v2

    .line 139
    goto :goto_2

    .line 141
    :sswitch_1
    const v3, 0x10001003

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v2

    .line 142
    goto :goto_2

    .line 144
    :sswitch_2
    const-string v3, "moveArrowToCurrentViewBtn"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 145
    const v3, 0x10001004

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v2

    .line 146
    goto :goto_2

    .line 148
    :sswitch_3
    const v3, 0x10001005

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v2

    .line 149
    goto :goto_2

    .line 151
    :sswitch_4
    const v3, 0x10001006

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v2

    .line 152
    goto :goto_2

    .line 154
    :sswitch_5
    const v3, 0x10001008

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v2

    .line 155
    goto :goto_2

    .line 157
    :sswitch_6
    const v3, 0x10001007

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v2

    .line 158
    goto :goto_2

    .line 160
    :sswitch_7
    const v3, 0x1000100a

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v2

    .line 161
    goto :goto_2

    .line 163
    :sswitch_8
    const v3, 0x1000100b

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v2

    .line 164
    goto :goto_2

    .line 168
    :sswitch_9
    if-eqz p1, :cond_4

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v3

    :goto_3
    packed-switch v3, :pswitch_data_0

    goto :goto_2

    .line 171
    :pswitch_0
    const/high16 v3, 0x1e300000

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getBtnCenterPositionWithBackBtn_Collage(I)I

    move-result v2

    .line 172
    goto :goto_2

    .line 168
    :cond_4
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getPreviousSubMode()I

    move-result v3

    goto :goto_3

    .line 174
    :pswitch_1
    const v3, 0x1e300001

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getBtnCenterPositionWithBackBtn_Collage(I)I

    move-result v2

    .line 175
    goto/16 :goto_2

    .line 177
    :pswitch_2
    const v3, 0x1e300002

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getBtnCenterPositionWithBackBtn_Collage(I)I

    move-result v2

    .line 178
    goto/16 :goto_2

    .line 180
    :pswitch_3
    const v3, 0x1e300003

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getBtnCenterPositionWithBackBtn_Collage(I)I

    move-result v2

    goto/16 :goto_2

    .line 135
    :sswitch_data_0
    .sparse-switch
        0x11000000 -> :sswitch_7
        0x11100000 -> :sswitch_0
        0x11200000 -> :sswitch_1
        0x15000000 -> :sswitch_2
        0x16000000 -> :sswitch_3
        0x18000000 -> :sswitch_4
        0x1e110000 -> :sswitch_9
        0x2c000000 -> :sswitch_9
        0x31000000 -> :sswitch_8
        0x31100000 -> :sswitch_5
        0x31200000 -> :sswitch_6
    .end sparse-switch

    .line 168
    :pswitch_data_0
    .packed-switch 0x1e300000
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getBottomButtonLayoutRect()Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 773
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 774
    .local v0, "bottomBtnRect":Landroid/graphics/Rect;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 802
    :goto_0
    return-object v0

    .line 774
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 775
    .local v1, "btn":Landroid/widget/LinearLayout;
    instance-of v3, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v3, :cond_0

    .line 776
    const/4 v2, -0x1

    .line 777
    .local v2, "btnMode":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :goto_1
    move-object v3, v1

    .line 795
    check-cast v3, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getButtonType()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 796
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    goto :goto_0

    .line 779
    :sswitch_0
    const v2, 0x1000100a

    .line 780
    goto :goto_1

    .line 782
    :sswitch_1
    const v2, 0x10001004

    .line 783
    goto :goto_1

    .line 785
    :sswitch_2
    const v2, 0x10001005

    .line 786
    goto :goto_1

    .line 788
    :sswitch_3
    const v2, 0x10001006

    .line 789
    goto :goto_1

    .line 791
    :sswitch_4
    const v2, 0x1000100b

    goto :goto_1

    .line 777
    nop

    :sswitch_data_0
    .sparse-switch
        0x11000000 -> :sswitch_0
        0x15000000 -> :sswitch_1
        0x16000000 -> :sswitch_2
        0x18000000 -> :sswitch_3
        0x31000000 -> :sswitch_4
    .end sparse-switch
.end method

.method public getBtnCenterPositionWithBackBtn_Collage(I)I
    .locals 7
    .param p1, "buttonId"    # I

    .prologue
    .line 641
    const/4 v3, -0x1

    .line 642
    .local v3, "ret":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    .line 645
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v1, v5, :cond_1

    .line 660
    .end local v1    # "i":I
    :cond_0
    :goto_1
    return v3

    .line 647
    .restart local v1    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 649
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v5

    if-ne v5, p1, :cond_2

    .line 651
    const v5, 0x7f090004

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 652
    .local v4, "view":Landroid/view/View;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 654
    .local v2, "r":Landroid/graphics/Rect;
    invoke-virtual {v4, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 655
    iget v5, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int v3, v5, v6

    .line 656
    goto :goto_1

    .line 645
    .end local v2    # "r":Landroid/graphics/Rect;
    .end local v4    # "view":Landroid/view/View;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getButtonCenterPosition(I)I
    .locals 9
    .param p1, "buttonId"    # I

    .prologue
    const/4 v8, 0x0

    .line 605
    const/4 v4, -0x1

    .line 606
    .local v4, "ret":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    .line 607
    const/4 v6, 0x0

    .line 608
    .local v6, "width":I
    const/4 v0, 0x0

    .line 609
    .local v0, "btnCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v2, v7, :cond_1

    .line 635
    .end local v0    # "btnCount":I
    .end local v2    # "i":I
    .end local v6    # "width":I
    :cond_0
    :goto_1
    return v4

    .line 610
    .restart local v0    # "btnCount":I
    .restart local v2    # "i":I
    .restart local v6    # "width":I
    :cond_1
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 612
    .local v1, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->invalidate()V

    .line 613
    invoke-virtual {v1, v8, v8}, Landroid/widget/LinearLayout;->measure(II)V

    .line 615
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v7

    if-ne v7, p1, :cond_2

    .line 617
    const v7, 0x7f090004

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 618
    .local v5, "view":Landroid/view/View;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 619
    .local v3, "r":Landroid/graphics/Rect;
    if-eqz v5, :cond_0

    .line 620
    invoke-virtual {v5, v3}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 621
    iget v7, v3, Landroid/graphics/Rect;->left:I

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int v4, v7, v8

    .line 626
    goto :goto_1

    .line 630
    .end local v3    # "r":Landroid/graphics/Rect;
    .end local v5    # "view":Landroid/view/View;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 631
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v6, v7

    .line 609
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getButtonHeight()I
    .locals 4

    .prologue
    .line 1661
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->measure(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1665
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->getMeasuredHeight()I

    move-result v1

    return v1

    .line 1662
    :catch_0
    move-exception v0

    .line 1663
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getButtonInstance(I)Landroid/widget/LinearLayout;
    .locals 4
    .param p1, "idx"    # I

    .prologue
    .line 1637
    const/4 v2, 0x0

    .line 1638
    .local v2, "ret":Landroid/widget/LinearLayout;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 1639
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 1647
    .end local v1    # "i":I
    :cond_0
    :goto_1
    return-object v2

    .line 1640
    .restart local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1641
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    if-ne v3, p1, :cond_2

    .line 1642
    move-object v2, v0

    .line 1643
    goto :goto_1

    .line 1639
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getButtonLeft(I)I
    .locals 8
    .param p1, "buttonId"    # I

    .prologue
    const/4 v7, 0x0

    .line 576
    const/4 v3, -0x1

    .line 577
    .local v3, "ret":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    .line 578
    const/4 v4, 0x0

    .line 579
    .local v4, "width":I
    const/4 v0, 0x0

    .line 581
    .local v0, "btnCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_1

    .line 600
    .end local v0    # "btnCount":I
    .end local v2    # "i":I
    .end local v4    # "width":I
    :cond_0
    :goto_1
    return v3

    .line 582
    .restart local v0    # "btnCount":I
    .restart local v2    # "i":I
    .restart local v4    # "width":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 583
    .local v1, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 584
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->invalidate()V

    .line 585
    invoke-virtual {v1, v7, v7}, Landroid/widget/LinearLayout;->measure(II)V

    .line 586
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "kbr : buttongetId = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "buttonId "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 587
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v5

    if-ne v5, p1, :cond_2

    .line 589
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getBottomButtonSideMargin()I

    move-result v5

    add-int/2addr v4, v5

    .line 590
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mViewWidth:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mButtonWidth:I

    invoke-direct {p0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getBottomButtonInterMargin(II)I

    move-result v5

    mul-int/2addr v5, v0

    add-int v3, v4, v5

    .line 591
    goto :goto_1

    .line 595
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 596
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v4, v5

    .line 581
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getConfigChange()Z
    .locals 1

    .prologue
    .line 2737
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsChange:Z

    return v0
.end method

.method public getCurrentModeButtonId()I
    .locals 2

    .prologue
    .line 2472
    const/4 v0, -0x1

    .line 2473
    .local v0, "ret":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2508
    :goto_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2522
    :goto_1
    :pswitch_0
    return v0

    .line 2476
    :sswitch_0
    const v0, 0x10001002

    .line 2477
    goto :goto_0

    .line 2479
    :sswitch_1
    const v0, 0x10001003

    .line 2480
    goto :goto_0

    .line 2482
    :sswitch_2
    const v0, 0x10001004

    .line 2483
    goto :goto_0

    .line 2485
    :sswitch_3
    const v0, 0x10001005

    .line 2486
    goto :goto_0

    .line 2488
    :sswitch_4
    const v0, 0x10001006

    .line 2489
    goto :goto_0

    .line 2491
    :sswitch_5
    const v0, 0x10001008

    .line 2492
    goto :goto_0

    .line 2494
    :sswitch_6
    const v0, 0x10001007

    .line 2495
    goto :goto_0

    .line 2498
    :sswitch_7
    const v0, 0x1000100f

    .line 2499
    goto :goto_0

    .line 2501
    :sswitch_8
    const v0, 0x1000100a

    .line 2502
    goto :goto_0

    .line 2504
    :sswitch_9
    const v0, 0x1000100b

    goto :goto_0

    .line 2510
    :pswitch_1
    const/high16 v0, 0x1e300000

    .line 2511
    goto :goto_1

    .line 2514
    :pswitch_2
    const v0, 0x1e300001

    .line 2515
    goto :goto_1

    .line 2518
    :pswitch_3
    const v0, 0x1e300003

    goto :goto_1

    .line 2473
    nop

    :sswitch_data_0
    .sparse-switch
        0x11000000 -> :sswitch_8
        0x11100000 -> :sswitch_0
        0x11200000 -> :sswitch_1
        0x15000000 -> :sswitch_2
        0x16000000 -> :sswitch_3
        0x18000000 -> :sswitch_4
        0x31000000 -> :sswitch_9
        0x31100000 -> :sswitch_5
        0x31200000 -> :sswitch_6
        0x31600000 -> :sswitch_7
    .end sparse-switch

    .line 2508
    :pswitch_data_0
    .packed-switch 0x1e300000
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public getCurrentSelectedButton()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 2165
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v0, :cond_0

    .line 2166
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->getCurrentSelectedButton()Landroid/widget/LinearLayout;

    move-result-object v0

    .line 2167
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mCurrentSelectedButton:Landroid/widget/LinearLayout;

    goto :goto_0
.end method

.method public getID(I)Landroid/widget/LinearLayout;
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 1743
    const/4 v0, 0x0

    .line 1744
    .local v0, "ret":Landroid/widget/LinearLayout;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->getButtonInstance(I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1745
    return-object v0
.end method

.method public getMainBtnList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2159
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v0, :cond_0

    .line 2160
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v0

    .line 2161
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public getPenColorLine()Landroid/widget/FrameLayout;
    .locals 3

    .prologue
    .line 1651
    const/4 v0, 0x0

    .line 1652
    .local v0, "colorLine":Landroid/widget/FrameLayout;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 1653
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1654
    const v2, 0x7f090075

    .line 1653
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "colorLine":Landroid/widget/FrameLayout;
    check-cast v0, Landroid/widget/FrameLayout;

    .line 1656
    .restart local v0    # "colorLine":Landroid/widget/FrameLayout;
    :cond_0
    return-object v0
.end method

.method public getSelectedButton()Landroid/widget/LinearLayout;
    .locals 3

    .prologue
    .line 496
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 498
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 507
    .end local v1    # "i":I
    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0

    .line 500
    .restart local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 501
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isSelected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 498
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getSellectedButtonsPos()[I
    .locals 5

    .prologue
    .line 512
    const/4 v2, 0x2

    new-array v1, v2, [I

    .line 513
    .local v1, "buttonPos":[I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getSelectedButton()Landroid/widget/LinearLayout;

    move-result-object v0

    .line 514
    .local v0, "button":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 515
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getLocationOnScreen([I)V

    .line 517
    :cond_0
    const/4 v2, 0x0

    aget v3, v1, v2

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    aput v3, v1, v2

    .line 518
    return-object v1
.end method

.method public hasFocus()Z
    .locals 1

    .prologue
    .line 2792
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->hasFocus()Z

    move-result v0

    return v0
.end method

.method public hide(Z)V
    .locals 9
    .param p1, "isConfig"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 316
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    if-eqz v3, :cond_1

    .line 318
    if-eqz p1, :cond_0

    .line 319
    const/16 v3, 0x8

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBottomLayoutVisible(I)V

    .line 321
    :cond_0
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .line 323
    const/high16 v8, 0x40800000    # 4.0f

    move v3, v1

    move v4, v2

    move v5, v1

    move v6, v2

    move v7, v1

    .line 321
    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 325
    .local v0, "animation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 327
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$5;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 349
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$6;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Landroid/view/animation/Animation;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 360
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_1
    return-void
.end method

.method public hideSubBottomButton()Z
    .locals 5

    .prologue
    .line 855
    const/4 v0, 0x0

    .line 856
    .local v0, "isHided":Z
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 858
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    const v4, 0x7f0900b7

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 859
    .local v2, "submenuLayout":Landroid/widget/FrameLayout;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    const v4, 0x7f0900b8

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 860
    .local v1, "subButtonLayout":Landroid/widget/LinearLayout;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 862
    const/4 v0, 0x1

    .line 863
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 868
    .end local v1    # "subButtonLayout":Landroid/widget/LinearLayout;
    .end local v2    # "submenuLayout":Landroid/widget/FrameLayout;
    :cond_0
    return v0
.end method

.method public initBottomButtonForMultiGridSplit(I)V
    .locals 0
    .param p1, "viewWidth"    # I

    .prologue
    .line 1471
    return-void
.end method

.method public initBottomButtonForPen(I)V
    .locals 13
    .param p1, "viewWidth"    # I

    .prologue
    const/16 v12, 0x11

    const/4 v4, 0x1

    const/4 v11, 0x0

    .line 1533
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1534
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1535
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->removeAllViews()V

    .line 1537
    new-instance v8, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-direct {v8, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1538
    .local v8, "layout":Landroid/widget/LinearLayout;
    invoke-virtual {v8, v12}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1539
    invoke-virtual {v8, v11}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1541
    const/4 v6, 0x0

    .line 1542
    .local v6, "buttonWidth":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1581
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    invoke-virtual {v1, v8}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->addView(Landroid/view/View;)V

    .line 1582
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x17000000

    if-ne v1, v2, :cond_2

    .line 1585
    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout$LayoutParams;

    .line 1586
    .local v9, "llpp":Landroid/widget/LinearLayout$LayoutParams;
    iput v12, v9, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1587
    invoke-virtual {v8, v12}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1588
    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1615
    .end local v9    # "llpp":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mButtonWidth:I

    .line 1617
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1634
    .end local v6    # "buttonWidth":I
    .end local v8    # "layout":Landroid/widget/LinearLayout;
    :cond_1
    :goto_1
    return-void

    .line 1544
    .restart local v6    # "buttonWidth":I
    .restart local v8    # "layout":Landroid/widget/LinearLayout;
    :pswitch_0
    const v1, 0x7f020372

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 1545
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    const/4 v1, 0x4

    if-ge v7, v1, :cond_0

    .line 1546
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v2, 0x7f030030

    invoke-direct {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 1547
    .local v0, "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    packed-switch v7, :pswitch_data_1

    .line 1570
    :goto_3
    invoke-virtual {v0, v11, v11}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1573
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1574
    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1575
    invoke-virtual {v0, v11, v11}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1576
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v6, v1

    .line 1545
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 1549
    :pswitch_1
    const v1, 0x1a001842

    .line 1550
    const v2, 0x7f0201b2

    .line 1551
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f060069

    .line 1550
    invoke-static {v3, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    move v5, v4

    .line 1549
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_3

    .line 1554
    :pswitch_2
    const v1, 0x1a001843

    .line 1555
    const v2, 0x7f0201b1

    .line 1556
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f06008a

    .line 1555
    invoke-static {v3, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    move v5, v11

    .line 1554
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_3

    .line 1559
    :pswitch_3
    const v1, 0x1a001844

    .line 1560
    const v2, 0x7f0201b4

    .line 1561
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f06001d

    invoke-static {v3, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    move v5, v11

    .line 1559
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_3

    .line 1564
    :pswitch_4
    const v1, 0x1a001845

    .line 1565
    const v2, 0x7f0201b3

    .line 1566
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f06001c

    invoke-static {v3, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x2

    .line 1564
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    .line 1567
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->removeDivider()V

    goto :goto_3

    .line 1592
    .end local v0    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v7    # "i":I
    :cond_2
    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout$LayoutParams;

    .line 1596
    .local v10, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    .line 1608
    const/4 v1, -0x2

    iput v1, v10, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1611
    :goto_4
    invoke-virtual {v8, v12}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1612
    invoke-virtual {v8, v10}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 1602
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050249

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v10, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1603
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050248

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v10, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    goto :goto_4

    .line 1625
    .end local v10    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :sswitch_1
    invoke-direct {p0, p1, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->adjustBottomButtonMargin(II)V

    goto/16 :goto_1

    .line 1542
    :pswitch_data_0
    .packed-switch 0x1a000000
        :pswitch_0
    .end packed-switch

    .line 1617
    :sswitch_data_0
    .sparse-switch
        0x10000000 -> :sswitch_1
        0x11000000 -> :sswitch_1
        0x15000000 -> :sswitch_1
        0x16000000 -> :sswitch_1
        0x18000000 -> :sswitch_1
        0x20000000 -> :sswitch_1
        0x31000000 -> :sswitch_1
    .end sparse-switch

    .line 1547
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 1596
    :sswitch_data_1
    .sparse-switch
        0x11100000 -> :sswitch_0
        0x11200000 -> :sswitch_0
        0x11300000 -> :sswitch_0
        0x14000000 -> :sswitch_0
        0x1a000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public initBottomButtonForSticker(I)V
    .locals 9
    .param p1, "viewWidth"    # I

    .prologue
    const/4 v4, 0x0

    .line 1473
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 1474
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1475
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->removeAllViews()V

    .line 1487
    new-instance v8, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-direct {v8, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1488
    .local v8, "layout":Landroid/widget/LinearLayout;
    invoke-virtual {v8, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1493
    const/4 v6, 0x0

    .line 1494
    .local v6, "buttonWidth":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    const/4 v1, 0x4

    if-lt v7, v1, :cond_1

    .line 1527
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v6, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v8, v2}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1529
    .end local v6    # "buttonWidth":I
    .end local v7    # "i":I
    .end local v8    # "layout":Landroid/widget/LinearLayout;
    :cond_0
    return-void

    .line 1495
    .restart local v6    # "buttonWidth":I
    .restart local v7    # "i":I
    .restart local v8    # "layout":Landroid/widget/LinearLayout;
    :cond_1
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .line 1496
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v2, 0x7f030031

    .line 1495
    invoke-direct {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 1498
    .local v0, "stickerButton":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    packed-switch v7, :pswitch_data_0

    .line 1521
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1522
    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1494
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1500
    :pswitch_0
    const v1, 0x19001822

    .line 1501
    const v2, 0x7f02003a

    .line 1502
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f0600b3

    invoke-static {v3, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    .line 1500
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_1

    .line 1505
    :pswitch_1
    const v1, 0x19001823

    .line 1506
    const v2, 0x7f020039

    .line 1507
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f0600cb

    invoke-static {v3, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    move v5, v4

    .line 1505
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_1

    .line 1510
    :pswitch_2
    const v1, 0x19001824

    .line 1511
    const v2, 0x7f02003b

    .line 1512
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f0600cf

    invoke-static {v3, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    move v5, v4

    .line 1510
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_1

    .line 1515
    :pswitch_3
    const v1, 0x19001825

    .line 1516
    const v2, 0x7f02003c

    .line 1517
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v5, 0x7f0600cd

    invoke-static {v3, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x2

    .line 1515
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    .line 1518
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->removeDivider()V

    goto :goto_1

    .line 1498
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public initBottomButtonWithIcon(I)V
    .locals 20
    .param p1, "viewWidth"    # I

    .prologue
    .line 980
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sj, initBottomButtonWithIcon() - curMode : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 981
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    .line 982
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 983
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->removeAllViews()V

    .line 984
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->setVisibility(I)V

    .line 986
    new-instance v14, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-direct {v14, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 990
    .local v14, "layout":Landroid/widget/LinearLayout;
    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 992
    const/16 v18, 0x0

    .line 993
    .local v18, "splitString":Ljava/lang/String;
    const/4 v12, 0x0

    .line 995
    .local v12, "buttonWidth":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v16

    .line 996
    .local v16, "mode":I
    const/high16 v3, 0x16000000

    move/from16 v0, v16

    if-ne v0, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    instance-of v3, v3, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v3, :cond_0

    .line 998
    const/high16 v16, 0x1e110000

    .line 1001
    :cond_0
    sparse-switch v16, :sswitch_data_0

    .line 1312
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    invoke-virtual {v3, v14}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->addView(Landroid/view/View;)V

    .line 1313
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v3

    const/high16 v4, 0x17000000

    if-ne v3, v4, :cond_4

    .line 1315
    invoke-virtual {v14}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Landroid/widget/LinearLayout$LayoutParams;

    .line 1316
    .local v15, "llpp":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v3, 0x51

    iput v3, v15, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1317
    const/16 v3, 0x51

    invoke-virtual {v14, v3}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1318
    invoke-virtual {v14, v15}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1343
    .end local v15    # "llpp":Landroid/widget/LinearLayout$LayoutParams;
    :goto_1
    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mButtonWidth:I

    .line 1345
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v3

    sparse-switch v3, :sswitch_data_1

    .line 1363
    .end local v12    # "buttonWidth":I
    .end local v14    # "layout":Landroid/widget/LinearLayout;
    .end local v16    # "mode":I
    .end local v18    # "splitString":Ljava/lang/String;
    :cond_2
    :goto_2
    return-void

    .line 1011
    .restart local v12    # "buttonWidth":I
    .restart local v14    # "layout":Landroid/widget/LinearLayout;
    .restart local v16    # "mode":I
    .restart local v18    # "splitString":Ljava/lang/String;
    :sswitch_0
    const-string v3, "initBottomButtonWithIcon"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1012
    const/4 v13, 0x1

    .local v13, "i":I
    :goto_3
    const/4 v3, 0x6

    if-ge v13, v3, :cond_1

    .line 1013
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .line 1014
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x7f03002c

    .line 1013
    invoke-direct {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 1015
    .local v2, "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    const v3, 0x7f090005

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 1016
    .local v19, "textView":Landroid/widget/TextView;
    const/4 v3, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 1017
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1018
    packed-switch v13, :pswitch_data_0

    .line 1053
    :goto_4
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1054
    invoke-virtual {v14, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1055
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1056
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v12, v3

    .line 1012
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 1026
    :pswitch_1
    const v3, 0x1000100a

    .line 1027
    const v4, 0x7f020022

    .line 1028
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v7, 0x7f060185

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 1026
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_4

    .line 1031
    :pswitch_2
    const v3, 0x10001004

    .line 1032
    const v4, 0x7f02015f

    .line 1033
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v7, 0x7f060186

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1031
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_4

    .line 1036
    :pswitch_3
    const v3, 0x10001005

    .line 1037
    const v4, 0x7f0201c2

    .line 1038
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v7, 0x7f060043

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1036
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_4

    .line 1041
    :pswitch_4
    const v3, 0x10001006

    .line 1042
    const v4, 0x7f0204cb

    .line 1043
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v7, 0x7f0600e1

    .line 1042
    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1043
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1041
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_4

    .line 1047
    :pswitch_5
    const v3, 0x1000100b

    .line 1048
    const v4, 0x7f02019a

    .line 1049
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v7, 0x7f060058

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x2

    .line 1047
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    .line 1050
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->removeDivider()V

    goto/16 :goto_4

    .line 1060
    .end local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v13    # "i":I
    .end local v19    # "textView":Landroid/widget/TextView;
    :sswitch_1
    const v3, 0x7f020372

    invoke-virtual {v14, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 1061
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_5
    const/4 v3, 0x3

    if-ge v13, v3, :cond_1

    .line 1062
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .line 1063
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x7f030031

    .line 1062
    invoke-direct {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 1064
    .restart local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    packed-switch v13, :pswitch_data_1

    .line 1104
    :goto_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1105
    invoke-virtual {v14, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1106
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1107
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v12, v3

    .line 1061
    add-int/lit8 v13, v13, 0x1

    goto :goto_5

    .line 1066
    :pswitch_6
    const v3, 0x14001404

    .line 1067
    const v4, 0x7f02009e

    .line 1068
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v7, 0x7f0601d3

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x1

    .line 1066
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_6

    .line 1071
    :pswitch_7
    const v3, 0x14001402

    .line 1072
    const v4, 0x7f02009f

    .line 1073
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v7, 0x7f0600b5

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1071
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_6

    .line 1076
    :pswitch_8
    const v3, 0x14001405

    .line 1077
    const v4, 0x7f02009d

    .line 1078
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v7, 0x7f0600b4

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x2

    .line 1076
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    .line 1079
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->removeDivider()V

    goto :goto_6

    .line 1111
    .end local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v13    # "i":I
    :sswitch_2
    const v3, 0x7f020372

    invoke-virtual {v14, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 1112
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_7
    const/4 v3, 0x5

    if-ge v13, v3, :cond_1

    .line 1113
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x7f030031

    invoke-direct {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 1114
    .restart local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    packed-switch v13, :pswitch_data_2

    .line 1142
    :goto_8
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1145
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1146
    invoke-virtual {v14, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1147
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1148
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v12, v3

    .line 1112
    add-int/lit8 v13, v13, 0x1

    goto :goto_7

    .line 1116
    :pswitch_9
    const v3, 0x11101101

    .line 1117
    const v4, 0x7f0204f2

    .line 1118
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v7, 0x7f060099

    .line 1117
    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1118
    const/4 v6, 0x1

    const/4 v7, 0x1

    .line 1116
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_8

    .line 1121
    :pswitch_a
    const v3, 0x11101102

    .line 1122
    const v4, 0x7f0204f4

    .line 1123
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v7, 0x7f06009a

    .line 1122
    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1123
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1121
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_8

    .line 1126
    :pswitch_b
    const v3, 0x11101103

    .line 1127
    const v4, 0x7f0204f1

    .line 1128
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v7, 0x7f060017

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1126
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_8

    .line 1131
    :pswitch_c
    const v3, 0x11101104

    .line 1132
    const v4, 0x7f0204f5

    .line 1133
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v7, 0x7f060018

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1131
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_8

    .line 1136
    :pswitch_d
    const v3, 0x11001108

    .line 1137
    const v4, 0x7f0204f3

    .line 1138
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v7, 0x7f060056

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x2

    .line 1136
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    .line 1139
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->removeDivider()V

    goto/16 :goto_8

    .line 1152
    .end local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v13    # "i":I
    :sswitch_3
    const v3, 0x7f020372

    invoke-virtual {v14, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 1153
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_9
    const/4 v3, 0x5

    if-ge v13, v3, :cond_1

    .line 1154
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x7f030031

    invoke-direct {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 1156
    .restart local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    packed-switch v13, :pswitch_data_3

    .line 1184
    :goto_a
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1186
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1187
    invoke-virtual {v14, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1188
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1189
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v12, v3

    .line 1153
    add-int/lit8 v13, v13, 0x1

    goto :goto_9

    .line 1158
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x7f060032

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1159
    .local v5, "text":Ljava/lang/String;
    const v3, 0x11301206

    .line 1160
    const v4, 0x7f0204ec

    const/4 v6, 0x1

    const/4 v7, 0x1

    .line 1159
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_a

    .line 1163
    .end local v5    # "text":Ljava/lang/String;
    :pswitch_f
    new-instance v3, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060032

    invoke-static {v4, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " 10"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1164
    .restart local v5    # "text":Ljava/lang/String;
    const v3, 0x11301201

    .line 1165
    const v4, 0x7f0204ee

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1164
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_a

    .line 1168
    .end local v5    # "text":Ljava/lang/String;
    :pswitch_10
    new-instance v3, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060032

    invoke-static {v4, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " 25"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1169
    .restart local v5    # "text":Ljava/lang/String;
    const v3, 0x11301202

    .line 1170
    const v4, 0x7f0204ef

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1169
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto/16 :goto_a

    .line 1173
    .end local v5    # "text":Ljava/lang/String;
    :pswitch_11
    new-instance v3, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060032

    invoke-static {v4, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " 50"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1174
    .restart local v5    # "text":Ljava/lang/String;
    const v3, 0x11301203

    .line 1175
    const v4, 0x7f0204eb

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1174
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto/16 :goto_a

    .line 1178
    .end local v5    # "text":Ljava/lang/String;
    :pswitch_12
    new-instance v3, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060032

    invoke-static {v4, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " 75"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1179
    .restart local v5    # "text":Ljava/lang/String;
    const v3, 0x11301204

    .line 1180
    const v4, 0x7f0204ed

    const/4 v6, 0x1

    const/4 v7, 0x2

    .line 1179
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    .line 1181
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->removeDivider()V

    goto/16 :goto_a

    .line 1193
    .end local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v5    # "text":Ljava/lang/String;
    .end local v13    # "i":I
    :sswitch_4
    const v3, 0x7f020372

    invoke-virtual {v14, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 1194
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_b
    const/4 v3, 0x5

    if-ge v13, v3, :cond_1

    .line 1195
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .line 1196
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x7f030026

    .line 1195
    invoke-direct {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 1198
    .restart local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    packed-switch v13, :pswitch_data_4

    .line 1223
    :goto_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1224
    invoke-virtual {v14, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1225
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1226
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v12, v3

    .line 1227
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "init icon button width:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1194
    add-int/lit8 v13, v13, 0x1

    goto :goto_b

    .line 1200
    :pswitch_13
    const v7, 0x11201306

    .line 1201
    const v8, 0x7f02016a

    .line 1202
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x7f060092

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v11, 0x1

    move-object v6, v2

    .line 1200
    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_c

    .line 1205
    :pswitch_14
    const v7, 0x11201302

    .line 1206
    const v8, 0x7f020169

    const-string v9, "1:1"

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object v6, v2

    .line 1205
    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_c

    .line 1209
    :pswitch_15
    const v7, 0x11201303

    .line 1210
    const v8, 0x7f020164

    const-string v9, "4:3"

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object v6, v2

    .line 1209
    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_c

    .line 1213
    :pswitch_16
    const v7, 0x11201304

    .line 1214
    const v8, 0x7f02016b

    const-string v9, "16:9"

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object v6, v2

    .line 1213
    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_c

    .line 1217
    :pswitch_17
    const v7, 0x11201305

    .line 1218
    const v8, 0x7f020165

    .line 1219
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x7f060091

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v11, 0x2

    move-object v6, v2

    .line 1217
    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    .line 1220
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->removeDivider()V

    goto/16 :goto_c

    .line 1231
    .end local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v13    # "i":I
    :sswitch_5
    const v3, 0x7f020372

    invoke-virtual {v14, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 1232
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_d
    const/4 v3, 0x2

    if-ge v13, v3, :cond_1

    .line 1233
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .line 1234
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x7f030025

    .line 1233
    invoke-direct {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 1236
    .restart local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    packed-switch v13, :pswitch_data_5

    .line 1251
    :goto_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1252
    invoke-virtual {v14, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1253
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1254
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v12, v3

    .line 1232
    add-int/lit8 v13, v13, 0x1

    goto :goto_d

    .line 1239
    :pswitch_18
    const v7, 0x1d001701

    .line 1240
    const v8, 0x7f020162

    .line 1241
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x7f060103

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v11, 0x1

    move-object v6, v2

    .line 1238
    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    .line 1242
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setSelected(Z)V

    goto :goto_e

    .line 1246
    :pswitch_19
    const v7, 0x1d001702

    .line 1247
    const v8, 0x7f020160

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x7f060104

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v11, 0x2

    move-object v6, v2

    .line 1245
    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    .line 1248
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->removeDivider()V

    goto :goto_e

    .line 1260
    .end local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v13    # "i":I
    :sswitch_6
    const/4 v11, 0x0

    .line 1261
    .local v11, "buttonSide":I
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_f
    const/4 v3, 0x4

    if-ge v13, v3, :cond_1

    .line 1263
    if-nez v13, :cond_3

    .line 1265
    const/4 v11, 0x1

    .line 1271
    :goto_10
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .line 1272
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x7f03002d

    .line 1271
    invoke-direct {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 1273
    .restart local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    const v3, 0x7f090005

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 1274
    .restart local v19    # "textView":Landroid/widget/TextView;
    const/4 v3, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 1275
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1277
    const/high16 v3, 0x1e300000

    add-int v7, v3, v13

    .line 1278
    const v3, 0x7f020042

    add-int v8, v3, v13

    .line 1279
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x7f060194

    add-int/2addr v4, v13

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 1280
    const/4 v10, 0x0

    move-object v6, v2

    .line 1277
    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    .line 1284
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1285
    invoke-virtual {v14, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1286
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1287
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v12, v3

    .line 1261
    add-int/lit8 v13, v13, 0x1

    goto :goto_f

    .line 1269
    .end local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v19    # "textView":Landroid/widget/TextView;
    :cond_3
    const/4 v11, 0x0

    goto :goto_10

    .line 1293
    .end local v11    # "buttonSide":I
    .end local v13    # "i":I
    :sswitch_7
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;

    .line 1294
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    .line 1293
    invoke-direct {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;-><init>(Landroid/content/Context;)V

    .line 1295
    .local v2, "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;
    invoke-virtual {v14, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1297
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomFillparentWidthButtonLayout:Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;

    .line 1300
    const/high16 v3, 0x17000000

    .line 1301
    const v4, 0x7f0200e1

    .line 1300
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->init(II)V

    .line 1306
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->removeDivider()V

    .line 1307
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1322
    .end local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;
    :cond_4
    invoke-virtual {v14}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout$LayoutParams;

    .line 1324
    .local v17, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v3

    sparse-switch v3, :sswitch_data_2

    .line 1336
    const/4 v3, -0x2

    move-object/from16 v0, v17

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1340
    :goto_11
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 1330
    :sswitch_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050249

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    move-object/from16 v0, v17

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1331
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050248

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    move-object/from16 v0, v17

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    goto :goto_11

    .line 1355
    .end local v17    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :sswitch_9
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v12}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->adjustBottomButtonMargin(II)V

    goto/16 :goto_2

    .line 1001
    :sswitch_data_0
    .sparse-switch
        0x10000000 -> :sswitch_0
        0x11000000 -> :sswitch_0
        0x11100000 -> :sswitch_2
        0x11200000 -> :sswitch_4
        0x11300000 -> :sswitch_3
        0x14000000 -> :sswitch_1
        0x15000000 -> :sswitch_0
        0x16000000 -> :sswitch_0
        0x17000000 -> :sswitch_7
        0x18000000 -> :sswitch_0
        0x1c000000 -> :sswitch_0
        0x1d000000 -> :sswitch_5
        0x1e110000 -> :sswitch_6
        0x20000000 -> :sswitch_0
        0x2b000000 -> :sswitch_0
        0x2c000000 -> :sswitch_6
        0x31000000 -> :sswitch_0
    .end sparse-switch

    .line 1345
    :sswitch_data_1
    .sparse-switch
        0x10000000 -> :sswitch_9
        0x11000000 -> :sswitch_9
        0x15000000 -> :sswitch_9
        0x16000000 -> :sswitch_9
        0x18000000 -> :sswitch_9
        0x1e110000 -> :sswitch_9
        0x20000000 -> :sswitch_9
        0x2c000000 -> :sswitch_9
        0x31000000 -> :sswitch_9
    .end sparse-switch

    .line 1018
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 1064
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 1114
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 1156
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch

    .line 1198
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch

    .line 1236
    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_18
        :pswitch_19
    .end packed-switch

    .line 1324
    :sswitch_data_2
    .sparse-switch
        0x11100000 -> :sswitch_8
        0x11200000 -> :sswitch_8
        0x11300000 -> :sswitch_8
        0x14000000 -> :sswitch_8
        0x1a000000 -> :sswitch_8
    .end sparse-switch
.end method

.method public initBottomButtonWithThumbnail(I[III)V
    .locals 17
    .param p1, "viewWidth"    # I
    .param p2, "image"    # [I
    .param p3, "imageWidth"    # I
    .param p4, "imageHeight"    # I

    .prologue
    .line 1379
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1462
    :cond_0
    :goto_0
    return-void

    .line 1381
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 1382
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1383
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->removeAllViews()V

    .line 1385
    new-instance v13, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-direct {v13, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1386
    .local v13, "layout":Landroid/widget/LinearLayout;
    const/4 v1, 0x0

    invoke-virtual {v13, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1387
    new-instance v14, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    move/from16 v0, p1

    invoke-direct {v14, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1388
    .local v14, "lp":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    invoke-virtual {v1, v13, v14}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1390
    const/4 v5, 0x0

    .line 1391
    .local v5, "width":I
    const/4 v6, 0x0

    .line 1392
    .local v6, "height":I
    const/4 v15, 0x0

    .line 1393
    .local v15, "thumbnail":[I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    const/16 v1, 0xa

    if-lt v11, v1, :cond_1

    .line 1457
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->moveScrollToFirst()V

    goto :goto_0

    .line 1394
    :cond_1
    new-instance v10, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    .line 1395
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    .line 1394
    invoke-direct {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;-><init>(Landroid/content/Context;)V

    .line 1397
    .local v10, "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1398
    invoke-virtual {v13, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1399
    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->getLayoutWidth()I

    move-result v5

    .line 1400
    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->getLayoutHeight()I

    move-result v6

    .line 1402
    const/4 v7, 0x0

    .local v7, "resizeWidth":I
    const/4 v8, 0x0

    .line 1403
    .local v8, "resizeHeight":I
    const/high16 v9, 0x3f800000    # 1.0f

    .line 1404
    .local v9, "scale":F
    int-to-float v1, v5

    int-to-float v2, v6

    div-float v16, v1, v2

    .line 1405
    .local v16, "viewRatio":F
    move/from16 v0, p3

    int-to-float v1, v0

    move/from16 v0, p4

    int-to-float v2, v0

    div-float v12, v1, v2

    .line 1407
    .local v12, "imgRatio":F
    cmpl-float v1, v16, v12

    if-lez v1, :cond_4

    .line 1408
    int-to-float v1, v5

    move/from16 v0, p3

    int-to-float v2, v0

    div-float v9, v1, v2

    .line 1409
    move/from16 v0, p4

    int-to-float v1, v0

    mul-float/2addr v1, v9

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v8, v1

    .line 1410
    move v7, v5

    .line 1419
    :goto_2
    if-nez v11, :cond_2

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    .line 1420
    invoke-direct/range {v1 .. v9}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->makeThumbnail([IIIIIIIF)[I

    move-result-object v15

    .line 1422
    :cond_2
    if-eqz v15, :cond_3

    .line 1423
    invoke-virtual {v10, v15, v7, v8}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->setThumbnail([III)V

    .line 1424
    :cond_3
    packed-switch v11, :pswitch_data_1

    .line 1393
    :goto_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 1414
    :cond_4
    int-to-float v1, v6

    move/from16 v0, p4

    int-to-float v2, v0

    div-float v9, v1, v2

    .line 1415
    move/from16 v0, p3

    int-to-float v1, v0

    mul-float/2addr v1, v9

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v7, v1

    .line 1416
    move v8, v6

    goto :goto_2

    .line 1426
    :pswitch_1
    const v1, 0x3120006e

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->setId(I)V

    goto :goto_3

    .line 1429
    :pswitch_2
    const v1, 0x3120006f

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->setId(I)V

    goto :goto_3

    .line 1432
    :pswitch_3
    const v1, 0x31200070

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->setId(I)V

    goto :goto_3

    .line 1435
    :pswitch_4
    const v1, 0x31200071

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->setId(I)V

    goto :goto_3

    .line 1438
    :pswitch_5
    const v1, 0x31200072

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->setId(I)V

    goto :goto_3

    .line 1441
    :pswitch_6
    const v1, 0x31200073

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->setId(I)V

    goto :goto_3

    .line 1444
    :pswitch_7
    const v1, 0x31200074

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->setId(I)V

    goto :goto_3

    .line 1447
    :pswitch_8
    const v1, 0x31200075

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->setId(I)V

    goto :goto_3

    .line 1450
    :pswitch_9
    const v1, 0x31200076

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->setId(I)V

    goto :goto_3

    .line 1453
    :pswitch_a
    const v1, 0x31200077

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->setId(I)V

    goto :goto_3

    .line 1379
    nop

    :pswitch_data_0
    .packed-switch 0x31200000
        :pswitch_0
    .end packed-switch

    .line 1424
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public initSubViewButtons(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V
    .locals 3
    .param p1, "effectEffect"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .prologue
    .line 86
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    const v2, 0x7f0900b7

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 87
    .local v0, "submenuLayout":Landroid/widget/FrameLayout;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 88
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    .line 90
    return-void
.end method

.method public isAnimation()Z
    .locals 1

    .prologue
    .line 376
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mAnimate:Z

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 380
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsShowing:Z

    return v0
.end method

.method public isSubButtonShowing()Z
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->isShowing()Z

    move-result v0

    .line 387
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveArrowToCurrentViewBtn()V
    .locals 3

    .prologue
    .line 2527
    const/4 v0, 0x0

    .line 2529
    .local v0, "position":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2579
    :goto_0
    const-string v1, "sj"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->whoIsCallMe(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2580
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sj, VBM - moveArrowToCurrentViewBtn() - ViewStatus.getCurrentMode() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / position : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2582
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v1, :cond_0

    .line 2583
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->moveArrowToCurrentViewBtn(I)V

    .line 2584
    :cond_0
    return-void

    .line 2532
    :sswitch_0
    const v1, 0x10001002

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v0

    .line 2533
    goto :goto_0

    .line 2535
    :sswitch_1
    const v1, 0x10001003

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v0

    .line 2536
    goto :goto_0

    .line 2538
    :sswitch_2
    const-string v1, "moveArrowToCurrentViewBtn"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2539
    const v1, 0x10001004

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v0

    .line 2540
    goto :goto_0

    .line 2542
    :sswitch_3
    const v1, 0x10001005

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v0

    .line 2543
    goto :goto_0

    .line 2545
    :sswitch_4
    const v1, 0x10001006

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v0

    .line 2546
    goto :goto_0

    .line 2548
    :sswitch_5
    const v1, 0x10001008

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v0

    .line 2549
    goto :goto_0

    .line 2551
    :sswitch_6
    const v1, 0x10001007

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v0

    .line 2552
    goto :goto_0

    .line 2554
    :sswitch_7
    const v1, 0x1000100a

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v0

    .line 2555
    goto :goto_0

    .line 2557
    :sswitch_8
    const v1, 0x1000100b

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonCenterPosition(I)I

    move-result v0

    .line 2558
    goto/16 :goto_0

    .line 2562
    :sswitch_9
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 2565
    :pswitch_0
    const/high16 v1, 0x1e300000

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getBtnCenterPositionWithBackBtn_Collage(I)I

    move-result v0

    .line 2566
    goto/16 :goto_0

    .line 2568
    :pswitch_1
    const v1, 0x1e300001

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getBtnCenterPositionWithBackBtn_Collage(I)I

    move-result v0

    .line 2569
    goto/16 :goto_0

    .line 2571
    :pswitch_2
    const v1, 0x1e300002

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getBtnCenterPositionWithBackBtn_Collage(I)I

    move-result v0

    .line 2572
    goto/16 :goto_0

    .line 2574
    :pswitch_3
    const v1, 0x1e300003

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getBtnCenterPositionWithBackBtn_Collage(I)I

    move-result v0

    goto/16 :goto_0

    .line 2529
    :sswitch_data_0
    .sparse-switch
        0x11000000 -> :sswitch_7
        0x11100000 -> :sswitch_0
        0x11200000 -> :sswitch_1
        0x15000000 -> :sswitch_2
        0x16000000 -> :sswitch_3
        0x18000000 -> :sswitch_4
        0x1e110000 -> :sswitch_9
        0x2c000000 -> :sswitch_9
        0x31000000 -> :sswitch_8
        0x31100000 -> :sswitch_5
        0x31200000 -> :sswitch_6
    .end sparse-switch

    .line 2562
    :pswitch_data_0
    .packed-switch 0x1e300000
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public moveScrollToFirst()V
    .locals 0

    .prologue
    .line 2373
    return-void
.end method

.method public movoToPosition(I)V
    .locals 6
    .param p1, "buttonId"    # I

    .prologue
    const/4 v5, 0x0

    .line 523
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 524
    const/4 v3, 0x0

    .line 525
    .local v3, "width":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 545
    .end local v1    # "i":I
    .end local v3    # "width":I
    :cond_0
    return-void

    .line 526
    .restart local v1    # "i":I
    .restart local v3    # "width":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 527
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v5, v5}, Landroid/widget/LinearLayout;->measure(II)V

    .line 528
    if-eqz v1, :cond_2

    .line 529
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    .line 531
    :cond_2
    move v2, v3

    .line 532
    .local v2, "position":I
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v4

    if-eq v4, p1, :cond_0

    .line 525
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public movoToPositionWithBackBtn(I)V
    .locals 6
    .param p1, "buttonId"    # I

    .prologue
    const/4 v5, 0x0

    .line 548
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 549
    const/4 v3, 0x0

    .line 550
    .local v3, "width":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 572
    .end local v1    # "i":I
    .end local v3    # "width":I
    :cond_0
    :goto_1
    return-void

    .line 551
    .restart local v1    # "i":I
    .restart local v3    # "width":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 552
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v5, v5}, Landroid/widget/LinearLayout;->measure(II)V

    .line 554
    move v2, v3

    .line 555
    .local v2, "position":I
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v4

    if-ne v4, p1, :cond_2

    .line 556
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "movoToPosition:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    goto :goto_1

    .line 567
    :cond_2
    if-eqz v1, :cond_3

    .line 568
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    .line 550
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public onConfigurationChanged()V
    .locals 14

    .prologue
    .line 1941
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->removeAllViews()V

    .line 1942
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    .line 1943
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    const v10, 0x7f0900ba

    invoke-virtual {v9, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    .line 1944
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBottomLayoutCallback()V

    .line 1951
    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v9}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1953
    .local v2, "layout":Landroid/widget/LinearLayout;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    .line 2104
    :goto_0
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mAnimate:Z

    .line 2107
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2109
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v9, :cond_0

    .line 2110
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->onConfigurationChanged()V

    .line 2114
    :cond_0
    new-instance v9, Landroid/os/Handler;

    invoke-direct {v9}, Landroid/os/Handler;-><init>()V

    new-instance v10, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$13;

    invoke-direct {v10, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 2122
    const-wide/16 v12, 0x64

    .line 2114
    invoke-virtual {v9, v10, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2123
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeLanguage()V

    .line 2124
    :goto_1
    return-void

    .line 1960
    :sswitch_0
    const v9, 0x7f020372

    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 1975
    :sswitch_1
    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1978
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v1, v9, :cond_3

    .line 2009
    const/4 v0, 0x0

    .line 2010
    .local v0, "buttonWidth":I
    const/4 v1, 0x0

    :goto_3
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v1, v9, :cond_9

    .line 2020
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    invoke-virtual {v9, v2}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->addView(Landroid/view/View;)V

    .line 2021
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v9

    const/high16 v10, 0x20000000

    if-eq v9, v10, :cond_1

    .line 2022
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v9

    const/high16 v10, 0x10000000

    if-eq v9, v10, :cond_1

    .line 2023
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v9

    const/high16 v10, 0x11000000

    if-eq v9, v10, :cond_1

    .line 2024
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v9

    const/high16 v10, 0x15000000

    if-eq v9, v10, :cond_1

    .line 2025
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v9

    const/high16 v10, 0x16000000

    if-eq v9, v10, :cond_1

    .line 2026
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v9

    const/high16 v10, 0x18000000

    if-eq v9, v10, :cond_1

    .line 2027
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v9

    const/high16 v10, 0x31000000

    if-eq v9, v10, :cond_1

    .line 2028
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v9

    const/high16 v10, 0x2c000000

    if-eq v9, v10, :cond_1

    .line 2029
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v9

    const/high16 v10, 0x1e110000

    if-ne v9, v10, :cond_2

    .line 2030
    :cond_1
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 2031
    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2030
    invoke-direct {p0, v9, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->adjustBottomButtonMargin(II)V

    .line 2034
    :cond_2
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v9

    const/high16 v10, 0x17000000

    if-ne v9, v10, :cond_b

    .line 2036
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    .line 2038
    .local v4, "newConfig":Landroid/content/res/Configuration;
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 2039
    .local v3, "llpp":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v9, 0x51

    iput v9, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2040
    const/16 v9, 0x51

    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 2041
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2042
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomFillparentWidthButtonLayout:Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;

    invoke-virtual {v9, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2044
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v8

    .line 2045
    .local v8, "visibility":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    const v10, 0x7f0900b5

    invoke-virtual {v9, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    .line 2046
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v9, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1980
    .end local v0    # "buttonWidth":I
    .end local v3    # "llpp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v4    # "newConfig":Landroid/content/res/Configuration;
    .end local v8    # "visibility":I
    :cond_3
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 1981
    .local v6, "v":Landroid/view/ViewGroup;
    instance-of v9, v6, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    if-eqz v9, :cond_5

    .line 1983
    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    .end local v6    # "v":Landroid/view/ViewGroup;
    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->configurationChanged()V

    .line 1978
    :cond_4
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 1985
    .restart local v6    # "v":Landroid/view/ViewGroup;
    :cond_5
    instance-of v9, v6, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    if-eqz v9, :cond_6

    .line 1987
    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .end local v6    # "v":Landroid/view/ViewGroup;
    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->configurationChanged()V

    goto :goto_4

    .line 1989
    .restart local v6    # "v":Landroid/view/ViewGroup;
    :cond_6
    instance-of v9, v6, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v9, :cond_7

    move-object v9, v6

    .line 1991
    check-cast v9, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->configurationChanged()V

    .line 1992
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ne v1, v9, :cond_4

    .line 1993
    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .end local v6    # "v":Landroid/view/ViewGroup;
    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->removeDivider()V

    goto :goto_4

    .line 1995
    .restart local v6    # "v":Landroid/view/ViewGroup;
    :cond_7
    instance-of v9, v6, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;

    if-eqz v9, :cond_8

    move-object v9, v6

    .line 1997
    check-cast v9, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->configurationChanged()V

    .line 1998
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ne v1, v9, :cond_4

    .line 1999
    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;

    .end local v6    # "v":Landroid/view/ViewGroup;
    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->removeDivider()V

    goto :goto_4

    .line 2001
    .restart local v6    # "v":Landroid/view/ViewGroup;
    :cond_8
    instance-of v9, v6, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;

    if-eqz v9, :cond_4

    move-object v9, v6

    .line 2003
    check-cast v9, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->configurationChanged()V

    .line 2004
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ne v1, v9, :cond_4

    .line 2005
    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;

    .end local v6    # "v":Landroid/view/ViewGroup;
    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->removeDivider()V

    goto :goto_4

    .line 2012
    .restart local v0    # "buttonWidth":I
    :cond_9
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 2013
    .restart local v6    # "v":Landroid/view/ViewGroup;
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 2014
    .local v7, "vGroup":Landroid/view/ViewGroup;
    if-eqz v7, :cond_a

    .line 2015
    invoke-virtual {v7, v6}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2016
    :cond_a
    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2017
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v6, v9, v10}, Landroid/view/ViewGroup;->measure(II)V

    .line 2018
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v0, v9

    .line 2010
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    .line 2050
    .end local v6    # "v":Landroid/view/ViewGroup;
    .end local v7    # "vGroup":Landroid/view/ViewGroup;
    :cond_b
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 2051
    .local v5, "params":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v9, -0x2

    iput v9, v5, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 2052
    const/4 v9, -0x2

    iput v9, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2053
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v9

    sparse-switch v9, :sswitch_data_1

    .line 2078
    :goto_5
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    if-eqz v9, :cond_d

    .line 2080
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v8

    .line 2081
    .restart local v8    # "visibility":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    const v10, 0x7f0900b5

    invoke-virtual {v9, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    .line 2082
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v9, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2084
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v9

    const/high16 v10, 0x10000000

    if-ne v9, v10, :cond_c

    .line 2086
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2089
    :cond_c
    const/4 v9, -0x2

    iput v9, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2093
    .end local v8    # "visibility":I
    :cond_d
    :goto_6
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 2059
    :sswitch_2
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    if-eqz v9, :cond_e

    .line 2060
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    const v10, 0x7f0900b5

    invoke-virtual {v9, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    .line 2061
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2063
    :cond_e
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f050249

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v9, v9

    iput v9, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2064
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f050248

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v9, v9

    iput v9, v5, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    goto :goto_6

    .line 2069
    :sswitch_3
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    if-eqz v9, :cond_f

    .line 2071
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    const v10, 0x7f0900b5

    invoke-virtual {v9, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    .line 2072
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2073
    const/4 v9, -0x2

    iput v9, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2075
    :cond_f
    const-string v9, "kbr : ViewButtonManger onConfigurationChanged"

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 2098
    .end local v0    # "buttonWidth":I
    .end local v1    # "i":I
    .end local v5    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :sswitch_4
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    if-eqz v9, :cond_10

    .line 2099
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->setVisibility(I)V

    .line 2100
    :cond_10
    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    goto/16 :goto_1

    .line 1953
    nop

    :sswitch_data_0
    .sparse-switch
        0x10000000 -> :sswitch_1
        0x11000000 -> :sswitch_1
        0x11100000 -> :sswitch_0
        0x11200000 -> :sswitch_0
        0x11300000 -> :sswitch_0
        0x14000000 -> :sswitch_0
        0x15000000 -> :sswitch_1
        0x16000000 -> :sswitch_1
        0x17000000 -> :sswitch_1
        0x18000000 -> :sswitch_1
        0x1a000000 -> :sswitch_0
        0x1c000000 -> :sswitch_1
        0x1d000000 -> :sswitch_0
        0x1e000000 -> :sswitch_1
        0x1e110000 -> :sswitch_1
        0x20000000 -> :sswitch_1
        0x2b000000 -> :sswitch_1
        0x2c000000 -> :sswitch_1
        0x2f000000 -> :sswitch_1
        0x31000000 -> :sswitch_1
        0x31600000 -> :sswitch_4
    .end sparse-switch

    .line 2053
    :sswitch_data_1
    .sparse-switch
        0x11100000 -> :sswitch_2
        0x11200000 -> :sswitch_2
        0x11300000 -> :sswitch_2
        0x14000000 -> :sswitch_2
        0x1a000000 -> :sswitch_2
        0x1e110000 -> :sswitch_3
        0x2c000000 -> :sswitch_3
    .end sparse-switch
.end method

.method public onkey_main_Enter()V
    .locals 10

    .prologue
    .line 2333
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v4, :cond_0

    .line 2335
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2336
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->onkey_main_Enter()V

    .line 2339
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    .line 2341
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v9, v4, :cond_2

    .line 2369
    .end local v9    # "i":I
    :cond_1
    :goto_1
    return-void

    .line 2344
    .restart local v9    # "i":I
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2345
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ViewButtonsManager onKeyUp mButtonList.get("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") focused"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2346
    const-wide/16 v0, 0x0

    .line 2347
    .local v0, "upTime":J
    const-wide/16 v2, 0x0

    .line 2348
    .local v2, "eventTime":J
    const/4 v8, 0x0

    .line 2350
    .local v8, "e":Landroid/view/MotionEvent;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 2351
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 2353
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 2352
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 2354
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2356
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 2357
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 2359
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 2358
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 2361
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    .line 2365
    .end local v0    # "upTime":J
    .end local v2    # "eventTime":J
    .end local v8    # "e":Landroid/view/MotionEvent;
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ViewButtonsManager not focused onKeyUp mButtonList.get("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") not focused"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2341
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0
.end method

.method public refreshScrollView()V
    .locals 1

    .prologue
    .line 2824
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v0, :cond_0

    .line 2825
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->refreshScrollView()V

    .line 2827
    :cond_0
    return-void
.end method

.method public removeButtonsInFrame()Z
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x1

    return v0
.end method

.method public requestLayout()V
    .locals 6

    .prologue
    .line 2743
    const-string v4, "KHJ requestLayout()"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2744
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mButtonWidth:I

    invoke-direct {p0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->adjustBottomButtonMargin(II)V

    .line 2746
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 2748
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_1

    .line 2783
    .end local v2    # "i":I
    :cond_0
    return-void

    .line 2750
    .restart local v2    # "i":I
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2751
    .local v0, "button":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_2

    .line 2753
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 2754
    .local v3, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v5

    if-eq v4, v5, :cond_3

    .line 2755
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$14;

    invoke-direct {v5, p0, v3, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Landroid/widget/LinearLayout$LayoutParams;Landroid/widget/LinearLayout;)V

    invoke-virtual {v4, v5}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2748
    .end local v0    # "button":Landroid/widget/LinearLayout;
    .end local v3    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2769
    .restart local v0    # "button":Landroid/widget/LinearLayout;
    .restart local v3    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_4

    .line 2770
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mButtonWidth:I

    invoke-direct {p0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->adjustBottomButtonMargin(II)V

    .line 2771
    :cond_4
    if-eqz v3, :cond_5

    .line 2772
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2773
    :cond_5
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->invalidate()V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2776
    .end local v0    # "button":Landroid/widget/LinearLayout;
    .end local v3    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :catch_0
    move-exception v1

    .line 2777
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ViewButtonsManager - changeLayoutSize() : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public resume()V
    .locals 0

    .prologue
    .line 283
    return-void
.end method

.method public scrollViewRequestLayout()V
    .locals 0

    .prologue
    .line 2377
    return-void
.end method

.method public setAnimation(Z)V
    .locals 0
    .param p1, "animate"    # Z

    .prologue
    .line 2728
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mAnimate:Z

    .line 2729
    return-void
.end method

.method public setBottomButtonDirectly(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    const/4 v1, 0x0

    .line 363
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mAnimate:Z

    .line 364
    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    .line 365
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsShowing:Z

    .line 368
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBottomLayoutVisible(I)V

    .line 369
    return-void

    .line 367
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsShowing:Z

    goto :goto_0
.end method

.method public setBottomLayoutVisible(I)V
    .locals 6
    .param p1, "visibility"    # I

    .prologue
    const/high16 v5, 0x20000000

    const/high16 v4, 0x10000000

    .line 2171
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    if-eqz v2, :cond_0

    .line 2172
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    invoke-virtual {v2, p1}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->setVisibility(I)V

    .line 2182
    :cond_0
    if-nez p1, :cond_4

    .line 2183
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setEnabledButtns(Z)V

    .line 2187
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v2, :cond_1

    .line 2188
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v2, p1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setBottomLayoutVisible(I)V

    .line 2190
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_3

    .line 2192
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f0900b5

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 2193
    .local v1, "middleBtnLayout":Landroid/widget/RelativeLayout;
    if-eqz v1, :cond_2

    .line 2196
    const-string v2, "kbr : viewbuttonManager : setBottomLayoutVisible"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2197
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    if-eq v2, v4, :cond_2

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    if-eq v2, v5, :cond_2

    .line 2198
    invoke-virtual {v1, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2201
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f0900b6

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2202
    .local v0, "middleBtn":Landroid/widget/Button;
    if-eqz v0, :cond_3

    .line 2203
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    if-eq v2, v4, :cond_3

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    if-eq v2, v5, :cond_3

    .line 2204
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2207
    .end local v0    # "middleBtn":Landroid/widget/Button;
    .end local v1    # "middleBtnLayout":Landroid/widget/RelativeLayout;
    :cond_3
    return-void

    .line 2185
    :cond_4
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setEnabledButtns(Z)V

    goto :goto_0
.end method

.method public setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V
    .locals 9
    .param p1, "buttonType"    # I
    .param p2, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    .prologue
    .line 1749
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v6, :cond_0

    .line 1750
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v6, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 1752
    :cond_0
    const/4 v1, 0x0

    .line 1753
    .local v1, "button":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v6, :cond_1

    .line 1754
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v2, v6, :cond_2

    .line 1786
    .end local v2    # "i":I
    :cond_1
    return-void

    .line 1755
    .restart local v2    # "i":I
    :cond_2
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "button":Landroid/widget/LinearLayout;
    check-cast v1, Landroid/widget/LinearLayout;

    .line 1756
    .restart local v1    # "button":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    .line 1757
    .local v3, "id":I
    if-ne v3, p1, :cond_4

    .line 1758
    const/high16 v6, -0x10000

    and-int v5, v3, v6

    .line 1759
    .local v5, "type":I
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-direct {v4, v6, v7, p2, v8}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    .line 1760
    .local v4, "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    new-instance v6, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$10;

    invoke-direct {v6, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    invoke-virtual {v4, v6}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->setAnimationCallback(Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;)V

    .line 1767
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1768
    const/high16 v6, 0x15000000

    if-eq v5, v6, :cond_3

    .line 1769
    const/high16 v6, 0x16000000

    if-ne v5, v6, :cond_5

    .line 1770
    :cond_3
    instance-of v6, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    if-eqz v6, :cond_4

    move-object v0, v1

    .line 1772
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .line 1773
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->init(I)V

    .line 1754
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    .end local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    .end local v5    # "type":I
    :cond_4
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1776
    .restart local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    .restart local v5    # "type":I
    :cond_5
    const/high16 v6, 0x31200000

    if-ne v5, v6, :cond_4

    .line 1777
    instance-of v6, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    if-eqz v6, :cond_4

    move-object v0, v1

    .line 1779
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    .line 1780
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->initFrame(I)V

    goto :goto_1
.end method

.method public setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;Ljava/util/HashMap;)V
    .locals 9
    .param p1, "buttonType"    # I
    .param p2, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .param p3, "callback"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "[I>;)V"
        }
    .end annotation

    .prologue
    .line 1895
    .local p4, "textureData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;[I>;"
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v6, :cond_0

    .line 1896
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v6, p1, p2, p4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Ljava/util/HashMap;)V

    .line 1898
    :cond_0
    const/4 v1, 0x0

    .line 1899
    .local v1, "button":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v6, :cond_1

    .line 1900
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v2, v6, :cond_2

    .line 1925
    .end local v2    # "i":I
    :cond_1
    return-void

    .line 1901
    .restart local v2    # "i":I
    :cond_2
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "button":Landroid/widget/LinearLayout;
    check-cast v1, Landroid/widget/LinearLayout;

    .line 1902
    .restart local v1    # "button":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    .line 1903
    .local v3, "id":I
    if-ne v3, p1, :cond_3

    .line 1904
    const/high16 v6, -0x10000

    and-int v5, v3, v6

    .line 1905
    .local v5, "type":I
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-direct {v4, v6, v7, p2, v8}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    .line 1906
    .local v4, "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    invoke-virtual {v4, p3}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->setAnimationCallback(Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;)V

    .line 1907
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1908
    const/high16 v6, 0x16000000

    if-ne v5, v6, :cond_4

    .line 1909
    instance-of v6, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    if-eqz v6, :cond_3

    move-object v0, v1

    .line 1911
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .line 1912
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->init(I)V

    .line 1900
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    .end local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    .end local v5    # "type":I
    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1915
    .restart local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    .restart local v5    # "type":I
    :cond_4
    const/high16 v6, 0x31200000

    if-ne v5, v6, :cond_3

    .line 1916
    instance-of v6, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    if-eqz v6, :cond_3

    move-object v0, v1

    .line 1918
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    .line 1919
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->initFrame(I)V

    goto :goto_1
.end method

.method public setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V
    .locals 9
    .param p1, "buttonType"    # I
    .param p2, "enabled"    # Z
    .param p3, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    .prologue
    .line 1845
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v6, :cond_0

    .line 1847
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v6, p1, p3}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 1849
    :cond_0
    const/4 v1, 0x0

    .line 1850
    .local v1, "button":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v6, :cond_1

    .line 1851
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v2, v6, :cond_2

    .line 1893
    .end local v2    # "i":I
    :cond_1
    return-void

    .line 1852
    .restart local v2    # "i":I
    :cond_2
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "button":Landroid/widget/LinearLayout;
    check-cast v1, Landroid/widget/LinearLayout;

    .line 1854
    .restart local v1    # "button":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    .line 1855
    .local v3, "id":I
    if-ne v3, p1, :cond_4

    .line 1856
    if-eqz p2, :cond_6

    .line 1858
    const/high16 v6, -0x10000

    and-int v5, v3, v6

    .line 1859
    .local v5, "type":I
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-direct {v4, v6, v7, p3, v8}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    .line 1860
    .local v4, "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    new-instance v6, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$12;

    invoke-direct {v6, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    invoke-virtual {v4, v6}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->setAnimationCallback(Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;)V

    .line 1867
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1868
    const/high16 v6, 0x15000000

    if-eq v5, v6, :cond_3

    .line 1869
    const/high16 v6, 0x16000000

    if-ne v5, v6, :cond_5

    .line 1870
    :cond_3
    instance-of v6, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    if-eqz v6, :cond_4

    move-object v0, v1

    .line 1872
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .line 1873
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->init(I)V

    .line 1851
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    .end local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    .end local v5    # "type":I
    :cond_4
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1876
    .restart local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    .restart local v5    # "type":I
    :cond_5
    const/high16 v6, 0x31200000

    if-ne v5, v6, :cond_4

    .line 1877
    instance-of v6, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    if-eqz v6, :cond_4

    move-object v0, v1

    .line 1879
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    .line 1880
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->initFrame(I)V

    goto :goto_1

    .line 1887
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    .end local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    .end local v5    # "type":I
    :cond_6
    instance-of v6, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v6, :cond_4

    move-object v6, v1

    .line 1888
    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    invoke-virtual {v6, p2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setEnabledWidthChildren(Z)V

    goto :goto_1
.end method

.method public setBtnListener(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V
    .locals 9
    .param p1, "buttonType"    # I
    .param p2, "enabled"    # Z
    .param p3, "dim"    # Z
    .param p4, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    .prologue
    const/4 v8, 0x0

    .line 1788
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v6, :cond_0

    .line 1790
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v6, p1, p4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 1792
    :cond_0
    const/4 v1, 0x0

    .line 1793
    .local v1, "button":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v6, :cond_1

    .line 1794
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v2, v6, :cond_2

    .line 1843
    .end local v2    # "i":I
    :cond_1
    return-void

    .line 1795
    .restart local v2    # "i":I
    :cond_2
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "button":Landroid/widget/LinearLayout;
    check-cast v1, Landroid/widget/LinearLayout;

    .line 1797
    .restart local v1    # "button":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    .line 1798
    .local v3, "id":I
    if-ne v3, p1, :cond_4

    .line 1799
    if-eqz p2, :cond_6

    .line 1801
    const/high16 v6, -0x10000

    and-int v5, v3, v6

    .line 1802
    .local v5, "type":I
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-direct {v4, v6, v7, p4, v8}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    .line 1803
    .local v4, "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    new-instance v6, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$11;

    invoke-direct {v6, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    invoke-virtual {v4, v6}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->setAnimationCallback(Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;)V

    .line 1810
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1811
    const/high16 v6, 0x15000000

    if-eq v5, v6, :cond_3

    .line 1812
    const/high16 v6, 0x16000000

    if-ne v5, v6, :cond_5

    .line 1813
    :cond_3
    instance-of v6, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    if-eqz v6, :cond_4

    move-object v0, v1

    .line 1815
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .line 1816
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->init(I)V

    .line 1794
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    .end local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    .end local v5    # "type":I
    :cond_4
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1819
    .restart local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    .restart local v5    # "type":I
    :cond_5
    const/high16 v6, 0x31200000

    if-ne v5, v6, :cond_4

    .line 1820
    instance-of v6, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    if-eqz v6, :cond_4

    move-object v0, v1

    .line 1822
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    .line 1823
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->initFrame(I)V

    goto :goto_1

    .line 1830
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    .end local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    .end local v5    # "type":I
    :cond_6
    if-eqz p3, :cond_7

    .line 1832
    instance-of v6, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v6, :cond_4

    move-object v6, v1

    .line 1833
    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    invoke-virtual {v6, p2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setEnabledWidthChildren(Z)V

    goto :goto_1

    :cond_7
    move-object v6, v1

    .line 1837
    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    invoke-virtual {v6, v8}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setEnabledWithoutDim(Z)V

    goto :goto_1
.end method

.method public setButtonEnable(ZI)V
    .locals 3
    .param p1, "isEnable"    # Z
    .param p2, "buttonId"    # I

    .prologue
    .line 2711
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 2713
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 2724
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 2715
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2717
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v2

    if-ne v2, p2, :cond_2

    .line 2719
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .end local v0    # "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setEnabledWidthChildren(ZI)V

    .line 2713
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setClearSelectedButton()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 735
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 736
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 742
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 737
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 738
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 739
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setPressed(Z)V

    .line 736
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setClearSelectedSubButton()V
    .locals 1

    .prologue
    .line 746
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setClearSelectedButton()V

    .line 747
    return-void
.end method

.method public setConfigChange(Z)V
    .locals 0
    .param p1, "isChange"    # Z

    .prologue
    .line 2733
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsChange:Z

    .line 2734
    return-void
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 2127
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 2128
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_2

    .line 2133
    .end local v0    # "i":I
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2134
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v2, p1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setEnabled(Z)V

    .line 2135
    :cond_1
    return-void

    .line 2129
    .restart local v0    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 2130
    .local v1, "v":Landroid/view/View;
    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 2128
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setEnabledButtns(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 2152
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 2153
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 2157
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 2154
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 2153
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setEnabledWithChildren(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 2138
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 2139
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_2

    .line 2147
    .end local v0    # "i":I
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v2, :cond_1

    .line 2148
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v2, p1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setEnabledWithChildren(Z)V

    .line 2149
    :cond_1
    return-void

    .line 2140
    .restart local v0    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 2141
    .local v1, "v":Landroid/view/View;
    instance-of v2, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v2, :cond_3

    .line 2143
    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .end local v1    # "v":Landroid/view/View;
    invoke-virtual {v1, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setEnabledWidthChildren(Z)V

    .line 2139
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setMiddleButtonListener(Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V
    .locals 5
    .param p1, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    .prologue
    .line 1687
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f0900b6

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1688
    .local v0, "MiddleButton":Landroid/widget/Button;
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, p1, v4}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    .line 1689
    .local v1, "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1690
    return-void
.end method

.method public setMiddleButtonVisible(Z)V
    .locals 6
    .param p1, "visible"    # Z

    .prologue
    const/high16 v5, 0x10000000

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 1694
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->subAnimate:Z

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    .line 1695
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsMiddleButtonVisible:Z

    .line 1738
    :cond_0
    :goto_0
    return-void

    .line 1698
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 1701
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    const v2, 0x7f0900b5

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    .line 1702
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    const v2, 0x7f0900b6

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1704
    .local v0, "MiddleButton":Landroid/widget/Button;
    if-eqz p1, :cond_3

    .line 1706
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_2

    .line 1708
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1709
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 1711
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1712
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1716
    :cond_2
    if-eqz v0, :cond_0

    .line 1718
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 1719
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    if-ne v1, v5, :cond_0

    .line 1721
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1722
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 1728
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_4

    .line 1730
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1732
    :cond_4
    if-eqz v0, :cond_0

    .line 1734
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public setRecentButton(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setRecentButton(Landroid/view/View;)V

    .line 373
    return-void
.end method

.method public setSelectedButton(IIZ)V
    .locals 4
    .param p1, "mainBtnId"    # I
    .param p2, "subBtnId"    # I
    .param p3, "b"    # Z

    .prologue
    .line 665
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v2, :cond_0

    .line 667
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setSelectedButton(IZ)V

    .line 670
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 672
    const-string v2, "TouchFunction setSelectedButton"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 673
    const-string v2, "sj, VBM - setSelectedButton() 1"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 674
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 689
    .end local v1    # "i":I
    :cond_1
    return-void

    .line 676
    .restart local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 677
    .local v0, "button":Landroid/widget/LinearLayout;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sj, VBM - setSelectedButton() - button.getId() : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / buttonId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 678
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v2

    if-ne v2, p1, :cond_3

    .line 680
    invoke-virtual {v0, p3}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 686
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sj, VBM - setSelectedButton() - button select? : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isSelected()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 674
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 684
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto :goto_1
.end method

.method public setSelectedButton(IZ)V
    .locals 3
    .param p1, "buttonId"    # I
    .param p2, "b"    # Z

    .prologue
    .line 696
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v2, :cond_0

    .line 697
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setSelectedButton(IZ)V

    .line 699
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 700
    const-string v2, "TouchFunction setSelectedButton"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 702
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 713
    .end local v1    # "i":I
    :cond_1
    return-void

    .line 703
    .restart local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 705
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v2

    if-ne v2, p1, :cond_3

    .line 706
    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 702
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 708
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto :goto_1
.end method

.method public setSelectedButton(Landroid/view/View;Z)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "b"    # Z

    .prologue
    .line 716
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v2, :cond_0

    .line 717
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setSelectedButton(Landroid/view/View;Z)V

    .line 720
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 721
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 732
    .end local v1    # "i":I
    :cond_1
    return-void

    .line 722
    .restart local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 724
    .local v0, "button":Landroid/widget/LinearLayout;
    if-ne p1, v0, :cond_3

    .line 725
    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 721
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 727
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto :goto_1
.end method

.method public setSubViewButtonSelected(IZ)V
    .locals 1
    .param p1, "buttonId"    # I
    .param p2, "b"    # Z

    .prologue
    .line 691
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v0, :cond_0

    .line 692
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setSelectedButton(IZ)V

    .line 694
    :cond_0
    return-void
.end method

.method public setViewWidth(I)V
    .locals 5
    .param p1, "width"    # I

    .prologue
    .line 2381
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mViewWidth:I

    .line 2382
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2384
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2385
    .local v0, "bottomBtnRect":Landroid/graphics/Rect;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2418
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    if-eqz v3, :cond_1

    .line 2419
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v3, p1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setViewWidth(I)V

    .line 2421
    .end local v0    # "bottomBtnRect":Landroid/graphics/Rect;
    :cond_1
    return-void

    .line 2385
    .restart local v0    # "bottomBtnRect":Landroid/graphics/Rect;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2387
    .local v1, "btn":Landroid/widget/LinearLayout;
    instance-of v4, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v4, :cond_0

    .line 2390
    const/4 v2, -0x1

    .line 2391
    .local v2, "btnMode":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 2412
    :goto_1
    if-eqz v1, :cond_0

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .end local v1    # "btn":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getButtonType()I

    move-result v4

    if-ne v4, v2, :cond_0

    goto :goto_0

    .line 2393
    .restart local v1    # "btn":Landroid/widget/LinearLayout;
    :sswitch_0
    const v2, 0x1000100a

    .line 2394
    goto :goto_1

    .line 2396
    :sswitch_1
    const v2, 0x10001004

    .line 2397
    goto :goto_1

    .line 2399
    :sswitch_2
    const v2, 0x10001005

    .line 2400
    goto :goto_1

    .line 2402
    :sswitch_3
    const v2, 0x10001006

    .line 2403
    goto :goto_1

    .line 2405
    :sswitch_4
    const v2, 0x1000100b

    .line 2406
    goto :goto_1

    .line 2409
    :sswitch_5
    const/high16 v2, 0x1e110000

    goto :goto_1

    .line 2391
    nop

    :sswitch_data_0
    .sparse-switch
        0x11000000 -> :sswitch_0
        0x15000000 -> :sswitch_1
        0x16000000 -> :sswitch_2
        0x18000000 -> :sswitch_3
        0x1e110000 -> :sswitch_5
        0x2c000000 -> :sswitch_5
        0x31000000 -> :sswitch_4
    .end sparse-switch
.end method

.method public show(Z)V
    .locals 9
    .param p1, "animate"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 392
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    if-eqz v3, :cond_0

    .line 394
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$7;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 402
    if-eqz p1, :cond_1

    .line 404
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .line 406
    const/high16 v6, 0x40800000    # 4.0f

    move v3, v1

    move v4, v2

    move v5, v1

    move v7, v1

    move v8, v2

    .line 404
    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 407
    .local v0, "animation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 408
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsShowing:Z

    .line 409
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$8;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 435
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$9;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Landroid/view/animation/Animation;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 454
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_0
    :goto_0
    return-void

    .line 450
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    if-eqz v1, :cond_0

    .line 451
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public showArrow(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 2589
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->showArrow(Z)V

    .line 2590
    return-void
.end method

.method public showSubBottomButton(I)V
    .locals 3
    .param p1, "viewWidth"    # I

    .prologue
    .line 807
    const-string v1, "sj"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->whoIsCallMe(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 808
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->initBottomButtonWithIcon(II)V

    .line 809
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getCurrentModeButtonId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonLeft(I)I

    move-result v0

    .line 810
    .local v0, "left":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->adjustSubmenuBarPosition(II)V

    .line 811
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewAnimate()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 812
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->subMenuAnimate()V

    .line 814
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->moveArrowToCurrentViewBtn()V

    .line 815
    return-void
.end method

.method public showSubBottomButton(II)V
    .locals 3
    .param p1, "viewWidth"    # I
    .param p2, "collageImgNum"    # I

    .prologue
    .line 818
    const-string v1, "sj"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->whoIsCallMe(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 819
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->initBottomButtonWithIcon(II)V

    .line 820
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getCurrentModeButtonId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonLeft(I)I

    move-result v0

    .line 821
    .local v0, "left":I
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "kbr : collage showSubBottomButton left : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 822
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->adjustSubmenuBarPosition(II)V

    .line 823
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewAnimate()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 825
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->subMenuAnimate()V

    .line 827
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->moveArrowToCurrentViewBtn()V

    .line 828
    return-void
.end method

.method public showSubBottomThumbnailButton(I[III)V
    .locals 3
    .param p1, "viewWidth"    # I
    .param p2, "image"    # [I
    .param p3, "imageWidth"    # I
    .param p4, "imageHeight"    # I

    .prologue
    .line 843
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->initBottomButtonWithThumbnail(I[III)V

    .line 845
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getCurrentModeButtonId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonLeft(I)I

    move-result v0

    .line 846
    .local v0, "left":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->adjustSubmenuBarPosition(II)V

    .line 847
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewAnimate()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 849
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->subMenuAnimate()V

    .line 851
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->moveArrowToCurrentViewBtn()V

    .line 852
    return-void
.end method

.method public unableRedo()V
    .locals 4

    .prologue
    .line 2666
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const v2, 0x1a001845

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .line 2667
    .local v0, "redo":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    if-eqz v0, :cond_0

    .line 2670
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2671
    const/4 v2, 0x0

    .line 2672
    const/4 v3, 0x0

    .line 2669
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->PenUndoRedoEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 2674
    :cond_0
    return-void
.end method

.method public unableUndo()V
    .locals 4

    .prologue
    .line 2644
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const v2, 0x1a001844

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .line 2645
    .local v0, "undo":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    if-eqz v0, :cond_0

    .line 2648
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2649
    const/4 v2, 0x0

    .line 2650
    const/4 v3, 0x0

    .line 2647
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->PenUndoRedoEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 2652
    :cond_0
    return-void
.end method

.method public updateAnimation()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2796
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 2798
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2799
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2803
    :cond_0
    return-void
.end method
