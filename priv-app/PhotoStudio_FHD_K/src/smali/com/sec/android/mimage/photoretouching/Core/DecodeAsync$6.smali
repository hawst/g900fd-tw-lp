.class Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;
.super Ljava/lang/Object;
.source "DecodeAsync.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    return-object v0
.end method


# virtual methods
.method public finishTask(I)V
    .locals 2
    .param p1, "idx"    # I

    .prologue
    .line 187
    const/4 v0, -0x3

    if-ne p1, v0, :cond_1

    .line 189
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->checkTaskFinish(I)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFrom:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$2(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    .line 208
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$13(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 210
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$2;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 218
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$14(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 220
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$3;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 243
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFrom:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$2(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 245
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v0, :cond_4

    .line 247
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$4;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6$4;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 257
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$5(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->access$5(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;->invalidateViews()V

    goto :goto_0
.end method
