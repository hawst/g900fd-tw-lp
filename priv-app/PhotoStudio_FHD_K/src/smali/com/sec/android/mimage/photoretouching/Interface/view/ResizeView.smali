.class public Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "ResizeView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# instance fields
.field private final REDOALL_DIALOG:I

.field private final UNDOALL_DIALOG:I

.field private final drawRunner:Ljava/lang/Runnable;

.field i:I

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mContext:Landroid/content/Context;

.field private mDarkBGBitmap:Landroid/graphics/Bitmap;

.field private mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mHandler:Landroid/os/Handler;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mPaint:Landroid/graphics/Paint;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 275
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->i:I

    .line 276
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->drawRunner:Ljava/lang/Runnable;

    .line 711
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 713
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 714
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mContext:Landroid/content/Context;

    .line 715
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 716
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 717
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 718
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 719
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 720
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 722
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 723
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDarkBGBitmap:Landroid/graphics/Bitmap;

    .line 724
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mPaint:Landroid/graphics/Paint;

    .line 726
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    .line 728
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->UNDOALL_DIALOG:I

    .line 729
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->REDOALL_DIALOG:I

    .line 730
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mHandler:Landroid/os/Handler;

    .line 78
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mContext:Landroid/content/Context;

    .line 79
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 80
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 81
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 82
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 83
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mHandler:Landroid/os/Handler;

    .line 84
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->setInterface(Ljava/lang/Object;)V

    .line 86
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mPaint:Landroid/graphics/Paint;

    .line 87
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 90
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 91
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 94
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 97
    .local v0, "supMatrix":Landroid/graphics/Matrix;
    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 102
    .end local v0    # "supMatrix":Landroid/graphics/Matrix;
    :cond_0
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v1, v2, p1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    .line 103
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$2;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->setOnCallback(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnResizeCallback;)V

    .line 110
    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->setViewLayerType(I)V

    .line 111
    const-string v1, "resize view"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;
    .locals 1

    .prologue
    .line 726
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 730
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->drawRunner:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 714
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V
    .locals 0

    .prologue
    .line 673
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->doDone()V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 711
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 711
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 718
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method private checkRectSizeChanged()V
    .locals 9

    .prologue
    const v8, 0x11301206

    .line 444
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getSelectedButton()Landroid/widget/LinearLayout;

    move-result-object v2

    .line 445
    .local v2, "selectedButton":Landroid/widget/LinearLayout;
    const/4 v0, 0x0

    .line 446
    .local v0, "isChanged":Z
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    if-eq v3, v8, :cond_0

    .line 448
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    if-eqz v3, :cond_0

    .line 450
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->getCalculatedScale()F

    move-result v1

    .line 451
    .local v1, "scale":F
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 473
    .end local v1    # "scale":F
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 474
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v4, 0x1

    invoke-virtual {v3, v8, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    .line 475
    :cond_1
    return-void

    .line 453
    .restart local v1    # "scale":F
    :pswitch_0
    float-to-double v4, v1

    const-wide v6, 0x3fb70a3d70a3d70aL    # 0.09

    cmpg-double v3, v4, v6

    if-ltz v3, :cond_2

    float-to-double v4, v1

    const-wide v6, 0x3fbc28f5c28f5c29L    # 0.11

    cmpl-double v3, v4, v6

    if-lez v3, :cond_0

    .line 454
    :cond_2
    const/4 v0, 0x1

    .line 455
    goto :goto_0

    .line 457
    :pswitch_1
    float-to-double v4, v1

    const-wide v6, 0x3fceb851eb851eb8L    # 0.24

    cmpg-double v3, v4, v6

    if-ltz v3, :cond_3

    float-to-double v4, v1

    const-wide v6, 0x3fd0a3d70a3d70a4L    # 0.26

    cmpl-double v3, v4, v6

    if-lez v3, :cond_0

    .line 458
    :cond_3
    const/4 v0, 0x1

    .line 459
    goto :goto_0

    .line 461
    :pswitch_2
    float-to-double v4, v1

    const-wide v6, 0x3fdf5c28f5c28f5cL    # 0.49

    cmpg-double v3, v4, v6

    if-ltz v3, :cond_4

    float-to-double v4, v1

    const-wide v6, 0x3fe051eb851eb852L    # 0.51

    cmpl-double v3, v4, v6

    if-lez v3, :cond_0

    .line 462
    :cond_4
    const/4 v0, 0x1

    .line 463
    goto :goto_0

    .line 465
    :pswitch_3
    float-to-double v4, v1

    const-wide v6, 0x3fe7ae147ae147aeL    # 0.74

    cmpg-double v3, v4, v6

    if-ltz v3, :cond_5

    float-to-double v4, v1

    const-wide v6, 0x3fe851eb851eb852L    # 0.76

    cmpl-double v3, v4, v6

    if-lez v3, :cond_0

    .line 466
    :cond_5
    const/4 v0, 0x1

    .line 467
    goto :goto_0

    .line 451
    :pswitch_data_0
    .packed-switch 0x11301201
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private doDone()V
    .locals 7

    .prologue
    const/high16 v6, 0x42c80000    # 100.0f

    .line 674
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->applyPreview()V

    .line 677
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-direct {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;)V

    .line 678
    .local v4, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->getScaleW()F

    move-result v1

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->getScaleH()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->setScale(FF)V

    .line 679
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 680
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 681
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 682
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getEnhance()Z

    move-result v5

    .line 679
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([IIILcom/sec/android/mimage/photoretouching/Core/EffectInfo;Z)V

    .line 684
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v6

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v6

    if-gez v0, :cond_1

    .line 685
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 688
    :goto_0
    return-void

    .line 687
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x11000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method private initUndoRedoAllDialog()V
    .locals 15

    .prologue
    const v6, 0x103012e

    const/16 v1, 0x1000

    const/16 v14, 0x500

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 598
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 600
    const v3, 0x7f0600a6

    move v4, v2

    .line 598
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 605
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 606
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 607
    const v3, 0x7f06009c

    .line 609
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$11;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    .line 606
    invoke-virtual {v0, v14, v3, v5, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 616
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060007

    .line 617
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$12;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    .line 616
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 624
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060009

    .line 625
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$13;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    .line 624
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 633
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 635
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 636
    const/4 v9, 0x2

    .line 637
    const v10, 0x7f0600a1

    move v8, v1

    move v11, v2

    move-object v12, v5

    move v13, v6

    .line 635
    invoke-virtual/range {v7 .. v13}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 642
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 643
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 644
    const v1, 0x7f0601ce

    .line 646
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$14;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    .line 643
    invoke-virtual {v0, v14, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 653
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 654
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$15;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    .line 653
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 661
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 662
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$16;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$16;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    .line 661
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 670
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 671
    return-void
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 424
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->transformVirtualPreviewCoordinate(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)V

    .line 425
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-virtual {v0, p2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->resizeTouch(Landroid/view/MotionEvent;)Z

    .line 426
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->refreshDone()V

    .line 427
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->checkRectSizeChanged()V

    .line 428
    const/4 v0, 0x1

    return v0
.end method

.method public backPressed()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x11000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 274
    return-void
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 570
    return-void
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 582
    const/4 v0, 0x0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 586
    const/4 v0, 0x0

    return v0
.end method

.method public getStatusHeight()I
    .locals 3

    .prologue
    .line 590
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 591
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 592
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 593
    const/4 v2, 0x0

    return v2
.end method

.method public initActionbar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 294
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 299
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$8;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    .line 296
    invoke-virtual {v0, v4, v2, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 321
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$9;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    .line 318
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 340
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 343
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$10;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    .line 340
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 371
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 372
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 373
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 375
    :cond_0
    return-void
.end method

.method public initButtons()V
    .locals 4

    .prologue
    const v3, 0x11301206

    .line 126
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x11301201

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$3;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x11301202

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$4;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x11301203

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$5;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x11301204

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$6;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$7;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 267
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->resizeButtonTouch(I)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    .line 269
    return-void
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 383
    :cond_0
    return-void
.end method

.method public initEffect()V
    .locals 0

    .prologue
    .line 117
    return-void
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 410
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 558
    return-void
.end method

.method public initTrayLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 561
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 565
    :cond_0
    return-void
.end method

.method public initView()V
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 122
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 505
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 506
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 520
    :goto_0
    return-void

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_1

    .line 514
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 515
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 516
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 518
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 480
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mContext:Landroid/content/Context;

    .line 481
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 482
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mPaint:Landroid/graphics/Paint;

    .line 484
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 485
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 487
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeCancelPressState(Z)V

    .line 489
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 490
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 491
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 492
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->destroy()V

    .line 493
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    .line 494
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 497
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 498
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 501
    :cond_0
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 387
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 395
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 396
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mPaint:Landroid/graphics/Paint;

    .line 393
    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 398
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDarkBGBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 400
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->resizeDraw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->showResizeText(Landroid/graphics/Canvas;)V

    .line 404
    :cond_0
    return-void
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 523
    const/16 v0, 0x17

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-ne p1, v0, :cond_3

    .line 525
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 526
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    .line 529
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_2

    .line 530
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 552
    :cond_2
    :goto_0
    return v1

    .line 534
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 536
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->backPressed()V

    goto :goto_0
.end method

.method public onLayout()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 692
    const-string v0, "bigheadk, onLayout()"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 693
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 694
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 697
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 698
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 700
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 703
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 704
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 698
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 706
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDarkBGBitmap:Landroid/graphics/Bitmap;

    .line 707
    new-instance v8, Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDarkBGBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v8, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 708
    .local v8, "c":Landroid/graphics/Canvas;
    const/high16 v0, -0x80000000

    invoke-virtual {v8, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 709
    return-void
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 579
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 574
    return-void
.end method

.method public refreshDone()V
    .locals 2

    .prologue
    .line 431
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->getCalculatedScale()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 436
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 441
    :cond_0
    :goto_0
    return-void

    .line 438
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    goto :goto_0
.end method

.method public refreshView()V
    .locals 0

    .prologue
    .line 735
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 1

    .prologue
    .line 415
    const-string v0, "bigheadk, setConfigurationChanged()"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 416
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 417
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 418
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 419
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 420
    return-void
.end method
