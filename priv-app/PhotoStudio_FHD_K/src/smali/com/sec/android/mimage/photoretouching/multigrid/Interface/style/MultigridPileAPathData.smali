.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;
.super Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;
.source "MultigridPileAPathData.java"


# static fields
.field static mCurrentPreset:[[[F

.field static mDrawPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation
.end field

.field private static final mPilePreset2_1to1:[[[F

.field private static final mPilePreset2_9to16:[[[F

.field private static final mPilePreset3_1to1:[[[F

.field private static final mPilePreset3_9to16:[[[F

.field private static final mPilePreset4_1to1:[[[F

.field private static final mPilePreset4_9to16:[[[F

.field private static final mPilePreset5_1to1:[[[F

.field private static final mPilePreset5_9to16:[[[F

.field private static final mPilePreset6_1to1:[[[F

.field private static final mPilePreset6_9to16:[[[F


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 15
    new-array v0, v4, [[[F

    .line 16
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_0

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_1

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_2

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_3

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 17
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_4

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_5

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_6

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_7

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 14
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset2_1to1:[[[F

    .line 21
    new-array v0, v7, [[[F

    .line 22
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_8

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_9

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_b

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 23
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_f

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 24
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_10

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_11

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_12

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_13

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 20
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset3_1to1:[[[F

    .line 28
    new-array v0, v8, [[[F

    .line 29
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_14

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_15

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_16

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_17

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 30
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_18

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_19

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_1a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_1b

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 31
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_1c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_1d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_1e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_1f

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 32
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_20

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_21

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_22

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_23

    aput-object v2, v1, v7

    aput-object v1, v0, v7

    .line 27
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset4_1to1:[[[F

    .line 36
    const/4 v0, 0x5

    new-array v0, v0, [[[F

    .line 37
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_24

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_25

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_26

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_27

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 38
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_28

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_29

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_2a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_2b

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 39
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_2c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_2d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_2e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_2f

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 40
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_30

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_31

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_32

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_33

    aput-object v2, v1, v7

    aput-object v1, v0, v7

    .line 41
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_34

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_35

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_36

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_37

    aput-object v2, v1, v7

    aput-object v1, v0, v8

    .line 35
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset5_1to1:[[[F

    .line 45
    const/4 v0, 0x6

    new-array v0, v0, [[[F

    .line 46
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_38

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_39

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_3a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_3b

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 47
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_3c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_3d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_3e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_3f

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 48
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_40

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_41

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_42

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_43

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 49
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_44

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_45

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_46

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_47

    aput-object v2, v1, v7

    aput-object v1, v0, v7

    .line 50
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_48

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_49

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_4a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_4b

    aput-object v2, v1, v7

    aput-object v1, v0, v8

    const/4 v1, 0x5

    .line 51
    new-array v2, v8, [[F

    new-array v3, v4, [F

    fill-array-data v3, :array_4c

    aput-object v3, v2, v5

    new-array v3, v4, [F

    fill-array-data v3, :array_4d

    aput-object v3, v2, v6

    new-array v3, v4, [F

    fill-array-data v3, :array_4e

    aput-object v3, v2, v4

    new-array v3, v4, [F

    fill-array-data v3, :array_4f

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    .line 44
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset6_1to1:[[[F

    .line 55
    new-array v0, v4, [[[F

    .line 56
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_50

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_51

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_52

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_53

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 57
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_54

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_55

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_56

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_57

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 54
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset2_9to16:[[[F

    .line 61
    new-array v0, v7, [[[F

    .line 62
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_58

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_59

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_5a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_5b

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 63
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_5c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_5d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_5e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_5f

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 64
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_60

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_61

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_62

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_63

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 60
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset3_9to16:[[[F

    .line 68
    new-array v0, v8, [[[F

    .line 69
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_64

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_65

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_66

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_67

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 70
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_68

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_69

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_6a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_6b

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 71
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_6c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_6d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_6e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_6f

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 72
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_70

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_71

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_72

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_73

    aput-object v2, v1, v7

    aput-object v1, v0, v7

    .line 67
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset4_9to16:[[[F

    .line 76
    const/4 v0, 0x5

    new-array v0, v0, [[[F

    .line 77
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_74

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_75

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_76

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_77

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 78
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_78

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_79

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_7a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_7b

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 79
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_7c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_7d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_7e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_7f

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 80
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_80

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_81

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_82

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_83

    aput-object v2, v1, v7

    aput-object v1, v0, v7

    .line 81
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_84

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_85

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_86

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_87

    aput-object v2, v1, v7

    aput-object v1, v0, v8

    .line 75
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset5_9to16:[[[F

    .line 85
    const/4 v0, 0x6

    new-array v0, v0, [[[F

    .line 86
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_88

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_89

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_8a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_8b

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    .line 87
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_8c

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_8d

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_8e

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_8f

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    .line 88
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_90

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_91

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_92

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_93

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    .line 89
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_94

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_95

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_96

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_97

    aput-object v2, v1, v7

    aput-object v1, v0, v7

    .line 90
    new-array v1, v8, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_98

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_99

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_9a

    aput-object v2, v1, v4

    new-array v2, v4, [F

    fill-array-data v2, :array_9b

    aput-object v2, v1, v7

    aput-object v1, v0, v8

    const/4 v1, 0x5

    .line 91
    new-array v2, v8, [[F

    new-array v3, v4, [F

    fill-array-data v3, :array_9c

    aput-object v3, v2, v5

    new-array v3, v4, [F

    fill-array-data v3, :array_9d

    aput-object v3, v2, v6

    new-array v3, v4, [F

    fill-array-data v3, :array_9e

    aput-object v3, v2, v4

    new-array v3, v4, [F

    fill-array-data v3, :array_9f

    aput-object v3, v2, v7

    aput-object v2, v0, v1

    .line 84
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset6_9to16:[[[F

    .line 208
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    .line 209
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mDrawPaths:Ljava/util/ArrayList;

    return-void

    .line 16
    :array_0
    .array-data 4
        0x3f14a234    # 0.5806f
        0x3d4ccccd    # 0.05f
    .end array-data

    :array_1
    .array-data 4
        0x3f8aa993    # 1.0833f
        0x3e4faace    # 0.2028f
    .end array-data

    :array_2
    .array-data 4
        0x3f621ff3    # 0.8833f
        0x3f5b089a    # 0.8556f
    .end array-data

    :array_3
    .array-data 4
        0x3ec2de01    # 0.3806f
        0x3f33eab3    # 0.7028f
    .end array-data

    .line 17
    :array_4
    .array-data 4
        -0x4271de6a    # -0.0694f
        0x3e89eecc    # 0.2694f
    .end array-data

    :array_5
    .array-data 4
        0x3ee22681    # 0.4417f
        0x3e16bb99    # 0.1472f
    .end array-data

    :array_6
    .array-data 4
        0x3f1a511a    # 0.6028f
        0x3f4eecc0    # 0.8083f
    .end array-data

    :array_7
    .array-data 4
        0x3dbbcd36    # 0.0917f
        0x3f6eecc0    # 0.9333f
    .end array-data

    .line 22
    :array_8
    .array-data 4
        0x3ef61134    # 0.4806f
        -0x43333333    # -0.025f
    .end array-data

    :array_9
    .array-data 4
        0x3f688659    # 0.9083f
        0x3c63bcd3    # 0.0139f
    .end array-data

    :array_a
    .array-data 4
        0x3f5b089a    # 0.8556f
        0x3f127bb3    # 0.5722f
    .end array-data

    :array_b
    .array-data 4
        0x3edb089a    # 0.4278f
        0x3f088659    # 0.5333f
    .end array-data

    .line 23
    :array_c
    .array-data 4
        0x3e16bb99    # 0.1472f
        0x3d63bcd3    # 0.0556f
    .end array-data

    :array_d
    .array-data 4
        0x3f0fa440    # 0.5611f
        0x3e305532    # 0.1722f
    .end array-data

    :array_e
    .array-data 4
        0x3ed10cb3    # 0.4083f
        0x3f360aa6    # 0.7111f
    .end array-data

    :array_f
    .array-data 4
        -0x44487fcc    # -0.0056f
        0x3f182a99    # 0.5944f
    .end array-data

    .line 24
    :array_10
    .array-data 4
        0x3e7a43fe    # 0.2444f
        0x3f182a99    # 0.5944f
    .end array-data

    :array_11
    .array-data 4
        0x3f4ccccd    # 0.8f
        0x3f04f766    # 0.5194f
    .end array-data

    :array_12
    .array-data 4
        0x3f5bb98c    # 0.8583f
        0x3f71c433    # 0.9444f
    .end array-data

    :array_13
    .array-data 4
        0x3e9dd97f    # 0.3083f
        0x3f827bb3    # 1.0194f
    .end array-data

    .line 29
    :array_14
    .array-data 4
        0x3de38866    # 0.1111f
        -0x434a233a    # -0.0222f
    .end array-data

    :array_15
    .array-data 4
        0x3ef4a234    # 0.4778f
        -0x422d7732    # -0.1028f
    .end array-data

    :array_16
    .array-data 4
        0x3f160aa6    # 0.5861f
        0x3ec16f00    # 0.3778f
    .end array-data

    :array_17
    .array-data 4
        0x3e5de69b    # 0.2167f
        0x3eeaa64c    # 0.4583f
    .end array-data

    .line 30
    :array_18
    .array-data 4
        0x3f133333    # 0.575f
        0x3e000000    # 0.125f
    .end array-data

    :array_19
    .array-data 4
        0x3f838ef3    # 1.0278f
        0x3e7d21ff    # 0.2472f
    .end array-data

    :array_1a
    .array-data 4
        0x3f5de00d    # 0.8667f
        0x3f56c227    # 0.8389f
    .end array-data

    :array_1b
    .array-data 4
        0x3ed27bb3    # 0.4111f
        0x3f36c227    # 0.7139f
    .end array-data

    .line 31
    :array_1c
    .array-data 4
        -0x43333333    # -0.025f
        0x3ebbc01a    # 0.3667f
    .end array-data

    :array_1d
    .array-data 4
        0x3ec43fe6    # 0.3833f
        0x3e800000    # 0.25f
    .end array-data

    :array_1e
    .array-data 4
        0x3f088659    # 0.5333f
        0x3f471de7    # 0.7778f
    .end array-data

    :array_1f
    .array-data 4
        0x3e02de01    # 0.1278f
        0x3f64f766    # 0.8944f
    .end array-data

    .line 32
    :array_20
    .array-data 4
        0x3edc779a    # 0.4306f
        0x3f1c710d    # 0.6111f
    .end array-data

    :array_21
    .array-data 4
        0x3f6fa440    # 0.9361f
        0x3f288659    # 0.6583f
    .end array-data

    :array_22
    .array-data 4
        0x3f666666    # 0.9f
        0x3f860aa6    # 1.0472f
    .end array-data

    :array_23
    .array-data 4
        0x3ec9eecc    # 0.3944f
        0x3f805bc0    # 1.0028f
    .end array-data

    .line 37
    :array_24
    .array-data 4
        0x3de94467    # 0.1139f
        0x3dd844d0    # 0.1056f
    .end array-data

    :array_25
    .array-data 4
        0x3ee0b780    # 0.4389f
        -0x42b33333    # -0.05f
    .end array-data

    :array_26
    .array-data 4
        0x3f244674    # 0.6417f
        0x3ebd21ff    # 0.3694f
    .end array-data

    :array_27
    .array-data 4
        0x3ea38866    # 0.3194f
        0x3f071de7    # 0.5278f
    .end array-data

    .line 38
    :array_28
    .array-data 4
        0x3f182a99    # 0.5944f
        0x3d086595    # 0.0333f
    .end array-data

    :array_29
    .array-data 4
        0x3f7c710d    # 0.9861f
        0x3dcccccd    # 0.1f
    .end array-data

    :array_2a
    .array-data 4
        0x3f666666    # 0.9f
        0x3f1b089a    # 0.6056f
    .end array-data

    :array_2b
    .array-data 4
        0x3f02d773    # 0.5111f
        0x3f0aacda    # 0.5417f
    .end array-data

    .line 39
    :array_2c
    .array-data 4
        0x3d4154ca    # 0.0472f
        0x3f133333    # 0.575f
    .end array-data

    :array_2d
    .array-data 4
        0x3ed3eab3    # 0.4139f
        0x3ef33333    # 0.475f
    .end array-data

    :array_2e
    .array-data 4
        0x3f0b5dcc    # 0.5444f
        0x3f733333    # 0.95f
    .end array-data

    :array_2f
    .array-data 4
        0x3e3e9100    # 0.1861f
        0x3f866666    # 1.05f
    .end array-data

    .line 40
    :array_30
    .array-data 4
        0x3f0e3bcd    # 0.5556f
        0x3f13eab3    # 0.5778f
    .end array-data

    :array_31
    .array-data 4
        0x3f7f4880    # 0.9972f
        0x3f200000    # 0.625f
    .end array-data

    :array_32
    .array-data 4
        0x3f760aa6    # 0.9611f
        0x3f7779a7    # 0.9667f
    .end array-data

    :array_33
    .array-data 4
        0x3f044674    # 0.5167f
        0x3f6aacda    # 0.9167f
    .end array-data

    .line 41
    :array_34
    .array-data 4
        0x3e8ccccd    # 0.275f
        0x3e82de01    # 0.2556f
    .end array-data

    :array_35
    .array-data 4
        0x3f3f4880    # 0.7472f
        0x3e3e9100    # 0.1861f
    .end array-data

    :array_36
    .array-data 4
        0x3f4d844d    # 0.8028f
        0x3f0ccccd    # 0.55f
    .end array-data

    :array_37
    .array-data 4
        0x3ea66666    # 0.325f
        0x3f1de00d    # 0.6167f
    .end array-data

    .line 46
    :array_38
    .array-data 4
        0x3d1f559b    # 0.0389f
        0x0
    .end array-data

    :array_39
    .array-data 4
        0x3ed82a99    # 0.4222f
        0x3dc154ca    # 0.0944f
    .end array-data

    :array_3a
    .array-data 4
        0x3e99999a    # 0.3f
        0x3f182a99    # 0.5944f
    .end array-data

    :array_3b
    .array-data 4
        -0x425566cf    # -0.0833f
        0x3efd21ff    # 0.4944f
    .end array-data

    .line 47
    :array_3c
    .array-data 4
        0x3f305bc0    # 0.6889f
        0x3e4710cb    # 0.1944f
    .end array-data

    :array_3d
    .array-data 4
        0x3f849eed    # 1.0361f
        0x3dd844d0    # 0.1056f
    .end array-data

    :array_3e
    .array-data 4
        0x3f938ef3    # 1.1528f
        0x3f0fa440    # 0.5611f
    .end array-data

    :array_3f
    .array-data 4
        0x3f4e3bcd    # 0.8056f
        0x3f266666    # 0.65f
    .end array-data

    .line 48
    :array_40
    .array-data 4
        0x3ea7d567    # 0.3278f
        -0x44c87fcc    # -0.0028f
    .end array-data

    :array_41
    .array-data 4
        0x3f4e3bcd    # 0.8056f
        -0x42beab36    # -0.0472f
    .end array-data

    :array_42
    .array-data 4
        0x3f5779a7    # 0.8417f
        0x3ea38866    # 0.3194f
    .end array-data

    :array_43
    .array-data 4
        0x3eba511a    # 0.3639f
        0x3eba511a    # 0.3639f
    .end array-data

    .line 49
    :array_44
    .array-data 4
        -0x427765fe    # -0.0667f
        0x3f2e3bcd    # 0.6806f
    .end array-data

    :array_45
    .array-data 4
        0x3e8ccccd    # 0.275f
        0x3efbc01a    # 0.4917f
    .end array-data

    :array_46
    .array-data 4
        0x3f071de7    # 0.5278f
        0x3f6fa440    # 0.9361f
    .end array-data

    :array_47
    .array-data 4
        0x3e416f00    # 0.1889f
        0x3f8fa440    # 1.1222f
    .end array-data

    .line 50
    :array_48
    .array-data 4
        0x3f071de7    # 0.5278f
        0x3f2eecc0    # 0.6833f
    .end array-data

    :array_49
    .array-data 4
        0x3f7a511a    # 0.9778f
        0x3f182a99    # 0.5944f
    .end array-data

    :array_4a
    .array-data 4
        0x3f860aa6    # 1.0472f
        0x3f711340    # 0.9417f
    .end array-data

    :array_4b
    .array-data 4
        0x3f182a99    # 0.5944f
        0x3f83eab3    # 1.0306f
    .end array-data

    .line 51
    :array_4c
    .array-data 4
        0x3e85aee6    # 0.2611f
        0x3eaef34d    # 0.3417f
    .end array-data

    :array_4d
    .array-data 4
        0x3f3f4880    # 0.7472f
        0x3ec71de7    # 0.3889f
    .end array-data

    :array_4e
    .array-data 4
        0x3f360aa6    # 0.7111f
        0x3f42d773    # 0.7611f
    .end array-data

    :array_4f
    .array-data 4
        0x3e666666    # 0.225f
        0x3f36c227    # 0.7139f
    .end array-data

    .line 56
    :array_50
    .array-data 4
        0x3f09f55a    # 0.5389f
        0x3ea594af    # 0.3234f
    .end array-data

    :array_51
    .array-data 4
        0x3f99f55a    # 1.2028f
        0x3ed73190    # 0.4203f
    .end array-data

    :array_52
    .array-data 4
        0x3f79999a    # 0.975f
        0x3f679a6b    # 0.9047f
    .end array-data

    :array_53
    .array-data 4
        0x3e9f4880    # 0.3111f
        0x3f4e69ad    # 0.8063f
    .end array-data

    .line 57
    :array_54
    .array-data 4
        -0x4205bc02    # -0.1222f
        0x3e032ca5    # 0.1281f
    .end array-data

    :array_55
    .array-data 4
        0x3f0d844d    # 0.5528f
        0x3d734d6a    # 0.0594f
    .end array-data

    :array_56
    .array-data 4
        0x3f36c227    # 0.7139f
        0x3f0d35a8    # 0.5516f
    .end array-data

    :array_57
    .array-data 4
        0x3d1f559b    # 0.0389f
        0x3f1ecbfb    # 0.6203f
    .end array-data

    .line 62
    :array_58
    .array-data 4
        0x3ec2de01    # 0.3806f
        0x3e99999a    # 0.3f
    .end array-data

    :array_59
    .array-data 4
        0x3f6d844d    # 0.9278f
        0x3e84d014    # 0.2594f
    .end array-data

    :array_5a
    .array-data 4
        0x3f82d773    # 1.0222f
        0x3f280347    # 0.6563f
    .end array-data

    :array_5b
    .array-data 4
        0x3ef33333    # 0.475f
        0x3f32ca58    # 0.6984f
    .end array-data

    .line 63
    :array_5c
    .array-data 4
        0x3dcccccd    # 0.1f
        0x3d06594b    # 0.0328f
    .end array-data

    :array_5d
    .array-data 4
        0x3f216f00    # 0.6306f
        0x3decbfb1    # 0.1156f
    .end array-data

    :array_5e
    .array-data 4
        0x3edf4880    # 0.4361f
        0x3f00cb29    # 0.5031f
    .end array-data

    :array_5f
    .array-data 4
        -0x423eab36    # -0.0944f
        0x3ed66cf4    # 0.4188f
    .end array-data

    .line 64
    :array_60
    .array-data 4
        0x3e087fcc    # 0.1333f
        0x3f300000    # 0.6875f
    .end array-data

    :array_61
    .array-data 4
        0x3f421ff3    # 0.7583f
        0x3f246738    # 0.6422f
    .end array-data

    :array_62
    .array-data 4
        0x3f51c433    # 0.8194f
        0x3f69999a    # 0.9125f
    .end array-data

    :array_63
    .array-data 4
        0x3e4710cb    # 0.1944f
        0x3f753261    # 0.9578f
    .end array-data

    .line 69
    :array_64
    .array-data 4
        -0x42ca233a    # -0.0444f
        0x3d669ad4    # 0.0563f
    .end array-data

    :array_65
    .array-data 4
        0x3ee22681    # 0.4417f
        -0x4465fd8b    # -0.0047f
    .end array-data

    :array_66
    .array-data 4
        0x3f160aa6    # 0.5861f
        0x3eb404ea    # 0.3516f
    .end array-data

    :array_67
    .array-data 4
        0x3dc710cb    # 0.0972f
        0x3ed404ea    # 0.4141f
    .end array-data

    .line 70
    :array_68
    .array-data 4
        0x3f1de00d    # 0.6167f
        0x3e365fd9    # 0.1781f
    .end array-data

    :array_69
    .array-data 4
        0x3f92233a    # 1.1417f
        0x3e84d014    # 0.2594f
    .end array-data

    :array_6a
    .array-data 4
        0x3f73eab3    # 0.9528f
        0x3f253261    # 0.6453f
    .end array-data

    :array_6b
    .array-data 4
        0x3edb089a    # 0.4278f
        0x3f0f9724    # 0.5609f
    .end array-data

    .line 71
    :array_6c
    .array-data 4
        -0x42913405    # -0.0583f
        0x3ee19653    # 0.4406f
    .end array-data

    :array_6d
    .array-data 4
        0x3ed9999a    # 0.425f
        0x3eb8c7e3    # 0.3609f
    .end array-data

    :array_6e
    .array-data 4
        0x3f1e9100    # 0.6194f
        0x3f36cf42    # 0.7141f
    .end array-data

    :array_6f
    .array-data 4
        0x3e087fcc    # 0.1333f
        0x3f4c01a3    # 0.7969f
    .end array-data

    .line 72
    :array_70
    .array-data 4
        0x3ec00000    # 0.375f
        0x3f2d97f6    # 0.6781f
    .end array-data

    :array_71
    .array-data 4
        0x3f782a99    # 0.9694f
        0x3f366666    # 0.7125f
    .end array-data

    :array_72
    .array-data 4
        0x3f6e3bcd    # 0.9306f
        0x3f7930be    # 0.9734f
    .end array-data

    :array_73
    .array-data 4
        0x3ea7d567    # 0.3278f
        0x3f7068dc    # 0.9391f
    .end array-data

    .line 77
    :array_74
    .array-data 4
        0x3f105bc0    # 0.5639f
        0x3e200d1b    # 0.1563f
    .end array-data

    :array_75
    .array-data 4
        0x3f882de0    # 1.0639f
        0x3e519ce0    # 0.2047f
    .end array-data

    :array_76
    .array-data 4
        0x3f73eab3    # 0.9528f
        0x3f12680a    # 0.5719f
    .end array-data

    :array_77
    .array-data 4
        0x3ee7d567    # 0.4528f
        0x3f05fd8b    # 0.5234f
    .end array-data

    .line 78
    :array_78
    .array-data 4
        0x3d086595    # 0.0333f
        0x3f34d014    # 0.7063f
    .end array-data

    :array_79
    .array-data 4
        0x3f038ef3    # 0.5139f
        0x3f259b3d    # 0.6469f
    .end array-data

    :array_7a
    .array-data 4
        0x3f271de7    # 0.6528f
        0x3f800000    # 1.0f
    .end array-data

    :array_7b
    .array-data 4
        0x3e2d7732    # 0.1694f
        0x3f879a6b    # 1.0594f
    .end array-data

    .line 79
    :array_7c
    .array-data 4
        -0x424432ca    # -0.0917f
        0x3ebcd35b    # 0.3688f
    .end array-data

    :array_7d
    .array-data 4
        0x3ebbc01a    # 0.3667f
        0x3e8d9e84    # 0.2766f
    .end array-data

    :array_7e
    .array-data 4
        0x3f155326    # 0.5833f
        0x3f1ccccd    # 0.6125f
    .end array-data

    :array_7f
    .array-data 4
        0x3e000000    # 0.125f
        0x3f34d014    # 0.7063f
    .end array-data

    .line 80
    :array_80
    .array-data 4
        0x3c9eecc0    # 0.0194f
        0x3d600d1b    # 0.0547f
    .end array-data

    :array_81
    .array-data 4
        0x3f1bb98c    # 0.6083f
        0x3bff9724    # 0.0078f
    .end array-data

    :array_82
    .array-data 4
        0x3f2ccccd    # 0.675f
        0x3e87381d    # 0.2641f
    .end array-data

    :array_83
    .array-data 4
        0x3daa9931    # 0.0833f
        0x3e9f2e49    # 0.3109f
    .end array-data

    .line 81
    :array_84
    .array-data 4
        0x3f066666    # 0.525f
        0x3f11ff2e    # 0.5703f
    .end array-data

    :array_85
    .array-data 4
        0x3f89f55a    # 1.0778f
        0x3f279a6b    # 0.6547f
    .end array-data

    :array_86
    .array-data 4
        0x3f76c227    # 0.9639f
        0x3f659b3d    # 0.8969f
    .end array-data

    :array_87
    .array-data 4
        0x3ed10cb3    # 0.4083f
        0x3f4f9724    # 0.8109f
    .end array-data

    .line 86
    :array_88
    .array-data 4
        -0x43611340    # -0.0194f
        0x3f180347    # 0.5938f
    .end array-data

    :array_89
    .array-data 4
        0x3ee38866    # 0.4444f
        0x3f0b98c8    # 0.5453f
    .end array-data

    :array_8a
    .array-data 4
        0x3f0eecc0    # 0.5583f
        0x3f62ca58    # 0.8859f
    .end array-data

    :array_8b
    .array-data 4
        0x3dc154ca    # 0.0944f
        0x3f6f34d7    # 0.9344f
    .end array-data

    .line 87
    :array_8c
    .array-data 4
        0x3eec154d    # 0.4611f
        0x3dacd9e8    # 0.0844f
    .end array-data

    :array_8d
    .array-data 4
        0x3f73eab3    # 0.9528f
        0x3c6703b0    # 0.0141f
    .end array-data

    :array_8e
    .array-data 4
        0x3f8e9446    # 1.1139f
        0x3ebf2e49    # 0.3734f
    .end array-data

    :array_8f
    .array-data 4
        0x3f1f4880    # 0.6222f
        0x3ee3fe5d    # 0.4453f
    .end array-data

    .line 88
    :array_90
    .array-data 4
        0x3efa511a    # 0.4889f
        0x3ed33333    # 0.4125f
    .end array-data

    :array_91
    .array-data 4
        0x3f8bbcd3    # 1.0917f
        0x3ef404ea    # 0.4766f
    .end array-data

    :array_92
    .array-data 4
        0x3f80b780    # 1.0056f
        0x3f3ccccd    # 0.7375f
    .end array-data

    :array_93
    .array-data 4
        0x3ece3bcd    # 0.4028f
        0x3f2c63f1    # 0.6734f
    .end array-data

    .line 89
    :array_94
    .array-data 4
        -0x425aee63    # -0.0806f
        0x3d8ce704    # 0.0688f
    .end array-data

    :array_95
    .array-data 4
        0x3efe9100    # 0.4972f
        0x3bce703b    # 0.0063f
    .end array-data

    :array_96
    .array-data 4
        0x3f14a234    # 0.5806f
        0x3e8339c1    # 0.2563f
    .end array-data

    :array_97
    .array-data 4
        0x3bb78034    # 0.0056f
        0x3ea339c1    # 0.3188f
    .end array-data

    .line 90
    :array_98
    .array-data 4
        -0x439c432d    # -0.0139f
        0x3eab367a    # 0.3344f
    .end array-data

    :array_99
    .array-data 4
        0x3ecccccd    # 0.4f
        0x3e6809d5    # 0.2266f
    .end array-data

    :array_9a
    .array-data 4
        0x3f266666    # 0.65f
        0x3f079a6b    # 0.5297f
    .end array-data

    :array_9b
    .array-data 4
        0x3e71c433    # 0.2361f
        0x3f22ca58    # 0.6359f
    .end array-data

    .line 91
    :array_9c
    .array-data 4
        0x3ebbc01a    # 0.3667f
        0x3f3d35a8    # 0.7391f
    .end array-data

    :array_9d
    .array-data 4
        0x3f733333    # 0.95f
        0x3f2e00d2    # 0.6797f
    .end array-data

    :array_9e
    .array-data 4
        0x3f83eab3    # 1.0306f
        0x3f6ecbfb    # 0.9328f
    .end array-data

    :array_9f
    .array-data 4
        0x3ee4f766    # 0.4472f
        0x3f7e00d2    # 0.9922f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;-><init>()V

    return-void
.end method

.method public static getPathPoints(ILcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;)[[[F
    .locals 2
    .param p0, "imgCnt"    # I
    .param p1, "proportion"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .prologue
    .line 134
    const/high16 v0, 0x1e200000

    const/4 v1, 0x2

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getMaxStyleNum(I)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    .line 136
    packed-switch p0, :pswitch_data_0

    .line 194
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    .line 198
    :goto_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    return-object v0

    .line 139
    :pswitch_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    if-ne p1, v0, :cond_0

    .line 141
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset2_1to1:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 145
    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset2_9to16:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 150
    :pswitch_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    if-ne p1, v0, :cond_1

    .line 152
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset3_1to1:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 156
    :cond_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset3_9to16:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 161
    :pswitch_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    if-ne p1, v0, :cond_2

    .line 163
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset4_1to1:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 167
    :cond_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset4_9to16:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 172
    :pswitch_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    if-ne p1, v0, :cond_3

    .line 174
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset5_1to1:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 178
    :cond_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset5_9to16:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 183
    :pswitch_4
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    if-ne p1, v0, :cond_4

    .line 185
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset6_1to1:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 189
    :cond_4
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mPilePreset6_9to16:[[[F

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    goto :goto_0

    .line 136
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getPaths(ILcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;Landroid/graphics/Rect;)Ljava/util/ArrayList;
    .locals 13
    .param p0, "imgCnt"    # I
    .param p1, "proportion"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;
    .param p2, "canvasRoi"    # Landroid/graphics/Rect;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;",
            "Landroid/graphics/Rect;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 96
    invoke-static {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->getPathPoints(ILcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;)[[[F

    move-result-object v6

    if-nez v6, :cond_0

    .line 97
    const/4 v6, 0x0

    .line 128
    :goto_0
    return-object v6

    .line 99
    :cond_0
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mDrawPaths:Ljava/util/ArrayList;

    if-nez v6, :cond_1

    .line 101
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    sput-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mDrawPaths:Ljava/util/ArrayList;

    .line 108
    :goto_1
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 109
    .local v5, "viewWidth":I
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v4

    .line 110
    .local v4, "viewHeight":I
    iget v1, p2, Landroid/graphics/Rect;->left:I

    .line 111
    .local v1, "leftPadding":I
    iget v3, p2, Landroid/graphics/Rect;->top:I

    .line 114
    .local v3, "topPadding":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    if-lt v0, p0, :cond_2

    .line 128
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mDrawPaths:Ljava/util/ArrayList;

    goto :goto_0

    .line 105
    .end local v0    # "i":I
    .end local v1    # "leftPadding":I
    .end local v3    # "topPadding":I
    .end local v4    # "viewHeight":I
    .end local v5    # "viewWidth":I
    :cond_1
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mDrawPaths:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 116
    .restart local v0    # "i":I
    .restart local v1    # "leftPadding":I
    .restart local v3    # "topPadding":I
    .restart local v4    # "viewHeight":I
    .restart local v5    # "viewWidth":I
    :cond_2
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 117
    .local v2, "path":Landroid/graphics/Path;
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    aget-object v6, v6, v0

    aget-object v6, v6, v9

    aget v6, v6, v9

    int-to-float v7, v5

    mul-float/2addr v6, v7

    int-to-float v7, v1

    add-float/2addr v6, v7

    sget-object v7, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    aget-object v7, v7, v0

    aget-object v7, v7, v9

    aget v7, v7, v10

    int-to-float v8, v4

    mul-float/2addr v7, v8

    int-to-float v8, v3

    add-float/2addr v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 118
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    aget-object v6, v6, v0

    aget-object v6, v6, v10

    aget v6, v6, v9

    int-to-float v7, v5

    mul-float/2addr v6, v7

    int-to-float v7, v1

    add-float/2addr v6, v7

    sget-object v7, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    aget-object v7, v7, v0

    aget-object v7, v7, v10

    aget v7, v7, v10

    int-to-float v8, v4

    mul-float/2addr v7, v8

    int-to-float v8, v3

    add-float/2addr v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 119
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    aget-object v6, v6, v0

    aget-object v6, v6, v11

    aget v6, v6, v9

    int-to-float v7, v5

    mul-float/2addr v6, v7

    int-to-float v7, v1

    add-float/2addr v6, v7

    sget-object v7, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    aget-object v7, v7, v0

    aget-object v7, v7, v11

    aget v7, v7, v10

    int-to-float v8, v4

    mul-float/2addr v7, v8

    int-to-float v8, v3

    add-float/2addr v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 120
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    aget-object v6, v6, v0

    aget-object v6, v6, v12

    aget v6, v6, v9

    int-to-float v7, v5

    mul-float/2addr v6, v7

    int-to-float v7, v1

    add-float/2addr v6, v7

    sget-object v7, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    aget-object v7, v7, v0

    aget-object v7, v7, v12

    aget v7, v7, v10

    int-to-float v8, v4

    mul-float/2addr v7, v8

    int-to-float v8, v3

    add-float/2addr v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 121
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    aget-object v6, v6, v0

    aget-object v6, v6, v9

    aget v6, v6, v9

    int-to-float v7, v5

    mul-float/2addr v6, v7

    int-to-float v7, v1

    add-float/2addr v6, v7

    sget-object v7, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    aget-object v7, v7, v0

    aget-object v7, v7, v9

    aget v7, v7, v10

    int-to-float v8, v4

    mul-float/2addr v7, v8

    int-to-float v8, v3

    add-float/2addr v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 122
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    aget-object v6, v6, v0

    aget-object v6, v6, v10

    aget v6, v6, v9

    int-to-float v7, v5

    mul-float/2addr v6, v7

    int-to-float v7, v1

    add-float/2addr v6, v7

    sget-object v7, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mCurrentPreset:[[[F

    aget-object v7, v7, v0

    aget-object v7, v7, v10

    aget v7, v7, v10

    int-to-float v8, v4

    mul-float/2addr v7, v8

    int-to-float v8, v3

    add-float/2addr v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 125
    sget-object v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->mDrawPaths:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2
.end method


# virtual methods
.method public getRotate(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 205
    const/4 v0, 0x0

    return v0
.end method
