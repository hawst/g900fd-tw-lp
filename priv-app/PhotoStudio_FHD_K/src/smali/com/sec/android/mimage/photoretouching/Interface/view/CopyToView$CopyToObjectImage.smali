.class public Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;
.super Ljava/lang/Object;
.source "CopyToView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CopyToObjectImage"
.end annotation


# instance fields
.field public objectAvgB:I

.field public objectAvgG:I

.field public objectAvgR:I

.field public objectBitmap:Landroid/graphics/Bitmap;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 22
    .param p2, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    .line 1056
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    .line 1055
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 1050
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectBitmap:Landroid/graphics/Bitmap;

    .line 1051
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgR:I

    .line 1052
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgG:I

    .line 1053
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgB:I

    .line 1057
    const/4 v12, 0x0

    .line 1058
    .local v12, "count":I
    invoke-static/range {p1 .. p2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 1059
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewMatrix()Landroid/graphics/Matrix;

    move-result-object v18

    .line 1060
    .local v18, "matrix":Landroid/graphics/Matrix;
    new-instance v21, Landroid/graphics/Rect;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Rect;-><init>()V

    .line 1061
    .local v21, "viewMaskRoi":Landroid/graphics/Rect;
    new-instance v20, Landroid/graphics/RectF;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 1062
    .local v20, "src":Landroid/graphics/RectF;
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    .line 1063
    .local v13, "dst":Landroid/graphics/RectF;
    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v13, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1065
    iget v2, v13, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v4, v13, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    iget v5, v13, Landroid/graphics/RectF;->right:F

    float-to-int v5, v5

    iget v6, v13, Landroid/graphics/RectF;->bottom:F

    float-to-int v6, v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 1067
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->width()I

    move-result v2

    new-array v3, v2, [I

    .line 1068
    .local v3, "objectData":[I
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->height()I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectBitmap:Landroid/graphics/Bitmap;

    .line 1069
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v16, v0

    .local v16, "j":I
    :goto_0
    move-object/from16 v0, v21

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v0, v16

    if-lt v0, v2, :cond_1

    .line 1103
    if-gtz v12, :cond_0

    .line 1104
    const/4 v12, 0x1

    .line 1106
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgR:I

    div-int/2addr v2, v12

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgR:I

    .line 1107
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgG:I

    div-int/2addr v2, v12

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgG:I

    .line 1108
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgB:I

    div-int/2addr v2, v12

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgB:I

    .line 1110
    const/4 v3, 0x0

    .line 1111
    return-void

    .line 1070
    :cond_1
    move-object/from16 v0, v21

    iget v15, v0, Landroid/graphics/Rect;->left:I

    .local v15, "i":I
    :goto_1
    move-object/from16 v0, v21

    iget v2, v0, Landroid/graphics/Rect;->right:I

    if-lt v15, v2, :cond_2

    .line 1095
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectBitmap:Landroid/graphics/Bitmap;

    .line 1096
    const/4 v4, 0x0

    .line 1097
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 1098
    const/4 v6, 0x0

    .line 1099
    move-object/from16 v0, v21

    iget v7, v0, Landroid/graphics/Rect;->top:I

    sub-int v7, v16, v7

    .line 1100
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->width()I

    move-result v8

    .line 1101
    const/4 v9, 0x1

    .line 1095
    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1069
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 1071
    :cond_2
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskBuffer()[B

    move-result-object v2

    array-length v2, v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    mul-int v4, v4, v16

    add-int/2addr v4, v15

    if-le v2, v4, :cond_3

    .line 1072
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    array-length v2, v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    mul-int v4, v4, v16

    add-int/2addr v4, v15

    if-le v2, v4, :cond_3

    .line 1074
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskBuffer()[B

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    mul-int v4, v4, v16

    add-int/2addr v4, v15

    aget-byte v17, v2, v4

    .line 1075
    .local v17, "maskvalue":B
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    mul-int v4, v4, v16

    add-int/2addr v4, v15

    aget v11, v2, v4

    .line 1077
    .local v11, "color":I
    if-nez v17, :cond_4

    .line 1078
    const v2, 0xffffff

    and-int/2addr v11, v2

    .line 1092
    :goto_2
    move-object/from16 v0, v21

    iget v2, v0, Landroid/graphics/Rect;->left:I

    sub-int v2, v15, v2

    aput v11, v3, v2

    .line 1070
    .end local v11    # "color":I
    .end local v17    # "maskvalue":B
    :cond_3
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_1

    .line 1081
    .restart local v11    # "color":I
    .restart local v17    # "maskvalue":B
    :cond_4
    shr-int/lit8 v2, v11, 0x10

    and-int/lit16 v0, v2, 0xff

    move/from16 v19, v0

    .line 1082
    .local v19, "r":I
    shr-int/lit8 v2, v11, 0x8

    and-int/lit16 v14, v2, 0xff

    .line 1083
    .local v14, "g":I
    and-int/lit16 v10, v11, 0xff

    .line 1085
    .local v10, "b":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgR:I

    add-int v2, v2, v19

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgR:I

    .line 1086
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgG:I

    add-int/2addr v2, v14

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgG:I

    .line 1087
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgB:I

    add-int/2addr v2, v10

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgB:I

    .line 1089
    add-int/lit8 v12, v12, 0x1

    goto :goto_2
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 1115
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1116
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectBitmap:Landroid/graphics/Bitmap;

    .line 1117
    return-void
.end method
