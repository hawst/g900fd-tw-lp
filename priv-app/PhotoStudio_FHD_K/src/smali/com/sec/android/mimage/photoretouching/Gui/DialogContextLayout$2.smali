.class Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;
.super Ljava/lang/Object;
.source "DialogContextLayout.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setBrushSeekBar(Landroid/app/Dialog;IILcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;

.field private final synthetic val$listener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

.field private final synthetic val$max:I

.field private final synthetic val$rIcon:Landroid/widget/LinearLayout;

.field private final synthetic val$textView:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;ILandroid/widget/TextView;Landroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$listener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$max:I

    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$textView:Landroid/widget/TextView;

    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$rIcon:Landroid/widget/LinearLayout;

    .line 339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 355
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ysjeong, max = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$max:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", onProgressChanged, DialogContextLayout"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 356
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$listener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$textView:Landroid/widget/TextView;

    invoke-interface {v4, p1, p2, p3, v5}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;->onMyProgressChanged(Landroid/widget/SeekBar;IZLandroid/widget/TextView;)V

    .line 357
    const/high16 v1, 0x3f800000    # 1.0f

    .line 358
    .local v1, "scale":F
    int-to-float v4, p2

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$max:I

    int-to-float v5, v5

    div-float v2, v4, v5

    .line 362
    .local v2, "temp_progress":F
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$max:I

    div-int/lit8 v3, v4, 0x2

    .line 363
    .local v3, "tmpCal":I
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$max:I

    int-to-float v4, v4

    mul-float v0, v4, v2

    .line 364
    .local v0, "calibration":F
    int-to-float v4, v3

    add-float/2addr v4, v0

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$max:I

    int-to-float v5, v5

    div-float v1, v4, v5

    .line 366
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$rIcon:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_0

    .line 368
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$rIcon:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->setScaleX(F)V

    .line 369
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$rIcon:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->setScaleY(F)V

    .line 371
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 349
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$listener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;->onMyStartTrackingTouch(Landroid/widget/SeekBar;)V

    .line 350
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 344
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;->val$listener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;->onMyStopTrackingTouch(Landroid/widget/SeekBar;)V

    .line 345
    return-void
.end method
