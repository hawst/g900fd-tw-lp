.class public Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
.super Ljava/lang/Object;
.source "ActionBarManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$RecentShareVia;,
        Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$RecentShareViaCallback;
    }
.end annotation


# static fields
.field public static final BRUSH:I = 0xe

.field public static final CANCEL:I = 0x4

.field public static final CCW:I = 0x8

.field public static final CROP_ROTATE:I = 0xf

.field public static final CW:I = 0x7

.field public static final DONE:I = 0x3

.field public static final HFLIP:I = 0x9

.field public static final HOME:I = 0x0

.field public static final MIRROR:I = 0xd

.field public static final NEXT:I = 0x6

.field public static final NO_ICON:I = -0x1

.field public static final NO_TEXT:I = -0x1

.field public static final OPTION:I = 0xc

.field public static final RECENT_SHARE_VIA:I = 0x12

.field public static final REDO:I = 0x2

.field public static final RESIZE:I = 0xb

.field public static final SAVE:I = 0x5

.field public static final SHARE_VIA:I = 0x10

.field public static final SHOW_PREVIOUS:I = 0x11

.field public static final UNDO:I = 0x1

.field public static final VFLIP:I = 0xa


# instance fields
.field private doneState:Z

.field private mAbleButtonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mActionBar:Landroid/app/ActionBar;

.field private mBackState:Z

.field private mButtonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public mButtonListDisabledByPressing:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mCancelState:Z

.field private mContext:Landroid/content/Context;

.field private mCustomLayout:Landroid/widget/LinearLayout;

.field private mFromMultigridiView:Z

.field private mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

.field private mMenu:Landroid/view/Menu;

.field private mRecentShareVia:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$RecentShareVia;

.field private mSaveState:Z

.field private mViewWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 2089
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    .line 2091
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;

    .line 2092
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    .line 2093
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    .line 2094
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    .line 2095
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->doneState:Z

    .line 2096
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mSaveState:Z

    .line 2097
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mBackState:Z

    .line 2098
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCancelState:Z

    .line 2099
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mViewWidth:I

    .line 2124
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    .line 2125
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mRecentShareVia:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$RecentShareVia;

    .line 2126
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    .line 2128
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonListDisabledByPressing:Ljava/util/ArrayList;

    .line 2129
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mFromMultigridiView:Z

    .line 67
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    .line 68
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    .line 69
    const/4 v15, 0x0

    .line 70
    .local v15, "v":Landroid/view/View;
    const/4 v2, 0x0

    .line 72
    .local v2, "ab":Landroid/app/ActionBar;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    check-cast v16, Landroid/app/Activity;

    invoke-virtual/range {v16 .. v16}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v16

    const v17, 0x7f030005

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v15

    .line 75
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    check-cast v16, Landroid/app/Activity;

    invoke-virtual/range {v16 .. v16}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    .line 77
    if-eqz v2, :cond_0

    if-eqz v15, :cond_0

    .line 79
    new-instance v16, Landroid/app/ActionBar$LayoutParams;

    const/16 v17, -0x1

    .line 80
    const/16 v18, -0x1

    invoke-direct/range {v16 .. v18}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    .line 79
    move-object/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 81
    const/16 v16, 0x10

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 84
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    check-cast v16, Landroid/app/Activity;

    const v17, 0x7f090008

    invoke-virtual/range {v16 .. v17}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 85
    .local v9, "home_layout":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    check-cast v16, Landroid/app/Activity;

    invoke-virtual/range {v16 .. v16}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v16

    const v17, 0x7f030004

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 86
    .local v3, "button":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 87
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setId(I)V

    .line 88
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    check-cast v16, Landroid/app/Activity;

    invoke-virtual/range {v16 .. v16}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v16

    const-string v17, "fromStudio"

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 90
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 96
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    new-instance v16, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v17, -0x2

    const/16 v18, -0x1

    invoke-direct/range {v16 .. v18}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, v16

    invoke-virtual {v9, v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 98
    const v16, 0x7f090009

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/LinearLayout;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;

    .line 100
    const/4 v13, 0x0

    .line 101
    .local v13, "layoutId":I
    const-string v16, "sans-serif-regular"

    const/16 v17, 0x0

    invoke-static/range {v16 .. v17}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v8

    .line 102
    .local v8, "font":Landroid/graphics/Typeface;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    check-cast v16, Landroid/app/Activity;

    invoke-virtual/range {v16 .. v16}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v11

    .line 103
    .local v11, "inflater":Landroid/view/LayoutInflater;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    const/16 v16, 0xa

    move/from16 v0, v16

    if-le v10, v0, :cond_2

    .line 214
    const/4 v3, 0x0

    .line 215
    const/4 v2, 0x0

    .line 216
    const/4 v15, 0x0

    .line 218
    .end local v3    # "button":Landroid/widget/LinearLayout;
    .end local v8    # "font":Landroid/graphics/Typeface;
    .end local v9    # "home_layout":Landroid/widget/LinearLayout;
    .end local v10    # "i":I
    .end local v11    # "inflater":Landroid/view/LayoutInflater;
    .end local v13    # "layoutId":I
    :cond_0
    return-void

    .line 94
    .restart local v3    # "button":Landroid/widget/LinearLayout;
    .restart local v9    # "home_layout":Landroid/widget/LinearLayout;
    :cond_1
    const/16 v16, 0x8

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 105
    .restart local v8    # "font":Landroid/graphics/Typeface;
    .restart local v10    # "i":I
    .restart local v11    # "inflater":Landroid/view/LayoutInflater;
    .restart local v13    # "layoutId":I
    :cond_2
    packed-switch v10, :pswitch_data_0

    .line 187
    :goto_2
    const/16 v16, 0x4

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 188
    new-instance v14, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v16, -0x2

    const/16 v17, -0x1

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v14, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 189
    .local v14, "param":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v16, 0x7

    move/from16 v0, v16

    if-eq v10, v0, :cond_3

    .line 191
    const/16 v16, 0x3

    move/from16 v0, v16

    if-ne v10, v0, :cond_5

    .line 193
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v16

    if-nez v16, :cond_3

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 204
    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    const/16 v17, 0x4

    invoke-static/range {v16 .. v17}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v4

    .line 205
    .local v4, "cancel":Landroid/widget/LinearLayout;
    if-eqz v4, :cond_4

    .line 206
    const v16, 0x7f090004

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 207
    .local v12, "iv":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f060009

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v12}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 211
    .end local v12    # "iv":Landroid/widget/ImageView;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f020020

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v14}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 103
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 107
    .end local v4    # "cancel":Landroid/widget/LinearLayout;
    .end local v14    # "param":Landroid/widget/LinearLayout$LayoutParams;
    :pswitch_0
    const v13, 0x7f030007

    .line 108
    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v11, v13, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .end local v3    # "button":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 109
    .restart local v3    # "button":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    const/16 v16, 0xd

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setId(I)V

    goto/16 :goto_2

    .line 113
    :pswitch_1
    const/high16 v13, 0x7f030000

    .line 114
    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v11, v13, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .end local v3    # "button":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 115
    .restart local v3    # "button":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    const/16 v16, 0xe

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setId(I)V

    goto/16 :goto_2

    .line 119
    :pswitch_2
    const v13, 0x7f030002

    .line 120
    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v11, v13, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .end local v3    # "button":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 121
    .restart local v3    # "button":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    const/16 v16, 0xf

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setId(I)V

    goto/16 :goto_2

    .line 126
    :pswitch_3
    const v13, 0x7f03000e

    .line 127
    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v11, v13, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .end local v3    # "button":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 128
    .restart local v3    # "button":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    const/16 v16, 0x11

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setId(I)V

    goto/16 :goto_2

    .line 132
    :pswitch_4
    const v13, 0x7f03000f

    .line 133
    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v11, v13, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .end local v3    # "button":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 134
    .restart local v3    # "button":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setId(I)V

    goto/16 :goto_2

    .line 138
    :pswitch_5
    const v13, 0x7f03000a

    .line 139
    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v11, v13, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .end local v3    # "button":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 140
    .restart local v3    # "button":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setId(I)V

    goto/16 :goto_2

    .line 144
    :pswitch_6
    const v13, 0x7f030001

    .line 145
    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v11, v13, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .end local v3    # "button":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 146
    .restart local v3    # "button":Landroid/widget/LinearLayout;
    const v16, 0x7f090005

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 147
    .local v5, "cancelText":Landroid/widget/TextView;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f060009

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->toUppercaseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 148
    .local v4, "cancel":Ljava/lang/String;
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    const/16 v16, 0x4

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setId(I)V

    goto/16 :goto_2

    .line 154
    .end local v4    # "cancel":Ljava/lang/String;
    .end local v5    # "cancelText":Landroid/widget/TextView;
    :pswitch_7
    const v13, 0x7f030003

    .line 155
    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v11, v13, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .end local v3    # "button":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 156
    .restart local v3    # "button":Landroid/widget/LinearLayout;
    const v16, 0x7f090005

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 157
    .local v7, "doneText":Landroid/widget/TextView;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f060008

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->toUppercaseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 158
    .local v6, "done":Ljava/lang/String;
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    const/16 v16, 0x3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setId(I)V

    goto/16 :goto_2

    .line 164
    .end local v6    # "done":Ljava/lang/String;
    .end local v7    # "doneText":Landroid/widget/TextView;
    :pswitch_8
    const v13, 0x7f03000b

    .line 165
    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v11, v13, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .end local v3    # "button":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 166
    .restart local v3    # "button":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    const/16 v16, 0x5

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setId(I)V

    goto/16 :goto_2

    .line 170
    :pswitch_9
    const v13, 0x7f03000d

    .line 171
    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v11, v13, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .end local v3    # "button":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 172
    .restart local v3    # "button":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    const/16 v16, 0x10

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setId(I)V

    goto/16 :goto_2

    .line 176
    :pswitch_a
    const v13, 0x7f030008

    .line 177
    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v11, v13, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .end local v3    # "button":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 178
    .restart local v3    # "button":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    const/16 v16, 0xc

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setId(I)V

    .line 181
    :pswitch_b
    const v13, 0x7f030009

    .line 182
    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v11, v13, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .end local v3    # "button":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 183
    .restart local v3    # "button":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    const/16 v16, 0x12

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setId(I)V

    goto/16 :goto_2

    .line 200
    .restart local v14    # "param":Landroid/widget/LinearLayout$LayoutParams;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    goto/16 :goto_3

    .line 105
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V
    .locals 9
    .param p1, "l"    # Landroid/widget/LinearLayout;
    .param p2, "icon"    # Landroid/widget/ImageView;
    .param p3, "text"    # Landroid/widget/TextView;
    .param p4, "flag"    # Z

    .prologue
    .line 1587
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1588
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    .line 1590
    :cond_0
    if-eqz p4, :cond_4

    .line 1592
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 1593
    .local v7, "size":I
    const/4 v6, 0x0

    .line 1594
    .local v6, "i":I
    :goto_0
    if-lt v6, v7, :cond_3

    .line 1599
    :cond_1
    if-ne v6, v7, :cond_2

    .line 1600
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1614
    :cond_2
    :goto_1
    if-eqz p4, :cond_6

    .line 1616
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    move-object v8, v0

    check-cast v8, Landroid/app/Activity;

    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$8;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/LinearLayout;Z)V

    invoke-virtual {v8, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1658
    :goto_2
    return-void

    .line 1596
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 1594
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1604
    .end local v6    # "i":I
    .end local v7    # "size":I
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 1605
    .restart local v7    # "size":I
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_3
    if-ge v6, v7, :cond_2

    .line 1607
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 1608
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 1605
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1637
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    move-object v8, v0

    check-cast v8, Landroid/app/Activity;

    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/LinearLayout;Z)V

    invoke-virtual {v8, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_2
.end method

.method private ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V
    .locals 1
    .param p1, "l"    # Landroid/widget/LinearLayout;
    .param p2, "icon"    # Landroid/widget/ImageView;
    .param p3, "text"    # Landroid/widget/TextView;
    .param p4, "flag"    # Z

    .prologue
    .line 1661
    if-eqz p4, :cond_2

    .line 1663
    if-eqz p2, :cond_0

    .line 1665
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$10;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Landroid/widget/ImageView;)V

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 1673
    :cond_0
    if-eqz p3, :cond_1

    .line 1675
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$11;

    invoke-direct {v0, p0, p3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Landroid/widget/TextView;)V

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 1685
    :cond_1
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$12;

    invoke-direct {v0, p0, p1, p4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Landroid/widget/LinearLayout;Z)V

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    .line 1731
    :goto_0
    return-void

    .line 1698
    :cond_2
    if-eqz p2, :cond_3

    .line 1700
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$13;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Landroid/widget/ImageView;)V

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 1709
    :cond_3
    if-eqz p3, :cond_4

    .line 1711
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$14;

    invoke-direct {v0, p0, p3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Landroid/widget/TextView;)V

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 1721
    :cond_4
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$15;

    invoke-direct {v0, p0, p1, p4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Landroid/widget/LinearLayout;Z)V

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    .locals 1

    .prologue
    .line 2094
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 2091
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)I
    .locals 1

    .prologue
    .line 2099
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mViewWidth:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 2092
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 2089
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 2093
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)V
    .locals 0

    .prologue
    .line 1946
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resetActionBarBtns()V

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Landroid/app/ActionBar;
    .locals 1

    .prologue
    .line 2124
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$RecentShareVia;
    .locals 1

    .prologue
    .line 2125
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mRecentShareVia:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$RecentShareVia;

    return-object v0
.end method

.method private changeLanguage()V
    .locals 9

    .prologue
    .line 765
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v2, v7, :cond_0

    .line 826
    return-void

    .line 767
    :cond_0
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 768
    .local v0, "button":Landroid/view/View;
    const/4 v3, 0x0

    .line 769
    .local v3, "id":I
    const v7, 0x7f090005

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 770
    .local v6, "textView":Landroid/widget/TextView;
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 814
    :cond_1
    :goto_1
    :pswitch_0
    if-eqz v6, :cond_2

    if-eqz v3, :cond_2

    const/4 v7, -0x1

    if-eq v3, v7, :cond_2

    .line 816
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-static {v7, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 817
    .local v5, "text":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-static {v7, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->doNeedUppercaseString(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 818
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->toUppercaseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 819
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 765
    .end local v5    # "text":Ljava/lang/String;
    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 774
    :pswitch_1
    :try_start_0
    invoke-virtual {v6}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_1

    .line 775
    :catch_0
    move-exception v1

    .line 776
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 784
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :pswitch_2
    const v3, 0x7f060008

    .line 785
    goto :goto_1

    .line 787
    :pswitch_3
    const v3, 0x7f060009

    .line 788
    goto :goto_1

    .line 790
    :pswitch_4
    const v3, 0x7f06000d

    .line 791
    goto :goto_1

    .line 793
    :pswitch_5
    if-eqz v6, :cond_1

    .line 795
    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 796
    .restart local v5    # "text":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    const v8, 0x7f0600bf

    invoke-static {v7, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    .line 797
    .local v4, "ret":I
    if-nez v4, :cond_3

    .line 798
    const v3, 0x7f0600bf

    goto :goto_1

    .line 801
    :cond_3
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    const v8, 0x7f0600be

    invoke-static {v7, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    .line 802
    if-nez v4, :cond_1

    .line 803
    const v3, 0x7f0600be

    .line 806
    goto :goto_1

    .line 811
    .end local v4    # "ret":I
    .end local v5    # "text":Ljava/lang/String;
    :pswitch_6
    const v3, 0x7f0600b3

    goto :goto_1

    .line 821
    .restart local v5    # "text":Ljava/lang/String;
    :cond_4
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 770
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_0
    .end packed-switch
.end method

.method private enableExtraButtons()V
    .locals 3

    .prologue
    .line 1834
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 1836
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 1843
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 1838
    .restart local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1839
    .local v1, "l":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 1840
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1836
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getButton(I)Landroid/view/View;
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 524
    const/4 v1, 0x0

    .line 525
    .local v1, "ret":Landroid/view/View;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 532
    return-object v1

    .line 527
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getId()I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 529
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "ret":Landroid/view/View;
    check-cast v1, Landroid/view/View;

    .line 525
    .restart local v1    # "ret":Landroid/view/View;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private resetActionBarBtns()V
    .locals 3

    .prologue
    .line 1949
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 1951
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 1958
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 1953
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1954
    .local v0, "button":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isPressed()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1955
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setPressed(Z)V

    .line 1951
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private setActionBarTitle(Landroid/view/View;I)Z
    .locals 4
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "titleTextId"    # I

    .prologue
    .line 875
    const v2, 0x7f090005

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 876
    .local v1, "tv":Landroid/widget/TextView;
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 877
    if-eqz v1, :cond_0

    .line 879
    const/4 v2, -0x1

    if-ne p2, v2, :cond_1

    .line 881
    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 899
    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    .line 885
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-static {v2, p2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 891
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 892
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setActionBarTitle text:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setDescription(ILjava/lang/String;)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "str"    # Ljava/lang/String;

    .prologue
    .line 1902
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1908
    return-void

    .line 1902
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1903
    .local v0, "button":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    const/16 v3, 0x12

    if-ne v2, v3, :cond_0

    .line 1905
    invoke-virtual {v0, p2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setHoverActionBarBtns()V
    .locals 3

    .prologue
    .line 499
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/16 v2, 0x11

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 500
    .local v0, "showPreviousBtn":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 502
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v1

    if-nez v1, :cond_1

    .line 504
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 511
    :cond_0
    :goto_0
    return-void

    .line 508
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->removeHovering(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public ableBrush()V
    .locals 4

    .prologue
    .line 1004
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/16 v2, 0xe

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1005
    .local v0, "overflow":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1009
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1010
    const v2, 0x7f090005

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1011
    const/4 v3, 0x1

    .line 1007
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1013
    :cond_0
    return-void
.end method

.method public ableCancel()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1342
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCancelState:Z

    .line 1343
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1344
    .local v0, "cancel":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1348
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1349
    const v2, 0x7f090005

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1346
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1352
    :cond_0
    return-void
.end method

.method public ableCancelExceptAbleList()V
    .locals 4

    .prologue
    .line 1369
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1370
    .local v0, "cancel":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1374
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1375
    const v2, 0x7f090005

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1376
    const/4 v3, 0x1

    .line 1372
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1378
    :cond_0
    return-void
.end method

.method public ableDone()V
    .locals 6

    .prologue
    const v5, 0x7f090005

    const/4 v4, 0x1

    .line 904
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->doneState:Z

    .line 905
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 906
    .local v0, "done":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 910
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 911
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 908
    invoke-direct {p0, v0, v1, v2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 913
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    const v3, 0x7f060008

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 914
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHoveringWithoutPopUpDisplay(Landroid/content/Context;Landroid/view/View;)V

    .line 915
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 917
    :cond_0
    return-void
.end method

.method public ableHome()V
    .locals 4

    .prologue
    .line 1250
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1251
    .local v0, "home":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1255
    const v1, 0x7f090007

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1256
    const/4 v2, 0x0

    .line 1257
    const/4 v3, 0x1

    .line 1253
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1259
    :cond_0
    return-void
.end method

.method public ableOption()V
    .locals 4

    .prologue
    .line 1133
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/16 v2, 0xc

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1134
    .local v0, "overflow":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1138
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1139
    const v2, 0x7f090005

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1140
    const/4 v3, 0x1

    .line 1136
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1142
    :cond_0
    return-void
.end method

.method public ableRedo()V
    .locals 4

    .prologue
    .line 1201
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1202
    .local v0, "redo":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1206
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1207
    const/4 v2, 0x0

    .line 1208
    const/4 v3, 0x1

    .line 1204
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1210
    :cond_0
    return-void
.end method

.method public ableRedoExceptAbleList()V
    .locals 4

    .prologue
    .line 1225
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1226
    .local v0, "redo":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1230
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1231
    const/4 v2, 0x0

    .line 1232
    const/4 v3, 0x1

    .line 1228
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1234
    :cond_0
    return-void
.end method

.method public ableSave()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1276
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mSaveState:Z

    .line 1277
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1278
    .local v0, "save":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1282
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1283
    const v2, 0x7f090005

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1280
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1286
    :cond_0
    return-void
.end method

.method public ableSaveExceptAbleList()V
    .locals 4

    .prologue
    .line 1316
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1317
    .local v0, "save":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1321
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1322
    const v2, 0x7f090005

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1323
    const/4 v3, 0x1

    .line 1319
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1325
    :cond_0
    return-void
.end method

.method public ableSharevia()V
    .locals 4

    .prologue
    .line 1304
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/16 v2, 0x10

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1305
    .local v0, "sharevia":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1309
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1310
    const v2, 0x7f090005

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1311
    const/4 v3, 0x1

    .line 1307
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1313
    :cond_0
    return-void
.end method

.method public ableUndo()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1157
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1158
    .local v0, "undo":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1161
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1162
    const/4 v2, 0x0

    .line 1160
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1165
    :cond_0
    return-void
.end method

.method public ableUndoExceptAbleList()V
    .locals 4

    .prologue
    .line 1168
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1169
    .local v0, "undo":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1172
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1173
    const/4 v2, 0x0

    .line 1174
    const/4 v3, 0x0

    .line 1171
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1176
    :cond_0
    return-void
.end method

.method public brushGone()V
    .locals 3

    .prologue
    .line 1098
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/16 v2, 0xe

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1099
    .local v0, "overflow":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1101
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1103
    :cond_0
    return-void
.end method

.method public brushOn()V
    .locals 3

    .prologue
    .line 1106
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/16 v2, 0xe

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1107
    .local v0, "overflow":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1109
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1110
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1112
    :cond_0
    return-void
.end method

.method public buttonGone(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    const v2, 0x7f090157

    .line 1028
    const/16 v1, 0x10

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getMenu()Landroid/view/Menu;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1029
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1031
    :cond_0
    const/16 v1, 0xc

    if-ne p1, v1, :cond_2

    .line 1033
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    if-eqz v1, :cond_1

    .line 1035
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 1053
    :cond_1
    :goto_0
    return-void

    .line 1040
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1045
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 1047
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public buttonVisible(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    const v3, 0x7f090157

    const/4 v2, 0x0

    .line 1056
    const/16 v1, 0x10

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getMenu()Landroid/view/Menu;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1057
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1059
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1067
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 1069
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1070
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 1071
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "button visible id:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1076
    :cond_1
    return-void
.end method

.method public changeCancelPressState(Z)V
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 1462
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1463
    .local v0, "cancel":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1464
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setPressed(Z)V

    .line 1465
    :cond_0
    return-void
.end method

.method public changeDoneCancelLayout()V
    .locals 11

    .prologue
    .line 1399
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    const v10, 0x7f090008

    invoke-virtual {v9, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 1400
    .local v7, "vGroup":Landroid/view/ViewGroup;
    const/16 v9, 0x8

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1401
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v10, 0x4

    invoke-static {v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1402
    .local v0, "cancel":Landroid/widget/LinearLayout;
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v2

    .line 1403
    .local v2, "done":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 1405
    .local v6, "lp":Landroid/widget/LinearLayout$LayoutParams;
    const v9, 0x7f090004

    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 1406
    const v9, 0x7f090004

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 1409
    const v9, 0x7f090005

    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1410
    .local v3, "doneText":Landroid/widget/TextView;
    const v9, 0x7f090005

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1413
    .local v1, "cancelText":Landroid/widget/TextView;
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1414
    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1416
    const-string v9, "sans-serif"

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    .line 1417
    .local v4, "font":Landroid/graphics/Typeface;
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1418
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1420
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v8, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1421
    .local v8, "width":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v5, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1422
    .local v5, "height":I
    if-ge v8, v5, :cond_0

    .line 1423
    move v8, v5

    .line 1427
    :cond_0
    iput v8, v6, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1428
    const/high16 v9, 0x3f000000    # 0.5f

    iput v9, v6, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1429
    const/16 v9, 0x11

    iput v9, v6, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1430
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1431
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .end local v6    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 1434
    .restart local v6    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    iput v8, v6, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1435
    const/high16 v9, 0x3f000000    # 0.5f

    iput v9, v6, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1436
    const/16 v9, 0x11

    iput v9, v6, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1437
    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1438
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 1439
    return-void
.end method

.method public changeLayoutSize(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 1395
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mViewWidth:I

    .line 1396
    return-void
.end method

.method public changeOtherButtonLayout()V
    .locals 11

    .prologue
    const v10, 0x7f090004

    const v9, 0x7f090005

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 1443
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    const v6, 0x7f090008

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 1444
    .local v4, "vGroup":Landroid/view/ViewGroup;
    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1445
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v6, 0x4

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1446
    .local v0, "cancel":Landroid/widget/LinearLayout;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v1

    .line 1447
    .local v1, "done":Landroid/widget/LinearLayout;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v6, 0x5

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v3

    .line 1449
    .local v3, "save":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1450
    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1452
    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1453
    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1454
    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1456
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    const/4 v6, -0x1

    invoke-direct {v2, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1457
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1458
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1459
    return-void
.end method

.method public cropRotateGone()V
    .locals 3

    .prologue
    .line 1080
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/16 v2, 0xf

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1081
    .local v0, "rotate":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1083
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1085
    :cond_0
    return-void
.end method

.method public cropRotateOn()V
    .locals 3

    .prologue
    .line 1088
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/16 v2, 0xf

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1089
    .local v0, "rotate":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1091
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1092
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1094
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 363
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 364
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 365
    :cond_0
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;

    .line 367
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_5

    .line 373
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->removeList(Ljava/util/ArrayList;)V

    .line 374
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    .line 376
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonListDisabledByPressing:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 378
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonListDisabledByPressing:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_6

    .line 383
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonListDisabledByPressing:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->removeList(Ljava/util/ArrayList;)V

    .line 385
    :cond_1
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonListDisabledByPressing:Ljava/util/ArrayList;

    .line 387
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    if-eqz v1, :cond_2

    .line 388
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->removeAllViews()V

    .line 389
    :cond_2
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    .line 391
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    .line 392
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 394
    :cond_3
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    .line 395
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    .line 396
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    if-eqz v1, :cond_4

    .line 397
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 398
    :cond_4
    return-void

    .line 367
    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 369
    .local v0, "button":Landroid/widget/LinearLayout;
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 370
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViewsInLayout()V

    goto :goto_0

    .line 378
    .end local v0    # "button":Landroid/widget/LinearLayout;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 380
    .restart local v0    # "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViewsInLayout()V

    goto :goto_1
.end method

.method public disableAnotherButtons(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1738
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 1740
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 1751
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 1742
    .restart local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1743
    .local v1, "l":Landroid/widget/LinearLayout;
    if-ne v1, p1, :cond_2

    .line 1744
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1740
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1747
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_1
.end method

.method public disableBtnWithTextColor(I)V
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 958
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-static {v2, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v1

    .line 959
    .local v1, "v":Landroid/view/View;
    const v2, 0x7f090005

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 961
    .local v0, "text":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$7;

    invoke-direct {v3, p0, v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Landroid/widget/TextView;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 974
    return-void
.end method

.method public disableButtons()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1815
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 1817
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 1827
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-static {v4, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1828
    .local v0, "home":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1830
    .end local v0    # "home":Landroid/widget/LinearLayout;
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 1819
    .restart local v1    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 1820
    .local v3, "l":Landroid/widget/LinearLayout;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_2

    .line 1817
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1822
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getId()I

    move-result v5

    if-ne v4, v5, :cond_3

    .line 1823
    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1820
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public enableBtnWithTextColor(I)V
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 938
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-static {v2, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v1

    .line 939
    .local v1, "v":Landroid/view/View;
    const v2, 0x7f090005

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 941
    .local v0, "text":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$6;

    invoke-direct {v3, p0, v1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Landroid/view/View;Landroid/widget/TextView;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 955
    return-void
.end method

.method public enableButtons()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1797
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 1799
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 1810
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1811
    .local v0, "home":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1813
    .end local v0    # "home":Landroid/widget/LinearLayout;
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 1801
    .restart local v1    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 1802
    .local v3, "l":Landroid/widget/LinearLayout;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_2

    .line 1799
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1804
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getId()I

    move-result v5

    if-ne v4, v5, :cond_3

    .line 1805
    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1802
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public focusCancelAndDone(I)V
    .locals 7
    .param p1, "option"    # I

    .prologue
    const v6, 0x7f090005

    const v5, 0x7f090004

    const/4 v4, 0x1

    .line 2135
    packed-switch p1, :pswitch_data_0

    .line 2167
    :goto_0
    return-void

    .line 2137
    :pswitch_0
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCancelState:Z

    .line 2138
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 2139
    .local v0, "cancel":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 2143
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 2144
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 2141
    invoke-direct {p0, v0, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 2147
    :cond_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    .line 2151
    .end local v0    # "cancel":Landroid/widget/LinearLayout;
    :pswitch_1
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->doneState:Z

    .line 2152
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v1

    .line 2153
    .local v1, "done":Landroid/widget/LinearLayout;
    if-eqz v1, :cond_1

    .line 2157
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 2158
    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 2155
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 2161
    :cond_1
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    .line 2135
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getActionBar()Landroid/app/ActionBar;
    .locals 1

    .prologue
    .line 1962
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method public getActionbarHeight()I
    .locals 4

    .prologue
    .line 1987
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1988
    .local v0, "tv":Landroid/util/TypedValue;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x10102eb

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1989
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    return v1
.end method

.method public getActionbarLayout()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getButtonList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1982
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getButtonPosition(I)Landroid/graphics/Point;
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 536
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 537
    .local v1, "point":Landroid/graphics/Point;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-static {v3, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 539
    .local v0, "button":Landroid/widget/LinearLayout;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 540
    .local v2, "r":Landroid/graphics/Rect;
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 542
    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    iput v3, v1, Landroid/graphics/Point;->x:I

    .line 543
    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    iput v3, v1, Landroid/graphics/Point;->y:I

    .line 544
    return-object v1
.end method

.method public getMenu()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 2020
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 1734
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWidth(I)I
    .locals 3
    .param p1, "type"    # I

    .prologue
    const/4 v2, 0x0

    .line 1968
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    move v1, v2

    .line 1977
    :goto_1
    return v1

    .line 1970
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v1, p1, :cond_1

    .line 1972
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2, v2}, Landroid/widget/LinearLayout;->measure(II)V

    .line 1973
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v1

    goto :goto_1

    .line 1968
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 335
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$3;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 343
    return-void
.end method

.method public ignoreAnotherButtonTouchEvent(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1754
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonListDisabledByPressing:Ljava/util/ArrayList;

    .line 1755
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 1757
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 1769
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 1759
    .restart local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1760
    .local v1, "l":Landroid/widget/LinearLayout;
    if-eq v1, p1, :cond_2

    .line 1765
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonListDisabledByPressing:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1757
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V
    .locals 6
    .param p1, "isEnabled"    # Z
    .param p2, "iconId"    # I
    .param p3, "titleTextId"    # I
    .param p4, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 732
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    .line 734
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 735
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setId(I)V

    .line 736
    if-eqz p1, :cond_4

    .line 738
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableHome()V

    .line 739
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 746
    :goto_0
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;

    invoke-direct {v2, p0, p4}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 747
    .local v2, "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 749
    if-lez p2, :cond_0

    .line 750
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    invoke-virtual {p0, v3, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarIcon(II)Z

    .line 751
    :cond_0
    if-lez p3, :cond_1

    .line 752
    invoke-direct {p0, v0, p3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarTitle(Landroid/view/View;I)Z

    .line 754
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .local v1, "i":I
    :goto_1
    if-gtz v1, :cond_5

    .line 760
    .end local v0    # "button":Landroid/widget/LinearLayout;
    .end local v1    # "i":I
    .end local v2    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 761
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 762
    :cond_3
    return-void

    .line 743
    .restart local v0    # "button":Landroid/widget/LinearLayout;
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableHome()V

    .line 744
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 756
    .restart local v1    # "i":I
    .restart local v2    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 757
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 754
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method public initHorizontalScrollView()V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setScrollViewCallback(Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 309
    return-void
.end method

.method public initSaveBtn(ZZ)V
    .locals 3
    .param p1, "isEdited"    # Z
    .param p2, "saveIsVisible"    # Z

    .prologue
    const/16 v2, 0x10

    const/4 v0, 0x5

    const/16 v1, 0x12

    .line 1873
    if-eqz p2, :cond_1

    .line 1874
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 1875
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonVisible(I)V

    .line 1876
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mRecentShareVia:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$RecentShareVia;

    if-eqz v0, :cond_0

    .line 1877
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonVisible(I)V

    .line 1880
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableSharevia()V

    .line 1891
    :goto_1
    return-void

    .line 1879
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    goto :goto_0

    .line 1883
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 1884
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 1885
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonVisible(I)V

    .line 1886
    if-eqz p1, :cond_2

    .line 1887
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableSave()V

    goto :goto_1

    .line 1889
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableSave()V

    goto :goto_1
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->invalidate()V

    .line 332
    return-void
.end method

.method public isCancelFocused()Z
    .locals 3

    .prologue
    .line 2171
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCancelState:Z

    .line 2172
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 2173
    .local v0, "cancel":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v1

    return v1
.end method

.method public isConfigurationChangedFromMultigridView()Z
    .locals 1

    .prologue
    .line 2085
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mFromMultigridiView:Z

    return v0
.end method

.method public isDisabledButtonTouchEvent(Landroid/view/View;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1780
    const/4 v1, 0x0

    .line 1781
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonListDisabledByPressing:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 1783
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonListDisabledByPressing:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 1792
    .end local v0    # "i":I
    :cond_0
    :goto_1
    return v1

    .line 1785
    .restart local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonListDisabledByPressing:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_2

    .line 1787
    const/4 v1, 0x1

    .line 1788
    goto :goto_1

    .line 1783
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public isDoneFocused()Z
    .locals 3

    .prologue
    .line 2179
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCancelState:Z

    .line 2180
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 2181
    .local v0, "done":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v1

    return v1
.end method

.method public isEnabledCancel()Z
    .locals 1

    .prologue
    .line 986
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCancelState:Z

    return v0
.end method

.method public isEnabledDone()Z
    .locals 1

    .prologue
    .line 978
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->doneState:Z

    return v0
.end method

.method public isEnabledSave()Z
    .locals 1

    .prologue
    .line 982
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mSaveState:Z

    return v0
.end method

.method public isFocused()Z
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 439
    const/4 v10, 0x0

    .line 440
    .local v10, "ret":Z
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    .line 442
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v9, v6, :cond_1

    .line 461
    .end local v9    # "i":I
    :cond_0
    :goto_1
    return v10

    .line 444
    .restart local v9    # "i":I
    :cond_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 446
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 447
    .local v0, "upTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .local v2, "eventTime":J
    move v6, v5

    move v7, v4

    .line 448
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 450
    .local v8, "e":Landroid/view/MotionEvent;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v10

    .line 451
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    .line 456
    .end local v0    # "upTime":J
    .end local v2    # "eventTime":J
    .end local v8    # "e":Landroid/view/MotionEvent;
    :cond_2
    const-string v6, "onKeyUp mButtonList.get(i) null"

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 442
    add-int/lit8 v9, v9, 0x1

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 2

    .prologue
    .line 357
    const/4 v0, 0x0

    .line 358
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    .line 359
    return v0
.end method

.method public isValidBackKey()Z
    .locals 1

    .prologue
    .line 1000
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mBackState:Z

    return v0
.end method

.method public isVisibleTitle()Z
    .locals 3

    .prologue
    .line 2071
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    const v2, 0x7f090008

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2072
    .local v0, "vGroup":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 2073
    const/4 v1, 0x1

    .line 2075
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onConfigurationChanged()V
    .locals 0

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resume()V

    .line 516
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLanguage()V

    .line 517
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 518
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resetActionBarBtns()V

    .line 519
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setHoverActionBarBtns()V

    .line 520
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resetAnotherButtonTouchEvent()V

    .line 521
    return-void
.end method

.method public onEnter()Z
    .locals 11

    .prologue
    .line 402
    const/4 v10, 0x0

    .line 403
    .local v10, "ret":Z
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onKeyUp mButtonList size "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 404
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v9, v4, :cond_0

    .line 435
    :goto_1
    return v10

    .line 406
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 408
    const/4 v10, 0x1

    .line 409
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onKeyUp mButtonList.get("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") focused"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 410
    const-wide/16 v0, 0x0

    .line 411
    .local v0, "upTime":J
    const-wide/16 v2, 0x0

    .line 412
    .local v2, "eventTime":J
    const/4 v8, 0x0

    .line 414
    .local v8, "e":Landroid/view/MotionEvent;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 415
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 417
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 416
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 418
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 420
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 421
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 423
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 422
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 425
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    .line 432
    .end local v0    # "upTime":J
    .end local v2    # "eventTime":J
    .end local v8    # "e":Landroid/view/MotionEvent;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onKeyUp mButtonList.get("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") not focused"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 404
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0
.end method

.method public onKeyup()Z
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1846
    const/4 v10, 0x0

    .line 1847
    .local v10, "ret":Z
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    .line 1849
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v9, v6, :cond_1

    .line 1868
    .end local v9    # "i":I
    :cond_0
    :goto_1
    return v10

    .line 1851
    .restart local v9    # "i":I
    :cond_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 1853
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1854
    .local v0, "upTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .local v2, "eventTime":J
    move v6, v5

    move v7, v4

    .line 1855
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 1857
    .local v8, "e":Landroid/view/MotionEvent;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v10

    .line 1858
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    .line 1863
    .end local v0    # "upTime":J
    .end local v2    # "eventTime":J
    .end local v8    # "e":Landroid/view/MotionEvent;
    :cond_2
    const-string v6, "onKeyUp mButtonList.get(i) null"

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1849
    add-int/lit8 v9, v9, 0x1

    goto :goto_0
.end method

.method public onkeyEnter()Z
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 466
    const/4 v10, 0x0

    .line 467
    .local v10, "ret":Z
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    .line 469
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v9, v6, :cond_1

    .line 489
    .end local v9    # "i":I
    :cond_0
    :goto_1
    return v10

    .line 472
    .restart local v9    # "i":I
    :cond_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 474
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 475
    .local v0, "upTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .local v2, "eventTime":J
    move v6, v5

    move v7, v4

    .line 476
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 478
    .local v8, "e":Landroid/view/MotionEvent;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v10

    .line 479
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    .line 484
    .end local v0    # "upTime":J
    .end local v2    # "eventTime":J
    .end local v8    # "e":Landroid/view/MotionEvent;
    :cond_2
    const-string v6, "onKeyUp mButtonList.get(i) null"

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 469
    add-int/lit8 v9, v9, 0x1

    goto :goto_0
.end method

.method public registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z
    .locals 7
    .param p1, "type"    # I
    .param p2, "isEnabled"    # Z
    .param p3, "isLongPressed"    # Z
    .param p4, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;

    .prologue
    const/4 v5, 0x1

    const v6, 0x7f090005

    const v3, 0x7f090004

    const/4 v4, 0x0

    .line 549
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;

    if-nez v2, :cond_0

    move v2, v4

    .line 728
    :goto_0
    return v2

    .line 552
    :cond_0
    const/4 v0, 0x0

    .line 553
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getButton(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "button":Landroid/widget/LinearLayout;
    check-cast v0, Landroid/widget/LinearLayout;

    .line 555
    .restart local v0    # "button":Landroid/widget/LinearLayout;
    packed-switch p1, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v2, v5

    .line 728
    goto :goto_0

    .line 557
    :pswitch_1
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 558
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p3, p0, p4}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;-><init>(Landroid/content/Context;ZLcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 559
    .local v1, "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 560
    if-eqz p2, :cond_1

    .line 561
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    goto :goto_1

    .line 563
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    goto :goto_1

    .line 566
    .end local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    :pswitch_2
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 567
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p3, p0, p4}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;-><init>(Landroid/content/Context;ZLcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 568
    .restart local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 569
    if-eqz p2, :cond_2

    .line 570
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    goto :goto_1

    .line 572
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_1

    .line 575
    .end local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    :pswitch_3
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 577
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;

    invoke-direct {v1, p0, p4}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 578
    .restart local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 579
    if-eqz p2, :cond_3

    .line 580
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    goto :goto_1

    .line 582
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    goto :goto_1

    .line 585
    .end local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    :pswitch_4
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 591
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p3, p0, p4}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;-><init>(Landroid/content/Context;ZLcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 592
    .restart local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 593
    if-eqz p2, :cond_4

    .line 594
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableCancel()V

    goto :goto_1

    .line 596
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableCancel()V

    goto :goto_1

    .line 599
    .end local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    :pswitch_5
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 600
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p3, p0, p4}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;-><init>(Landroid/content/Context;ZLcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 601
    .restart local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 602
    if-eqz p2, :cond_5

    .line 603
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableSave()V

    goto :goto_1

    .line 605
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableSave()V

    goto :goto_1

    .line 608
    .end local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    :pswitch_6
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 609
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;

    invoke-direct {v1, p0, p4}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 610
    .restart local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_1

    .line 613
    .end local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    :pswitch_7
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 614
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;

    invoke-direct {v1, p0, p4}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 615
    .restart local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 616
    if-eqz p2, :cond_6

    .line 617
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableOption()V

    goto/16 :goto_1

    .line 619
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableOption()V

    goto/16 :goto_1

    .line 622
    .end local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    :pswitch_8
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 623
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;

    invoke-direct {v1, p0, p4}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 624
    .restart local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 625
    if-eqz p2, :cond_7

    .line 626
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->brushOn()V

    goto/16 :goto_1

    .line 628
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->brushGone()V

    goto/16 :goto_1

    .line 631
    .end local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    :pswitch_9
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 632
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;

    invoke-direct {v1, p0, p4}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 633
    .restart local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 634
    if-eqz p2, :cond_8

    .line 635
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->showPreviousOn()V

    goto/16 :goto_1

    .line 637
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->showPreviousGone()V

    goto/16 :goto_1

    .line 640
    .end local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    :pswitch_a
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 641
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;

    invoke-direct {v1, p0, p4}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 642
    .restart local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_1

    .line 645
    .end local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    :pswitch_b
    const-string v2, "regist button share via"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 646
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 647
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p3, p0, p4}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;-><init>(Landroid/content/Context;ZLcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 648
    .restart local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 649
    if-eqz p2, :cond_9

    .line 653
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 654
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 651
    invoke-direct {p0, v0, v2, v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    goto/16 :goto_1

    .line 661
    :cond_9
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 662
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 659
    invoke-direct {p0, v0, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    goto/16 :goto_1

    .line 667
    .end local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    :pswitch_c
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 669
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$5;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)V

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 693
    .restart local v1    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 694
    if-eqz p2, :cond_a

    .line 698
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 699
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 696
    invoke-direct {p0, v0, v2, v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    goto/16 :goto_1

    .line 706
    :cond_a
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 707
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 704
    invoke-direct {p0, v0, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    goto/16 :goto_1

    .line 555
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_a
        :pswitch_b
        :pswitch_9
        :pswitch_c
    .end packed-switch
.end method

.method public resetAnotherButtonTouchEvent()V
    .locals 1

    .prologue
    .line 1772
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonListDisabledByPressing:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1774
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonListDisabledByPressing:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1775
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonListDisabledByPressing:Ljava/util/ArrayList;

    .line 1777
    :cond_0
    return-void
.end method

.method public resetMenu()V
    .locals 3

    .prologue
    .line 2024
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    if-eqz v1, :cond_1

    .line 2026
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 2027
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x1c000000

    if-eq v1, v2, :cond_0

    .line 2028
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x2c000000

    if-eq v1, v2, :cond_0

    .line 2029
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x1e110000

    if-ne v1, v2, :cond_2

    .line 2030
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 2031
    .local v0, "inflater":Landroid/view/MenuInflater;
    const/high16 v1, 0x7f080000

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2042
    .end local v0    # "inflater":Landroid/view/MenuInflater;
    :cond_1
    :goto_0
    return-void

    .line 2035
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 2036
    .restart local v0    # "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f080002

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2037
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    const v2, 0x7f09015a

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 2038
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    const v2, 0x7f090155

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 2039
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    const v2, 0x7f090156

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0
.end method

.method public resetMenu(Z)V
    .locals 3
    .param p1, "isMinimum"    # Z

    .prologue
    .line 2046
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "JW mMenu="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2047
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    if-eqz v1, :cond_1

    .line 2049
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 2050
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x1c000000

    if-eq v1, v2, :cond_0

    .line 2051
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x2c000000

    if-eq v1, v2, :cond_0

    .line 2052
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x1e110000

    if-ne v1, v2, :cond_2

    .line 2053
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 2054
    .local v0, "inflater":Landroid/view/MenuInflater;
    const/high16 v1, 0x7f080000

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2064
    :goto_0
    if-eqz p1, :cond_1

    .line 2065
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    const v2, 0x7f09015b

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 2067
    .end local v0    # "inflater":Landroid/view/MenuInflater;
    :cond_1
    return-void

    .line 2058
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 2059
    .restart local v0    # "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f080002

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2060
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    const v2, 0x7f09015a

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 2061
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    const v2, 0x7f090155

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 2062
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    const v2, 0x7f090156

    invoke-interface {v1, v2}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0
.end method

.method public resume()V
    .locals 7

    .prologue
    .line 313
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    const v5, 0x7f090008

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 315
    .local v1, "home_layout":Landroid/widget/LinearLayout;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 317
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_0

    .line 328
    return-void

    .line 319
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 320
    .local v0, "button":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 321
    .local v3, "vGroup":Landroid/view/ViewGroup;
    if-eqz v3, :cond_1

    .line 322
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 323
    :cond_1
    if-nez v2, :cond_2

    .line 324
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    const/4 v6, -0x1

    invoke-direct {v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 317
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 326
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1
.end method

.method public setActionBarBtnVisibility()V
    .locals 4

    .prologue
    .line 1910
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 1912
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 1944
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 1914
    .restart local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 1912
    :cond_2
    :goto_1
    :pswitch_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1926
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1927
    .local v0, "button":Landroid/view/View;
    const v3, 0x7f090005

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1928
    .local v2, "textView":Landroid/view/View;
    if-eqz v2, :cond_2

    .line 1930
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1936
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 1914
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setActionBarIcon(II)Z
    .locals 3
    .param p1, "type"    # I
    .param p2, "resid"    # I

    .prologue
    .line 829
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-static {v2, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 830
    .local v0, "button":Landroid/widget/LinearLayout;
    const v2, 0x7f090004

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 832
    .local v1, "iv":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 834
    if-eqz p2, :cond_0

    .line 841
    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 848
    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public setActionBarIcon(ILandroid/graphics/drawable/Drawable;)Z
    .locals 3
    .param p1, "type"    # I
    .param p2, "icon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 852
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-static {v2, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 853
    .local v0, "button":Landroid/widget/LinearLayout;
    const v2, 0x7f090004

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 855
    .local v1, "iv":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 857
    if-eqz p2, :cond_0

    .line 864
    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 871
    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public setAllDim()V
    .locals 12

    .prologue
    const v11, 0x7f090005

    const v10, 0x7f090004

    const/4 v9, 0x0

    .line 1469
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    .line 1471
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v6

    .line 1472
    .local v6, "undo":Landroid/widget/LinearLayout;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v8, 0x2

    invoke-static {v7, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v3

    .line 1473
    .local v3, "redo":Landroid/widget/LinearLayout;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v8, 0x4

    invoke-static {v7, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1474
    .local v0, "cancel":Landroid/widget/LinearLayout;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v8, 0x5

    invoke-static {v7, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v4

    .line 1476
    .local v4, "save":Landroid/widget/LinearLayout;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/16 v8, 0x10

    invoke-static {v7, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v5

    .line 1477
    .local v5, "sharevia":Landroid/widget/LinearLayout;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v1, v7, :cond_1

    .line 1525
    .end local v0    # "cancel":Landroid/widget/LinearLayout;
    .end local v1    # "i":I
    .end local v3    # "redo":Landroid/widget/LinearLayout;
    .end local v4    # "save":Landroid/widget/LinearLayout;
    .end local v5    # "sharevia":Landroid/widget/LinearLayout;
    .end local v6    # "undo":Landroid/widget/LinearLayout;
    :cond_0
    return-void

    .line 1479
    .restart local v0    # "cancel":Landroid/widget/LinearLayout;
    .restart local v1    # "i":I
    .restart local v3    # "redo":Landroid/widget/LinearLayout;
    .restart local v4    # "save":Landroid/widget/LinearLayout;
    .restart local v5    # "sharevia":Landroid/widget/LinearLayout;
    .restart local v6    # "undo":Landroid/widget/LinearLayout;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1480
    .local v2, "idx":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getId()I

    move-result v8

    if-ne v7, v8, :cond_3

    .line 1483
    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1484
    invoke-virtual {v6, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1482
    invoke-direct {p0, v6, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1477
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1487
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getId()I

    move-result v8

    if-ne v7, v8, :cond_4

    .line 1490
    invoke-virtual {v3, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1491
    invoke-virtual {v3, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1489
    invoke-direct {p0, v3, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    goto :goto_1

    .line 1494
    :cond_4
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v8

    if-ne v7, v8, :cond_5

    .line 1497
    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1498
    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1496
    invoke-direct {p0, v0, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    goto :goto_1

    .line 1501
    :cond_5
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getId()I

    move-result v8

    if-ne v7, v8, :cond_6

    .line 1504
    invoke-virtual {v4, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1505
    invoke-virtual {v4, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1503
    invoke-direct {p0, v4, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    goto :goto_1

    .line 1516
    :cond_6
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getId()I

    move-result v8

    if-ne v7, v8, :cond_2

    .line 1519
    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1520
    invoke-virtual {v5, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1518
    invoke-direct {p0, v5, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    goto :goto_1
.end method

.method public setAllNormal()V
    .locals 12

    .prologue
    const v11, 0x7f090005

    const v10, 0x7f090004

    const/4 v9, 0x1

    .line 1528
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    .line 1530
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-static {v7, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v6

    .line 1531
    .local v6, "undo":Landroid/widget/LinearLayout;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v8, 0x2

    invoke-static {v7, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v3

    .line 1532
    .local v3, "redo":Landroid/widget/LinearLayout;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v8, 0x4

    invoke-static {v7, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1533
    .local v0, "cancel":Landroid/widget/LinearLayout;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v8, 0x5

    invoke-static {v7, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v4

    .line 1535
    .local v4, "save":Landroid/widget/LinearLayout;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/16 v8, 0x10

    invoke-static {v7, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v5

    .line 1536
    .local v5, "sharevia":Landroid/widget/LinearLayout;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v1, v7, :cond_1

    .line 1584
    .end local v0    # "cancel":Landroid/widget/LinearLayout;
    .end local v1    # "i":I
    .end local v3    # "redo":Landroid/widget/LinearLayout;
    .end local v4    # "save":Landroid/widget/LinearLayout;
    .end local v5    # "sharevia":Landroid/widget/LinearLayout;
    .end local v6    # "undo":Landroid/widget/LinearLayout;
    :cond_0
    return-void

    .line 1538
    .restart local v0    # "cancel":Landroid/widget/LinearLayout;
    .restart local v1    # "i":I
    .restart local v3    # "redo":Landroid/widget/LinearLayout;
    .restart local v4    # "save":Landroid/widget/LinearLayout;
    .restart local v5    # "sharevia":Landroid/widget/LinearLayout;
    .restart local v6    # "undo":Landroid/widget/LinearLayout;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mAbleButtonList:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1539
    .local v2, "idx":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getId()I

    move-result v8

    if-ne v7, v8, :cond_3

    .line 1542
    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1543
    invoke-virtual {v6, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1541
    invoke-direct {p0, v6, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1536
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1546
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getId()I

    move-result v8

    if-ne v7, v8, :cond_4

    .line 1549
    invoke-virtual {v3, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1550
    invoke-virtual {v3, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1548
    invoke-direct {p0, v3, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    goto :goto_1

    .line 1553
    :cond_4
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v8

    if-ne v7, v8, :cond_5

    .line 1556
    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1557
    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1555
    invoke-direct {p0, v0, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    goto :goto_1

    .line 1560
    :cond_5
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getId()I

    move-result v8

    if-ne v7, v8, :cond_6

    .line 1563
    invoke-virtual {v4, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1564
    invoke-virtual {v4, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1562
    invoke-direct {p0, v4, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    goto :goto_1

    .line 1575
    :cond_6
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getId()I

    move-result v8

    if-ne v7, v8, :cond_2

    .line 1578
    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1579
    invoke-virtual {v5, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1577
    invoke-direct {p0, v5, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    goto :goto_1
.end method

.method public setBackKeyState(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 996
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mBackState:Z

    .line 997
    return-void
.end method

.method public setConfigurationChangedFromMultigridView(Z)V
    .locals 1
    .param p1, "fromMultigridView"    # Z

    .prologue
    .line 2080
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mFromMultigridiView:Z

    .line 2081
    const-string v0, "JW setConfigurationChangedFromMultigridView"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2082
    return-void
.end method

.method public setMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 2016
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mMenu:Landroid/view/Menu;

    .line 2017
    return-void
.end method

.method public setRecentShareVia(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$RecentShareViaCallback;)V
    .locals 1
    .param p1, "iconDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$RecentShareViaCallback;

    .prologue
    const/16 v0, 0x12

    .line 1895
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonVisible(I)V

    .line 1896
    invoke-virtual {p0, v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarIcon(ILandroid/graphics/drawable/Drawable;)Z

    .line 1897
    invoke-direct {p0, v0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setDescription(ILjava/lang/String;)V

    .line 1898
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$RecentShareVia;

    invoke-direct {v0, p0, p1, p3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$RecentShareVia;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Landroid/graphics/drawable/Drawable;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$RecentShareViaCallback;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mRecentShareVia:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$RecentShareVia;

    .line 1899
    return-void
.end method

.method public setSave(Z)V
    .locals 0
    .param p1, "isEnabledSave"    # Z

    .prologue
    .line 991
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mSaveState:Z

    .line 992
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$4;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 354
    return-void
.end method

.method public showPreviousGone()V
    .locals 3

    .prologue
    .line 1115
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/16 v2, 0x11

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1116
    .local v0, "showPrevious":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1118
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1120
    :cond_0
    return-void
.end method

.method public showPreviousOn()V
    .locals 3

    .prologue
    .line 1123
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/16 v2, 0x11

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1124
    .local v0, "showPrevious":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1126
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1127
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1129
    :cond_0
    return-void
.end method

.method public unableBrush()V
    .locals 4

    .prologue
    .line 1016
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/16 v2, 0xe

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1017
    .local v0, "overflow":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1021
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1022
    const v2, 0x7f090005

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1023
    const/4 v3, 0x0

    .line 1019
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1025
    :cond_0
    return-void
.end method

.method public unableCancel()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1356
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCancelState:Z

    .line 1357
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1358
    .local v0, "cancel":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1362
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1363
    const v2, 0x7f090005

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1360
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1366
    :cond_0
    return-void
.end method

.method public unableCancelExceptAbleList()V
    .locals 4

    .prologue
    .line 1382
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1383
    .local v0, "cancel":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1387
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1388
    const v2, 0x7f090005

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1389
    const/4 v3, 0x0

    .line 1385
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1391
    :cond_0
    return-void
.end method

.method public unableDone()V
    .locals 5

    .prologue
    const v4, 0x7f090005

    const/4 v3, 0x0

    .line 921
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->doneState:Z

    .line 922
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 923
    .local v0, "done":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 927
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 928
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 925
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 930
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string v2, "Disabled"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 931
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHoveringWithoutPopUpDisplay(Landroid/content/Context;Landroid/view/View;)V

    .line 932
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 934
    :cond_0
    return-void
.end method

.method public unableHome()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1263
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1264
    .local v0, "home":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1268
    const v1, 0x7f090007

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1269
    const/4 v2, 0x0

    .line 1266
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1272
    :cond_0
    return-void
.end method

.method public unableOption()V
    .locals 4

    .prologue
    .line 1145
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/16 v2, 0xc

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1146
    .local v0, "overflow":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1150
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1151
    const v2, 0x7f090005

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1152
    const/4 v3, 0x0

    .line 1148
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1154
    :cond_0
    return-void
.end method

.method public unableRedo()V
    .locals 4

    .prologue
    .line 1213
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1214
    .local v0, "redo":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1218
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1219
    const/4 v2, 0x0

    .line 1220
    const/4 v3, 0x0

    .line 1216
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1222
    :cond_0
    return-void
.end method

.method public unableRedoExceptAbleList()V
    .locals 4

    .prologue
    .line 1237
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1238
    .local v0, "redo":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1242
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1243
    const/4 v2, 0x0

    .line 1244
    const/4 v3, 0x0

    .line 1240
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1246
    :cond_0
    return-void
.end method

.method public unableSave()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1290
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mSaveState:Z

    .line 1291
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1292
    .local v0, "save":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1296
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1297
    const v2, 0x7f090005

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1294
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1300
    :cond_0
    return-void
.end method

.method public unableSaveExceptAbleList()V
    .locals 4

    .prologue
    .line 1329
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1330
    .local v0, "save":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1334
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1335
    const v2, 0x7f090005

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1336
    const/4 v3, 0x0

    .line 1332
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1338
    :cond_0
    return-void
.end method

.method public unableUndo()V
    .locals 4

    .prologue
    .line 1179
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1180
    .local v0, "undo":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1183
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1184
    const/4 v2, 0x0

    .line 1185
    const/4 v3, 0x0

    .line 1182
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1187
    :cond_0
    return-void
.end method

.method public unableUndoExceptAbleList()V
    .locals 4

    .prologue
    .line 1190
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->findButton(Ljava/util/ArrayList;I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1191
    .local v0, "undo":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 1194
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1195
    const/4 v2, 0x0

    .line 1196
    const/4 v3, 0x0

    .line 1193
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabledExceptAbleList(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 1198
    :cond_0
    return-void
.end method
