.class Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;
.super Ljava/lang/Object;
.source "NormalView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect$OnCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    .line 712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterApplyPreview()V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 721
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    move-result-object v3

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 723
    .local v1, "output":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    move-result-object v3

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v3

    if-nez v3, :cond_0

    .line 725
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    move-result-object v3

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/graphics/Bitmap;

    move-result-object v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v3, v4, v8}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 726
    .local v10, "bitmap":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    move-result-object v3

    invoke-static {v3, v10}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$33(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Landroid/graphics/Bitmap;)V

    .line 727
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    move-result-object v3

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPreviewBitmap(Landroid/graphics/Bitmap;)V

    .line 728
    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 733
    .end local v10    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    move-result-object v3

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 734
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    move-result-object v3

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 737
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    move-result-object v3

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 740
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 741
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 743
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$34(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/graphics/Paint;

    move-result-object v9

    move v4, v2

    move v5, v2

    .line 735
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 744
    return-void
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 716
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->invalidateViewsWithThread()V

    .line 717
    return-void
.end method
