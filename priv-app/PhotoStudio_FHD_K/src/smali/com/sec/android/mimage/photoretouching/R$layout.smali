.class public final Lcom/sec/android/mimage/photoretouching/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final actionbar_brush_layout:I = 0x7f030000

.field public static final actionbar_cancel_layout:I = 0x7f030001

.field public static final actionbar_crop_rotate_layout:I = 0x7f030002

.field public static final actionbar_done_layout:I = 0x7f030003

.field public static final actionbar_home_layout:I = 0x7f030004

.field public static final actionbar_layout:I = 0x7f030005

.field public static final actionbar_layout_launcher:I = 0x7f030006

.field public static final actionbar_mirror_layout:I = 0x7f030007

.field public static final actionbar_overflow_layout:I = 0x7f030008

.field public static final actionbar_recent_share_via_layout:I = 0x7f030009

.field public static final actionbar_redo_layout:I = 0x7f03000a

.field public static final actionbar_save_layout:I = 0x7f03000b

.field public static final actionbar_scale_info_layout:I = 0x7f03000c

.field public static final actionbar_share_via_layout:I = 0x7f03000d

.field public static final actionbar_show_previous_layout:I = 0x7f03000e

.field public static final actionbar_undo_layout:I = 0x7f03000f

.field public static final alert_dialog_title:I = 0x7f030010

.field public static final body_button_layout:I = 0x7f030011

.field public static final body_button_sharevia_layout:I = 0x7f030012

.field public static final body_lastbutton_layout:I = 0x7f030013

.field public static final body_lastbutton_sharevia_layout:I = 0x7f030014

.field public static final body_notify_layout:I = 0x7f030015

.field public static final body_notify_photoeditor_intro_layout:I = 0x7f030016

.field public static final body_question_layout:I = 0x7f030017

.field public static final body_radio_button_layout:I = 0x7f030018

.field public static final body_radio_lastbutton_layout:I = 0x7f030019

.field public static final body_save_layout:I = 0x7f03001a

.field public static final body_seekbar_button_layout:I = 0x7f03001b

.field public static final body_sticker_layout:I = 0x7f03001c

.field public static final body_sticker_pager_layout:I = 0x7f03001d

.field public static final body_text_layout:I = 0x7f03001e

.field public static final body_thumbnail_layout:I = 0x7f03001f

.field public static final body_thumbnail_small_layout:I = 0x7f030020

.field public static final bottom_button_divide_layout:I = 0x7f030021

.field public static final bottom_collage_bg_button_with_bg:I = 0x7f030022

.field public static final bottom_collage_button_with_bg:I = 0x7f030023

.field public static final bottom_collage_layout_button_with_bg:I = 0x7f030024

.field public static final bottom_copyto_button:I = 0x7f030025

.field public static final bottom_crop_button:I = 0x7f030026

.field public static final bottom_fill_layout_layout:I = 0x7f030027

.field public static final bottom_fillparent_width_button_layout:I = 0x7f030028

.field public static final bottom_frame_button_layout:I = 0x7f030029

.field public static final bottom_icon_and_max_two_line_text_button_layout:I = 0x7f03002a

.field public static final bottom_icon_button_framelayout:I = 0x7f03002b

.field public static final bottom_icon_button_layout:I = 0x7f03002c

.field public static final bottom_icon_multi_grid_button_layout:I = 0x7f03002d

.field public static final bottom_pen_button_background:I = 0x7f03002e

.field public static final bottom_pen_button_layout:I = 0x7f03002f

.field public static final bottom_pen_selection_button:I = 0x7f030030

.field public static final bottom_rotate_sticker_selection_button:I = 0x7f030031

.field public static final bottom_rotate_sticker_selection_button_with_bg:I = 0x7f030032

.field public static final bottom_sticker_button_background:I = 0x7f030033

.field public static final bottom_thumbnail_button_layout:I = 0x7f030034

.field public static final bottom_thumbnail_managerbutton_layout:I = 0x7f030035

.field public static final collage_colorpicker_popup:I = 0x7f030036

.field public static final collage_modify_popup:I = 0x7f030037

.field public static final color_icon:I = 0x7f030038

.field public static final decoration_icon_button_layout:I = 0x7f030039

.field public static final decoration_indication:I = 0x7f03003a

.field public static final default_dialog_horizontal_layout:I = 0x7f03003b

.field public static final default_dialog_layout:I = 0x7f03003c

.field public static final default_dialog_layout_notitle:I = 0x7f03003d

.field public static final editor:I = 0x7f03003e

.field public static final editor_main_view_layout:I = 0x7f03003f

.field public static final effect_manager_layout:I = 0x7f030040

.field public static final effect_manager_thumbnail:I = 0x7f030041

.field public static final frame:I = 0x7f030042

.field public static final frame_button_layout:I = 0x7f030043

.field public static final frame_flipper:I = 0x7f030044

.field public static final frame_icon_5:I = 0x7f030045

.field public static final gridview:I = 0x7f030046

.field public static final label_layout:I = 0x7f030047

.field public static final launcher:I = 0x7f030048

.field public static final launcher_icon_1:I = 0x7f030049

.field public static final launcher_icon_2:I = 0x7f03004a

.field public static final launcher_icon_3:I = 0x7f03004b

.field public static final launcher_icon_4:I = 0x7f03004c

.field public static final launcher_icon_5:I = 0x7f03004d

.field public static final launcher_thumbnail_view:I = 0x7f03004e

.field public static final multigrid_editor:I = 0x7f03004f

.field public static final multigrid_split_button:I = 0x7f030050

.field public static final multigrid_style_button:I = 0x7f030051

.field public static final no_recent_layout:I = 0x7f030052

.field public static final pen_editor:I = 0x7f030053

.field public static final popup_help_layout_airbrushface:I = 0x7f030054

.field public static final popup_help_layout_collage_border:I = 0x7f030055

.field public static final popup_help_layout_collage_view:I = 0x7f030056

.field public static final popup_help_layout_color_before:I = 0x7f030057

.field public static final popup_help_layout_color_view:I = 0x7f030058

.field public static final popup_help_layout_crop_view:I = 0x7f030059

.field public static final popup_help_layout_dont_know_blur:I = 0x7f03005a

.field public static final popup_help_layout_effect_before:I = 0x7f03005b

.field public static final popup_help_layout_effect_view:I = 0x7f03005c

.field public static final popup_help_layout_facebrightness:I = 0x7f03005d

.field public static final popup_help_layout_frame_view:I = 0x7f03005e

.field public static final popup_help_layout_mirror_view:I = 0x7f03005f

.field public static final popup_help_layout_normal_view:I = 0x7f030060

.field public static final popup_help_layout_out_of_focus:I = 0x7f030061

.field public static final popup_help_layout_redeye_fix:I = 0x7f030062

.field public static final popup_help_layout_rotate_view:I = 0x7f030063

.field public static final popup_help_layout_seletion_view:I = 0x7f030064

.field public static final popup_help_layout_stamp_view:I = 0x7f030065

.field public static final recent_button_layout:I = 0x7f030066

.field public static final recent_thumbnail_button_layout:I = 0x7f030067

.field public static final set_as_gridview:I = 0x7f030068

.field public static final set_as_picker:I = 0x7f030069

.field public static final share_via_gridview:I = 0x7f03006a

.field public static final share_via_picker:I = 0x7f03006b

.field public static final stamp_button_layout:I = 0x7f03006c

.field public static final sticker_gridview:I = 0x7f03006d

.field public static final sticker_item_layout:I = 0x7f03006e

.field public static final tab_host:I = 0x7f03006f

.field public static final tray_add_button_layout:I = 0x7f030070

.field public static final tray_button_layout:I = 0x7f030071

.field public static final tray_layout:I = 0x7f030072

.field public static final tray_skelton_slidingdrawer:I = 0x7f030073

.field public static final tray_skelton_slidingdrawer_combined_arrow:I = 0x7f030074

.field public static final view_pager:I = 0x7f030075


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2917
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
