.class public interface abstract Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;
.super Ljava/lang/Object;
.source "RotateEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RotateCallback"
.end annotation


# virtual methods
.method public abstract ableDone()V
.end method

.method public abstract actionBarIsEnableDone()Z
.end method

.method public abstract getImageEditViewHeight()I
.end method

.method public abstract getImageEditViewWidth()I
.end method

.method public abstract invalidate()V
.end method

.method public abstract setBottomButtonEnabled(Z)V
.end method

.method public abstract setEnabledWithChildren(Z)V
.end method

.method public abstract unableDone()V
.end method
