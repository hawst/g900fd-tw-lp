.class Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;
.super Ljava/lang/Object;
.source "CopyToView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->onShow(Landroid/content/DialogInterface;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

.field private final synthetic val$arg0:Landroid/content/DialogInterface;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->val$arg0:Landroid/content/DialogInterface;

    .line 648
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v9, 0x7f0600e5

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 652
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->val$arg0:Landroid/content/DialogInterface;

    check-cast v5, Landroid/app/Dialog;

    const v6, 0x7f09001f

    invoke-virtual {v5, v6}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;

    .line 654
    .local v0, "eView":Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v5

    .line 655
    const-string v6, "input_method"

    .line 654
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 656
    .local v2, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    invoke-virtual {v2, v5, v8}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 658
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 659
    .local v1, "fileName":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    sget-object v5, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-direct {v3, v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    .local v3, "saveFile":Ljava/io/File;
    if-eqz v0, :cond_0

    .line 662
    if-nez v1, :cond_1

    .line 663
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 664
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 666
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 667
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 668
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    invoke-static {v5, v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    .line 697
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 698
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v5

    const/16 v6, 0x10

    invoke-virtual {v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonVisible(I)V

    .line 702
    return-void

    .line 671
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_2

    .line 672
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 673
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 675
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 676
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 677
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    invoke-static {v5, v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    goto :goto_0

    .line 680
    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 681
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0600da

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 682
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 684
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 685
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 686
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    invoke-static {v5, v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    goto/16 :goto_0

    .line 691
    :cond_3
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V

    .line 692
    .local v4, "task":Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    aput-object v1, v5, v8

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 693
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9$1;->val$arg0:Landroid/content/DialogInterface;

    invoke-interface {v5}, Landroid/content/DialogInterface;->cancel()V

    goto/16 :goto_0
.end method
