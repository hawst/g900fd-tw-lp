.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$WaitingThread;
.super Ljava/lang/Thread;
.source "PenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WaitingThread"
.end annotation


# instance fields
.field height:I

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

.field waitingTime:I

.field width:I


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;III)V
    .locals 1
    .param p2, "time"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 1425
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$WaitingThread;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1422
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$WaitingThread;->waitingTime:I

    .line 1426
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$WaitingThread;->waitingTime:I

    .line 1427
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$WaitingThread;->width:I

    .line 1428
    iput p4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$WaitingThread;->height:I

    .line 1429
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/pen/PenActivity$WaitingThread;)Lcom/sec/android/mimage/photoretouching/pen/PenActivity;
    .locals 1

    .prologue
    .line 1421
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$WaitingThread;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1433
    :try_start_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$WaitingThread;->waitingTime:I

    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1438
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$WaitingThread;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$WaitingThread$1;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$WaitingThread$1;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity$WaitingThread;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1444
    return-void

    .line 1434
    :catch_0
    move-exception v0

    .line 1435
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
