.class Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;
.super Ljava/lang/Object;
.source "MultiGridActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CurrentCollageState"
.end annotation


# instance fields
.field public mCurrentBackgroundColor:I

.field public mCurrentBackgroundID:I

.field public mCurrentLayout:I

.field public mCurrentProportionID:I

.field public mCurrentSaveSize:I

.field public mCurrentTouchedIndex:I

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 969
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 971
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentTouchedIndex:I

    .line 972
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentProportionID:I

    .line 973
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentLayout:I

    .line 974
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentBackgroundID:I

    .line 975
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentBackgroundColor:I

    .line 976
    const v0, 0x7f0601cb

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;->mCurrentSaveSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;)V
    .locals 0

    .prologue
    .line 969
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CurrentCollageState;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)V

    return-void
.end method
