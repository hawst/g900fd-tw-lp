.class public Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;
.super Landroid/widget/TextView;
.source "BottomButtonTextView.java"


# instance fields
.field private final BIG_TEXT_SIZE:I

.field private final MID_TEXT_SIZE:I

.field private mContext:Landroid/content/Context;

.field private mDP:F

.field private mDensity:F

.field private mPaint:Landroid/graphics/Paint;

.field private mStep:I

.field private mTextSize:F

.field private onDraw:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 18
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 15
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->onDraw:Z

    .line 98
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mStep:I

    .line 100
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mContext:Landroid/content/Context;

    .line 101
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mPaint:Landroid/graphics/Paint;

    .line 102
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mTextSize:F

    .line 103
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mDensity:F

    .line 104
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->BIG_TEXT_SIZE:I

    .line 105
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->MID_TEXT_SIZE:I

    .line 106
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mDP:F

    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->init(Landroid/content/Context;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->onDraw:Z

    .line 98
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mStep:I

    .line 100
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mContext:Landroid/content/Context;

    .line 101
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mPaint:Landroid/graphics/Paint;

    .line 102
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mTextSize:F

    .line 103
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mDensity:F

    .line 104
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->BIG_TEXT_SIZE:I

    .line 105
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->MID_TEXT_SIZE:I

    .line 106
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mDP:F

    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->init(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->onDraw:Z

    .line 98
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mStep:I

    .line 100
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mContext:Landroid/content/Context;

    .line 101
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mPaint:Landroid/graphics/Paint;

    .line 102
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mTextSize:F

    .line 103
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mDensity:F

    .line 104
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->BIG_TEXT_SIZE:I

    .line 105
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->MID_TEXT_SIZE:I

    .line 106
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mDP:F

    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->init(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mContext:Landroid/content/Context;

    .line 36
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mPaint:Landroid/graphics/Paint;

    .line 37
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mPaint:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mDensity:F

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->getTextSize()F

    move-result v2

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mDensity:F

    div-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mTextSize:F

    .line 41
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mTextSize:F

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 43
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->setSingleLine(Z)V

    .line 44
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 45
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 46
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 47
    iget v2, v1, Landroid/util/DisplayMetrics;->density:F

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mDP:F

    .line 48
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v3, 0x0

    .line 52
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 53
    .local v0, "tv":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 55
    invoke-virtual {v0, v3, v3}, Landroid/widget/TextView;->measure(II)V

    .line 59
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 88
    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    invoke-virtual {p0, v3, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->setTextSize(IF)V

    .line 89
    invoke-virtual {v0}, Landroid/widget/TextView;->getLineCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 91
    invoke-virtual {v0}, Landroid/widget/TextView;->getLineCount()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->setLines(I)V

    .line 93
    :cond_0
    const/4 v0, 0x0

    .line 95
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 97
    return-void

    .line 61
    :cond_1
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mStep:I

    packed-switch v1, :pswitch_data_0

    .line 82
    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 83
    invoke-virtual {v0, v3, v3}, Landroid/widget/TextView;->measure(II)V

    goto :goto_0

    .line 64
    :pswitch_0
    const/high16 v1, 0x41500000    # 13.0f

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mDP:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 65
    invoke-virtual {v0, v3, v3}, Landroid/widget/TextView;->measure(II)V

    .line 67
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mStep:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mStep:I

    goto :goto_0

    .line 70
    :pswitch_1
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLines(I)V

    .line 71
    invoke-virtual {v0, v3, v3}, Landroid/widget/TextView;->measure(II)V

    .line 73
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mStep:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mStep:I

    goto :goto_0

    .line 76
    :pswitch_2
    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mDP:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 77
    invoke-virtual {v0, v3, v3}, Landroid/widget/TextView;->measure(II)V

    .line 78
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mStep:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonTextView;->mStep:I

    goto :goto_0

    .line 61
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
