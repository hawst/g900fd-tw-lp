.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$19;
.super Ljava/lang/Object;
.source "PenActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->newSpenListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$19;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    .line 1265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged(III)V
    .locals 3
    .param p1, "color"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 1269
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$19;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1270
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$19;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/pen/util/SettingView;->getSpenPenInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v0

    .line 1271
    .local v0, "penInfo":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    iput p1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 1272
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$19;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/pen/util/SettingView;->setSpenPenInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 1273
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$19;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/pen/util/SettingView;->getTextInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v1

    .line 1274
    .local v1, "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    iput p1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    .line 1275
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$19;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/pen/util/SettingView;->setTextInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    .line 1277
    .end local v0    # "penInfo":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .end local v1    # "textInfo":Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    :cond_0
    return-void
.end method
