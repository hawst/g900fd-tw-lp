.class Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;
.super Ljava/lang/Thread;
.source "PortraitView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimationThread"
.end annotation


# instance fields
.field private mDrawBitmap:Landroid/graphics/Bitmap;

.field private mDrawPos:Landroid/graphics/Rect;

.field private mIconAnimation:Z

.field private mKill:Z

.field private mStopThread:Z

.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;FF)V
    .locals 10
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 2644
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    .line 2643
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2634
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mDrawBitmap:Landroid/graphics/Bitmap;

    .line 2637
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mIconAnimation:Z

    .line 2638
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mStopThread:Z

    .line 2639
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mKill:Z

    .line 2642
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mDrawPos:Landroid/graphics/Rect;

    .line 2645
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    move-result-object v6

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v5

    .line 2646
    .local v5, "viewTransform":Landroid/graphics/Matrix;
    const/16 v6, 0x9

    new-array v4, v6, [F

    .line 2648
    .local v4, "values":[F
    invoke-virtual {v5, v4}, Landroid/graphics/Matrix;->getValues([F)V

    .line 2649
    aget v1, v4, v9

    .line 2650
    .local v1, "scaleX":F
    const/4 v6, 0x4

    aget v2, v4, v6

    .line 2651
    .local v2, "scaleY":F
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mDrawPos:Landroid/graphics/Rect;

    .line 2652
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    move-result-object v6

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    add-float/2addr v6, p2

    mul-float/2addr v6, v1

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationFrame:[Landroid/graphics/Bitmap;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)[Landroid/graphics/Bitmap;

    move-result-object v7

    aget-object v7, v7, v9

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    sub-float/2addr v6, v7

    float-to-int v0, v6

    .line 2653
    .local v0, "left":I
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    move-result-object v6

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    add-float/2addr v6, p3

    mul-float/2addr v6, v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationFrame:[Landroid/graphics/Bitmap;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)[Landroid/graphics/Bitmap;

    move-result-object v7

    aget-object v7, v7, v9

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    sub-float/2addr v6, v7

    float-to-int v3, v6

    .line 2654
    .local v3, "top":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mDrawPos:Landroid/graphics/Rect;

    .line 2656
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationFrame:[Landroid/graphics/Bitmap;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)[Landroid/graphics/Bitmap;

    move-result-object v7

    aget-object v7, v7, v9

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/2addr v7, v0

    .line 2657
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationFrame:[Landroid/graphics/Bitmap;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)[Landroid/graphics/Bitmap;

    move-result-object v8

    aget-object v8, v8, v9

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    add-int/2addr v8, v3

    .line 2654
    invoke-virtual {v6, v0, v3, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 2658
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;[I[III)V
    .locals 2
    .param p2, "previous"    # [I
    .param p3, "current"    # [I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2660
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    .line 2659
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2634
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mDrawBitmap:Landroid/graphics/Bitmap;

    .line 2637
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mIconAnimation:Z

    .line 2638
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mStopThread:Z

    .line 2639
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mKill:Z

    .line 2642
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mDrawPos:Landroid/graphics/Rect;

    .line 2734
    return-void
.end method

.method private free()V
    .locals 3

    .prologue
    .line 2738
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mDrawBitmap:Landroid/graphics/Bitmap;

    .line 2739
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationThreadList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2744
    return-void

    .line 2739
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;

    .line 2741
    .local v0, "thread":Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;
    if-ne v0, p0, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public drawCanvas(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v3, 0x0

    .line 2796
    const-string v0, "drawCanvas"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2797
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mDrawBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 2799
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mDrawBitmap:Landroid/graphics/Bitmap;

    .line 2800
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mDrawPos:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    .line 2801
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mDrawPos:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    .line 2799
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2803
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mIconAnimation:Z

    if-nez v0, :cond_0

    .line 2804
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mDrawBitmap:Landroid/graphics/Bitmap;

    .line 2807
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mStopThread:Z

    if-eqz v0, :cond_1

    .line 2809
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->free()V

    .line 2810
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->invalidateViewsWithThread()V

    .line 2812
    :cond_1
    return-void
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 2824
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mStopThread:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public killThread()V
    .locals 1

    .prologue
    .line 2815
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mKill:Z

    .line 2816
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->wakeUp()V

    .line 2817
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2748
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 2749
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mStopThread:Z

    .line 2750
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mIconAnimation:Z

    .line 2751
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mId:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)[I

    move-result-object v2

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 2768
    :goto_1
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mIconAnimation:Z

    .line 2785
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mStopThread:Z

    .line 2786
    return-void

    .line 2753
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mKill:Z

    if-eqz v2, :cond_1

    .line 2755
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->free()V

    goto :goto_1

    .line 2758
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationFrame:[Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)[Landroid/graphics/Bitmap;

    move-result-object v2

    aget-object v2, v2, v1

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->mDrawBitmap:Landroid/graphics/Bitmap;

    .line 2759
    const-string v2, "run()"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2761
    const/16 v2, 0x3e8

    :try_start_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mId:[I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)[I

    move-result-object v3

    array-length v3, v3

    div-int/2addr v2, v3

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2766
    :goto_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->invalidateViewsWithThread()V

    .line 2751
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2762
    :catch_0
    move-exception v0

    .line 2763
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2
.end method

.method public declared-synchronized wakeUp()V
    .locals 1

    .prologue
    .line 2820
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2821
    monitor-exit p0

    return-void

    .line 2820
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
