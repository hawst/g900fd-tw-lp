.class Lcom/sec/android/mimage/photoretouching/PhotoRetouching$LocalBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PhotoRetouching.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/PhotoRetouching;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LocalBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)V
    .locals 0

    .prologue
    .line 1238
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$LocalBroadcastReceiver;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 1242
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1243
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$LocalBroadcastReceiver;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    # invokes: Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getCurrentFilePath()Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->access$6(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)Ljava/lang/String;

    move-result-object v1

    .line 1244
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1246
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1247
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1248
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$LocalBroadcastReceiver;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->finish()V

    .line 1252
    .end local v0    # "f":Ljava/io/File;
    .end local v1    # "path":Ljava/lang/String;
    :cond_0
    return-void
.end method
