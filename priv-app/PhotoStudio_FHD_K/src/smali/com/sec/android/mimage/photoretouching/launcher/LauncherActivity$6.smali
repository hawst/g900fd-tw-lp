.class Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$6;
.super Ljava/lang/Object;
.source "LauncherActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->registerButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$6;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    .line 398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 401
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$6;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v2, v4}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->SetEnableBtn(Z)V

    .line 403
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$6;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/MemoryStatus;->GetExternalStorageMount(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 404
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$6;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->finish()V

    .line 417
    :goto_0
    return-void

    .line 408
    :cond_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/MemoryStatus;->getAvailableExternalMemorySize()J

    move-result-wide v0

    .line 409
    .local v0, "available_memsize":J
    const-wide/32 v2, 0x6e45000

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 411
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "memory size = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " %"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 412
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$6;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    const-string v3, "Not enough available memory to launch application"

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 413
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$6;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->SetEnableBtn(Z)V

    goto :goto_0

    .line 416
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$6;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->openFromGallery()V

    goto :goto_0
.end method
