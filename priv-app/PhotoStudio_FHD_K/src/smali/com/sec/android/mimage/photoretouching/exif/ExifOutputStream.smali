.class Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;
.super Ljava/io/FilterOutputStream;
.source "ExifOutputStream.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final EXIF_HEADER:I = 0x45786966

.field private static final MAX_EXIF_SIZE:I = 0xffff

.field private static final STATE_FRAME_HEADER:I = 0x1

.field private static final STATE_JPEG_DATA:I = 0x2

.field private static final STATE_SOI:I = 0x0

.field private static final STREAMBUFFER_SIZE:I = 0x10000

.field private static final TAG:Ljava/lang/String; = "ExifOutputStream"

.field private static final TAG_SIZE:S = 0xcs

.field private static final TIFF_BIG_ENDIAN:S = 0x4d4ds

.field private static final TIFF_HEADER:S = 0x2as

.field private static final TIFF_HEADER_SIZE:S = 0x8s

.field private static final TIFF_LITTLE_ENDIAN:S = 0x4949s


# instance fields
.field private mBuffer:Ljava/nio/ByteBuffer;

.field private mByteToCopy:I

.field private mByteToSkip:I

.field private mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

.field private final mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

.field private mSingleByteArray:[B

.field private mState:I


# direct methods
.method protected constructor <init>(Ljava/io/OutputStream;Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;)V
    .locals 2
    .param p1, "ou"    # Ljava/io/OutputStream;
    .param p2, "iRef"    # Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    .prologue
    .line 72
    new-instance v0, Ljava/io/BufferedOutputStream;

    const/high16 v1, 0x10000

    invoke-direct {v0, p1, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    invoke-direct {p0, v0}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mState:I

    .line 67
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mSingleByteArray:[B

    .line 68
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    .line 73
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    .line 74
    return-void
.end method

.method private calculateAllOffset()I
    .locals 12

    .prologue
    .line 426
    const/16 v6, 0x8

    .line 427
    .local v6, "offset":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v3

    .line 428
    .local v3, "ifd0":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    invoke-direct {p0, v3, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->calculateOffsetOfIfd(Lcom/sec/android/mimage/photoretouching/exif/IfdData;I)I

    move-result v6

    .line 429
    sget v9, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXIF_IFD:I

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v9

    invoke-virtual {v3, v9}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getTag(S)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v9

    invoke-virtual {v9, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(I)Z

    .line 431
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v0

    .line 432
    .local v0, "exifIfd":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    invoke-direct {p0, v0, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->calculateOffsetOfIfd(Lcom/sec/android/mimage/photoretouching/exif/IfdData;I)I

    move-result v6

    .line 434
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/4 v10, 0x3

    invoke-virtual {v9, v10}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v5

    .line 435
    .local v5, "interIfd":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-eqz v5, :cond_0

    .line 436
    sget v9, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_INTEROPERABILITY_IFD:I

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v9

    invoke-virtual {v0, v9}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getTag(S)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v9

    .line 437
    invoke-virtual {v9, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(I)Z

    .line 438
    invoke-direct {p0, v5, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->calculateOffsetOfIfd(Lcom/sec/android/mimage/photoretouching/exif/IfdData;I)I

    move-result v6

    .line 441
    :cond_0
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v1

    .line 442
    .local v1, "gpsIfd":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-eqz v1, :cond_1

    .line 443
    sget v9, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_IFD:I

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v9

    invoke-virtual {v3, v9}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getTag(S)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v9

    invoke-virtual {v9, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(I)Z

    .line 444
    invoke-direct {p0, v1, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->calculateOffsetOfIfd(Lcom/sec/android/mimage/photoretouching/exif/IfdData;I)I

    move-result v6

    .line 447
    :cond_1
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v4

    .line 448
    .local v4, "ifd1":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-eqz v4, :cond_2

    .line 449
    invoke-virtual {v3, v6}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->setOffsetToNextIfd(I)V

    .line 450
    invoke-direct {p0, v4, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->calculateOffsetOfIfd(Lcom/sec/android/mimage/photoretouching/exif/IfdData;I)I

    move-result v6

    .line 454
    :cond_2
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->hasCompressedThumbnail()Z

    move-result v9

    if-eqz v9, :cond_4

    if-eqz v4, :cond_4

    .line 455
    sget v9, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT:I

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v9

    invoke-virtual {v4, v9}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getTag(S)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v9

    .line 456
    invoke-virtual {v9, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(I)Z

    .line 457
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getCompressedThumbnail()[B

    move-result-object v9

    array-length v9, v9

    add-int/2addr v6, v9

    .line 468
    :cond_3
    :goto_0
    return v6

    .line 458
    :cond_4
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->hasUncompressedStrip()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 459
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getStripCount()I

    move-result v8

    .line 460
    .local v8, "stripCount":I
    new-array v7, v8, [J

    .line 461
    .local v7, "offsets":[J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getStripCount()I

    move-result v9

    if-lt v2, v9, :cond_5

    .line 465
    sget v9, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_OFFSETS:I

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v9

    invoke-virtual {v4, v9}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getTag(S)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v9

    invoke-virtual {v9, v7}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([J)Z

    goto :goto_0

    .line 462
    :cond_5
    int-to-long v10, v6

    aput-wide v10, v7, v2

    .line 463
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v9, v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getStrip(I)[B

    move-result-object v9

    array-length v9, v9

    add-int/2addr v6, v9

    .line 461
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private calculateOffsetOfIfd(Lcom/sec/android/mimage/photoretouching/exif/IfdData;I)I
    .locals 6
    .param p1, "ifd"    # Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    .param p2, "offset"    # I

    .prologue
    .line 301
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getTagCount()I

    move-result v2

    mul-int/lit8 v2, v2, 0xc

    add-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr p2, v2

    .line 302
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getAllTags()[Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v1

    .line 303
    .local v1, "tags":[Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_0

    .line 309
    return p2

    .line 303
    :cond_0
    aget-object v0, v1, v2

    .line 304
    .local v0, "tag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataSize()I

    move-result v4

    const/4 v5, 0x4

    if-le v4, v5, :cond_1

    .line 305
    invoke-virtual {v0, p2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setOffset(I)V

    .line 306
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataSize()I

    move-result v4

    add-int/2addr p2, v4

    .line 303
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private createRequiredIfdAndTag()V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 314
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v7

    .line 315
    .local v7, "ifd0":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-nez v7, :cond_0

    .line 316
    new-instance v7, Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    .end local v7    # "ifd0":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    const/4 v15, 0x0

    invoke-direct {v7, v15}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;-><init>(I)V

    .line 317
    .restart local v7    # "ifd0":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v15, v7}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->addIfdData(Lcom/sec/android/mimage/photoretouching/exif/IfdData;)V

    .line 319
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    sget v16, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXIF_IFD:I

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->buildUninitializedTag(I)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v3

    .line 320
    .local v3, "exifOffsetTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v3, :cond_1

    .line 321
    new-instance v15, Ljava/io/IOException;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "No definition for crucial exif tag: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 322
    sget v17, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXIF_IFD:I

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 321
    invoke-direct/range {v15 .. v16}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 324
    :cond_1
    invoke-virtual {v7, v3}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 327
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/16 v16, 0x2

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v2

    .line 328
    .local v2, "exifIfd":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-nez v2, :cond_2

    .line 329
    new-instance v2, Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    .end local v2    # "exifIfd":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    const/4 v15, 0x2

    invoke-direct {v2, v15}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;-><init>(I)V

    .line 330
    .restart local v2    # "exifIfd":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v15, v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->addIfdData(Lcom/sec/android/mimage/photoretouching/exif/IfdData;)V

    .line 334
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/16 v16, 0x4

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v4

    .line 335
    .local v4, "gpsIfd":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-eqz v4, :cond_4

    .line 336
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    sget v16, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_IFD:I

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->buildUninitializedTag(I)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v5

    .line 337
    .local v5, "gpsOffsetTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v5, :cond_3

    .line 338
    new-instance v15, Ljava/io/IOException;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "No definition for crucial exif tag: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 339
    sget v17, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_IFD:I

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 338
    invoke-direct/range {v15 .. v16}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 341
    :cond_3
    invoke-virtual {v7, v5}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 345
    .end local v5    # "gpsOffsetTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/16 v16, 0x3

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v9

    .line 346
    .local v9, "interIfd":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-eqz v9, :cond_6

    .line 347
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    .line 348
    sget v16, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_INTEROPERABILITY_IFD:I

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->buildUninitializedTag(I)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v10

    .line 349
    .local v10, "interOffsetTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v10, :cond_5

    .line 350
    new-instance v15, Ljava/io/IOException;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "No definition for crucial exif tag: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 351
    sget v17, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_INTEROPERABILITY_IFD:I

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 350
    invoke-direct/range {v15 .. v16}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 353
    :cond_5
    invoke-virtual {v2, v10}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 356
    .end local v10    # "interOffsetTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v8

    .line 359
    .local v8, "ifd1":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->hasCompressedThumbnail()Z

    move-result v15

    if-eqz v15, :cond_b

    .line 361
    if-nez v8, :cond_7

    .line 362
    new-instance v8, Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    .end local v8    # "ifd1":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    const/4 v15, 0x1

    invoke-direct {v8, v15}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;-><init>(I)V

    .line 363
    .restart local v8    # "ifd1":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v15, v8}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->addIfdData(Lcom/sec/android/mimage/photoretouching/exif/IfdData;)V

    .line 366
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    .line 367
    sget v16, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT:I

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->buildUninitializedTag(I)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v13

    .line 368
    .local v13, "offsetTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v13, :cond_8

    .line 369
    new-instance v15, Ljava/io/IOException;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "No definition for crucial exif tag: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 370
    sget v17, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT:I

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 369
    invoke-direct/range {v15 .. v16}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 373
    :cond_8
    invoke-virtual {v8, v13}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 374
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    .line 375
    sget v16, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT_LENGTH:I

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->buildUninitializedTag(I)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v11

    .line 376
    .local v11, "lengthTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v11, :cond_9

    .line 377
    new-instance v15, Ljava/io/IOException;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "No definition for crucial exif tag: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 378
    sget v17, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT_LENGTH:I

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 377
    invoke-direct/range {v15 .. v16}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 381
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getCompressedThumbnail()[B

    move-result-object v15

    array-length v15, v15

    invoke-virtual {v11, v15}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(I)Z

    .line 382
    invoke-virtual {v8, v11}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 385
    sget v15, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_OFFSETS:I

    invoke-static {v15}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v15

    invoke-virtual {v8, v15}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->removeTag(S)V

    .line 386
    sget v15, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_BYTE_COUNTS:I

    invoke-static {v15}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v15

    invoke-virtual {v8, v15}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->removeTag(S)V

    .line 423
    .end local v11    # "lengthTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .end local v13    # "offsetTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :cond_a
    :goto_0
    return-void

    .line 387
    :cond_b
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->hasUncompressedStrip()Z

    move-result v15

    if-eqz v15, :cond_10

    .line 388
    if-nez v8, :cond_c

    .line 389
    new-instance v8, Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    .end local v8    # "ifd1":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    const/4 v15, 0x1

    invoke-direct {v8, v15}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;-><init>(I)V

    .line 390
    .restart local v8    # "ifd1":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v15, v8}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->addIfdData(Lcom/sec/android/mimage/photoretouching/exif/IfdData;)V

    .line 392
    :cond_c
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getStripCount()I

    move-result v14

    .line 393
    .local v14, "stripCount":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    sget v16, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_OFFSETS:I

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->buildUninitializedTag(I)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v13

    .line 394
    .restart local v13    # "offsetTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v13, :cond_d

    .line 395
    new-instance v15, Ljava/io/IOException;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "No definition for crucial exif tag: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 396
    sget v17, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_OFFSETS:I

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 395
    invoke-direct/range {v15 .. v16}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 398
    :cond_d
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    .line 399
    sget v16, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_BYTE_COUNTS:I

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->buildUninitializedTag(I)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v11

    .line 400
    .restart local v11    # "lengthTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v11, :cond_e

    .line 401
    new-instance v15, Ljava/io/IOException;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "No definition for crucial exif tag: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 402
    sget v17, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_BYTE_COUNTS:I

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 401
    invoke-direct/range {v15 .. v16}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 404
    :cond_e
    new-array v12, v14, [J

    .line 405
    .local v12, "lengths":[J
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getStripCount()I

    move-result v15

    if-lt v6, v15, :cond_f

    .line 408
    invoke-virtual {v11, v12}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue([J)Z

    .line 409
    invoke-virtual {v8, v13}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 410
    invoke-virtual {v8, v11}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 412
    sget v15, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT:I

    invoke-static {v15}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v15

    invoke-virtual {v8, v15}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->removeTag(S)V

    .line 414
    sget v15, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT_LENGTH:I

    invoke-static {v15}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v15

    .line 413
    invoke-virtual {v8, v15}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->removeTag(S)V

    goto/16 :goto_0

    .line 406
    :cond_f
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v15, v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getStrip(I)[B

    move-result-object v15

    array-length v15, v15

    int-to-long v0, v15

    move-wide/from16 v16, v0

    aput-wide v16, v12, v6

    .line 405
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 415
    .end local v6    # "i":I
    .end local v11    # "lengthTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .end local v12    # "lengths":[J
    .end local v13    # "offsetTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .end local v14    # "stripCount":I
    :cond_10
    if-eqz v8, :cond_a

    .line 417
    sget v15, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_OFFSETS:I

    invoke-static {v15}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v15

    invoke-virtual {v8, v15}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->removeTag(S)V

    .line 418
    sget v15, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_BYTE_COUNTS:I

    invoke-static {v15}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v15

    invoke-virtual {v8, v15}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->removeTag(S)V

    .line 419
    sget v15, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT:I

    invoke-static {v15}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v15

    invoke-virtual {v8, v15}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->removeTag(S)V

    .line 421
    sget v15, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT_LENGTH:I

    invoke-static {v15}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v15

    .line 420
    invoke-virtual {v8, v15}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->removeTag(S)V

    goto/16 :goto_0
.end method

.method private requestByteToBuffer(I[BII)I
    .locals 3
    .param p1, "requestByteCount"    # I
    .param p2, "buffer"    # [B
    .param p3, "offset"    # I
    .param p4, "length"    # I

    .prologue
    .line 93
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    sub-int v0, p1, v2

    .line 94
    .local v0, "byteNeeded":I
    if-le p4, v0, :cond_0

    move v1, v0

    .line 95
    .local v1, "byteToRead":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, p2, p3, v1}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 96
    return v1

    .end local v1    # "byteToRead":I
    :cond_0
    move v1, p4

    .line 94
    goto :goto_0
.end method

.method private stripNullValueTags(Lcom/sec/android/mimage/photoretouching/exif/ExifData;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "data"    # Lcom/sec/android/mimage/photoretouching/exif/ExifData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/mimage/photoretouching/exif/ExifData;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/exif/ExifTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 231
    .local v0, "nullTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getAllTags()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_1

    .line 233
    const-string v2, "stripNullValueTags null"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 242
    :cond_0
    return-object v0

    .line 236
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getAllTags()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 237
    .local v1, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValue()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getTagId()S

    move-result v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->isOffsetTag(S)Z

    move-result v3

    if-nez v3, :cond_2

    .line 238
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getTagId()S

    move-result v3

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getIfd()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->removeTag(SI)V

    .line 239
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private writeAllTags(Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;)V
    .locals 6
    .param p1, "dataOutputStream"    # Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 256
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v3

    invoke-direct {p0, v3, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->writeIfd(Lcom/sec/android/mimage/photoretouching/exif/IfdData;Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;)V

    .line 257
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v3

    invoke-direct {p0, v3, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->writeIfd(Lcom/sec/android/mimage/photoretouching/exif/IfdData;Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;)V

    .line 258
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v2

    .line 259
    .local v2, "interoperabilityIfd":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-eqz v2, :cond_0

    .line 260
    invoke-direct {p0, v2, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->writeIfd(Lcom/sec/android/mimage/photoretouching/exif/IfdData;Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;)V

    .line 262
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v0

    .line 263
    .local v0, "gpsIfd":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-eqz v0, :cond_1

    .line 264
    invoke-direct {p0, v0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->writeIfd(Lcom/sec/android/mimage/photoretouching/exif/IfdData;Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;)V

    .line 266
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v1

    .line 267
    .local v1, "ifd1":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-eqz v1, :cond_2

    .line 268
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v3

    invoke-direct {p0, v3, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->writeIfd(Lcom/sec/android/mimage/photoretouching/exif/IfdData;Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;)V

    .line 270
    :cond_2
    return-void
.end method

.method private writeExifData()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    if-nez v4, :cond_1

    .line 227
    :cond_0
    return-void

    .line 202
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-direct {p0, v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->stripNullValueTags(Lcom/sec/android/mimage/photoretouching/exif/ExifData;)Ljava/util/ArrayList;

    move-result-object v2

    .line 203
    .local v2, "nullTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->createRequiredIfdAndTag()V

    .line 204
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->calculateAllOffset()I

    move-result v1

    .line 205
    .local v1, "exifSize":I
    add-int/lit8 v4, v1, 0x8

    const v5, 0xffff

    if-le v4, v5, :cond_2

    .line 206
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Exif header is too large (>64Kb)"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 208
    :cond_2
    new-instance v0, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->out:Ljava/io/OutputStream;

    invoke-direct {v0, v4}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 209
    .local v0, "dataOutputStream":Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;
    sget-object v4, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->setByteOrder(Ljava/nio/ByteOrder;)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 210
    const/16 v4, -0x1f

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeShort(S)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 211
    add-int/lit8 v4, v1, 0x8

    int-to-short v4, v4

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeShort(S)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 212
    const v4, 0x45786966

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeInt(I)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 213
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeShort(S)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 214
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getByteOrder()Ljava/nio/ByteOrder;

    move-result-object v4

    sget-object v5, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    if-ne v4, v5, :cond_3

    .line 215
    const/16 v4, 0x4d4d

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeShort(S)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 219
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getByteOrder()Ljava/nio/ByteOrder;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->setByteOrder(Ljava/nio/ByteOrder;)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 220
    const/16 v4, 0x2a

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeShort(S)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 221
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeInt(I)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 222
    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->writeAllTags(Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;)V

    .line 223
    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->writeThumbnail(Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;)V

    .line 224
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 225
    .local v3, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v5, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->addTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    goto :goto_1

    .line 217
    .end local v3    # "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :cond_3
    const/16 v4, 0x4949

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeShort(S)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    goto :goto_0
.end method

.method private writeIfd(Lcom/sec/android/mimage/photoretouching/exif/IfdData;Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;)V
    .locals 9
    .param p1, "ifd"    # Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    .param p2, "dataOutputStream"    # Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v4, 0x0

    .line 274
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getAllTags()[Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v3

    .line 275
    .local v3, "tags":[Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    array-length v5, v3

    int-to-short v5, v5

    invoke-virtual {p2, v5}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeShort(S)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 276
    array-length v6, v3

    move v5, v4

    :goto_0
    if-lt v5, v6, :cond_0

    .line 292
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getOffsetToNextIfd()I

    move-result v5

    invoke-virtual {p2, v5}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeInt(I)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 293
    array-length v5, v3

    :goto_1
    if-lt v4, v5, :cond_3

    .line 298
    return-void

    .line 276
    :cond_0
    aget-object v2, v3, v5

    .line 277
    .local v2, "tag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getTagId()S

    move-result v7

    invoke-virtual {p2, v7}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeShort(S)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 278
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataType()S

    move-result v7

    invoke-virtual {p2, v7}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeShort(S)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 279
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v7

    invoke-virtual {p2, v7}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeInt(I)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 283
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataSize()I

    move-result v7

    if-le v7, v8, :cond_2

    .line 284
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getOffset()I

    move-result v7

    invoke-virtual {p2, v7}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeInt(I)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 276
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 286
    :cond_2
    invoke-static {v2, p2}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->writeTagValue(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;)V

    .line 287
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataSize()I

    move-result v7

    rsub-int/lit8 v1, v7, 0x4

    .local v1, "n":I
    :goto_2
    if-ge v0, v1, :cond_1

    .line 288
    invoke-virtual {p2, v4}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->write(I)V

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 293
    .end local v0    # "i":I
    .end local v1    # "n":I
    .end local v2    # "tag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :cond_3
    aget-object v2, v3, v4

    .line 294
    .restart local v2    # "tag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataSize()I

    move-result v6

    if-le v6, v8, :cond_4

    .line 295
    invoke-static {v2, p2}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->writeTagValue(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;)V

    .line 293
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method static writeTagValue(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;)V
    .locals 6
    .param p0, "tag"    # Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .param p1, "dataOutputStream"    # Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 473
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataType()S

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 508
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 475
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getStringByte()[B

    move-result-object v0

    .line 476
    .local v0, "buf":[B
    array-length v3, v0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v4

    if-ne v3, v4, :cond_1

    .line 477
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aput-byte v5, v0, v3

    .line 478
    invoke-virtual {p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->write([B)V

    goto :goto_0

    .line 480
    :cond_1
    invoke-virtual {p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->write([B)V

    .line 481
    invoke-virtual {p1, v5}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->write(I)V

    goto :goto_0

    .line 486
    .end local v0    # "buf":[B
    :pswitch_2
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v2

    .local v2, "n":I
    :goto_1
    if-ge v1, v2, :cond_0

    .line 487
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAt(I)J

    move-result-wide v4

    long-to-int v3, v4

    invoke-virtual {p1, v3}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeInt(I)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 486
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 492
    .end local v1    # "i":I
    .end local v2    # "n":I
    :pswitch_3
    const/4 v1, 0x0

    .restart local v1    # "i":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v2

    .restart local v2    # "n":I
    :goto_2
    if-ge v1, v2, :cond_0

    .line 493
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getRational(I)Lcom/sec/android/mimage/photoretouching/exif/Rational;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeRational(Lcom/sec/android/mimage/photoretouching/exif/Rational;)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 492
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 498
    .end local v1    # "i":I
    .end local v2    # "n":I
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v3

    new-array v0, v3, [B

    .line 499
    .restart local v0    # "buf":[B
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getBytes([B)V

    .line 500
    invoke-virtual {p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->write([B)V

    goto :goto_0

    .line 503
    .end local v0    # "buf":[B
    :pswitch_5
    const/4 v1, 0x0

    .restart local v1    # "i":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v2

    .restart local v2    # "n":I
    :goto_3
    if-ge v1, v2, :cond_0

    .line 504
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAt(I)J

    move-result-wide v4

    long-to-int v3, v4

    int-to-short v3, v3

    invoke-virtual {p1, v3}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->writeShort(S)Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;

    .line 503
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 473
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private writeThumbnail(Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;)V
    .locals 2
    .param p1, "dataOutputStream"    # Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 246
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->hasCompressedThumbnail()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 247
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getCompressedThumbnail()[B

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->write([B)V

    .line 253
    :cond_0
    return-void

    .line 248
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->hasUncompressedStrip()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 249
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getStripCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 250
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getStrip(I)[B

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/sec/android/mimage/photoretouching/exif/OrderedDataOutputStream;->write([B)V

    .line 249
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected getExifData()Lcom/sec/android/mimage/photoretouching/exif/ExifData;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    return-object v0
.end method

.method protected setExifData(Lcom/sec/android/mimage/photoretouching/exif/ExifData;)V
    .locals 0
    .param p1, "exifData"    # Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mExifData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    .line 82
    return-void
.end method

.method public write(I)V
    .locals 3
    .param p1, "oneByte"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mSingleByteArray:[B

    const/4 v1, 0x0

    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 184
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mSingleByteArray:[B

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->write([B)V

    .line 185
    return-void
.end method

.method public write([B)V
    .locals 2
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->write([BII)V

    .line 193
    return-void
.end method

.method public write([BII)V
    .locals 10
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v9, 0xffff

    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x2

    .line 105
    :goto_0
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mByteToSkip:I

    if-gtz v4, :cond_0

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mByteToCopy:I

    if-gtz v4, :cond_0

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mState:I

    if-eq v4, v6, :cond_1

    :cond_0
    if-gtz p3, :cond_3

    .line 172
    :cond_1
    if-lez p3, :cond_2

    .line 173
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v4, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 175
    :cond_2
    return-void

    .line 107
    :cond_3
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mByteToSkip:I

    if-lez v4, :cond_4

    .line 108
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mByteToSkip:I

    if-le p3, v4, :cond_6

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mByteToSkip:I

    .line 109
    .local v1, "byteToProcess":I
    :goto_1
    sub-int/2addr p3, v1

    .line 110
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mByteToSkip:I

    sub-int/2addr v4, v1

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mByteToSkip:I

    .line 111
    add-int/2addr p2, v1

    .line 113
    .end local v1    # "byteToProcess":I
    :cond_4
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mByteToCopy:I

    if-lez v4, :cond_5

    .line 114
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mByteToCopy:I

    if-le p3, v4, :cond_7

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mByteToCopy:I

    .line 115
    .restart local v1    # "byteToProcess":I
    :goto_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v4, p1, p2, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 116
    sub-int/2addr p3, v1

    .line 117
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mByteToCopy:I

    sub-int/2addr v4, v1

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mByteToCopy:I

    .line 118
    add-int/2addr p2, v1

    .line 120
    .end local v1    # "byteToProcess":I
    :cond_5
    if-eqz p3, :cond_2

    .line 123
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mState:I

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 125
    :pswitch_0
    invoke-direct {p0, v6, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->requestByteToBuffer(I[BII)I

    move-result v0

    .line 126
    .local v0, "byteRead":I
    add-int/2addr p2, v0

    .line 127
    sub-int/2addr p3, v0

    .line 128
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    if-lt v4, v6, :cond_2

    .line 131
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 132
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v4

    const/16 v5, -0x28

    if-eq v4, v5, :cond_8

    .line 133
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Not a valid jpeg image, cannot write exif"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .end local v0    # "byteRead":I
    :cond_6
    move v1, p3

    .line 108
    goto :goto_1

    :cond_7
    move v1, p3

    .line 114
    goto :goto_2

    .line 135
    .restart local v0    # "byteRead":I
    :cond_8
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->out:Ljava/io/OutputStream;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v4, v5, v7, v6}, Ljava/io/OutputStream;->write([BII)V

    .line 136
    const/4 v4, 0x1

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mState:I

    .line 137
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 138
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->writeExifData()V

    goto/16 :goto_0

    .line 143
    .end local v0    # "byteRead":I
    :pswitch_1
    invoke-direct {p0, v8, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->requestByteToBuffer(I[BII)I

    move-result v0

    .line 144
    .restart local v0    # "byteRead":I
    add-int/2addr p2, v0

    .line 145
    sub-int/2addr p3, v0

    .line 147
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    if-ne v4, v6, :cond_9

    .line 148
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v3

    .line 149
    .local v3, "tag":S
    const/16 v4, -0x27

    if-ne v3, v4, :cond_9

    .line 150
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->out:Ljava/io/OutputStream;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v4, v5, v7, v6}, Ljava/io/OutputStream;->write([BII)V

    .line 151
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 154
    .end local v3    # "tag":S
    :cond_9
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    if-lt v4, v8, :cond_2

    .line 157
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 158
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v2

    .line 159
    .local v2, "marker":S
    const/16 v4, -0x1f

    if-ne v2, v4, :cond_a

    .line 160
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v4

    and-int/2addr v4, v9

    add-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mByteToSkip:I

    .line 161
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mState:I

    .line 169
    :goto_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    goto/16 :goto_0

    .line 162
    :cond_a
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/exif/JpegHeader;->isSofMarker(S)Z

    move-result v4

    if-nez v4, :cond_b

    .line 163
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->out:Ljava/io/OutputStream;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v4, v5, v7, v8}, Ljava/io/OutputStream;->write([BII)V

    .line 164
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v4

    and-int/2addr v4, v9

    add-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mByteToCopy:I

    goto :goto_3

    .line 166
    :cond_b
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->out:Ljava/io/OutputStream;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v4, v5, v7, v8}, Ljava/io/OutputStream;->write([BII)V

    .line 167
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->mState:I

    goto :goto_3

    .line 123
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
