.class Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$3;
.super Ljava/lang/Object;
.source "EffectManagerActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->initActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    .line 446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v7, -0x41000000    # -0.5f

    const/4 v6, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 451
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 499
    :cond_0
    :goto_0
    :pswitch_0
    return v6

    .line 457
    :pswitch_1
    invoke-virtual {p1, v6}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 460
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpl-float v1, v1, v4

    if-ltz v1, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    cmpl-float v1, v1, v4

    if-ltz v1, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 461
    invoke-virtual {p1, v6}, Landroid/view/View;->setPressed(Z)V

    .line 462
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0900c0

    if-ne v1, v2, :cond_1

    .line 464
    const-string v1, "sec-roboto-light"

    invoke-static {v1, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 465
    .local v0, "font":Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mCancelText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$7(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 466
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mCancelText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$7(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v5, v4, v5, v7}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto :goto_0

    .line 468
    .end local v0    # "font":Landroid/graphics/Typeface;
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0900c3

    if-ne v1, v2, :cond_0

    .line 470
    const-string v1, "sec-roboto-light"

    invoke-static {v1, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 471
    .restart local v0    # "font":Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mDoneText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 472
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mDoneText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v5, v4, v5, v7}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto :goto_0

    .line 476
    .end local v0    # "font":Landroid/graphics/Typeface;
    :cond_2
    invoke-virtual {p1, v3}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 480
    :pswitch_3
    invoke-virtual {p1, v3}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 483
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$9(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->getNullBtn()Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v1

    if-nez v1, :cond_3

    .line 485
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0900c0

    if-ne v1, v2, :cond_4

    .line 487
    invoke-virtual {p1, v3}, Landroid/view/View;->playSoundEffect(I)V

    .line 488
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->cancel()V
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)V

    .line 496
    :cond_3
    :goto_1
    invoke-virtual {p1, v3}, Landroid/view/View;->setPressed(Z)V

    goto/16 :goto_0

    .line 490
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0900c3

    if-ne v1, v2, :cond_3

    .line 492
    invoke-virtual {p1, v3}, Landroid/view/View;->playSoundEffect(I)V

    .line 493
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$3;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->done()V
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$4(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)V

    goto :goto_1

    .line 451
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
