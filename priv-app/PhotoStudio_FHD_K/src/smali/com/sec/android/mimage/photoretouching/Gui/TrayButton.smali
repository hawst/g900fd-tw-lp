.class public Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
.super Landroid/widget/LinearLayout;
.source "TrayButton.java"


# static fields
.field public static final ADD_BUTTON:I = 0x0

.field public static final TRAY_BUTTON:I = 0x1


# instance fields
.field private mButton:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mDeleteButton:Landroid/widget/LinearLayout;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mIndex:I

.field private mIsDisable:Z

.field private mSaveNoti:Landroid/widget/LinearLayout;

.field private mShowDeletebutton:Z

.field private mShowSaveNoti:Z

.field private mThumbnailBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 412
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mIsDisable:Z

    .line 413
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mShowDeletebutton:Z

    .line 414
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mShowSaveNoti:Z

    .line 415
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mButton:Landroid/widget/ImageView;

    .line 416
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mSaveNoti:Landroid/widget/LinearLayout;

    .line 417
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mDeleteButton:Landroid/widget/LinearLayout;

    .line 418
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    .line 419
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 420
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 421
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mThumbnailBitmap:Landroid/graphics/Bitmap;

    .line 422
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mIndex:I

    .line 31
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 412
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mIsDisable:Z

    .line 413
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mShowDeletebutton:Z

    .line 414
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mShowSaveNoti:Z

    .line 415
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mButton:Landroid/widget/ImageView;

    .line 416
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mSaveNoti:Landroid/widget/LinearLayout;

    .line 417
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mDeleteButton:Landroid/widget/LinearLayout;

    .line 418
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    .line 419
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 420
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 421
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mThumbnailBitmap:Landroid/graphics/Bitmap;

    .line 422
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mIndex:I

    .line 37
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 412
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mIsDisable:Z

    .line 413
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mShowDeletebutton:Z

    .line 414
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mShowSaveNoti:Z

    .line 415
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mButton:Landroid/widget/ImageView;

    .line 416
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mSaveNoti:Landroid/widget/LinearLayout;

    .line 417
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mDeleteButton:Landroid/widget/LinearLayout;

    .line 418
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    .line 419
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 420
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 421
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mThumbnailBitmap:Landroid/graphics/Bitmap;

    .line 422
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mIndex:I

    .line 43
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    .line 44
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mThumbnailBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method private initDeleteButton()V
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mDeleteButton:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 406
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mShowDeletebutton:Z

    if-eqz v0, :cond_1

    .line 407
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mDeleteButton:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 411
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mDeleteButton:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private initHistoryManager()V
    .locals 3

    .prologue
    .line 313
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 314
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)V

    .line 335
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 354
    return-void
.end method

.method private initImageData(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V
    .locals 7
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .param p2, "decodeFrom"    # I

    .prologue
    .line 287
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 289
    iget-boolean v0, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 292
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    .line 293
    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    .line 294
    iget-object v3, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 291
    invoke-virtual {v0, v1, v2, v3, p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setDecodedImage(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/net/Uri;I)V

    .line 310
    :goto_0
    return-void

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    .line 300
    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    .line 301
    iget-object v3, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 302
    iget-boolean v4, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isCamera:Z

    .line 303
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->fileName:Ljava/lang/String;

    move v6, p2

    .line 299
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setDecodedImage(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/String;ZLjava/lang/String;I)V

    goto :goto_0
.end method

.method private initSaveNoti()V
    .locals 2

    .prologue
    .line 394
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mSaveNoti:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 396
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mShowSaveNoti:Z

    if-eqz v0, :cond_1

    .line 397
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mSaveNoti:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 399
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mSaveNoti:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private initThumbnail(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;)V
    .locals 3
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mThumbnailBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 234
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->thumbnail:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 235
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->thumbnail:Landroid/graphics/Bitmap;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mThumbnailBitmap:Landroid/graphics/Bitmap;

    .line 239
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$3;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$3;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 250
    :cond_0
    return-void

    .line 237
    :cond_1
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->makeThumbnail(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mThumbnailBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private makeThumbnail(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 18
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 356
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->measure(II)V

    .line 357
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getMeasuredWidth()I

    move-result v10

    .line 358
    .local v10, "buttonWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getMeasuredHeight()I

    move-result v9

    .line 359
    .local v9, "buttonHeight":I
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v10, v9, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 360
    .local v14, "ret":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v14}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 361
    .local v1, "c":Landroid/graphics/Canvas;
    new-instance v13, Landroid/graphics/Matrix;

    invoke-direct {v13}, Landroid/graphics/Matrix;-><init>()V

    .line 362
    .local v13, "m":Landroid/graphics/Matrix;
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 363
    .local v6, "p":Landroid/graphics/Paint;
    const/4 v2, 0x1

    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 365
    const/high16 v15, 0x3f800000    # 1.0f

    .line 366
    .local v15, "scale":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    .line 367
    .local v8, "bitmap_w":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 368
    .local v7, "bitmap_h":I
    int-to-float v2, v10

    int-to-float v3, v9

    div-float v16, v2, v3

    .line 369
    .local v16, "viewRatio":F
    int-to-float v2, v8

    int-to-float v3, v7

    div-float v12, v2, v3

    .line 371
    .local v12, "imgRatio":F
    cmpl-float v2, v16, v12

    if-lez v2, :cond_0

    .line 372
    int-to-float v2, v7

    mul-float/2addr v2, v15

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v2, v3

    float-to-int v11, v2

    .line 373
    .local v11, "h":I
    invoke-virtual {v13, v15, v15}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 374
    const/4 v2, 0x0

    sub-int v3, v9, v11

    int-to-float v3, v3

    invoke-virtual {v13, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 375
    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v13, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 383
    .end local v11    # "h":I
    :goto_0
    const/high16 v2, -0x1000000

    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 384
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 385
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 386
    const/4 v2, 0x0

    const/4 v3, 0x0

    add-int/lit8 v4, v10, -0x1

    int-to-float v4, v4

    add-int/lit8 v5, v9, -0x1

    int-to-float v5, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 388
    const/4 v1, 0x0

    .line 389
    const/4 v6, 0x0

    .line 390
    return-object v14

    .line 378
    :cond_0
    int-to-float v2, v8

    mul-float/2addr v2, v15

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v2, v3

    float-to-int v0, v2

    move/from16 v17, v0

    .line 379
    .local v17, "w":I
    invoke-virtual {v13, v15, v15}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 380
    sub-int v2, v10, v17

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {v13, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 381
    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v13, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private reloadImageData(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V
    .locals 7
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .param p2, "decodeFrom"    # I

    .prologue
    .line 253
    iget-boolean v0, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 256
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    .line 257
    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    .line 258
    iget-object v3, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 255
    invoke-virtual {v0, v1, v2, v3, p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setDecodedImage(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/net/Uri;I)V

    .line 270
    :goto_0
    return-void

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    .line 264
    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    .line 265
    iget-object v3, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 266
    iget-boolean v4, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isCamera:Z

    .line 267
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->fileName:Ljava/lang/String;

    move v6, p2

    .line 263
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setDecodedImage(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/String;ZLjava/lang/String;I)V

    goto :goto_0
.end method

.method private resetHistoryManager()V
    .locals 4

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->destroy()V

    .line 276
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 277
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v1

    .line 278
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v2

    .line 279
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    .line 277
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->initFirstOriginalHistory([III)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 281
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 282
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 280
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->initFirstPreviewHistory([III)V

    .line 284
    :cond_0
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->destroy()V

    .line 114
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 115
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->destroy()V

    .line 117
    :cond_1
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 118
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    .line 119
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 120
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 121
    :cond_2
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mButton:Landroid/widget/ImageView;

    .line 122
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mSaveNoti:Landroid/widget/LinearLayout;

    .line 123
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mDeleteButton:Landroid/widget/LinearLayout;

    .line 124
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mThumbnailBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mThumbnailBitmap:Landroid/graphics/Bitmap;

    .line 125
    return-void
.end method

.method public disable()V
    .locals 2

    .prologue
    .line 166
    const-string v0, "tray button disabled"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 167
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getId()I

    move-result v0

    if-nez v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mIsDisable:Z

    .line 180
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$2;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public enable()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 192
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mIsDisable:Z

    .line 194
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setEnabled(Z)V

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getId()I

    move-result v2

    if-nez v2, :cond_0

    .line 197
    const v2, 0x7f090143

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 198
    .local v1, "v":Landroid/widget/ImageView;
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 205
    .end local v1    # "v":Landroid/widget/ImageView;
    :goto_0
    return-void

    .line 202
    :cond_0
    const v2, 0x7f090144

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 203
    .local v0, "l":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setPressed(Z)V

    goto :goto_0
.end method

.method public getHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method public getIconBitmap()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mThumbnailBitmap:Landroid/graphics/Bitmap;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method public getIndexFromTrayButtonList()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mIndex:I

    return v0
.end method

.method public getSaveVisiblity()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mShowSaveNoti:Z

    return v0
.end method

.method public init(Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V
    .locals 3
    .param p2, "buttonListener"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;",
            ">;",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;>;"
    const/4 v2, 0x0

    .line 49
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    const v1, 0x7f030070

    invoke-static {v0, v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 50
    const v0, 0x7f090143

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mButton:Landroid/widget/ImageView;

    .line 52
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 53
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setFocusable(Z)V

    .line 54
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setId(I)V

    .line 55
    return-void
.end method

.method public init(Ljava/util/ArrayList;ZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V
    .locals 5
    .param p2, "longPress"    # Z
    .param p3, "buttonListener"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .param p4, "deleteListener"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;",
            ">;Z",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;>;"
    const/4 v4, 0x1

    .line 62
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mIndex:I

    .line 63
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    const v1, 0x7f030071

    invoke-static {v0, v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 64
    const v0, 0x7f090143

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mButton:Landroid/widget/ImageView;

    .line 65
    const v0, 0x7f090145

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mSaveNoti:Landroid/widget/LinearLayout;

    .line 66
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initSaveNoti()V

    .line 67
    const v0, 0x7f090146

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mDeleteButton:Landroid/widget/LinearLayout;

    .line 68
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mShowDeletebutton:Z

    .line 69
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initDeleteButton()V

    .line 70
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mDeleteButton:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v1, v2, p1, p4, v3}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 72
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p3, p2}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 74
    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setFocusable(Z)V

    .line 75
    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setId(I)V

    .line 77
    return-void
.end method

.method public invisibleDeleteButton()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initDeleteButton()V

    .line 144
    return-void
.end method

.method public invisibleSaveNoti()V
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mShowSaveNoti:Z

    .line 139
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initSaveNoti()V

    .line 140
    return-void
.end method

.method public isDisabled()Z
    .locals 1

    .prologue
    .line 221
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mIsDisable:Z

    return v0
.end method

.method public reloadDecodeInfo(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V
    .locals 0
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .param p2, "decodeFrom"    # I

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initSaveNoti()V

    .line 91
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initDeleteButton()V

    .line 92
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initThumbnail(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;)V

    .line 93
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->reloadImageData(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V

    .line 94
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->resetHistoryManager()V

    .line 95
    return-void
.end method

.method public setDecodeInfo(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V
    .locals 0
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .param p2, "decodeFrom"    # I

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initSaveNoti()V

    .line 82
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initDeleteButton()V

    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initThumbnail(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;)V

    .line 84
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initImageData(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V

    .line 85
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initHistoryManager()V

    .line 86
    return-void
.end method

.method public setDisableLongClick(Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V
    .locals 4
    .param p2, "buttonListener"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .param p3, "deleteListener"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;",
            ">;",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;>;"
    const/4 v3, 0x0

    .line 107
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mDeleteButton:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p1, p3, v3}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 109
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 209
    if-eqz p1, :cond_1

    .line 211
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mIsDisable:Z

    if-nez v0, :cond_0

    .line 212
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setLongClick(Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V
    .locals 4
    .param p2, "buttonListener"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .param p3, "deleteListener"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;",
            ">;",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            ")V"
        }
    .end annotation

    .prologue
    .line 100
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;>;"
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mDeleteButton:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v1, v2, p1, p3, v3}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 102
    return-void
.end method

.method public setVisibleDeleteButton(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mDeleteButton:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mDeleteButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 228
    const-string v0, "setVisibleDeleteButton true"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 230
    :cond_0
    return-void
.end method

.method public visibleDeleteButton()V
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mShowDeletebutton:Z

    .line 134
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initDeleteButton()V

    .line 135
    return-void
.end method

.method public visibleSaveNoti()V
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mShowSaveNoti:Z

    .line 129
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initSaveNoti()V

    .line 130
    return-void
.end method
