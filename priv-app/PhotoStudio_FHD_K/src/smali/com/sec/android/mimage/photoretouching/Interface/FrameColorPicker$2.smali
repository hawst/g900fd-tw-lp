.class Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$2;
.super Ljava/lang/Object;
.source "FrameColorPicker.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->initListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : mOnTouchColorPalette : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 178
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 196
    :goto_0
    return v2

    .line 181
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->setSelected(I)V
    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    goto :goto_0

    .line 185
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->getColor(Landroid/view/MotionEvent;)I
    invoke-static {v1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;Landroid/view/MotionEvent;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;)Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mCurrentColor:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;->setBackgroundColor(I)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->setPickerPos(Landroid/view/MotionEvent;Z)V
    invoke-static {v0, p2, v2}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;Landroid/view/MotionEvent;Z)V

    goto :goto_0

    .line 192
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;)Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mCurrentColor:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;->setBackgroundColor(I)V

    goto :goto_0

    .line 178
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
