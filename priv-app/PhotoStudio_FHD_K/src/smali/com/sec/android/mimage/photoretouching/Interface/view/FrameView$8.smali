.class Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;
.super Ljava/lang/Object;
.source "FrameView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->init3DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    .line 749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 0
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 797
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 753
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    .line 754
    .local v2, "position":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 755
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 791
    :goto_0
    return-void

    .line 759
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 760
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableCancel()V

    .line 761
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 763
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isRecentClicked()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 765
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->MAX_RECENTLY:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 766
    .local v3, "recentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;>;"
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v3

    .line 767
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_3

    .line 776
    :goto_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isRecentClicked()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 778
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v6}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v6

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getRscId(I)I
    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setRecentButton(I)V

    .line 779
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v4

    invoke-virtual {v4, v8, v7, v7}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setRecentClicked(ZII)V

    .line 786
    .end local v1    # "i":I
    .end local v3    # "recentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;>;"
    :cond_1
    :goto_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->saveToDB()V

    .line 789
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->doDone()V
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    .line 790
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v5, 0x31000000

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0

    .line 769
    .restart local v1    # "i":I
    .restart local v3    # "recentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;>;"
    :cond_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v5}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v5

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getRscId(I)I
    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;I)I

    move-result v5

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v4

    if-ne v5, v4, :cond_4

    .line 771
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v6}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v6

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getRscId(I)I
    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;I)I

    move-result v5

    invoke-virtual {v4, v5, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->resetRecentButton(II)V

    .line 772
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v4

    invoke-virtual {v4, v8, v7, v7}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setRecentClicked(ZII)V

    goto/16 :goto_2

    .line 767
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 784
    .end local v1    # "i":I
    .end local v3    # "recentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;>;"
    :cond_5
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v6}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v6

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getRscId(I)I
    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setRecentButton(I)V

    goto :goto_3
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 803
    return-void
.end method
