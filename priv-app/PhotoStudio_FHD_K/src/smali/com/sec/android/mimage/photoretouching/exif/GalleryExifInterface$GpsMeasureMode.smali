.class public interface abstract Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$GpsMeasureMode;
.super Ljava/lang/Object;
.source "GalleryExifInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GpsMeasureMode"
.end annotation


# static fields
.field public static final MODE_2_DIMENSIONAL:Ljava/lang/String; = "2"

.field public static final MODE_3_DIMENSIONAL:Ljava/lang/String; = "3"
