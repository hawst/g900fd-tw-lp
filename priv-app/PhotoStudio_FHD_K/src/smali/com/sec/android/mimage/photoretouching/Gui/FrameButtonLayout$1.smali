.class Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;
.super Ljava/lang/Object;
.source "FrameButtonLayout.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->initFrame(I)Ljava/util/ArrayList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 12
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    const-wide/16 v10, 0x0

    .line 132
    const-string v8, "JW FrameButtonLayout onFocusChange"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 134
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mDecoManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v8

    if-nez v8, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mDecoManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getGridView()Landroid/widget/GridView;

    move-result-object v1

    .line 139
    .local v1, "gridView":Landroid/widget/GridView;
    if-eqz p2, :cond_0

    .line 141
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 142
    .local v6, "rect":Landroid/graphics/Rect;
    invoke-virtual {p1, v6}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 143
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 144
    .local v0, "contentViewRect":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mDecoManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v8

    invoke-virtual {v8, v0}, Landroid/widget/FrameLayout;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 145
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f05021f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 146
    .local v5, "offset":I
    const/4 v4, 0x0

    .line 147
    .local v4, "itemOffsetForScrollUp":I
    const/4 v3, 0x0

    .line 149
    .local v3, "itemOffsetForScrollDown":I
    const/16 v7, 0x32

    .line 150
    .local v7, "scrollDuration":I
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "JW rect="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 151
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "JW contentViewRect="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 152
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "JW gridView.getFirstVisiblePosition()="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 153
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    goto :goto_0

    .line 156
    :sswitch_0
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050226

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 157
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050227

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 159
    iget v8, v6, Landroid/graphics/Rect;->bottom:I

    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    if-gt v8, v9, :cond_2

    iget v8, v6, Landroid/graphics/Rect;->bottom:I

    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v5

    if-lt v8, v9, :cond_2

    .line 161
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v8

    add-int/2addr v8, v4

    invoke-virtual {v1, v8, v7}, Landroid/widget/GridView;->smoothScrollBy(II)V

    .line 162
    const-string v8, "JW scrollUp"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 165
    :cond_2
    iget v8, v6, Landroid/graphics/Rect;->top:I

    iget v9, v0, Landroid/graphics/Rect;->top:I

    if-lt v8, v9, :cond_0

    iget v8, v6, Landroid/graphics/Rect;->top:I

    iget v9, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v5

    if-gt v8, v9, :cond_0

    .line 167
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v1}, Landroid/widget/GridView;->getCount()I

    move-result v8

    if-ge v2, v8, :cond_0

    .line 169
    invoke-virtual {v1, v2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->isFocused()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 171
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v8

    add-int/2addr v8, v3

    neg-int v8, v8

    invoke-virtual {v1, v8, v7}, Landroid/widget/GridView;->smoothScrollBy(II)V

    .line 172
    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setSelection(I)V

    .line 173
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1$1;

    invoke-direct {v8, p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;Landroid/view/View;)V

    invoke-virtual {p1, v8, v10, v11}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 182
    const-string v8, "JW scrolldown"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 167
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 190
    .end local v2    # "i":I
    :sswitch_1
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050229

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 191
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f05022a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 193
    iget v8, v6, Landroid/graphics/Rect;->bottom:I

    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    if-gt v8, v9, :cond_4

    iget v8, v6, Landroid/graphics/Rect;->bottom:I

    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v5

    if-lt v8, v9, :cond_4

    .line 195
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v8

    add-int/2addr v8, v4

    invoke-virtual {v1, v8, v7}, Landroid/widget/GridView;->smoothScrollBy(II)V

    .line 196
    const-string v8, "JW scrollUp"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 199
    :cond_4
    iget v8, v6, Landroid/graphics/Rect;->top:I

    iget v9, v0, Landroid/graphics/Rect;->top:I

    if-lt v8, v9, :cond_0

    iget v8, v6, Landroid/graphics/Rect;->top:I

    iget v9, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v5

    if-gt v8, v9, :cond_0

    .line 202
    invoke-virtual {v1}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mDecoManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getNumColumn()I

    move-result v9

    if-le v8, v9, :cond_5

    .line 204
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v8

    add-int/2addr v8, v3

    neg-int v8, v8

    invoke-virtual {v1, v8, v7}, Landroid/widget/GridView;->smoothScrollBy(II)V

    .line 205
    const-string v8, "JW scrolldown"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 209
    :cond_5
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    invoke-virtual {v1}, Landroid/widget/GridView;->getCount()I

    move-result v8

    if-ge v2, v8, :cond_0

    .line 211
    invoke-virtual {v1, v2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->isFocused()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 214
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->mDecoManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getNumColumn()I

    move-result v8

    sub-int v8, v2, v8

    invoke-virtual {v1, v8}, Landroid/widget/GridView;->setSelection(I)V

    .line 215
    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setSelection(I)V

    .line 216
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1$2;

    invoke-direct {v8, p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1$2;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout$1;Landroid/view/View;)V

    invoke-virtual {p1, v8, v10, v11}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 225
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "JW scrolldown to my position="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 209
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 153
    :sswitch_data_0
    .sparse-switch
        0x31200000 -> :sswitch_0
        0x31500000 -> :sswitch_1
    .end sparse-switch
.end method
