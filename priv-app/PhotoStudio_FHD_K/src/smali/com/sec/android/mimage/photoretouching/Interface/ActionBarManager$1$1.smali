.class Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1$1;
.super Ljava/lang/Object;
.source "ActionBarManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->onLayoutCallback()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;

.field private final synthetic val$params:Landroid/widget/LinearLayout$LayoutParams;

.field private final synthetic val$scrollParams:Landroid/widget/LinearLayout$LayoutParams;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;Landroid/widget/LinearLayout$LayoutParams;Landroid/widget/LinearLayout$LayoutParams;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1$1;->val$scrollParams:Landroid/widget/LinearLayout$LayoutParams;

    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1$1;->val$params:Landroid/widget/LinearLayout$LayoutParams;

    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1$1;->val$scrollParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1$1;->val$params:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 262
    :cond_1
    return-void
.end method
