.class Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;
.super Ljava/lang/Object;
.source "MyButton.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->startAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCurrentPos:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$7(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setLeft(I)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setTop(I)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setRight(I)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setBottom(I)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCurrentPos:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$7(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;Z)V

    .line 227
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 214
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;Z)V

    .line 208
    return-void
.end method
