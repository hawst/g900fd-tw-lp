.class Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;
.super Ljava/lang/Object;
.source "WatermarkView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->init2DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 218
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 177
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->getImageEditViewWidth()I
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->getImageEditViewHeight()I
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)I

    move-result v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->undo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z

    .line 178
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;Landroid/graphics/Bitmap;)V

    .line 179
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 180
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 181
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 182
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 183
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3, v8, v8}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 193
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 194
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 195
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 200
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 201
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 204
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 207
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 208
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 210
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mPaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/graphics/Paint;

    move-result-object v9

    move v4, v2

    move v5, v2

    .line 202
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 211
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->invalidateViews()V

    .line 212
    return-void

    .line 185
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "output":[I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3, v8, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_0

    .line 187
    :cond_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v4

    if-ne v3, v4, :cond_4

    .line 188
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3, v8, v8}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 191
    :goto_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    goto/16 :goto_0

    .line 190
    :cond_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3, v2, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_2

    .line 197
    :cond_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 198
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto/16 :goto_1
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 224
    return-void
.end method
