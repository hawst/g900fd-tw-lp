.class public Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;
.super Ljava/lang/Object;
.source "ResizeEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;,
        Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnActionbarCallback;,
        Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnResizeCallback;
    }
.end annotation


# instance fields
.field private currentRect:Landroid/graphics/RectF;

.field private isAnimating:Z

.field mAnimationIntervalright:F

.field mAnimationIntervaltop:F

.field private mBitmapResizeIcon:Landroid/graphics/Bitmap;

.field private mBitmapResizeIconPress:Landroid/graphics/Bitmap;

.field private mBitmap_bg:Landroid/graphics/Bitmap;

.field private mByTextBitmap:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mHandler_LB:Landroid/graphics/Bitmap;

.field private mHandler_LT:Landroid/graphics/Bitmap;

.field private mHandler_RB:Landroid/graphics/Bitmap;

.field private mHandler_RT:Landroid/graphics/Bitmap;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mIsGesture:Z

.field private mOnActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnActionbarCallback;

.field private mProgressLeftMagine:F

.field private mRectPaint:Landroid/graphics/Paint;

.field private mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

.field private mRezingTextBitmap:Landroid/graphics/Bitmap;

.field private mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field mScaleH:F

.field mScaleW:F

.field mTouchPressed:Z

.field private mTouchType:I

.field private myCallback:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnResizeCallback;

.field private rectToDraw:Landroid/graphics/RectF;

.field private scale:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mAnimationIntervaltop:F

    .line 208
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mAnimationIntervalright:F

    .line 603
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mProgressLeftMagine:F

    .line 604
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchType:I

    .line 605
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    .line 606
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnResizeCallback;

    .line 607
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mOnActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnActionbarCallback;

    .line 608
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 609
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 615
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 616
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 617
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 618
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 620
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchPressed:Z

    .line 623
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 624
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mByTextBitmap:Landroid/graphics/Bitmap;

    .line 625
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRezingTextBitmap:Landroid/graphics/Bitmap;

    .line 626
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    .line 627
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 628
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mIsGesture:Z

    .line 629
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleW:F

    .line 630
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleH:F

    .line 633
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->isAnimating:Z

    .line 634
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->scale:F

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/content/Context;)V
    .locals 4
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mAnimationIntervaltop:F

    .line 208
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mAnimationIntervalright:F

    .line 603
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mProgressLeftMagine:F

    .line 604
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchType:I

    .line 605
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    .line 606
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnResizeCallback;

    .line 607
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mOnActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnActionbarCallback;

    .line 608
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 609
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 615
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 616
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 617
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 618
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 620
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchPressed:Z

    .line 623
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 624
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mByTextBitmap:Landroid/graphics/Bitmap;

    .line 625
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRezingTextBitmap:Landroid/graphics/Bitmap;

    .line 626
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    .line 627
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 628
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mIsGesture:Z

    .line 629
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleW:F

    .line 630
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleH:F

    .line 633
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->isAnimating:Z

    .line 634
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->scale:F

    .line 51
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    .line 52
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 53
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    const/4 v1, 0x1

    invoke-direct {v0, p2, p1, v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;IZ)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    .line 54
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020374

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    .line 55
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020377

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIconPress:Landroid/graphics/Bitmap;

    .line 57
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 58
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 59
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 60
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 62
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 63
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRectPaint:Landroid/graphics/Paint;

    const v1, 0xffffff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRectPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRectPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41000000    # 8.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRectPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 67
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->setBitmapBG()V

    .line 68
    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;)V

    invoke-direct {v0, p2, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 69
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->rectToDraw:Landroid/graphics/RectF;

    .line 70
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    .line 71
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;)Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;Z)V
    .locals 0

    .prologue
    .line 628
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mIsGesture:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method private getProgressText(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 422
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f05025a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 423
    .local v5, "textTopMagine":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050259

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 424
    .local v2, "progressbarLeftMagine":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f05025b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 425
    .local v3, "progressbarTextSize":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f05025c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 426
    .local v4, "progressbarTextwidth":I
    const/4 v6, 0x1

    .line 428
    .local v6, "textViewMaxLine":I
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 429
    .local v1, "paint":Landroid/graphics/Paint;
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 431
    new-instance v8, Landroid/graphics/BlurMaskFilter;

    const/high16 v9, 0x40000000    # 2.0f

    sget-object v10, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v8, v9, v10}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 432
    new-instance v7, Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 434
    .local v7, "tv":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    const v9, 0x7f060032

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 435
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setWidth(I)V

    .line 436
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 438
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 439
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x14

    const/4 v11, 0x0

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 442
    const/4 v8, -0x1

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 443
    const/4 v8, 0x0

    int-to-float v9, v3

    invoke-virtual {v7, v8, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 444
    const-string v8, "sec-roboto-light"

    const/4 v9, 0x0

    invoke-static {v8, v9}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 445
    .local v0, "font":Landroid/graphics/Typeface;
    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 446
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v11, -0x41000000    # -0.5f

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 448
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    .line 451
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v8

    const/high16 v9, -0x80000000

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 452
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v9

    const/high16 v10, -0x80000000

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 451
    invoke-virtual {v7, v8, v9}, Landroid/widget/TextView;->measure(II)V

    .line 454
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v10

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v11

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->layout(IIII)V

    .line 455
    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 457
    invoke-virtual {v7}, Landroid/widget/TextView;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRezingTextBitmap:Landroid/graphics/Bitmap;

    .line 459
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRezingTextBitmap:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_0

    .line 460
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRezingTextBitmap:Landroid/graphics/Bitmap;

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mProgressLeftMagine:F

    int-to-float v10, v2

    add-float/2addr v9, v10

    .line 461
    int-to-float v10, v5

    .line 460
    invoke-virtual {p1, v8, v9, v10, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 463
    :cond_0
    return-void
.end method

.method private getRect(Landroid/graphics/RectF;Landroid/graphics/RectF;F)Landroid/graphics/RectF;
    .locals 2
    .param p1, "current"    # Landroid/graphics/RectF;
    .param p2, "rectToDraw"    # Landroid/graphics/RectF;
    .param p3, "scale"    # F

    .prologue
    .line 286
    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    if-eq v0, v1, :cond_0

    .line 287
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 289
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mAnimationIntervalright:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 294
    :cond_0
    :goto_0
    iget v0, p2, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    if-eq v0, v1, :cond_1

    .line 295
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p2, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    .line 296
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mAnimationIntervaltop:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 301
    :cond_1
    :goto_1
    iget v0, p2, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    if-eq v0, v1, :cond_2

    .line 302
    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget v1, p2, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_7

    .line 303
    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mAnimationIntervalright:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 308
    :cond_2
    :goto_2
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    float-to-int v1, v1

    if-eq v0, v1, :cond_3

    .line 309
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_8

    .line 310
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mAnimationIntervaltop:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 315
    :cond_3
    :goto_3
    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    if-ne v0, v1, :cond_4

    .line 316
    iget v0, p2, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    if-ne v0, v1, :cond_4

    .line 317
    iget v0, p2, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    if-ne v0, v1, :cond_4

    .line 318
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    float-to-int v1, v1

    if-ne v0, v1, :cond_4

    .line 319
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->isAnimating:Z

    .line 321
    :cond_4
    return-object p1

    .line 291
    :cond_5
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mAnimationIntervalright:F

    sub-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    goto :goto_0

    .line 298
    :cond_6
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mAnimationIntervaltop:F

    sub-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    goto :goto_1

    .line 305
    :cond_7
    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mAnimationIntervalright:F

    sub-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    goto :goto_2

    .line 312
    :cond_8
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mAnimationIntervaltop:F

    sub-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_3
.end method

.method private getRezieGauge(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 410
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050255

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 412
    .local v1, "topMargine":I
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 413
    .local v0, "paint":Landroid/graphics/Paint;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 416
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 417
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mProgressLeftMagine:F

    int-to-float v4, v1

    const/4 v5, 0x0

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 418
    :cond_0
    return-void
.end method

.method private setBitmapBG()V
    .locals 4

    .prologue
    .line 469
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 470
    .local v1, "res":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0203c8

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 471
    .local v0, "gauge_bg":Landroid/graphics/Bitmap;
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 472
    return-void
.end method

.method private setHandlerIconPressed(I)V
    .locals 1
    .param p1, "touchType"    # I

    .prologue
    .line 107
    packed-switch p1, :pswitch_data_0

    .line 159
    :goto_0
    return-void

    .line 109
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchPressed:Z

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIconPress:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 113
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 114
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 115
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 121
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchPressed:Z

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIconPress:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 125
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 126
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 127
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 133
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchPressed:Z

    if-eqz v0, :cond_2

    .line 135
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIconPress:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 137
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 138
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 139
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 142
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 145
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchPressed:Z

    if-eqz v0, :cond_3

    .line 147
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIconPress:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 149
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 150
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 151
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 154
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public AnimationDone()V
    .locals 1

    .prologue
    .line 639
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->isAnimating:Z

    .line 640
    return-void
.end method

.method public applyOriginal()[I
    .locals 17

    .prologue
    .line 509
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleW:F

    .line 510
    .local v14, "scaleW":F
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleH:F

    .line 512
    .local v13, "scaleH":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v14

    float-to-int v0, v2

    move/from16 v16, v0

    .line 513
    .local v16, "tempWidth":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v13

    float-to-int v15, v2

    .line 515
    .local v15, "tempHeight":I
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v16

    invoke-static {v0, v15, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 517
    .local v12, "result":Landroid/graphics/Bitmap;
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    .line 518
    .local v10, "paint":Landroid/graphics/Paint;
    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 519
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 520
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Canvas;->save()I

    .line 521
    new-instance v11, Landroid/graphics/Matrix;

    invoke-direct {v11}, Landroid/graphics/Matrix;-><init>()V

    .line 522
    .local v11, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v11, v14, v13}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 523
    invoke-virtual {v1, v11}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 524
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v2

    .line 525
    const/4 v3, 0x0

    .line 526
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v4

    .line 527
    const/4 v5, 0x0

    .line 528
    const/4 v6, 0x0

    .line 529
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v7

    .line 530
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v8

    .line 531
    const/4 v9, 0x1

    .line 524
    invoke-virtual/range {v1 .. v10}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 533
    invoke-virtual {v1}, Landroid/graphics/Canvas;->restore()V

    .line 536
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2, v12}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer(Landroid/graphics/Bitmap;)V

    .line 537
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v2

    return-object v2
.end method

.method public applyPreview()V
    .locals 17

    .prologue
    .line 476
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v14, v2, v3

    .line 477
    .local v14, "scaleW":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v13, v2, v3

    .line 478
    .local v13, "scaleH":F
    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleW:F

    .line 479
    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleH:F

    .line 481
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v14

    float-to-int v0, v2

    move/from16 v16, v0

    .line 482
    .local v16, "tempWidth":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v13

    float-to-int v15, v2

    .line 484
    .local v15, "tempHeight":I
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v16

    invoke-static {v0, v15, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 485
    .local v12, "result":Landroid/graphics/Bitmap;
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    .line 486
    .local v10, "paint":Landroid/graphics/Paint;
    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 487
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 488
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Canvas;->save()I

    .line 489
    new-instance v11, Landroid/graphics/Matrix;

    invoke-direct {v11}, Landroid/graphics/Matrix;-><init>()V

    .line 490
    .local v11, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v11, v14, v13}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 491
    invoke-virtual {v1, v11}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 492
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    .line 493
    const/4 v3, 0x0

    .line 494
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 495
    const/4 v5, 0x0

    .line 496
    const/4 v6, 0x0

    .line 497
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    .line 498
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v8

    .line 499
    const/4 v9, 0x1

    .line 492
    invoke-virtual/range {v1 .. v10}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 501
    invoke-virtual {v1}, Landroid/graphics/Canvas;->restore()V

    .line 503
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2, v12}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewBuffer(Landroid/graphics/Bitmap;)V

    .line 504
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 505
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->loadTextures(Landroid/content/Context;)V

    .line 506
    return-void
.end method

.method public calculateInitialValues(Z)V
    .locals 4
    .param p1, "mFirst"    # Z

    .prologue
    .line 198
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v0

    .line 199
    .local v0, "drawRectRoi":Landroid/graphics/RectF;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    .line 200
    .local v2, "viewTransform":Landroid/graphics/Matrix;
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 201
    .local v1, "drawRoi":Landroid/graphics/RectF;
    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 202
    if-eqz p1, :cond_0

    .line 203
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    .line 205
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->rectToDraw:Landroid/graphics/RectF;

    .line 206
    return-void
.end method

.method public calculateanimationintertval()V
    .locals 3

    .prologue
    const/high16 v2, 0x41100000    # 9.0f

    .line 211
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->rectToDraw:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mAnimationIntervaltop:F

    .line 212
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->rectToDraw:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mAnimationIntervalright:F

    .line 213
    return-void
.end method

.method public configurationChange()V
    .locals 0

    .prologue
    .line 325
    return-void
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;)V
    .locals 1
    .param p1, "resizeEffect"    # Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    .prologue
    .line 73
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 75
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleW:F

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleW:F

    .line 76
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleH:F

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleH:F

    .line 77
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 85
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIcon:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 86
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmapResizeIconPress:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 87
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 88
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 89
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 90
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 92
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mByTextBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mByTextBitmap:Landroid/graphics/Bitmap;

    .line 94
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRezingTextBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRezingTextBitmap:Landroid/graphics/Bitmap;

    .line 95
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mOnActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnActionbarCallback;

    .line 97
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    .line 98
    return-void
.end method

.method public getCalculatedScale()F
    .locals 2

    .prologue
    .line 583
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 585
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 578
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 580
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    if-eqz v0, :cond_0

    .line 599
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getScale()F

    move-result v0

    .line 600
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getScaleH()F
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    if-eqz v0, :cond_0

    .line 594
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleH:F

    .line 595
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getScaleW()F
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    if-eqz v0, :cond_0

    .line 589
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleW:F

    .line 590
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 573
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 574
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 575
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnActionbarCallback;)V
    .locals 0
    .param p1, "actionBarCallback"    # Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnActionbarCallback;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mOnActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnActionbarCallback;

    .line 81
    return-void
.end method

.method public isAnimationStarted()Z
    .locals 1

    .prologue
    .line 636
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->isAnimating:Z

    return v0
.end method

.method public resizeButtonTouch(I)V
    .locals 2
    .param p1, "statusId"    # I

    .prologue
    const/high16 v1, 0x3f000000    # 0.5f

    .line 328
    const v0, 0x11301201

    if-ne p1, v0, :cond_1

    .line 330
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    const v1, 0x3dcccccd    # 0.1f

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->setResizeScale(F)V

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 332
    :cond_1
    const v0, 0x11301202

    if-ne p1, v0, :cond_2

    .line 334
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    const/high16 v1, 0x3e800000    # 0.25f

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->setResizeScale(F)V

    goto :goto_0

    .line 336
    :cond_2
    const v0, 0x11301203

    if-ne p1, v0, :cond_3

    .line 338
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->setResizeScale(F)V

    goto :goto_0

    .line 340
    :cond_3
    const v0, 0x11301204

    if-ne p1, v0, :cond_4

    .line 342
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->setResizeScale(F)V

    goto :goto_0

    .line 344
    :cond_4
    const v0, 0x11301206

    if-ne p1, v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->setResizeScale(F)V

    goto :goto_0
.end method

.method public resizeDraw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "imagePaint"    # Landroid/graphics/Paint;

    .prologue
    .line 220
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->rectToDraw:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->rectToDraw:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v9

    div-float v4, v8, v9

    .line 222
    .local v4, "scale":F
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->calculateInitialValues(Z)V

    .line 223
    iget-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->isAnimating:Z

    if-eqz v8, :cond_4

    .line 224
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->rectToDraw:Landroid/graphics/RectF;

    invoke-direct {p0, v8, v9, v4}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->getRect(Landroid/graphics/RectF;Landroid/graphics/RectF;F)Landroid/graphics/RectF;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    .line 228
    :goto_0
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 229
    .local v7, "width":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 230
    .local v0, "height":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    float-to-int v3, v8

    .line 231
    .local v3, "resizeWidth":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v8

    float-to-int v2, v8

    .line 232
    .local v2, "resizeHeight":I
    int-to-float v8, v3

    int-to-float v9, v7

    div-float v6, v8, v9

    .line 233
    .local v6, "scaleWidth":F
    int-to-float v8, v2

    int-to-float v9, v0

    div-float v5, v8, v9

    .line 235
    .local v5, "scaleHeight":F
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 236
    .local v1, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v1, v6, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 237
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v8, v9}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 239
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {p1, v8, v1, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 240
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mRectPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 243
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_0

    .line 244
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 245
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->left:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    sub-float/2addr v9, v10

    .line 246
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->top:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    sub-float/2addr v10, v11

    .line 244
    invoke-virtual {p1, v8, v9, v10, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 248
    :cond_0
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_1

    .line 249
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 250
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->right:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    sub-float/2addr v9, v10

    .line 251
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->top:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    sub-float/2addr v10, v11

    .line 249
    invoke-virtual {p1, v8, v9, v10, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 253
    :cond_1
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_2

    .line 254
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 255
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->left:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    sub-float/2addr v9, v10

    .line 256
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    sub-float/2addr v10, v11

    .line 254
    invoke-virtual {p1, v8, v9, v10, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 259
    :cond_2
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_3

    .line 260
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 261
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->right:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    sub-float/2addr v9, v10

    .line 262
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    sub-float/2addr v10, v11

    .line 260
    invoke-virtual {p1, v8, v9, v10, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 284
    :cond_3
    return-void

    .line 226
    .end local v0    # "height":I
    .end local v1    # "matrix":Landroid/graphics/Matrix;
    .end local v2    # "resizeHeight":I
    .end local v3    # "resizeWidth":I
    .end local v5    # "scaleHeight":F
    .end local v6    # "scaleWidth":F
    .end local v7    # "width":I
    :cond_4
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->rectToDraw:Landroid/graphics/RectF;

    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->currentRect:Landroid/graphics/RectF;

    goto/16 :goto_0
.end method

.method public resizeTouch(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 164
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 165
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mIsGesture:Z

    if-nez v2, :cond_0

    .line 167
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 168
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 169
    .local v1, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 194
    .end local v0    # "x":F
    .end local v1    # "y":F
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnResizeCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnResizeCallback;->invalidate()V

    .line 195
    return v3

    .line 172
    .restart local v0    # "x":F
    .restart local v1    # "y":F
    :pswitch_0
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchPressed:Z

    .line 173
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->InitMoveObject(FF)I

    move-result v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchType:I

    .line 174
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchType:I

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->setHandlerIconPressed(I)V

    goto :goto_0

    .line 177
    :pswitch_1
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchPressed:Z

    .line 178
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchType:I

    if-eq v2, v3, :cond_1

    .line 180
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->StartMoveObject(FF)V

    .line 182
    :cond_1
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchType:I

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->setHandlerIconPressed(I)V

    goto :goto_0

    .line 186
    :pswitch_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchPressed:Z

    .line 187
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchType:I

    if-eq v2, v3, :cond_2

    .line 188
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->EndMoveObject()V

    .line 189
    :cond_2
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mTouchType:I

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->setHandlerIconPressed(I)V

    goto :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setOnCallback(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnResizeCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnResizeCallback;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnResizeCallback;

    .line 103
    return-void
.end method

.method public showResizeText(Landroid/graphics/Canvas;)V
    .locals 17
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 366
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v14}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v14

    int-to-float v14, v14

    div-float v12, v13, v14

    .line 367
    .local v12, "wScale":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v14}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v14

    int-to-float v14, v14

    div-float v2, v13, v14

    .line 368
    .local v2, "hScale":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v13

    int-to-float v13, v13

    mul-float/2addr v13, v12

    float-to-int v4, v13

    .line 369
    .local v4, "nWidth":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v13

    int-to-float v13, v13

    mul-float/2addr v13, v2

    float-to-int v3, v13

    .line 370
    .local v3, "nHeight":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f05025a

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 371
    .local v9, "textTopMagine":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f050259

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 372
    .local v6, "progressbarRightMagine":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f050256

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 373
    .local v8, "progressbarWidth":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f05025b

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 375
    .local v7, "progressbarTextSize":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v13

    sub-int/2addr v13, v8

    div-int/lit8 v13, v13, 0x2

    int-to-float v13, v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mProgressLeftMagine:F

    .line 376
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, " X "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 378
    .local v11, "value":Ljava/lang/String;
    new-instance v10, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-direct {v10, v13}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 379
    .local v10, "tv":Landroid/widget/TextView;
    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 382
    .local v5, "paint":Landroid/graphics/Paint;
    const/4 v13, 0x1

    invoke-virtual {v5, v13}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 384
    const/4 v13, -0x1

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 385
    const/4 v13, 0x0

    int-to-float v14, v7

    invoke-virtual {v10, v13, v14}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 386
    const-string v13, "sec-roboto-light"

    const/4 v14, 0x0

    invoke-static {v13, v14}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    .line 387
    .local v1, "font":Landroid/graphics/Typeface;
    invoke-virtual {v10, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 388
    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x0

    const/high16 v15, 0x3f800000    # 1.0f

    const/high16 v16, -0x41000000    # -0.5f

    move/from16 v0, v16

    invoke-virtual {v10, v13, v14, v15, v0}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 390
    const/4 v13, 0x1

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    .line 392
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v13

    const/high16 v14, -0x80000000

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    .line 393
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v14

    const/high16 v15, -0x80000000

    invoke-static {v14, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    .line 392
    invoke-virtual {v10, v13, v14}, Landroid/widget/TextView;->measure(II)V

    .line 395
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v10}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v15

    invoke-virtual {v10}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v10, v13, v14, v15, v0}, Landroid/widget/TextView;->layout(IIII)V

    .line 396
    const/16 v13, 0x11

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setGravity(I)V

    .line 398
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->getRezieGauge(Landroid/graphics/Canvas;)V

    .line 400
    invoke-virtual {v10}, Landroid/widget/TextView;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mByTextBitmap:Landroid/graphics/Bitmap;

    .line 402
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mByTextBitmap:Landroid/graphics/Bitmap;

    if-eqz v13, :cond_0

    .line 403
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mByTextBitmap:Landroid/graphics/Bitmap;

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v14

    int-to-float v14, v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mProgressLeftMagine:F

    sub-float/2addr v14, v15

    int-to-float v15, v6

    sub-float/2addr v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mByTextBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    int-to-float v15, v15

    sub-float/2addr v14, v15

    int-to-float v15, v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14, v15, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 405
    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->getProgressText(Landroid/graphics/Canvas;)V

    .line 406
    return-void
.end method

.method public showResizeToast()V
    .locals 7

    .prologue
    .line 354
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v4, v5, v6

    .line 355
    .local v4, "wScale":F
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    int-to-float v6, v6

    div-float v0, v5, v6

    .line 356
    .local v0, "hScale":F
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v4

    float-to-int v2, v5

    .line 357
    .local v2, "nWidth":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v0

    float-to-int v1, v5

    .line 358
    .local v1, "nHeight":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " X "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 359
    .local v3, "value":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mContext:Landroid/content/Context;

    invoke-static {v5, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 361
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnResizeCallback;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$OnResizeCallback;->invalidate()V

    .line 362
    return-void
.end method

.method public startAnimationDone()V
    .locals 1

    .prologue
    .line 642
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->isAnimating:Z

    .line 643
    return-void
.end method
