.class Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2$1;
.super Ljava/lang/Object;
.source "LauncherActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;

.field private final synthetic val$popup:Landroid/widget/ListPopupWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;Landroid/widget/ListPopupWindow;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2$1;->this$1:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2$1;->val$popup:Landroid/widget/ListPopupWindow;

    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 224
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2$1;->val$popup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 225
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2$1;->this$1:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;)Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->access$2(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2$1;->this$1:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;)Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->access$2(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->resetMultiSelection()V

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2$1;->this$1:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;)Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setMultiSelectDropText()V

    .line 228
    return-void
.end method
