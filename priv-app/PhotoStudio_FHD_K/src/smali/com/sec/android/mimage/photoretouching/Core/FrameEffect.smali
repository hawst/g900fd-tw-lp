.class public Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;
.super Ljava/lang/Object;
.source "FrameEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/FrameEffect$OnCallback;
    }
.end annotation


# instance fields
.field private final BOTTOM:I

.field public final BOTTOM_LEFT:I

.field public final BOTTOM_MIDDLE:I

.field public final BOTTOM_RIGHT:I

.field private final EFFECT:I

.field public final FRAME_01:I

.field public final FRAME_02:I

.field public final FRAME_03:I

.field public final FRAME_04:I

.field public final FRAME_05:I

.field public final FRAME_06:I

.field public final FRAME_07:I

.field public final FRAME_08:I

.field public final FRAME_09:I

.field public final FRAME_10:I

.field public final FRAME_11:I

.field public final FRAME_12:I

.field public final FRAME_13:I

.field public final FRAME_14:I

.field public final FRAME_15:I

.field public final FRAME_16:I

.field public final FRAME_17:I

.field public final FRAME_18:I

.field public final FRAME_19:I

.field public final FRAME_20:I

.field public final FRAME_21:I

.field public final FRAME_22:I

.field public final FRAME_23:I

.field public final FRAME_24:I

.field public final FRAME_25:I

.field public final FRAME_26:I

.field public final FRAME_27:I

.field public final FRAME_28:I

.field public final FRAME_29:I

.field public final FRAME_INNER_02:I

.field public final FRAME_INNER_08:I

.field public final FRAME_INNER_08_2:I

.field public final FRAME_INNER_09:I

.field public final FRAME_INNER_10:I

.field private final LEFT:I

.field public final LEFTBAR:I

.field public final LEFTBAR_BOTTOM:I

.field public final LEFTBAR_TOP:I

.field private final MAX_FRAME_PICES:I

.field private final MAX_IMAGE_HEIGHT:I

.field private final MAX_IMAGE_WIDTH:I

.field private final MAX_INNER_FRAME_PICES:I

.field public final PAPER_DOWN:I

.field public final PAPER_DOWN_MIDDLE:I

.field public final PAPER_LEFT:I

.field public final PAPER_LEFT_MIDDLE:I

.field public final PAPER_MIDDLE:I

.field public final PAPER_RIGHT:I

.field public final PAPER_RIGHT_MIDDLE:I

.field public final PAPER_UP:I

.field public final PAPER_UP_MIDDLE:I

.field private final RIGHT:I

.field public final RIGHTBAR:I

.field public final RIGHTBAR_BOTTOM:I

.field public final RIGHTBAR_TOP:I

.field private final TOP:I

.field public final TOP_LEFT:I

.field public final TOP_MIDDLE:I

.field public final TOP_RIGHT:I

.field public mBitmapFrame:[Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mFrameMode:I

.field private mFrameVectorColor:I

.field private final mFrameVectorRes:[I

.field private mFramedCanvas:Landroid/graphics/Canvas;

.field private final mFramesRes_16_9:[[I

.field private final mFramesRes_1_1:[[I

.field private final mFramesRes_9_16:[[I

.field private mHeight:I

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mIsOrg:Z

.field private mMarginBottom:I

.field private mMarginLeft:I

.field private mMarginRight:I

.field private mMarginTop:I

.field private mMaskPaint:Landroid/graphics/Paint;

.field private mOrgHeight:I

.field private mOrgToPrvRatio:F

.field private mOrgWidth:I

.field private mPaint:Landroid/graphics/Paint;

.field private mResId:I

.field private mResizeToOrg:F

.field mResources:Landroid/content/res/Resources;

.field private mWidth:I


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x5

    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_01:I

    .line 23
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_02:I

    .line 24
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_03:I

    .line 25
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_04:I

    .line 26
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_05:I

    .line 27
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_06:I

    .line 28
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_07:I

    .line 29
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_08:I

    .line 30
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_09:I

    .line 31
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_10:I

    .line 32
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_11:I

    .line 33
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_12:I

    .line 34
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_13:I

    .line 35
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_14:I

    .line 36
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_15:I

    .line 37
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_16:I

    .line 38
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_17:I

    .line 39
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_18:I

    .line 40
    const/16 v0, 0x12

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_19:I

    .line 41
    const/16 v0, 0x13

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_20:I

    .line 42
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_21:I

    .line 43
    const/16 v0, 0x15

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_22:I

    .line 44
    const/16 v0, 0x16

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_23:I

    .line 45
    const/16 v0, 0x17

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_24:I

    .line 46
    const/16 v0, 0x18

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_25:I

    .line 47
    const/16 v0, 0x19

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_26:I

    .line 48
    const/16 v0, 0x1a

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_27:I

    .line 49
    const/16 v0, 0x1b

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_28:I

    .line 50
    const/16 v0, 0x1c

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_29:I

    .line 51
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_INNER_02:I

    .line 52
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_INNER_08:I

    .line 53
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_INNER_08_2:I

    .line 54
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_INNER_09:I

    .line 55
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_INNER_10:I

    .line 57
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->TOP_LEFT:I

    .line 58
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->TOP_MIDDLE:I

    .line 59
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->TOP_RIGHT:I

    .line 60
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->LEFTBAR:I

    .line 61
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->RIGHTBAR:I

    .line 62
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->BOTTOM_LEFT:I

    .line 63
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->BOTTOM_MIDDLE:I

    .line 64
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->BOTTOM_RIGHT:I

    .line 65
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->LEFTBAR_TOP:I

    .line 66
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->LEFTBAR_BOTTOM:I

    .line 67
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->RIGHTBAR_TOP:I

    .line 68
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->RIGHTBAR_BOTTOM:I

    .line 70
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_UP:I

    .line 71
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_UP_MIDDLE:I

    .line 72
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_LEFT:I

    .line 73
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_LEFT_MIDDLE:I

    .line 74
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_RIGHT:I

    .line 75
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_RIGHT_MIDDLE:I

    .line 76
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_DOWN:I

    .line 77
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_DOWN_MIDDLE:I

    .line 78
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_MIDDLE:I

    .line 80
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->TOP:I

    .line 81
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->BOTTOM:I

    .line 82
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->LEFT:I

    .line 83
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->RIGHT:I

    .line 84
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->EFFECT:I

    .line 86
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->MAX_FRAME_PICES:I

    .line 87
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->MAX_INNER_FRAME_PICES:I

    .line 90
    const/16 v0, 0xa

    new-array v0, v0, [[I

    .line 91
    new-array v1, v3, [I

    fill-array-data v1, :array_0

    .line 92
    aput-object v1, v0, v4

    .line 94
    new-array v1, v3, [I

    fill-array-data v1, :array_1

    .line 95
    aput-object v1, v0, v6

    .line 97
    new-array v1, v3, [I

    fill-array-data v1, :array_2

    .line 98
    aput-object v1, v0, v7

    const/4 v1, 0x3

    .line 100
    new-array v2, v3, [I

    fill-array-data v2, :array_3

    .line 101
    aput-object v2, v0, v1

    .line 103
    new-array v1, v3, [I

    fill-array-data v1, :array_4

    .line 104
    aput-object v1, v0, v3

    .line 106
    new-array v1, v3, [I

    fill-array-data v1, :array_5

    .line 107
    aput-object v1, v0, v5

    const/4 v1, 0x6

    .line 109
    new-array v2, v3, [I

    fill-array-data v2, :array_6

    .line 110
    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 112
    new-array v2, v5, [I

    fill-array-data v2, :array_7

    .line 114
    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 116
    new-array v2, v3, [I

    fill-array-data v2, :array_8

    .line 117
    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 119
    new-array v2, v3, [I

    fill-array-data v2, :array_9

    .line 120
    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramesRes_1_1:[[I

    .line 123
    const/16 v0, 0xa

    new-array v0, v0, [[I

    .line 124
    new-array v1, v3, [I

    fill-array-data v1, :array_a

    .line 125
    aput-object v1, v0, v4

    .line 127
    new-array v1, v3, [I

    fill-array-data v1, :array_b

    .line 128
    aput-object v1, v0, v6

    .line 130
    new-array v1, v3, [I

    fill-array-data v1, :array_c

    .line 131
    aput-object v1, v0, v7

    const/4 v1, 0x3

    .line 133
    new-array v2, v3, [I

    fill-array-data v2, :array_d

    .line 134
    aput-object v2, v0, v1

    .line 136
    new-array v1, v3, [I

    fill-array-data v1, :array_e

    .line 137
    aput-object v1, v0, v3

    .line 139
    new-array v1, v3, [I

    fill-array-data v1, :array_f

    .line 140
    aput-object v1, v0, v5

    const/4 v1, 0x6

    .line 142
    new-array v2, v3, [I

    fill-array-data v2, :array_10

    .line 143
    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 145
    new-array v2, v5, [I

    fill-array-data v2, :array_11

    .line 147
    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 149
    new-array v2, v3, [I

    fill-array-data v2, :array_12

    .line 150
    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 152
    new-array v2, v3, [I

    fill-array-data v2, :array_13

    .line 153
    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramesRes_9_16:[[I

    .line 156
    const/16 v0, 0xa

    new-array v0, v0, [[I

    .line 157
    new-array v1, v3, [I

    fill-array-data v1, :array_14

    .line 158
    aput-object v1, v0, v4

    .line 160
    new-array v1, v3, [I

    fill-array-data v1, :array_15

    .line 161
    aput-object v1, v0, v6

    .line 163
    new-array v1, v3, [I

    fill-array-data v1, :array_16

    .line 164
    aput-object v1, v0, v7

    const/4 v1, 0x3

    .line 166
    new-array v2, v3, [I

    fill-array-data v2, :array_17

    .line 167
    aput-object v2, v0, v1

    .line 169
    new-array v1, v3, [I

    fill-array-data v1, :array_18

    .line 170
    aput-object v1, v0, v3

    .line 172
    new-array v1, v3, [I

    fill-array-data v1, :array_19

    .line 173
    aput-object v1, v0, v5

    const/4 v1, 0x6

    .line 175
    new-array v2, v3, [I

    fill-array-data v2, :array_1a

    .line 176
    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 178
    new-array v2, v5, [I

    fill-array-data v2, :array_1b

    .line 180
    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 182
    new-array v2, v3, [I

    fill-array-data v2, :array_1c

    .line 183
    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 185
    new-array v2, v3, [I

    fill-array-data v2, :array_1d

    .line 186
    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramesRes_16_9:[[I

    .line 189
    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_1e

    .line 195
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameVectorRes:[I

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mContext:Landroid/content/Context;

    .line 200
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResources:Landroid/content/res/Resources;

    .line 202
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramedCanvas:Landroid/graphics/Canvas;

    .line 204
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameMode:I

    .line 205
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mWidth:I

    .line 206
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mHeight:I

    .line 207
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginLeft:I

    .line 208
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginRight:I

    .line 209
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginTop:I

    .line 210
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginBottom:I

    .line 211
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    .line 214
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgWidth:I

    .line 215
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgHeight:I

    .line 217
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgToPrvRatio:F

    .line 219
    new-array v0, v5, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    .line 221
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 224
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mPaint:Landroid/graphics/Paint;

    .line 225
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMaskPaint:Landroid/graphics/Paint;

    .line 227
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameVectorColor:I

    .line 228
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    .line 229
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getDisplayHeightBasedOnLand()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->MAX_IMAGE_WIDTH:I

    .line 230
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getDisplayWidthBasedOnLand()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->MAX_IMAGE_HEIGHT:I

    .line 232
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mIsOrg:Z

    .line 262
    return-void

    .line 91
    :array_0
    .array-data 4
        0x7f0201e7
        0x7f0201dd
        0x7f0201e3
        0x7f0201e5
    .end array-data

    .line 94
    :array_1
    .array-data 4
        0x7f0201f3
        0x7f0201e9
        0x7f0201ef
        0x7f0201f1
    .end array-data

    .line 97
    :array_2
    .array-data 4
        0x7f0201ff
        0x7f0201f5
        0x7f0201fb
        0x7f0201fd
    .end array-data

    .line 100
    :array_3
    .array-data 4
        0x7f02020b
        0x7f020201
        0x7f020207
        0x7f020209
    .end array-data

    .line 103
    :array_4
    .array-data 4
        0x7f020217
        0x7f02020d
        0x7f020213
        0x7f020215
    .end array-data

    .line 106
    :array_5
    .array-data 4
        0x7f020223
        0x7f020219
        0x7f02021f
        0x7f020221
    .end array-data

    .line 109
    :array_6
    .array-data 4
        0x7f02022f
        0x7f020225
        0x7f02022b
        0x7f02022d
    .end array-data

    .line 112
    :array_7
    .array-data 4
        0x7f02023e
        0x7f020231
        0x7f02023a
        0x7f02023c
        0x7f020233
    .end array-data

    .line 116
    :array_8
    .array-data 4
        0x7f02024a
        0x7f020240
        0x7f020246
        0x7f020248
    .end array-data

    .line 119
    :array_9
    .array-data 4
        0x7f020256
        0x7f02024c
        0x7f020252
        0x7f020254
    .end array-data

    .line 124
    :array_a
    .array-data 4
        0x7f0201e8
        0x7f0201de
        0x7f0201e4
        0x7f0201e6
    .end array-data

    .line 127
    :array_b
    .array-data 4
        0x7f0201f4
        0x7f0201ea
        0x7f0201f0
        0x7f0201f2
    .end array-data

    .line 130
    :array_c
    .array-data 4
        0x7f020200
        0x7f0201f6
        0x7f0201fc
        0x7f0201fe
    .end array-data

    .line 133
    :array_d
    .array-data 4
        0x7f02020c
        0x7f020202
        0x7f020208
        0x7f02020a
    .end array-data

    .line 136
    :array_e
    .array-data 4
        0x7f020218
        0x7f02020e
        0x7f020214
        0x7f020216
    .end array-data

    .line 139
    :array_f
    .array-data 4
        0x7f020224
        0x7f02021a
        0x7f020220
        0x7f020222
    .end array-data

    .line 142
    :array_10
    .array-data 4
        0x7f020230
        0x7f020226
        0x7f02022c
        0x7f02022e
    .end array-data

    .line 145
    :array_11
    .array-data 4
        0x7f02023f
        0x7f020232
        0x7f02023b
        0x7f02023d
        0x7f020234
    .end array-data

    .line 149
    :array_12
    .array-data 4
        0x7f02024b
        0x7f020241
        0x7f020247
        0x7f020249
    .end array-data

    .line 152
    :array_13
    .array-data 4
        0x7f020257
        0x7f02024d
        0x7f020253
        0x7f020255
    .end array-data

    .line 157
    :array_14
    .array-data 4
        0x7f0201e2
        0x7f0201df
        0x7f0201e0
        0x7f0201e1
    .end array-data

    .line 160
    :array_15
    .array-data 4
        0x7f0201ee
        0x7f0201eb
        0x7f0201ec
        0x7f0201ed
    .end array-data

    .line 163
    :array_16
    .array-data 4
        0x7f0201fa
        0x7f0201f7
        0x7f0201f8
        0x7f0201f9
    .end array-data

    .line 166
    :array_17
    .array-data 4
        0x7f020206
        0x7f020203
        0x7f020204
        0x7f020205
    .end array-data

    .line 169
    :array_18
    .array-data 4
        0x7f020212
        0x7f02020f
        0x7f020210
        0x7f020211
    .end array-data

    .line 172
    :array_19
    .array-data 4
        0x7f02021e
        0x7f02021b
        0x7f02021c
        0x7f02021d
    .end array-data

    .line 175
    :array_1a
    .array-data 4
        0x7f02022a
        0x7f020227
        0x7f020228
        0x7f020229
    .end array-data

    .line 178
    :array_1b
    .array-data 4
        0x7f020239
        0x7f020235
        0x7f020237
        0x7f020238
        0x7f020236
    .end array-data

    .line 182
    :array_1c
    .array-data 4
        0x7f020245
        0x7f020242
        0x7f020243
        0x7f020244
    .end array-data

    .line 185
    :array_1d
    .array-data 4
        0x7f020251
        0x7f02024e
        0x7f02024f
        0x7f020250
    .end array-data

    .line 189
    :array_1e
    .array-data 4
        0x7f0203a8
        0x7f0203a7
        0x7f0203a4
        0x7f0203a5
        0x7f0203a6
        0x7f0203a3
        0x7f0203a2
        0x7f0203b0
        0x7f0203b2
        0x7f0203b4
        0x7f0203b6
        0x7f0203b8
        0x7f0203ba
        0x7f0203bc
        0x7f0203be
        0x7f0203c0
        0x7f0203c2
        0x7f0203c4
        0x7f0203c6
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x5

    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_01:I

    .line 23
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_02:I

    .line 24
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_03:I

    .line 25
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_04:I

    .line 26
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_05:I

    .line 27
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_06:I

    .line 28
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_07:I

    .line 29
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_08:I

    .line 30
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_09:I

    .line 31
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_10:I

    .line 32
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_11:I

    .line 33
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_12:I

    .line 34
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_13:I

    .line 35
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_14:I

    .line 36
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_15:I

    .line 37
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_16:I

    .line 38
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_17:I

    .line 39
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_18:I

    .line 40
    const/16 v0, 0x12

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_19:I

    .line 41
    const/16 v0, 0x13

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_20:I

    .line 42
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_21:I

    .line 43
    const/16 v0, 0x15

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_22:I

    .line 44
    const/16 v0, 0x16

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_23:I

    .line 45
    const/16 v0, 0x17

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_24:I

    .line 46
    const/16 v0, 0x18

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_25:I

    .line 47
    const/16 v0, 0x19

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_26:I

    .line 48
    const/16 v0, 0x1a

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_27:I

    .line 49
    const/16 v0, 0x1b

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_28:I

    .line 50
    const/16 v0, 0x1c

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_29:I

    .line 51
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_INNER_02:I

    .line 52
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_INNER_08:I

    .line 53
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_INNER_08_2:I

    .line 54
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_INNER_09:I

    .line 55
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->FRAME_INNER_10:I

    .line 57
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->TOP_LEFT:I

    .line 58
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->TOP_MIDDLE:I

    .line 59
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->TOP_RIGHT:I

    .line 60
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->LEFTBAR:I

    .line 61
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->RIGHTBAR:I

    .line 62
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->BOTTOM_LEFT:I

    .line 63
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->BOTTOM_MIDDLE:I

    .line 64
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->BOTTOM_RIGHT:I

    .line 65
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->LEFTBAR_TOP:I

    .line 66
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->LEFTBAR_BOTTOM:I

    .line 67
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->RIGHTBAR_TOP:I

    .line 68
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->RIGHTBAR_BOTTOM:I

    .line 70
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_UP:I

    .line 71
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_UP_MIDDLE:I

    .line 72
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_LEFT:I

    .line 73
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_LEFT_MIDDLE:I

    .line 74
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_RIGHT:I

    .line 75
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_RIGHT_MIDDLE:I

    .line 76
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_DOWN:I

    .line 77
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_DOWN_MIDDLE:I

    .line 78
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->PAPER_MIDDLE:I

    .line 80
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->TOP:I

    .line 81
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->BOTTOM:I

    .line 82
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->LEFT:I

    .line 83
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->RIGHT:I

    .line 84
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->EFFECT:I

    .line 86
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->MAX_FRAME_PICES:I

    .line 87
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->MAX_INNER_FRAME_PICES:I

    .line 90
    const/16 v0, 0xa

    new-array v0, v0, [[I

    .line 91
    new-array v1, v3, [I

    fill-array-data v1, :array_0

    .line 92
    aput-object v1, v0, v4

    .line 94
    new-array v1, v3, [I

    fill-array-data v1, :array_1

    .line 95
    aput-object v1, v0, v6

    .line 97
    new-array v1, v3, [I

    fill-array-data v1, :array_2

    .line 98
    aput-object v1, v0, v7

    const/4 v1, 0x3

    .line 100
    new-array v2, v3, [I

    fill-array-data v2, :array_3

    .line 101
    aput-object v2, v0, v1

    .line 103
    new-array v1, v3, [I

    fill-array-data v1, :array_4

    .line 104
    aput-object v1, v0, v3

    .line 106
    new-array v1, v3, [I

    fill-array-data v1, :array_5

    .line 107
    aput-object v1, v0, v5

    const/4 v1, 0x6

    .line 109
    new-array v2, v3, [I

    fill-array-data v2, :array_6

    .line 110
    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 112
    new-array v2, v5, [I

    fill-array-data v2, :array_7

    .line 114
    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 116
    new-array v2, v3, [I

    fill-array-data v2, :array_8

    .line 117
    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 119
    new-array v2, v3, [I

    fill-array-data v2, :array_9

    .line 120
    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramesRes_1_1:[[I

    .line 123
    const/16 v0, 0xa

    new-array v0, v0, [[I

    .line 124
    new-array v1, v3, [I

    fill-array-data v1, :array_a

    .line 125
    aput-object v1, v0, v4

    .line 127
    new-array v1, v3, [I

    fill-array-data v1, :array_b

    .line 128
    aput-object v1, v0, v6

    .line 130
    new-array v1, v3, [I

    fill-array-data v1, :array_c

    .line 131
    aput-object v1, v0, v7

    const/4 v1, 0x3

    .line 133
    new-array v2, v3, [I

    fill-array-data v2, :array_d

    .line 134
    aput-object v2, v0, v1

    .line 136
    new-array v1, v3, [I

    fill-array-data v1, :array_e

    .line 137
    aput-object v1, v0, v3

    .line 139
    new-array v1, v3, [I

    fill-array-data v1, :array_f

    .line 140
    aput-object v1, v0, v5

    const/4 v1, 0x6

    .line 142
    new-array v2, v3, [I

    fill-array-data v2, :array_10

    .line 143
    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 145
    new-array v2, v5, [I

    fill-array-data v2, :array_11

    .line 147
    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 149
    new-array v2, v3, [I

    fill-array-data v2, :array_12

    .line 150
    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 152
    new-array v2, v3, [I

    fill-array-data v2, :array_13

    .line 153
    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramesRes_9_16:[[I

    .line 156
    const/16 v0, 0xa

    new-array v0, v0, [[I

    .line 157
    new-array v1, v3, [I

    fill-array-data v1, :array_14

    .line 158
    aput-object v1, v0, v4

    .line 160
    new-array v1, v3, [I

    fill-array-data v1, :array_15

    .line 161
    aput-object v1, v0, v6

    .line 163
    new-array v1, v3, [I

    fill-array-data v1, :array_16

    .line 164
    aput-object v1, v0, v7

    const/4 v1, 0x3

    .line 166
    new-array v2, v3, [I

    fill-array-data v2, :array_17

    .line 167
    aput-object v2, v0, v1

    .line 169
    new-array v1, v3, [I

    fill-array-data v1, :array_18

    .line 170
    aput-object v1, v0, v3

    .line 172
    new-array v1, v3, [I

    fill-array-data v1, :array_19

    .line 173
    aput-object v1, v0, v5

    const/4 v1, 0x6

    .line 175
    new-array v2, v3, [I

    fill-array-data v2, :array_1a

    .line 176
    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 178
    new-array v2, v5, [I

    fill-array-data v2, :array_1b

    .line 180
    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 182
    new-array v2, v3, [I

    fill-array-data v2, :array_1c

    .line 183
    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 185
    new-array v2, v3, [I

    fill-array-data v2, :array_1d

    .line 186
    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramesRes_16_9:[[I

    .line 189
    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_1e

    .line 195
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameVectorRes:[I

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mContext:Landroid/content/Context;

    .line 200
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResources:Landroid/content/res/Resources;

    .line 202
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramedCanvas:Landroid/graphics/Canvas;

    .line 204
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameMode:I

    .line 205
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mWidth:I

    .line 206
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mHeight:I

    .line 207
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginLeft:I

    .line 208
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginRight:I

    .line 209
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginTop:I

    .line 210
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginBottom:I

    .line 211
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    .line 214
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgWidth:I

    .line 215
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgHeight:I

    .line 217
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgToPrvRatio:F

    .line 219
    new-array v0, v5, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    .line 221
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 224
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mPaint:Landroid/graphics/Paint;

    .line 225
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMaskPaint:Landroid/graphics/Paint;

    .line 227
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameVectorColor:I

    .line 228
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    .line 229
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getDisplayHeightBasedOnLand()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->MAX_IMAGE_WIDTH:I

    .line 230
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getDisplayWidthBasedOnLand()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->MAX_IMAGE_HEIGHT:I

    .line 232
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mIsOrg:Z

    .line 240
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 241
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mContext:Landroid/content/Context;

    .line 242
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResources:Landroid/content/res/Resources;

    .line 243
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mPaint:Landroid/graphics/Paint;

    .line 244
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMaskPaint:Landroid/graphics/Paint;

    .line 245
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 246
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->init()V

    .line 248
    return-void

    .line 91
    :array_0
    .array-data 4
        0x7f0201e7
        0x7f0201dd
        0x7f0201e3
        0x7f0201e5
    .end array-data

    .line 94
    :array_1
    .array-data 4
        0x7f0201f3
        0x7f0201e9
        0x7f0201ef
        0x7f0201f1
    .end array-data

    .line 97
    :array_2
    .array-data 4
        0x7f0201ff
        0x7f0201f5
        0x7f0201fb
        0x7f0201fd
    .end array-data

    .line 100
    :array_3
    .array-data 4
        0x7f02020b
        0x7f020201
        0x7f020207
        0x7f020209
    .end array-data

    .line 103
    :array_4
    .array-data 4
        0x7f020217
        0x7f02020d
        0x7f020213
        0x7f020215
    .end array-data

    .line 106
    :array_5
    .array-data 4
        0x7f020223
        0x7f020219
        0x7f02021f
        0x7f020221
    .end array-data

    .line 109
    :array_6
    .array-data 4
        0x7f02022f
        0x7f020225
        0x7f02022b
        0x7f02022d
    .end array-data

    .line 112
    :array_7
    .array-data 4
        0x7f02023e
        0x7f020231
        0x7f02023a
        0x7f02023c
        0x7f020233
    .end array-data

    .line 116
    :array_8
    .array-data 4
        0x7f02024a
        0x7f020240
        0x7f020246
        0x7f020248
    .end array-data

    .line 119
    :array_9
    .array-data 4
        0x7f020256
        0x7f02024c
        0x7f020252
        0x7f020254
    .end array-data

    .line 124
    :array_a
    .array-data 4
        0x7f0201e8
        0x7f0201de
        0x7f0201e4
        0x7f0201e6
    .end array-data

    .line 127
    :array_b
    .array-data 4
        0x7f0201f4
        0x7f0201ea
        0x7f0201f0
        0x7f0201f2
    .end array-data

    .line 130
    :array_c
    .array-data 4
        0x7f020200
        0x7f0201f6
        0x7f0201fc
        0x7f0201fe
    .end array-data

    .line 133
    :array_d
    .array-data 4
        0x7f02020c
        0x7f020202
        0x7f020208
        0x7f02020a
    .end array-data

    .line 136
    :array_e
    .array-data 4
        0x7f020218
        0x7f02020e
        0x7f020214
        0x7f020216
    .end array-data

    .line 139
    :array_f
    .array-data 4
        0x7f020224
        0x7f02021a
        0x7f020220
        0x7f020222
    .end array-data

    .line 142
    :array_10
    .array-data 4
        0x7f020230
        0x7f020226
        0x7f02022c
        0x7f02022e
    .end array-data

    .line 145
    :array_11
    .array-data 4
        0x7f02023f
        0x7f020232
        0x7f02023b
        0x7f02023d
        0x7f020234
    .end array-data

    .line 149
    :array_12
    .array-data 4
        0x7f02024b
        0x7f020241
        0x7f020247
        0x7f020249
    .end array-data

    .line 152
    :array_13
    .array-data 4
        0x7f020257
        0x7f02024d
        0x7f020253
        0x7f020255
    .end array-data

    .line 157
    :array_14
    .array-data 4
        0x7f0201e2
        0x7f0201df
        0x7f0201e0
        0x7f0201e1
    .end array-data

    .line 160
    :array_15
    .array-data 4
        0x7f0201ee
        0x7f0201eb
        0x7f0201ec
        0x7f0201ed
    .end array-data

    .line 163
    :array_16
    .array-data 4
        0x7f0201fa
        0x7f0201f7
        0x7f0201f8
        0x7f0201f9
    .end array-data

    .line 166
    :array_17
    .array-data 4
        0x7f020206
        0x7f020203
        0x7f020204
        0x7f020205
    .end array-data

    .line 169
    :array_18
    .array-data 4
        0x7f020212
        0x7f02020f
        0x7f020210
        0x7f020211
    .end array-data

    .line 172
    :array_19
    .array-data 4
        0x7f02021e
        0x7f02021b
        0x7f02021c
        0x7f02021d
    .end array-data

    .line 175
    :array_1a
    .array-data 4
        0x7f02022a
        0x7f020227
        0x7f020228
        0x7f020229
    .end array-data

    .line 178
    :array_1b
    .array-data 4
        0x7f020239
        0x7f020235
        0x7f020237
        0x7f020238
        0x7f020236
    .end array-data

    .line 182
    :array_1c
    .array-data 4
        0x7f020245
        0x7f020242
        0x7f020243
        0x7f020244
    .end array-data

    .line 185
    :array_1d
    .array-data 4
        0x7f020251
        0x7f02024e
        0x7f02024f
        0x7f020250
    .end array-data

    .line 189
    :array_1e
    .array-data 4
        0x7f0203a8
        0x7f0203a7
        0x7f0203a4
        0x7f0203a5
        0x7f0203a6
        0x7f0203a3
        0x7f0203a2
        0x7f0203b0
        0x7f0203b2
        0x7f0203b4
        0x7f0203b6
        0x7f0203b8
        0x7f0203ba
        0x7f0203bc
        0x7f0203be
        0x7f0203c0
        0x7f0203c2
        0x7f0203c4
        0x7f0203c6
    .end array-data
.end method

.method private declared-synchronized applyFrame()Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    .line 1114
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 1115
    .local v2, "width":I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    .line 1116
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 1115
    add-int v1, v3, v4

    .line 1118
    .local v1, "height":I
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1120
    .local v0, "frameBitmap":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramedCanvas:Landroid/graphics/Canvas;

    .line 1122
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramedCanvas:Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1124
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramedCanvas:Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v8, 0x2

    aget-object v7, v7, v8

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1126
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramedCanvas:Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1128
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramedCanvas:Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v7, 0x3

    aget-object v6, v6, v7

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1132
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameMode:I

    const/4 v4, 0x7

    if-ne v3, v4, :cond_0

    .line 1133
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramedCanvas:Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    .line 1134
    const/4 v5, 0x0

    .line 1135
    const/4 v6, 0x0

    .line 1136
    const/4 v7, 0x0

    .line 1133
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1138
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->recycleFrameBitmap()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1139
    monitor-exit p0

    return-object v0

    .line 1114
    .end local v0    # "frameBitmap":Landroid/graphics/Bitmap;
    .end local v1    # "height":I
    .end local v2    # "width":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public static calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 6
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    .line 1292
    iget v2, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 1293
    .local v2, "height":I
    iget v4, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 1294
    .local v4, "width":I
    const/4 v3, 0x1

    .line 1296
    .local v3, "inSampleSize":I
    if-gt v2, p2, :cond_0

    if-le v4, p1, :cond_1

    .line 1298
    :cond_0
    div-int/lit8 v0, v2, 0x2

    .line 1299
    .local v0, "halfHeight":I
    div-int/lit8 v1, v4, 0x2

    .line 1304
    .local v1, "halfWidth":I
    :goto_0
    div-int v5, v0, v3

    if-le v5, p2, :cond_1

    .line 1305
    div-int v5, v1, v3

    .line 1304
    if-gt v5, p1, :cond_2

    .line 1310
    .end local v0    # "halfHeight":I
    .end local v1    # "halfWidth":I
    :cond_1
    return v3

    .line 1306
    .restart local v0    # "halfHeight":I
    .restart local v1    # "halfWidth":I
    :cond_2
    mul-int/lit8 v3, v3, 0x2

    goto :goto_0
.end method

.method private createCroppedImageFromOriginal(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "frameBitMap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 818
    const/4 v2, 0x0

    .line 819
    .local v2, "imageBitmap":Landroid/graphics/Bitmap;
    const/4 v10, 0x0

    .line 821
    .local v10, "temp":Landroid/graphics/Bitmap;
    const/4 v7, 0x0

    .line 822
    .local v7, "scaledWidth":I
    const/4 v6, 0x0

    .line 825
    .local v6, "scaledHeight":I
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v4

    .line 826
    .local v4, "orgWidth":I
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    .line 829
    .local v3, "orgHeight":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 830
    .local v1, "frameWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 832
    .local v0, "frameHeight":I
    int-to-float v11, v4

    int-to-float v12, v3

    div-float v5, v11, v12

    .line 834
    .local v5, "ratio":F
    rem-int v11, v4, v1

    if-nez v11, :cond_3

    rem-int v11, v3, v0

    if-nez v11, :cond_3

    .line 835
    move v7, v1

    .line 836
    move v6, v0

    .line 857
    :cond_0
    :goto_0
    if-lt v7, v1, :cond_8

    .line 862
    :goto_1
    if-lt v6, v0, :cond_9

    .line 872
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v11

    const/4 v12, 0x1

    invoke-static {v11, v7, v6, v12}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 874
    move-object v2, v10

    .line 878
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    if-gt v11, v1, :cond_1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    if-le v11, v0, :cond_2

    .line 879
    :cond_1
    const/4 v8, 0x0

    .line 880
    .local v8, "startX":I
    const/4 v9, 0x0

    .line 882
    .local v9, "startY":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    if-le v11, v1, :cond_a

    .line 883
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    sub-int/2addr v11, v1

    div-int/lit8 v8, v11, 0x2

    .line 887
    :goto_2
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    if-le v11, v0, :cond_b

    .line 888
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    sub-int/2addr v11, v0

    div-int/lit8 v9, v11, 0x2

    .line 892
    :goto_3
    invoke-static {v2, v8, v9, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 893
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 894
    move-object v2, v10

    .line 897
    .end local v8    # "startX":I
    .end local v9    # "startY":I
    :cond_2
    return-object v2

    .line 837
    :cond_3
    if-ne v1, v0, :cond_6

    .line 838
    if-ge v4, v3, :cond_4

    .line 839
    move v7, v1

    .line 840
    int-to-float v11, v1

    div-float/2addr v11, v5

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 841
    goto :goto_0

    :cond_4
    if-le v4, v3, :cond_5

    .line 842
    int-to-float v11, v0

    mul-float/2addr v11, v5

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 843
    move v6, v0

    .line 844
    goto :goto_0

    :cond_5
    if-ne v4, v3, :cond_0

    .line 845
    move v7, v1

    .line 846
    move v6, v0

    .line 848
    goto :goto_0

    :cond_6
    if-ge v1, v0, :cond_7

    .line 849
    move v7, v1

    .line 850
    int-to-float v11, v1

    div-float/2addr v11, v5

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 851
    goto :goto_0

    :cond_7
    if-le v1, v0, :cond_0

    .line 852
    int-to-float v11, v0

    mul-float/2addr v11, v5

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 853
    move v6, v0

    .line 857
    goto :goto_0

    .line 858
    :cond_8
    add-int/lit8 v7, v7, 0x5

    .line 859
    int-to-float v11, v7

    div-float/2addr v11, v5

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v6

    goto :goto_0

    .line 863
    :cond_9
    add-int/lit8 v6, v6, 0x5

    .line 864
    int-to-float v11, v0

    mul-float/2addr v11, v5

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v7

    goto :goto_1

    .line 885
    .restart local v8    # "startX":I
    .restart local v9    # "startY":I
    :cond_a
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    goto :goto_2

    .line 890
    :cond_b
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    goto :goto_3
.end method

.method private decodeResource(I)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "resourceId"    # I

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x1

    .line 1061
    const/4 v3, 0x0

    .line 1062
    .local v3, "returnBitmap":Landroid/graphics/Bitmap;
    const/4 v0, 0x0

    .line 1064
    .local v0, "resizeBitmap":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResources:Landroid/content/res/Resources;

    invoke-static {v4, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1065
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgToPrvRatio:F

    cmpl-float v4, v4, v5

    if-nez v4, :cond_0

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_2

    .line 1067
    :cond_0
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1068
    .local v2, "resizedWidth":I
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1070
    .local v1, "resizedHeight":I
    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1071
    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1073
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-ne v4, v2, :cond_1

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-eq v4, v1, :cond_2

    .line 1075
    :cond_1
    invoke-static {v3, v2, v1, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1077
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 1078
    const/4 v3, 0x0

    .line 1079
    move-object v3, v0

    .line 1082
    .end local v1    # "resizedHeight":I
    .end local v2    # "resizedWidth":I
    :cond_2
    return-object v3
.end method

.method private decodeResourceForFrames(I)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "resourceId"    # I

    .prologue
    .line 1086
    const/4 v0, 0x0

    .line 1088
    .local v0, "returnBitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResources:Landroid/content/res/Resources;

    invoke-static {v1, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1089
    return-object v0
.end method

.method private frameVector(Z)Landroid/graphics/Bitmap;
    .locals 23
    .param p1, "done"    # Z

    .prologue
    .line 1188
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->loadFrameVectorBitmap()V

    .line 1189
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->shapeFramesize()[I

    move-result-object v20

    .line 1190
    .local v20, "size":[I
    const/4 v3, 0x0

    aget v22, v20, v3

    .line 1191
    .local v22, "width":I
    const/4 v3, 0x1

    aget v10, v20, v3

    .line 1192
    .local v10, "height":I
    if-nez p1, :cond_0

    .line 1193
    new-instance v15, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v15}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1194
    .local v15, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    iput-boolean v3, v15, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1195
    move/from16 v0, v22

    iput v0, v15, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 1196
    iput v10, v15, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 1198
    const/16 v3, 0xc8

    const/16 v4, 0xc8

    invoke-static {v15, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v19

    .line 1199
    .local v19, "sampleSize":I
    div-int v22, v22, v19

    .line 1200
    div-int v10, v10, v19

    .line 1202
    .end local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v19    # "sampleSize":I
    :cond_0
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v22

    invoke-static {v0, v10, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1204
    .local v1, "ret":Landroid/graphics/Bitmap;
    const/4 v14, 0x0

    .line 1205
    .local v14, "maskWdt":I
    const/4 v13, 0x0

    .line 1207
    .local v13, "maskHgt":I
    move/from16 v0, v22

    if-le v0, v10, :cond_7

    .line 1209
    move v14, v10

    .line 1210
    move v13, v10

    .line 1218
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-ne v3, v14, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-eq v3, v13, :cond_3

    .line 1220
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const/4 v4, 0x1

    invoke-static {v3, v14, v13, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 1221
    .local v21, "temp":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1222
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aput-object v21, v3, v4

    .line 1226
    .end local v21    # "temp":Landroid/graphics/Bitmap;
    :cond_3
    sub-int v3, v22, v14

    div-int/lit8 v16, v3, 0x2

    .line 1227
    .local v16, "paddX":I
    sub-int v3, v10, v13

    div-int/lit8 v17, v3, 0x2

    .line 1229
    .local v17, "paddY":I
    new-instance v9, Landroid/graphics/Canvas;

    invoke-direct {v9, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1230
    .local v9, "canvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameVectorColor:I

    invoke-virtual {v9, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1231
    new-instance v18, Landroid/graphics/Paint;

    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    .line 1232
    .local v18, "paint":Landroid/graphics/Paint;
    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setDither(Z)V

    .line 1233
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 1234
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_4

    .line 1235
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    move/from16 v0, v16

    int-to-float v4, v0

    move/from16 v0, v17

    int-to-float v5, v0

    move-object/from16 v0, v18

    invoke-virtual {v9, v3, v4, v5, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1237
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->recycleFrameBitmap()V

    .line 1238
    mul-int v3, v22, v10

    new-array v2, v3, [I

    .line 1240
    .local v2, "maskData":[I
    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1241
    if-eqz v16, :cond_5

    .line 1243
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    if-lt v11, v10, :cond_8

    .line 1250
    .end local v11    # "i":I
    :cond_5
    if-eqz v17, :cond_6

    .line 1252
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_2
    move/from16 v0, v22

    if-lt v12, v0, :cond_9

    .line 1260
    .end local v12    # "j":I
    :cond_6
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move/from16 v4, v22

    move/from16 v7, v22

    move v8, v10

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1286
    return-object v1

    .line 1214
    .end local v2    # "maskData":[I
    .end local v9    # "canvas":Landroid/graphics/Canvas;
    .end local v16    # "paddX":I
    .end local v17    # "paddY":I
    .end local v18    # "paint":Landroid/graphics/Paint;
    :cond_7
    move/from16 v14, v22

    .line 1215
    move/from16 v13, v22

    goto/16 :goto_0

    .line 1245
    .restart local v2    # "maskData":[I
    .restart local v9    # "canvas":Landroid/graphics/Canvas;
    .restart local v11    # "i":I
    .restart local v16    # "paddX":I
    .restart local v17    # "paddY":I
    .restart local v18    # "paint":Landroid/graphics/Paint;
    :cond_8
    mul-int v3, v11, v22

    add-int v3, v3, v16

    aget v4, v2, v3

    const/high16 v5, -0x1000000

    or-int/2addr v4, v5

    aput v4, v2, v3

    .line 1246
    mul-int v3, v11, v22

    add-int v3, v3, v16

    add-int/2addr v3, v14

    aget v4, v2, v3

    const/high16 v5, -0x1000000

    or-int/2addr v4, v5

    aput v4, v2, v3

    .line 1247
    mul-int v3, v11, v22

    add-int v3, v3, v16

    add-int/2addr v3, v14

    add-int/lit8 v3, v3, -0x1

    aget v4, v2, v3

    const/high16 v5, -0x1000000

    or-int/2addr v4, v5

    aput v4, v2, v3

    .line 1243
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 1254
    .end local v11    # "i":I
    .restart local v12    # "j":I
    :cond_9
    mul-int v3, v17, v22

    add-int/2addr v3, v12

    aget v4, v2, v3

    const/high16 v5, -0x1000000

    or-int/2addr v4, v5

    aput v4, v2, v3

    .line 1255
    add-int v3, v17, v14

    mul-int v3, v3, v22

    add-int/2addr v3, v12

    aget v4, v2, v3

    const/high16 v5, -0x1000000

    or-int/2addr v4, v5

    aput v4, v2, v3

    .line 1256
    add-int v3, v17, v14

    add-int/lit8 v3, v3, -0x1

    mul-int v3, v3, v22

    add-int/2addr v3, v12

    aget v4, v2, v3

    const/high16 v5, -0x1000000

    or-int/2addr v4, v5

    aput v4, v2, v3

    .line 1252
    add-int/lit8 v12, v12, 0x1

    goto :goto_2
.end method

.method private declared-synchronized frame_01_10()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1107
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->loadFrameBitmap()V

    .line 1109
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->applyFrame()Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 1107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private init()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 301
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameMode:I

    .line 302
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mWidth:I

    .line 303
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mHeight:I

    .line 305
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginLeft:I

    .line 306
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginRight:I

    .line 307
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginTop:I

    .line 308
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginBottom:I

    .line 320
    return-void
.end method

.method private declared-synchronized loadFrameBitmap()V
    .locals 8

    .prologue
    const/high16 v7, 0x3fc00000    # 1.5f

    const/high16 v6, 0x3f400000    # 0.75f

    .line 1145
    monitor-enter p0

    const/4 v2, 0x0

    .line 1146
    .local v2, "mFrameRes":[[I
    const/4 v0, 0x4

    .line 1147
    .local v0, "count":I
    :try_start_0
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameMode:I

    const/4 v5, 0x7

    if-ne v4, v5, :cond_0

    .line 1148
    const/4 v0, 0x5

    .line 1151
    :cond_0
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgHeight:I

    int-to-float v5, v5

    div-float v3, v4, v5

    .line 1153
    .local v3, "ratio":F
    cmpg-float v4, v3, v6

    if-gez v4, :cond_2

    .line 1154
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramesRes_9_16:[[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1160
    :cond_1
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v0, :cond_4

    .line 1167
    monitor-exit p0

    return-void

    .line 1155
    .end local v1    # "i":I
    :cond_2
    cmpl-float v4, v3, v6

    if-ltz v4, :cond_3

    cmpg-float v4, v3, v7

    if-gtz v4, :cond_3

    .line 1156
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramesRes_1_1:[[I

    .line 1157
    goto :goto_0

    :cond_3
    cmpl-float v4, v3, v7

    if-lez v4, :cond_1

    .line 1158
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramesRes_16_9:[[I

    goto :goto_0

    .line 1162
    .restart local v1    # "i":I
    :cond_4
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramesRes_1_1:[[I

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameMode:I

    aget-object v4, v4, v5

    aget v4, v4, v1

    if-eqz v4, :cond_5

    .line 1164
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameMode:I

    aget-object v5, v2, v5

    aget v5, v5, v1

    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->decodeResourceForFrames(I)Landroid/graphics/Bitmap;

    move-result-object v5

    aput-object v5, v4, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1160
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1145
    .end local v1    # "i":I
    .end local v3    # "ratio":F
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method private loadFrameVectorBitmap()V
    .locals 4

    .prologue
    .line 1098
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameMode:I

    add-int/lit8 v0, v1, -0xa

    .line 1099
    .local v0, "tempInt":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameVectorRes:[I

    aget v1, v1, v0

    if-eqz v1, :cond_0

    .line 1101
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameVectorRes:[I

    aget v3, v3, v0

    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->decodeResource(I)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1103
    :cond_0
    return-void
.end method

.method private recycleFrameBitmap()V
    .locals 3

    .prologue
    .line 324
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 326
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x5

    if-lt v0, v1, :cond_1

    .line 335
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 328
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 330
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 331
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 326
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public ApplyPreview(IIIIIZ)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "bufferWidth"    # I
    .param p2, "bufferHeight"    # I
    .param p3, "originalWidth"    # I
    .param p4, "originalHeight"    # I
    .param p5, "frameMode"    # I
    .param p6, "done"    # Z

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 991
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->init()V

    .line 992
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mWidth:I

    .line 993
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mHeight:I

    .line 995
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgWidth:I

    .line 996
    iput p4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgHeight:I

    .line 999
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mWidth:I

    int-to-float v5, v5

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->MAX_IMAGE_WIDTH:I

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 1000
    .local v3, "resizeToOrgWidth":F
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mHeight:I

    int-to-float v5, v5

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->MAX_IMAGE_HEIGHT:I

    int-to-float v6, v6

    div-float v2, v5, v6

    .line 1001
    .local v2, "resizeToOrgHeight":F
    cmpl-float v5, v3, v2

    if-lez v5, :cond_1

    .line 1002
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    .line 1007
    :goto_0
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    cmpl-float v5, v5, v7

    if-lez v5, :cond_0

    .line 1008
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    .line 1009
    :cond_0
    iput p5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameMode:I

    .line 1011
    div-int v5, p3, p4

    int-to-float v1, v5

    .line 1012
    .local v1, "ratio":F
    const/4 v0, 0x1

    .line 1018
    .local v0, "height":I
    const/4 v4, 0x0

    .line 1019
    .local v4, "ret":Landroid/graphics/Bitmap;
    packed-switch p5, :pswitch_data_0

    .line 1056
    :goto_1
    return-object v4

    .line 1004
    .end local v0    # "height":I
    .end local v1    # "ratio":F
    .end local v4    # "ret":Landroid/graphics/Bitmap;
    :cond_1
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    goto :goto_0

    .line 1031
    .restart local v0    # "height":I
    .restart local v1    # "ratio":F
    .restart local v4    # "ret":Landroid/graphics/Bitmap;
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->frame_01_10()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1032
    goto :goto_1

    .line 1053
    :pswitch_1
    invoke-direct {p0, p6}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->frameVector(Z)Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_1

    .line 1019
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public Destroy()V
    .locals 2

    .prologue
    .line 287
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 289
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 293
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    .line 295
    .end local v0    # "i":I
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameVectorColor:I

    .line 298
    return-void

    .line 291
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 289
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized applyImage(IZZ)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "type"    # I
    .param p2, "isPageNum"    # Z
    .param p3, "done"    # Z

    .prologue
    .line 560
    monitor-enter p0

    const/4 v1, 0x0

    .local v1, "w":I
    const/4 v2, 0x0

    .line 562
    .local v2, "h":I
    const/4 v5, 0x0

    .line 563
    .local v5, "frameToApply":I
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 564
    const/4 v11, 0x0

    .line 814
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v11

    .line 565
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    .line 566
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    .line 573
    if-eqz p2, :cond_4

    .line 575
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    const v3, 0x3120006e

    if-lt v0, v3, :cond_2

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    const v3, 0x31200077

    if-gt v0, v3, :cond_2

    .line 577
    packed-switch p1, :pswitch_data_0

    .line 686
    :goto_1
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    packed-switch v0, :pswitch_data_1

    .line 777
    :goto_2
    const/4 v8, 0x0

    .line 778
    .local v8, "frameBitMap":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    .line 779
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v4

    move-object v0, p0

    move v6, p3

    .line 778
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->ApplyPreview(IIIIIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 781
    if-eqz p3, :cond_5

    .line 782
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v10

    .line 784
    .local v10, "input":[I
    const/4 v11, 0x0

    .line 785
    .local v11, "ret":Landroid/graphics/Bitmap;
    const/4 v9, 0x0

    .line 786
    .local v9, "imageBitmap":Landroid/graphics/Bitmap;
    const/4 v7, 0x0

    .line 789
    .local v7, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 790
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 789
    invoke-static {v0, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 791
    new-instance v7, Landroid/graphics/Canvas;

    .end local v7    # "canvas":Landroid/graphics/Canvas;
    invoke-direct {v7, v11}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 793
    .restart local v7    # "canvas":Landroid/graphics/Canvas;
    invoke-direct {p0, v8}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->createCroppedImageFromOriginal(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 794
    const/4 v0, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v9, v0, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 796
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    .line 806
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 807
    const/4 v0, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v8, v0, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 808
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 560
    .end local v7    # "canvas":Landroid/graphics/Canvas;
    .end local v8    # "frameBitMap":Landroid/graphics/Bitmap;
    .end local v9    # "imageBitmap":Landroid/graphics/Bitmap;
    .end local v10    # "input":[I
    .end local v11    # "ret":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 580
    :pswitch_0
    const v0, 0x3120006e

    :try_start_2
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 583
    :pswitch_1
    const v0, 0x3120006f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 586
    :pswitch_2
    const v0, 0x31200070

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 589
    :pswitch_3
    const v0, 0x31200071

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 592
    :pswitch_4
    const v0, 0x31200072

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 595
    :pswitch_5
    const v0, 0x31200073

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 598
    :pswitch_6
    const v0, 0x31200074

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 601
    :pswitch_7
    const v0, 0x31200075

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 604
    :pswitch_8
    const v0, 0x31200076

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 607
    :pswitch_9
    const v0, 0x31200077

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 611
    :cond_2
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    const v3, 0x31200078

    if-lt v0, v3, :cond_3

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    const v3, 0x3120007e

    if-gt v0, v3, :cond_3

    .line 613
    packed-switch p1, :pswitch_data_2

    goto/16 :goto_1

    .line 616
    :pswitch_a
    const v0, 0x31200078

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 619
    :pswitch_b
    const v0, 0x31200079

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 622
    :pswitch_c
    const v0, 0x3120007a

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 625
    :pswitch_d
    const v0, 0x3120007b

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 628
    :pswitch_e
    const v0, 0x3120007c

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 631
    :pswitch_f
    const v0, 0x3120007d

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 634
    :pswitch_10
    const v0, 0x3120007e

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 640
    :cond_3
    packed-switch p1, :pswitch_data_3

    goto/16 :goto_1

    .line 643
    :pswitch_11
    const v0, 0x3120007f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 646
    :pswitch_12
    const v0, 0x31200080

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 649
    :pswitch_13
    const v0, 0x31200081

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 652
    :pswitch_14
    const v0, 0x31200082

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 655
    :pswitch_15
    const v0, 0x31200083

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 658
    :pswitch_16
    const v0, 0x31200084

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 661
    :pswitch_17
    const v0, 0x31200085

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 664
    :pswitch_18
    const v0, 0x31200086

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 667
    :pswitch_19
    const v0, 0x31200087

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 670
    :pswitch_1a
    const v0, 0x31200088

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 673
    :pswitch_1b
    const v0, 0x31200089

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 676
    :pswitch_1c
    const v0, 0x3120008a

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 683
    :cond_4
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 689
    :pswitch_1d
    const/4 v5, 0x0

    .line 690
    goto/16 :goto_2

    .line 692
    :pswitch_1e
    const/4 v5, 0x1

    .line 693
    goto/16 :goto_2

    .line 695
    :pswitch_1f
    const/4 v5, 0x2

    .line 696
    goto/16 :goto_2

    .line 698
    :pswitch_20
    const/4 v5, 0x3

    .line 699
    goto/16 :goto_2

    .line 701
    :pswitch_21
    const/4 v5, 0x4

    .line 702
    goto/16 :goto_2

    .line 704
    :pswitch_22
    const/4 v5, 0x5

    .line 705
    goto/16 :goto_2

    .line 707
    :pswitch_23
    const/4 v5, 0x6

    .line 708
    goto/16 :goto_2

    .line 710
    :pswitch_24
    const/4 v5, 0x7

    .line 711
    goto/16 :goto_2

    .line 713
    :pswitch_25
    const/16 v5, 0x8

    .line 714
    goto/16 :goto_2

    .line 716
    :pswitch_26
    const/16 v5, 0x9

    .line 717
    goto/16 :goto_2

    .line 719
    :pswitch_27
    const/16 v5, 0xa

    .line 720
    goto/16 :goto_2

    .line 722
    :pswitch_28
    const/16 v5, 0xb

    .line 723
    goto/16 :goto_2

    .line 725
    :pswitch_29
    const/16 v5, 0xc

    .line 726
    goto/16 :goto_2

    .line 728
    :pswitch_2a
    const/16 v5, 0xd

    .line 729
    goto/16 :goto_2

    .line 731
    :pswitch_2b
    const/16 v5, 0xe

    .line 732
    goto/16 :goto_2

    .line 734
    :pswitch_2c
    const/16 v5, 0xf

    .line 735
    goto/16 :goto_2

    .line 737
    :pswitch_2d
    const/16 v5, 0x10

    .line 738
    goto/16 :goto_2

    .line 740
    :pswitch_2e
    const/16 v5, 0x11

    .line 741
    goto/16 :goto_2

    .line 743
    :pswitch_2f
    const/16 v5, 0x12

    .line 744
    goto/16 :goto_2

    .line 746
    :pswitch_30
    const/16 v5, 0x13

    .line 747
    goto/16 :goto_2

    .line 749
    :pswitch_31
    const/16 v5, 0x14

    .line 750
    goto/16 :goto_2

    .line 752
    :pswitch_32
    const/16 v5, 0x15

    .line 753
    goto/16 :goto_2

    .line 755
    :pswitch_33
    const/16 v5, 0x16

    .line 756
    goto/16 :goto_2

    .line 758
    :pswitch_34
    const/16 v5, 0x17

    .line 759
    goto/16 :goto_2

    .line 761
    :pswitch_35
    const/16 v5, 0x18

    .line 762
    goto/16 :goto_2

    .line 764
    :pswitch_36
    const/16 v5, 0x19

    .line 765
    goto/16 :goto_2

    .line 767
    :pswitch_37
    const/16 v5, 0x1a

    .line 768
    goto/16 :goto_2

    .line 770
    :pswitch_38
    const/16 v5, 0x1b

    .line 771
    goto/16 :goto_2

    .line 773
    :pswitch_39
    const/16 v5, 0x1c

    goto/16 :goto_2

    .restart local v8    # "frameBitMap":Landroid/graphics/Bitmap;
    :cond_5
    move-object v11, v8

    .line 814
    goto/16 :goto_0

    .line 577
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 686
    :pswitch_data_1
    .packed-switch 0x3120006e
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
    .end packed-switch

    .line 613
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 640
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch
.end method

.method public applyOriginal(IIIII)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "bufferWidth"    # I
    .param p2, "bufferHeight"    # I
    .param p3, "originalWidth"    # I
    .param p4, "originalHeight"    # I
    .param p5, "frameMode"    # I

    .prologue
    .line 935
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->init()V

    .line 936
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mWidth:I

    .line 937
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mHeight:I

    .line 939
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->MAX_IMAGE_WIDTH:I

    int-to-float v4, v4

    div-float v12, v3, v4

    .line 940
    .local v12, "resizeToOrgWidth":F
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mHeight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->MAX_IMAGE_HEIGHT:I

    int-to-float v4, v4

    div-float v11, v3, v4

    .line 942
    .local v11, "resizeToOrgHeight":F
    cmpg-float v3, v12, v11

    if-gez v3, :cond_1

    .line 943
    iput v12, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    .line 948
    :goto_0
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 949
    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    .line 951
    :cond_0
    move/from16 v0, p5

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameMode:I

    .line 952
    move/from16 v0, p3

    int-to-float v3, v0

    move/from16 v0, p4

    int-to-float v4, v0

    div-float v10, v3, v4

    .line 953
    .local v10, "ratio":F
    const/4 v9, 0x1

    .line 954
    .local v9, "height":I
    :goto_1
    mul-int v3, v9, v9

    int-to-float v3, v3

    mul-float/2addr v3, v10

    const v4, 0x4af42400    # 8000000.0f

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_2

    .line 958
    mul-int/lit8 v3, p2, 0x2

    int-to-float v3, v3

    int-to-float v4, v9

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgToPrvRatio:F

    .line 959
    const/4 v1, 0x0

    .line 963
    .local v1, "bitmapFrame":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 964
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 963
    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 965
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 966
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    .line 965
    mul-int/2addr v3, v4

    new-array v2, v3, [I

    .line 967
    .local v2, "preBuff":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v2

    .line 969
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 970
    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    .line 971
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v8

    .line 969
    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 985
    return-object v1

    .line 945
    .end local v1    # "bitmapFrame":Landroid/graphics/Bitmap;
    .end local v2    # "preBuff":[I
    .end local v9    # "height":I
    .end local v10    # "ratio":F
    :cond_1
    iput v11, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    goto :goto_0

    .line 956
    .restart local v9    # "height":I
    .restart local v10    # "ratio":F
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public applyOriginal()[I
    .locals 6

    .prologue
    .line 901
    const/4 v4, 0x0

    .local v4, "w":I
    const/4 v2, 0x0

    .line 902
    .local v2, "h":I
    const/4 v0, 0x0

    .line 903
    .local v0, "frameBitMap":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    .line 904
    .local v1, "frameToApply":I
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mIsOrg:Z

    .line 906
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v4

    .line 907
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v2

    .line 911
    const/4 v3, 0x0

    .line 916
    .local v3, "input":[I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v3

    .line 917
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 930
    return-object v3
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;)V
    .locals 1
    .param p1, "frameEffect"    # Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    .prologue
    .line 265
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mContext:Landroid/content/Context;

    .line 266
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResources:Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResources:Landroid/content/res/Resources;

    .line 267
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 268
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    .line 269
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mBitmapFrame:[Landroid/graphics/Bitmap;

    .line 270
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramedCanvas:Landroid/graphics/Canvas;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFramedCanvas:Landroid/graphics/Canvas;

    .line 271
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mWidth:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mWidth:I

    .line 272
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mHeight:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mHeight:I

    .line 273
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginLeft:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginLeft:I

    .line 274
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginTop:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginTop:I

    .line 275
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginRight:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginRight:I

    .line 276
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginBottom:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mMarginBottom:I

    .line 277
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgToPrvRatio:F

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgToPrvRatio:F

    .line 278
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameMode:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameMode:I

    .line 279
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameVectorColor:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameVectorColor:I

    .line 280
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResizeToOrg:F

    .line 283
    return-void
.end method

.method public getCroppedImage(IZ)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "type"    # I
    .param p2, "isPageNum"    # Z

    .prologue
    const v9, 0x3120007e

    const v6, 0x31200078

    const v4, 0x31200077

    const v3, 0x3120006e

    .line 338
    const/4 v8, 0x0

    .line 340
    .local v8, "imageBitmap":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    .local v1, "w":I
    const/4 v2, 0x0

    .line 342
    .local v2, "h":I
    const/4 v5, 0x0

    .line 343
    .local v5, "frameToApply":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v0, :cond_0

    .line 344
    const/4 v0, 0x0

    .line 555
    :goto_0
    return-object v0

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    .line 346
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    .line 348
    if-eqz p2, :cond_3

    .line 349
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    if-lt v0, v3, :cond_1

    .line 350
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    if-gt v0, v4, :cond_1

    .line 351
    packed-switch p1, :pswitch_data_0

    .line 452
    :goto_1
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    packed-switch v0, :pswitch_data_1

    .line 542
    :goto_2
    const/4 v7, 0x0

    .line 543
    .local v7, "frameBitMap":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    .line 544
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v4

    const/4 v6, 0x0

    move-object v0, p0

    .line 543
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->ApplyPreview(IIIIIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 548
    invoke-direct {p0, v7}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->createCroppedImageFromOriginal(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 554
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v8

    .line 555
    goto :goto_0

    .line 353
    .end local v7    # "frameBitMap":Landroid/graphics/Bitmap;
    :pswitch_0
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 356
    :pswitch_1
    const v0, 0x3120006f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 359
    :pswitch_2
    const v0, 0x31200070

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 362
    :pswitch_3
    const v0, 0x31200071

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 365
    :pswitch_4
    const v0, 0x31200072

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 368
    :pswitch_5
    const v0, 0x31200073

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 371
    :pswitch_6
    const v0, 0x31200074

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 374
    :pswitch_7
    const v0, 0x31200075

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 377
    :pswitch_8
    const v0, 0x31200076

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 380
    :pswitch_9
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 383
    :cond_1
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    if-lt v0, v6, :cond_2

    .line 384
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    if-gt v0, v9, :cond_2

    .line 385
    packed-switch p1, :pswitch_data_2

    goto :goto_1

    .line 387
    :pswitch_a
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 390
    :pswitch_b
    const v0, 0x31200079

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 393
    :pswitch_c
    const v0, 0x3120007a

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 396
    :pswitch_d
    const v0, 0x3120007b

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 399
    :pswitch_e
    const v0, 0x3120007c

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto :goto_1

    .line 402
    :pswitch_f
    const v0, 0x3120007d

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 405
    :pswitch_10
    iput v9, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 409
    :cond_2
    packed-switch p1, :pswitch_data_3

    goto/16 :goto_1

    .line 411
    :pswitch_11
    const v0, 0x3120007f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 414
    :pswitch_12
    const v0, 0x31200080

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 417
    :pswitch_13
    const v0, 0x31200081

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 420
    :pswitch_14
    const v0, 0x31200082

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 423
    :pswitch_15
    const v0, 0x31200083

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 426
    :pswitch_16
    const v0, 0x31200084

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 429
    :pswitch_17
    const v0, 0x31200085

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 432
    :pswitch_18
    const v0, 0x31200086

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 435
    :pswitch_19
    const v0, 0x31200087

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 438
    :pswitch_1a
    const v0, 0x31200088

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 441
    :pswitch_1b
    const v0, 0x31200089

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 444
    :pswitch_1c
    const v0, 0x3120008a

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 449
    :cond_3
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    goto/16 :goto_1

    .line 454
    :pswitch_1d
    const/4 v5, 0x0

    .line 455
    goto/16 :goto_2

    .line 457
    :pswitch_1e
    const/4 v5, 0x1

    .line 458
    goto/16 :goto_2

    .line 460
    :pswitch_1f
    const/4 v5, 0x2

    .line 461
    goto/16 :goto_2

    .line 463
    :pswitch_20
    const/4 v5, 0x3

    .line 464
    goto/16 :goto_2

    .line 466
    :pswitch_21
    const/4 v5, 0x4

    .line 467
    goto/16 :goto_2

    .line 469
    :pswitch_22
    const/4 v5, 0x5

    .line 470
    goto/16 :goto_2

    .line 472
    :pswitch_23
    const/4 v5, 0x6

    .line 473
    goto/16 :goto_2

    .line 475
    :pswitch_24
    const/4 v5, 0x7

    .line 476
    goto/16 :goto_2

    .line 478
    :pswitch_25
    const/16 v5, 0x8

    .line 479
    goto/16 :goto_2

    .line 481
    :pswitch_26
    const/16 v5, 0x9

    .line 482
    goto/16 :goto_2

    .line 484
    :pswitch_27
    const/16 v5, 0xa

    .line 485
    goto/16 :goto_2

    .line 487
    :pswitch_28
    const/16 v5, 0xb

    .line 488
    goto/16 :goto_2

    .line 490
    :pswitch_29
    const/16 v5, 0xc

    .line 491
    goto/16 :goto_2

    .line 493
    :pswitch_2a
    const/16 v5, 0xd

    .line 494
    goto/16 :goto_2

    .line 496
    :pswitch_2b
    const/16 v5, 0xe

    .line 497
    goto/16 :goto_2

    .line 499
    :pswitch_2c
    const/16 v5, 0xf

    .line 500
    goto/16 :goto_2

    .line 502
    :pswitch_2d
    const/16 v5, 0x10

    .line 503
    goto/16 :goto_2

    .line 505
    :pswitch_2e
    const/16 v5, 0x11

    .line 506
    goto/16 :goto_2

    .line 508
    :pswitch_2f
    const/16 v5, 0x12

    .line 509
    goto/16 :goto_2

    .line 511
    :pswitch_30
    const/16 v5, 0x13

    .line 512
    goto/16 :goto_2

    .line 514
    :pswitch_31
    const/16 v5, 0x14

    .line 515
    goto/16 :goto_2

    .line 517
    :pswitch_32
    const/16 v5, 0x15

    .line 518
    goto/16 :goto_2

    .line 520
    :pswitch_33
    const/16 v5, 0x16

    .line 521
    goto/16 :goto_2

    .line 523
    :pswitch_34
    const/16 v5, 0x17

    .line 524
    goto/16 :goto_2

    .line 526
    :pswitch_35
    const/16 v5, 0x18

    .line 527
    goto/16 :goto_2

    .line 529
    :pswitch_36
    const/16 v5, 0x19

    .line 530
    goto/16 :goto_2

    .line 532
    :pswitch_37
    const/16 v5, 0x1a

    .line 533
    goto/16 :goto_2

    .line 535
    :pswitch_38
    const/16 v5, 0x1b

    .line 536
    goto/16 :goto_2

    .line 538
    :pswitch_39
    const/16 v5, 0x1c

    goto/16 :goto_2

    .line 351
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 452
    :pswitch_data_1
    .packed-switch 0x3120006e
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
    .end packed-switch

    .line 385
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 409
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch
.end method

.method public getFrameVectorColor()I
    .locals 1

    .prologue
    .line 1318
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameVectorColor:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 257
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getResId()I
    .locals 1

    .prologue
    .line 1326
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 252
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFrameVectorColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 1314
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mFrameVectorColor:I

    .line 1315
    return-void
.end method

.method public setOnCallback(Lcom/sec/android/mimage/photoretouching/Core/FrameEffect$OnCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Core/FrameEffect$OnCallback;

    .prologue
    .line 1331
    return-void
.end method

.method public setResId(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 1322
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mResId:I

    .line 1323
    return-void
.end method

.method public declared-synchronized shapeFramesize()[I
    .locals 8

    .prologue
    const/high16 v7, 0x3fc00000    # 1.5f

    const/high16 v6, 0x3f400000    # 0.75f

    .line 1169
    monitor-enter p0

    const/4 v4, 0x2

    :try_start_0
    new-array v3, v4, [I

    .line 1170
    .local v3, "size":[I
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mOrgHeight:I

    int-to-float v5, v5

    div-float v2, v4, v5

    .line 1171
    .local v2, "ratio":F
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v1, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1172
    .local v1, "deviceWidth":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v0, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1174
    .local v0, "deviceHeight":I
    cmpg-float v4, v2, v6

    if-gez v4, :cond_1

    .line 1175
    const/4 v4, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    aput v5, v3, v4

    .line 1176
    const/4 v4, 0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v5

    aput v5, v3, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1184
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v3

    .line 1177
    :cond_1
    cmpl-float v4, v2, v6

    if-ltz v4, :cond_2

    cmpg-float v4, v2, v7

    if-gtz v4, :cond_2

    .line 1178
    const/4 v4, 0x0

    :try_start_1
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    aput v5, v3, v4

    .line 1179
    const/4 v4, 0x1

    const/4 v5, 0x0

    aget v5, v3, v5

    aput v5, v3, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1169
    .end local v0    # "deviceHeight":I
    .end local v1    # "deviceWidth":I
    .end local v2    # "ratio":F
    .end local v3    # "size":[I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 1180
    .restart local v0    # "deviceHeight":I
    .restart local v1    # "deviceWidth":I
    .restart local v2    # "ratio":F
    .restart local v3    # "size":[I
    :cond_2
    cmpl-float v4, v2, v7

    if-lez v4, :cond_0

    .line 1181
    const/4 v4, 0x0

    :try_start_2
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v5

    aput v5, v3, v4

    .line 1182
    const/4 v4, 0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    aput v5, v3, v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
