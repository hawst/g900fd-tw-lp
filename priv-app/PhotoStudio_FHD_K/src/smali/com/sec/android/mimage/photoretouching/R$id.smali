.class public final Lcom/sec/android/mimage/photoretouching/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final CheckboxLinear:I = 0x7f09001c

.field public static final TextViewLinear:I = 0x7f09001b

.field public static final action_bar_custom_layout:I = 0x7f090009

.field public static final action_bar_title_layout:I = 0x7f090008

.field public static final actionbar_btn_home:I = 0x7f090007

.field public static final actionbar_cancel:I = 0x7f090011

.field public static final actionbar_cancel_icon:I = 0x7f090012

.field public static final actionbar_cancel_text:I = 0x7f090013

.field public static final actionbar_done:I = 0x7f090014

.field public static final actionbar_done_cancel_layout:I = 0x7f090010

.field public static final actionbar_done_icon:I = 0x7f090015

.field public static final actionbar_done_text:I = 0x7f090016

.field public static final actionbar_drop_button:I = 0x7f09000c

.field public static final actionbar_layout:I = 0x7f0900bf

.field public static final actionbar_take_picture:I = 0x7f09000e

.field public static final actionbar_take_picture_icon:I = 0x7f09000f

.field public static final actionbar_take_picture_layout:I = 0x7f09000d

.field public static final actionbar_title_layout:I = 0x7f09000a

.field public static final actionbar_title_text:I = 0x7f09000b

.field public static final activity_layout:I = 0x7f0900c6

.field public static final arrow:I = 0x7f090151

.field public static final body:I = 0x7f09014a

.field public static final bottomToTop:I = 0x7f090000

.field public static final bottom_buttons_layout:I = 0x7f0900b4

.field public static final button_control_layout:I = 0x7f0900c7

.field public static final button_open:I = 0x7f09014c

.field public static final cancel:I = 0x7f0900c0

.field public static final cancel_text:I = 0x7f0900c2

.field public static final canvas_container:I = 0x7f090128

.field public static final checkbox:I = 0x7f09001d

.field public static final clipboard_btn_bar:I = 0x7f090071

.field public static final collage_colorpicker_color:I = 0x7f090079

.field public static final collage_colorpicker_color_01:I = 0x7f09007a

.field public static final collage_colorpicker_color_02:I = 0x7f09007b

.field public static final collage_colorpicker_color_03:I = 0x7f09007c

.field public static final collage_colorpicker_color_04:I = 0x7f09007d

.field public static final collage_colorpicker_color_05:I = 0x7f09007e

.field public static final collage_colorpicker_color_06:I = 0x7f09007f

.field public static final collage_colorpicker_color_07:I = 0x7f090080

.field public static final collage_colorpicker_color_08:I = 0x7f090081

.field public static final collage_colorpicker_color_09:I = 0x7f090082

.field public static final collage_colorpicker_color_10:I = 0x7f090083

.field public static final collage_colorpicker_color_11:I = 0x7f090084

.field public static final collage_colorpicker_color_12:I = 0x7f090085

.field public static final collage_colorpicker_color_13:I = 0x7f090086

.field public static final collage_colorpicker_color_14:I = 0x7f090087

.field public static final collage_colorpicker_palette:I = 0x7f090089

.field public static final collage_colorpicker_palette_picker:I = 0x7f09008a

.field public static final collage_colorpicker_picker:I = 0x7f090088

.field public static final collage_colorpicker_popup:I = 0x7f090078

.field public static final collage_modify_01_replace:I = 0x7f09008c

.field public static final collage_modify_02_remove:I = 0x7f09008d

.field public static final collage_modify_03_effect:I = 0x7f09008e

.field public static final collage_modify_04_rotate:I = 0x7f09008f

.field public static final collage_modify_popup:I = 0x7f09008b

.field public static final color_icon:I = 0x7f090091

.field public static final color_icon_layout:I = 0x7f090090

.field public static final color_picker_layout:I = 0x7f0900b1

.field public static final content:I = 0x7f09014e

.field public static final custom_bar:I = 0x7f090006

.field public static final decoration_container:I = 0x7f0900bc

.field public static final decoration_gridview:I = 0x7f0900dd

.field public static final decorationmenuviewpager:I = 0x7f090153

.field public static final dialog_body:I = 0x7f0900a9

.field public static final dialog_button:I = 0x7f090019

.field public static final dialog_button_divider:I = 0x7f090018

.field public static final dialog_icon_down:I = 0x7f0900ab

.field public static final dialog_icon_up:I = 0x7f0900aa

.field public static final dialog_last_button:I = 0x7f09001a

.field public static final done:I = 0x7f0900c3

.field public static final done_text:I = 0x7f0900c4

.field public static final drawer:I = 0x7f09014d

.field public static final editview:I = 0x7f0900be

.field public static final editview_main_layout:I = 0x7f0900bd

.field public static final filename_edit:I = 0x7f09001f

.field public static final flipper_layout:I = 0x7f0900cf

.field public static final flipper_view:I = 0x7f0900d0

.field public static final frame_layer_layout:I = 0x7f0900b0

.field public static final frame_layout:I = 0x7f0900cc

.field public static final frame_pager_color_icon:I = 0x7f0900ce

.field public static final frame_pager_view:I = 0x7f0900cd

.field public static final handle:I = 0x7f09014f

.field public static final handle_image:I = 0x7f090150

.field public static final help_popup_airbrushface_top:I = 0x7f09012a

.field public static final help_popup_close_btn:I = 0x7f09012b

.field public static final help_popup_color_view_top:I = 0x7f09012d

.field public static final help_popup_facebrightness_top:I = 0x7f09012e

.field public static final help_popup_outoffocus_top:I = 0x7f09012f

.field public static final icon:I = 0x7f090004

.field public static final iconFrame:I = 0x7f090072

.field public static final iconFrameThumb:I = 0x7f090073

.field public static final icon_background:I = 0x7f090077

.field public static final icon_layout:I = 0x7f090130

.field public static final icon_overlay:I = 0x7f090070

.field public static final image:I = 0x7f0900ca

.field public static final imagebackground0:I = 0x7f0900ed

.field public static final imagebackground1:I = 0x7f0900f1

.field public static final imagebackground10:I = 0x7f090115

.field public static final imagebackground11:I = 0x7f090119

.field public static final imagebackground12:I = 0x7f09011d

.field public static final imagebackground13:I = 0x7f090121

.field public static final imagebackground14:I = 0x7f090125

.field public static final imagebackground2:I = 0x7f0900f5

.field public static final imagebackground3:I = 0x7f0900f9

.field public static final imagebackground4:I = 0x7f0900fd

.field public static final imagebackground5:I = 0x7f090101

.field public static final imagebackground6:I = 0x7f090105

.field public static final imagebackground7:I = 0x7f090109

.field public static final imagebackground8:I = 0x7f09010d

.field public static final imagebackground9:I = 0x7f090111

.field public static final imageframe0:I = 0x7f0900ec

.field public static final imageframe1:I = 0x7f0900f0

.field public static final imageframe10:I = 0x7f090114

.field public static final imageframe11:I = 0x7f090118

.field public static final imageframe12:I = 0x7f09011c

.field public static final imageframe13:I = 0x7f090120

.field public static final imageframe14:I = 0x7f090124

.field public static final imageframe2:I = 0x7f0900f4

.field public static final imageframe3:I = 0x7f0900f8

.field public static final imageframe4:I = 0x7f0900fc

.field public static final imageframe5:I = 0x7f090100

.field public static final imageframe6:I = 0x7f090104

.field public static final imageframe7:I = 0x7f090108

.field public static final imageframe8:I = 0x7f09010c

.field public static final imageframe9:I = 0x7f090110

.field public static final imageview:I = 0x7f0900d1

.field public static final imageview0:I = 0x7f0900eb

.field public static final imageview1:I = 0x7f0900ef

.field public static final imageview10:I = 0x7f090113

.field public static final imageview11:I = 0x7f090117

.field public static final imageview12:I = 0x7f09011b

.field public static final imageview13:I = 0x7f09011f

.field public static final imageview14:I = 0x7f090123

.field public static final imageview2:I = 0x7f0900f3

.field public static final imageview3:I = 0x7f0900f7

.field public static final imageview4:I = 0x7f0900fb

.field public static final imageview5:I = 0x7f0900ff

.field public static final imageview6:I = 0x7f090103

.field public static final imageview7:I = 0x7f090107

.field public static final imageview8:I = 0x7f09010b

.field public static final imageview9:I = 0x7f09010f

.field public static final indication_icon1:I = 0x7f090094

.field public static final indication_icon10:I = 0x7f09009d

.field public static final indication_icon11:I = 0x7f09009e

.field public static final indication_icon12:I = 0x7f09009f

.field public static final indication_icon13:I = 0x7f0900a0

.field public static final indication_icon14:I = 0x7f0900a1

.field public static final indication_icon15:I = 0x7f0900a2

.field public static final indication_icon16:I = 0x7f0900a3

.field public static final indication_icon17:I = 0x7f0900a4

.field public static final indication_icon18:I = 0x7f0900a5

.field public static final indication_icon19:I = 0x7f0900a6

.field public static final indication_icon2:I = 0x7f090095

.field public static final indication_icon20:I = 0x7f0900a7

.field public static final indication_icon21:I = 0x7f0900a8

.field public static final indication_icon3:I = 0x7f090096

.field public static final indication_icon4:I = 0x7f090097

.field public static final indication_icon5:I = 0x7f090098

.field public static final indication_icon6:I = 0x7f090099

.field public static final indication_icon7:I = 0x7f09009a

.field public static final indication_icon8:I = 0x7f09009b

.field public static final indication_icon9:I = 0x7f09009c

.field public static final indication_layout:I = 0x7f090092

.field public static final indication_page:I = 0x7f090093

.field public static final l_icon:I = 0x7f090023

.field public static final l_icon_b:I = 0x7f090040

.field public static final l_icon_background:I = 0x7f090022

.field public static final l_icon_background_b:I = 0x7f09003f

.field public static final l_icon_background_g:I = 0x7f090037

.field public static final l_icon_background_r:I = 0x7f09002f

.field public static final l_icon_g:I = 0x7f090038

.field public static final l_icon_hue:I = 0x7f090029

.field public static final l_icon_layout:I = 0x7f090021

.field public static final l_icon_layout_b:I = 0x7f09003e

.field public static final l_icon_layout_g:I = 0x7f090036

.field public static final l_icon_layout_r:I = 0x7f09002e

.field public static final l_icon_r:I = 0x7f090030

.field public static final label_image:I = 0x7f09013d

.field public static final label_left_image:I = 0x7f0900de

.field public static final label_middle_image:I = 0x7f0900e0

.field public static final label_middle_layout:I = 0x7f0900df

.field public static final label_right_image:I = 0x7f0900e2

.field public static final label_text:I = 0x7f0900e1

.field public static final laucher_list_view:I = 0x7f0900e9

.field public static final laucher_select_picture:I = 0x7f0900e6

.field public static final launcher_flipper_layout:I = 0x7f0900e8

.field public static final launcher_indicator:I = 0x7f0900e5

.field public static final launcher_layout:I = 0x7f0900e3

.field public static final launcher_pager_view:I = 0x7f0900e4

.field public static final launcher_thumbnail0:I = 0x7f0900ea

.field public static final launcher_thumbnail1:I = 0x7f0900ee

.field public static final launcher_thumbnail10:I = 0x7f090112

.field public static final launcher_thumbnail11:I = 0x7f090116

.field public static final launcher_thumbnail12:I = 0x7f09011a

.field public static final launcher_thumbnail13:I = 0x7f09011e

.field public static final launcher_thumbnail14:I = 0x7f090122

.field public static final launcher_thumbnail2:I = 0x7f0900f2

.field public static final launcher_thumbnail3:I = 0x7f0900f6

.field public static final launcher_thumbnail4:I = 0x7f0900fa

.field public static final launcher_thumbnail5:I = 0x7f0900fe

.field public static final launcher_thumbnail6:I = 0x7f090102

.field public static final launcher_thumbnail7:I = 0x7f090106

.field public static final launcher_thumbnail8:I = 0x7f09010a

.field public static final launcher_thumbnail9:I = 0x7f09010e

.field public static final leftToRight:I = 0x7f090001

.field public static final left_bar:I = 0x7f0900c9

.field public static final loading_view:I = 0x7f0900ad

.field public static final main_activity_layout:I = 0x7f0900ac

.field public static final main_assistant_layout:I = 0x7f0900bb

.field public static final main_button_layout:I = 0x7f0900ba

.field public static final main_layout:I = 0x7f09006f

.field public static final main_view_assistant_layout:I = 0x7f0900ae

.field public static final main_view_layout:I = 0x7f0900af

.field public static final middle_button:I = 0x7f0900b6

.field public static final middle_button_layout:I = 0x7f0900b5

.field public static final middle_layer:I = 0x7f0900b2

.field public static final middle_layer_layout:I = 0x7f090126

.field public static final no_recent_text:I = 0x7f090127

.field public static final page1:I = 0x7f0900d3

.field public static final page1_image:I = 0x7f0900d4

.field public static final page2:I = 0x7f0900d5

.field public static final page2_image:I = 0x7f0900d6

.field public static final page3:I = 0x7f0900d7

.field public static final page3_image:I = 0x7f0900d8

.field public static final page4:I = 0x7f0900d9

.field public static final page4_image:I = 0x7f0900da

.field public static final page5:I = 0x7f0900db

.field public static final page5_image:I = 0x7f0900dc

.field public static final pen_bottom_line:I = 0x7f090075

.field public static final pen_bottom_text:I = 0x7f090076

.field public static final photoeditor_menu_capture:I = 0x7f09015a

.field public static final photoeditor_menu_clipboard:I = 0x7f09015b

.field public static final photoeditor_menu_gallery:I = 0x7f090155

.field public static final photoeditor_menu_save:I = 0x7f090159

.field public static final photoeditor_menu_saveas:I = 0x7f090156

.field public static final photoeditor_menu_setas:I = 0x7f090158

.field public static final photoeditor_menu_share:I = 0x7f090157

.field public static final r_icon:I = 0x7f090027

.field public static final r_icon_b:I = 0x7f090044

.field public static final r_icon_background:I = 0x7f090026

.field public static final r_icon_background_b:I = 0x7f090043

.field public static final r_icon_background_g:I = 0x7f09003b

.field public static final r_icon_background_r:I = 0x7f090033

.field public static final r_icon_g:I = 0x7f09003c

.field public static final r_icon_hue:I = 0x7f09002b

.field public static final r_icon_layout:I = 0x7f090025

.field public static final r_icon_layout_b:I = 0x7f090042

.field public static final r_icon_layout_g:I = 0x7f09003a

.field public static final r_icon_layout_r:I = 0x7f090032

.field public static final r_icon_r:I = 0x7f090034

.field public static final radio_button:I = 0x7f09001e

.field public static final recent_button_body:I = 0x7f090131

.field public static final recent_button_body_divider_layout:I = 0x7f090132

.field public static final rightToLeft:I = 0x7f090002

.field public static final scroll_layout:I = 0x7f0900c5

.field public static final seekbar:I = 0x7f090024

.field public static final seekbar_b:I = 0x7f090041

.field public static final seekbar_g:I = 0x7f090039

.field public static final seekbar_hue:I = 0x7f09002a

.field public static final seekbar_layout:I = 0x7f090020

.field public static final seekbar_layout_b:I = 0x7f09003d

.field public static final seekbar_layout_collage:I = 0x7f090045

.field public static final seekbar_layout_collage_bg:I = 0x7f090046

.field public static final seekbar_layout_collage_bottom_arrow:I = 0x7f090050

.field public static final seekbar_layout_collage_roundness_icon:I = 0x7f090049

.field public static final seekbar_layout_collage_roundness_layout:I = 0x7f090047

.field public static final seekbar_layout_collage_roundness_seekbar:I = 0x7f09004a

.field public static final seekbar_layout_collage_roundness_text:I = 0x7f090048

.field public static final seekbar_layout_collage_spacing:I = 0x7f09004c

.field public static final seekbar_layout_collage_spacing_icon:I = 0x7f09004e

.field public static final seekbar_layout_collage_spacing_seekbar:I = 0x7f09004f

.field public static final seekbar_layout_collage_spacing_text:I = 0x7f09004d

.field public static final seekbar_layout_collage_spliter:I = 0x7f09004b

.field public static final seekbar_layout_g:I = 0x7f090035

.field public static final seekbar_layout_hue:I = 0x7f090028

.field public static final seekbar_layout_r:I = 0x7f09002d

.field public static final seekbar_layout_rgb:I = 0x7f09002c

.field public static final seekbar_r:I = 0x7f090031

.field public static final select_picture_text:I = 0x7f0900e7

.field public static final selector:I = 0x7f090074

.field public static final set_as_gridview:I = 0x7f090133

.field public static final setasName:I = 0x7f090136

.field public static final setasThumb:I = 0x7f090135

.field public static final setasThumbLayout:I = 0x7f090134

.field public static final setting_container:I = 0x7f090129

.field public static final shareThumbLayout:I = 0x7f090138

.field public static final shareviaName:I = 0x7f09013a

.field public static final shareviaThumb:I = 0x7f090139

.field public static final sharevia_grid_view:I = 0x7f090137

.field public static final sticker_1:I = 0x7f090053

.field public static final sticker_10:I = 0x7f090065

.field public static final sticker_11:I = 0x7f090067

.field public static final sticker_12:I = 0x7f090069

.field public static final sticker_2:I = 0x7f090055

.field public static final sticker_3:I = 0x7f090057

.field public static final sticker_4:I = 0x7f090059

.field public static final sticker_5:I = 0x7f09005b

.field public static final sticker_6:I = 0x7f09005d

.field public static final sticker_7:I = 0x7f09005f

.field public static final sticker_8:I = 0x7f090061

.field public static final sticker_9:I = 0x7f090063

.field public static final sticker_gridview:I = 0x7f09013b

.field public static final sticker_image:I = 0x7f09013c

.field public static final sticker_indicator:I = 0x7f09006d

.field public static final sticker_list_view:I = 0x7f090051

.field public static final sticker_page:I = 0x7f0900d2

.field public static final sticker_pager_list_view:I = 0x7f09006b

.field public static final sticker_pager_view:I = 0x7f09006c

.field public static final sticker_upper_list_view:I = 0x7f090052

.field public static final sticker_view_icon1:I = 0x7f090054

.field public static final sticker_view_icon10:I = 0x7f090066

.field public static final sticker_view_icon11:I = 0x7f090068

.field public static final sticker_view_icon12:I = 0x7f09006a

.field public static final sticker_view_icon2:I = 0x7f090056

.field public static final sticker_view_icon3:I = 0x7f090058

.field public static final sticker_view_icon4:I = 0x7f09005a

.field public static final sticker_view_icon5:I = 0x7f09005c

.field public static final sticker_view_icon6:I = 0x7f09005e

.field public static final sticker_view_icon7:I = 0x7f090060

.field public static final sticker_view_icon8:I = 0x7f090062

.field public static final sticker_view_icon9:I = 0x7f090064

.field public static final sub_button_arrow_layout:I = 0x7f0900b9

.field public static final sub_button_layout:I = 0x7f0900b8

.field public static final sub_menu_layout:I = 0x7f0900b7

.field public static final tab_content:I = 0x7f090141

.field public static final tabhost:I = 0x7f090152

.field public static final tabs:I = 0x7f090140

.field public static final temp_decoration_gridview:I = 0x7f090142

.field public static final temp_tab_host:I = 0x7f09013f

.field public static final text:I = 0x7f090005

.field public static final textView1:I = 0x7f09012c

.field public static final textview:I = 0x7f0900cb

.field public static final thumbnail:I = 0x7f09006e

.field public static final thumbnail_layout:I = 0x7f0900c1

.field public static final thumbnail_main_layout:I = 0x7f0900c8

.field public static final title:I = 0x7f090017

.field public static final topToBottom:I = 0x7f090003

.field public static final tray_button:I = 0x7f090143

.field public static final tray_delete_button:I = 0x7f090146

.field public static final tray_focus_frame:I = 0x7f090144

.field public static final tray_layout:I = 0x7f0900b3

.field public static final tray_layout_button:I = 0x7f090149

.field public static final tray_layout_button_icon:I = 0x7f09014b

.field public static final tray_multi_direction_slidind_drawer:I = 0x7f090147

.field public static final tray_save_noti:I = 0x7f090145

.field public static final tray_scroll_layout:I = 0x7f090148

.field public static final view_pager:I = 0x7f090154

.field public static final watermark_image:I = 0x7f09013e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
