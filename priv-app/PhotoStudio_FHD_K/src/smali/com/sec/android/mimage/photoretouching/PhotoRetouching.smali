.class public Lcom/sec/android/mimage/photoretouching/PhotoRetouching;
.super Landroid/app/Activity;
.source "PhotoRetouching.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/PhotoRetouching$CountGetIntentTimeThread;,
        Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;,
        Lcom/sec/android/mimage/photoretouching/PhotoRetouching$LocalBroadcastReceiver;,
        Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    }
.end annotation


# static fields
.field public static CONFIG_CHANGE:Ljava/lang/String; = null

.field private static final PERSONAL_PAGE_OFF:Ljava/lang/String; = "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

.field private static final PERSONAL_PAGE_ON:Ljava/lang/String; = "com.samsung.android.intent.action.PRIVATE_MODE_ON"

.field public static handler:Landroid/os/Handler;

.field private static mActivity:Landroid/app/Activity;

.field private static mNewConfig:Landroid/content/res/Configuration;


# instance fields
.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field public mCheckOnPause:Z

.field public mCopyTodInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

.field private mCurrentMode:I

.field private mCurrentSubMode:I

.field private mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

.field private mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field public mIsEdit:Z

.field public mIsFirst:Z

.field private mIsGetIntentFinish:Z

.field private mMainLayout:Landroid/widget/FrameLayout;

.field private mOptionMenu:Landroid/view/Menu;

.field private mReceiver:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$LocalBroadcastReceiver;

.field private mScaleH:F

.field private mScaleW:F

.field private mSecretModeReceiver:Landroid/content/BroadcastReceiver;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 625
    new-instance v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$2;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$2;-><init>()V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->handler:Landroid/os/Handler;

    .line 1305
    const-string v0, "config_change"

    sput-object v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->CONFIG_CHANGE:Ljava/lang/String;

    .line 1306
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 137
    new-instance v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$1;-><init>(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    .line 1307
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mIsGetIntentFinish:Z

    .line 1308
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    .line 1309
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1310
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1311
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1312
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1313
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 1315
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 1317
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mIsEdit:Z

    .line 1318
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCheckOnPause:Z

    .line 1319
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mMainLayout:Landroid/widget/FrameLayout;

    .line 1320
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCopyTodInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 1321
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mScaleW:F

    .line 1322
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mScaleH:F

    .line 1324
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCurrentMode:I

    .line 1325
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCurrentSubMode:I

    .line 1327
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mReceiver:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$LocalBroadcastReceiver;

    .line 1328
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mOptionMenu:Landroid/view/Menu;

    .line 70
    return-void
.end method

.method static synthetic access$0()Landroid/content/res/Configuration;
    .locals 1

    .prologue
    .line 1306
    sget-object v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mNewConfig:Landroid/content/res/Configuration;

    return-object v0
.end method

.method static synthetic access$1()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 1304
    sget-object v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$2(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 1306
    sput-object p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mNewConfig:Landroid/content/res/Configuration;

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Z)V
    .locals 0

    .prologue
    .line 1307
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mIsGetIntentFinish:Z

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)Z
    .locals 1

    .prologue
    .line 1307
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mIsGetIntentFinish:Z

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)V
    .locals 0

    .prologue
    .line 1162
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->prcessingException()V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1259
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getCurrentFilePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    .locals 1

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    return-object v0
.end method

.method private createManager()V
    .locals 1

    .prologue
    .line 488
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 489
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 490
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 492
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 494
    return-void
.end method

.method private getCurrentFilePath()Ljava/lang/String;
    .locals 9

    .prologue
    .line 1261
    const/4 v5, 0x0

    .line 1263
    .local v5, "ret":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v8, :cond_2

    .line 1265
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    .line 1266
    .local v3, "iData":Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    if-eqz v3, :cond_2

    .line 1268
    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getUri()Landroid/net/Uri;

    move-result-object v6

    .line 1269
    .local v6, "uri":Landroid/net/Uri;
    if-nez v6, :cond_1

    .line 1271
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 1272
    .local v1, "extras":Landroid/os/Bundle;
    const/4 v0, 0x0

    .line 1273
    .local v0, "count":I
    if-eqz v1, :cond_0

    .line 1275
    const-string v8, "selectedCount"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1277
    :cond_0
    if-lez v0, :cond_1

    .line 1279
    const-string v8, "selectedItems"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 1280
    .local v7, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v7, :cond_1

    .line 1282
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "uri":Landroid/net/Uri;
    check-cast v6, Landroid/net/Uri;

    .line 1287
    .end local v0    # "count":I
    .end local v1    # "extras":Landroid/os/Bundle;
    .end local v7    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .restart local v6    # "uri":Landroid/net/Uri;
    :cond_1
    if-eqz v6, :cond_2

    .line 1289
    invoke-static {p0, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 1290
    .local v4, "path":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 1292
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1293
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1295
    move-object v5, v4

    .line 1301
    .end local v2    # "f":Ljava/io/File;
    .end local v3    # "iData":Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .end local v4    # "path":Ljava/lang/String;
    .end local v6    # "uri":Landroid/net/Uri;
    :cond_2
    return-object v5
.end method

.method private initBroadcastReceiver()V
    .locals 2

    .prologue
    .line 1229
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mReceiver:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$LocalBroadcastReceiver;

    if-nez v1, :cond_0

    .line 1231
    new-instance v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$LocalBroadcastReceiver;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$LocalBroadcastReceiver;-><init>(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mReceiver:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$LocalBroadcastReceiver;

    .line 1232
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1233
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1234
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mReceiver:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$LocalBroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1236
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private initCircumstance()Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 206
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/MemoryStatus;->GetExternalStorageMount(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 207
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->prcessingException()V

    .line 241
    :goto_0
    return v3

    .line 210
    :cond_0
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/MemoryStatus;->checkLowExternalMemory(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 211
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->prcessingException()V

    goto :goto_0

    .line 215
    :cond_1
    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->TEMP_CAMERA_PATH:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 216
    .local v2, "path":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_2

    .line 217
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 219
    :cond_2
    const/4 v2, 0x0

    .line 221
    new-instance v2, Ljava/io/File;

    .end local v2    # "path":Ljava/io/File;
    sget-object v3, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 222
    .restart local v2    # "path":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_3

    .line 223
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 225
    :cond_3
    const/4 v2, 0x0

    .line 227
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v1, v3, Landroid/content/res/Configuration;->orientation:I

    .line 228
    .local v1, "orientation":I
    if-ne v1, v4, :cond_5

    .line 229
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setPortrait()V

    .line 230
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 231
    .local v0, "display":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v3, v5}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setDisplaySize(II)V

    .end local v0    # "display":Landroid/util/DisplayMetrics;
    :cond_4
    :goto_1
    move v3, v4

    .line 241
    goto :goto_0

    .line 233
    :cond_5
    const/4 v3, 0x2

    if-ne v1, v3, :cond_4

    .line 234
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setLandscape()V

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 236
    .restart local v0    # "display":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v3, v5}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setDisplaySize(II)V

    goto :goto_1
.end method

.method private prcessingException()V
    .locals 1

    .prologue
    .line 1164
    const-string v0, "exception!"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 1165
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->finish()V

    .line 1166
    return-void
.end method

.method private registerPrivateModeReceiver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 129
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 130
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 131
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 133
    new-instance v0, Landroid/content/IntentFilter;

    .end local v0    # "filter":Landroid/content/IntentFilter;
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 134
    .restart local v0    # "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 136
    return-void
.end method

.method private unregisterPrivateModeReceiver(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 177
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 179
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 180
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 183
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    goto :goto_0

    .line 182
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    .line 183
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    .line 184
    throw v1
.end method


# virtual methods
.method public buttonsDestroy()V
    .locals 1

    .prologue
    .line 871
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    .line 872
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->destroy()V

    .line 873
    :cond_0
    return-void
.end method

.method public changeViewStatus(I)V
    .locals 9
    .param p1, "status"    # I

    .prologue
    const/4 v1, -0x1

    const/4 v4, 0x0

    .line 876
    if-ne p1, v1, :cond_1

    .line 1075
    :cond_0
    :goto_0
    return-void

    .line 882
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_2

    .line 883
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 885
    :cond_2
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setCurrentMode(I)V

    .line 886
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCurrentMode:I

    .line 888
    const v0, 0xffff

    and-int/2addr v0, p1

    if-eqz v0, :cond_3

    .line 890
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->setSubMenuView()V

    goto :goto_0

    .line 894
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_4

    .line 896
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->destroy()V

    .line 897
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    .line 899
    :cond_4
    sparse-switch p1, :sswitch_data_0

    .line 1071
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_0

    .line 1072
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->setView()V

    goto :goto_0

    .line 901
    :sswitch_0
    const-string v0, "launcher"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_1

    .line 913
    :sswitch_1
    const/high16 v0, 0x20000000

    if-ne p1, v0, :cond_5

    .line 914
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->invalidateOptionsMenu()V

    .line 916
    :cond_5
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    .line 917
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 918
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 919
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 920
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 916
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto :goto_1

    .line 923
    :sswitch_2
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    .line 924
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 925
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 926
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 927
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 928
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    .line 923
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto :goto_1

    .line 931
    :sswitch_3
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    .line 932
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 933
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 934
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 935
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 931
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto :goto_1

    .line 938
    :sswitch_4
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    .line 939
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 940
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 941
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 942
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 938
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto :goto_1

    .line 945
    :sswitch_5
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    .line 946
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 947
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 948
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 949
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 945
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto :goto_1

    .line 952
    :sswitch_6
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    .line 953
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 954
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 955
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 956
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 952
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto :goto_1

    .line 959
    :sswitch_7
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    .line 960
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 961
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 962
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 963
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 959
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto/16 :goto_1

    .line 966
    :sswitch_8
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    .line 967
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 968
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 969
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 970
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 966
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto/16 :goto_1

    .line 973
    :sswitch_9
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;

    .line 974
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 975
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 976
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 977
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 973
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto/16 :goto_1

    .line 980
    :sswitch_a
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    .line 981
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 982
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 983
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 984
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 980
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto/16 :goto_1

    .line 987
    :sswitch_b
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    .line 988
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 989
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 990
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 991
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 987
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto/16 :goto_1

    .line 994
    :sswitch_c
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    .line 995
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 996
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 997
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 998
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 994
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto/16 :goto_1

    .line 1001
    :sswitch_d
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    .line 1002
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1003
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1004
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1005
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1006
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    .line 1001
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto/16 :goto_1

    .line 1009
    :sswitch_e
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    .line 1010
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1011
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1012
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1013
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1014
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    .line 1009
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto/16 :goto_1

    .line 1017
    :sswitch_f
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    .line 1018
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1019
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1020
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1021
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1022
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    .line 1017
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto/16 :goto_1

    .line 1025
    :sswitch_10
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    .line 1026
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1027
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1028
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1029
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1030
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    .line 1025
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto/16 :goto_1

    .line 1033
    :sswitch_11
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .line 1034
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1035
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1036
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1037
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1038
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    .line 1033
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto/16 :goto_1

    .line 1041
    :sswitch_12
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    .line 1042
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1043
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1045
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    move-object v6, v4

    .line 1046
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    .line 1041
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto/16 :goto_1

    .line 1049
    :sswitch_13
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_6

    .line 1051
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    .line 1052
    .local v7, "iData":Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    if-eqz v7, :cond_6

    .line 1054
    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->isAtLeastSaved()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1056
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 1057
    .local v8, "intent":Landroid/content/Intent;
    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1059
    invoke-virtual {p0, v1, v8}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->setResult(ILandroid/content/Intent;)V

    .line 1067
    .end local v7    # "iData":Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .end local v8    # "intent":Landroid/content/Intent;
    :cond_6
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->destroyMainData()V

    .line 1068
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->finish()V

    goto/16 :goto_1

    .line 1063
    .restart local v7    # "iData":Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->setResult(I)V

    goto :goto_2

    .line 899
    nop

    :sswitch_data_0
    .sparse-switch
        0x10000000 -> :sswitch_1
        0x11000000 -> :sswitch_3
        0x11100000 -> :sswitch_4
        0x11200000 -> :sswitch_5
        0x11300000 -> :sswitch_9
        0x14000000 -> :sswitch_a
        0x15000000 -> :sswitch_6
        0x16000000 -> :sswitch_7
        0x17000000 -> :sswitch_c
        0x18000000 -> :sswitch_8
        0x1d000000 -> :sswitch_b
        0x20000000 -> :sswitch_1
        0x2d000000 -> :sswitch_0
        0x2e000000 -> :sswitch_13
        0x31000000 -> :sswitch_2
        0x31100000 -> :sswitch_e
        0x31200000 -> :sswitch_d
        0x31300000 -> :sswitch_f
        0x31400000 -> :sswitch_10
        0x31500000 -> :sswitch_11
        0x31600000 -> :sswitch_12
    .end sparse-switch
.end method

.method public changeViewStatus(ILcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;)V
    .locals 7
    .param p1, "status"    # I
    .param p2, "info"    # Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .prologue
    .line 1078
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 1126
    :cond_0
    :goto_0
    return-void

    .line 1080
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_2

    .line 1081
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 1083
    :cond_2
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setCurrentMode(I)V

    .line 1085
    const v0, 0xffff

    and-int/2addr v0, p1

    if-eqz v0, :cond_3

    .line 1087
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->setSubMenuView()V

    goto :goto_0

    .line 1091
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_4

    .line 1093
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->destroy()V

    .line 1094
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    .line 1096
    :cond_4
    sparse-switch p1, :sswitch_data_0

    .line 1122
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_0

    .line 1123
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->setView()V

    goto :goto_0

    .line 1098
    :sswitch_0
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    .line 1099
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1100
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1101
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1102
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    move-object v6, p2

    .line 1103
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;)V

    .line 1098
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto :goto_1

    .line 1106
    :sswitch_1
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    .line 1107
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1108
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1109
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1110
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    move-object v6, p2

    .line 1111
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;)V

    .line 1106
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto :goto_1

    .line 1114
    :sswitch_2
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    .line 1115
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1116
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1117
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1118
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object v1, p0

    move-object v6, p2

    .line 1119
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;)V

    .line 1114
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    goto :goto_1

    .line 1096
    nop

    :sswitch_data_0
    .sparse-switch
        0x14000000 -> :sswitch_0
        0x15000000 -> :sswitch_1
        0x16000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public checkIfValidData()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 505
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-nez v1, :cond_1

    .line 521
    :cond_0
    :goto_0
    return v0

    .line 509
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v1, :cond_0

    .line 513
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_0

    .line 517
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_0

    .line 521
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public destroyMainData()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 688
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    if-eqz v0, :cond_0

    .line 689
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->destroy()V

    .line 690
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 692
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_1

    .line 693
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->destroy()V

    .line 694
    :cond_1
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    .line 696
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 697
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->destroy()V

    .line 698
    :cond_2
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 700
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_3

    .line 701
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 702
    :cond_3
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 704
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_4

    .line 705
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->destroy()V

    .line 706
    :cond_4
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 708
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_5

    .line 709
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->destroy()V

    .line 710
    :cond_5
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 712
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    if-eqz v0, :cond_6

    .line 713
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->destroy()V

    .line 714
    :cond_6
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 716
    return-void
.end method

.method public getImage(Landroid/content/Intent;)V
    .locals 29
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 246
    new-instance v13, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$CountGetIntentTimeThread;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$CountGetIntentTimeThread;-><init>(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)V

    .line 247
    .local v13, "countGetIntentTimeThread":Ljava/lang/Thread;
    invoke-virtual {v13}, Ljava/lang/Thread;->start()V

    .line 249
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mIsGetIntentFinish:Z

    .line 251
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v19

    .line 252
    .local v19, "extras":Landroid/os/Bundle;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    .line 254
    .local v9, "action":Ljava/lang/String;
    if-nez v19, :cond_0

    if-nez v9, :cond_0

    .line 255
    const v3, 0x7f0601b9

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;I)V

    .line 256
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->prcessingException()V

    .line 484
    :goto_0
    return-void

    .line 260
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v3, :cond_1

    .line 262
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const-string v4, "fromStudio"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setBackKeyState(Z)V

    .line 263
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 264
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 265
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 272
    :cond_1
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 274
    .local v16, "decodeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;>;"
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "android.intent.action.EDIT"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 276
    :cond_2
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 278
    const-string v3, "android.intent.extra.STREAM"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 280
    new-instance v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    invoke-direct {v15}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;-><init>()V

    .line 281
    .local v15, "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    const/4 v3, 0x1

    iput-boolean v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 282
    const-string v3, "android.intent.extra.STREAM"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    iput-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 283
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getImage extra_stream:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 284
    iget-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "content"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 285
    iget-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 290
    :goto_1
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 416
    .end local v15    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    :cond_3
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getImage decodeList.size()"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 417
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 418
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 420
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 421
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v3, v0, v1, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;I)V

    .line 418
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    .line 422
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    new-instance v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$4;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$4;-><init>(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->setChangeViewCallback(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;)V

    .line 448
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    .line 449
    .local v26, "time1":J
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "_data"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 450
    .local v14, "cursor":Landroid/database/Cursor;
    if-eqz v14, :cond_7

    .line 452
    const-string v3, "_id"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 454
    .local v11, "column_index":I
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    .line 455
    invoke-interface {v14, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 456
    .local v22, "imageId":J
    const/4 v3, 0x1

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 457
    .local v24, "path":Ljava/lang/String;
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 458
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-wide/from16 v0, v22

    invoke-static {v3, v0, v1, v4, v5}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 459
    .local v10, "bitmap":Landroid/graphics/Bitmap;
    const v3, 0x7f0900ad

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    .line 461
    .local v21, "img":Landroid/widget/ImageView;
    :try_start_0
    new-instance v18, Landroid/media/ExifInterface;

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 462
    .local v18, "exif":Landroid/media/ExifInterface;
    const-string v3, "Orientation"

    const/4 v4, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v25

    .line 463
    .local v25, "rotation":I
    const/4 v3, 0x6

    move/from16 v0, v25

    if-eq v0, v3, :cond_4

    .line 464
    const/4 v3, 0x3

    move/from16 v0, v25

    if-eq v0, v3, :cond_4

    .line 465
    const/16 v3, 0x8

    move/from16 v0, v25

    if-ne v0, v3, :cond_6

    .line 466
    :cond_4
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 467
    .local v7, "matrix":Landroid/graphics/Matrix;
    const/4 v3, 0x6

    move/from16 v0, v25

    if-ne v0, v3, :cond_14

    .line 468
    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 473
    :cond_5
    :goto_3
    move-object v2, v10

    .line 474
    .local v2, "tmp":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 475
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 481
    .end local v2    # "tmp":Landroid/graphics/Bitmap;
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v18    # "exif":Landroid/media/ExifInterface;
    .end local v25    # "rotation":I
    :cond_6
    :goto_4
    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 483
    .end local v10    # "bitmap":Landroid/graphics/Bitmap;
    .end local v11    # "column_index":I
    .end local v21    # "img":Landroid/widget/ImageView;
    .end local v22    # "imageId":J
    .end local v24    # "path":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->start()V

    goto/16 :goto_0

    .line 288
    .end local v14    # "cursor":Landroid/database/Cursor;
    .end local v26    # "time1":J
    .restart local v15    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    :cond_8
    iget-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    goto/16 :goto_1

    .line 294
    .end local v15    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    :cond_9
    const v3, 0x7f0601b9

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;I)V

    .line 295
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->prcessingException()V

    goto/16 :goto_0

    .line 301
    :cond_a
    const/4 v12, 0x0

    .line 302
    .local v12, "count":I
    if-eqz v19, :cond_b

    .line 304
    const-string v3, "selectedCount"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 306
    :cond_b
    if-lez v12, :cond_12

    .line 308
    const-string v3, "selectedItems"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v28

    .line 309
    .local v28, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v28, :cond_3

    .line 311
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_5
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v3

    move/from16 v0, v20

    if-ge v0, v3, :cond_3

    .line 313
    new-instance v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    invoke-direct {v15}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;-><init>()V

    .line 315
    .restart local v15    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    const/4 v3, 0x1

    iput-boolean v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 316
    move-object/from16 v0, v28

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    iput-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 318
    iget-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    if-nez v3, :cond_c

    .line 320
    const v3, 0x7f0601b9

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;I)V

    .line 321
    const/high16 v3, 0x2e000000

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0

    .line 325
    :cond_c
    if-nez v20, :cond_d

    .line 326
    const/4 v3, 0x1

    iput-boolean v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isMain:Z

    .line 327
    :cond_d
    iget-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "content"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 328
    iget-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 332
    :goto_6
    iget-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    if-nez v3, :cond_f

    .line 334
    const v3, 0x7f0601b9

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;I)V

    .line 335
    const/high16 v3, 0x2e000000

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0

    .line 330
    :cond_e
    iget-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    goto :goto_6

    .line 338
    :cond_f
    iget-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->existsFile(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10

    .line 340
    const v3, 0x7f0601b9

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;I)V

    .line 341
    const/high16 v3, 0x2e000000

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0

    .line 345
    :cond_10
    iget-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    const-string v4, ".jpg"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_11

    iget-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    const-string v4, ".JPG"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_11

    .line 346
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/Image;->setPng(Z)V

    .line 352
    :goto_7
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_5

    .line 349
    :cond_11
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/Image;->setPng(Z)V

    goto :goto_7

    .line 358
    .end local v15    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .end local v20    # "i":I
    .end local v28    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_12
    new-instance v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    invoke-direct {v15}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;-><init>()V

    .line 359
    .restart local v15    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    const/4 v3, 0x1

    iput-boolean v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 360
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    iput-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 361
    const/4 v3, 0x1

    iput-boolean v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isMain:Z

    .line 362
    iget-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "content"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 363
    iget-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 366
    :goto_8
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 365
    :cond_13
    iget-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v15, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    goto :goto_8

    .line 469
    .end local v12    # "count":I
    .end local v15    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .restart local v7    # "matrix":Landroid/graphics/Matrix;
    .restart local v10    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v11    # "column_index":I
    .restart local v14    # "cursor":Landroid/database/Cursor;
    .restart local v18    # "exif":Landroid/media/ExifInterface;
    .restart local v21    # "img":Landroid/widget/ImageView;
    .restart local v22    # "imageId":J
    .restart local v24    # "path":Ljava/lang/String;
    .restart local v25    # "rotation":I
    .restart local v26    # "time1":J
    :cond_14
    const/4 v3, 0x3

    move/from16 v0, v25

    if-ne v0, v3, :cond_15

    .line 470
    const/high16 v3, 0x43340000    # 180.0f

    :try_start_1
    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3

    .line 478
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v18    # "exif":Landroid/media/ExifInterface;
    .end local v25    # "rotation":I
    :catch_0
    move-exception v17

    .line 479
    .local v17, "e":Ljava/lang/Exception;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    .line 471
    .end local v17    # "e":Ljava/lang/Exception;
    .restart local v7    # "matrix":Landroid/graphics/Matrix;
    .restart local v18    # "exif":Landroid/media/ExifInterface;
    .restart local v25    # "rotation":I
    :cond_15
    const/16 v3, 0x8

    move/from16 v0, v25

    if-ne v0, v3, :cond_5

    .line 472
    const/high16 v3, 0x43870000    # 270.0f

    :try_start_2
    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_3
.end method

.method public getImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 1256
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    return-object v0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 1224
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mScaleW:F

    return v0
.end method

.method public initButtons()V
    .locals 0

    .prologue
    .line 868
    return-void
.end method

.method public invalidateViews()V
    .locals 1

    .prologue
    .line 1157
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_0

    .line 1159
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->invalidateViewsWithThread()V

    .line 1161
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    .line 820
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 821
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;>;"
    new-instance v7, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    invoke-direct {v7}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;-><init>()V

    .line 822
    .local v7, "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 826
    const/high16 v0, 0x80000

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 827
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 828
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCurrentSubMode:I

    .line 829
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setCurrentSubModeForActivity(I)V

    .line 832
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v2, 0x0

    move-object v0, p0

    move v4, p1

    move v5, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->activityResult(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;Ljava/util/ArrayList;IILandroid/content/Intent;)V

    .line 833
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, 0x1

    .line 642
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 644
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x1a000000

    if-ne v0, v1, :cond_1

    .line 645
    sput-object p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mNewConfig:Landroid/content/res/Configuration;

    .line 669
    :cond_0
    :goto_0
    return-void

    .line 649
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCheckOnPause:Z

    if-nez v0, :cond_2

    if-eqz p1, :cond_3

    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v0, v2, :cond_3

    .line 652
    :cond_2
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCurrentMode:I

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setCurrentMode(I)V

    .line 654
    :cond_3
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_5

    .line 656
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setPortrait()V

    .line 662
    :cond_4
    :goto_1
    const v0, 0x7f03003e

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->setContentView(I)V

    .line 664
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_6

    .line 665
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->onConfigurationChanged()V

    goto :goto_0

    .line 658
    :cond_5
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 660
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setLandscape()V

    goto :goto_1

    .line 666
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setConfigChange(Z)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v6, 0x400

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 84
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 85
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 86
    const v5, 0x7f070001

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->setTheme(I)V

    .line 88
    :cond_0
    sput-object p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActivity:Landroid/app/Activity;

    .line 89
    const/16 v5, 0x9

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->requestWindowFeature(I)Z

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/ActionBar;->hide()V

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getWindow()Landroid/view/Window;

    move-result-object v4

    .line 94
    .local v4, "win":Landroid/view/Window;
    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 95
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 96
    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 97
    const/high16 v5, 0x100000

    iget v6, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/2addr v5, v6

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 98
    invoke-virtual {v4, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 105
    :try_start_0
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 106
    .local v0, "config":Landroid/view/ViewConfiguration;
    const-class v5, Landroid/view/ViewConfiguration;

    const-string v6, "sHasPermanentMenuKey"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 107
    .local v3, "menuKeyField":Ljava/lang/reflect/Field;
    if-eqz v3, :cond_1

    .line 108
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 109
    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    .end local v0    # "config":Landroid/view/ViewConfiguration;
    .end local v3    # "menuKeyField":Ljava/lang/reflect/Field;
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->initCircumstance()Z

    move-result v5

    if-nez v5, :cond_2

    .line 124
    :goto_1
    return-void

    .line 111
    :catch_0
    move-exception v1

    .line 112
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 118
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    const v5, 0x7f03003e

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->setContentView(I)V

    .line 120
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->createManager()V

    .line 121
    invoke-direct {p0, p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->registerPrivateModeReceiver(Landroid/content/Context;)V

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getImage(Landroid/content/Intent;)V

    .line 123
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mIsFirst:Z

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v6, 0x1

    .line 721
    const/4 v3, 0x0

    .line 722
    .local v3, "ret":Z
    const-string v4, "onCreateOptionsMenu()"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 723
    const-string v4, "JW onCreateOptionsMenu"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 724
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    const/high16 v5, 0x1c000000

    if-eq v4, v5, :cond_3

    .line 725
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    const/high16 v5, 0x10000000

    if-eq v4, v5, :cond_3

    .line 726
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    const/high16 v5, 0x20000000

    if-eq v4, v5, :cond_3

    .line 727
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    const/high16 v5, 0x1e110000

    if-eq v4, v5, :cond_3

    .line 728
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    const/high16 v5, 0x2c000000

    if-eq v4, v5, :cond_3

    .line 729
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    const/high16 v5, 0x15000000

    if-ne v4, v5, :cond_0

    .line 730
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v4

    if-eq v4, v6, :cond_3

    .line 731
    :cond_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    const/high16 v5, 0x11000000

    if-eq v4, v5, :cond_3

    .line 732
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    const/high16 v5, 0x16000000

    if-ne v4, v5, :cond_1

    .line 733
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v4

    if-eq v4, v6, :cond_3

    .line 734
    :cond_1
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    const/high16 v5, 0x18000000

    if-ne v4, v5, :cond_2

    .line 735
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v4

    if-eq v4, v6, :cond_3

    .line 736
    :cond_2
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    const/high16 v5, 0x31000000

    if-ne v4, v5, :cond_4

    .line 738
    :cond_3
    if-eqz p1, :cond_4

    .line 739
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 740
    .local v2, "inflater":Landroid/view/MenuInflater;
    const v4, 0x7f080002

    invoke-virtual {v2, v4, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 741
    const v4, 0x7f09015a

    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 742
    const v4, 0x7f090155

    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 743
    const v4, 0x7f090156

    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 744
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getButtonList()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getButtonList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 745
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getButtonList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_5

    .line 755
    .end local v1    # "i":I
    .end local v2    # "inflater":Landroid/view/MenuInflater;
    :cond_4
    :goto_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    .line 765
    return v3

    .line 746
    .restart local v1    # "i":I
    .restart local v2    # "inflater":Landroid/view/MenuInflater;
    :cond_5
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getButtonList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 747
    .local v0, "button":Landroid/view/View;
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v4

    const/16 v5, 0x10

    if-ne v4, v5, :cond_6

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_6

    .line 748
    const v4, 0x7f090157

    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_1

    .line 745
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 674
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->destroyMainData()V

    .line 675
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_0

    .line 676
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->destroy()V

    .line 677
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->destroyStampsOrFrames()V

    .line 679
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 680
    sput-object v1, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    .line 681
    invoke-direct {p0, p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->unregisterPrivateModeReceiver(Landroid/content/Context;)V

    .line 682
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->enhanceFlag:Z

    .line 683
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 684
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 611
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 612
    const/4 v0, 0x0

    .line 622
    :cond_0
    :goto_0
    return v0

    .line 615
    :cond_1
    const/4 v0, 0x1

    .line 616
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v1, :cond_2

    .line 617
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->onKeyUp(ILandroid/view/KeyEvent;)Z

    .line 619
    :cond_2
    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    .line 620
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .prologue
    .line 188
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 189
    const-string v0, "android.nfc.action.TAG_DISCOVERED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$3;-><init>(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 791
    const-string v0, "JW onOptionsItemSelected: PhotoRetouching"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 792
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    if-eqz v0, :cond_0

    .line 793
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->onOptionsItemSelected(I)V

    .line 794
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    if-eqz v0, :cond_1

    .line 795
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->onOptionsItemSelected(I)V

    .line 796
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    if-eqz v0, :cond_2

    .line 797
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->onOptionsItemSelected(I)V

    .line 798
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    if-eqz v0, :cond_3

    .line 799
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->onOptionsItemSelected(I)V

    .line 800
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    if-eqz v0, :cond_4

    .line 801
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->onOptionsItemSelected(I)V

    .line 802
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    if-eqz v0, :cond_5

    .line 803
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->onOptionsItemSelected(I)V

    .line 804
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    if-eqz v0, :cond_6

    .line 805
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->onOptionsItemSelected(I)V

    .line 806
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    if-eqz v0, :cond_7

    .line 807
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->onOptionsItemSelected(I)V

    .line 808
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    if-eqz v0, :cond_8

    .line 809
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->onOptionsItemSelected(I)V

    .line 810
    :cond_8
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    if-eqz v0, :cond_9

    .line 811
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->onOptionsItemSelected(I)V

    .line 813
    :cond_9
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 589
    const-string v0, "onPause"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 590
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->pause()V

    .line 592
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCheckOnPause:Z

    .line 594
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCurrentMode:I

    .line 595
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCurrentSubMode:I

    .line 597
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 598
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 772
    const/4 v0, 0x0

    .line 774
    .local v0, "ret":Z
    const-string v1, "JW onPrepareOptionsMenu"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 775
    const-string v1, "preapareOptions"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 777
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 779
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_0

    .line 780
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setMenu(Landroid/view/Menu;)V

    .line 782
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->invalidateOptionsMenu()V

    .line 783
    return v0
.end method

.method public onResume()V
    .locals 10

    .prologue
    const/high16 v9, 0x2e000000

    const/4 v8, 0x0

    .line 527
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 528
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->initCircumstance()Z

    move-result v7

    if-nez v7, :cond_1

    .line 584
    :cond_0
    :goto_0
    return-void

    .line 531
    :cond_1
    iget-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCheckOnPause:Z

    if-eqz v7, :cond_0

    .line 533
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCurrentMode:I

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setCurrentModeForActivity(I)V

    .line 534
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCurrentSubMode:I

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setCurrentSubModeForActivity(I)V

    .line 535
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mCheckOnPause:Z

    .line 536
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->checkIfValidData()Z

    move-result v7

    if-nez v7, :cond_2

    .line 538
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->reCreate()V

    .line 539
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mIsFirst:Z

    .line 541
    :cond_2
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    .line 542
    .local v3, "iData":Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    if-eqz v3, :cond_8

    .line 544
    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getUri()Landroid/net/Uri;

    move-result-object v5

    .line 545
    .local v5, "uri":Landroid/net/Uri;
    if-nez v5, :cond_4

    .line 547
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 548
    .local v1, "extras":Landroid/os/Bundle;
    const/4 v0, 0x0

    .line 549
    .local v0, "count":I
    if-eqz v1, :cond_3

    .line 551
    const-string v7, "selectedCount"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 553
    :cond_3
    if-lez v0, :cond_4

    .line 555
    const-string v7, "selectedItems"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 556
    .local v6, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v6, :cond_4

    .line 558
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "uri":Landroid/net/Uri;
    check-cast v5, Landroid/net/Uri;

    .line 563
    .end local v0    # "count":I
    .end local v1    # "extras":Landroid/os/Bundle;
    .end local v6    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .restart local v5    # "uri":Landroid/net/Uri;
    :cond_4
    if-nez v5, :cond_5

    .line 564
    invoke-virtual {p0, v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 565
    :cond_5
    invoke-static {p0, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 566
    .local v4, "path":Ljava/lang/String;
    if-eqz v4, :cond_7

    .line 568
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 569
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_6

    .line 571
    invoke-virtual {p0, v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 581
    .end local v2    # "f":Ljava/io/File;
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_6
    :goto_1
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    if-eqz v7, :cond_0

    .line 582
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->resume()V

    goto :goto_0

    .line 575
    .restart local v4    # "path":Ljava/lang/String;
    .restart local v5    # "uri":Landroid/net/Uri;
    :cond_7
    invoke-virtual {p0, v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_1

    .line 579
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getImage(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public reCreate()V
    .locals 1

    .prologue
    .line 498
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->createManager()V

    .line 499
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 500
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mIsFirst:Z

    .line 502
    return-void
.end method

.method public refreshView()V
    .locals 1

    .prologue
    .line 837
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    if-eqz v0, :cond_0

    .line 838
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->refreshView()V

    .line 839
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    if-eqz v0, :cond_1

    .line 840
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->refreshView()V

    .line 841
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    if-eqz v0, :cond_2

    .line 842
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->refreshView()V

    .line 843
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    if-eqz v0, :cond_3

    .line 844
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->refreshView()V

    .line 845
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    if-eqz v0, :cond_4

    .line 846
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->refreshView()V

    .line 847
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    if-eqz v0, :cond_5

    .line 848
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->refreshView()V

    .line 849
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    if-eqz v0, :cond_6

    .line 850
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->refreshView()V

    .line 851
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    if-eqz v0, :cond_7

    .line 852
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->refreshView()V

    .line 853
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    if-eqz v0, :cond_8

    .line 854
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->refreshView()V

    .line 855
    :cond_8
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    if-eqz v0, :cond_9

    .line 856
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->refreshView()V

    .line 857
    :cond_9
    return-void
.end method

.method public refreshViewFromEffectManger(I)V
    .locals 1
    .param p1, "resultCode"    # I

    .prologue
    .line 862
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    if-eqz v0, :cond_0

    .line 863
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mViewFrame:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->refreshViewFromEffectManager(I)V

    .line 864
    :cond_0
    return-void
.end method

.method public setScale(FF)V
    .locals 0
    .param p1, "scaleW"    # F
    .param p2, "scaleH"    # F

    .prologue
    .line 1219
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mScaleW:F

    .line 1220
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mScaleH:F

    .line 1221
    return-void
.end method
