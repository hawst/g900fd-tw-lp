.class Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;
.super Landroid/os/AsyncTask;
.source "RotateView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V
    .locals 0

    .prologue
    .line 732
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    .line 731
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 733
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 10
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 736
    const/4 v2, 0x0

    .line 737
    .local v2, "fileName":Ljava/lang/String;
    array-length v5, p1

    if-lez v5, :cond_0

    .line 738
    const/4 v5, 0x0

    aget-object v2, p1, v5

    .line 740
    :cond_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getAvailableExternalMemorySize()J

    move-result-wide v0

    .line 741
    .local v0, "available_memsize":J
    const-wide/32 v6, 0xa00000

    cmp-long v5, v0, v6

    if-gez v5, :cond_3

    .line 742
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/app/ProgressDialog;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 743
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/app/ProgressDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->dismiss()V

    .line 744
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "memory size = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " %"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 746
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f06007b

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 776
    :cond_2
    :goto_0
    return-object v9

    .line 751
    :cond_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 753
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v6

    .line 754
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v7

    .line 755
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v8

    .line 753
    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([III)V

    .line 759
    :cond_4
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 761
    const/4 v3, 0x0

    .line 762
    .local v3, "filePath":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_6

    .line 764
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 765
    .local v4, "tempPath":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 771
    .end local v4    # "tempPath":Ljava/lang/String;
    :goto_1
    if-nez v2, :cond_5

    .line 772
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSimpleDate()Ljava/lang/String;

    move-result-object v2

    .line 774
    :cond_5
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v5

    invoke-virtual {v5, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->saveCurrentImage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 769
    :cond_6
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const v4, 0x7f0601df

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 787
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 788
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 789
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 792
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 794
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isPersonalPage()Z

    move-result v2

    if-nez v2, :cond_2

    .line 796
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Studio"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 797
    .local v1, "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 812
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 814
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 815
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 816
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    .line 818
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 819
    return-void

    .line 801
    .end local v1    # "text":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPrivateSaveFolder()Ljava/lang/String;

    move-result-object v0

    .line 802
    .local v0, "privateFolder":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 803
    .restart local v1    # "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 808
    .end local v0    # "privateFolder":Ljava/lang/String;
    .end local v1    # "text":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Studio"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 809
    .restart local v1    # "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 779
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 780
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Landroid/app/ProgressDialog;)V

    .line 781
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06008f

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 782
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 783
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 784
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 785
    return-void
.end method
