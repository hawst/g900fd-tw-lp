.class Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;
.super Ljava/lang/Object;
.source "SubViewButtonsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;

.field private final synthetic val$scrollTo:I


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;

    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;->val$scrollTo:I

    .line 1325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1329
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;)Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v2

    if-nez v2, :cond_2

    .line 1330
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    const/high16 v3, 0x1e110000

    if-eq v2, v3, :cond_0

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    const/high16 v3, 0x2c000000

    if-ne v2, v3, :cond_1

    .line 1331
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;)Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    move-result-object v2

    invoke-static {v2, v5}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;Z)V

    .line 1332
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;)Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1333
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;)Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setAnimation(Z)V

    .line 1345
    :cond_1
    :goto_0
    return-void

    .line 1337
    :cond_2
    const v1, 0x3fdeb852    # 1.74f

    .line 1338
    .local v1, "scrollSpeed":F
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;)Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setVisibility(I)V

    .line 1339
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;)Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    const/high16 v3, 0x16000000

    if-eq v2, v3, :cond_4

    .line 1340
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;)Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1341
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;)Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->subMenuAnimate()V

    .line 1342
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;)Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v2

    const-string v3, "scrollX"

    const/4 v4, 0x1

    new-array v4, v4, [I

    aput v5, v4, v5

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1343
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13$1;->val$scrollTo:I

    int-to-float v2, v2

    mul-float/2addr v2, v1

    float-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1344
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method
