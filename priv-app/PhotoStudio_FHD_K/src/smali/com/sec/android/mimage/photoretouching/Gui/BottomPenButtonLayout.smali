.class public Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;
.super Landroid/widget/LinearLayout;
.source "BottomPenButtonLayout.java"


# instance fields
.field private mButton:[Landroid/widget/LinearLayout;

.field private mButtonId:I

.field private mButtonType:I

.field private mContext:Landroid/content/Context;

.field private mDivider:Landroid/widget/LinearLayout;

.field private mEraserBtn:Landroid/widget/LinearLayout;

.field private mIcon:Landroid/widget/ImageView;

.field private mIconId:I

.field private mLayout:Landroid/view/ViewGroup;

.field private mPenBackBtn:Landroid/widget/LinearLayout;

.field private mPenBtn:Landroid/widget/LinearLayout;

.field private mSelected:Z

.field private mText:Landroid/widget/TextView;

.field private penColorLine:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v2, 0x7f03002f

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 20
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mSelected:Z

    .line 21
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mButtonId:I

    .line 22
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mIconId:I

    .line 23
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mButtonType:I

    .line 168
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mIcon:Landroid/widget/ImageView;

    .line 169
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mText:Landroid/widget/TextView;

    .line 170
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 171
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mContext:Landroid/content/Context;

    .line 175
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mPenBtn:Landroid/widget/LinearLayout;

    .line 176
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mEraserBtn:Landroid/widget/LinearLayout;

    .line 177
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mPenBackBtn:Landroid/widget/LinearLayout;

    .line 178
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 179
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->penColorLine:Landroid/widget/LinearLayout;

    .line 27
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mContext:Landroid/content/Context;

    .line 28
    invoke-static {p1, v2, p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 30
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mButtonId:I

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLandroid/widget/LinearLayout;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "flag"    # Z
    .param p3, "penBtn"    # Landroid/widget/LinearLayout;
    .param p4, "eraserBtn"    # Landroid/widget/LinearLayout;
    .param p5, "penBackBtn"    # Landroid/widget/LinearLayout;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 103
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 20
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mSelected:Z

    .line 21
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mButtonId:I

    .line 22
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mIconId:I

    .line 23
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mButtonType:I

    .line 168
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mIcon:Landroid/widget/ImageView;

    .line 169
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mText:Landroid/widget/TextView;

    .line 170
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 171
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mContext:Landroid/content/Context;

    .line 175
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mPenBtn:Landroid/widget/LinearLayout;

    .line 176
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mEraserBtn:Landroid/widget/LinearLayout;

    .line 177
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mPenBackBtn:Landroid/widget/LinearLayout;

    .line 178
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 179
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->penColorLine:Landroid/widget/LinearLayout;

    .line 105
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mPenBtn:Landroid/widget/LinearLayout;

    .line 106
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mEraserBtn:Landroid/widget/LinearLayout;

    .line 107
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mPenBackBtn:Landroid/widget/LinearLayout;

    .line 108
    if-eqz p2, :cond_0

    .line 110
    const v0, 0x7f03002f

    invoke-static {p1, v0, p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 118
    :goto_0
    return-void

    .line 116
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->initWidget(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private initWidget(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 121
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mButton:[Landroid/widget/LinearLayout;

    .line 123
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mButton:[Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mPenBackBtn:Landroid/widget/LinearLayout;

    aput-object v2, v0, v1

    .line 124
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mButton:[Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mPenBtn:Landroid/widget/LinearLayout;

    aput-object v2, v0, v1

    .line 125
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mButton:[Landroid/widget/LinearLayout;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mEraserBtn:Landroid/widget/LinearLayout;

    aput-object v2, v0, v1

    .line 126
    return-void
.end method


# virtual methods
.method public configurationChanged()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->removeAllViews()V

    .line 158
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mButtonId:I

    invoke-static {v1, v2, p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 159
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mButtonType:I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mIconId:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->init(II)V

    .line 161
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mSelected:Z

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->setSelected(Z)V

    .line 162
    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 163
    .local v0, "v":Landroid/view/View;
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mSelected:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 164
    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->setPressed(Z)V

    .line 166
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mIcon:Landroid/widget/ImageView;

    .line 55
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mText:Landroid/widget/TextView;

    .line 56
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mContext:Landroid/content/Context;

    .line 57
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 58
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 59
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 60
    return-void
.end method

.method public init(II)V
    .locals 3
    .param p1, "buttonType"    # I
    .param p2, "iconId"    # I

    .prologue
    const/4 v2, 0x0

    .line 63
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mButtonType:I

    .line 64
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mIconId:I

    .line 66
    const v0, 0x7f090004

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mIcon:Landroid/widget/ImageView;

    .line 67
    const v0, 0x7f090076

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mText:Landroid/widget/TextView;

    .line 68
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 73
    const v0, 0x1a001842

    if-ne p1, v0, :cond_3

    .line 74
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mText:Landroid/widget/TextView;

    const v1, 0x7f060069

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 78
    :cond_1
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->setId(I)V

    .line 79
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 80
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030021

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 81
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 82
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->addView(Landroid/view/View;)V

    .line 86
    :cond_2
    return-void

    .line 76
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mText:Landroid/widget/TextView;

    const v1, 0x7f06008a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public isFocused()Z
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mLayout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    return v0
.end method

.method public removeColorLine()V
    .locals 2

    .prologue
    .line 98
    const v0, 0x7f090075

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->penColorLine:Landroid/widget/LinearLayout;

    .line 99
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->penColorLine:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 100
    return-void
.end method

.method public removeDivider()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 89
    return-void
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 134
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 135
    return-void
.end method

.method public setEnabledWidthChildren(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 138
    const v2, 0x7f090005

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 139
    .local v1, "tView":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 141
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 143
    :cond_0
    const v2, 0x7f090004

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 144
    .local v0, "icon":Landroid/widget/ImageView;
    if-eqz v0, :cond_1

    .line 146
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 148
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 149
    return-void
.end method

.method public setIcon(I)V
    .locals 1
    .param p1, "iconId"    # I

    .prologue
    .line 91
    const v0, 0x7f090004

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mIcon:Landroid/widget/ImageView;

    .line 92
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 96
    :cond_0
    return-void
.end method

.method public setSelected(Z)V
    .locals 1
    .param p1, "selected"    # Z

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mSelected:Z

    .line 130
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->mSelected:Z

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 131
    return-void
.end method
