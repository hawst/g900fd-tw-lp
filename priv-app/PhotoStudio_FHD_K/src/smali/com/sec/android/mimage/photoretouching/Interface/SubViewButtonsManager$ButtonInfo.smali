.class Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
.super Ljava/lang/Object;
.source "SubViewButtonsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ButtonInfo"
.end annotation


# instance fields
.field private mButtonId:I

.field private mButtonIndex:I

.field private mRecentUsed:Z

.field private mUse:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;IIII)V
    .locals 4
    .param p2, "buttonId"    # I
    .param p3, "buttonIdx"    # I
    .param p4, "recentUsed"    # I
    .param p5, "use"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 2632
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    .line 2631
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2627
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mButtonId:I

    .line 2628
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mButtonIndex:I

    .line 2629
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mRecentUsed:Z

    .line 2630
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mUse:Z

    .line 2633
    const/4 v0, 0x0

    .line 2634
    .local v0, "bRecentUsed":Z
    const/4 v1, 0x0

    .line 2635
    .local v1, "bUse":Z
    if-lez p4, :cond_0

    .line 2636
    const/4 v0, 0x1

    .line 2637
    :cond_0
    if-lez p5, :cond_1

    .line 2638
    const/4 v1, 0x1

    .line 2639
    :cond_1
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mButtonId:I

    .line 2640
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mButtonIndex:I

    .line 2641
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mRecentUsed:Z

    .line 2642
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mUse:Z

    .line 2643
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;)Z
    .locals 1

    .prologue
    .line 2629
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mRecentUsed:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;)I
    .locals 1

    .prologue
    .line 2628
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mButtonIndex:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;)I
    .locals 1

    .prologue
    .line 2627
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mButtonId:I

    return v0
.end method


# virtual methods
.method public getButtonId()I
    .locals 1

    .prologue
    .line 2646
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mButtonId:I

    return v0
.end method

.method public getButtonIndex()I
    .locals 1

    .prologue
    .line 2650
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mButtonIndex:I

    return v0
.end method

.method public getButtonRecentUsed()Z
    .locals 1

    .prologue
    .line 2654
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mRecentUsed:Z

    return v0
.end method

.method public getButtonUse()Z
    .locals 1

    .prologue
    .line 2658
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mUse:Z

    return v0
.end method
