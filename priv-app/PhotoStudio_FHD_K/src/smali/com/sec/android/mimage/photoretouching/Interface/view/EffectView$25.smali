.class Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;
.super Ljava/lang/Object;
.source "EffectView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initUndoRedoAllDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    .line 1964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/4 v4, 0x0

    .line 1968
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewWidth()I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$32(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewHeight()I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$33(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->undoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 1969
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->refreshImageAndBottomButtons()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$34(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 1970
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1972
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1973
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 1984
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1986
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1987
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 1994
    :cond_1
    :goto_1
    return-void

    .line 1977
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1978
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 1979
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    .line 1980
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableSave()V

    .line 1981
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setIsEdited(Z)V

    goto :goto_0

    .line 1991
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1992
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_1
.end method
