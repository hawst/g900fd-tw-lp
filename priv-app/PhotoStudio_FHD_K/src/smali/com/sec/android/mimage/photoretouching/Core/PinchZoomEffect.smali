.class public Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
.super Ljava/lang/Object;
.source "PinchZoomEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;,
        Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MySimpleOnScaleGestureListener;,
        Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;
    }
.end annotation


# instance fields
.field private final TOUCH_TOLERANCE:F

.field private mCenterFromOriginal:Landroid/graphics/PointF;

.field private mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

.field private mIsBottomMax:Z

.field private mIsLeftMax:Z

.field private mIsMultiTouchEnd:Z

.field private mIsMultiTouchStart:Z

.field private mIsPinchZoom:Z

.field private mIsRightMax:Z

.field private mIsTopMax:Z

.field private mIsTranslate:Z

.field private mListener:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

.field private mMatrix:Landroid/graphics/Matrix;

.field private mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

.field private mSavedMatrix:Landroid/graphics/Matrix;

.field private mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field private mWithSingleTouch:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "withSingleTouch"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 785
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    .line 786
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    .line 788
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    .line 789
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mSavedMatrix:Landroid/graphics/Matrix;

    .line 792
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mWithSingleTouch:Z

    .line 793
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsLeftMax:Z

    .line 794
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsRightMax:Z

    .line 795
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTopMax:Z

    .line 796
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsBottomMax:Z

    .line 797
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTranslate:Z

    .line 798
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsPinchZoom:Z

    .line 799
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .line 800
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mListener:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .line 801
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 802
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCenterFromOriginal:Landroid/graphics/PointF;

    .line 804
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->TOUCH_TOLERANCE:F

    .line 28
    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MySimpleOnScaleGestureListener;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MySimpleOnScaleGestureListener;-><init>(Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MySimpleOnScaleGestureListener;)V

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 29
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mSavedMatrix:Landroid/graphics/Matrix;

    .line 30
    iput-boolean p2, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mWithSingleTouch:Z

    .line 31
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCenterFromOriginal:Landroid/graphics/PointF;

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLandroid/view/ScaleGestureDetector;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "withSingleTouch"    # Z
    .param p3, "scaleGestureDetector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 785
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    .line 786
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    .line 788
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    .line 789
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mSavedMatrix:Landroid/graphics/Matrix;

    .line 792
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mWithSingleTouch:Z

    .line 793
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsLeftMax:Z

    .line 794
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsRightMax:Z

    .line 795
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTopMax:Z

    .line 796
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsBottomMax:Z

    .line 797
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTranslate:Z

    .line 798
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsPinchZoom:Z

    .line 799
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .line 800
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mListener:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .line 801
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 802
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCenterFromOriginal:Landroid/graphics/PointF;

    .line 804
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->TOUCH_TOLERANCE:F

    .line 35
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 36
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mSavedMatrix:Landroid/graphics/Matrix;

    .line 37
    iput-boolean p2, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mWithSingleTouch:Z

    .line 38
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCenterFromOriginal:Landroid/graphics/PointF;

    .line 39
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 788
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;Z)V
    .locals 0

    .prologue
    .line 798
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsPinchZoom:Z

    return-void
.end method

.method private matrixTurning(Landroid/graphics/Matrix;)V
    .locals 22
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 502
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    .line 504
    const/16 v18, 0x9

    move/from16 v0, v18

    new-array v14, v0, [F

    .line 505
    .local v14, "value":[F
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->getValues([F)V

    .line 506
    const/16 v18, 0x9

    move/from16 v0, v18

    new-array v12, v0, [F

    .line 507
    .local v12, "savedValue":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mSavedMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Landroid/graphics/Matrix;->getValues([F)V

    .line 509
    const/16 v18, 0x0

    aget v18, v14, v18

    const/high16 v19, 0x41200000    # 10.0f

    cmpl-float v18, v18, v19

    if-gtz v18, :cond_0

    const/16 v18, 0x4

    aget v18, v14, v18

    const/high16 v19, 0x41200000    # 10.0f

    cmpl-float v18, v18, v19

    if-lez v18, :cond_2

    .line 511
    :cond_0
    const/16 v18, 0x0

    const/16 v19, 0x0

    aget v19, v12, v19

    aput v19, v14, v18

    .line 512
    const/16 v18, 0x4

    const/16 v19, 0x4

    aget v19, v12, v19

    aput v19, v14, v18

    .line 513
    const/16 v18, 0x2

    const/16 v19, 0x2

    aget v19, v12, v19

    aput v19, v14, v18

    .line 514
    const/16 v18, 0x5

    const/16 v19, 0x5

    aget v19, v12, v19

    aput v19, v14, v18

    .line 516
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->setValues([F)V

    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mSavedMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 518
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->setSupMatrix(Landroid/graphics/Matrix;)V

    .line 631
    .end local v12    # "savedValue":[F
    .end local v14    # "value":[F
    :cond_1
    :goto_0
    return-void

    .line 521
    .restart local v12    # "savedValue":[F
    .restart local v14    # "value":[F
    :cond_2
    const/16 v18, 0x0

    aget v18, v14, v18

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-ltz v18, :cond_3

    const/16 v18, 0x4

    aget v18, v14, v18

    const/high16 v19, 0x3f800000    # 1.0f

    cmpg-float v18, v18, v19

    if-gez v18, :cond_4

    .line 523
    :cond_3
    const/16 v18, 0x0

    const/high16 v19, 0x3f800000    # 1.0f

    aput v19, v14, v18

    .line 524
    const/16 v18, 0x4

    const/high16 v19, 0x3f800000    # 1.0f

    aput v19, v14, v18

    .line 525
    const/16 v18, 0x2

    const/16 v19, 0x2

    aget v19, v12, v19

    aput v19, v14, v18

    .line 526
    const/16 v18, 0x5

    const/16 v19, 0x5

    aget v19, v12, v19

    aput v19, v14, v18

    .line 528
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->setValues([F)V

    .line 529
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mSavedMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 530
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->setSupMatrix(Landroid/graphics/Matrix;)V

    .line 534
    :cond_4
    const/16 v16, 0x0

    .line 535
    .local v16, "viewWidth":I
    const/4 v15, 0x0

    .line 536
    .local v15, "viewHeight":I
    const/4 v10, 0x0

    .line 538
    .local v10, "r":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewWidth()I

    move-result v16

    .line 539
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewHeight()I

    move-result v15

    .line 541
    new-instance v10, Landroid/graphics/RectF;

    .end local v10    # "r":Landroid/graphics/RectF;
    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v10, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 543
    .restart local v10    # "r":Landroid/graphics/RectF;
    if-eqz p1, :cond_5

    .line 544
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 546
    :cond_5
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v7

    .line 548
    .local v7, "height":F
    const/4 v5, 0x0

    .line 550
    .local v5, "deltaY":F
    int-to-float v0, v15

    move/from16 v18, v0

    cmpg-float v18, v7, v18

    if-gez v18, :cond_a

    .line 552
    int-to-float v0, v15

    move/from16 v18, v0

    sub-float v18, v18, v7

    const/high16 v19, 0x40000000    # 2.0f

    div-float v18, v18, v19

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v19, v0

    sub-float v5, v18, v19

    .line 553
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTopMax:Z

    .line 554
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsBottomMax:Z

    .line 575
    :goto_1
    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    move-result v17

    .line 577
    .local v17, "width":F
    const/4 v4, 0x0

    .line 579
    .local v4, "deltaX":F
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v18, v0

    cmpg-float v18, v17, v18

    if-gez v18, :cond_d

    .line 581
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v18, v0

    sub-float v18, v18, v17

    const/high16 v19, 0x40000000    # 2.0f

    div-float v18, v18, v19

    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    sub-float v4, v18, v19

    .line 582
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsLeftMax:Z

    .line 583
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsRightMax:Z

    .line 603
    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 604
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mSavedMatrix:Landroid/graphics/Matrix;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 605
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->setSupMatrix(Landroid/graphics/Matrix;)V

    .line 607
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v9

    .line 608
    .local v9, "m":Landroid/graphics/Matrix;
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 609
    .local v8, "inverse":Landroid/graphics/Matrix;
    invoke-virtual {v9, v8}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 610
    new-instance v11, Landroid/graphics/RectF;

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v11, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 611
    .local v11, "rect":Landroid/graphics/RectF;
    invoke-virtual {v9, v11}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 613
    iget v0, v11, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_6

    .line 614
    const/16 v18, 0x0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->left:F

    .line 615
    :cond_6
    iget v0, v11, Landroid/graphics/RectF;->right:F

    move/from16 v18, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v19, v0

    cmpl-float v18, v18, v19

    if-lez v18, :cond_7

    .line 616
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->right:F

    .line 617
    :cond_7
    iget v0, v11, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    const/16 v19, 0x0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_8

    .line 618
    const/16 v18, 0x0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->top:F

    .line 619
    :cond_8
    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    int-to-float v0, v15

    move/from16 v19, v0

    cmpl-float v18, v18, v19

    if-lez v18, :cond_9

    .line 620
    int-to-float v0, v15

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v11, Landroid/graphics/RectF;->bottom:F

    .line 622
    :cond_9
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v13, v0, [F

    .line 623
    .local v13, "src":[F
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v6, v0, [F

    .line 624
    .local v6, "dst":[F
    const/16 v18, 0x0

    iget v0, v11, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    iget v0, v11, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v11, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    const/high16 v21, 0x40000000    # 2.0f

    div-float v20, v20, v21

    add-float v19, v19, v20

    aput v19, v13, v18

    .line 625
    const/16 v18, 0x1

    iget v0, v11, Landroid/graphics/RectF;->top:F

    move/from16 v19, v0

    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v11, Landroid/graphics/RectF;->top:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    const/high16 v21, 0x40000000    # 2.0f

    div-float v20, v20, v21

    add-float v19, v19, v20

    aput v19, v13, v18

    .line 626
    invoke-virtual {v8, v11}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 627
    invoke-virtual {v8, v6, v13}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 629
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCenterFromOriginal:Landroid/graphics/PointF;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aget v19, v6, v19

    const/16 v20, 0x1

    aget v20, v6, v20

    invoke-virtual/range {v18 .. v20}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_0

    .line 558
    .end local v4    # "deltaX":F
    .end local v6    # "dst":[F
    .end local v8    # "inverse":Landroid/graphics/Matrix;
    .end local v9    # "m":Landroid/graphics/Matrix;
    .end local v11    # "rect":Landroid/graphics/RectF;
    .end local v13    # "src":[F
    .end local v17    # "width":F
    :cond_a
    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    const/16 v19, 0x0

    cmpl-float v18, v18, v19

    if-lez v18, :cond_b

    .line 560
    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    move/from16 v0, v18

    neg-float v5, v0

    .line 561
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTopMax:Z

    goto/16 :goto_1

    .line 564
    :cond_b
    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v18, v0

    int-to-float v0, v15

    move/from16 v19, v0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_c

    .line 566
    int-to-float v0, v15

    move/from16 v18, v0

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v19, v0

    sub-float v5, v18, v19

    .line 567
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsBottomMax:Z

    goto/16 :goto_1

    .line 571
    :cond_c
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTopMax:Z

    .line 572
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsBottomMax:Z

    goto/16 :goto_1

    .line 587
    .restart local v4    # "deltaX":F
    .restart local v17    # "width":F
    :cond_d
    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    const/16 v19, 0x0

    cmpl-float v18, v18, v19

    if-lez v18, :cond_e

    .line 589
    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    move/from16 v0, v18

    neg-float v4, v0

    .line 590
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsLeftMax:Z

    goto/16 :goto_2

    .line 592
    :cond_e
    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v18, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v19, v0

    cmpg-float v18, v18, v19

    if-gez v18, :cond_f

    .line 594
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v18, v0

    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    sub-float v4, v18, v19

    .line 595
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsRightMax:Z

    goto/16 :goto_2

    .line 599
    :cond_f
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsLeftMax:Z

    .line 600
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsRightMax:Z

    goto/16 :goto_2
.end method

.method private reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 675
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    if-eqz v0, :cond_0

    .line 677
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 678
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 679
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 682
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    if-eqz v0, :cond_1

    .line 684
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    if-eqz v0, :cond_1

    .line 685
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 686
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 690
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mSavedMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 691
    return-void
.end method

.method private set(Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "curr"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 694
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    if-eqz v1, :cond_0

    .line 695
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 697
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 699
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 701
    .local v0, "prev":Landroid/view/MotionEvent;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    if-eqz v1, :cond_1

    .line 703
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-le v1, v5, :cond_2

    .line 704
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-le v1, v5, :cond_2

    .line 706
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    sub-float/2addr v2, v3

    iput v2, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->fingerDiffX:F

    .line 707
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    sub-float/2addr v2, v3

    iput v2, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->fingerDiffY:F

    .line 708
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    sub-float/2addr v2, v3

    iput v2, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->fingerDiffX:F

    .line 709
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    sub-float/2addr v2, v3

    iput v2, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->fingerDiffY:F

    .line 711
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget v3, v3, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->fingerDiffX:F

    mul-float/2addr v3, v6

    add-float/2addr v2, v3

    iput v2, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusX:F

    .line 712
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget v3, v3, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->fingerDiffY:F

    mul-float/2addr v3, v6

    add-float/2addr v2, v3

    iput v2, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusY:F

    .line 714
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget v3, v3, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->fingerDiffX:F

    mul-float/2addr v3, v6

    add-float/2addr v2, v3

    iput v2, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusX:F

    .line 715
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget v3, v3, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->fingerDiffY:F

    mul-float/2addr v3, v6

    add-float/2addr v2, v3

    iput v2, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusY:F

    .line 726
    :cond_1
    :goto_0
    return-void

    .line 719
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iput v2, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusX:F

    .line 720
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    iput v2, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusY:F

    .line 722
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iput v2, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusX:F

    .line 723
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    iput v2, v1, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusY:F

    goto :goto_0
.end method

.method private singleTouchScroll(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 639
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 640
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 673
    :goto_0
    return-void

    .line 642
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->reset()V

    .line 644
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 646
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->set(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 649
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->set(Landroid/view/MotionEvent;)V

    .line 651
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget v3, v3, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusX:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget v4, v4, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusX:F

    sub-float v1, v3, v4

    .line 652
    .local v1, "scrollX":F
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget v3, v3, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusY:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget v4, v4, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusY:F

    sub-float v2, v3, v4

    .line 654
    .local v2, "scrollY":F
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    neg-float v4, v1

    neg-float v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 655
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTranslate:Z

    .line 656
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    if-eqz v3, :cond_0

    .line 658
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    .line 660
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    goto :goto_0

    .line 663
    .end local v1    # "scrollX":F
    .end local v2    # "scrollY":F
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->set(Landroid/view/MotionEvent;)V

    .line 665
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iput v4, v3, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusX:F

    .line 666
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iput v4, v3, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusY:F

    .line 668
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsMultiTouchStart:Z

    .line 670
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->reset()V

    goto :goto_0

    .line 640
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public configurationChange()V
    .locals 14

    .prologue
    const/4 v10, 0x2

    const/high16 v13, 0x40000000    # 2.0f

    const/high16 v9, 0x3f000000    # 0.5f

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 80
    const/16 v8, 0x9

    new-array v7, v8, [F

    .line 81
    .local v7, "values":[F
    new-array v6, v10, [F

    .line 82
    .local v6, "targetPoint":[F
    new-array v0, v10, [F

    .line 83
    .local v0, "center":[F
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCenterFromOriginal:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    aput v8, v6, v11

    .line 84
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCenterFromOriginal:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    aput v8, v6, v12

    .line 85
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    if-eqz v8, :cond_0

    .line 87
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v8}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v9

    aput v8, v0, v11

    .line 88
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v8}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v9

    aput v8, v0, v12

    .line 89
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v8}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v5

    .line 90
    .local v5, "supMatrix":Landroid/graphics/Matrix;
    invoke-virtual {v5, v7}, Landroid/graphics/Matrix;->getValues([F)V

    .line 91
    aget v3, v7, v11

    .line 92
    .local v3, "scaleX":F
    const/4 v8, 0x4

    aget v4, v7, v8

    .line 108
    .local v4, "scaleY":F
    invoke-virtual {v5}, Landroid/graphics/Matrix;->reset()V

    .line 109
    invoke-virtual {v5, v3, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 110
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v8}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewWidth()I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v9}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v9, v3

    sub-float/2addr v8, v9

    div-float/2addr v8, v13

    .line 111
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v9}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewHeight()I

    move-result v9

    int-to-float v9, v9

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v10}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    mul-float/2addr v10, v4

    sub-float/2addr v9, v10

    div-float/2addr v9, v13

    .line 110
    invoke-virtual {v5, v8, v9}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 112
    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 113
    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 115
    aget v8, v0, v11

    aget v9, v6, v11

    sub-float v1, v8, v9

    .line 116
    .local v1, "dx":F
    aget v8, v0, v12

    aget v9, v6, v12

    sub-float v2, v8, v9

    .line 117
    .local v2, "dy":F
    invoke-virtual {v5, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 118
    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->updateMatrix(Landroid/graphics/Matrix;)V

    .line 120
    .end local v1    # "dx":F
    .end local v2    # "dy":F
    .end local v3    # "scaleX":F
    .end local v4    # "scaleY":F
    .end local v5    # "supMatrix":Landroid/graphics/Matrix;
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    .line 61
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .line 62
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mSavedMatrix:Landroid/graphics/Matrix;

    .line 64
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 69
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    .line 70
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 75
    :cond_1
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    .line 76
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCenterFromOriginal:Landroid/graphics/PointF;

    .line 77
    return-void
.end method

.method public endZoom()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 303
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsLeftMax:Z

    .line 304
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsRightMax:Z

    .line 305
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTopMax:Z

    .line 306
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsBottomMax:Z

    .line 307
    return-void
.end method

.method public getSubMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public init(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;)V
    .locals 2
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .param p2, "touch"    # Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .prologue
    const/4 v1, 0x0

    .line 43
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .line 44
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mListener:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsMultiTouchStart:Z

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsMultiTouchEnd:Z

    .line 48
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    .line 49
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    .line 51
    instance-of v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 53
    check-cast p1, Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .end local p1    # "imageData":Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->updateMatrix()V

    .line 56
    return-void
.end method

.method public isBottomMax()Z
    .locals 1

    .prologue
    .line 364
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsBottomMax:Z

    return v0
.end method

.method public isLeftMax()Z
    .locals 1

    .prologue
    .line 343
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsLeftMax:Z

    return v0
.end method

.method public isRightMax()Z
    .locals 1

    .prologue
    .line 350
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsRightMax:Z

    return v0
.end method

.method public isTopMax()Z
    .locals 1

    .prologue
    .line 357
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTopMax:Z

    return v0
.end method

.method public isZoomMinRatio()Z
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 262
    const/4 v0, 0x0

    .line 263
    .local v0, "ret":Z
    const/16 v2, 0x9

    new-array v1, v2, [F

    .line 264
    .local v1, "values":[F
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 265
    const/4 v2, 0x0

    aget v2, v1, v2

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    const/4 v2, 0x4

    aget v2, v1, v2

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 266
    const/4 v0, 0x1

    .line 267
    :cond_0
    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const/4 v9, 0x0

    const/4 v5, 0x1

    .line 123
    const/4 v6, 0x1

    .line 124
    .local v6, "ret":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 125
    .local v0, "action":I
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    if-eqz v10, :cond_0

    .line 126
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v10, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 136
    :cond_0
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    if-eqz v10, :cond_2

    .line 138
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTranslate:Z

    .line 139
    iget-boolean v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsMultiTouchStart:Z

    if-nez v10, :cond_6

    .line 140
    const/4 v10, 0x5

    if-eq v0, v10, :cond_1

    const/16 v10, 0x105

    if-ne v0, v10, :cond_3

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v10

    const/4 v11, 0x2

    if-lt v10, v11, :cond_3

    .line 142
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->reset()V

    .line 144
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 146
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->set(Landroid/view/MotionEvent;)V

    .line 148
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsMultiTouchStart:Z

    .line 149
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsMultiTouchEnd:Z

    .line 256
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->updateMatrix()V

    .line 258
    return v6

    .line 154
    :cond_3
    iget-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsMultiTouchEnd:Z

    if-nez v9, :cond_4

    .line 156
    if-ne v0, v5, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v9

    if-ne v9, v5, :cond_2

    .line 158
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsMultiTouchEnd:Z

    goto :goto_0

    .line 163
    :cond_4
    iget-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mWithSingleTouch:Z

    if-eqz v9, :cond_5

    .line 165
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mListener:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    if-eqz v9, :cond_2

    .line 167
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mListener:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    invoke-interface {v9, p1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;->onSingleTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 172
    :cond_5
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->singleTouchScroll(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 178
    :cond_6
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 217
    :sswitch_0
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->set(Landroid/view/MotionEvent;)V

    .line 219
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v10, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v10

    float-to-int v10, v10

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v11, v11, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v11, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v11

    float-to-int v11, v11

    sub-int v1, v10, v11

    .line 220
    .local v1, "deltaX0":I
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v10, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v10

    float-to-int v10, v10

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v11, v11, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v11, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v9

    float-to-int v9, v9

    sub-int v3, v10, v9

    .line 222
    .local v3, "deltaY0":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v9, v9, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v9, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v9

    float-to-int v9, v9

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v10, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v10

    float-to-int v10, v10

    sub-int v2, v9, v10

    .line 223
    .local v2, "deltaX1":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v9, v9, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v9, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v9

    float-to-int v9, v9

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v10, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v10

    float-to-int v10, v10

    sub-int v4, v9, v10

    .line 227
    .local v4, "deltaY1":I
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v9

    int-to-float v9, v9

    cmpg-float v9, v9, v12

    if-gez v9, :cond_7

    .line 228
    const/4 v1, 0x0

    .line 229
    :cond_7
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v9

    int-to-float v9, v9

    cmpg-float v9, v9, v12

    if-gez v9, :cond_8

    .line 230
    const/4 v3, 0x0

    .line 231
    :cond_8
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v9

    int-to-float v9, v9

    cmpg-float v9, v9, v12

    if-gez v9, :cond_9

    .line 232
    const/4 v2, 0x0

    .line 233
    :cond_9
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v9

    int-to-float v9, v9

    cmpg-float v9, v9, v12

    if-gez v9, :cond_a

    .line 234
    const/4 v4, 0x0

    .line 237
    :cond_a
    mul-int v9, v1, v2

    if-gtz v9, :cond_b

    mul-int v9, v3, v4

    if-lez v9, :cond_d

    .line 238
    :cond_b
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget v9, v9, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusX:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget v10, v10, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusX:F

    sub-float v7, v9, v10

    .line 239
    .local v7, "scrollX":F
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget v9, v9, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusY:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget v10, v10, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusY:F

    sub-float v8, v9, v10

    .line 241
    .local v8, "scrollY":F
    iget-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mWithSingleTouch:Z

    if-eqz v9, :cond_d

    .line 243
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    if-eqz v9, :cond_c

    .line 244
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    neg-float v10, v7

    neg-float v11, v8

    invoke-virtual {v9, v10, v11}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 245
    :cond_c
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTranslate:Z

    .line 249
    .end local v7    # "scrollX":F
    .end local v8    # "scrollY":F
    :cond_d
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    iget-object v9, v9, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v9}, Landroid/view/MotionEvent;->recycle()V

    .line 250
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mPrev:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v10

    iput-object v10, v9, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    goto/16 :goto_0

    .line 182
    .end local v1    # "deltaX0":I
    .end local v2    # "deltaX1":I
    .end local v3    # "deltaY0":I
    .end local v4    # "deltaY1":I
    :sswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->set(Landroid/view/MotionEvent;)V

    .line 185
    const v10, 0xff00

    and-int/2addr v10, v0

    shr-int/lit8 v10, v10, 0x8

    if-nez v10, :cond_e

    .line 187
    .local v5, "id":I
    :goto_1
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v11

    iput v11, v10, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusX:F

    .line 188
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mCurr:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v11

    iput v11, v10, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MultiTouchInfo;->focusY:F

    .line 190
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsMultiTouchStart:Z

    .line 192
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->reset()V

    goto/16 :goto_0

    .end local v5    # "id":I
    :cond_e
    move v5, v9

    .line 185
    goto :goto_1

    .line 204
    :sswitch_2
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsMultiTouchStart:Z

    .line 206
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->reset()V

    goto/16 :goto_0

    .line 178
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_2
        0x6 -> :sswitch_1
        0x106 -> :sswitch_1
    .end sparse-switch
.end method

.method public resetZoom()V
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->updateMatrix()V

    .line 276
    :cond_0
    return-void
.end method

.method public setScroll(FF)V
    .locals 3
    .param p1, "scrollX"    # F
    .param p2, "scrollY"    # F

    .prologue
    .line 634
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    neg-float v1, p1

    neg-float v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 635
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->updateMatrix()V

    .line 636
    return-void
.end method

.method public setZoom(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/high16 v1, 0x41200000    # 10.0f

    .line 279
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1, v1, p1, p2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->updateMatrix()V

    .line 281
    return-void
.end method

.method public setZoom(FFF)V
    .locals 5
    .param p1, "zoomratio"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    const/high16 v4, 0x41200000    # 10.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 285
    const/16 v1, 0x9

    new-array v0, v1, [F

    .line 286
    .local v0, "value":[F
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 288
    aget v1, v0, v2

    mul-float/2addr v1, p1

    cmpg-float v1, v1, v3

    if-gez v1, :cond_1

    .line 290
    aget v1, v0, v2

    div-float p1, v3, v1

    .line 298
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, p1, p1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 299
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->updateMatrix()V

    .line 300
    return-void

    .line 292
    :cond_1
    aget v1, v0, v2

    mul-float/2addr v1, p1

    cmpl-float v1, v1, v4

    if-lez v1, :cond_0

    .line 294
    aget v1, v0, v2

    div-float p1, v4, v1

    goto :goto_0
.end method

.method public updateMatrix()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 314
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->matrixTurning(Landroid/graphics/Matrix;)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->updatePinchZoomMatrix(Landroid/graphics/Matrix;)V

    .line 319
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsPinchZoom:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTranslate:Z

    if-eqz v0, :cond_3

    .line 321
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mListener:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    if-eqz v0, :cond_2

    .line 322
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mListener:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;->onPinchZoom()V

    .line 323
    :cond_2
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsPinchZoom:Z

    .line 324
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTranslate:Z

    .line 326
    :cond_3
    return-void
.end method

.method public updateMatrix(Landroid/graphics/Matrix;)V
    .locals 2
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    const/4 v1, 0x0

    .line 329
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->matrixTurning(Landroid/graphics/Matrix;)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->updatePinchZoomMatrix(Landroid/graphics/Matrix;)V

    .line 332
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsPinchZoom:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTranslate:Z

    if-eqz v0, :cond_2

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mListener:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    if-eqz v0, :cond_1

    .line 335
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mListener:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;->onPinchZoom()V

    .line 336
    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsPinchZoom:Z

    .line 337
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mIsTranslate:Z

    .line 339
    :cond_2
    return-void
.end method
