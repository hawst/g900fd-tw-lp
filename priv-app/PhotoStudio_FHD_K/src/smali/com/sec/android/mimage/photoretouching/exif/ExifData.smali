.class Lcom/sec/android/mimage/photoretouching/exif/ExifData;
.super Ljava/lang/Object;
.source "ExifData.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ExifData"

.field private static final USER_COMMENT_ASCII:[B

.field private static final USER_COMMENT_JIS:[B

.field private static final USER_COMMENT_UNICODE:[B


# instance fields
.field private final mByteOrder:Ljava/nio/ByteOrder;

.field private final mIfdDatas:[Lcom/sec/android/mimage/photoretouching/exif/IfdData;

.field private mStripBytes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field

.field private mThumbnail:[B


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v2, 0x49

    .line 20
    new-array v0, v6, [B

    .line 21
    const/16 v1, 0x41

    aput-byte v1, v0, v3

    const/16 v1, 0x53

    aput-byte v1, v0, v4

    const/16 v1, 0x43

    aput-byte v1, v0, v5

    const/4 v1, 0x3

    aput-byte v2, v0, v1

    const/4 v1, 0x4

    aput-byte v2, v0, v1

    .line 20
    sput-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->USER_COMMENT_ASCII:[B

    .line 23
    new-array v0, v6, [B

    .line 24
    const/16 v1, 0x4a

    aput-byte v1, v0, v3

    aput-byte v2, v0, v4

    const/16 v1, 0x53

    aput-byte v1, v0, v5

    .line 23
    sput-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->USER_COMMENT_JIS:[B

    .line 26
    new-array v0, v6, [B

    .line 27
    const/16 v1, 0x55

    aput-byte v1, v0, v3

    const/16 v1, 0x4e

    aput-byte v1, v0, v4

    aput-byte v2, v0, v5

    const/4 v1, 0x3

    const/16 v2, 0x43

    aput-byte v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x4f

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0x44

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x45

    aput-byte v2, v0, v1

    .line 26
    sput-object v0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->USER_COMMENT_UNICODE:[B

    .line 28
    return-void
.end method

.method constructor <init>(Ljava/nio/ByteOrder;)V
    .locals 1
    .param p1, "order"    # Ljava/nio/ByteOrder;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mIfdDatas:[Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    .line 36
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mByteOrder:Ljava/nio/ByteOrder;

    .line 37
    return-void
.end method


# virtual methods
.method protected addIfdData(Lcom/sec/android/mimage/photoretouching/exif/IfdData;)V
    .locals 2
    .param p1, "data"    # Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mIfdDatas:[Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getId()I

    move-result v1

    aput-object p1, v0, v1

    .line 124
    return-void
.end method

.method protected addTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .locals 2
    .param p1, "tag"    # Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .prologue
    .line 153
    if-eqz p1, :cond_0

    .line 154
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getIfd()I

    move-result v0

    .line 155
    .local v0, "ifd":I
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->addTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;I)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v1

    .line 157
    .end local v0    # "ifd":I
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected addTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;I)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .locals 2
    .param p1, "tag"    # Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .param p2, "ifdId"    # I

    .prologue
    .line 165
    if-eqz p1, :cond_0

    invoke-static {p2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->isValidIfd(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getOrCreateIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v0

    .line 167
    .local v0, "ifdData":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v1

    .line 169
    .end local v0    # "ifdData":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected clearThumbnailAndStrips()V
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mThumbnail:[B

    .line 174
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 175
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 302
    if-ne p0, p1, :cond_0

    move v4, v6

    .line 329
    :goto_0
    return v4

    .line 305
    :cond_0
    if-nez p1, :cond_1

    move v4, v7

    .line 306
    goto :goto_0

    .line 308
    :cond_1
    instance-of v4, p1, Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    if-eqz v4, :cond_8

    move-object v0, p1

    .line 309
    check-cast v0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    .line 310
    .local v0, "data":Lcom/sec/android/mimage/photoretouching/exif/ExifData;
    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mByteOrder:Ljava/nio/ByteOrder;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mByteOrder:Ljava/nio/ByteOrder;

    if-ne v4, v5, :cond_2

    .line 311
    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v4, v5, :cond_2

    .line 312
    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mThumbnail:[B

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mThumbnail:[B

    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-nez v4, :cond_3

    :cond_2
    move v4, v7

    .line 313
    goto :goto_0

    .line 315
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_4

    .line 320
    const/4 v1, 0x0

    :goto_2
    const/4 v4, 0x5

    if-lt v1, v4, :cond_6

    move v4, v6

    .line 327
    goto :goto_0

    .line 316
    :cond_4
    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-nez v4, :cond_5

    move v4, v7

    .line 317
    goto :goto_0

    .line 315
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 321
    :cond_6
    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v2

    .line 322
    .local v2, "ifd1":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v3

    .line 323
    .local v3, "ifd2":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-eq v2, v3, :cond_7

    if-eqz v2, :cond_7

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v4, v7

    .line 324
    goto :goto_0

    .line 320
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v0    # "data":Lcom/sec/android/mimage/photoretouching/exif/ExifData;
    .end local v1    # "i":I
    .end local v2    # "ifd1":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    .end local v3    # "ifd2":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    :cond_8
    move v4, v7

    .line 329
    goto :goto_0
.end method

.method protected getAllTags()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/mimage/photoretouching/exif/ExifTag;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 240
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 241
    .local v1, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mIfdDatas:[Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    array-length v8, v7

    move v6, v5

    :goto_0
    if-lt v6, v8, :cond_1

    .line 251
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_0

    .line 252
    const/4 v1, 0x0

    .line 254
    .end local v1    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    :cond_0
    return-object v1

    .line 241
    .restart local v1    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    :cond_1
    aget-object v0, v7, v6

    .line 242
    .local v0, "d":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-eqz v0, :cond_2

    .line 243
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getAllTags()[Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v3

    .line 244
    .local v3, "tags":[Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-eqz v3, :cond_2

    .line 245
    array-length v9, v3

    move v4, v5

    :goto_1
    if-lt v4, v9, :cond_3

    .line 241
    .end local v3    # "tags":[Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :cond_2
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .line 245
    .restart local v3    # "tags":[Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :cond_3
    aget-object v2, v3, v4

    .line 246
    .local v2, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method protected getAllTagsForIfd(I)Ljava/util/List;
    .locals 7
    .param p1, "ifd"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/mimage/photoretouching/exif/ExifTag;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 262
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mIfdDatas:[Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    aget-object v0, v5, p1

    .line 263
    .local v0, "d":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-nez v0, :cond_1

    move-object v1, v4

    .line 277
    :cond_0
    :goto_0
    return-object v1

    .line 266
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getAllTags()[Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v3

    .line 267
    .local v3, "tags":[Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v3, :cond_2

    move-object v1, v4

    .line 268
    goto :goto_0

    .line 270
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    array-length v5, v3

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 271
    .local v1, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    array-length v6, v3

    const/4 v5, 0x0

    :goto_1
    if-lt v5, v6, :cond_3

    .line 274
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_0

    move-object v1, v4

    .line 275
    goto :goto_0

    .line 271
    :cond_3
    aget-object v2, v3, v5

    .line 272
    .local v2, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method protected getAllTagsForTagId(S)Ljava/util/List;
    .locals 6
    .param p1, "tag"    # S
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/mimage/photoretouching/exif/ExifTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 286
    .local v1, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mIfdDatas:[Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_1

    .line 294
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 295
    const/4 v1, 0x0

    .line 297
    .end local v1    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    :cond_0
    return-object v1

    .line 286
    .restart local v1    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    :cond_1
    aget-object v0, v4, v3

    .line 287
    .local v0, "d":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-eqz v0, :cond_2

    .line 288
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getTag(S)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v2

    .line 289
    .local v2, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-eqz v2, :cond_2

    .line 290
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 286
    .end local v2    # "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method protected getByteOrder()Ljava/nio/ByteOrder;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mByteOrder:Ljava/nio/ByteOrder;

    return-object v0
.end method

.method protected getCompressedThumbnail()[B
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mThumbnail:[B

    return-object v0
.end method

.method protected getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    .locals 1
    .param p1, "ifdId"    # I

    .prologue
    .line 112
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->isValidIfd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mIfdDatas:[Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    aget-object v0, v0, p1

    .line 115
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getOrCreateIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    .locals 2
    .param p1, "ifdId"    # I

    .prologue
    .line 131
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mIfdDatas:[Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    aget-object v0, v1, p1

    .line 132
    .local v0, "ifdData":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-nez v0, :cond_0

    .line 133
    new-instance v0, Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    .end local v0    # "ifdData":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    invoke-direct {v0, p1}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;-><init>(I)V

    .line 134
    .restart local v0    # "ifdData":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mIfdDatas:[Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    aput-object v0, v1, p1

    .line 136
    :cond_0
    return-object v0
.end method

.method protected getStrip(I)[B
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method protected getStripCount()I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method protected getTag(SI)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .locals 2
    .param p1, "tag"    # S
    .param p2, "ifd"    # I

    .prologue
    .line 144
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mIfdDatas:[Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    aget-object v0, v1, p2

    .line 145
    .local v0, "ifdData":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getTag(S)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v1

    goto :goto_0
.end method

.method protected getUserComment()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x8

    .line 201
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mIfdDatas:[Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    aget-object v3, v6, v8

    .line 202
    .local v3, "ifdData":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-nez v3, :cond_1

    .line 231
    :cond_0
    :goto_0
    return-object v5

    .line 205
    :cond_1
    sget v6, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_USER_COMMENT:I

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v6

    invoke-virtual {v3, v6}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getTag(S)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v4

    .line 206
    .local v4, "tag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-eqz v4, :cond_0

    .line 209
    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v6

    if-lt v6, v7, :cond_0

    .line 213
    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v6

    new-array v0, v6, [B

    .line 214
    .local v0, "buf":[B
    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getBytes([B)V

    .line 216
    new-array v1, v7, [B

    .line 217
    .local v1, "code":[B
    invoke-static {v0, v8, v1, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 220
    :try_start_0
    sget-object v6, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->USER_COMMENT_ASCII:[B

    invoke-static {v1, v6}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 221
    new-instance v6, Ljava/lang/String;

    const/16 v7, 0x8

    array-length v8, v0

    add-int/lit8 v8, v8, -0x8

    const-string v9, "US-ASCII"

    invoke-direct {v6, v0, v7, v8, v9}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    move-object v5, v6

    goto :goto_0

    .line 222
    :cond_2
    sget-object v6, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->USER_COMMENT_JIS:[B

    invoke-static {v1, v6}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 223
    new-instance v6, Ljava/lang/String;

    const/16 v7, 0x8

    array-length v8, v0

    add-int/lit8 v8, v8, -0x8

    const-string v9, "EUC-JP"

    invoke-direct {v6, v0, v7, v8, v9}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    move-object v5, v6

    goto :goto_0

    .line 224
    :cond_3
    sget-object v6, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->USER_COMMENT_UNICODE:[B

    invoke-static {v1, v6}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 225
    new-instance v6, Ljava/lang/String;

    const/16 v7, 0x8

    array-length v8, v0

    add-int/lit8 v8, v8, -0x8

    const-string v9, "UTF-16"

    invoke-direct {v6, v0, v7, v8, v9}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v5, v6

    goto :goto_0

    .line 229
    :catch_0
    move-exception v2

    .line 230
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    const-string v6, "ExifData"

    const-string v7, "Failed to decode the user comment"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected hasCompressedThumbnail()Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mThumbnail:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected hasUncompressedStrip()Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected removeTag(SI)V
    .locals 2
    .param p1, "tagId"    # S
    .param p2, "ifdId"    # I

    .prologue
    .line 189
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mIfdDatas:[Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    aget-object v0, v1, p2

    .line 190
    .local v0, "ifdData":Lcom/sec/android/mimage/photoretouching/exif/IfdData;
    if-nez v0, :cond_0

    .line 194
    :goto_0
    return-void

    .line 193
    :cond_0
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->removeTag(S)V

    goto :goto_0
.end method

.method protected removeThumbnailData()V
    .locals 3

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->clearThumbnailAndStrips()V

    .line 182
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mIfdDatas:[Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 183
    return-void
.end method

.method protected setCompressedThumbnail([B)V
    .locals 0
    .param p1, "thumbnail"    # [B

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mThumbnail:[B

    .line 54
    return-void
.end method

.method protected setStripBytes(I[B)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "strip"    # [B

    .prologue
    .line 67
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 68
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    invoke-virtual {v1, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 75
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .local v0, "i":I
    :goto_1
    if-lt v0, p1, :cond_1

    .line 73
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 71
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->mStripBytes:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
