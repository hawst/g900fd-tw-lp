.class public Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "NormalView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$DoDoneAsyncTask;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ResolverSetAsAdapter;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ResolverShareAdapter;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$TrayDeleteFunction;
    }
.end annotation


# instance fields
.field public final FINISH_PROGRESS:I

.field private final REDO:I

.field private final REDOALL:I

.field public final SHOW_PROGRESS:I

.field public final SHOW_SAVE_TOAST:I

.field public final TOP_BOTTOM_SHOW_HIDE_AVAILABLE_DIFF:I

.field private final UNDO:I

.field private final UNDOALL:I

.field private isReverseAnimRunning:Z

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;

.field private mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

.field private mAlpha:I

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mClearPaint:Landroid/graphics/Paint;

.field private mColor:I

.field private mContext:Landroid/content/Context;

.field private mCurrentSaveSize:I

.field private mCurrentTime:J

.field private mDashPathEffectPaint:Landroid/graphics/Paint;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mDrawAnimRunnable:Ljava/lang/Runnable;

.field private mDrawOriginal:Z

.field private mFileName:Ljava/lang/String;

.field private mFirstPt:Landroid/graphics/Point;

.field private mGesture:Landroid/view/GestureDetector;

.field private mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

.field private mHandler:Landroid/os/Handler;

.field private mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mInitializeThread:Ljava/lang/Thread;

.field private mIsBackPressed:Z

.field private mIsDestory:Z

.field private mIsMinimum:Z

.field private mIsMultiTouch:Z

.field private mIsReadyForDraw:Z

.field private final mKey:Ljava/lang/String;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mLongpressButton:Z

.field private mMiddleBtn:Landroid/widget/Button;

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mOptionItemId:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

.field private mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mPushSaveButton:Z

.field private mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

.field public mShouldReverseAnimate:Z

.field private mShowTopBottomButtons:Z

.field private mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

.field private mTrayDeleteFunction:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$TrayDeleteFunction;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;

.field private msave:Z

.field private msetas:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 270
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mInitializeThread:Ljava/lang/Thread;

    .line 3726
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mFirstPt:Landroid/graphics/Point;

    .line 3727
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    .line 3728
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 3729
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 3730
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 3731
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 3732
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 3733
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 3735
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    .line 3736
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 3737
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPaint:Landroid/graphics/Paint;

    .line 3738
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mClearPaint:Landroid/graphics/Paint;

    .line 3739
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mLinePaint:Landroid/graphics/Paint;

    .line 3740
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 3741
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 3742
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 3743
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mGesture:Landroid/view/GestureDetector;

    .line 3744
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;

    .line 3746
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 3748
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayDeleteFunction:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$TrayDeleteFunction;

    .line 3749
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 3751
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    .line 3753
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mFileName:Ljava/lang/String;

    .line 3754
    const-string v4, "Normal"

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mKey:Ljava/lang/String;

    .line 3755
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsBackPressed:Z

    .line 3756
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMinimum:Z

    .line 3757
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMultiTouch:Z

    .line 3758
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPushSaveButton:Z

    .line 3759
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mShowTopBottomButtons:Z

    .line 3760
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsDestory:Z

    .line 3761
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mOptionItemId:I

    .line 3763
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mColor:I

    .line 3765
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->UNDO:I

    const/4 v4, 0x1

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->REDO:I

    const/4 v4, 0x2

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->UNDOALL:I

    const/4 v4, 0x3

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->REDOALL:I

    .line 3766
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 3767
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->SHOW_PROGRESS:I

    .line 3768
    const/4 v4, 0x1

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->FINISH_PROGRESS:I

    .line 3769
    const/4 v4, 0x2

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->SHOW_SAVE_TOAST:I

    .line 3770
    const/16 v4, 0xa

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->TOP_BOTTOM_SHOW_HIDE_AVAILABLE_DIFF:I

    .line 3772
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 3774
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mCheckBox:Landroid/widget/CheckBox;

    .line 3775
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    .line 3777
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mCurrentSaveSize:I

    .line 3779
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawOriginal:Z

    .line 3780
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mLongpressButton:Z

    .line 3782
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsReadyForDraw:Z

    .line 3783
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mCurrentTime:J

    .line 3784
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->isReverseAnimRunning:Z

    .line 3786
    const/16 v4, 0x32

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAlpha:I

    .line 3787
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mShouldReverseAnimate:Z

    .line 3788
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$1;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    .line 3796
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->msetas:Z

    .line 3797
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->msave:Z

    .line 3799
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 106
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHandler:Landroid/os/Handler;

    .line 108
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    .line 109
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 110
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 111
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 112
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 114
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->setInterface(Ljava/lang/Object;)V

    .line 116
    new-instance v4, Landroid/graphics/Paint;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPaint:Landroid/graphics/Paint;

    .line 117
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPaint:Landroid/graphics/Paint;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 119
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mClearPaint:Landroid/graphics/Paint;

    .line 120
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mClearPaint:Landroid/graphics/Paint;

    new-instance v5, Landroid/graphics/PorterDuffXfermode;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v5, v6}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 122
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mLinePaint:Landroid/graphics/Paint;

    .line 123
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 124
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 126
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 127
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 128
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 129
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    new-instance v5, Landroid/graphics/DashPathEffect;

    const/4 v6, 0x4

    new-array v6, v6, [F

    fill-array-data v6, :array_0

    const/high16 v7, 0x40000000    # 2.0f

    invoke-direct {v5, v6, v7}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 131
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;-><init>(Landroid/content/Context;Z)V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 133
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v4, :cond_0

    .line 135
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 136
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 138
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initGesture()V

    .line 140
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    const/high16 v5, 0x10000000

    new-instance v6, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$2;

    invoke-direct {v6, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-direct {v4, p1, v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;-><init>(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;)V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 148
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$3;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 187
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v4, :cond_3

    .line 190
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 193
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 195
    const/4 v3, 0x0

    .line 196
    .local v3, "previewBuffer":[I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v4, :cond_1

    .line 197
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->init(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;)V

    .line 199
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 200
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v3

    .line 203
    :goto_0
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 205
    .local v2, "canvas":Landroid/graphics/Canvas;
    const/4 v4, 0x0

    .line 206
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    .line 207
    const/4 v6, 0x0

    .line 208
    const/4 v7, 0x0

    .line 209
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v8

    .line 210
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v9

    .line 211
    const/4 v10, 0x1

    .line 212
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPaint:Landroid/graphics/Paint;

    .line 204
    invoke-virtual/range {v2 .. v11}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 263
    .end local v2    # "canvas":Landroid/graphics/Canvas;
    .end local v3    # "previewBuffer":[I
    :goto_1
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;

    .line 264
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mFirstPt:Landroid/graphics/Point;

    .line 268
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->init(I)V

    .line 269
    return-void

    .line 202
    .restart local v3    # "previewBuffer":[I
    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto :goto_0

    .line 215
    .end local v3    # "previewBuffer":[I
    :catch_0
    move-exception v12

    .line 217
    .local v12, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v12}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 223
    .end local v12    # "e":Ljava/lang/NullPointerException;
    :cond_3
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$4;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mInitializeThread:Ljava/lang/Thread;

    .line 260
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mInitializeThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_1

    .line 129
    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    .locals 1

    .prologue
    .line 3741
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Z)V
    .locals 0

    .prologue
    .line 3759
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mShowTopBottomButtons:Z

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 3729
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 3728
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 3736
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)I
    .locals 1

    .prologue
    .line 3777
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mCurrentSaveSize:I

    return v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 3766
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 3772
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 3772
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$TrayDeleteFunction;
    .locals 1

    .prologue
    .line 3748
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayDeleteFunction:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$TrayDeleteFunction;

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$TrayDeleteFunction;)V
    .locals 0

    .prologue
    .line 3748
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayDeleteFunction:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$TrayDeleteFunction;

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)I
    .locals 1

    .prologue
    .line 3761
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mOptionItemId:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Z
    .locals 1

    .prologue
    .line 3779
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawOriginal:Z

    return v0
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;I)Z
    .locals 1

    .prologue
    .line 2909
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->runOptionItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;)V
    .locals 0

    .prologue
    .line 3751
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)I
    .locals 1

    .prologue
    .line 3763
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mColor:I

    return v0
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;I)V
    .locals 0

    .prologue
    .line 3763
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mColor:I

    return-void
.end method

.method static synthetic access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 3785
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 3730
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$26(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Z
    .locals 1

    .prologue
    .line 3760
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsDestory:Z

    return v0
.end method

.method static synthetic access$27(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;I)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->setViewLayerType(I)V

    return-void
.end method

.method static synthetic access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 3746
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$29(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Z
    .locals 1

    .prologue
    .line 3756
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMinimum:Z

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 3731
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$30(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)J
    .locals 2

    .prologue
    .line 3783
    iget-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mCurrentTime:J

    return-wide v0
.end method

.method static synthetic access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;)V
    .locals 0

    .prologue
    .line 3775
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    return-void
.end method

.method static synthetic access$32(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;
    .locals 1

    .prologue
    .line 3775
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    return-object v0
.end method

.method static synthetic access$33(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 3746
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$34(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 3737
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$35(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;J)V
    .locals 1

    .prologue
    .line 3783
    iput-wide p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mCurrentTime:J

    return-void
.end method

.method static synthetic access$36(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->getImageEditViewWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$37(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->getImageEditViewHeight()I

    move-result v0

    return v0
.end method

.method static synthetic access$38(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V
    .locals 0

    .prologue
    .line 3685
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->refreshImageAndBottomButtons()V

    return-void
.end method

.method static synthetic access$39(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Z)V
    .locals 0

    .prologue
    .line 3780
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mLongpressButton:Z

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 3726
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mFirstPt:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$40(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Z
    .locals 1

    .prologue
    .line 3780
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mLongpressButton:Z

    return v0
.end method

.method static synthetic access$41(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Landroid/content/Intent;Z)V
    .locals 0

    .prologue
    .line 1641
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    return-void
.end method

.method static synthetic access$42(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Z)V
    .locals 0

    .prologue
    .line 3758
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPushSaveButton:Z

    return-void
.end method

.method static synthetic access$43(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;I)V
    .locals 0

    .prologue
    .line 3761
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mOptionItemId:I

    return-void
.end method

.method static synthetic access$44(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;I)V
    .locals 0

    .prologue
    .line 3777
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mCurrentSaveSize:I

    return-void
.end method

.method static synthetic access$45(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;
    .locals 1

    .prologue
    .line 3751
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    return-object v0
.end method

.method static synthetic access$46(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V
    .locals 0

    .prologue
    .line 3733
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    return-void
.end method

.method static synthetic access$47(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Z)V
    .locals 0

    .prologue
    .line 3779
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawOriginal:Z

    return-void
.end method

.method static synthetic access$48(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;
    .locals 1

    .prologue
    .line 3799
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Z
    .locals 1

    .prologue
    .line 3759
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mShowTopBottomButtons:Z

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;
    .locals 1

    .prologue
    .line 3744
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .locals 1

    .prologue
    .line 3732
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 3766
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 3727
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private checkHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 3126
    const/4 v0, 0x0

    .line 3127
    .local v0, "isNoShow":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->isDoNotShowPopup(Landroid/content/Context;I)Z

    move-result v0

    .line 3129
    if-nez v0, :cond_0

    .line 3131
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-nez v1, :cond_0

    .line 3133
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initHelpPopup(I)V

    .line 3134
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->showCurrentModeHelpPopup()V

    .line 3137
    :cond_0
    return-void
.end method

.method private initCancelDialog()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1910
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 1912
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1913
    const/16 v2, 0xa

    .line 1914
    const v3, 0x7f0601d7

    .line 1915
    const/4 v4, 0x1

    .line 1917
    const v6, 0x103012e

    .line 1912
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1918
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1919
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x500

    .line 1920
    const v2, 0x7f06009c

    .line 1922
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$26;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$26;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 1919
    invoke-virtual {v0, v1, v2, v5, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1929
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601b2

    .line 1930
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$27;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$27;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 1929
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1939
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1940
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$28;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$28;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 1939
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1948
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1950
    :cond_0
    return-void
.end method

.method private initDefaultSelectDialog()V
    .locals 13

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2597
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2598
    const/4 v2, 0x1

    .line 2599
    const v3, 0x7f0600db

    .line 2602
    const v6, 0x103012e

    .line 2597
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2603
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 2604
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v7, 0x110

    .line 2605
    const v8, 0x14001407

    .line 2607
    const v10, 0x7f06002d

    .line 2609
    new-instance v12, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$45;

    invoke-direct {v12, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$45;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    move v9, v4

    move-object v11, v5

    .line 2604
    invoke-virtual/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2622
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v7, 0x100

    .line 2623
    const v8, 0x1400140a

    .line 2625
    const v10, 0x7f060027

    .line 2627
    new-instance v12, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;

    invoke-direct {v12, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$46;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    move v9, v4

    move-object v11, v5

    .line 2622
    invoke-virtual/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2661
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v7, 0x120

    .line 2664
    const v10, 0x7f0600fc

    .line 2666
    new-instance v12, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$47;

    invoke-direct {v12, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$47;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    move v8, v4

    move v9, v4

    move-object v11, v5

    .line 2661
    invoke-virtual/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2679
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2680
    return-void
.end method

.method private initGesture()V
    .locals 4

    .prologue
    .line 3152
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$51;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$51;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 3233
    .local v0, "gesture":Landroid/view/GestureDetector$OnGestureListener;
    new-instance v1, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    .line 3234
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-direct {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;)V

    .line 3233
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 3235
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mGesture:Landroid/view/GestureDetector;

    .line 3236
    return-void
.end method

.method private initHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 3140
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 3141
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$50;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$50;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->setHelpPopupCallback(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;)V

    .line 3148
    return-void
.end method

.method private initMiddleButton()V
    .locals 13

    .prologue
    .line 661
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f060036

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 662
    .local v3, "text":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    check-cast v7, Landroid/app/Activity;

    const v8, 0x7f0900b6

    invoke-virtual {v7, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    .line 664
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f050232

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 665
    .local v6, "viewWitdh":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f050233

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 666
    .local v5, "viewHeight":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f050234

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 669
    .local v4, "textSize":I
    const-string v7, "sans-serif-light"

    const/4 v8, 0x0

    invoke-static {v7, v8}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 671
    .local v0, "font":Landroid/graphics/Typeface;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    if-eqz v7, :cond_0

    .line 673
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    const v8, 0x7f020325

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 674
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v7, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 675
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 676
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    const/4 v8, 0x0

    int-to-float v9, v4

    invoke-virtual {v7, v8, v9}, Landroid/widget/Button;->setTextSize(IF)V

    .line 677
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f040033

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    .line 678
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v7, v6}, Landroid/widget/Button;->setWidth(I)V

    .line 679
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v7, v5}, Landroid/widget/Button;->setHeight(I)V

    .line 680
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setEnabled(Z)V

    .line 681
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setPressed(Z)V

    .line 682
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v7}, Landroid/widget/Button;->setSingleLine()V

    .line 683
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 684
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setHorizontalFadingEdgeEnabled(Z)V

    .line 685
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 688
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 689
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v7, 0xb

    invoke-virtual {v2, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 690
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f05022e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    iput v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 691
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f05022f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    iput v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 692
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v7, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 693
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050231

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    const/4 v9, 0x0

    .line 694
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f050231

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v10, v10

    const/16 v11, 0x8

    .line 693
    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/Button;->setPadding(IIII)V

    .line 697
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    check-cast v7, Landroid/app/Activity;

    const v8, 0x7f0900b5

    invoke-virtual {v7, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 698
    .local v1, "middleButtonLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->invalidate()V

    .line 701
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mMiddleBtn:Landroid/widget/Button;

    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;

    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 781
    .end local v1    # "middleButtonLayout":Landroid/widget/RelativeLayout;
    .end local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method private initSaveDialog()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const v3, 0x7f06000d

    .line 1864
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1865
    const v2, 0x7f03000b

    .line 1867
    const/4 v4, 0x1

    .line 1869
    const v6, 0x103012e

    .line 1864
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1871
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1872
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x500

    .line 1875
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$23;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$23;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 1872
    invoke-virtual {v0, v1, v3, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1882
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000a

    .line 1883
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$24;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$24;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 1882
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1897
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1898
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$25;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$25;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 1897
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1905
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1906
    return-void
.end method

.method private initSaveOptionDialog()V
    .locals 11

    .prologue
    const v10, 0x7f0601f0

    const v9, 0x7f0601cb

    const/16 v2, 0x9

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 2165
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2219
    :goto_0
    return-void

    .line 2170
    :cond_0
    new-instance v7, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$35;

    invoke-direct {v7, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$35;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 2184
    .local v7, "saveListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2186
    const v3, 0x7f0601cd

    .line 2187
    const/4 v4, 0x1

    .line 2189
    const v6, 0x103012e

    .line 2184
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2191
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 2193
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x810

    move v2, v9

    move v3, v8

    move v4, v9

    move-object v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2197
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x820

    move v2, v10

    move v3, v8

    move v4, v10

    move-object v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2198
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 2199
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$36;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$36;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 2198
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2206
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    .line 2207
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$37;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$37;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 2206
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2218
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    goto :goto_0
.end method

.method private initSaveYesNoCancelForFinish()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 2525
    const/4 v7, 0x0

    .line 2526
    .local v7, "fileName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 2527
    .local v8, "path":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 2529
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2530
    if-nez v7, :cond_0

    .line 2532
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2536
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2537
    const/16 v2, 0x8

    .line 2538
    const v3, 0x7f06019d

    .line 2539
    const/4 v4, 0x1

    .line 2541
    const v6, 0x103012e

    .line 2536
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2543
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 2544
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x500

    .line 2547
    const v4, 0x7f060192

    move v2, v9

    move v3, v9

    move-object v6, v5

    .line 2544
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2551
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    .line 2552
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$42;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$42;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 2551
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnShowListener;)V

    .line 2574
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601b2

    .line 2575
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$43;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$43;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 2574
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2583
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 2584
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$44;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$44;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 2583
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2591
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2592
    return-void
.end method

.method private initSelectedDialog()V
    .locals 14

    .prologue
    const v8, 0x14001407

    const/16 v13, 0x100

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2337
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2338
    const/4 v2, 0x2

    .line 2339
    const v3, 0x7f0600db

    .line 2342
    const v6, 0x103012e

    .line 2337
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2344
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 2345
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v7, 0x110

    .line 2348
    const v10, 0x7f06002d

    .line 2350
    new-instance v12, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$38;

    invoke-direct {v12, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$38;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    move v9, v4

    move-object v11, v5

    .line 2345
    invoke-virtual/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2362
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2365
    const v10, 0x7f060026

    .line 2367
    new-instance v12, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$39;

    invoke-direct {v12, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$39;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    move v7, v13

    move v9, v4

    move-object v11, v5

    .line 2362
    invoke-virtual/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2393
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2394
    const v8, 0x1400140a

    .line 2396
    const v10, 0x7f0600de

    .line 2398
    new-instance v12, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$40;

    invoke-direct {v12, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$40;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    move v7, v13

    move v9, v4

    move-object v11, v5

    .line 2393
    invoke-virtual/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2409
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v7, 0x120

    .line 2412
    const v10, 0x7f0600fc

    .line 2414
    new-instance v12, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$41;

    invoke-direct {v12, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$41;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    move v8, v4

    move v9, v4

    move-object v11, v5

    .line 2409
    invoke-virtual/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2427
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2428
    return-void
.end method

.method private initSetAsDialog()V
    .locals 7

    .prologue
    .line 1798
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 1800
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1801
    const v2, 0x7f090158

    .line 1802
    const v3, 0x7f0600a3

    .line 1803
    const/4 v4, 0x0

    .line 1804
    const/4 v5, 0x0

    .line 1805
    const v6, 0x103012e

    .line 1800
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1806
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1808
    :cond_0
    return-void
.end method

.method private initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isShareVia"    # Z

    .prologue
    .line 1644
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1645
    .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    const/4 v2, 0x0

    .line 1646
    .local v2, "shareChooser":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 1647
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    const v4, 0x7f06000f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 1651
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Landroid/content/pm/LabeledIntent;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/pm/LabeledIntent;

    .line 1652
    .local v0, "extraIntents":[Landroid/content/pm/LabeledIntent;
    const-string v3, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1653
    const/high16 v3, 0x24000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1654
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1783
    return-void

    .line 1649
    .end local v0    # "extraIntents":[Landroid/content/pm/LabeledIntent;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    const v4, 0x7f0600a3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method private initShareViaDialog()V
    .locals 7

    .prologue
    .line 1786
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 1788
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1789
    const v2, 0x7f090157

    .line 1790
    const v3, 0x7f06000f

    .line 1791
    const/4 v4, 0x0

    .line 1792
    const/4 v5, 0x0

    .line 1793
    const v6, 0x103012e

    .line 1788
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1794
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1796
    :cond_0
    return-void
.end method

.method private initTrayDeleteQuestionDialog()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1811
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1812
    const/4 v2, 0x3

    .line 1813
    const v3, 0x7f0600d3

    .line 1814
    const/4 v4, 0x1

    .line 1816
    const v6, 0x103012e

    .line 1811
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1818
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1819
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x500

    .line 1820
    const v2, 0x7f06009c

    .line 1822
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$20;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$20;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 1819
    invoke-virtual {v0, v1, v2, v5, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1829
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000a

    .line 1830
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$21;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$21;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 1829
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1851
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1852
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$22;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$22;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 1851
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1860
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1861
    return-void
.end method

.method private initUndoRedoAllDialog()V
    .locals 8

    .prologue
    const v6, 0x103012e

    const/16 v1, 0x1000

    const/16 v7, 0x500

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2035
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2036
    const/4 v2, 0x4

    .line 2037
    const v3, 0x7f0600a6

    .line 2035
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2042
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 2043
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2044
    const v2, 0x7f06009c

    .line 2046
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$29;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$29;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 2043
    invoke-virtual {v0, v7, v2, v5, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2053
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v2, 0x7f060007

    .line 2054
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$30;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$30;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 2053
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2087
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v2, 0x7f060009

    .line 2088
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$31;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$31;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 2087
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2096
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2098
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2099
    const/4 v2, 0x5

    .line 2100
    const v3, 0x7f0600a1

    .line 2098
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2105
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 2106
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2107
    const v1, 0x7f0601ce

    .line 2109
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$32;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$32;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 2106
    invoke-virtual {v0, v7, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2116
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 2117
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$33;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$33;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 2116
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2151
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 2152
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$34;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$34;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 2151
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2160
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2161
    return-void
.end method

.method private refreshImageAndBottomButtons()V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/high16 v10, 0x42c80000    # 100.0f

    const/4 v2, 0x0

    .line 3687
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_0

    .line 3689
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 3690
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 3691
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 3692
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 3695
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 3698
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 3699
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 3701
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 3693
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 3703
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "output":[I
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_3

    .line 3705
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_2

    .line 3707
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-ltz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-gez v3, :cond_4

    .line 3708
    :cond_1
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMinimum:Z

    .line 3712
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$54;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$54;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3722
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->invalidateViews()V

    .line 3723
    return-void

    .line 3710
    :cond_4
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMinimum:Z

    goto :goto_0
.end method

.method private runOptionItem(I)Z
    .locals 7
    .param p1, "optionItemId"    # I

    .prologue
    const v6, 0x7f090158

    const v5, 0x7a1200

    const/4 v4, 0x0

    const/16 v3, 0x9

    .line 2911
    const/4 v0, 0x0

    .line 2912
    .local v0, "ret":Z
    const/4 v1, 0x0

    .line 2913
    .local v1, "uri":Landroid/net/Uri;
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mOptionItemId:I

    .line 2914
    sparse-switch p1, :sswitch_data_0

    .line 3011
    :cond_0
    :goto_0
    return v0

    .line 2931
    :sswitch_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v3, 0x2e000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 2932
    const/4 v0, 0x1

    .line 2933
    goto :goto_0

    .line 2949
    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2951
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mCurrentSaveSize:I

    .line 2952
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2953
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2965
    :cond_1
    :goto_1
    const/4 v0, 0x1

    .line 2966
    goto :goto_0

    .line 2955
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f090157

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2956
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 2957
    if-nez v1, :cond_3

    .line 2959
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2961
    :cond_3
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    goto :goto_1

    .line 2969
    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2971
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mCurrentSaveSize:I

    .line 2972
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2973
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2986
    :cond_4
    :goto_2
    const/4 v0, 0x1

    .line 2987
    goto/16 :goto_0

    .line 2975
    :cond_5
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2978
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 2979
    if-nez v1, :cond_6

    .line 2981
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2983
    :cond_6
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeSetAsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, v2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    .line 2984
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    goto :goto_2

    .line 2996
    :sswitch_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v2, :cond_7

    .line 2997
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->resetZoom()V

    .line 3000
    :cond_7
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->isShowing()Z

    move-result v2

    if-nez v2, :cond_8

    .line 3002
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->show()V

    .line 3003
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mShowTopBottomButtons:Z

    .line 3007
    :cond_8
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 3008
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v3, 0x17000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0

    .line 2914
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f090007 -> :sswitch_0
        0x7f090157 -> :sswitch_1
        0x7f090158 -> :sswitch_2
        0x7f09015b -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v6, 0xff

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1337
    const/4 v3, 0x1

    .line 1338
    .local v3, "ret":Z
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v5, :cond_1

    .line 1421
    :cond_0
    :goto_0
    return v8

    .line 1340
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 1341
    .local v1, "m":Landroid/graphics/Matrix;
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1342
    .local v0, "inversM":Landroid/graphics/Matrix;
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1343
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 1344
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    if-le v5, v8, :cond_2

    .line 1346
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMultiTouch:Z

    .line 1353
    :goto_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mGesture:Landroid/view/GestureDetector;

    invoke-virtual {v5, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1355
    const/4 v3, 0x1

    .line 1411
    :goto_2
    :pswitch_0
    :sswitch_0
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawOriginal:Z

    if-nez v5, :cond_0

    .line 1413
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    .line 1414
    .local v2, "matrix":Landroid/graphics/Matrix;
    const/16 v5, 0x9

    new-array v4, v5, [F

    .line 1415
    .local v4, "value":[F
    invoke-virtual {v2, v4}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1416
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "JW scale="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget v6, v4, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1417
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v5, :cond_0

    .line 1418
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    aget v6, v4, v7

    invoke-virtual {v5, p2, v6}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->onTouchEventForSeekbar(Landroid/view/MotionEvent;F)V

    goto :goto_0

    .line 1350
    .end local v2    # "matrix":Landroid/graphics/Matrix;
    .end local v4    # "value":[F
    :cond_2
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMultiTouch:Z

    goto :goto_1

    .line 1359
    :cond_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    invoke-virtual {v5, p2}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1360
    const/4 v3, 0x1

    .line 1362
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 1368
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    goto :goto_2

    .line 1386
    :sswitch_1
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawOriginal:Z

    if-eqz v5, :cond_4

    .line 1388
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawOriginal:Z

    .line 1389
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v5, :cond_4

    .line 1391
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->resetPreProgressValue()V

    .line 1392
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->invalidateViews()V

    .line 1395
    :cond_4
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->startseek()V

    .line 1396
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->isReverseAnimRunning:Z

    .line 1397
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAlpha:I

    if-le v5, v6, :cond_5

    .line 1399
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAlpha:I

    .line 1401
    :cond_5
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 1371
    :sswitch_2
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->isReverseAnimRunning:Z

    if-eqz v5, :cond_6

    .line 1373
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->isReverseAnimRunning:Z

    .line 1374
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mShouldReverseAnimate:Z

    goto :goto_2

    .line 1379
    :cond_6
    const/16 v5, 0x32

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAlpha:I

    goto :goto_2

    .line 1362
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1368
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x105 -> :sswitch_2
    .end sparse-switch
.end method

.method public backPressed()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 786
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    .line 787
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->updateAnimation()V

    .line 789
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_1

    .line 791
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v0, :cond_2

    .line 813
    :cond_1
    :goto_0
    return-void

    .line 795
    :cond_2
    const v0, 0x7f090007

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mOptionItemId:I

    .line 796
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 798
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_1

    .line 800
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 801
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    goto :goto_0

    .line 803
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    goto :goto_0

    .line 807
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 809
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x2e000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public changeImage(I)V
    .locals 5
    .param p1, "trayButtonIdx"    # I

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x42c80000    # 100.0f

    .line 2788
    const-string v1, "changeImage"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 2789
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v1, :cond_0

    .line 2791
    const-string v1, "mTrayManager != null"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 2792
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 2794
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 2796
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v1, :cond_2

    .line 2798
    const-string v1, "mImageData null"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 2852
    :cond_1
    :goto_0
    return-void

    .line 2804
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v2

    div-float/2addr v1, v2

    cmpg-float v1, v1, v3

    if-ltz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v2

    div-float/2addr v1, v2

    cmpg-float v1, v1, v3

    if-gez v1, :cond_7

    .line 2805
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMinimum:Z

    .line 2808
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 2809
    .local v0, "viewBuffer":[I
    if-eqz v0, :cond_5

    .line 2811
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v1, :cond_4

    .line 2813
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->init(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;)V

    .line 2814
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->configurationChange()V

    .line 2816
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_5

    .line 2818
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2819
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->setImageDataToImageEditView(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;)V

    .line 2822
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initEffect()V

    .line 2823
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->invalidateViewsWithThread()V

    .line 2824
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initDialog()V

    .line 2825
    const-string v1, "invalidateViewsWithThread"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 2826
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_6

    .line 2828
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$49;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$49;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2846
    :cond_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    if-nez v1, :cond_1

    .line 2848
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    invoke-direct {v1, p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    .line 2849
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;->start()V

    goto :goto_0

    .line 2807
    .end local v0    # "viewBuffer":[I
    :cond_7
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMinimum:Z

    goto :goto_1
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x0

    .line 314
    .local v0, "ret":I
    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 318
    const/4 v0, 0x0

    .line 319
    .local v0, "ret":I
    return v0
.end method

.method public getStatusHeight()I
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x0

    .line 324
    .local v0, "ret":I
    return v0
.end method

.method public initActionbar()V
    .locals 9

    .prologue
    const/16 v8, 0x12

    const/16 v7, 0x10

    const/4 v4, -0x1

    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 817
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_2

    .line 819
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isValidBackKey()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 821
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 824
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$14;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 821
    invoke-virtual {v1, v6, v4, v4, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 853
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 854
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v1

    .line 856
    :goto_1
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$15;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 853
    invoke-virtual {v3, v6, v1, v6, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 904
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v4, 0x2

    .line 905
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v1

    .line 907
    :goto_2
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$16;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$16;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 904
    invoke-virtual {v3, v4, v1, v6, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 961
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v3, 0x4

    .line 964
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$17;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$17;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 961
    invoke-virtual {v1, v3, v6, v6, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1004
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1007
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$18;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$18;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 1004
    invoke-virtual {v1, v7, v6, v6, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1043
    const/4 v0, 0x0

    .line 1044
    .local v0, "enable":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_0

    .line 1046
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1048
    const/4 v0, 0x1

    .line 1051
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v3, 0x5

    .line 1054
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$19;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$19;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 1051
    invoke-virtual {v1, v3, v0, v6, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1093
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1094
    const/4 v3, 0x0

    .line 1093
    invoke-virtual {v1, v8, v6, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1095
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1, v7}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 1096
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1, v8}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 1146
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMinimum:Z

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resetMenu(Z)V

    .line 1147
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_1

    .line 1148
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isSaved()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 1150
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    .line 1152
    .end local v0    # "enable":Z
    :cond_2
    const-string v1, "normal initActionbar"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1153
    return-void

    .line 850
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v2, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    goto/16 :goto_0

    :cond_4
    move v1, v2

    .line 854
    goto/16 :goto_1

    :cond_5
    move v1, v2

    .line 905
    goto :goto_2
.end method

.method public initButtons()V
    .locals 8

    .prologue
    const v7, 0x10001005

    const v6, 0x10001004

    const v5, 0x1000100b

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 329
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewRatio()V

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_1

    .line 333
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 334
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMinimum:Z

    if-eqz v0, :cond_2

    .line 336
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 337
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$6;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001006

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 363
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 618
    :goto_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getPreviousMode()I

    move-result v0

    const/high16 v1, -0x10000

    and-int/2addr v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 652
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 653
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBottomButtonDirectly(I)V

    .line 656
    :cond_1
    const-string v0, "normal initButton"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 658
    return-void

    .line 367
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$7;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 391
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$8;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-virtual {v0, v7, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 424
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001006

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$9;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 449
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$10;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 481
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v0, :cond_3

    .line 483
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$11;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_0

    .line 509
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEnableDecoration()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 511
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$12;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_0

    .line 539
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_0

    .line 621
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001007

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto :goto_1

    .line 624
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001009

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto :goto_1

    .line 627
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001008

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 630
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 633
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 636
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001006

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 639
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001003

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 642
    :sswitch_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001002

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 618
    :sswitch_data_0
    .sparse-switch
        0x11100000 -> :sswitch_7
        0x11200000 -> :sswitch_6
        0x15000000 -> :sswitch_4
        0x16000000 -> :sswitch_3
        0x18000000 -> :sswitch_5
        0x1a000000 -> :sswitch_1
        0x31100000 -> :sswitch_2
        0x31200000 -> :sswitch_0
    .end sparse-switch
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 1157
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 1159
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 1160
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initShareViaDialog()V

    .line 1161
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initSetAsDialog()V

    .line 1162
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initTrayDeleteQuestionDialog()V

    .line 1163
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initDefaultSelectDialog()V

    .line 1164
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initSelectedDialog()V

    .line 1165
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initSaveYesNoCancelForFinish()V

    .line 1166
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initUndoRedoAllDialog()V

    .line 1167
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initSaveOptionDialog()V

    .line 1168
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initCancelDialog()V

    .line 1174
    :cond_0
    const-string v0, "normal initDialog"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1175
    return-void
.end method

.method public initEffect()V
    .locals 3

    .prologue
    const/high16 v2, 0x42c80000    # 100.0f

    .line 274
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_2

    .line 276
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    .line 277
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMinimum:Z

    .line 280
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMinimum:Z

    if-eqz v0, :cond_1

    .line 282
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    const v2, 0x7f0600c4

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 284
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    if-nez v0, :cond_2

    .line 286
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    .line 287
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;->start()V

    .line 290
    :cond_2
    return-void

    .line 279
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMinimum:Z

    goto :goto_0
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 1296
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 2738
    return-void
.end method

.method public initTrayLayout()V
    .locals 3

    .prologue
    .line 2741
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 2743
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v1, 0x0

    .line 2744
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$48;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$48;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 2743
    invoke-virtual {v0, v1, p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 2777
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 2779
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2781
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->setEdited()V

    .line 2785
    :cond_0
    return-void
.end method

.method public initView()V
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->setImageEditViewPinchZoomCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;)V

    .line 295
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->startImageEditViewAnimation(Landroid/view/animation/Animation;)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_1

    .line 307
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V

    .line 308
    :cond_1
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x20000000

    if-ne v0, v1, :cond_2

    .line 309
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->show()V

    .line 310
    :cond_2
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 3
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x9

    .line 1465
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1466
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1472
    const v0, 0x7a1200

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mCurrentSaveSize:I

    .line 1473
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1474
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1493
    :cond_0
    :goto_0
    return-void

    .line 1478
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_0

    .line 1480
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 1481
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 1482
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1483
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1484
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1486
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1488
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v0, :cond_0

    .line 1489
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->init(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1427
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsDestory:Z

    .line 1428
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1429
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    .line 1430
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPaint:Landroid/graphics/Paint;

    .line 1431
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mClearPaint:Landroid/graphics/Paint;

    .line 1432
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mLinePaint:Landroid/graphics/Paint;

    .line 1433
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 1435
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v0, :cond_0

    .line 1436
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->destroy()V

    .line 1437
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 1438
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_1

    .line 1439
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 1441
    :cond_1
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1442
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1444
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 1445
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->show()V

    .line 1446
    :cond_2
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1448
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_3

    .line 1450
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->free()V

    .line 1451
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1453
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v0, :cond_4

    .line 1455
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->Destroy()V

    .line 1456
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1459
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    if-eqz v0, :cond_5

    .line 1460
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;->destroy()V

    .line 1461
    :cond_5
    return-void
.end method

.method public declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/16 v11, 0xff

    .line 1181
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsReadyForDraw:Z

    if-eqz v1, :cond_0

    .line 1183
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 1290
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1186
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_2

    .line 1189
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawOriginal:Z

    if-eqz v1, :cond_4

    .line 1191
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOrgPreviewWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOrgPreviewHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 1192
    .local v10, "tempOriginalViewBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1193
    .local v0, "tempCanvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getInitialPreviewInputBuffer()[I

    move-result-object v1

    .line 1194
    const/4 v2, 0x0

    .line 1195
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOrgPreviewWidth()I

    move-result v3

    .line 1196
    const/4 v4, 0x0

    .line 1197
    const/4 v5, 0x0

    .line 1198
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOrgPreviewWidth()I

    move-result v6

    .line 1199
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOrgPreviewHeight()I

    move-result v7

    .line 1200
    const/4 v8, 0x1

    .line 1201
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPaint:Landroid/graphics/Paint;

    .line 1193
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 1206
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1207
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPaint:Landroid/graphics/Paint;

    .line 1204
    invoke-static {p1, v10, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawOrgImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 1210
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 1239
    .end local v0    # "tempCanvas":Landroid/graphics/Canvas;
    .end local v10    # "tempOriginalViewBitmap":Landroid/graphics/Bitmap;
    :cond_2
    :goto_1
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMultiTouch:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mShouldReverseAnimate:Z

    if-eqz v1, :cond_9

    .line 1244
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->isReverseAnimRunning:Z

    if-eqz v1, :cond_7

    .line 1248
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAlpha:I

    if-lez v1, :cond_6

    .line 1251
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1252
    const/4 v2, -0x1

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawOriginal:Z

    .line 1253
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAlpha:I

    .line 1251
    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    .line 1254
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAlpha:I

    add-int/lit8 v1, v1, -0xf

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAlpha:I

    .line 1256
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1181
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1216
    :cond_4
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1219
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1220
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1221
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPaint:Landroid/graphics/Paint;

    .line 1218
    invoke-static {p1, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 1227
    :cond_5
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1228
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1229
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mColor:I

    .line 1230
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPaint:Landroid/graphics/Paint;

    .line 1231
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mClearPaint:Landroid/graphics/Paint;

    .line 1232
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mLinePaint:Landroid/graphics/Paint;

    .line 1233
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    move-object v1, p1

    .line 1226
    invoke-static/range {v1 .. v8}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawCanvasWithPath(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;ILandroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 1260
    :cond_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->resetPreProgressValue()V

    .line 1261
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->isReverseAnimRunning:Z

    .line 1262
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mShouldReverseAnimate:Z

    goto/16 :goto_0

    .line 1266
    :cond_7
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mShouldReverseAnimate:Z

    .line 1268
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAlpha:I

    if-gt v1, v11, :cond_8

    .line 1270
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1271
    const/4 v2, -0x1

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawOriginal:Z

    .line 1272
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAlpha:I

    .line 1270
    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    .line 1273
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAlpha:I

    add-int/lit8 v1, v1, 0xf

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mAlpha:I

    goto/16 :goto_0

    .line 1277
    :cond_8
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1278
    const/4 v2, -0x1

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawOriginal:Z

    const/16 v4, 0xff

    .line 1277
    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    goto/16 :goto_0

    .line 1283
    :cond_9
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawOriginal:Z

    if-eqz v1, :cond_0

    .line 1284
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1285
    const/4 v2, -0x1

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawOriginal:Z

    .line 1286
    const/16 v4, 0xff

    .line 1284
    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 1496
    const/16 v0, 0x17

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-ne p1, v0, :cond_3

    .line 1498
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 1499
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    .line 1502
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_2

    .line 1503
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 1529
    :cond_2
    :goto_0
    return v1

    .line 1507
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    .line 1509
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->backPressed()V

    goto :goto_0

    .line 1512
    :cond_4
    const/16 v0, 0x52

    if-ne p1, v0, :cond_2

    .line 1513
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    goto :goto_0
.end method

.method public onLayout()V
    .locals 4

    .prologue
    .line 3448
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isReadyForPreview()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3450
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsReadyForDraw:Z

    .line 3452
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_1

    .line 3453
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeLayoutSize(I)V

    .line 3454
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getConfigChange()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3456
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 3457
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setConfigChange(Z)V

    .line 3461
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 3462
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 3463
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->getImageEditViewWidth()I

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->getImageEditViewHeight()I

    move-result v0

    if-eqz v0, :cond_3

    .line 3465
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_3

    .line 3467
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsDestory:Z

    if-eqz v0, :cond_4

    .line 3521
    :cond_3
    :goto_0
    return-void

    .line 3469
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setOrientation(I)V

    .line 3470
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->getImageEditViewHeight()I

    move-result v2

    .line 3471
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$52;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$52;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    .line 3470
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(IILcom/sec/android/mimage/photoretouching/Core/ImageData$setViewSizeMatrixCallback;)V

    .line 3483
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEnableDecoration()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isToastShowing()Z

    move-result v0

    if-nez v0, :cond_5

    .line 3484
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$53;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$53;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3495
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v0, :cond_3

    .line 3498
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->configurationChange()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 1535
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->runOptionItem(I)Z

    .line 1536
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2855
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 2857
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->resume()V

    .line 2859
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 2861
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resume()V

    .line 2868
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->msave:Z

    if-eqz v0, :cond_2

    .line 2869
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2870
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mCurrentSaveSize:I

    const v1, 0x7a1200

    if-ne v0, v1, :cond_5

    .line 2871
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cb

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setSelectedFalseAnotherButtons(I)V

    .line 2874
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->msave:Z

    .line 2876
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_3

    .line 2877
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 2878
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2879
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 2880
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2893
    :cond_4
    return-void

    .line 2873
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601f0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setSelectedFalseAnotherButtons(I)V

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2897
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->pause()V

    .line 2898
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f090158

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2899
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->msetas:Z

    .line 2900
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 2902
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2903
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->msave:Z

    .line 2904
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 2908
    :cond_1
    return-void
.end method

.method public refreshView()V
    .locals 3

    .prologue
    .line 3678
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 3679
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->getImageEditViewHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 3681
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->refreshImageAndBottomButtons()V

    .line 3682
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 1

    .prologue
    .line 1300
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 1301
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 1302
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 1303
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 1304
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V

    .line 1305
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_0

    .line 1306
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 1308
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;

    if-eqz v0, :cond_1

    .line 1310
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->show()V

    .line 1311
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mShowTopBottomButtons:Z

    .line 1316
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    .line 1322
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->setImageEditViewPinchZoomCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;)V

    .line 1329
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPopupLayout()V

    .line 1332
    return-void
.end method
