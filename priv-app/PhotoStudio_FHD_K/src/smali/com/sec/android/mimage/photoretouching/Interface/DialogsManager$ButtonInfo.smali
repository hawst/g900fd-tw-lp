.class Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
.super Ljava/lang/Object;
.source "DialogsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ButtonInfo"
.end annotation


# instance fields
.field button:Landroid/widget/LinearLayout;

.field buttonId:I

.field buttonType:I

.field icon:Landroid/graphics/drawable/Drawable;

.field iconId:I

.field iconId2:I

.field key:Ljava/lang/String;

.field seekBarLeftListener:Landroid/view/View$OnClickListener;

.field seekBarListener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

.field seekBarMax:I

.field seekBarRightListener:Landroid/view/View$OnClickListener;

.field seekBarStartPos:I

.field text:Ljava/lang/String;

.field textId:I

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field thumbnail:Landroid/graphics/Bitmap;

.field touchFunction:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1912
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1922
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarListener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    .line 1923
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarLeftListener:Landroid/view/View$OnClickListener;

    .line 1924
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarRightListener:Landroid/view/View$OnClickListener;

    .line 1925
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarStartPos:I

    .line 1926
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarMax:I

    .line 1928
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->thumbnail:Landroid/graphics/Bitmap;

    .line 1930
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->key:Ljava/lang/String;

    .line 1932
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->touchFunction:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1933
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    return-void
.end method
