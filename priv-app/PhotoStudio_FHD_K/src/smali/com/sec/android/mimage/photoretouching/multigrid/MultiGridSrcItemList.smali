.class public Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
.super Ljava/util/ArrayList;
.source "MultiGridSrcItemList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;",
        ">;"
    }
.end annotation


# static fields
.field public static MAX_ITEM_CNT:I

.field public static MIN_ITEM_CNT:I


# instance fields
.field private final MOVE_TOLLERENCE:I

.field private mCurrenIndex:I

.field private mDragFromIndex:I

.field private mDragToIndex:I

.field private mPrevX:F

.field private mPrevY:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->MIN_ITEM_CNT:I

    .line 294
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->MAX_ITEM_CNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 14
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 295
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->MOVE_TOLLERENCE:I

    .line 297
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mCurrenIndex:I

    .line 298
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mPrevX:F

    .line 299
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mPrevY:F

    .line 300
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mDragFromIndex:I

    .line 301
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mDragToIndex:I

    .line 17
    return-void
.end method

.method private removeBrokenImg()V
    .locals 4

    .prologue
    .line 56
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 71
    return-void

    .line 58
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 59
    .local v1, "item":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cheus, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : removeBrokenImg : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getDecodeInfo()Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    move-result-object v3

    iget v3, v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->index:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 60
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getDecodeInfo()Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    move-result-object v2

    iget-boolean v2, v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isCanceled:Z

    if-eqz v2, :cond_1

    .line 62
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->remove(Ljava/lang/Object;)Z

    .line 63
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cheus, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : removeBrokenImg : remove : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_0

    .line 67
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getDecodeInfo()Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    move-result-object v2

    iput v0, v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->index:I

    .line 68
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public changeCurrentIndexImage(Landroid/graphics/Bitmap;)Z
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x0

    .line 110
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mCurrenIndex:I

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->isIndexInList(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 122
    :goto_0
    return v1

    .line 113
    :cond_0
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mCurrenIndex:I

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 115
    .local v0, "multiItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setOrgImage(Landroid/graphics/Bitmap;)V

    .line 116
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 118
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : changeCurrentIndexImage : true : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mCurrenIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 119
    const/4 v1, 0x1

    goto :goto_0

    .line 121
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cheus, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : changeCurrentIndexImage : false : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mCurrenIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public changeImage(I)Z
    .locals 6
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 76
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cheus, "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : changeImage : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 79
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->isIndexInList(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 81
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->removeBrokenImg()V

    .line 105
    :goto_0
    return v2

    .line 85
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 87
    .local v1, "multiItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setOrgImage()V

    .line 88
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_1

    move v5, v3

    :goto_1
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v2

    :goto_2
    and-int/2addr v4, v5

    if-eqz v4, :cond_3

    .line 90
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cheus, "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : changeImage : true : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    move v2, v3

    .line 91
    goto :goto_0

    :cond_1
    move v5, v2

    .line 88
    goto :goto_1

    :cond_2
    move v4, v3

    goto :goto_2

    .line 93
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cheus, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : changeImage : false : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 95
    .end local v1    # "multiItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 100
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 102
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public changeIndex(II)Z
    .locals 6
    .param p1, "index1"    # I
    .param p2, "index2"    # I

    .prologue
    const/4 v4, 0x0

    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->isIndexInList(I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v4

    .line 26
    :cond_1
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->isIndexInList(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 31
    if-eq p1, p2, :cond_0

    .line 36
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 37
    .local v0, "indexH":I
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 39
    .local v1, "indexT":I
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 40
    .local v2, "itemH":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 42
    .local v3, "itemT":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setOrgImage()V

    .line 43
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setOrgImage()V

    .line 45
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->remove(I)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 46
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->add(ILjava/lang/Object;)V

    .line 48
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->remove(I)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 49
    invoke-virtual {p0, v0, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->add(ILjava/lang/Object;)V

    .line 51
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public contain(Landroid/net/Uri;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 136
    const/4 v1, 0x0

    .line 137
    .local v1, "item":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 146
    const/4 v2, 0x0

    :goto_1
    return v2

    .line 139
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "item":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 140
    .restart local v1    # "item":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getUri()Landroid/net/Uri;

    move-result-object v2

    if-ne v2, p1, :cond_1

    .line 142
    const/4 v2, 0x1

    goto :goto_1

    .line 137
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public destroy()V
    .locals 3

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 226
    return-void

    .line 222
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 224
    .local v0, "item":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->destroy()V

    goto :goto_0
.end method

.method public destroy(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 230
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : destroy : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 231
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->isIndexInList(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->destroy()V

    .line 235
    :cond_0
    return-void
.end method

.method public getAddImgCnt()I
    .locals 2

    .prologue
    .line 289
    sget v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->MAX_ITEM_CNT:I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getCurrentIndex()I
    .locals 1

    .prologue
    .line 283
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mCurrenIndex:I

    return v0
.end method

.method public getDragFromIndex()I
    .locals 1

    .prologue
    .line 255
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mDragFromIndex:I

    return v0
.end method

.method public getDragToIndex()I
    .locals 1

    .prologue
    .line 263
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mDragToIndex:I

    return v0
.end method

.method public getMaxSizeIndex()I
    .locals 4

    .prologue
    .line 239
    const/4 v1, -0x1

    .line 240
    .local v1, "index":I
    const/4 v2, 0x0

    .line 242
    .local v2, "maxSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 251
    return v1

    .line 244
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgSizeInPixel()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 246
    move v1, v0

    .line 247
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgSizeInPixel()I

    move-result v2

    .line 242
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public isIndexInList(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 127
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 129
    :cond_0
    const/4 v0, 0x0

    .line 131
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public moveItem(IFF)Z
    .locals 5
    .param p1, "index"    # I
    .param p2, "pointX"    # F
    .param p3, "pointY"    # F

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x0

    .line 165
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mCurrenIndex:I

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->isIndexInList(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 189
    :cond_0
    :goto_0
    return v2

    .line 170
    :cond_1
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mCurrenIndex:I

    if-eq v3, p1, :cond_2

    .line 172
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mCurrenIndex:I

    goto :goto_0

    .line 176
    :cond_2
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mPrevX:F

    sub-float v3, p2, v3

    float-to-int v1, v3

    .line 177
    .local v1, "diffRight":I
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mPrevY:F

    sub-float v3, p3, v3

    float-to-int v0, v3

    .line 179
    .local v0, "diffBottom":I
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mPrevX:F

    .line 180
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mPrevY:F

    .line 183
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-gt v3, v4, :cond_3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-le v3, v4, :cond_0

    .line 185
    :cond_3
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mCurrenIndex:I

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v2, v1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->moveDrawRect(II)V

    .line 186
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public moveItemEnd()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 193
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mCurrenIndex:I

    .line 194
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mPrevX:F

    .line 195
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mPrevY:F

    .line 196
    return-void
.end method

.method public moveItemStart(IFF)Z
    .locals 1
    .param p1, "index"    # I
    .param p2, "pointX"    # F
    .param p3, "pointY"    # F

    .prologue
    .line 150
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->isIndexInList(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mCurrenIndex:I

    .line 153
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mPrevX:F

    .line 154
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mPrevY:F

    .line 156
    const/4 v0, 0x1

    .line 160
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pinchZoom(II)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "diffDst"    # I

    .prologue
    .line 200
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->isIndexInList(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v0, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->pinchZoom(I)V

    .line 204
    :cond_0
    return-void
.end method

.method public remove(I)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 209
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->isIndexInList(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    invoke-super {p0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 216
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic remove(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->remove(I)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    move-result-object v0

    return-object v0
.end method

.method public resetDragIndex()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 272
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mDragFromIndex:I

    .line 273
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mDragToIndex:I

    .line 274
    return-void
.end method

.method public setCurrentIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 278
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mCurrenIndex:I

    .line 279
    return-void
.end method

.method public setDragFromIndex(I)V
    .locals 0
    .param p1, "mDragFromIndex"    # I

    .prologue
    .line 259
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mDragFromIndex:I

    .line 260
    return-void
.end method

.method public setDragToIndex(I)V
    .locals 0
    .param p1, "mDragToIndex"    # I

    .prologue
    .line 267
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->mDragToIndex:I

    .line 268
    return-void
.end method
