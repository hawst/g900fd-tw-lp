.class Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CountGetIntentTimeThread;
.super Ljava/lang/Thread;
.source "MultiGridActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CountGetIntentTimeThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)V
    .locals 1

    .prologue
    .line 884
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CountGetIntentTimeThread;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    .line 883
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 885
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->access$1(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;Z)V

    .line 886
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/16 v5, 0x32

    const/4 v4, 0x1

    .line 888
    const/4 v1, 0x0

    .line 889
    .local v1, "sleepCount":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CountGetIntentTimeThread;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mIsGetIntentFinish:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->access$2(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)Z

    move-result v2

    if-nez v2, :cond_0

    if-lt v1, v5, :cond_1

    .line 899
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CountGetIntentTimeThread;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mIsGetIntentFinish:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->access$2(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)Z

    move-result v2

    if-nez v2, :cond_2

    if-ne v1, v5, :cond_2

    .line 901
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CountGetIntentTimeThread;->destroy()V

    .line 902
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CountGetIntentTimeThread;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->prcessingException()V
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->access$3(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)V

    .line 903
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CountGetIntentTimeThread;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->access$1(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;Z)V

    .line 907
    :goto_1
    return-void

    .line 892
    :cond_1
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 897
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 893
    :catch_0
    move-exception v0

    .line 895
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    .line 906
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$CountGetIntentTimeThread;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->access$1(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;Z)V

    goto :goto_1
.end method
