.class Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;
.super Ljava/lang/Thread;
.source "ColorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ColorAnimationThread"
.end annotation


# instance fields
.field private mBuffer:[I

.field private mDuration:I

.field private mKillThread:Z

.field private mRunningAnimation:Z

.field private mStartTime:J

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;[I[I)V
    .locals 3
    .param p2, "animationBuffer"    # [I
    .param p3, "srcPreview"    # [I

    .prologue
    const/4 v2, 0x0

    .line 2864
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    .line 2863
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2858
    const/16 v0, 0x2bc

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mDuration:I

    .line 2859
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mStartTime:J

    .line 2860
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mKillThread:Z

    .line 2861
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mRunningAnimation:Z

    .line 2862
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mBuffer:[I

    .line 2865
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mBuffer:[I

    .line 2866
    return-void
.end method

.method private declared-synchronized wakeUp()V
    .locals 1

    .prologue
    .line 2947
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2948
    monitor-exit p0

    return-void

    .line 2947
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized drawCanvas(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2874
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mBuffer:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mKillThread:Z

    if-nez v0, :cond_0

    .line 2876
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v10

    .line 2877
    .local v10, "supMatrixBasedOnViewTransform":Landroid/graphics/Matrix;
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2878
    invoke-virtual {p1, v10}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 2879
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mBuffer:[I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 2880
    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v0, p1

    .line 2879
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 2881
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2883
    .end local v10    # "supMatrixBasedOnViewTransform":Landroid/graphics/Matrix;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->wakeUp()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2884
    monitor-exit p0

    return-void

    .line 2874
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public free()V
    .locals 1

    .prologue
    .line 2869
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mRunningAnimation:Z

    .line 2870
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mBuffer:[I

    .line 2871
    return-void
.end method

.method public killThread()V
    .locals 3

    .prologue
    .line 2926
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mKillThread:Z

    .line 2927
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->wakeUp()V

    .line 2928
    const/4 v1, 0x1

    .line 2929
    .local v1, "retry":Z
    :goto_0
    if-nez v1, :cond_0

    .line 2939
    return-void

    .line 2932
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2937
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2933
    :catch_0
    move-exception v0

    .line 2935
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method public run()V
    .locals 12

    .prologue
    .line 2888
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 2889
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mRunningAnimation:Z

    .line 2890
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mStartTime:J

    .line 2891
    const/4 v8, 0x0

    .line 2894
    .local v8, "diff":I
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mStartTime:J

    sub-long/2addr v0, v2

    long-to-int v8, v0

    .line 2895
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mDuration:I

    if-lt v0, v8, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mKillThread:Z

    if-eqz v0, :cond_1

    .line 2921
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->invalidateViewsWithThread()V

    .line 2922
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->free()V

    .line 2923
    return-void

    .line 2897
    :cond_1
    int-to-float v0, v8

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mDuration:I

    int-to-float v1, v1

    div-float v10, v0, v1

    .line 2898
    .local v10, "input":F
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, v10, v0

    if-lez v0, :cond_2

    .line 2899
    const/high16 v10, 0x3f800000    # 1.0f

    .line 2900
    :cond_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v11

    monitor-enter v11

    .line 2901
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2903
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    move-result-object v0

    const/high16 v1, 0x42480000    # 50.0f

    mul-float/2addr v1, v10

    float-to-int v1, v1

    .line 2904
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    .line 2905
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mBuffer:[I

    .line 2906
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskBuffer()[B

    move-result-object v4

    .line 2907
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    .line 2908
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    .line 2909
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v7

    .line 2903
    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->applyPreview(I[I[I[BIILandroid/graphics/Rect;)I

    .line 2911
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->invalidateViewsWithThread(Landroid/graphics/Rect;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2914
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2900
    :goto_1
    :try_start_2
    monitor-exit v11

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2915
    :catch_0
    move-exception v9

    .line 2916
    .local v9, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v9}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public runningAnimation()Z
    .locals 1

    .prologue
    .line 2943
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ColorAnimationThread;->mRunningAnimation:Z

    return v0
.end method
