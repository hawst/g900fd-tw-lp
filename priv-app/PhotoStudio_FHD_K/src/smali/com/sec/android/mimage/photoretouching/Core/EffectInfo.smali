.class public Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
.super Ljava/lang/Object;
.source "EffectInfo.java"


# instance fields
.field private mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

.field private mClearSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;

.field private mClipboardEffect:Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

.field private mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

.field private mCrop:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

.field private mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

.field private mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

.field private mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

.field private mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

.field private mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

.field private mPenEffect:Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

.field private mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

.field private mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

.field private mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

.field private mScaleH:F

.field private mScaleW:F

.field private mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

.field private mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

.field private mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;)V
    .locals 1
    .param p1, "AdjustEffect"    # Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 56
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    .line 57
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;)V
    .locals 1
    .param p1, "clearselecteffect"    # Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 74
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClearSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;

    .line 75
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClearSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;)V
    .locals 1
    .param p1, "clipboardeffect"    # Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 92
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClipboardEffect:Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClipboardEffect:Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;)V

    .line 94
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;)V
    .locals 1
    .param p1, "colorEffect"    # Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 35
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    .line 36
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/CropEffect;)V
    .locals 1
    .param p1, "crop"    # Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 87
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mCrop:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    .line 88
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mCrop:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/CropEffect;)V

    .line 89
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V
    .locals 1
    .param p1, "effectEffect"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 41
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .line 42
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;)V
    .locals 1
    .param p1, "frameEffect"    # Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 110
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    .line 111
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;)V

    .line 112
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;)V
    .locals 1
    .param p1, "imageStickerEffect"    # Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 134
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    .line 135
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;)V

    .line 136
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;)V
    .locals 1
    .param p1, "labelEffect"    # Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 126
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    .line 127
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;)V

    .line 128
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;)V
    .locals 1
    .param p1, "mirrorEffect"    # Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 114
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    .line 115
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;)V

    .line 116
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/PenEffect;)V
    .locals 1
    .param p1, "penEffect"    # Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 130
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPenEffect:Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    .line 131
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPenEffect:Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/PenEffect;)V

    .line 132
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)V
    .locals 1
    .param p1, "portraitEffect"    # Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 62
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    .line 63
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;)V
    .locals 1
    .param p1, "effectEffect"    # Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 47
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    .line 48
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;)V

    .line 50
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleW:F

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 51
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mScaleH:F

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)V
    .locals 1
    .param p1, "rotateEffect"    # Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 105
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    .line 106
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;)V
    .locals 1
    .param p1, "rotateeffect"    # Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
    .param p2, "mirroreffect"    # Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 79
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    .line 80
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)V

    .line 82
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    .line 83
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-virtual {v0, p2}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;)V

    .line 84
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)V
    .locals 1
    .param p1, "selectEffect"    # Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 68
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    .line 69
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/StampEffect;)V
    .locals 1
    .param p1, "stampEffect"    # Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 118
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    .line 119
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/StampEffect;)V

    .line 120
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;)V
    .locals 1
    .param p1, "stickerEffect"    # Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    .line 31
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    .line 122
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    .line 123
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->copy(Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;)V

    .line 124
    return-void
.end method


# virtual methods
.method public applyImage(I)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "Idx"    # I

    .prologue
    const/4 v3, 0x0

    .line 181
    const/4 v0, 0x0

    .line 182
    .local v0, "ret":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    if-eqz v1, :cond_0

    .line 183
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->applyImage(IZZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 184
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    if-eqz v1, :cond_1

    .line 185
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v1, p1, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->applyPreview(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 186
    :cond_1
    return-object v0
.end method

.method public applyOriginal()[I
    .locals 2

    .prologue
    .line 140
    const/4 v0, 0x0

    .line 141
    .local v0, "ret":[I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    if-eqz v1, :cond_0

    .line 142
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->applyOriginal()[I

    move-result-object v0

    .line 143
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    if-eqz v1, :cond_1

    .line 144
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->applyOriginal()[I

    move-result-object v0

    .line 145
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    if-eqz v1, :cond_2

    .line 146
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->applyOriginal()[I

    move-result-object v0

    .line 147
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    if-eqz v1, :cond_3

    .line 148
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->applyOriginal()[I

    move-result-object v0

    .line 149
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    if-eqz v1, :cond_4

    .line 150
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->applyOriginal()[I

    move-result-object v0

    .line 151
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    if-eqz v1, :cond_5

    .line 152
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->applyOriginal()[I

    move-result-object v0

    .line 153
    :cond_5
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mCrop:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    if-eqz v1, :cond_6

    .line 154
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mCrop:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->applyOriginal()[I

    move-result-object v0

    .line 155
    :cond_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    if-eqz v1, :cond_7

    .line 156
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->applyOriginal()[I

    move-result-object v0

    .line 157
    :cond_7
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    if-eqz v1, :cond_8

    .line 158
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->applyOriginal()[I

    move-result-object v0

    .line 159
    :cond_8
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClearSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;

    if-eqz v1, :cond_9

    .line 160
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClearSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->applyOriginal()[I

    move-result-object v0

    .line 161
    :cond_9
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    if-eqz v1, :cond_a

    .line 162
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->applyOriginal()[I

    move-result-object v0

    .line 163
    :cond_a
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    if-eqz v1, :cond_b

    .line 164
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->applyOriginal()[I

    move-result-object v0

    .line 165
    :cond_b
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClipboardEffect:Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    if-eqz v1, :cond_c

    .line 166
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClipboardEffect:Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->applyOriginal()[I

    move-result-object v0

    .line 167
    :cond_c
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    if-eqz v1, :cond_d

    .line 168
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->applyOriginal()[I

    move-result-object v0

    .line 169
    :cond_d
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    if-eqz v1, :cond_e

    .line 170
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->applyOriginal()[I

    move-result-object v0

    .line 171
    :cond_e
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    if-eqz v1, :cond_f

    .line 172
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->applyOriginal()[I

    move-result-object v0

    .line 173
    :cond_f
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPenEffect:Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    if-eqz v1, :cond_10

    .line 174
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPenEffect:Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->applyOriginal()[I

    move-result-object v0

    .line 176
    :cond_10
    return-object v0
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 282
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    if-eqz v0, :cond_0

    .line 285
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    if-eqz v0, :cond_1

    .line 290
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .line 292
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    if-eqz v0, :cond_2

    .line 294
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->destroy()V

    .line 295
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    .line 297
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    if-eqz v0, :cond_3

    .line 300
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    .line 302
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    if-eqz v0, :cond_4

    .line 305
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    .line 307
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    if-eqz v0, :cond_5

    .line 310
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    .line 312
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mCrop:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    if-eqz v0, :cond_6

    .line 315
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mCrop:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    .line 317
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    if-eqz v0, :cond_7

    .line 320
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    .line 322
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    if-eqz v0, :cond_8

    .line 325
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    .line 327
    :cond_8
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClearSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;

    if-eqz v0, :cond_9

    .line 330
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClearSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;

    .line 332
    :cond_9
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    if-eqz v0, :cond_a

    .line 335
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    .line 337
    :cond_a
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    if-eqz v0, :cond_b

    .line 339
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->Destroy()V

    .line 340
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    .line 342
    :cond_b
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClipboardEffect:Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    if-eqz v0, :cond_c

    .line 344
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClipboardEffect:Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->destroy()V

    .line 345
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClipboardEffect:Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    .line 347
    :cond_c
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    if-eqz v0, :cond_d

    .line 349
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->Destroy()V

    .line 350
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    .line 352
    :cond_d
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    if-eqz v0, :cond_e

    .line 354
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->Destroy()V

    .line 355
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    .line 357
    :cond_e
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    if-eqz v0, :cond_f

    .line 359
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->Destroy()V

    .line 360
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    .line 362
    :cond_f
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPenEffect:Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    if-eqz v0, :cond_10

    .line 364
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPenEffect:Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->Destroy()V

    .line 365
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPenEffect:Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    .line 367
    :cond_10
    return-void
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->getHeight()I

    move-result v0

    .line 270
    :goto_0
    return v0

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    if-eqz v0, :cond_1

    .line 236
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getHeight()I

    move-result v0

    goto :goto_0

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    if-eqz v0, :cond_2

    .line 238
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->getHeight()I

    move-result v0

    goto :goto_0

    .line 239
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    if-eqz v0, :cond_3

    .line 240
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->getHeight()I

    move-result v0

    goto :goto_0

    .line 241
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    if-eqz v0, :cond_4

    .line 242
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getHeight()I

    move-result v0

    goto :goto_0

    .line 243
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    if-eqz v0, :cond_5

    .line 244
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->getHeight()I

    move-result v0

    goto :goto_0

    .line 245
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mCrop:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    if-eqz v0, :cond_6

    .line 246
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mCrop:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->getHeight()I

    move-result v0

    goto :goto_0

    .line 247
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    if-eqz v0, :cond_7

    .line 248
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->getHeight()I

    move-result v0

    goto :goto_0

    .line 249
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    if-eqz v0, :cond_8

    .line 250
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->getHeight()I

    move-result v0

    goto :goto_0

    .line 253
    :cond_8
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    if-eqz v0, :cond_9

    .line 254
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getHeight()I

    move-result v0

    goto :goto_0

    .line 255
    :cond_9
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    if-eqz v0, :cond_a

    .line 256
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->getHeight()I

    move-result v0

    goto :goto_0

    .line 257
    :cond_a
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClipboardEffect:Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    if-eqz v0, :cond_b

    .line 258
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClipboardEffect:Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->getHeight()I

    move-result v0

    goto :goto_0

    .line 259
    :cond_b
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    if-eqz v0, :cond_c

    .line 260
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->getHeight()I

    move-result v0

    goto/16 :goto_0

    .line 261
    :cond_c
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    if-eqz v0, :cond_d

    .line 262
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->getHeight()I

    move-result v0

    goto/16 :goto_0

    .line 264
    :cond_d
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    if-eqz v0, :cond_e

    .line 265
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->getHeight()I

    move-result v0

    goto/16 :goto_0

    .line 267
    :cond_e
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPenEffect:Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    if-eqz v0, :cond_f

    .line 268
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPenEffect:Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->getHeight()I

    move-result v0

    goto/16 :goto_0

    .line 270
    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public getScaleH()F
    .locals 1

    .prologue
    .line 278
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleH:F

    return v0
.end method

.method public getScaleW()F
    .locals 1

    .prologue
    .line 274
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mScaleW:F

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->getWidth()I

    move-result v0

    .line 228
    :goto_0
    return v0

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getWidth()I

    move-result v0

    goto :goto_0

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    if-eqz v0, :cond_2

    .line 196
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mAdjustEffect:Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/AdjustEffect;->getWidth()I

    move-result v0

    goto :goto_0

    .line 197
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    if-eqz v0, :cond_3

    .line 198
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->getWidth()I

    move-result v0

    goto :goto_0

    .line 199
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    if-eqz v0, :cond_4

    .line 200
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getWidth()I

    move-result v0

    goto :goto_0

    .line 201
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    if-eqz v0, :cond_5

    .line 202
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->getWidth()I

    move-result v0

    goto :goto_0

    .line 203
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mCrop:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    if-eqz v0, :cond_6

    .line 204
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mCrop:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->getWidth()I

    move-result v0

    goto :goto_0

    .line 205
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    if-eqz v0, :cond_7

    .line 206
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->getWidth()I

    move-result v0

    goto :goto_0

    .line 207
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    if-eqz v0, :cond_8

    .line 208
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->getWidth()I

    move-result v0

    goto :goto_0

    .line 211
    :cond_8
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    if-eqz v0, :cond_9

    .line 212
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getWidth()I

    move-result v0

    goto :goto_0

    .line 213
    :cond_9
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    if-eqz v0, :cond_a

    .line 214
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->getWidth()I

    move-result v0

    goto :goto_0

    .line 215
    :cond_a
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClipboardEffect:Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    if-eqz v0, :cond_b

    .line 216
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mClipboardEffect:Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;->getWidth()I

    move-result v0

    goto :goto_0

    .line 217
    :cond_b
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    if-eqz v0, :cond_c

    .line 218
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->getWidth()I

    move-result v0

    goto/16 :goto_0

    .line 219
    :cond_c
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    if-eqz v0, :cond_d

    .line 220
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->getWidth()I

    move-result v0

    goto/16 :goto_0

    .line 222
    :cond_d
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    if-eqz v0, :cond_e

    .line 223
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->getWidth()I

    move-result v0

    goto/16 :goto_0

    .line 225
    :cond_e
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPenEffect:Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    if-eqz v0, :cond_f

    .line 226
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->mPenEffect:Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->getWidth()I

    move-result v0

    goto/16 :goto_0

    .line 228
    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
