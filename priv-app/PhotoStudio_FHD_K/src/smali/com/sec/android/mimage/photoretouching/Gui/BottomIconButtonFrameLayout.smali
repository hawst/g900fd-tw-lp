.class public Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;
.super Landroid/widget/LinearLayout;
.source "BottomIconButtonFrameLayout.java"


# instance fields
.field private mButtonId:I

.field private mContext:Landroid/content/Context;

.field private mDivider:Landroid/widget/LinearLayout;

.field private mIsEnabled:Z

.field private mLayout:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutResId"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 20
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mButtonId:I

    .line 178
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 179
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mContext:Landroid/content/Context;

    .line 180
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mIsEnabled:Z

    .line 181
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mLayout:Landroid/view/ViewGroup;

    .line 25
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mContext:Landroid/content/Context;

    .line 26
    invoke-static {p1, p2, p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mLayout:Landroid/view/ViewGroup;

    .line 28
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mButtonId:I

    .line 30
    return-void
.end method


# virtual methods
.method public configurationChanged()V
    .locals 2

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->removeAllViews()V

    .line 174
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mButtonId:I

    invoke-static {v0, v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mLayout:Landroid/view/ViewGroup;

    .line 177
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mContext:Landroid/content/Context;

    .line 54
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 55
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 56
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mLayout:Landroid/view/ViewGroup;

    .line 57
    return-void
.end method

.method public init(IIILjava/lang/String;)V
    .locals 6
    .param p1, "buttonType"    # I
    .param p2, "iconId"    # I
    .param p3, "selectIconId"    # I
    .param p4, "text"    # Ljava/lang/String;

    .prologue
    .line 84
    const v3, 0x7f090004

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 85
    .local v0, "icon":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 89
    :cond_0
    const v3, 0x7f090005

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 90
    .local v2, "tView":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    if-eqz p4, :cond_1

    .line 92
    invoke-virtual {v2, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    :cond_1
    const v3, 0x7f090074

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 95
    .local v1, "l":Landroid/widget/LinearLayout;
    if-eqz v1, :cond_2

    .line 97
    invoke-virtual {v1, p3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 98
    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 100
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->setId(I)V

    .line 101
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030021

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 102
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mDivider:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->addView(Landroid/view/View;)V

    .line 103
    return-void
.end method

.method public init(IILjava/lang/String;)V
    .locals 5
    .param p1, "buttonType"    # I
    .param p2, "iconId"    # I
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    const v4, 0x7f030021

    const/4 v3, 0x0

    .line 60
    const v2, 0x7f090004

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 61
    .local v0, "icon":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 65
    :cond_0
    const v2, 0x7f090005

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 66
    .local v1, "tView":Landroid/widget/TextView;
    if-eqz v1, :cond_1

    if-eqz p3, :cond_1

    .line 68
    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->setId(I)V

    .line 71
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 72
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v2, :cond_3

    .line 74
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 80
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mDivider:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->addView(Landroid/view/View;)V

    .line 81
    return-void

    .line 76
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    if-eqz v2, :cond_2

    .line 78
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mDivider:Landroid/widget/LinearLayout;

    goto :goto_0
.end method

.method public isFocused()Z
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mLayout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    return v0
.end method

.method public removeDivider()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mDivider:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 106
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 155
    if-eqz p1, :cond_1

    .line 157
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->mIsEnabled:Z

    if-nez v0, :cond_0

    .line 158
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setSelected(Z)V
    .locals 3
    .param p1, "selected"    # Z

    .prologue
    const/4 v2, 0x0

    .line 110
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 111
    if-eqz p1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->setEnabled(Z)V

    .line 113
    const v1, 0x7f090074

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 114
    .local v0, "l":Landroid/widget/LinearLayout;
    if-eqz p1, :cond_2

    .line 116
    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 129
    :cond_0
    :goto_1
    return-void

    .line 111
    .end local v0    # "l":Landroid/widget/LinearLayout;
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 124
    .restart local v0    # "l":Landroid/widget/LinearLayout;
    :cond_2
    if-eqz v0, :cond_0

    .line 126
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method
