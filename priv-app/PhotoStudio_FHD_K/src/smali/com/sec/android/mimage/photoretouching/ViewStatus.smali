.class public Lcom/sec/android/mimage/photoretouching/ViewStatus;
.super Ljava/lang/Object;
.source "ViewStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;
    }
.end annotation


# static fields
.field public static final ADJUSTMENT_VIEW:I = 0x11000000

.field public static final CLIPBOARD_VIEW:I = 0x17000000

.field public static final COLLAGE_VIEW:I = 0x1e110000

.field public static final COLOR_VIEW:I = 0x15000000

.field public static final COPY_TO_VIEW:I = 0x1d000000

.field public static final CROP_VIEW:I = 0x11200000

.field public static final DECORATION_VIEW:I = 0x31000000

.field public static final EFFECT_VIEW:I = 0x16000000

.field public static final FRAME_VIEW:I = 0x31200000

.field public static final IMAGE_STICKER_VIEW:I = 0x31600000

.field public static final INIT_COLLAGE_VIEW:I = 0x2c000000

.field public static final INIT_MOTION_PHOTO_VIEW:I = 0x2f000000

.field public static final INIT_NOIMAGE_VIEW:I = 0x2b000000

.field public static final INIT_NORMAL_VIEW:I = 0x20000000

.field public static final KILL_VIEW:I = 0x2e000000

.field public static final LABEL_VIEW:I = 0x31300000

.field public static final LAUNCHER_VIEW:I = 0x2d000000

.field public static final MULTIGRID_2_SPLIT_VIEW:I = 0x1e110000

.field public static final MULTIGRID_3_SPLIT_VIEW:I = 0x1e120000

.field public static final MULTIGRID_4_SPLIT_VIEW:I = 0x1e130000

.field public static final MULTIGRID_5_SPLIT_VIEW:I = 0x1e140000

.field public static final MULTIGRID_6_SPLIT_VIEW:I = 0x1e150000

.field public static final MULTIGRID_EDIT_VIEW:I = 0x1f000000

.field public static final MULTIGRID_SPLIT_VIEW:I = 0x1e000000

.field public static final NOIMAGE_VIEW:I = 0x1c000000

.field public static final NORMAL_VIEW:I = 0x10000000

.field public static final PEN_VIEW:I = 0x1a000000

.field public static final PORTRAIT_VIEW:I = 0x18000000

.field public static final RESIZE_VIEW:I = 0x11300000

.field public static final ROTATE_VIEW:I = 0x11100000

.field public static final SELECT_VIEW:I = 0x14000000

.field public static final STAMP_VIEW:I = 0x31500000

.field public static final STICKER_VIEW:I = 0x31100000

.field public static final WATERMARK_VIEW:I = 0x31400000

.field private static mCurrentMode:I

.field private static mDisplayHeightBasedOnLand:I

.field private static mDisplayWidthBasedOnLand:I

.field private static mIsLandscape:Z

.field private static mIsStartActivity:Z

.field private static mPreviousMode:I

.field private static mSavedAndDestroy:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 906
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mIsLandscape:Z

    .line 907
    sput-boolean v1, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mSavedAndDestroy:Z

    .line 908
    sput-boolean v1, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mIsStartActivity:Z

    .line 909
    sput v1, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mDisplayWidthBasedOnLand:I

    .line 910
    sput v1, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mDisplayHeightBasedOnLand:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCurrentMode()I
    .locals 1

    .prologue
    .line 42
    sget v0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mCurrentMode:I

    return v0
.end method

.method public static getDisplayHeightBasedOnLand()I
    .locals 1

    .prologue
    .line 83
    sget v0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mDisplayHeightBasedOnLand:I

    return v0
.end method

.method public static getDisplayWidthBasedOnLand()I
    .locals 1

    .prologue
    .line 79
    sget v0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mDisplayWidthBasedOnLand:I

    return v0
.end method

.method public static getPreviousMode()I
    .locals 1

    .prologue
    .line 46
    sget v0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mPreviousMode:I

    return v0
.end method

.method public static getSavedAndDestroy()Z
    .locals 1

    .prologue
    .line 63
    sget-boolean v0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mSavedAndDestroy:Z

    return v0
.end method

.method public static getStartNormalView()Z
    .locals 1

    .prologue
    .line 75
    sget-boolean v0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mIsStartActivity:Z

    return v0
.end method

.method public static isLandscape()Z
    .locals 1

    .prologue
    .line 60
    sget-boolean v0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mIsLandscape:Z

    return v0
.end method

.method public static setCurrentMode(I)V
    .locals 1
    .param p0, "mode"    # I

    .prologue
    .line 18
    const v0, 0xffff

    and-int/2addr v0, p0

    if-eqz v0, :cond_0

    .line 20
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 28
    :goto_0
    return-void

    .line 24
    :cond_0
    sget v0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mCurrentMode:I

    sput v0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mPreviousMode:I

    .line 25
    sput p0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mCurrentMode:I

    .line 26
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    goto :goto_0
.end method

.method public static setCurrentModeForActivity(I)V
    .locals 0
    .param p0, "mode"    # I

    .prologue
    .line 32
    sput p0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mCurrentMode:I

    .line 33
    return-void
.end method

.method public static setCurrentSubModeForActivity(I)V
    .locals 0
    .param p0, "subMode"    # I

    .prologue
    .line 37
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 38
    return-void
.end method

.method public static setDisplaySize(II)V
    .locals 0
    .param p0, "width"    # I
    .param p1, "height"    # I

    .prologue
    .line 87
    sput p1, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mDisplayHeightBasedOnLand:I

    .line 88
    sput p0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mDisplayWidthBasedOnLand:I

    .line 89
    return-void
.end method

.method public static setLandscape()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mIsLandscape:Z

    .line 51
    return-void
.end method

.method public static setPortrait()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mIsLandscape:Z

    .line 56
    return-void
.end method

.method public static setSaveAndDestroy(Z)V
    .locals 0
    .param p0, "flag"    # Z

    .prologue
    .line 66
    sput-boolean p0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mSavedAndDestroy:Z

    .line 67
    return-void
.end method

.method public static setStartNormalView(Z)V
    .locals 0
    .param p0, "isStartActivity"    # Z

    .prologue
    .line 71
    sput-boolean p0, Lcom/sec/android/mimage/photoretouching/ViewStatus;->mIsStartActivity:Z

    .line 72
    return-void
.end method
