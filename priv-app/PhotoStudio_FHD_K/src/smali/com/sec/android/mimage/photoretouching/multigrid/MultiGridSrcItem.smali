.class public Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
.super Ljava/lang/Object;
.source "MultiGridSrcItem.java"


# static fields
.field public static final DEFAULT_ZOOM_RATIO:F = 0.5f


# instance fields
.field private final MAX_ZOOM_RATIO:F

.field private final MIN_ZOOM_RATIO:F

.field private final PINCHZOOM_RATIO:F

.field private mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

.field private mDrawRect:Landroid/graphics/Rect;

.field private mIsFromPersonalPage:Z

.field private mOrgBitmap:Landroid/graphics/Bitmap;

.field private mOrgRect:Landroid/graphics/Rect;

.field private mOrgSrcHeight:I

.field private mOrgSrcWidth:I

.field private mPersonalPagePath:Ljava/lang/String;

.field private mPrivateSaveFolder:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->MIN_ZOOM_RATIO:F

    .line 445
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->MAX_ZOOM_RATIO:F

    .line 448
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->PINCHZOOM_RATIO:F

    .line 450
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 452
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    .line 455
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcWidth:I

    .line 456
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcHeight:I

    .line 458
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    .line 459
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    .line 461
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mIsFromPersonalPage:Z

    .line 462
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mPersonalPagePath:Ljava/lang/String;

    .line 463
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mPrivateSaveFolder:Ljava/lang/String;

    .line 163
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "orgImage"    # Landroid/graphics/Bitmap;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->MIN_ZOOM_RATIO:F

    .line 445
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->MAX_ZOOM_RATIO:F

    .line 448
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->PINCHZOOM_RATIO:F

    .line 450
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 452
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    .line 455
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcWidth:I

    .line 456
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcHeight:I

    .line 458
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    .line 459
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    .line 461
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mIsFromPersonalPage:Z

    .line 462
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mPersonalPagePath:Ljava/lang/String;

    .line 463
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mPrivateSaveFolder:Ljava/lang/String;

    .line 167
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setOrgImage(Landroid/graphics/Bitmap;)V

    .line 172
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->MIN_ZOOM_RATIO:F

    .line 445
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->MAX_ZOOM_RATIO:F

    .line 448
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->PINCHZOOM_RATIO:F

    .line 450
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 452
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    .line 455
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcWidth:I

    .line 456
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcHeight:I

    .line 458
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    .line 459
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    .line 461
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mIsFromPersonalPage:Z

    .line 462
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mPersonalPagePath:Ljava/lang/String;

    .line 463
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mPrivateSaveFolder:Ljava/lang/String;

    .line 176
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 178
    return-void
.end method

.method private checkRectInOrg(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 4
    .param p1, "tempRect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v3, 0x0

    .line 46
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->checkRectSizeInOrg(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object p1

    .line 47
    const/4 v0, 0x0

    .line 49
    .local v0, "diff":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    invoke-virtual {v1, p1}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 51
    iget v1, p1, Landroid/graphics/Rect;->left:I

    if-gez v1, :cond_0

    .line 53
    iget v1, p1, Landroid/graphics/Rect;->right:I

    iget v2, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    iput v1, p1, Landroid/graphics/Rect;->right:I

    .line 54
    iput v3, p1, Landroid/graphics/Rect;->left:I

    .line 57
    :cond_0
    iget v1, p1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-le v1, v2, :cond_1

    .line 59
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget v2, p1, Landroid/graphics/Rect;->right:I

    sub-int v0, v1, v2

    .line 60
    iget v1, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v0

    iput v1, p1, Landroid/graphics/Rect;->left:I

    .line 61
    iget v1, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v0

    iput v1, p1, Landroid/graphics/Rect;->right:I

    .line 64
    :cond_1
    iget v1, p1, Landroid/graphics/Rect;->top:I

    if-gez v1, :cond_2

    .line 66
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    iput v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 67
    iput v3, p1, Landroid/graphics/Rect;->top:I

    .line 70
    :cond_2
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-le v1, v2, :cond_3

    .line 72
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v1, v2

    .line 73
    iget v1, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v0

    iput v1, p1, Landroid/graphics/Rect;->top:I

    .line 74
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v0

    iput v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 78
    :cond_3
    return-object p1
.end method

.method private checkRectSizeInOrg(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 8
    .param p1, "drawRect"    # Landroid/graphics/Rect;

    .prologue
    .line 84
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v3, v4

    .line 85
    .local v3, "srcWidth":F
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v2, v4

    .line 87
    .local v2, "srcHeight":F
    const/4 v1, 0x0

    .line 88
    .local v1, "newWidth":F
    const/4 v0, 0x0

    .line 90
    .local v0, "newHeight":F
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcWidth:I

    if-le v4, v5, :cond_0

    .line 92
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcWidth:I

    int-to-float v1, v4

    .line 93
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcWidth:I

    int-to-float v4, v4

    mul-float/2addr v4, v2

    div-float v0, v4, v3

    .line 95
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 96
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 97
    iget v6, p1, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    add-float/2addr v6, v1

    float-to-int v6, v6

    .line 98
    iget v7, p1, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    add-float/2addr v7, v0

    float-to-int v7, v7

    .line 95
    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 101
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcHeight:I

    if-le v4, v5, :cond_1

    .line 103
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcHeight:I

    int-to-float v0, v4

    .line 104
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcHeight:I

    int-to-float v4, v4

    mul-float/2addr v4, v3

    div-float v1, v4, v2

    .line 106
    iget v4, p1, Landroid/graphics/Rect;->left:I

    .line 107
    iget v5, p1, Landroid/graphics/Rect;->top:I

    .line 108
    iget v6, p1, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    add-float/2addr v6, v1

    float-to-int v6, v6

    .line 109
    iget v7, p1, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    add-float/2addr v7, v0

    float-to-int v7, v7

    .line 106
    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 111
    :cond_1
    return-object p1
.end method

.method private setDefaultDrawRect()V
    .locals 7

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    .line 143
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcWidth:I

    int-to-float v4, v4

    mul-float/2addr v4, v5

    float-to-int v1, v4

    .line 144
    .local v1, "defaultWidth":I
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcHeight:I

    int-to-float v4, v4

    mul-float/2addr v4, v5

    float-to-int v0, v4

    .line 145
    .local v0, "defaultHeight":I
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcWidth:I

    sub-int/2addr v4, v1

    div-int/lit8 v2, v4, 0x2

    .line 146
    .local v2, "leftPadding":I
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcHeight:I

    sub-int/2addr v4, v0

    div-int/lit8 v3, v4, 0x2

    .line 148
    .local v3, "topPadding":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    if-nez v4, :cond_0

    .line 150
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    .line 153
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    .line 155
    add-int v5, v2, v1

    .line 156
    add-int v6, v3, v0

    .line 153
    invoke-virtual {v4, v2, v3, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 157
    return-void
.end method

.method private setOrgImgRect()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 130
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 132
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 139
    :cond_2
    return-void
.end method


# virtual methods
.method public checkRectInOrg(IIII)Landroid/graphics/Rect;
    .locals 2
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 311
    const/4 v0, 0x0

    .line 312
    .local v0, "diff":I
    if-gez p1, :cond_0

    .line 314
    rsub-int/lit8 v0, p1, 0x0

    .line 315
    const/4 p1, 0x0

    .line 316
    add-int/2addr p3, v0

    .line 319
    :cond_0
    if-gez p2, :cond_1

    .line 321
    rsub-int/lit8 v0, p2, 0x0

    .line 322
    const/4 p2, 0x0

    .line 323
    add-int/2addr p4, v0

    .line 326
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    if-le p3, v1, :cond_2

    .line 328
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int v0, v1, p3

    .line 329
    add-int/2addr p1, v0

    .line 330
    add-int/2addr p3, v0

    .line 333
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    if-le p4, v1, :cond_3

    .line 335
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v1, p4

    .line 336
    add-int/2addr p2, v0

    .line 337
    add-int/2addr p4, v0

    .line 340
    :cond_3
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, p1, p2, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v1
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    .line 243
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    .line 246
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->thumbnail:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->thumbnail:Landroid/graphics/Bitmap;

    .line 249
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 250
    return-void
.end method

.method public determinePersonalPage(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 374
    const/4 v9, 0x0

    .line 376
    .local v9, "path":Ljava/lang/String;
    if-nez p2, :cond_1

    .line 430
    .end local p2    # "object":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 379
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_1
    instance-of v0, p2, Landroid/net/Uri;

    if-eqz v0, :cond_7

    .line 381
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "sj, MGSI - determinePersonalPage() - uri : "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v0, p2

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 382
    const/4 v6, 0x0

    .line 383
    .local v6, "c":Landroid/database/Cursor;
    if-eqz p1, :cond_2

    .line 384
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p2

    check-cast v1, Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 385
    :cond_2
    if-nez v6, :cond_6

    .line 387
    check-cast p2, Landroid/net/Uri;

    .end local p2    # "object":Ljava/lang/Object;
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    .line 405
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_3
    :goto_1
    if-eqz v9, :cond_0

    .line 407
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mIsFromPersonalPage:Z

    if-eqz v0, :cond_0

    .line 409
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    .line 412
    .local v8, "lastFileSeparatorIdx":I
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v0

    if-le v8, v0, :cond_4

    .line 413
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v8

    .line 414
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {v9, v0, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mPersonalPagePath:Ljava/lang/String;

    .line 417
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mPersonalPagePath:Ljava/lang/String;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v8, v0, 0x1

    .line 418
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mPersonalPagePath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v8, v0, :cond_5

    .line 419
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mPersonalPagePath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    .line 420
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mPersonalPagePath:Ljava/lang/String;

    const-string v1, "/storage/Private"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    const-string v0, "Private"

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mPrivateSaveFolder:Ljava/lang/String;

    goto :goto_0

    .line 391
    .end local v8    # "lastFileSeparatorIdx":I
    .restart local v6    # "c":Landroid/database/Cursor;
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_6
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 392
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 394
    .local v7, "idx":I
    if-ltz v7, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 395
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 397
    goto :goto_1

    .line 398
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v7    # "idx":I
    :cond_7
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_3

    move-object v9, p2

    .line 400
    check-cast v9, Ljava/lang/String;

    goto :goto_1
.end method

.method public getDecodeInfo()Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    return-object v0
.end method

.method public getOrgHeight()I
    .locals 1

    .prologue
    .line 369
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcHeight:I

    return v0
.end method

.method public getOrgImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getOrgRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getOrgSizeInPixel()I
    .locals 2

    .prologue
    .line 359
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcWidth:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcHeight:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public getOrgWidth()I
    .locals 1

    .prologue
    .line 364
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcWidth:I

    return v0
.end method

.method public getPersonalPagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mPersonalPagePath:Ljava/lang/String;

    return-object v0
.end method

.method public getPrivateSaveFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mPrivateSaveFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->getUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getZoomRatio()F
    .locals 5

    .prologue
    .line 257
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float v1, v3, v4

    .line 258
    .local v1, "widthRatio":F
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 259
    .local v0, "heightRatio":F
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 261
    .local v2, "zoomRatio":F
    return v2
.end method

.method public getmDrawRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public isPersonalPage()Z
    .locals 1

    .prologue
    .line 433
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mIsFromPersonalPage:Z

    return v0
.end method

.method public moveDrawRect(II)V
    .locals 5
    .param p1, "diffRight"    # I
    .param p2, "diffBottom"    # I

    .prologue
    .line 290
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    if-nez v4, :cond_0

    .line 299
    :goto_0
    return-void

    .line 293
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int v1, v4, p1

    .line 294
    .local v1, "mDrawLeft":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int v2, v4, p1

    .line 295
    .local v2, "mDrawRight":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int v3, v4, p2

    .line 296
    .local v3, "mDrawTop":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v4, p2

    .line 298
    .local v0, "mDrawBottom":I
    invoke-virtual {p0, v1, v3, v2, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->checkRectInOrg(IIII)Landroid/graphics/Rect;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public pinchZoom(I)V
    .locals 12
    .param p1, "diffDst"    # I

    .prologue
    const/high16 v11, 0x3f800000    # 1.0f

    .line 266
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    mul-int/2addr v6, p1

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int v0, v6, v7

    .line 268
    .local v0, "diffY":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    int-to-float v7, p1

    mul-float/2addr v7, v11

    add-float v2, v6, v7

    .line 269
    .local v2, "mDrawLeft":F
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    int-to-float v7, p1

    mul-float/2addr v7, v11

    sub-float v3, v6, v7

    .line 270
    .local v3, "mDrawRight":F
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    int-to-float v7, v0

    mul-float/2addr v7, v11

    add-float v4, v6, v7

    .line 271
    .local v4, "mDrawTop":F
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v6

    int-to-float v7, v0

    mul-float/2addr v7, v11

    sub-float v1, v6, v7

    .line 273
    .local v1, "mDrawBottom":F
    new-instance v5, Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    invoke-direct {v5, v6}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 275
    .local v5, "mPrevRect":Landroid/graphics/Rect;
    new-instance v6, Landroid/graphics/Rect;

    float-to-int v7, v2

    float-to-int v8, v4

    float-to-int v9, v3

    float-to-int v10, v1

    invoke-direct {v6, v7, v8, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setmDrawRect(Landroid/graphics/Rect;)V

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getZoomRatio()F

    move-result v6

    const/high16 v7, 0x40400000    # 3.0f

    cmpl-float v6, v6, v7

    if-gtz v6, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getZoomRatio()F

    move-result v6

    cmpg-float v6, v6, v11

    if-gtz v6, :cond_1

    .line 278
    :cond_0
    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    .line 280
    :cond_1
    return-void
.end method

.method public setDecodeInfo(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;)V
    .locals 0
    .param p1, "mDInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 194
    return-void
.end method

.method public setOrgImage()V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    if-nez v0, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    .line 208
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcWidth:I

    .line 211
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcHeight:I

    .line 212
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setOrgImgRect()V

    .line 213
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setDefaultDrawRect()V

    goto :goto_0
.end method

.method public setOrgImage(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "orgImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 219
    if-nez p1, :cond_1

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 224
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    .line 225
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    .line 228
    :cond_2
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    .line 229
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDecInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    .line 231
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcWidth:I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcHeight:I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 233
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcWidth:I

    .line 234
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mOrgSrcHeight:I

    .line 235
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setOrgImgRect()V

    .line 236
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setDefaultDrawRect()V

    goto :goto_0
.end method

.method public setmDrawRect(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "drawRect"    # Landroid/graphics/Rect;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->checkRectInOrg(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->mDrawRect:Landroid/graphics/Rect;

    .line 41
    return-void
.end method
