.class Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$10;
.super Ljava/lang/Object;
.source "CopyToView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->initSaveAsDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    .line 710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 714
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 716
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 717
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 718
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    .line 720
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 721
    return-void
.end method
