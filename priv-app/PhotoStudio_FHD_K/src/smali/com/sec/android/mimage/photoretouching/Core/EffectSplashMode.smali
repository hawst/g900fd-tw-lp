.class public Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;
.super Lcom/sec/android/mimage/photoretouching/Core/EffectInfoInterface;
.source "EffectSplashMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;
    }
.end annotation


# instance fields
.field final MAX_RADIUS_PIXELIZE:I

.field final MIN_RADIUS_PIXELIZE:I

.field private mContext:Landroid/content/Context;

.field private mEffectType:I

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

.field private mStartInitializing:Z

.field private mutexOn:Z

.field private myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

.field private myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onCallback"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    .param p3, "onActionbarCallback"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;
    .param p4, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 22
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfoInterface;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 373
    const/16 v0, 0x19

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->MIN_RADIUS_PIXELIZE:I

    .line 374
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->MAX_RADIUS_PIXELIZE:I

    .line 376
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mEffectType:I

    .line 378
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mStartInitializing:Z

    .line 379
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mutexOn:Z

    .line 381
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mContext:Landroid/content/Context;

    .line 382
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    .line 383
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    .line 385
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    .line 387
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 24
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    .line 25
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    .line 26
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 27
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mContext:Landroid/content/Context;

    .line 28
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method private applySplashPreview(Landroid/view/MotionEvent;[I)V
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "output"    # [I

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    .line 217
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 218
    .local v2, "tempROI":Landroid/graphics/Rect;
    iput v5, v2, Landroid/graphics/Rect;->left:I

    .line 219
    iput v5, v2, Landroid/graphics/Rect;->top:I

    .line 220
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    iput v5, v2, Landroid/graphics/Rect;->right:I

    .line 221
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    iput v5, v2, Landroid/graphics/Rect;->bottom:I

    .line 223
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-boolean v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->initSplash:Z

    if-nez v5, :cond_0

    .line 225
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempOriginal:[I

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    .line 226
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v7

    .line 225
    invoke-static {v5, v6, v7, v2}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->storeObject([IIILandroid/graphics/Rect;)I

    .line 227
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    .line 228
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v8

    .line 227
    invoke-static {v5, v6, v7, v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->setGrayInSplash([I[III)I

    .line 229
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempOriginal:[I

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->initSplash([III)I

    .line 230
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->invalidate()V

    .line 231
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iput-boolean v9, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->initSplash:Z

    .line 234
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 235
    .local v3, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 236
    .local v4, "y":F
    const/4 v1, 0x0

    .line 237
    .local v1, "t":Ljava/lang/Thread;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v5

    float-to-int v6, v3

    float-to-int v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 239
    invoke-direct {p0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->getRealPosition(FF)Landroid/graphics/Point;

    move-result-object v0

    .line 240
    .local v0, "point":Landroid/graphics/Point;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 291
    .end local v0    # "point":Landroid/graphics/Point;
    :cond_1
    :goto_0
    return-void

    .line 245
    .restart local v0    # "point":Landroid/graphics/Point;
    :pswitch_0
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mutexOn:Z

    .line 246
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;

    .end local v1    # "t":Ljava/lang/Thread;
    invoke-direct {v1, p0, v0, p2}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$2;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;Landroid/graphics/Point;[I)V

    .line 267
    .restart local v1    # "t":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 271
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v5, :cond_1

    .line 273
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    invoke-interface {v5, v9}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    goto :goto_0

    .line 280
    .end local v0    # "point":Landroid/graphics/Point;
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_1

    :pswitch_2
    goto :goto_0

    .line 284
    :pswitch_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v5, :cond_1

    .line 286
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    invoke-interface {v5, v9}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    goto :goto_0

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 280
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getRealPosition(FF)Landroid/graphics/Point;
    .locals 11
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 294
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 295
    .local v4, "p":Landroid/graphics/Point;
    const/4 v0, 0x0

    .line 296
    .local v0, "drawCanvasRoi":Landroid/graphics/Rect;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v6, :cond_0

    .line 297
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v0

    .line 299
    float-to-int v6, p1

    float-to-int v7, p2

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 301
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    .line 302
    .local v3, "m":Landroid/graphics/Matrix;
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 303
    .local v2, "i":Landroid/graphics/Matrix;
    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 304
    new-array v5, v10, [F

    .line 305
    .local v5, "src":[F
    new-array v1, v10, [F

    .line 306
    .local v1, "dst":[F
    aput p1, v5, v8

    aput p2, v5, v9

    .line 307
    invoke-virtual {v2, v1, v5}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 308
    aget v6, v1, v8

    float-to-int v6, v6

    iput v6, v4, Landroid/graphics/Point;->x:I

    aget v6, v1, v9

    float-to-int v6, v6

    iput v6, v4, Landroid/graphics/Point;->y:I

    .line 312
    .end local v1    # "dst":[F
    .end local v2    # "i":Landroid/graphics/Matrix;
    .end local v3    # "m":Landroid/graphics/Matrix;
    .end local v5    # "src":[F
    :cond_0
    return-object v4
.end method

.method private initSplash()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 32
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    .line 33
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v7

    mul-int/2addr v6, v7

    new-array v6, v6, [I

    iput-object v6, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempOriginal:[I

    .line 34
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    mul-int/2addr v6, v7

    new-array v6, v6, [I

    iput-object v6, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    .line 35
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v5

    .line 37
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempOriginal:[I

    .line 39
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v7, v7, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempOriginal:[I

    array-length v7, v7

    .line 35
    invoke-static {v5, v8, v6, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 41
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    if-lt v0, v5, :cond_1

    .line 56
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 57
    .local v3, "tempROI":Landroid/graphics/Rect;
    iput v8, v3, Landroid/graphics/Rect;->left:I

    .line 58
    iput v8, v3, Landroid/graphics/Rect;->top:I

    .line 59
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    iput v5, v3, Landroid/graphics/Rect;->right:I

    .line 60
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    iput v5, v3, Landroid/graphics/Rect;->bottom:I

    .line 62
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-boolean v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->initSplash:Z

    if-nez v5, :cond_0

    .line 64
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempOriginal:[I

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    .line 65
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v7

    .line 64
    invoke-static {v5, v6, v7, v3}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->storeObject([IIILandroid/graphics/Rect;)I

    .line 66
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempOriginal:[I

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->initSplash([III)I

    .line 67
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    .line 68
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v8

    .line 67
    invoke-static {v5, v6, v7, v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->setGrayInSplash([I[III)I

    .line 69
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->initSplash:Z

    .line 72
    :cond_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v4

    .line 74
    .local v4, "viewBuffer":[I
    const/4 v0, 0x0

    :goto_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    if-lt v0, v5, :cond_2

    .line 87
    return-void

    .line 43
    .end local v3    # "tempROI":Landroid/graphics/Rect;
    .end local v4    # "viewBuffer":[I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v5

    .line 44
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v6

    add-int/2addr v6, v0

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v7

    mul-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v7

    add-int/2addr v6, v7

    .line 45
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    .line 43
    invoke-static {v5, v6, v7}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v1

    .line 46
    .local v1, "inputBuffer":Ljava/nio/IntBuffer;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    .line 47
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    mul-int/2addr v6, v0

    .line 48
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    .line 46
    invoke-static {v5, v6, v7}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v2

    .line 50
    .local v2, "outputBuffer":Ljava/nio/IntBuffer;
    invoke-virtual {v2, v1}, Ljava/nio/IntBuffer;->put(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;

    .line 52
    const/4 v1, 0x0

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 76
    .end local v1    # "inputBuffer":Ljava/nio/IntBuffer;
    .end local v2    # "outputBuffer":Ljava/nio/IntBuffer;
    .restart local v3    # "tempROI":Landroid/graphics/Rect;
    .restart local v4    # "viewBuffer":[I
    :cond_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    .line 77
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    mul-int/2addr v6, v0

    .line 78
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    .line 76
    invoke-static {v5, v6, v7}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v1

    .line 80
    .restart local v1    # "inputBuffer":Ljava/nio/IntBuffer;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v5

    add-int/2addr v5, v0

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v6

    mul-int/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v6

    add-int/2addr v5, v6

    .line 81
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 79
    invoke-static {v4, v5, v6}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v2

    .line 82
    .restart local v2    # "outputBuffer":Ljava/nio/IntBuffer;
    invoke-virtual {v2, v1}, Ljava/nio/IntBuffer;->put(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;

    .line 83
    const/4 v1, 0x0

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1
.end method


# virtual methods
.method public applyOriginal()[I
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 322
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v6, :cond_1

    .line 324
    const/4 v3, 0x0

    .line 325
    .local v3, "output":[I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v2

    .line 326
    .local v2, "mask":[B
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v1

    .line 327
    .local v1, "input":[I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    .line 328
    .local v5, "w":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 329
    .local v0, "h":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v4

    .line 331
    .local v4, "r":Landroid/graphics/Rect;
    mul-int v6, v5, v0

    new-array v3, v6, [I

    .line 332
    array-length v6, v3

    invoke-static {v1, v8, v3, v8, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 334
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->deleteStoredObject()I

    .line 335
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->finishSplash()I

    .line 336
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iput-boolean v8, v6, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->initSplash:Z

    .line 337
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempOriginal:[I

    mul-int v7, v5, v0

    invoke-static {v6, v8, v3, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 339
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v6, :cond_0

    .line 340
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([I)V

    .line 341
    :cond_0
    const-string v6, "applyOriginal end"

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 344
    .end local v0    # "h":I
    .end local v1    # "input":[I
    .end local v2    # "mask":[B
    .end local v3    # "output":[I
    .end local v4    # "r":Landroid/graphics/Rect;
    .end local v5    # "w":I
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public applyPreview()I
    .locals 1

    .prologue
    .line 354
    const/4 v0, -0x1

    return v0
.end method

.method public configurationChange()V
    .locals 12

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v2

    .line 131
    .local v2, "w":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    .line 133
    .local v3, "h":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 134
    .local v4, "pw":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    .line 136
    .local v5, "ph":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v10

    .line 138
    .local v10, "viewWidth":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v9

    .line 140
    .local v9, "previewOutput":[I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v2

    .line 141
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    .line 143
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 144
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v9

    .line 148
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    if-eqz v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v11

    mul-int/2addr v1, v11

    new-array v1, v1, [I

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    .line 154
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempOriginal:[I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->resizeBuff([I[IIIII)V

    .line 156
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    if-lt v6, v0, :cond_2

    .line 169
    .end local v6    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->afterApplyPreview()V

    .line 170
    return-void

    .line 158
    .restart local v6    # "i":I
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    .line 159
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    mul-int/2addr v1, v6

    .line 160
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v11

    .line 158
    invoke-static {v0, v1, v11}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v7

    .line 162
    .local v7, "inputBuffer":Ljava/nio/IntBuffer;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v0

    add-int/2addr v0, v6

    mul-int/2addr v0, v10

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    .line 161
    invoke-static {v9, v0, v1}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v8

    .line 164
    .local v8, "outputBuffer":Ljava/nio/IntBuffer;
    invoke-virtual {v8, v7}, Ljava/nio/IntBuffer;->put(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;

    .line 165
    const/4 v7, 0x0

    .line 156
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 180
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->destroy()V

    .line 181
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    .line 183
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 184
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 119
    return-void
.end method

.method public getBrushSize()I
    .locals 2

    .prologue
    .line 122
    const/4 v0, 0x0

    .line 123
    .local v0, "ret":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    if-eqz v1, :cond_0

    .line 124
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    iget v1, v1, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mRadius:F

    float-to-int v0, v1

    .line 125
    :cond_0
    return v0
.end method

.method public getMaxBrushSize()F
    .locals 2

    .prologue
    .line 187
    const/4 v0, 0x0

    .line 188
    .local v0, "ret":F
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    if-eqz v1, :cond_0

    .line 189
    const/high16 v0, 0x43480000    # 200.0f

    .line 191
    :cond_0
    return v0
.end method

.method public getMutexOn()Z
    .locals 1

    .prologue
    .line 317
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mutexOn:Z

    return v0
.end method

.method public init(I)V
    .locals 4
    .param p1, "effectType"    # I

    .prologue
    const/4 v3, 0x0

    .line 93
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mStartInitializing:Z

    .line 94
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    invoke-interface {v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    .line 98
    :cond_0
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mEffectType:I

    .line 99
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 100
    .local v0, "viewBuffer":[I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 102
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->initSplash()V

    .line 104
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$1;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 112
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mStartInitializing:Z

    .line 113
    return-void
.end method

.method public isStartInitializing()Z
    .locals 1

    .prologue
    .line 349
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mStartInitializing:Z

    return v0
.end method

.method public setBrushSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mSplashInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;

    int-to-float v1, p1

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mRadius:F

    .line 197
    :cond_0
    return-void
.end method

.method public setStep(I)V
    .locals 0
    .param p1, "step"    # I

    .prologue
    .line 176
    return-void
.end method

.method public touch(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 201
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v3, :cond_0

    .line 202
    const/4 v3, 0x0

    .line 213
    :goto_0
    return v3

    .line 204
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v2

    .line 206
    .local v2, "viewBuffer":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 207
    .local v1, "m":Landroid/graphics/Matrix;
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 208
    .local v0, "inversM":Landroid/graphics/Matrix;
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 209
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 211
    invoke-direct {p0, p1, v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;->applySplashPreview(Landroid/view/MotionEvent;[I)V

    .line 213
    const/4 v3, 0x1

    goto :goto_0
.end method
