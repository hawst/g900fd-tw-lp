.class Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$15;
.super Ljava/lang/Object;
.source "AdjustmentView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->init2DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$15;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    .line 677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 0
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 694
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 681
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$15;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 682
    .local v0, "display":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 683
    .local v2, "size":Landroid/graphics/Point;
    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 684
    iget v3, v2, Landroid/graphics/Point;->x:I

    .line 685
    .local v3, "width":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$15;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getWidth(I)I

    move-result v1

    .line 686
    .local v1, "left":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$15;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getWidth(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 687
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$15;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v4

    const/16 v5, 0xe

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getWidth(I)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v4

    .line 688
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 700
    return-void
.end method
