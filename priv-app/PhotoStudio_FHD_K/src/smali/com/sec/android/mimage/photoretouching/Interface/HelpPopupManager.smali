.class public Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;
.super Ljava/lang/Object;
.source "HelpPopupManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;,
        Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;
    }
.end annotation


# static fields
.field private static final helpPopUpPrefName:Ljava/lang/String; = "PhotoStudioHelpPopUp"

.field public static final mColorSubModeMaskedValue:I = 0x15001000

.field public static final mColorSubModeMaskingValue:I = -0x1000

.field public static final mEffectSubModeMaskedValue:I = 0x16001600

.field public static final mEffectSubModeMaskingValue:I = -0x100


# instance fields
.field private mCloseButtonClickListener:Landroid/view/View$OnClickListener;

.field private mCloseButtonKeyListener:Landroid/view/View$OnKeyListener;

.field private mContext:Landroid/content/Context;

.field private mCurrentMode:I

.field mHelpPopupCallback:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;

.field private mHelpPopupDialog:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;

.field private mIsEnablePopupFunction:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mode"    # I

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    .line 25
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mHelpPopupDialog:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;

    .line 26
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCloseButtonKeyListener:Landroid/view/View$OnKeyListener;

    .line 27
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCloseButtonClickListener:Landroid/view/View$OnClickListener;

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mIsEnablePopupFunction:Z

    .line 46
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mHelpPopupCallback:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;

    .line 51
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    .line 52
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    .line 53
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->initListener()V

    .line 54
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;Landroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 184
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->setHelpPopupCloseBtnListener(Landroid/widget/LinearLayout;)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V
    .locals 0

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->touchCancelButton()V

    return-void
.end method

.method private checkShowAnotherPopup()Z
    .locals 2

    .prologue
    .line 149
    const/4 v0, 0x0

    .line 150
    .local v0, "isNoShow":Z
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    sparse-switch v1, :sswitch_data_0

    .line 174
    const/4 v0, 0x1

    .line 178
    :goto_0
    if-nez v0, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->showCurrentModeHelpPopup()V

    .line 181
    :cond_0
    return v0

    .line 153
    :sswitch_0
    const v1, 0x16001600

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    goto :goto_0

    .line 156
    :sswitch_1
    const v1, 0x15001000

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    goto :goto_0

    .line 159
    :sswitch_2
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 168
    :pswitch_0
    const v1, 0x17001701

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    goto :goto_0

    .line 162
    :pswitch_1
    const v1, 0x17001702

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    goto :goto_0

    .line 165
    :pswitch_2
    const v1, 0x17001703

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    goto :goto_0

    .line 150
    nop

    :sswitch_data_0
    .sparse-switch
        0x15000000 -> :sswitch_1
        0x16000000 -> :sswitch_0
        0x18000000 -> :sswitch_2
    .end sparse-switch

    .line 159
    :pswitch_data_0
    .packed-switch 0x17001701
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static getPrefIdFromMode(I)Ljava/lang/String;
    .locals 2
    .param p0, "mode"    # I

    .prologue
    .line 282
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "id_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initCloseButtonClickListener()V
    .locals 1

    .prologue
    .line 306
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCloseButtonClickListener:Landroid/view/View$OnClickListener;

    .line 313
    return-void
.end method

.method private initCloseButtonKeyListener()V
    .locals 1

    .prologue
    .line 317
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCloseButtonKeyListener:Landroid/view/View$OnKeyListener;

    .line 338
    return-void
.end method

.method private initListener()V
    .locals 0

    .prologue
    .line 300
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->initCloseButtonKeyListener()V

    .line 301
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->initCloseButtonClickListener()V

    .line 302
    return-void
.end method

.method public static isDoNotShowPopup(Landroid/content/Context;I)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mode"    # I

    .prologue
    const/4 v3, 0x0

    .line 206
    const/4 v1, 0x0

    .line 208
    .local v1, "ret":Z
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, "id":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 217
    :goto_0
    return v3

    .line 213
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "PhotoStudioHelpPopUp"

    invoke-virtual {v4, v5, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 215
    .local v2, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    move v3, v1

    .line 217
    goto :goto_0
.end method

.method private setHelpPopupCloseBtnListener(Landroid/widget/LinearLayout;)V
    .locals 1
    .param p1, "l"    # Landroid/widget/LinearLayout;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCloseButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCloseButtonKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 188
    return-void
.end method

.method private touchCancelButton()V
    .locals 2

    .prologue
    .line 192
    const/4 v0, 0x0

    .line 193
    .local v0, "isNoShow":Z
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->setDoNotShowPopup()V

    .line 194
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mHelpPopupDialog:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;->dismiss()V

    .line 195
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mHelpPopupDialog:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;

    .line 196
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->checkShowAnotherPopup()Z

    move-result v0

    .line 197
    if-eqz v0, :cond_0

    .line 199
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mHelpPopupCallback:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;

    if-eqz v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mHelpPopupCallback:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;

    invoke-interface {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;->onPopupClosed()V

    .line 202
    :cond_0
    return-void
.end method


# virtual methods
.method public configurationChange()V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mHelpPopupDialog:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mHelpPopupDialog:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;->dismiss()V

    .line 290
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mHelpPopupDialog:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;

    .line 291
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 292
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mHelpPopupDialog:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;

    .line 293
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mHelpPopupDialog:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;->show()V

    .line 296
    :cond_0
    return-void
.end method

.method public getCurrentModeHelpPopup()Landroid/view/View;
    .locals 4

    .prologue
    const v2, 0x7f03005b

    const/4 v3, 0x0

    .line 58
    const/4 v0, 0x0

    .line 60
    .local v0, "ret":Landroid/view/View;
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    sparse-switch v1, :sswitch_data_0

    .line 131
    :goto_0
    return-object v0

    .line 63
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030060

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 64
    goto :goto_0

    .line 66
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03005f

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 67
    goto :goto_0

    .line 69
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03005c

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 70
    goto :goto_0

    .line 72
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030064

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 73
    goto :goto_0

    .line 75
    :sswitch_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030058

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 76
    goto :goto_0

    .line 78
    :sswitch_5
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030063

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 79
    goto :goto_0

    .line 81
    :sswitch_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030059

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 82
    goto :goto_0

    .line 84
    :sswitch_7
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03005e

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 86
    goto :goto_0

    .line 88
    :sswitch_8
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030065

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 89
    goto/16 :goto_0

    .line 91
    :sswitch_9
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030062

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 92
    goto/16 :goto_0

    .line 94
    :sswitch_a
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 95
    goto/16 :goto_0

    .line 97
    :sswitch_b
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 98
    goto/16 :goto_0

    .line 100
    :sswitch_c
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 109
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03005d

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 103
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030054

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 104
    goto/16 :goto_0

    .line 106
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030061

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 107
    goto/16 :goto_0

    .line 115
    :sswitch_d
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 116
    goto/16 :goto_0

    .line 118
    :sswitch_e
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030056

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 119
    goto/16 :goto_0

    .line 121
    :sswitch_f
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030055

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 122
    goto/16 :goto_0

    .line 124
    :sswitch_10
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 125
    goto/16 :goto_0

    .line 127
    :sswitch_11
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030057

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 60
    :sswitch_data_0
    .sparse-switch
        0x10000000 -> :sswitch_0
        0x11001108 -> :sswitch_1
        0x11100000 -> :sswitch_5
        0x11200000 -> :sswitch_6
        0x14000000 -> :sswitch_3
        0x15000000 -> :sswitch_4
        0x15001000 -> :sswitch_11
        0x16000000 -> :sswitch_2
        0x16001600 -> :sswitch_10
        0x17001700 -> :sswitch_9
        0x17001701 -> :sswitch_a
        0x17001702 -> :sswitch_b
        0x17001703 -> :sswitch_d
        0x18000000 -> :sswitch_c
        0x1e300002 -> :sswitch_f
        0x2c000000 -> :sswitch_e
        0x31200000 -> :sswitch_7
        0x31500000 -> :sswitch_8
    .end sparse-switch

    .line 100
    :pswitch_data_0
    .packed-switch 0x17001701
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setDoNotShowPopup()V
    .locals 11

    .prologue
    const v10, 0x17001702

    const v9, 0x17001701

    const/high16 v8, 0x16000000

    const/high16 v7, 0x15000000

    const/4 v6, 0x1

    .line 222
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v1

    .line 224
    .local v1, "id":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "PhotoStudioHelpPopUp"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 225
    .local v2, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 227
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 229
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    const/high16 v4, 0x31200000

    if-ne v3, v4, :cond_2

    .line 230
    const/high16 v3, 0x31500000

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 235
    :cond_0
    :goto_0
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    if-ne v3, v7, :cond_3

    .line 236
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 237
    const/high16 v3, 0x18000000

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 238
    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 239
    const v3, 0x17001703

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 240
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 277
    :cond_1
    :goto_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 278
    return-void

    .line 231
    :cond_2
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    const/high16 v4, 0x31500000

    if-ne v3, v4, :cond_0

    .line 232
    const/high16 v3, 0x31200000

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 241
    :cond_3
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    if-ne v3, v8, :cond_4

    .line 242
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 243
    const/high16 v3, 0x18000000

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 244
    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 245
    const v3, 0x17001703

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 246
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 249
    :cond_4
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    if-ne v3, v10, :cond_5

    .line 251
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 252
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 254
    const v3, 0x17001703

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 255
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 257
    :cond_5
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    const v4, 0x17001703

    if-ne v3, v4, :cond_6

    .line 259
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 260
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 262
    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 263
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_1

    .line 265
    :cond_6
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mCurrentMode:I

    if-ne v3, v9, :cond_1

    .line 267
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 268
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 270
    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 271
    const v3, 0x17001703

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getPrefIdFromMode(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_1
.end method

.method public setHelpPopupCallback(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mHelpPopupCallback:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;

    .line 45
    return-void
.end method

.method public showCurrentModeHelpPopup()V
    .locals 2

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mIsEnablePopupFunction:Z

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 140
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mHelpPopupDialog:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;

    .line 141
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mHelpPopupDialog:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;->show()V

    .line 144
    :cond_0
    return-void
.end method
