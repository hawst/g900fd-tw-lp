.class Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$13;
.super Ljava/lang/Object;
.source "RotateView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->initSaveAsDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    .line 1386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 1390
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1392
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 1393
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 1394
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    .line 1396
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 1397
    return-void
.end method
