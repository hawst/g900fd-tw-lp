.class Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$2;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "RotateEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->initGesture()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    .line 645
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 688
    const-string v0, "onDoubleTapUp"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 689
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->access$5(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;I)V

    .line 690
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    const v1, 0x11101106

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->changeDoneCancelStatus(I)V

    .line 692
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mRotateCallback:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$RotateCallback;->setBottomButtonEnabled(Z)V

    .line 693
    return v2
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 658
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->mAlphaAnim:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->access$1(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->ANIM_SHOW:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->access$2(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 659
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->ANIM_SHOW:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->access$2(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->access$3(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;I)V

    .line 660
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$2;->this$0:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    const/16 v1, 0x32

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->access$4(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;I)V

    .line 663
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 669
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 674
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 679
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 684
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 697
    const/4 v0, 0x0

    return v0
.end method
