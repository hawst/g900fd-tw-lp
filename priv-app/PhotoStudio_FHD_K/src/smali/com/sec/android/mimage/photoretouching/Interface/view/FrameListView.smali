.class public Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;
.super Landroid/widget/LinearLayout;
.source "FrameListView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;
    }
.end annotation


# static fields
.field public static final FRAME_RECENT:I = 0x2004

.field public static final FRAME_TYPE1:I = 0x2001

.field public static final FRAME_TYPE2:I = 0x2002

.field public static final FRAME_TYPE3:I = 0x2003


# instance fields
.field public final FRAME_THREAD_MAX_NUM:I

.field public final FRAME_TYPE1_MAX_NUM:I

.field public final FRAME_TYPE2_MAX_NUM:I

.field public final FRAME_TYPE3_MAX_NUM:I

.field private bLongPressed:Z

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;

.field private mBGColorTask:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;

.field private mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

.field mContext:Landroid/content/Context;

.field private mFirstTouchPoint:Landroid/graphics/PointF;

.field private mFlipperLinearLayout:Landroid/widget/FrameLayout;

.field private mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

.field private mFrameView:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

.field mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

.field private mResId:I

.field public mViewPager:Landroid/support/v4/view/ViewPager;

.field private mViewPagerBottomContainer:Landroid/widget/LinearLayout;

.field private mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

.field public mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 32
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 356
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 357
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;

    .line 358
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFirstTouchPoint:Landroid/graphics/PointF;

    .line 360
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 362
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 363
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    .line 364
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameView:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    .line 365
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mContext:Landroid/content/Context;

    .line 371
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->FRAME_TYPE1_MAX_NUM:I

    .line 372
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->FRAME_TYPE2_MAX_NUM:I

    .line 373
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->FRAME_TYPE3_MAX_NUM:I

    .line 374
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I

    .line 375
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->bLongPressed:Z

    .line 377
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    .line 378
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    .line 379
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    .line 381
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    .line 382
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->FRAME_THREAD_MAX_NUM:I

    .line 33
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mContext:Landroid/content/Context;

    .line 35
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->initWidget()V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "frameEffect"    # Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;
    .param p3, "colorpicker"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;
    .param p4, "frameview"    # Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 38
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 356
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 357
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;

    .line 358
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFirstTouchPoint:Landroid/graphics/PointF;

    .line 360
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 362
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 363
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    .line 364
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameView:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    .line 365
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mContext:Landroid/content/Context;

    .line 371
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->FRAME_TYPE1_MAX_NUM:I

    .line 372
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->FRAME_TYPE2_MAX_NUM:I

    .line 373
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->FRAME_TYPE3_MAX_NUM:I

    .line 374
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I

    .line 375
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->bLongPressed:Z

    .line 377
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    .line 378
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    .line 379
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    .line 381
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    .line 382
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->FRAME_THREAD_MAX_NUM:I

    .line 39
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mContext:Landroid/content/Context;

    .line 40
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameView:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    .line 41
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    .line 42
    invoke-direct {p0, p3}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->initView(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)V

    .line 45
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameView:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)I
    .locals 1

    .prologue
    .line 374
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBGColorTask:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBGColorTask:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;

    return-void
.end method

.method private initView(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)V
    .locals 6
    .param p1, "colorpicker"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameView:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getResId()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I

    .line 49
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    .line 115
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->getCurrentPage(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;-><init>(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;ILcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    .line 116
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getLayout()Landroid/widget/FrameLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    .line 117
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 118
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->addView(Landroid/view/View;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getBitmapListClass()Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    .line 121
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameView:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameView:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getActionBar()Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 123
    :cond_0
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;

    .line 124
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFirstTouchPoint:Landroid/graphics/PointF;

    .line 125
    return-void
.end method

.method private initWidget()V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameView:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getResId()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I

    .line 268
    return-void
.end method


# virtual methods
.method public Release()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 294
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBGColorTask:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBGColorTask:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBGColorTask:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->cancel(Z)Z

    .line 296
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBGColorTask:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    if-eqz v0, :cond_1

    .line 299
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v0, v3, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->show(ZZ)Z

    .line 300
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    if-eqz v0, :cond_2

    .line 301
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->Release()V

    .line 302
    :cond_2
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    .line 303
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_3

    .line 304
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 305
    :cond_3
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFlipperLinearLayout:Landroid/widget/FrameLayout;

    .line 306
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_4

    .line 307
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->removeAllViews()V

    .line 308
    :cond_4
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 309
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 310
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 233
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 262
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1

    .line 235
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    if-eqz v1, :cond_1

    .line 236
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->actionDown()V

    .line 238
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFirstTouchPoint:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 243
    :pswitch_2
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {v0, v1, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 244
    .local v0, "curPt":Landroid/graphics/PointF;
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFirstTouchPoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v4

    float-to-double v4, v1

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    iget v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFirstTouchPoint:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v6

    float-to-double v6, v1

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 245
    .local v2, "distance":D
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    .line 247
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;

    if-eqz v1, :cond_0

    .line 249
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 250
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->hide()V

    goto :goto_0

    .line 252
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$ActionbarNBottomButtonAnimation;->show()V

    goto :goto_0

    .line 257
    .end local v0    # "curPt":Landroid/graphics/PointF;
    .end local v2    # "distance":D
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFirstTouchPoint:Landroid/graphics/PointF;

    invoke-virtual {v1, v4, v4}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 233
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public getColorPicker()Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    return-object v0
.end method

.method getCurrentPage(I)I
    .locals 4
    .param p1, "resId"    # I

    .prologue
    const v3, 0x3120006e

    .line 196
    const/4 v0, 0x0

    .line 197
    .local v0, "pageNum":I
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I

    if-lt v1, v3, :cond_0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I

    const v2, 0x31200077

    if-gt v1, v2, :cond_0

    .line 199
    sub-int v0, p1, v3

    .line 209
    :goto_0
    return v0

    .line 201
    :cond_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I

    const v2, 0x31200078

    if-lt v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I

    const v2, 0x3120007e

    if-gt v1, v2, :cond_1

    .line 203
    sub-int v1, p1, v3

    add-int/lit8 v0, v1, -0xa

    .line 204
    goto :goto_0

    .line 207
    :cond_1
    sub-int v1, p1, v3

    add-int/lit8 v0, v1, -0x11

    goto :goto_0
.end method

.method public isContainedViewPagerBottomArea(FF)Z
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 285
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 286
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerBottomContainer:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 287
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerBottomContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 289
    :cond_0
    float-to-int v1, p1

    float-to-int v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    return v1
.end method

.method public isLongPressed()Z
    .locals 1

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->bLongPressed:Z

    return v0
.end method

.method public onConfigurationChanged()V
    .locals 4

    .prologue
    .line 214
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 215
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->showColorpicker()V

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 220
    .local v0, "curPage":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getPagerAdapter()Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 221
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v1

    if-nez v1, :cond_1

    .line 223
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05025e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 225
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 226
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 272
    const/4 v0, 0x1

    return v0
.end method

.method public removeViewPager()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 276
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameView:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->initView()V

    .line 278
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 279
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 280
    return-void
.end method
