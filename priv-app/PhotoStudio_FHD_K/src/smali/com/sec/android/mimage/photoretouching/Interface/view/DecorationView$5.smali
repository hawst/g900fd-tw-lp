.class Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$5;
.super Ljava/lang/Object;
.source "DecorationView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->setMainBtnListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 286
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsPenTouch:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(Landroid/view/View;Z)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x15000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 275
    :cond_0
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 280
    return-void
.end method
