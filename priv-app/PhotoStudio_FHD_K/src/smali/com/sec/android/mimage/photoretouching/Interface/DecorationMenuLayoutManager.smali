.class public Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
.super Ljava/lang/Object;
.source "DecorationMenuLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;,
        Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;,
        Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;,
        Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;,
        Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;,
        Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;,
        Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnFrameCallback;,
        Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnLabelCallback;,
        Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnStampCallback;,
        Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnTempStickerCallback;,
        Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnWatermarkCallback;,
        Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$TabFactory;
    }
.end annotation


# static fields
.field public static final FRAME_RECENTLY:I = 0x5

.field public static final FRAME_TYPE1:I = 0x6

.field public static final FRAME_TYPE2:I = 0x7

.field public static final FRAME_TYPE3:I = 0x8

.field public static final LABEL_RECENTLY:I = 0x9

.field public static final LABEL_TYPE1:I = 0xa

.field public static final LABEL_TYPE2:I = 0xb

.field public static MAX_FRAME_TYPE1:I = 0x0

.field public static MAX_FRAME_TYPE2:I = 0x0

.field public static MAX_FRAME_TYPE3:I = 0x0

.field public static MAX_LABEL_TYPE1:I = 0x0

.field public static MAX_LABEL_TYPE2:I = 0x0

.field public static MAX_STAMP_TYPE1:I = 0x0

.field public static MAX_STAMP_TYPE2:I = 0x0

.field public static MAX_STICKER_TYPE1:I = 0x0

.field public static MAX_STICKER_TYPE2:I = 0x0

.field public static MAX_STICKER_TYPE3:I = 0x0

.field public static final STAMP_RECENTLY:I = 0xe

.field public static final STAMP_TYPE1:I = 0xf

.field public static final STAMP_TYPE2:I = 0x10

.field public static final STICKER_RECENTLY:I = 0x1

.field public static final STICKER_TYPE1:I = 0x2

.field public static final STICKER_TYPE2:I = 0x3

.field public static final STICKER_TYPE3:I = 0x4

.field public static TAB_FRAME_RECENTLY:I = 0x0

.field public static TAB_FRAME_TYPE1:I = 0x0

.field public static TAB_FRAME_TYPE2:I = 0x0

.field public static TAB_FRAME_TYPE3:I = 0x0

.field public static TAB_LABEL_RECENTLY:I = 0x0

.field public static TAB_LABEL_TYPE1:I = 0x0

.field public static TAB_LABEL_TYPE2:I = 0x0

.field public static TAB_STAMP_RECENTLY:I = 0x0

.field public static TAB_STAMP_TYPE1:I = 0x0

.field public static TAB_STAMP_TYPE2:I = 0x0

.field public static TAB_STICKER_RECENTLY:I = 0x0

.field public static TAB_STICKER_TYPE1:I = 0x0

.field public static TAB_STICKER_TYPE2:I = 0x0

.field public static TAB_STICKER_TYPE3:I = 0x0

.field public static TAB_WATERMARK_RECENTLY:I = 0x0

.field public static TAB_WATERMARK_TYPE1:I = 0x0

.field public static final WATERMARK_RECENTLY:I = 0xc

.field public static final WATERMARK_TYPE1:I = 0xd


# instance fields
.field private MAX_RECENTLY:I

.field private mAnimate:Z

.field private mAssistantFrameInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;

.field private mAssistantLabelInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;

.field private mAssistantStampInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;

.field private mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;

.field private mBasicThumbnail:Landroid/graphics/Bitmap;

.field private mBasicThumbnailArray:[I

.field private mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

.field private mButtonInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentType:I

.field mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

.field mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

.field private mDecorationTabContainer:Landroid/widget/LinearLayout;

.field private mDrawerHandle:Landroid/widget/FrameLayout;

.field private mFrameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mGridView:Landroid/widget/GridView;

.field private mIsExpand:Z

.field private mIsRecently:Z

.field private mIsShowing:Z

.field private mNumColumn:I

.field private mOrgTabLayoutHeight:I

.field private mRecentBtnId:I

.field private mRecentClicked:Z

.field private mRecentIdx:I

.field private mStampList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mStampThumbnail:[I

.field private mStampsOrFrames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mStates:[Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;

.field private mTabHostLayout:Landroid/widget/TabHost;

.field private mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;

.field private mThumbnail:[I

.field private mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

.field private mTrayDrawerCloseListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;

.field private mTrayDrawerOpenListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerOpenListener;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xd

    const/16 v3, 0xa

    const/4 v2, 0x7

    const/16 v1, 0xc

    .line 2018
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_RECENTLY:I

    .line 2019
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE1:I

    .line 2020
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE2:I

    .line 2021
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE3:I

    .line 2023
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_RECENTLY:I

    .line 2024
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE1:I

    .line 2025
    sput v2, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE2:I

    .line 2026
    const/16 v0, 0x8

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE3:I

    .line 2028
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_RECENTLY:I

    .line 2029
    sput v3, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_TYPE1:I

    .line 2030
    const/16 v0, 0xb

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_TYPE2:I

    .line 2032
    sput v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_WATERMARK_TYPE1:I

    .line 2033
    sput v4, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_WATERMARK_RECENTLY:I

    .line 2035
    const/16 v0, 0xe

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_RECENTLY:I

    .line 2036
    const/16 v0, 0xf

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_TYPE1:I

    .line 2037
    const/16 v0, 0x10

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_TYPE2:I

    .line 2053
    sput v3, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE1:I

    .line 2054
    sput v2, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE2:I

    .line 2055
    sput v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE3:I

    .line 2057
    const/16 v0, 0x15

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE1:I

    .line 2058
    sput v4, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE2:I

    .line 2059
    const/16 v0, 0x23

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    .line 2060
    const/16 v0, 0x1e

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    .line 2061
    const/16 v0, 0x14

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    .line 2062
    sput v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    .line 2063
    sput v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE2:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1836
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawerOpenListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerOpenListener;

    .line 1851
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawerCloseListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;

    .line 1979
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBasicThumbnailArray:[I

    .line 1980
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;

    .line 1981
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStampInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;

    .line 1982
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantLabelInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;

    .line 1983
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    .line 1984
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mCurrentType:I

    .line 1986
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    .line 1988
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    .line 2040
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mGridView:Landroid/widget/GridView;

    .line 2042
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    .line 2043
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    .line 2044
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampsOrFrames:Ljava/util/ArrayList;

    .line 2046
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 2047
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    .line 2048
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_RECENTLY:I

    .line 2065
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    .line 2066
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    .line 2067
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    .line 2069
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAnimate:Z

    .line 2070
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mIsShowing:Z

    .line 2072
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mThumbnail:[I

    .line 2073
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampThumbnail:[I

    .line 2074
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBasicThumbnail:Landroid/graphics/Bitmap;

    .line 2075
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mIsExpand:Z

    .line 2076
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantFrameInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;

    .line 2077
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mOrgTabLayoutHeight:I

    .line 2079
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mIsRecently:Z

    .line 2080
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mNumColumn:I

    .line 2082
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mRecentBtnId:I

    .line 2083
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mRecentIdx:I

    .line 2084
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mRecentClicked:Z

    .line 2085
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;

    .line 2086
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStates:[Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;

    .line 66
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    .line 67
    return-void
.end method

.method private AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching;
    .param p2, "tabHost"    # Landroid/widget/TabHost;
    .param p3, "tabSpec"    # Landroid/widget/TabHost$TabSpec;

    .prologue
    .line 330
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$TabFactory;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$TabFactory;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;Landroid/content/Context;)V

    invoke-virtual {p3, v0}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;

    .line 331
    invoke-virtual {p2, p3}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 332
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;
    .locals 1

    .prologue
    .line 2065
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;
    .locals 1

    .prologue
    .line 2085
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;Z)V
    .locals 0

    .prologue
    .line 2075
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mIsExpand:Z

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)Landroid/widget/TabHost;
    .locals 1

    .prologue
    .line 1986
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;Z)V
    .locals 0

    .prologue
    .line 2069
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAnimate:Z

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;Z)V
    .locals 0

    .prologue
    .line 2070
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mIsShowing:Z

    return-void
.end method

.method private getResizedDrawable(III)Landroid/graphics/drawable/Drawable;
    .locals 7
    .param p1, "resId"    # I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 347
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    invoke-static {v6, p2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getDpToPixel(Landroid/content/Context;I)I

    move-result v5

    .line 348
    .local v5, "w":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    invoke-static {v6, p3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getDpToPixel(Landroid/content/Context;I)I

    move-result v2

    .line 350
    .local v2, "h":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 351
    .local v3, "res":Landroid/content/res/Resources;
    invoke-static {v3, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 352
    .local v0, "bmp":Landroid/graphics/Bitmap;
    const/4 v6, 0x1

    invoke-static {v0, v5, v2, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 354
    .local v4, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 356
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 358
    return-object v1
.end method

.method private makeFrameThumbnail(I[III)V
    .locals 15
    .param p1, "viewWidth"    # I
    .param p2, "image"    # [I
    .param p3, "imageWidth"    # I
    .param p4, "imageHeight"    # I

    .prologue
    .line 1460
    const/4 v5, 0x0

    .line 1461
    .local v5, "width":I
    const/4 v6, 0x0

    .line 1462
    .local v6, "height":I
    const/4 v13, 0x0

    .line 1464
    .local v13, "thumbnail":[I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1465
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1466
    :cond_0
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE1:I

    if-lt v11, v1, :cond_1

    .line 1535
    const/4 v11, 0x0

    :goto_1
    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE2:I

    if-lt v11, v1, :cond_5

    .line 1590
    const/4 v11, 0x0

    :goto_2
    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE3:I

    if-lt v11, v1, :cond_9

    .line 1660
    return-void

    .line 1468
    :cond_1
    new-instance v10, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    .line 1469
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    .line 1468
    invoke-direct {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;-><init>(Landroid/content/Context;)V

    .line 1471
    .local v10, "button":Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;
    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->getLayoutWidth()I

    move-result v5

    .line 1472
    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->getLayoutHeight()I

    move-result v6

    .line 1474
    const/4 v7, 0x0

    .local v7, "resizeWidth":I
    const/4 v8, 0x0

    .line 1475
    .local v8, "resizeHeight":I
    const/high16 v9, 0x3f800000    # 1.0f

    .line 1476
    .local v9, "scale":F
    int-to-float v1, v5

    int-to-float v2, v6

    div-float v14, v1, v2

    .line 1477
    .local v14, "viewRatio":F
    move/from16 v0, p3

    int-to-float v1, v0

    move/from16 v0, p4

    int-to-float v2, v0

    div-float v12, v1, v2

    .line 1479
    .local v12, "imgRatio":F
    cmpl-float v1, v14, v12

    if-lez v1, :cond_4

    .line 1480
    int-to-float v1, v5

    move/from16 v0, p3

    int-to-float v2, v0

    div-float v9, v1, v2

    .line 1481
    move/from16 v0, p4

    int-to-float v1, v0

    mul-float/2addr v1, v9

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v8, v1

    .line 1482
    move v7, v5

    .line 1491
    :goto_3
    if-nez v11, :cond_2

    move-object v1, p0

    move-object/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    .line 1492
    invoke-direct/range {v1 .. v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->makeThumbnail([IIIIIIIF)[I

    move-result-object v13

    .line 1494
    :cond_2
    if-eqz v13, :cond_3

    .line 1495
    invoke-virtual {v10, v13, v5, v6}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnail([III)V

    .line 1497
    :cond_3
    packed-switch v11, :pswitch_data_0

    .line 1530
    :goto_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1466
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 1486
    :cond_4
    int-to-float v1, v6

    move/from16 v0, p4

    int-to-float v2, v0

    div-float v9, v1, v2

    .line 1487
    move/from16 v0, p3

    int-to-float v1, v0

    mul-float/2addr v1, v9

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v7, v1

    .line 1488
    move v8, v6

    goto :goto_3

    .line 1499
    :pswitch_0
    const v1, 0x3120006e

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_4

    .line 1502
    :pswitch_1
    const v1, 0x3120006f

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_4

    .line 1505
    :pswitch_2
    const v1, 0x31200070

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_4

    .line 1508
    :pswitch_3
    const v1, 0x31200071

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_4

    .line 1511
    :pswitch_4
    const v1, 0x31200072

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_4

    .line 1514
    :pswitch_5
    const v1, 0x31200073

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_4

    .line 1517
    :pswitch_6
    const v1, 0x31200074

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_4

    .line 1520
    :pswitch_7
    const v1, 0x31200075

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_4

    .line 1523
    :pswitch_8
    const v1, 0x31200076

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_4

    .line 1526
    :pswitch_9
    const v1, 0x31200077

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_4

    .line 1536
    .end local v7    # "resizeWidth":I
    .end local v8    # "resizeHeight":I
    .end local v9    # "scale":F
    .end local v10    # "button":Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;
    .end local v12    # "imgRatio":F
    .end local v14    # "viewRatio":F
    :cond_5
    new-instance v10, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    .line 1537
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    .line 1536
    invoke-direct {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;-><init>(Landroid/content/Context;)V

    .line 1539
    .restart local v10    # "button":Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;
    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->getLayoutWidth()I

    move-result v5

    .line 1540
    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->getLayoutHeight()I

    move-result v6

    .line 1542
    const/4 v7, 0x0

    .restart local v7    # "resizeWidth":I
    const/4 v8, 0x0

    .line 1543
    .restart local v8    # "resizeHeight":I
    const/high16 v9, 0x3f800000    # 1.0f

    .line 1544
    .restart local v9    # "scale":F
    int-to-float v1, v5

    int-to-float v2, v6

    div-float v14, v1, v2

    .line 1545
    .restart local v14    # "viewRatio":F
    move/from16 v0, p3

    int-to-float v1, v0

    move/from16 v0, p4

    int-to-float v2, v0

    div-float v12, v1, v2

    .line 1547
    .restart local v12    # "imgRatio":F
    cmpl-float v1, v14, v12

    if-lez v1, :cond_8

    .line 1548
    int-to-float v1, v5

    move/from16 v0, p3

    int-to-float v2, v0

    div-float v9, v1, v2

    .line 1549
    move/from16 v0, p4

    int-to-float v1, v0

    mul-float/2addr v1, v9

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v8, v1

    .line 1550
    move v7, v5

    .line 1559
    :goto_5
    if-nez v11, :cond_6

    move-object v1, p0

    move-object/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    .line 1560
    invoke-direct/range {v1 .. v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->makeThumbnail([IIIIIIIF)[I

    move-result-object v13

    .line 1562
    :cond_6
    if-eqz v13, :cond_7

    .line 1563
    invoke-virtual {v10, v13, v5, v6}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnail([III)V

    .line 1564
    :cond_7
    packed-switch v11, :pswitch_data_1

    .line 1588
    :goto_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1535
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 1554
    :cond_8
    int-to-float v1, v6

    move/from16 v0, p4

    int-to-float v2, v0

    div-float v9, v1, v2

    .line 1555
    move/from16 v0, p3

    int-to-float v1, v0

    mul-float/2addr v1, v9

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v7, v1

    .line 1556
    move v8, v6

    goto :goto_5

    .line 1566
    :pswitch_a
    const v1, 0x31200078

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_6

    .line 1569
    :pswitch_b
    const v1, 0x31200079

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_6

    .line 1572
    :pswitch_c
    const v1, 0x3120007a

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_6

    .line 1575
    :pswitch_d
    const v1, 0x3120007b

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_6

    .line 1578
    :pswitch_e
    const v1, 0x3120007c

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_6

    .line 1581
    :pswitch_f
    const v1, 0x3120007d

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_6

    .line 1584
    :pswitch_10
    const v1, 0x3120007e

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_6

    .line 1591
    .end local v7    # "resizeWidth":I
    .end local v8    # "resizeHeight":I
    .end local v9    # "scale":F
    .end local v10    # "button":Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;
    .end local v12    # "imgRatio":F
    .end local v14    # "viewRatio":F
    :cond_9
    new-instance v10, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    .line 1592
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    .line 1591
    invoke-direct {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;-><init>(Landroid/content/Context;)V

    .line 1594
    .restart local v10    # "button":Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;
    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->getLayoutWidth()I

    move-result v5

    .line 1595
    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->getLayoutHeight()I

    move-result v6

    .line 1597
    const/4 v7, 0x0

    .restart local v7    # "resizeWidth":I
    const/4 v8, 0x0

    .line 1598
    .restart local v8    # "resizeHeight":I
    const/high16 v9, 0x3f800000    # 1.0f

    .line 1599
    .restart local v9    # "scale":F
    int-to-float v1, v5

    int-to-float v2, v6

    div-float v14, v1, v2

    .line 1600
    .restart local v14    # "viewRatio":F
    move/from16 v0, p3

    int-to-float v1, v0

    move/from16 v0, p4

    int-to-float v2, v0

    div-float v12, v1, v2

    .line 1602
    .restart local v12    # "imgRatio":F
    cmpl-float v1, v14, v12

    if-lez v1, :cond_c

    .line 1603
    int-to-float v1, v5

    move/from16 v0, p3

    int-to-float v2, v0

    div-float v9, v1, v2

    .line 1604
    move/from16 v0, p4

    int-to-float v1, v0

    mul-float/2addr v1, v9

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v8, v1

    .line 1605
    move v7, v5

    .line 1614
    :goto_7
    if-nez v11, :cond_a

    move-object v1, p0

    move-object/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    .line 1615
    invoke-direct/range {v1 .. v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->makeThumbnail([IIIIIIIF)[I

    move-result-object v13

    .line 1617
    :cond_a
    if-eqz v13, :cond_b

    .line 1618
    invoke-virtual {v10, v13, v5, v6}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnail([III)V

    .line 1619
    :cond_b
    packed-switch v11, :pswitch_data_2

    .line 1658
    :goto_8
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1590
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_2

    .line 1609
    :cond_c
    int-to-float v1, v6

    move/from16 v0, p4

    int-to-float v2, v0

    div-float v9, v1, v2

    .line 1610
    move/from16 v0, p3

    int-to-float v1, v0

    mul-float/2addr v1, v9

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v7, v1

    .line 1611
    move v8, v6

    goto :goto_7

    .line 1621
    :pswitch_11
    const v1, 0x3120007f

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_8

    .line 1624
    :pswitch_12
    const v1, 0x31200080

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_8

    .line 1627
    :pswitch_13
    const v1, 0x31200081

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_8

    .line 1630
    :pswitch_14
    const v1, 0x31200082

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_8

    .line 1633
    :pswitch_15
    const v1, 0x31200083

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_8

    .line 1636
    :pswitch_16
    const v1, 0x31200084

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_8

    .line 1639
    :pswitch_17
    const v1, 0x31200085

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_8

    .line 1642
    :pswitch_18
    const v1, 0x31200086

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_8

    .line 1645
    :pswitch_19
    const v1, 0x31200087

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_8

    .line 1648
    :pswitch_1a
    const v1, 0x31200088

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_8

    .line 1651
    :pswitch_1b
    const v1, 0x31200089

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_8

    .line 1654
    :pswitch_1c
    const v1, 0x3120008a

    invoke-virtual {v10, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_8

    .line 1497
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 1564
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 1619
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch
.end method

.method private makeThumbnail([IIIIIIIF)[I
    .locals 18
    .param p1, "bitmap"    # [I
    .param p2, "imageWidth"    # I
    .param p3, "imageHeight"    # I
    .param p4, "button_width"    # I
    .param p5, "button_height"    # I
    .param p6, "resizeWidth"    # I
    .param p7, "resizeHeight"    # I
    .param p8, "scale"    # F

    .prologue
    .line 1420
    const/4 v2, 0x0

    .line 1422
    .local v2, "thumbnail":[I
    move/from16 v13, p2

    .line 1423
    .local v13, "bitmap_w":I
    move/from16 v12, p3

    .line 1425
    .local v12, "bitmap_h":I
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "thumbnailINfo :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1426
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "thumbnailINfo resize:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p6

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p7

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1433
    const/4 v14, 0x0

    .line 1434
    .local v14, "startX":I
    const/4 v15, 0x0

    .line 1435
    .local v15, "startY":I
    const/16 v17, 0x0

    .line 1436
    .local v17, "tempWidth":I
    const/16 v16, 0x0

    .line 1437
    .local v16, "tempHeight":I
    const/high16 v8, 0x3f800000    # 1.0f

    .line 1439
    .local v8, "tempScale":F
    move/from16 v0, p2

    move/from16 v1, p3

    if-le v0, v1, :cond_0

    .line 1440
    move/from16 v0, p3

    int-to-float v3, v0

    move/from16 v0, p5

    int-to-float v4, v0

    div-float v8, v3, v4

    .line 1444
    :goto_0
    move/from16 v0, p2

    int-to-float v3, v0

    div-float/2addr v3, v8

    float-to-int v0, v3

    move/from16 v17, v0

    .line 1445
    move/from16 v0, p3

    int-to-float v3, v0

    div-float/2addr v3, v8

    float-to-int v0, v3

    move/from16 v16, v0

    .line 1448
    sub-int v3, v17, p4

    div-int/lit8 v14, v3, 0x2

    .line 1449
    sub-int v3, v16, p5

    div-int/lit8 v15, v3, 0x2

    .line 1451
    mul-int v3, p4, p5

    new-array v2, v3, [I

    .line 1453
    int-to-float v3, v14

    mul-float/2addr v3, v8

    float-to-int v9, v3

    .line 1454
    int-to-float v3, v15

    mul-float/2addr v3, v8

    float-to-int v10, v3

    new-instance v11, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    add-int/lit8 v5, p4, -0x1

    add-int/lit8 v6, p5, -0x1

    invoke-direct {v11, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    move/from16 v3, p4

    move/from16 v4, p5

    move-object/from16 v5, p1

    move/from16 v6, p2

    move/from16 v7, p3

    .line 1452
    invoke-static/range {v2 .. v11}, Lcom/sec/android/mimage/photoretouching/jni/Util;->native_drawImage([III[IIIFIILandroid/graphics/Rect;)Z

    .line 1456
    return-object v2

    .line 1442
    :cond_0
    move/from16 v0, p2

    int-to-float v3, v0

    move/from16 v0, p4

    int-to-float v4, v0

    div-float v8, v3, v4

    goto :goto_0
.end method

.method private resetTabHeight()V
    .locals 3

    .prologue
    .line 1914
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1915
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mOrgTabLayoutHeight:I

    if-eq v1, v2, :cond_0

    .line 1917
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mOrgTabLayoutHeight:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1918
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1920
    :cond_0
    return-void
.end method


# virtual methods
.method public addFrame(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnFrameCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnFrameCallback;

    .prologue
    .line 1121
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    .line 1122
    .local v0, "aInfo":Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;
    iput-object p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;->frameListener:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnFrameCallback;

    .line 1124
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantFrameInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;

    .line 1125
    return-void
.end method

.method public addLabel(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnLabelCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnLabelCallback;

    .prologue
    .line 1115
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    .line 1116
    .local v0, "aInfo":Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;
    iput-object p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;->labelListener:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnLabelCallback;

    .line 1118
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantLabelInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;

    .line 1119
    return-void
.end method

.method public addStamp(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnStampCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnStampCallback;

    .prologue
    .line 1109
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    .line 1110
    .local v0, "aInfo":Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;
    iput-object p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;->stampListener:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnStampCallback;

    .line 1112
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStampInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;

    .line 1113
    return-void
.end method

.method public addSticker(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnTempStickerCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnTempStickerCallback;

    .prologue
    .line 1103
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    .line 1104
    .local v0, "aInfo":Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;
    iput-object p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;->stickerListener:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnTempStickerCallback;

    .line 1106
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;

    .line 1107
    return-void
.end method

.method public closeDrawer()V
    .locals 1

    .prologue
    .line 1893
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    if-eqz v0, :cond_0

    .line 1894
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->animateClose()V

    .line 1895
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mIsExpand:Z

    .line 1897
    :cond_0
    return-void
.end method

.method public configurationChanged()V
    .locals 3

    .prologue
    .line 1093
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->init()V

    .line 1094
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTab()V

    .line 1096
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    const v2, 0x7f090150

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1097
    .local v0, "handleImage":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1098
    const v1, 0x7f0201af

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1101
    :goto_0
    return-void

    .line 1100
    :cond_0
    const v1, 0x7f0201b0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public destroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1031
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    if-eqz v1, :cond_0

    .line 1032
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 1034
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TabWidget;->removeAllViews()V

    .line 1035
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    .line 1037
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_1

    .line 1039
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->removeAllViewsInLayout()V

    .line 1040
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->removeAllViews()V

    .line 1041
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 1042
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    .line 1044
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    if-eqz v1, :cond_2

    .line 1046
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->destroy()V

    .line 1047
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    .line 1049
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    .line 1051
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_9

    .line 1056
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1057
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    .line 1059
    .end local v0    # "i":I
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    .line 1061
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_a

    .line 1066
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1067
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    .line 1069
    .end local v0    # "i":I
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantFrameInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;

    if-eqz v1, :cond_5

    .line 1070
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantFrameInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;

    iput-object v3, v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;->frameListener:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnFrameCallback;

    .line 1071
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantFrameInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;

    .line 1073
    :cond_5
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantLabelInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;

    if-eqz v1, :cond_6

    .line 1074
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantLabelInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;

    iput-object v3, v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;->labelListener:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnLabelCallback;

    .line 1075
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantLabelInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;

    .line 1077
    :cond_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStampInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;

    if-eqz v1, :cond_7

    .line 1078
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStampInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;

    iput-object v3, v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;->stampListener:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnStampCallback;

    .line 1079
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStampInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;

    .line 1081
    :cond_7
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;

    if-eqz v1, :cond_8

    .line 1082
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;

    iput-object v3, v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;->stickerListener:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnTempStickerCallback;

    .line 1083
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;

    .line 1085
    :cond_8
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStates:[Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;

    .line 1086
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampThumbnail:[I

    .line 1087
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mThumbnail:[I

    .line 1088
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBasicThumbnailArray:[I

    .line 1089
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;

    .line 1090
    return-void

    .line 1053
    .restart local v0    # "i":I
    :cond_9
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->destroy()V

    .line 1051
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1063
    :cond_a
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->destroy()V

    .line 1061
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public destroyStampsOrFrames()V
    .locals 1

    .prologue
    .line 1933
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampsOrFrames:Ljava/util/ArrayList;

    .line 1934
    return-void
.end method

.method public drawerHide()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 401
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    if-eqz v3, :cond_0

    .line 402
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->setVisibility(I)V

    .line 404
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .line 406
    const/high16 v8, 0x3f800000    # 1.0f

    move v3, v1

    move v4, v2

    move v5, v1

    move v6, v2

    move v7, v1

    .line 404
    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 407
    .local v0, "animation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 408
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mIsShowing:Z

    .line 409
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$8;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 428
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 429
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$9;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;Landroid/view/animation/Animation;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 438
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_0
    return-void
.end method

.method public drawerShow()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 362
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    if-eqz v3, :cond_0

    .line 363
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->setVisibility(I)V

    .line 365
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .line 367
    const/high16 v6, 0x3f800000    # 1.0f

    move v3, v1

    move v4, v2

    move v5, v1

    move v7, v1

    move v8, v2

    .line 365
    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 368
    .local v0, "animation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 369
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mIsShowing:Z

    .line 370
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$6;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 388
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 389
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$7;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;Landroid/view/animation/Animation;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 398
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_0
    return-void
.end method

.method public getBasicThumbnail()[I
    .locals 1

    .prologue
    .line 1949
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBasicThumbnailArray:[I

    return-object v0
.end method

.method public getCurrnetType()I
    .locals 1

    .prologue
    .line 957
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mCurrentType:I

    return v0
.end method

.method public getFrameList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1942
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGridView()Landroid/widget/GridView;
    .locals 1

    .prologue
    .line 1953
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mGridView:Landroid/widget/GridView;

    return-object v0
.end method

.method public getNumColumn()I
    .locals 2

    .prologue
    .line 1970
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x31100000

    if-ne v0, v1, :cond_1

    .line 1971
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    .line 1975
    :goto_0
    return v0

    .line 1971
    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    .line 1972
    :cond_1
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x31300000

    if-ne v0, v1, :cond_3

    .line 1973
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    .line 1975
    :cond_3
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mNumColumn:I

    goto :goto_0
.end method

.method public getRecentButton()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 851
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRecentId()I
    .locals 1

    .prologue
    .line 832
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mRecentBtnId:I

    return v0
.end method

.method public getStampList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1924
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getStampsOrFrames()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1929
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampsOrFrames:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTabContentView()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 1961
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v0

    return-object v0
.end method

.method public getTabWidget()Landroid/widget/TabWidget;
    .locals 1

    .prologue
    .line 1957
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    return-object v0
.end method

.method public getmTestInterface()Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;
    .locals 1

    .prologue
    .line 1877
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;

    return-object v0
.end method

.method public init()V
    .locals 14

    .prologue
    .line 71
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    if-eqz v8, :cond_0

    .line 73
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/TabWidget;->removeAllViews()V

    .line 74
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    .line 78
    :cond_0
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const v9, 0x7f090153

    invoke-virtual {v8, v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/support/v4/view/ViewPager;

    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    .line 79
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const v9, 0x7f0900bc

    invoke-virtual {v8, v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    .line 80
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    const v9, 0x7f090152

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TabHost;

    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    .line 82
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 84
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    const/high16 v9, 0x2000000

    invoke-virtual {v8, v9}, Landroid/support/v4/view/ViewPager;->setScrollBarStyle(I)V

    .line 85
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    const/16 v9, 0xa

    const/4 v10, 0x0

    const/16 v11, 0xa

    const/4 v12, 0x0

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/support/v4/view/ViewPager;->setPadding(IIII)V

    .line 88
    const/16 v8, 0x10

    new-array v4, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    const v10, 0x7f0601e6

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    const v10, 0x7f0601e7

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    const/4 v8, 0x2

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    const v10, 0x7f0601e8

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    const/4 v8, 0x3

    .line 89
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    const v10, 0x7f060058

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    const/4 v8, 0x4

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    const v10, 0x7f0601e6

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    const/4 v8, 0x5

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    const v10, 0x7f0601ed

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    const/4 v8, 0x6

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    const v10, 0x7f0601ee

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    const/4 v8, 0x7

    .line 90
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    const v10, 0x7f0601ef

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    const/16 v8, 0x8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    const v10, 0x7f0601e6

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    const/16 v8, 0x9

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    const v10, 0x7f0601eb

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    const/16 v8, 0xa

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    const v10, 0x7f0601ec

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    const/16 v8, 0xb

    const-string v9, " dummy"

    aput-object v9, v4, v8

    const/16 v8, 0xc

    const-string v9, "dummy"

    aput-object v9, v4, v8

    const/16 v8, 0xd

    .line 91
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    const v10, 0x7f0601e6

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    const/16 v8, 0xe

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    const v10, 0x7f0601e9

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    const/16 v8, 0xf

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    const v10, 0x7f0601ea

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v8

    .line 93
    .local v4, "mTabStrings":[Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v9, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$3;

    invoke-direct {v9, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    invoke-virtual {v8, v9}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 135
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    if-eqz v8, :cond_1

    .line 137
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->setup()V

    .line 146
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "DEFAULT"

    invoke-virtual {v10, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v10

    const-string v11, "DEFAULT"

    invoke-virtual {v10, v11}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v10

    invoke-direct {p0, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 148
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "STICKER_RECENTLY"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f020196

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 150
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "STICKER_TYPE1"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f02048f

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 152
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "STICKER_TYPE2"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f02048e

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 154
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "STICKER_TYPE3"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f02048d

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 157
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "FRAME_RECENTLY"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f020196

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 159
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "FRAME_TYPE1"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f020191

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 161
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "FRAME_TYPE2"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f020193

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 163
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "FRAME_TYPE3"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f020192

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 166
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "LABEL_RECENTLY"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f020196

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 168
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "LABEL_TYPE1"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f020194

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 170
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "LABEL_TYPE2"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f020195

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 173
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "WATERMARK_RECENTLY"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f020196

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 175
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "WATERMARK_TYPE1"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f020494

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 178
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "STAMP_RECENTLY"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f020196

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 180
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "STAMP_TYPE1"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f020197

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 182
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    const-string v11, "STAMP_TYPE2"

    invoke-virtual {v9, v11}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v11

    const-string v12, ""

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v13, 0x7f020198

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v11, v12, v9}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-direct {p0, v8, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->AddTab(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Landroid/widget/TabHost;Landroid/widget/TabHost$TabSpec;)V

    .line 185
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v9, v10, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v8, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 186
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TabWidget;->setShowDividers(I)V

    .line 187
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/TabWidget;->getChildCount()I

    move-result v8

    new-array v8, v8, [Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;

    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStates:[Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;

    .line 189
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/TabWidget;->getChildCount()I

    move-result v8

    if-lt v3, v8, :cond_2

    .line 217
    const/4 v3, 0x0

    :goto_1
    array-length v8, v4

    if-lt v3, v8, :cond_3

    .line 225
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    new-instance v9, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;

    invoke-direct {v9, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    invoke-virtual {v8, v9}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 294
    .end local v3    # "i":I
    :cond_1
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    const v9, 0x7f09014d

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    .line 295
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    const v9, 0x7f09014f

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/FrameLayout;

    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    .line 298
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    const v9, 0x7f090150

    invoke-virtual {v8, v9}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 299
    .local v1, "handleImage":Landroid/widget/ImageView;
    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 300
    .local v5, "param":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v0, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 301
    .local v0, "displayWidth":I
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9}, Landroid/widget/ImageView;->measure(II)V

    .line 302
    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    .line 305
    .local v2, "handlerWidth":I
    sub-int v8, v0, v2

    iput v8, v5, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 306
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 309
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawerCloseListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;

    invoke-virtual {v8, v9, v0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->setOnDrawerCloseListener(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;II)V

    .line 310
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawerOpenListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerOpenListener;

    invoke-virtual {v8, v9}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->setOnDrawerOpenListener(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerOpenListener;)V

    .line 313
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mOrgTabLayoutHeight:I

    if-nez v8, :cond_4

    .line 315
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout$LayoutParams;

    .line 316
    .local v6, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget v8, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mOrgTabLayoutHeight:I

    .line 327
    .end local v6    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :goto_2
    return-void

    .line 191
    .end local v0    # "displayWidth":I
    .end local v1    # "handleImage":Landroid/widget/ImageView;
    .end local v2    # "handlerWidth":I
    .end local v5    # "param":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v3    # "i":I
    :cond_2
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 192
    .local v5, "param":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f05021d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 193
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 195
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStates:[Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;

    new-instance v9, Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    invoke-direct {v9, v10}, Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;-><init>(Landroid/content/Context;)V

    aput-object v9, v8, v3

    .line 196
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStates:[Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;

    aget-object v9, v9, v3

    invoke-virtual {v8, v9}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 197
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/view/View;->setFocusable(Z)V

    .line 200
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    new-instance v9, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$4;

    invoke-direct {v9, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 189
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 218
    .end local v5    # "param":Landroid/widget/LinearLayout$LayoutParams;
    :cond_3
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v8

    add-int/lit8 v9, v3, 0x1

    invoke-virtual {v8, v9}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 219
    .local v7, "v":Landroid/view/View;
    aget-object v8, v4, v3

    invoke-virtual {v7, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    invoke-static {v8, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 217
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 319
    .end local v3    # "i":I
    .end local v7    # "v":Landroid/view/View;
    .restart local v0    # "displayWidth":I
    .restart local v1    # "handleImage":Landroid/widget/ImageView;
    .restart local v2    # "handlerWidth":I
    .local v5, "param":Landroid/widget/FrameLayout$LayoutParams;
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->resetTabHeight()V

    goto/16 :goto_2
.end method

.method public initDataBase(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v8, 0x10

    const/4 v1, 0x6

    .line 671
    new-instance v0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 672
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->open()Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 673
    const/4 v6, 0x0

    .line 675
    .local v6, "cs":Landroid/database/Cursor;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 698
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    .line 699
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 704
    :cond_0
    :try_start_0
    const-string v0, "button_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 705
    .local v2, "buttonId":I
    const-string v0, "button_idx"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 706
    .local v3, "buttonIdx":I
    const-string v0, "recent_used"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 707
    .local v4, "buttonRecent":I
    const-string v0, "use"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 709
    .local v5, "buttonUse":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    const/4 v9, 0x0

    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;IIII)V

    invoke-virtual {v8, v9, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 714
    .end local v2    # "buttonId":I
    .end local v3    # "buttonIdx":I
    .end local v4    # "buttonRecent":I
    .end local v5    # "buttonUse":I
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 716
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW initDataBase: mButtonInfoList.size="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 717
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->close()V

    .line 718
    return-void

    .line 678
    :sswitch_0
    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_RECENTLY:I

    .line 679
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentStrickerButtonInfo()Landroid/database/Cursor;

    move-result-object v6

    .line 680
    goto :goto_0

    .line 682
    :sswitch_1
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_RECENTLY:I

    .line 683
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentLabelButtonInfo()Landroid/database/Cursor;

    move-result-object v6

    .line 684
    goto :goto_0

    .line 686
    :sswitch_2
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_RECENTLY:I

    .line 687
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentWatermarkButtonInfo()Landroid/database/Cursor;

    move-result-object v6

    .line 688
    goto/16 :goto_0

    .line 690
    :sswitch_3
    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_RECENTLY:I

    .line 691
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentFrameButtonInfo()Landroid/database/Cursor;

    move-result-object v6

    .line 692
    goto/16 :goto_0

    .line 694
    :sswitch_4
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_RECENTLY:I

    .line 695
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentStampButtonInfo()Landroid/database/Cursor;

    move-result-object v6

    goto/16 :goto_0

    .line 711
    :catch_0
    move-exception v7

    .line 712
    .local v7, "e":Landroid/database/CursorIndexOutOfBoundsException;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SubViewButtonManager - initDataBase() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Landroid/database/CursorIndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_1

    .line 675
    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_0
        0x31200000 -> :sswitch_3
        0x31300000 -> :sswitch_1
        0x31400000 -> :sswitch_2
        0x31500000 -> :sswitch_4
    .end sparse-switch
.end method

.method public initStampsOrFrames()V
    .locals 1

    .prologue
    .line 1938
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampsOrFrames:Ljava/util/ArrayList;

    .line 1939
    return-void
.end method

.method public isDrawerOpened()Z
    .locals 1

    .prologue
    .line 1901
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mIsExpand:Z

    return v0
.end method

.method public isRecentClicked()Z
    .locals 1

    .prologue
    .line 847
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mRecentClicked:Z

    return v0
.end method

.method public isRecentlyTab()Z
    .locals 1

    .prologue
    .line 1399
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mIsRecently:Z

    return v0
.end method

.method public makeStampThumbnail(ZII[III)V
    .locals 22
    .param p1, "isBackground"    # Z
    .param p2, "index"    # I
    .param p3, "viewWidth"    # I
    .param p4, "image"    # [I
    .param p5, "imageWidth"    # I
    .param p6, "imageHeight"    # I

    .prologue
    .line 1663
    const/4 v5, 0x0

    .line 1664
    .local v5, "width":I
    const/4 v6, 0x0

    .line 1667
    .local v6, "height":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 1668
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    .line 1670
    :cond_0
    new-instance v18, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    .line 1671
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    .line 1670
    move-object/from16 v0, v18

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;-><init>(Landroid/content/Context;)V

    .line 1673
    .local v18, "button":Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->getLayoutWidth()I

    move-result v5

    .line 1674
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->getLayoutHeight()I

    move-result v6

    .line 1676
    const/4 v7, 0x0

    .local v7, "resizeWidth":I
    const/4 v8, 0x0

    .line 1677
    .local v8, "resizeHeight":I
    const/high16 v9, 0x3f800000    # 1.0f

    .line 1678
    .local v9, "scale":F
    int-to-float v1, v5

    int-to-float v2, v6

    div-float v21, v1, v2

    .line 1679
    .local v21, "viewRatio":F
    move/from16 v0, p5

    int-to-float v1, v0

    move/from16 v0, p6

    int-to-float v2, v0

    div-float v19, v1, v2

    .line 1681
    .local v19, "imgRatio":F
    cmpl-float v1, v21, v19

    if-lez v1, :cond_2

    .line 1682
    int-to-float v1, v5

    move/from16 v0, p5

    int-to-float v2, v0

    div-float v9, v1, v2

    .line 1683
    move/from16 v0, p6

    int-to-float v1, v0

    mul-float/2addr v1, v9

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v8, v1

    .line 1684
    move v7, v5

    .line 1693
    :goto_0
    if-eqz p1, :cond_3

    .line 1695
    const/4 v1, 0x1

    move/from16 v0, p2

    if-ne v0, v1, :cond_1

    move-object/from16 v1, p0

    move-object/from16 v2, p4

    move/from16 v3, p5

    move/from16 v4, p6

    .line 1697
    invoke-direct/range {v1 .. v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->makeThumbnail([IIIIIIIF)[I

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mThumbnail:[I

    .line 1700
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mThumbnail:[I

    if-eqz v1, :cond_1

    .line 1702
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    add-int/lit8 v2, p2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    .line 1705
    .local v20, "tempButton":Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mThumbnail:[I

    move-object/from16 v0, v20

    invoke-virtual {v0, v1, v5, v6}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->applyEffectThumbnail([III)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBasicThumbnail:Landroid/graphics/Bitmap;

    .line 1706
    mul-int v1, v5, v6

    new-array v1, v1, [I

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBasicThumbnailArray:[I

    .line 1707
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBasicThumbnail:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBasicThumbnailArray:[I

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move v13, v5

    move/from16 v16, v5

    move/from16 v17, v6

    invoke-virtual/range {v10 .. v17}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1708
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBasicThumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1835
    .end local v20    # "tempButton":Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;
    :cond_1
    :goto_1
    return-void

    .line 1688
    :cond_2
    int-to-float v1, v6

    move/from16 v0, p6

    int-to-float v2, v0

    div-float v9, v1, v2

    .line 1689
    move/from16 v0, p5

    int-to-float v1, v0

    mul-float/2addr v1, v9

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v7, v1

    .line 1690
    move v8, v6

    goto :goto_0

    .line 1715
    :cond_3
    add-int/lit8 v1, p2, -0x1

    packed-switch v1, :pswitch_data_0

    .line 1822
    :goto_2
    const/4 v1, 0x1

    move/from16 v0, p2

    if-lt v0, v1, :cond_4

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE1:I

    sget v2, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE2:I

    add-int/2addr v1, v2

    move/from16 v0, p2

    if-gt v0, v1, :cond_4

    move-object/from16 v1, p0

    move-object/from16 v2, p4

    move/from16 v3, p5

    move/from16 v4, p6

    .line 1824
    invoke-direct/range {v1 .. v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->makeThumbnail([IIIIIIIF)[I

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampThumbnail:[I

    .line 1829
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampThumbnail:[I

    if-eqz v1, :cond_5

    .line 1830
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampThumbnail:[I

    move-object/from16 v0, v18

    invoke-virtual {v0, v1, v5, v6}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setStampThumbnail([III)V

    .line 1832
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1717
    :pswitch_0
    const v1, 0x3150008b

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_2

    .line 1720
    :pswitch_1
    const v1, 0x3150008c

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_2

    .line 1723
    :pswitch_2
    const v1, 0x3150008d

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_2

    .line 1726
    :pswitch_3
    const v1, 0x3150008e

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_2

    .line 1729
    :pswitch_4
    const v1, 0x3150008f

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_2

    .line 1732
    :pswitch_5
    const v1, 0x31500090

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_2

    .line 1735
    :pswitch_6
    const v1, 0x31500091

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_2

    .line 1738
    :pswitch_7
    const v1, 0x31500092

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto :goto_2

    .line 1741
    :pswitch_8
    const v1, 0x31500093

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1744
    :pswitch_9
    const v1, 0x31500094

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1747
    :pswitch_a
    const v1, 0x31500095

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1750
    :pswitch_b
    const v1, 0x31500096

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1753
    :pswitch_c
    const v1, 0x31500097

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1756
    :pswitch_d
    const v1, 0x31500098

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1759
    :pswitch_e
    const v1, 0x31500099

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1762
    :pswitch_f
    const v1, 0x3150009a

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1765
    :pswitch_10
    const v1, 0x3150009b

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1768
    :pswitch_11
    const v1, 0x3150009c

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1771
    :pswitch_12
    const v1, 0x3150009d

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1774
    :pswitch_13
    const v1, 0x3150009e

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1777
    :pswitch_14
    const v1, 0x3150009f

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1780
    :pswitch_15
    const v1, 0x315000a0

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1783
    :pswitch_16
    const v1, 0x315000a1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1786
    :pswitch_17
    const v1, 0x315000a2

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1789
    :pswitch_18
    const v1, 0x315000a3

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1792
    :pswitch_19
    const v1, 0x315000a4

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1795
    :pswitch_1a
    const v1, 0x315000a5

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1798
    :pswitch_1b
    const v1, 0x315000a6

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1801
    :pswitch_1c
    const v1, 0x315000a7

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1804
    :pswitch_1d
    const v1, 0x315000a8

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1807
    :pswitch_1e
    const v1, 0x315000a9

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1810
    :pswitch_1f
    const v1, 0x315000aa

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1813
    :pswitch_20
    const v1, 0x315000ab

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1816
    :pswitch_21
    const v1, 0x315000ac

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->setThumbnailId(I)V

    goto/16 :goto_2

    .line 1715
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
    .end packed-switch
.end method

.method public openDrawer()V
    .locals 1

    .prologue
    .line 1886
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    if-eqz v0, :cond_0

    .line 1887
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTrayDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->animateOpen()V

    .line 1888
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mIsExpand:Z

    .line 1890
    :cond_0
    return-void
.end method

.method public resetRecentButton(II)V
    .locals 7
    .param p1, "btnId"    # I
    .param p2, "index"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 782
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 784
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 786
    iget p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mRecentBtnId:I

    .line 787
    iget p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mRecentIdx:I

    .line 790
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 791
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    move-object v1, p0

    move v2, p1

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;IIII)V

    invoke-virtual {v6, v3, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 793
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    if-eqz v0, :cond_1

    .line 795
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->open()Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 796
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    invoke-virtual {v0, v1, p1, v3}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->updateButtonInfoInButtonType(IIZ)V

    .line 797
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->close()V

    .line 800
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mIsRecently:Z

    if-eqz v0, :cond_2

    .line 802
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 828
    :cond_2
    :goto_0
    return-void

    .line 805
    :sswitch_0
    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setImageContext(I)V

    goto :goto_0

    .line 808
    :sswitch_1
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setImageContext(I)V

    goto :goto_0

    .line 811
    :sswitch_2
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setImageContext(I)V

    goto :goto_0

    .line 814
    :sswitch_3
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setThumbnailContext(I)V

    goto :goto_0

    .line 817
    :sswitch_4
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setThumbnailContext(I)V

    goto :goto_0

    .line 802
    nop

    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_0
        0x31200000 -> :sswitch_3
        0x31300000 -> :sswitch_1
        0x31400000 -> :sswitch_2
        0x31500000 -> :sswitch_4
    .end sparse-switch
.end method

.method public saveToDB()V
    .locals 6

    .prologue
    .line 987
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    if-eqz v3, :cond_0

    .line 989
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->open()Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 992
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 993
    .local v2, "tempInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "JW saveToDB: mButtonInfoList.size()="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 994
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 1001
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1023
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->close()V

    .line 1024
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 1026
    .end local v1    # "i":I
    .end local v2    # "tempInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;>;"
    :cond_0
    return-void

    .line 996
    .restart local v1    # "i":I
    .restart local v2    # "tempInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;>;"
    :cond_1
    const/4 v4, 0x0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    invoke-virtual {v2, v4, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 994
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1001
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    .line 1003
    .local v0, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    goto :goto_1

    .line 1006
    :sswitch_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRecentStickerButtonInfo(I)V

    goto :goto_1

    .line 1009
    :sswitch_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRecentLabelButtonInfo(I)V

    goto :goto_1

    .line 1012
    :sswitch_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRecentWaterMarkButtonInfo(I)V

    goto :goto_1

    .line 1015
    :sswitch_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRecentFrameButtonInfo(I)V

    goto :goto_1

    .line 1018
    :sswitch_4
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRecentStampButtonInfo(I)V

    goto :goto_1

    .line 1003
    nop

    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_0
        0x31200000 -> :sswitch_3
        0x31300000 -> :sswitch_1
        0x31400000 -> :sswitch_2
        0x31500000 -> :sswitch_4
    .end sparse-switch
.end method

.method public setCurrentType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 960
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mCurrentType:I

    .line 961
    return-void
.end method

.method public setDrawerHandleVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    .line 1905
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_0

    .line 1907
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    const v2, 0x7f090150

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1908
    .local v0, "handleImage":Landroid/widget/ImageView;
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1910
    .end local v0    # "handleImage":Landroid/widget/ImageView;
    :cond_0
    return-void
.end method

.method public setFrameMode(I)Z
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 1164
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantFrameInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantFrameInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;->frameListener:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnFrameCallback;

    if-eqz v0, :cond_0

    .line 1165
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantFrameInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantFrameInfo;->frameListener:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnFrameCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnFrameCallback;->setFrame(I)Z

    move-result v0

    .line 1166
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setImageContext(I)V
    .locals 6
    .param p1, "type"    # I

    .prologue
    const/4 v5, 0x1

    .line 1171
    if-eq p1, v5, :cond_0

    const/16 v4, 0x9

    if-ne p1, v4, :cond_1

    .line 1172
    :cond_0
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mIsRecently:Z

    .line 1175
    :goto_0
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mCurrentType:I

    .line 1187
    const/4 v0, 0x0

    .line 1188
    .local v0, "hSpacing":I
    const/4 v3, 0x0

    .line 1189
    .local v3, "vSpacing":I
    const/4 v1, 0x0

    .line 1190
    .local v1, "margin":I
    const/4 v2, 0x0

    .line 1191
    .local v2, "side_margin":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050035

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1192
    move v2, v1

    .line 1277
    return-void

    .line 1174
    .end local v0    # "hSpacing":I
    .end local v1    # "margin":I
    .end local v2    # "side_margin":I
    .end local v3    # "vSpacing":I
    :cond_1
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mIsRecently:Z

    goto :goto_0
.end method

.method public setLabelMode(II)V
    .locals 1
    .param p1, "viewId"    # I
    .param p2, "styleNum"    # I

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantLabelInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;

    if-eqz v0, :cond_0

    .line 1161
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantLabelInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantLabelInfo;->labelListener:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnLabelCallback;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnLabelCallback;->setLabel(II)V

    .line 1162
    :cond_0
    return-void
.end method

.method public setRecentButton(I)V
    .locals 11
    .param p1, "buttonId"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 721
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 723
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 724
    .local v8, "size":I
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_RECENTLY:I

    if-lt v8, v0, :cond_0

    .line 726
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    add-int/lit8 v1, v8, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 729
    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 730
    iget p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mRecentBtnId:I

    .line 732
    :cond_1
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    move-object v1, p0

    move v2, p1

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;IIII)V

    invoke-virtual {v10, v3, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 733
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW setRecentButton: mButtonInfoList.size="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 736
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    if-eqz v0, :cond_2

    .line 738
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->open()Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 741
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 742
    .local v9, "tempInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW saveToDB: mButtonInfoList.size()="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 743
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v7, v0, :cond_3

    .line 749
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_4

    .line 771
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->close()V

    .line 772
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 773
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mCurrentType:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    .line 779
    .end local v7    # "i":I
    .end local v8    # "size":I
    .end local v9    # "tempInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;>;"
    :cond_2
    return-void

    .line 745
    .restart local v7    # "i":I
    .restart local v8    # "size":I
    .restart local v9    # "tempInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;>;"
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    invoke-virtual {v9, v3, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 743
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 749
    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    .line 751
    .local v6, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_1

    .line 754
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRecentStickerButtonInfo(I)V

    goto :goto_1

    .line 757
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRecentLabelButtonInfo(I)V

    goto :goto_1

    .line 760
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRecentWaterMarkButtonInfo(I)V

    goto :goto_1

    .line 763
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRecentFrameButtonInfo(I)V

    goto :goto_1

    .line 766
    :sswitch_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRecentStampButtonInfo(I)V

    goto :goto_1

    .line 751
    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_0
        0x31200000 -> :sswitch_3
        0x31300000 -> :sswitch_1
        0x31400000 -> :sswitch_2
        0x31500000 -> :sswitch_4
    .end sparse-switch
.end method

.method public setRecentClicked(ZII)V
    .locals 0
    .param p1, "isClicked"    # Z
    .param p2, "id"    # I
    .param p3, "recentIdx"    # I

    .prologue
    .line 840
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mRecentClicked:Z

    .line 842
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mRecentBtnId:I

    .line 843
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mRecentIdx:I

    .line 844
    return-void
.end method

.method public setRecentId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 836
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mRecentBtnId:I

    .line 837
    return-void
.end method

.method public setStampMode(I)Z
    .locals 1
    .param p1, "viewId"    # I

    .prologue
    .line 1154
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStampInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;

    if-eqz v0, :cond_0

    .line 1155
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStampInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStampInfo;->stampListener:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnStampCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnStampCallback;->setStamp(I)Z

    move-result v0

    .line 1157
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setStickerLongMode(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 1150
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;

    if-eqz v0, :cond_0

    .line 1151
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;->stickerListener:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnTempStickerCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnTempStickerCallback;->setLongClickSticker(I)V

    .line 1152
    :cond_0
    return-void
.end method

.method public setStickerMode(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 1146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;

    if-eqz v0, :cond_0

    .line 1147
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$AssistantStickerInfo;->stickerListener:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnTempStickerCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnTempStickerCallback;->setSticker(I)V

    .line 1148
    :cond_0
    return-void
.end method

.method public setTab()V
    .locals 8

    .prologue
    const/4 v2, 0x3

    const/4 v7, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 441
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout$LayoutParams;

    .line 444
    .local v6, "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 636
    .end local v6    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    :goto_0
    return-void

    .line 447
    .restart local v6    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 448
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 449
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 450
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE3:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 452
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 453
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 454
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 455
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE3:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 457
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 458
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 459
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 461
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_WATERMARK_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 462
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_WATERMARK_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 464
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 465
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 466
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 469
    iget v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mOrgTabLayoutHeight:I

    if-ne v0, v1, :cond_1

    .line 471
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 473
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4, v4}, Landroid/widget/FrameLayout;->measure(II)V

    .line 474
    iget v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 475
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 480
    :cond_1
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    const/4 v2, 0x4

    const/4 v3, 0x4

    new-array v4, v3, [I

    fill-array-data v4, :array_0

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;-><init>(Landroid/app/Activity;ILcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;[ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    .line 481
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    goto/16 :goto_0

    .line 486
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 487
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 488
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 489
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE3:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 491
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 492
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 493
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 494
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE3:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 496
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 497
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 498
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 500
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_WATERMARK_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 501
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_WATERMARK_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 503
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 504
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 505
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 508
    iget v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mOrgTabLayoutHeight:I

    if-eq v0, v1, :cond_0

    .line 510
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4, v4}, Landroid/widget/FrameLayout;->measure(II)V

    .line 513
    iget v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 514
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 522
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 523
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 524
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 525
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE3:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 527
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 528
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 529
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 530
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE3:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 532
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 533
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 534
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 536
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_WATERMARK_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 537
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_WATERMARK_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 539
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 540
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 541
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 544
    iget v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mOrgTabLayoutHeight:I

    if-ne v0, v1, :cond_2

    .line 546
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_2

    .line 548
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4, v4}, Landroid/widget/FrameLayout;->measure(II)V

    .line 549
    iget v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 550
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 554
    :cond_2
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-array v4, v2, [I

    fill-array-data v4, :array_1

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;-><init>(Landroid/app/Activity;ILcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;[ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    .line 555
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    goto/16 :goto_0

    .line 560
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 561
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 562
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 563
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE3:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 565
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 566
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 567
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 568
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE3:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 570
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 571
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 572
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 574
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_WATERMARK_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 575
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_WATERMARK_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 577
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 578
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 579
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 582
    iget v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mOrgTabLayoutHeight:I

    if-eq v0, v1, :cond_3

    .line 584
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_3

    .line 586
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4, v4}, Landroid/widget/FrameLayout;->measure(II)V

    .line 587
    iget v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 588
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 593
    :cond_3
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-array v4, v7, [I

    fill-array-data v4, :array_2

    move v2, v7

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;-><init>(Landroid/app/Activity;ILcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;[ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    goto/16 :goto_0

    .line 598
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 599
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 600
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 601
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE3:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 603
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 604
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 605
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 606
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE3:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 608
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 609
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 610
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 612
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_WATERMARK_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 613
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_WATERMARK_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 615
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_RECENTLY:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 616
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_TYPE1:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 617
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_TYPE2:I

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 620
    iget v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mOrgTabLayoutHeight:I

    if-eq v0, v1, :cond_0

    .line 622
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 624
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4, v4}, Landroid/widget/FrameLayout;->measure(II)V

    .line 625
    iget v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDrawerHandle:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 626
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 444
    nop

    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_0
        0x31200000 -> :sswitch_1
        0x31300000 -> :sswitch_2
        0x31400000 -> :sswitch_3
        0x31500000 -> :sswitch_4
    .end sparse-switch

    .line 480
    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
    .end array-data

    .line 554
    :array_1
    .array-data 4
        0x9
        0xa
        0xb
    .end array-data

    .line 593
    :array_2
    .array-data 4
        0xc
        0xd
    .end array-data
.end method

.method public setTabIndex(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 641
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 642
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v0, p1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 644
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 665
    :goto_0
    :sswitch_0
    return-void

    .line 646
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 649
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    add-int/lit8 v1, v1, -0xe

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 652
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    add-int/lit8 v1, v1, -0x9

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 658
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    add-int/lit8 v1, v1, -0x5

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 644
    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_1
        0x31200000 -> :sswitch_4
        0x31300000 -> :sswitch_3
        0x31400000 -> :sswitch_0
        0x31500000 -> :sswitch_2
    .end sparse-switch
.end method

.method public setThumbnailContext(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 1286
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mCurrentType:I

    .line 1396
    return-void
.end method

.method public setVisibilityDirectly(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 980
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 984
    :goto_0
    return-void

    .line 983
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationTabContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public setmTestInterface(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;)V
    .locals 0
    .param p1, "mTestInterface"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;

    .prologue
    .line 1881
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;

    .line 1882
    return-void
.end method

.method public showTabView(II[IIIZ)V
    .locals 10
    .param p1, "assistantType"    # I
    .param p2, "viewWidth"    # I
    .param p3, "data"    # [I
    .param p4, "orgWidth"    # I
    .param p5, "orgHeight"    # I
    .param p6, "doOpen"    # Z

    .prologue
    .line 902
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mCurrentType:I

    .line 904
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x31200000

    if-ne v0, v1, :cond_5

    .line 906
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampsOrFrames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 908
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 909
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    .line 911
    :cond_0
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->makeFrameThumbnail(I[III)V

    .line 914
    const/4 v9, 0x0

    .local v9, "a":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v9, v0, :cond_4

    .line 918
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    .line 921
    .end local v9    # "a":I
    :cond_1
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    const/4 v2, 0x4

    const/4 v3, 0x4

    new-array v4, v3, [I

    fill-array-data v4, :array_0

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;-><init>(Landroid/app/Activity;ILcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;[ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    .line 922
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 946
    :cond_2
    :goto_1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setThumbnailContext(I)V

    .line 948
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setVisibilityDirectly(I)V

    .line 950
    if-eqz p6, :cond_3

    .line 951
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->openDrawer()V

    .line 954
    :cond_3
    return-void

    .line 916
    .restart local v9    # "a":I
    :cond_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampsOrFrames:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->getThumbnailId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->initFrame(I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 914
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 924
    .end local v9    # "a":I
    :cond_5
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x31500000

    if-ne v0, v1, :cond_2

    .line 926
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampsOrFrames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_6

    .line 928
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_2
    sget v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE1:I

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE2:I

    add-int/2addr v0, v1

    if-le v2, v0, :cond_7

    .line 934
    const/4 v9, 0x0

    .restart local v9    # "a":I
    :goto_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v9, v0, :cond_8

    .line 939
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    .line 942
    .end local v2    # "i":I
    .end local v9    # "a":I
    :cond_6
    new-instance v3, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    const/4 v5, 0x3

    const/4 v0, 0x3

    new-array v7, v0, [I

    fill-array-data v7, :array_1

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    move-object v6, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;-><init>(Landroid/app/Activity;ILcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;[ILjava/util/ArrayList;)V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    .line 943
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPagerAdapter:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    goto :goto_1

    .line 930
    .restart local v2    # "i":I
    :cond_7
    const/4 v1, 0x1

    move-object v0, p0

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->makeStampThumbnail(ZII[III)V

    .line 928
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 936
    .restart local v9    # "a":I
    :cond_8
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampsOrFrames:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mStampList:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->getThumbnailId()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getBasicThumbnail()[I

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/mimage/photoretouching/Gui/FrameButtonLayout;->initStamp(I[I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 934
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 921
    :array_0
    .array-data 4
        0x5
        0x6
        0x7
        0x8
    .end array-data

    .line 942
    :array_1
    .array-data 4
        0xe
        0xf
        0x10
    .end array-data
.end method

.method public showTabView(IZ)V
    .locals 1
    .param p1, "assistantType"    # I
    .param p2, "doOpen"    # Z

    .prologue
    .line 892
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mCurrentType:I

    .line 894
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setImageContext(I)V

    .line 896
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setVisibilityDirectly(I)V

    .line 897
    if-eqz p2, :cond_0

    .line 898
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->openDrawer()V

    .line 899
    :cond_0
    return-void
.end method
