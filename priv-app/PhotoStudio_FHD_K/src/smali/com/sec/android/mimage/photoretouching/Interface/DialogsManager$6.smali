.class Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$6;
.super Ljava/lang/Object;
.source "DialogsManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->initDlgKeyListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1949
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 19
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1954
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1955
    const/4 v6, 0x0

    .line 2073
    :goto_0
    return v6

    .line 1957
    :cond_0
    const/16 v6, 0x42

    move/from16 v0, p2

    if-ne v0, v6, :cond_4

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_4

    .line 1959
    const/4 v12, 0x0

    .line 1960
    .local v12, "buttonList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;>;"
    const/16 v16, 0x0

    .line 1961
    .local v16, "mDialogInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    const/16 v17, 0x0

    .line 1962
    .local v17, "mSaveBtn":Landroid/widget/Button;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_7

    .line 1970
    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    if-eqz v6, :cond_2

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    if-eqz v6, :cond_2

    .line 1971
    move-object/from16 v0, v16

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    check-cast v10, Landroid/app/AlertDialog;

    .line 1972
    .local v10, "aDialog":Landroid/app/AlertDialog;
    const/4 v6, -0x1

    invoke-virtual {v10, v6}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v17

    .line 1974
    .end local v10    # "aDialog":Landroid/app/AlertDialog;
    :cond_2
    const/16 v18, 0x0

    .line 1975
    .local v18, "selectedButton":Landroid/widget/LinearLayout;
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_8

    .line 2031
    if-eqz v18, :cond_4

    .line 2033
    const-wide/16 v2, 0x0

    .line 2034
    .local v2, "upTime":J
    const-wide/16 v4, 0x0

    .line 2035
    .local v4, "eventTime":J
    const/4 v15, 0x0

    .line 2037
    .local v15, "e":Landroid/view/MotionEvent;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 2038
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 2040
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 2039
    invoke-static/range {v2 .. v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v15

    .line 2041
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2043
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 2044
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 2046
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 2045
    invoke-static/range {v2 .. v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v15

    .line 2047
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2052
    .end local v2    # "upTime":J
    .end local v4    # "eventTime":J
    .end local v12    # "buttonList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;>;"
    .end local v15    # "e":Landroid/view/MotionEvent;
    .end local v16    # "mDialogInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    .end local v17    # "mSaveBtn":Landroid/widget/Button;
    .end local v18    # "selectedButton":Landroid/widget/LinearLayout;
    :cond_4
    const/4 v6, 0x4

    move/from16 v0, p2

    if-ne v0, v6, :cond_6

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_6

    .line 2054
    const/4 v12, 0x0

    .line 2055
    .restart local v12    # "buttonList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;>;"
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_d

    .line 2069
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 2073
    .end local v12    # "buttonList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;>;"
    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 1962
    .restart local v12    # "buttonList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;>;"
    .restart local v16    # "mDialogInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    .restart local v17    # "mSaveBtn":Landroid/widget/Button;
    :cond_7
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 1964
    .local v14, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget-object v7, v14, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    move-object/from16 v0, p1

    if-ne v7, v0, :cond_1

    .line 1966
    iget-object v12, v14, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    .line 1967
    move-object/from16 v16, v14

    goto/16 :goto_1

    .line 1975
    .end local v14    # "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    .restart local v18    # "selectedButton":Landroid/widget/LinearLayout;
    :cond_8
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    .line 1977
    .local v11, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iget v6, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    const/16 v8, 0x800

    if-eq v6, v8, :cond_9

    iget v6, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    const/16 v8, 0x820

    if-eq v6, v8, :cond_9

    iget v6, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    const/16 v8, 0x810

    if-ne v6, v8, :cond_a

    .line 1979
    :cond_9
    iget-object v6, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isRadioBtnFocused()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1981
    iget-object v0, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    .line 1982
    invoke-virtual/range {v17 .. v17}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_2

    .line 1985
    :cond_a
    iget v6, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    const/16 v8, 0x120

    if-ne v6, v8, :cond_b

    .line 1987
    iget-object v6, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isLastBtnFocused()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1989
    iget-object v0, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    .line 1990
    invoke-virtual/range {v17 .. v17}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_2

    .line 1993
    :cond_b
    iget v6, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    const/16 v8, 0x300

    if-ne v6, v8, :cond_c

    .line 1995
    iget-object v6, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isCheckBoxFocused()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1997
    iget-object v0, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    .line 1999
    iget-object v6, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    const v8, 0x7f09001d

    invoke-virtual {v6, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/CheckBox;

    .line 2000
    .local v13, "checkBox":Landroid/widget/CheckBox;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "sj, DM - onKey() - checkBox : "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2001
    if-eqz v13, :cond_3

    .line 2003
    const-wide/16 v2, 0x0

    .line 2004
    .restart local v2    # "upTime":J
    const-wide/16 v4, 0x0

    .line 2005
    .restart local v4    # "eventTime":J
    const/4 v15, 0x0

    .line 2007
    .restart local v15    # "e":Landroid/view/MotionEvent;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 2008
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 2010
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 2009
    invoke-static/range {v2 .. v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v15

    .line 2011
    invoke-virtual {v13, v15}, Landroid/widget/CheckBox;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2013
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 2014
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 2016
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 2015
    invoke-static/range {v2 .. v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v15

    .line 2017
    invoke-virtual {v13, v15}, Landroid/widget/CheckBox;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2019
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 2025
    .end local v2    # "upTime":J
    .end local v4    # "eventTime":J
    .end local v13    # "checkBox":Landroid/widget/CheckBox;
    .end local v15    # "e":Landroid/view/MotionEvent;
    :cond_c
    iget-object v6, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2027
    iget-object v0, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    goto/16 :goto_2

    .line 2055
    .end local v11    # "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    .end local v16    # "mDialogInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    .end local v17    # "mSaveBtn":Landroid/widget/Button;
    .end local v18    # "selectedButton":Landroid/widget/LinearLayout;
    :cond_d
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 2057
    .restart local v14    # "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget-object v7, v14, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    move-object/from16 v0, p1

    if-ne v7, v0, :cond_5

    .line 2059
    iget-object v12, v14, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    .line 2060
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_e
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    .line 2062
    .restart local v11    # "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iget v8, v11, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    const/16 v9, 0x900

    if-ne v8, v9, :cond_e

    .line 2064
    iget-object v8, v14, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v8, v8, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->listener:Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-interface {v8, v0, v1}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    goto :goto_3
.end method
