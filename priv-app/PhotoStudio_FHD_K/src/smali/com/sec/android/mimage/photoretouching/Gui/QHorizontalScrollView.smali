.class public Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
.super Landroid/widget/HorizontalScrollView;
.source "QHorizontalScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;
    }
.end annotation


# instance fields
.field mScrollViewCallback:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->mScrollViewCallback:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;

    .line 25
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->mScrollViewCallback:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;

    .line 30
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->mScrollViewCallback:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;

    .line 36
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 37
    return-void
.end method


# virtual methods
.method protected computeHorizontalScrollRange()I
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->computeHorizontalScrollRange()I

    move-result v0

    return v0
.end method

.method public onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 53
    invoke-super/range {p0 .. p5}, Landroid/widget/HorizontalScrollView;->onLayout(ZIIII)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->mScrollViewCallback:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->mScrollViewCallback:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;->onLayoutCallback()V

    .line 56
    :cond_0
    return-void
.end method

.method public setScrollViewCallback(Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->mScrollViewCallback:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;

    .line 42
    return-void
.end method
