.class Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;
.super Ljava/lang/Object;
.source "EffectView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 282
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/high16 v2, 0x16000000

    const/4 v3, 0x1

    .line 214
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 271
    :goto_0
    return-void

    .line 217
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 218
    .local v0, "viewId":I
    instance-of v1, p1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v1, :cond_2

    move-object v1, p1

    .line 219
    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getButtonType()I

    move-result v1

    const v2, 0x160016ff

    if-ne v1, v2, :cond_1

    .line 221
    const-string v1, "sj, EV - effect download button"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 223
    invoke-virtual {p1, v3}, Landroid/view/View;->setSelected(Z)V

    .line 270
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->disableButtonsForCollage()V
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$26(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    goto :goto_0

    .line 226
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->checkHelpPopup(I)V
    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;I)V

    .line 227
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 228
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 230
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 231
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 232
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 233
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 236
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    invoke-static {v1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;I)V

    .line 237
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentEffectType:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 239
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 240
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->init(I)V

    .line 243
    :cond_5
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->init()V

    .line 245
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->init3DepthActionBar()V
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 246
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 247
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v1, :cond_7

    .line 249
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 256
    :goto_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initMiddleButton()V
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 257
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->setMainBtnListener()V
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 259
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setClearSelectedSubButton()V

    .line 260
    invoke-virtual {p1, v3}, Landroid/view/View;->setSelected(Z)V

    .line 261
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setRecentButton(Landroid/view/View;)V

    .line 264
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 265
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->killThread()V

    .line 266
    :cond_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;)V

    .line 267
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    invoke-direct {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;)V

    .line 268
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->start()V

    goto/16 :goto_1

    .line 253
    :cond_7
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    goto :goto_2
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 276
    return-void
.end method
