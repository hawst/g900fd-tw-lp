.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$13;
.super Ljava/lang/Object;
.source "PenActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->newSpenListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$13;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    .line 1114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCommit(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 2
    .param p1, "page"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    .line 1117
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$13;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$13;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mUndoStarckEmpty:Z
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$27(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->refreshUndoRedoBtnState(Z)V

    .line 1118
    return-void
.end method

.method public onRedoable(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)V
    .locals 0
    .param p1, "arg0"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "arg1"    # Z

    .prologue
    .line 1123
    return-void
.end method

.method public onUndoable(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)V
    .locals 0
    .param p1, "arg0"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "arg1"    # Z

    .prologue
    .line 1128
    return-void
.end method
