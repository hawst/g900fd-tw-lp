.class public Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
.super Landroid/widget/LinearLayout;
.source "BottomThumbnailButtonLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$AsyncCallback;,
        Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;
    }
.end annotation


# static fields
.field public static final LEFT:I = 0x1

.field public static final MIDDLE:I = 0x0

.field public static final RIGHT:I = 0x2


# instance fields
.field private mButtonId:I

.field private mButtonType:I

.field private mContext:Landroid/content/Context;

.field private mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

.field private mLayout:Landroid/view/ViewGroup;

.field private mSelected:Z

.field private mTempThumb:[I

.field private mTexture1:[I

.field private mTexture2:[I

.field private mThumbnail:Landroid/graphics/Bitmap;

.field private mThumbnailAsync:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;

.field private mThumbnailHeight:I

.field private mThumbnailWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "effectEffect"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    .param p3, "side"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 588
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mSelected:Z

    .line 589
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mButtonId:I

    .line 591
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mButtonType:I

    .line 592
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    .line 593
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    .line 594
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTempThumb:[I

    .line 595
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailWidth:I

    .line 596
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailHeight:I

    .line 597
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 599
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .line 600
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailAsync:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;

    .line 41
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    .line 42
    const v0, 0x7f030034

    invoke-static {p1, v0, p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 44
    invoke-virtual {p0, p3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setPadding(I)V

    .line 46
    instance-of v0, p2, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    if-eqz v0, :cond_0

    .line 47
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .line 48
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)I
    .locals 1

    .prologue
    .line 591
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mButtonType:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture2:[I

    return-object v0
.end method

.method private applyEffectThumbnailAsync(Landroid/widget/ImageView;[III)V
    .locals 7
    .param p1, "icon"    # Landroid/widget/ImageView;
    .param p2, "in"    # [I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 262
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailAsync:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;

    .line 263
    new-instance v6, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;

    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move v4, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;Landroid/widget/ImageView;II[I)V

    invoke-direct {v6, p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$AsyncCallback;)V

    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailAsync:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;

    .line 452
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailAsync:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 453
    return-void
.end method

.method private getText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 168
    const/4 v0, 0x0

    .line 169
    .local v0, "ret":Ljava/lang/String;
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mButtonType:I

    packed-switch v1, :pswitch_data_0

    .line 257
    :goto_0
    :pswitch_0
    return-object v0

    .line 173
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f060044

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 174
    goto :goto_0

    .line 176
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f060046

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 177
    goto :goto_0

    .line 179
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f0601d0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 180
    goto :goto_0

    .line 182
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f060047

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 183
    goto :goto_0

    .line 185
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f060138

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 186
    goto :goto_0

    .line 188
    :pswitch_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f06004d

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 189
    goto :goto_0

    .line 191
    :pswitch_7
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f0600f4

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 192
    goto :goto_0

    .line 194
    :pswitch_8
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f06015e

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 195
    goto :goto_0

    .line 197
    :pswitch_9
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f060139

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 198
    goto :goto_0

    .line 200
    :pswitch_a
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f06012f

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 201
    goto :goto_0

    .line 203
    :pswitch_b
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f0600ec

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 204
    goto :goto_0

    .line 206
    :pswitch_c
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f0600ef

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 207
    goto :goto_0

    .line 209
    :pswitch_d
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f060165

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 210
    goto/16 :goto_0

    .line 212
    :pswitch_e
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f0600cc

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 213
    goto/16 :goto_0

    .line 215
    :pswitch_f
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f0600c9

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 216
    goto/16 :goto_0

    .line 218
    :pswitch_10
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f0600ce

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 219
    goto/16 :goto_0

    .line 221
    :pswitch_11
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f06018c

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 222
    goto/16 :goto_0

    .line 224
    :pswitch_12
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f06004c

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 225
    goto/16 :goto_0

    .line 227
    :pswitch_13
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f060151

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 228
    goto/16 :goto_0

    .line 230
    :pswitch_14
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f06015a

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 231
    goto/16 :goto_0

    .line 233
    :pswitch_15
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f0600d2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 234
    goto/16 :goto_0

    .line 236
    :pswitch_16
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f06012e

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 237
    goto/16 :goto_0

    .line 239
    :pswitch_17
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f060050

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 240
    goto/16 :goto_0

    .line 242
    :pswitch_18
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f06013b

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 243
    goto/16 :goto_0

    .line 245
    :pswitch_19
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f06004e

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 246
    goto/16 :goto_0

    .line 248
    :pswitch_1a
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f0601fb

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 249
    goto/16 :goto_0

    .line 251
    :pswitch_1b
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f060055

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 252
    goto/16 :goto_0

    .line 254
    :pswitch_1c
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v2, 0x7f06018e

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 169
    nop

    :pswitch_data_0
    .packed-switch 0x16001600
        :pswitch_13
        :pswitch_1
        :pswitch_10
        :pswitch_11
        :pswitch_3
        :pswitch_4
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_e
        :pswitch_f
        :pswitch_12
        :pswitch_6
        :pswitch_14
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch
.end method


# virtual methods
.method public configurationChanged()V
    .locals 9

    .prologue
    const v7, 0x7f090004

    const/4 v8, 0x0

    .line 538
    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 541
    .local v1, "icon":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->removeAllViews()V

    .line 543
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    const v6, 0x7f030034

    invoke-static {v5, v6, p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 545
    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "icon":Landroid/widget/ImageView;
    check-cast v1, Landroid/widget/ImageView;

    .line 547
    .restart local v1    # "icon":Landroid/widget/ImageView;
    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 548
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v5

    if-nez v5, :cond_2

    .line 549
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 553
    :goto_0
    const v5, 0x7f090005

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 554
    .local v2, "tView":Landroid/widget/TextView;
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 555
    sget-object v5, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 558
    const-string v5, "sans-serif-regular"

    invoke-static {v5, v8}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 559
    .local v0, "font":Landroid/graphics/Typeface;
    if-eqz v2, :cond_0

    .line 560
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 563
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->getText()Ljava/lang/String;

    move-result-object v3

    .line 564
    .local v3, "text":Ljava/lang/String;
    if-eqz v2, :cond_1

    if-eqz v3, :cond_1

    .line 566
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 570
    :cond_1
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mSelected:Z

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setSelected(Z)V

    .line 571
    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 572
    .local v4, "v":Landroid/view/View;
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mSelected:Z

    invoke-virtual {v4, v5}, Landroid/view/View;->setSelected(Z)V

    .line 573
    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setPressed(Z)V

    .line 574
    return-void

    .line 551
    .end local v0    # "font":Landroid/graphics/Typeface;
    .end local v2    # "tView":Landroid/widget/TextView;
    .end local v3    # "text":Ljava/lang/String;
    .end local v4    # "v":Landroid/view/View;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTempThumb:[I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailWidth:I

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailHeight:I

    invoke-direct {p0, v1, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->applyEffectThumbnailAsync(Landroid/widget/ImageView;[III)V

    goto :goto_0
.end method

.method public destroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 118
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailAsync:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;

    if-eqz v1, :cond_0

    .line 119
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailAsync:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;->cancel(Z)Z

    .line 120
    :cond_0
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailAsync:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;

    .line 122
    const v1, 0x7f090004

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 123
    .local v0, "icon":Landroid/widget/ImageView;
    if-eqz v0, :cond_1

    .line 125
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 127
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 129
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 131
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 134
    :cond_2
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    .line 135
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTempThumb:[I

    .line 136
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 137
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 139
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    .line 140
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .line 141
    return-void
.end method

.method public getIconType()I
    .locals 1

    .prologue
    .line 577
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mButtonType:I

    return v0
.end method

.method public getLayoutHeight()I
    .locals 4

    .prologue
    .line 152
    const/4 v2, 0x0

    .line 153
    .local v2, "ret":I
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 154
    .local v0, "layout":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 155
    .local v1, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 156
    return v2
.end method

.method public getLayoutWidth()I
    .locals 4

    .prologue
    .line 144
    const/4 v2, 0x0

    .line 145
    .local v2, "ret":I
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 146
    .local v0, "layout":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 147
    .local v1, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v2, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 148
    return v2
.end method

.method public getThumbnail()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getThumbnailBuffer()[I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTempThumb:[I

    return-object v0
.end method

.method public getThumbnailHeight()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailHeight:I

    return v0
.end method

.method public getThumbnailWidth()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailWidth:I

    return v0
.end method

.method public init(I)V
    .locals 7
    .param p1, "buttonType"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mButtonType:I

    .line 94
    const v4, 0x7f090004

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 95
    .local v1, "icon":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTempThumb:[I

    if-eqz v4, :cond_0

    .line 97
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTempThumb:[I

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailWidth:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailHeight:I

    invoke-direct {p0, v1, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->applyEffectThumbnailAsync(Landroid/widget/ImageView;[III)V

    .line 99
    :cond_0
    const v4, 0x7f090005

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 102
    .local v2, "tView":Landroid/widget/TextView;
    const-string v4, "sans-serif-regular"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 103
    .local v0, "font":Landroid/graphics/Typeface;
    if-eqz v2, :cond_1

    .line 104
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 107
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->getText()Ljava/lang/String;

    move-result-object v3

    .line 108
    .local v3, "text":Ljava/lang/String;
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 110
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    :cond_2
    return-void
.end method

.method public isFocused()Z
    .locals 3

    .prologue
    .line 527
    const v1, 0x7f090070

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 528
    .local v0, "icon":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 529
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sj, BTBL - isFocused() - icon.isFocused() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/widget/ImageView;->isFocused()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 530
    invoke-virtual {v0}, Landroid/widget/ImageView;->isFocused()Z

    move-result v1

    .line 533
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mLayout:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->isFocused()Z

    move-result v1

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 2
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 519
    invoke-super {p0, p1, p2, p3}, Landroid/widget/LinearLayout;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 520
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sj, onFocusChanged() - gainFocus : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / rect : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 521
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    .prologue
    .line 480
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 482
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sj, enabled : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 483
    return-void
.end method

.method public setFocusable(Z)V
    .locals 2
    .param p1, "focusable"    # Z

    .prologue
    .line 511
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 512
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sj, focusable : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 513
    return-void
.end method

.method public setPadding(I)V
    .locals 4
    .param p1, "side"    # I

    .prologue
    const/4 v3, 0x0

    .line 51
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mLayout:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05024b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 53
    .local v0, "padding":I
    packed-switch p1, :pswitch_data_0

    .line 68
    .end local v0    # "padding":I
    :cond_0
    :goto_0
    return-void

    .line 56
    .restart local v0    # "padding":I
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/view/ViewGroup;->setPadding(IIII)V

    goto :goto_0

    .line 59
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v0, v3, v0}, Landroid/view/ViewGroup;->setPadding(IIII)V

    goto :goto_0

    .line 62
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v0, v0, v0}, Landroid/view/ViewGroup;->setPadding(IIII)V

    goto :goto_0

    .line 53
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setPressed(Z)V
    .locals 2
    .param p1, "pressed"    # Z

    .prologue
    .line 504
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setPressed(Z)V

    .line 505
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sj, pressed : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 506
    return-void
.end method

.method public setSelected(Z)V
    .locals 3
    .param p1, "selected"    # Z

    .prologue
    .line 487
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mSelected:Z

    .line 488
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mSelected:Z

    invoke-super {p0, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 489
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sj, selected : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 491
    const v1, 0x7f090070

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 492
    .local v0, "iconOverlay":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 493
    if-eqz p1, :cond_1

    .line 494
    const v1, 0x7f0201bd

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 499
    :cond_0
    :goto_0
    return-void

    .line 496
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public setTexture1([I)V
    .locals 0
    .param p1, "texture"    # [I

    .prologue
    .line 581
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I

    .line 582
    return-void
.end method

.method public setTexture2([I)V
    .locals 0
    .param p1, "texture"    # [I

    .prologue
    .line 585
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture2:[I

    .line 586
    return-void
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "thumbnail"    # Landroid/graphics/Bitmap;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    .line 81
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    .line 82
    return-void
.end method

.method public setThumbnail([III)V
    .locals 1
    .param p1, "thumbnail"    # [I
    .param p2, "thumbnailWidth"    # I
    .param p3, "thumbnailHeight"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTempThumb:[I

    if-eqz v0, :cond_0

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTempThumb:[I

    .line 74
    :cond_0
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTempThumb:[I

    .line 75
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailWidth:I

    .line 76
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mThumbnailHeight:I

    .line 77
    return-void
.end method
