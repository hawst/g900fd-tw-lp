.class Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;
.super Ljava/lang/Object;
.source "AssistantLayoutManager.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    .line 1130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v2, 0x0

    .line 1135
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;Z)V

    .line 1136
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;)Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1138
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;)Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 1139
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;)Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mShowLayout:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1141
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;)Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1149
    :cond_0
    :goto_0
    return-void

    .line 1145
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;)Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentType:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 1146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;)Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1153
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;Z)V

    .line 1154
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;)Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1155
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;)Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 1156
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;Z)V

    .line 1161
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;)Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1162
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;)Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 1163
    :cond_0
    return-void
.end method
