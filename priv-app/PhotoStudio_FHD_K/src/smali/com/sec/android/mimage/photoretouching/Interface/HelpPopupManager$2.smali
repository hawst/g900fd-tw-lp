.class Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$2;
.super Ljava/lang/Object;
.source "HelpPopupManager.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->initCloseButtonKeyListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    .line 320
    const/16 v2, 0x42

    if-ne p2, v2, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 322
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getCurrentModeHelpPopup()Landroid/view/View;

    move-result-object v1

    .line 323
    .local v1, "currentModePopupLayout":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 325
    const v2, 0x7f09012b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 326
    .local v0, "closeBtn":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 328
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 330
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->touchCancelButton()V
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V

    .line 335
    .end local v0    # "closeBtn":Landroid/widget/LinearLayout;
    .end local v1    # "currentModePopupLayout":Landroid/view/View;
    :cond_0
    const/4 v2, 0x0

    return v2
.end method
