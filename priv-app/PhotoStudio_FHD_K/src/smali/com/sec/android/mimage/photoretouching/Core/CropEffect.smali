.class public Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
.super Ljava/lang/Object;
.source "CropEffect.java"


# static fields
.field private static CROP_MIN_SIZE:I


# instance fields
.field private final AVAILABLE_CROP_LASSO:I

.field private angle:I

.field private currentRect:Landroid/graphics/RectF;

.field private isAnimating:Z

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field mAnimationIntervalbottom:F

.field mAnimationIntervalleft:F

.field mAnimationIntervalright:F

.field mAnimationIntervaltop:F

.field private mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

.field private mClearPaint:Landroid/graphics/Paint;

.field private mContext:Landroid/content/Context;

.field private mCropDarkBackgroundPaint:Landroid/graphics/Paint;

.field private mCropFreeRoi:Landroid/graphics/Rect;

.field private mCropLassoPath:Landroid/graphics/Path;

.field private mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

.field private mDstDrawCanvasRect:Landroid/graphics/RectF;

.field private mFirstPt:Landroid/graphics/Point;

.field private mFixedHeight:I

.field private mFixedWidth:I

.field private mHandleBitmap:Landroid/graphics/Bitmap;

.field private mHandleBitmapPress:Landroid/graphics/Bitmap;

.field private mHandler_B:Landroid/graphics/Bitmap;

.field private mHandler_L:Landroid/graphics/Bitmap;

.field private mHandler_LB:Landroid/graphics/Bitmap;

.field private mHandler_LT:Landroid/graphics/Bitmap;

.field private mHandler_R:Landroid/graphics/Bitmap;

.field private mHandler_RB:Landroid/graphics/Bitmap;

.field private mHandler_RT:Landroid/graphics/Bitmap;

.field private mHandler_T:Landroid/graphics/Bitmap;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mLassoLinePaint_b:Landroid/graphics/Paint;

.field private mLassoLinePaint_w:Landroid/graphics/Paint;

.field private mPrePt:Landroid/graphics/Point;

.field private mRectInnerPaint:Landroid/graphics/Paint;

.field private mRectOuterPaint:Landroid/graphics/Paint;

.field private mSrcDrawCanvasRect:Landroid/graphics/Rect;

.field private mStartSelect:Z

.field private mTempHeight:I

.field private mTempPreviewMaskBuffer:[B

.field private mTempWidth:I

.field private mToggleRotate:Z

.field mTouchPressed:Z

.field private mTouchType:I

.field private rectToDraw:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1807
    const/16 v0, 0x3c

    sput v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->CROP_MIN_SIZE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 513
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervaltop:F

    .line 514
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalright:F

    .line 515
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalbottom:F

    .line 516
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalleft:F

    .line 1809
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1811
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    .line 1812
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mPrePt:Landroid/graphics/Point;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFirstPt:Landroid/graphics/Point;

    .line 1813
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    .line 1814
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mStartSelect:Z

    .line 1816
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    .line 1819
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    .line 1820
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    .line 1821
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mContext:Landroid/content/Context;

    .line 1823
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedWidth:I

    .line 1824
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedHeight:I

    .line 1825
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempWidth:I

    .line 1826
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempHeight:I

    .line 1827
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mToggleRotate:Z

    .line 1829
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_b:Landroid/graphics/Paint;

    .line 1830
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_w:Landroid/graphics/Paint;

    .line 1831
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropDarkBackgroundPaint:Landroid/graphics/Paint;

    .line 1832
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mClearPaint:Landroid/graphics/Paint;

    .line 1833
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectOuterPaint:Landroid/graphics/Paint;

    .line 1834
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectInnerPaint:Landroid/graphics/Paint;

    .line 1837
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    .line 1838
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmapPress:Landroid/graphics/Bitmap;

    .line 1840
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 1841
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 1842
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 1843
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 1844
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    .line 1845
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    .line 1846
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    .line 1847
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    .line 1849
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    .line 1852
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mSrcDrawCanvasRect:Landroid/graphics/Rect;

    .line 1853
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mDstDrawCanvasRect:Landroid/graphics/RectF;

    .line 1855
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->AVAILABLE_CROP_LASSO:I

    .line 1859
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->isAnimating:Z

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imgInfo"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 513
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervaltop:F

    .line 514
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalright:F

    .line 515
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalbottom:F

    .line 516
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalleft:F

    .line 1809
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1811
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    .line 1812
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mPrePt:Landroid/graphics/Point;

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFirstPt:Landroid/graphics/Point;

    .line 1813
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    .line 1814
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mStartSelect:Z

    .line 1816
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    .line 1819
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    .line 1820
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    .line 1821
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mContext:Landroid/content/Context;

    .line 1823
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedWidth:I

    .line 1824
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedHeight:I

    .line 1825
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempWidth:I

    .line 1826
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempHeight:I

    .line 1827
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mToggleRotate:Z

    .line 1829
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_b:Landroid/graphics/Paint;

    .line 1830
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_w:Landroid/graphics/Paint;

    .line 1831
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropDarkBackgroundPaint:Landroid/graphics/Paint;

    .line 1832
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mClearPaint:Landroid/graphics/Paint;

    .line 1833
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectOuterPaint:Landroid/graphics/Paint;

    .line 1834
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectInnerPaint:Landroid/graphics/Paint;

    .line 1837
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    .line 1838
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmapPress:Landroid/graphics/Bitmap;

    .line 1840
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 1841
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 1842
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 1843
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 1844
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    .line 1845
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    .line 1846
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    .line 1847
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    .line 1849
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    .line 1852
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mSrcDrawCanvasRect:Landroid/graphics/Rect;

    .line 1853
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mDstDrawCanvasRect:Landroid/graphics/RectF;

    .line 1855
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->AVAILABLE_CROP_LASSO:I

    .line 1859
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->isAnimating:Z

    .line 39
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mContext:Landroid/content/Context;

    .line 40
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/CropRectController;

    invoke-direct {v0, p1, p2, v4, v1}, Lcom/sec/android/mimage/photoretouching/Core/CropRectController;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;IZ)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    .line 41
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setAngle(I)V

    .line 42
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setRightTopRotate(Z)V

    .line 43
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mContext:Landroid/content/Context;

    sget v2, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->CROP_MIN_SIZE:I

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getDpToPixel(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setMinimumSize(I)V

    .line 44
    invoke-direct {p0, p1, p2, v3}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->initResource(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/graphics/Rect;)V

    .line 45
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mSrcDrawCanvasRect:Landroid/graphics/Rect;

    .line 46
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mDstDrawCanvasRect:Landroid/graphics/RectF;

    .line 47
    invoke-direct {p0, v4}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->calculateSrcToDstDrawCanvasRoi(Z)V

    .line 48
    invoke-virtual {p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempWidth:I

    .line 49
    invoke-virtual {p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempHeight:I

    .line 50
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->rectToDraw:Landroid/graphics/RectF;

    .line 51
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    .line 52
    return-void
.end method

.method private SetDestROI(Landroid/graphics/Rect;IIIII)V
    .locals 11
    .param p1, "input"    # Landroid/graphics/Rect;
    .param p2, "angle"    # I
    .param p3, "left"    # I
    .param p4, "right"    # I
    .param p5, "top"    # I
    .param p6, "bottom"    # I

    .prologue
    .line 1673
    iget v6, p1, Landroid/graphics/Rect;->left:I

    iget v7, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v7

    div-int/lit8 v0, v6, 0x2

    .line 1674
    .local v0, "offset_x":I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v6, v7

    div-int/lit8 v1, v6, 0x2

    .line 1677
    .local v1, "offset_y":I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    iget v7, p1, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    int-to-float v8, p2

    int-to-float v9, v0

    int-to-float v10, v1

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v2

    .line 1679
    .local v2, "pt1":Landroid/graphics/PointF;
    iget v6, p1, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    iget v7, p1, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    int-to-float v8, p2

    int-to-float v9, v0

    int-to-float v10, v1

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v3

    .line 1681
    .local v3, "pt2":Landroid/graphics/PointF;
    iget v6, p1, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    int-to-float v8, p2

    int-to-float v9, v0

    int-to-float v10, v1

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1683
    .local v4, "pt3":Landroid/graphics/PointF;
    iget v6, p1, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    int-to-float v8, p2

    int-to-float v9, v0

    int-to-float v10, v1

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v5

    .line 1685
    .local v5, "pt4":Landroid/graphics/PointF;
    iget v6, v5, Landroid/graphics/PointF;->x:F

    float-to-int v6, v6

    iput v6, p1, Landroid/graphics/Rect;->left:I

    .line 1686
    iget v6, v5, Landroid/graphics/PointF;->y:F

    float-to-int v6, v6

    iput v6, p1, Landroid/graphics/Rect;->top:I

    .line 1687
    iget v6, v3, Landroid/graphics/PointF;->x:F

    float-to-int v6, v6

    iput v6, p1, Landroid/graphics/Rect;->right:I

    .line 1688
    iget v6, v3, Landroid/graphics/PointF;->y:F

    float-to-int v6, v6

    iput v6, p1, Landroid/graphics/Rect;->bottom:I

    .line 1689
    return-void
.end method

.method private adjustRectWithRatio(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/graphics/Rect;)V
    .locals 12
    .param p1, "ii"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "roi"    # Landroid/graphics/Rect;

    .prologue
    .line 1260
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v4

    .line 1261
    .local v4, "rectWidth":I
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v3

    .line 1263
    .local v3, "rectHeight":I
    if-le v4, v3, :cond_1

    .line 1265
    int-to-float v9, v3

    iget v10, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedWidth:I

    int-to-float v10, v10

    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedHeight:I

    int-to-float v11, v11

    div-float/2addr v10, v11

    mul-float/2addr v9, v10

    float-to-int v8, v9

    .line 1267
    .local v8, "tempWidth":I
    if-le v8, v4, :cond_0

    .line 1269
    sub-int v2, v8, v4

    .line 1270
    .local v2, "diff":I
    iget v9, p2, Landroid/graphics/Rect;->right:I

    add-int/2addr v9, v2

    iput v9, p2, Landroid/graphics/Rect;->right:I

    .line 1278
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v9

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v10

    sub-int/2addr v9, v10

    div-int/lit8 v0, v9, 0x2

    .line 1279
    .local v0, "absoluteMarginX":I
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v7

    .line 1281
    .local v7, "tempRectWidth":I
    iput v0, p2, Landroid/graphics/Rect;->left:I

    .line 1282
    iget v9, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr v9, v7

    iput v9, p2, Landroid/graphics/Rect;->right:I

    .line 1305
    .end local v0    # "absoluteMarginX":I
    .end local v7    # "tempRectWidth":I
    .end local v8    # "tempWidth":I
    :goto_1
    return-void

    .line 1274
    .end local v2    # "diff":I
    .restart local v8    # "tempWidth":I
    :cond_0
    sub-int v2, v4, v8

    .line 1275
    .restart local v2    # "diff":I
    iget v9, p2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v9, v2

    iput v9, p2, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 1286
    .end local v2    # "diff":I
    .end local v8    # "tempWidth":I
    :cond_1
    int-to-float v9, v4

    iget v10, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedHeight:I

    int-to-float v10, v10

    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedWidth:I

    int-to-float v11, v11

    div-float/2addr v10, v11

    mul-float/2addr v9, v10

    float-to-int v5, v9

    .line 1288
    .local v5, "tempHeight":I
    if-le v5, v3, :cond_2

    .line 1290
    sub-int v2, v5, v3

    .line 1291
    .restart local v2    # "diff":I
    iget v9, p2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v9, v2

    iput v9, p2, Landroid/graphics/Rect;->bottom:I

    .line 1299
    :goto_2
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v9

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v10

    sub-int/2addr v9, v10

    div-int/lit8 v1, v9, 0x2

    .line 1300
    .local v1, "absoluteMarginY":I
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v6

    .line 1302
    .local v6, "tempRectHeight":I
    iput v1, p2, Landroid/graphics/Rect;->top:I

    .line 1303
    iget v9, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v6

    iput v9, p2, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    .line 1295
    .end local v1    # "absoluteMarginY":I
    .end local v2    # "diff":I
    .end local v6    # "tempRectHeight":I
    :cond_2
    sub-int v2, v3, v5

    .line 1296
    .restart local v2    # "diff":I
    iget v9, p2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v2

    iput v9, p2, Landroid/graphics/Rect;->bottom:I

    goto :goto_2
.end method

.method private calculateOrgRect(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 39
    .param p1, "imgInfo"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    .line 1475
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getAngle()I

    move-result v35

    .line 1476
    .local v35, "rectAngle":I
    const/4 v6, 0x4

    new-array v0, v6, [Landroid/graphics/PointF;

    move-object/from16 v30, v0

    .line 1477
    .local v30, "pt":[Landroid/graphics/PointF;
    const/16 v27, 0x0

    .local v27, "min_x":F
    const/16 v25, 0x0

    .line 1478
    .local v25, "max_x":F
    const/16 v28, 0x0

    .local v28, "min_y":F
    const/16 v26, 0x0

    .line 1482
    .local v26, "max_y":F
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getOrgDestPt1()Landroid/graphics/PointF;

    move-result-object v7

    aput-object v7, v30, v6

    .line 1483
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getOrgDestPt2()Landroid/graphics/PointF;

    move-result-object v7

    aput-object v7, v30, v6

    .line 1484
    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getOrgDestPt3()Landroid/graphics/PointF;

    move-result-object v7

    aput-object v7, v30, v6

    .line 1485
    const/4 v6, 0x3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getOrgDestPt4()Landroid/graphics/PointF;

    move-result-object v7

    aput-object v7, v30, v6

    .line 1488
    const/4 v6, 0x0

    aget-object v6, v30, v6

    const/4 v7, 0x0

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v8

    div-float/2addr v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 1489
    const/4 v6, 0x0

    aget-object v6, v30, v6

    const/4 v7, 0x0

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v8

    div-float/2addr v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 1491
    const/4 v6, 0x1

    aget-object v6, v30, v6

    const/4 v7, 0x1

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v8

    div-float/2addr v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 1492
    const/4 v6, 0x1

    aget-object v6, v30, v6

    const/4 v7, 0x1

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v8

    div-float/2addr v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 1494
    const/4 v6, 0x2

    aget-object v6, v30, v6

    const/4 v7, 0x2

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v8

    div-float/2addr v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 1495
    const/4 v6, 0x2

    aget-object v6, v30, v6

    const/4 v7, 0x2

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v8

    div-float/2addr v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 1497
    const/4 v6, 0x3

    aget-object v6, v30, v6

    const/4 v7, 0x3

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v8

    div-float/2addr v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 1498
    const/4 v6, 0x3

    aget-object v6, v30, v6

    const/4 v7, 0x3

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v8

    div-float/2addr v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 1500
    const/4 v6, 0x0

    aget-object v6, v30, v6

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    .line 1501
    const/4 v6, 0x0

    aget-object v6, v30, v6

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v28, v0

    .line 1503
    const/4 v6, 0x1

    aget-object v6, v30, v6

    iget v6, v6, Landroid/graphics/PointF;->x:F

    const/4 v7, 0x0

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->x:F

    sub-float v33, v6, v7

    .line 1504
    .local v33, "ptStartWidthX":F
    const/4 v6, 0x1

    aget-object v6, v30, v6

    iget v6, v6, Landroid/graphics/PointF;->y:F

    const/4 v7, 0x0

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->y:F

    sub-float v34, v6, v7

    .line 1505
    .local v34, "ptStartWidthY":F
    mul-float v6, v33, v33

    mul-float v7, v34, v34

    add-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-int v0, v6

    move/from16 v24, v0

    .line 1507
    .local v24, "mWidthX":I
    const/4 v6, 0x3

    aget-object v6, v30, v6

    iget v6, v6, Landroid/graphics/PointF;->x:F

    const/4 v7, 0x0

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->x:F

    sub-float v31, v6, v7

    .line 1508
    .local v31, "ptStartHeightX":F
    const/4 v6, 0x3

    aget-object v6, v30, v6

    iget v6, v6, Landroid/graphics/PointF;->y:F

    const/4 v7, 0x0

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->y:F

    sub-float v32, v6, v7

    .line 1509
    .local v32, "ptStartHeightY":F
    mul-float v6, v31, v31

    mul-float v7, v32, v32

    add-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-int v0, v6

    move/from16 v21, v0

    .line 1511
    .local v21, "mHeightY":I
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_0
    const/4 v6, 0x4

    move/from16 v0, v19

    if-lt v0, v6, :cond_8

    .line 1532
    const/4 v6, 0x0

    cmpg-float v6, v27, v6

    if-gez v6, :cond_0

    .line 1533
    const/16 v27, 0x0

    .line 1535
    :cond_0
    const/4 v6, 0x0

    cmpg-float v6, v28, v6

    if-gez v6, :cond_1

    .line 1536
    const/16 v28, 0x0

    .line 1538
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    cmpl-float v6, v26, v6

    if-lez v6, :cond_2

    .line 1539
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    int-to-float v0, v6

    move/from16 v26, v0

    .line 1541
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    cmpl-float v6, v25, v6

    if-lez v6, :cond_3

    .line 1542
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    int-to-float v0, v6

    move/from16 v25, v0

    .line 1545
    :cond_3
    sub-float v6, v25, v27

    float-to-int v6, v6

    add-int/lit8 v17, v6, 0x1

    .line 1546
    .local v17, "first_crop_width":I
    sub-float v6, v26, v28

    float-to-int v6, v6

    add-int/lit8 v16, v6, 0x1

    .line 1548
    .local v16, "first_crop_heiht":I
    mul-int v6, v17, v16

    new-array v0, v6, [I

    move-object/from16 v22, v0

    .line 1550
    .local v22, "mImageBuff":[I
    const/4 v6, 0x0

    move-object/from16 v0, v22

    invoke-static {v0, v6}, Ljava/util/Arrays;->fill([II)V

    .line 1551
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v23

    .line 1552
    .local v23, "mOriginalImageBuff":[I
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v38

    .line 1553
    .local v38, "width":I
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v18

    .line 1554
    .local v18, "height":I
    const/4 v14, 0x0

    .line 1556
    .local v14, "count":I
    mul-float v6, v26, v25

    move-object/from16 v0, v23

    array-length v7, v0

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-lez v6, :cond_5

    .line 1558
    move/from16 v0, v38

    int-to-float v6, v0

    mul-float v6, v6, v26

    add-float v6, v6, v25

    move-object/from16 v0, v23

    array-length v7, v0

    int-to-float v7, v7

    sub-float v15, v6, v7

    .line 1560
    .local v15, "diff":F
    move/from16 v0, v18

    int-to-float v6, v0

    cmpg-float v6, v6, v25

    if-gtz v6, :cond_4

    move/from16 v0, v38

    int-to-float v6, v0

    cmpg-float v6, v6, v26

    if-gtz v6, :cond_4

    .line 1562
    sub-float v25, v25, v15

    .line 1563
    sub-float v26, v26, v15

    .line 1565
    :cond_4
    move/from16 v0, v18

    int-to-float v6, v0

    cmpg-float v6, v6, v25

    if-gtz v6, :cond_d

    .line 1567
    sub-float v25, v25, v15

    .line 1574
    .end local v15    # "diff":F
    :cond_5
    :goto_1
    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v19, v0

    :goto_2
    move/from16 v0, v19

    int-to-float v6, v0

    cmpg-float v6, v6, v26

    if-lez v6, :cond_e

    .line 1580
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v22

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-static {v0, v1, v2, v6}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v36

    .line 1582
    .local v36, "src":Landroid/graphics/Bitmap;
    new-instance v37, Landroid/graphics/Matrix;

    invoke-direct/range {v37 .. v37}, Landroid/graphics/Matrix;-><init>()V

    .line 1584
    .local v37, "tempMatrix":Landroid/graphics/Matrix;
    const/16 v6, 0x2d

    move/from16 v0, v35

    if-lt v0, v6, :cond_6

    const/16 v6, 0x87

    move/from16 v0, v35

    if-lt v0, v6, :cond_7

    :cond_6
    const/16 v6, 0xe1

    move/from16 v0, v35

    if-lt v0, v6, :cond_11

    const/16 v6, 0x13b

    move/from16 v0, v35

    if-ge v0, v6, :cond_11

    .line 1586
    :cond_7
    const/4 v12, 0x0

    .line 1587
    .local v12, "angle":F
    const/16 v6, 0xe1

    move/from16 v0, v35

    if-lt v0, v6, :cond_10

    const/16 v6, 0x13b

    move/from16 v0, v35

    if-ge v0, v6, :cond_10

    .line 1589
    move/from16 v0, v35

    rsub-int v6, v0, 0x10e

    int-to-float v12, v6

    .line 1595
    :goto_3
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v21

    move/from16 v1, v24

    invoke-static {v0, v1, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1596
    .local v4, "result":Landroid/graphics/Bitmap;
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    mul-int/2addr v6, v7

    new-array v5, v6, [I

    .line 1598
    .local v5, "pixels":[I
    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    move-object/from16 v0, v37

    invoke-virtual {v0, v12, v6, v7}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1599
    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    sub-int/2addr v6, v7

    neg-int v6, v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    sub-int/2addr v7, v8

    neg-int v7, v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    move-object/from16 v0, v37

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1600
    new-instance v13, Landroid/graphics/Canvas;

    invoke-direct {v13, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1601
    .local v13, "canvas":Landroid/graphics/Canvas;
    new-instance v29, Landroid/graphics/Paint;

    invoke-direct/range {v29 .. v29}, Landroid/graphics/Paint;-><init>()V

    .line 1602
    .local v29, "paint":Landroid/graphics/Paint;
    const/4 v6, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1603
    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v29

    invoke-virtual {v13, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 1605
    const/4 v6, 0x0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1606
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([III)V

    .line 1608
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 1609
    const/4 v4, 0x0

    .line 1610
    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->recycle()V

    .line 1611
    const/16 v36, 0x0

    .line 1643
    :goto_4
    const/16 v22, 0x0

    .line 1644
    return-void

    .line 1513
    .end local v4    # "result":Landroid/graphics/Bitmap;
    .end local v5    # "pixels":[I
    .end local v12    # "angle":F
    .end local v13    # "canvas":Landroid/graphics/Canvas;
    .end local v14    # "count":I
    .end local v16    # "first_crop_heiht":I
    .end local v17    # "first_crop_width":I
    .end local v18    # "height":I
    .end local v22    # "mImageBuff":[I
    .end local v23    # "mOriginalImageBuff":[I
    .end local v29    # "paint":Landroid/graphics/Paint;
    .end local v36    # "src":Landroid/graphics/Bitmap;
    .end local v37    # "tempMatrix":Landroid/graphics/Matrix;
    .end local v38    # "width":I
    :cond_8
    aget-object v6, v30, v19

    iget v6, v6, Landroid/graphics/PointF;->x:F

    cmpg-float v6, v6, v27

    if-gez v6, :cond_9

    .line 1515
    aget-object v6, v30, v19

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    .line 1517
    :cond_9
    aget-object v6, v30, v19

    iget v6, v6, Landroid/graphics/PointF;->y:F

    cmpg-float v6, v6, v28

    if-gez v6, :cond_a

    .line 1519
    aget-object v6, v30, v19

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v28, v0

    .line 1522
    :cond_a
    aget-object v6, v30, v19

    iget v6, v6, Landroid/graphics/PointF;->x:F

    cmpl-float v6, v6, v25

    if-lez v6, :cond_b

    .line 1524
    aget-object v6, v30, v19

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v25, v0

    .line 1526
    :cond_b
    aget-object v6, v30, v19

    iget v6, v6, Landroid/graphics/PointF;->y:F

    cmpl-float v6, v6, v26

    if-lez v6, :cond_c

    .line 1528
    aget-object v6, v30, v19

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v26, v0

    .line 1511
    :cond_c
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_0

    .line 1571
    .restart local v14    # "count":I
    .restart local v15    # "diff":F
    .restart local v16    # "first_crop_heiht":I
    .restart local v17    # "first_crop_width":I
    .restart local v18    # "height":I
    .restart local v22    # "mImageBuff":[I
    .restart local v23    # "mOriginalImageBuff":[I
    .restart local v38    # "width":I
    :cond_d
    sub-float v26, v26, v15

    goto/16 :goto_1

    .line 1575
    .end local v15    # "diff":F
    :cond_e
    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v20, v0

    .local v20, "j":I
    :goto_5
    move/from16 v0, v20

    int-to-float v6, v0

    cmpg-float v6, v6, v25

    if-lez v6, :cond_f

    .line 1574
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_2

    .line 1576
    :cond_f
    mul-int v6, v19, v38

    add-int v6, v6, v20

    aget v6, v23, v6

    aput v6, v22, v14

    .line 1577
    add-int/lit8 v14, v14, 0x1

    .line 1575
    add-int/lit8 v20, v20, 0x1

    goto :goto_5

    .line 1593
    .end local v20    # "j":I
    .restart local v12    # "angle":F
    .restart local v36    # "src":Landroid/graphics/Bitmap;
    .restart local v37    # "tempMatrix":Landroid/graphics/Matrix;
    :cond_10
    rsub-int/lit8 v6, v35, 0x5a

    int-to-float v12, v6

    goto/16 :goto_3

    .line 1615
    .end local v12    # "angle":F
    :cond_11
    const/4 v12, 0x0

    .line 1616
    .restart local v12    # "angle":F
    const/16 v6, 0x87

    move/from16 v0, v35

    if-lt v0, v6, :cond_12

    const/16 v6, 0xe1

    move/from16 v0, v35

    if-ge v0, v6, :cond_12

    .line 1618
    move/from16 v0, v35

    rsub-int v6, v0, 0xb4

    int-to-float v12, v6

    .line 1624
    :goto_6
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v24

    move/from16 v1, v21

    invoke-static {v0, v1, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1625
    .restart local v4    # "result":Landroid/graphics/Bitmap;
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    mul-int/2addr v6, v7

    new-array v5, v6, [I

    .line 1627
    .restart local v5    # "pixels":[I
    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    move-object/from16 v0, v37

    invoke-virtual {v0, v12, v6, v7}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1628
    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    sub-int/2addr v6, v7

    neg-int v6, v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    sub-int/2addr v7, v8

    neg-int v7, v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    move-object/from16 v0, v37

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1629
    new-instance v13, Landroid/graphics/Canvas;

    invoke-direct {v13, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1630
    .restart local v13    # "canvas":Landroid/graphics/Canvas;
    new-instance v29, Landroid/graphics/Paint;

    invoke-direct/range {v29 .. v29}, Landroid/graphics/Paint;-><init>()V

    .line 1631
    .restart local v29    # "paint":Landroid/graphics/Paint;
    const/4 v6, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1632
    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v29

    invoke-virtual {v13, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 1634
    const/4 v6, 0x0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1635
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([III)V

    .line 1637
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 1638
    const/4 v4, 0x0

    .line 1639
    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->recycle()V

    .line 1640
    const/16 v36, 0x0

    goto/16 :goto_4

    .line 1622
    .end local v4    # "result":Landroid/graphics/Bitmap;
    .end local v5    # "pixels":[I
    .end local v13    # "canvas":Landroid/graphics/Canvas;
    .end local v29    # "paint":Landroid/graphics/Paint;
    :cond_12
    move/from16 v0, v35

    neg-int v6, v0

    int-to-float v12, v6

    goto/16 :goto_6
.end method

.method private calculateRect(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 39
    .param p1, "imgInfo"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    .line 1311
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getAngle()I

    move-result v35

    .line 1312
    .local v35, "rectAngle":I
    const/4 v6, 0x4

    new-array v0, v6, [Landroid/graphics/PointF;

    move-object/from16 v30, v0

    .line 1313
    .local v30, "pt":[Landroid/graphics/PointF;
    const/16 v26, 0x0

    .local v26, "min_x":F
    const/16 v24, 0x0

    .line 1314
    .local v24, "max_x":F
    const/16 v27, 0x0

    .local v27, "min_y":F
    const/16 v25, 0x0

    .line 1321
    .local v25, "max_y":F
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getOrgDestPt1()Landroid/graphics/PointF;

    move-result-object v7

    aput-object v7, v30, v6

    .line 1322
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getOrgDestPt2()Landroid/graphics/PointF;

    move-result-object v7

    aput-object v7, v30, v6

    .line 1323
    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getOrgDestPt3()Landroid/graphics/PointF;

    move-result-object v7

    aput-object v7, v30, v6

    .line 1324
    const/4 v6, 0x3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getOrgDestPt4()Landroid/graphics/PointF;

    move-result-object v7

    aput-object v7, v30, v6

    .line 1326
    const/4 v6, 0x0

    aget-object v6, v30, v6

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v26, v0

    .line 1327
    const/4 v6, 0x0

    aget-object v6, v30, v6

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v27, v0

    .line 1329
    const/4 v6, 0x1

    aget-object v6, v30, v6

    iget v6, v6, Landroid/graphics/PointF;->x:F

    const/4 v7, 0x0

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->x:F

    sub-float v33, v6, v7

    .line 1330
    .local v33, "ptStartWidthX":F
    const/4 v6, 0x1

    aget-object v6, v30, v6

    iget v6, v6, Landroid/graphics/PointF;->y:F

    const/4 v7, 0x0

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->y:F

    sub-float v34, v6, v7

    .line 1331
    .local v34, "ptStartWidthY":F
    mul-float v6, v33, v33

    mul-float v7, v34, v34

    add-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-int v0, v6

    move/from16 v23, v0

    .line 1333
    .local v23, "mWidthX":I
    const/4 v6, 0x3

    aget-object v6, v30, v6

    iget v6, v6, Landroid/graphics/PointF;->x:F

    const/4 v7, 0x0

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->x:F

    sub-float v31, v6, v7

    .line 1334
    .local v31, "ptStartHeightX":F
    const/4 v6, 0x3

    aget-object v6, v30, v6

    iget v6, v6, Landroid/graphics/PointF;->y:F

    const/4 v7, 0x0

    aget-object v7, v30, v7

    iget v7, v7, Landroid/graphics/PointF;->y:F

    sub-float v32, v6, v7

    .line 1335
    .local v32, "ptStartHeightY":F
    mul-float v6, v31, v31

    mul-float v7, v32, v32

    add-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-int v0, v6

    move/from16 v21, v0

    .line 1337
    .local v21, "mHeightY":I
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_0
    const/4 v6, 0x4

    move/from16 v0, v19

    if-lt v0, v6, :cond_8

    .line 1358
    const/4 v6, 0x0

    cmpg-float v6, v26, v6

    if-gez v6, :cond_0

    .line 1359
    const/16 v26, 0x0

    .line 1361
    :cond_0
    const/4 v6, 0x0

    cmpg-float v6, v27, v6

    if-gez v6, :cond_1

    .line 1362
    const/16 v27, 0x0

    .line 1364
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    cmpl-float v6, v25, v6

    if-lez v6, :cond_2

    .line 1365
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    int-to-float v0, v6

    move/from16 v25, v0

    .line 1367
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    cmpl-float v6, v24, v6

    if-lez v6, :cond_3

    .line 1368
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    int-to-float v0, v6

    move/from16 v24, v0

    .line 1371
    :cond_3
    sub-float v6, v24, v26

    float-to-int v6, v6

    add-int/lit8 v17, v6, 0x1

    .line 1372
    .local v17, "first_crop_width":I
    sub-float v6, v25, v27

    float-to-int v6, v6

    add-int/lit8 v16, v6, 0x1

    .line 1374
    .local v16, "first_crop_heiht":I
    mul-int v6, v17, v16

    new-array v0, v6, [I

    move-object/from16 v22, v0

    .line 1376
    .local v22, "mImageBuff":[I
    const/4 v6, 0x0

    move-object/from16 v0, v22

    invoke-static {v0, v6}, Ljava/util/Arrays;->fill([II)V

    .line 1377
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v29

    .line 1378
    .local v29, "previewImageBuff":[I
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v38

    .line 1379
    .local v38, "width":I
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v18

    .line 1380
    .local v18, "height":I
    const/4 v14, 0x0

    .line 1382
    .local v14, "count":I
    mul-float v6, v25, v24

    move-object/from16 v0, v29

    array-length v7, v0

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-lez v6, :cond_5

    .line 1384
    move/from16 v0, v38

    int-to-float v6, v0

    mul-float v6, v6, v25

    add-float v6, v6, v24

    move-object/from16 v0, v29

    array-length v7, v0

    int-to-float v7, v7

    sub-float v15, v6, v7

    .line 1386
    .local v15, "diff":F
    move/from16 v0, v18

    int-to-float v6, v0

    cmpg-float v6, v6, v24

    if-gtz v6, :cond_4

    move/from16 v0, v38

    int-to-float v6, v0

    cmpg-float v6, v6, v25

    if-gtz v6, :cond_4

    .line 1388
    sub-float v24, v24, v15

    .line 1389
    sub-float v25, v25, v15

    .line 1391
    :cond_4
    move/from16 v0, v18

    int-to-float v6, v0

    cmpg-float v6, v6, v24

    if-gtz v6, :cond_d

    .line 1393
    sub-float v24, v24, v15

    .line 1400
    .end local v15    # "diff":F
    :cond_5
    :goto_1
    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v19, v0

    :goto_2
    move/from16 v0, v19

    int-to-float v6, v0

    cmpg-float v6, v6, v25

    if-lez v6, :cond_e

    .line 1406
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v22

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-static {v0, v1, v2, v6}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v36

    .line 1408
    .local v36, "src":Landroid/graphics/Bitmap;
    new-instance v37, Landroid/graphics/Matrix;

    invoke-direct/range {v37 .. v37}, Landroid/graphics/Matrix;-><init>()V

    .line 1410
    .local v37, "tempMatrix":Landroid/graphics/Matrix;
    const/16 v6, 0x2d

    move/from16 v0, v35

    if-lt v0, v6, :cond_6

    const/16 v6, 0x87

    move/from16 v0, v35

    if-lt v0, v6, :cond_7

    :cond_6
    const/16 v6, 0xe1

    move/from16 v0, v35

    if-lt v0, v6, :cond_11

    const/16 v6, 0x13b

    move/from16 v0, v35

    if-ge v0, v6, :cond_11

    .line 1412
    :cond_7
    const/4 v12, 0x0

    .line 1413
    .local v12, "angle":F
    const/16 v6, 0xe1

    move/from16 v0, v35

    if-lt v0, v6, :cond_10

    const/16 v6, 0x13b

    move/from16 v0, v35

    if-ge v0, v6, :cond_10

    .line 1415
    move/from16 v0, v35

    rsub-int v6, v0, 0x10e

    int-to-float v12, v6

    .line 1421
    :goto_3
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v21

    move/from16 v1, v23

    invoke-static {v0, v1, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1422
    .local v4, "result":Landroid/graphics/Bitmap;
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    mul-int/2addr v6, v7

    new-array v5, v6, [I

    .line 1424
    .local v5, "pixels":[I
    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    move-object/from16 v0, v37

    invoke-virtual {v0, v12, v6, v7}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1425
    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    sub-int/2addr v6, v7

    neg-int v6, v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    sub-int/2addr v7, v8

    neg-int v7, v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    move-object/from16 v0, v37

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1426
    new-instance v13, Landroid/graphics/Canvas;

    invoke-direct {v13, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1427
    .local v13, "canvas":Landroid/graphics/Canvas;
    new-instance v28, Landroid/graphics/Paint;

    invoke-direct/range {v28 .. v28}, Landroid/graphics/Paint;-><init>()V

    .line 1428
    .local v28, "paint":Landroid/graphics/Paint;
    const/4 v6, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1429
    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v28

    invoke-virtual {v13, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 1431
    const/4 v6, 0x0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1432
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewBuffer([III)V

    .line 1434
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 1435
    const/4 v4, 0x0

    .line 1436
    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->recycle()V

    .line 1437
    const/16 v36, 0x0

    .line 1469
    :goto_4
    const/16 v22, 0x0

    .line 1470
    return-void

    .line 1339
    .end local v4    # "result":Landroid/graphics/Bitmap;
    .end local v5    # "pixels":[I
    .end local v12    # "angle":F
    .end local v13    # "canvas":Landroid/graphics/Canvas;
    .end local v14    # "count":I
    .end local v16    # "first_crop_heiht":I
    .end local v17    # "first_crop_width":I
    .end local v18    # "height":I
    .end local v22    # "mImageBuff":[I
    .end local v28    # "paint":Landroid/graphics/Paint;
    .end local v29    # "previewImageBuff":[I
    .end local v36    # "src":Landroid/graphics/Bitmap;
    .end local v37    # "tempMatrix":Landroid/graphics/Matrix;
    .end local v38    # "width":I
    :cond_8
    aget-object v6, v30, v19

    iget v6, v6, Landroid/graphics/PointF;->x:F

    cmpg-float v6, v6, v26

    if-gez v6, :cond_9

    .line 1341
    aget-object v6, v30, v19

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v26, v0

    .line 1343
    :cond_9
    aget-object v6, v30, v19

    iget v6, v6, Landroid/graphics/PointF;->y:F

    cmpg-float v6, v6, v27

    if-gez v6, :cond_a

    .line 1345
    aget-object v6, v30, v19

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v27, v0

    .line 1348
    :cond_a
    aget-object v6, v30, v19

    iget v6, v6, Landroid/graphics/PointF;->x:F

    cmpl-float v6, v6, v24

    if-lez v6, :cond_b

    .line 1350
    aget-object v6, v30, v19

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v24, v0

    .line 1352
    :cond_b
    aget-object v6, v30, v19

    iget v6, v6, Landroid/graphics/PointF;->y:F

    cmpl-float v6, v6, v25

    if-lez v6, :cond_c

    .line 1354
    aget-object v6, v30, v19

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v25, v0

    .line 1337
    :cond_c
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_0

    .line 1397
    .restart local v14    # "count":I
    .restart local v15    # "diff":F
    .restart local v16    # "first_crop_heiht":I
    .restart local v17    # "first_crop_width":I
    .restart local v18    # "height":I
    .restart local v22    # "mImageBuff":[I
    .restart local v29    # "previewImageBuff":[I
    .restart local v38    # "width":I
    :cond_d
    sub-float v25, v25, v15

    goto/16 :goto_1

    .line 1401
    .end local v15    # "diff":F
    :cond_e
    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v20, v0

    .local v20, "j":I
    :goto_5
    move/from16 v0, v20

    int-to-float v6, v0

    cmpg-float v6, v6, v24

    if-lez v6, :cond_f

    .line 1400
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_2

    .line 1402
    :cond_f
    mul-int v6, v19, v38

    add-int v6, v6, v20

    aget v6, v29, v6

    aput v6, v22, v14

    .line 1403
    add-int/lit8 v14, v14, 0x1

    .line 1401
    add-int/lit8 v20, v20, 0x1

    goto :goto_5

    .line 1419
    .end local v20    # "j":I
    .restart local v12    # "angle":F
    .restart local v36    # "src":Landroid/graphics/Bitmap;
    .restart local v37    # "tempMatrix":Landroid/graphics/Matrix;
    :cond_10
    rsub-int/lit8 v6, v35, 0x5a

    int-to-float v12, v6

    goto/16 :goto_3

    .line 1441
    .end local v12    # "angle":F
    :cond_11
    const/4 v12, 0x0

    .line 1442
    .restart local v12    # "angle":F
    const/16 v6, 0x87

    move/from16 v0, v35

    if-lt v0, v6, :cond_12

    const/16 v6, 0xe1

    move/from16 v0, v35

    if-ge v0, v6, :cond_12

    .line 1444
    move/from16 v0, v35

    rsub-int v6, v0, 0xb4

    int-to-float v12, v6

    .line 1450
    :goto_6
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v23

    move/from16 v1, v21

    invoke-static {v0, v1, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1451
    .restart local v4    # "result":Landroid/graphics/Bitmap;
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    mul-int/2addr v6, v7

    new-array v5, v6, [I

    .line 1453
    .restart local v5    # "pixels":[I
    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    move-object/from16 v0, v37

    invoke-virtual {v0, v12, v6, v7}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1454
    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    sub-int/2addr v6, v7

    neg-int v6, v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    sub-int/2addr v7, v8

    neg-int v7, v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    move-object/from16 v0, v37

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1455
    new-instance v13, Landroid/graphics/Canvas;

    invoke-direct {v13, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1456
    .restart local v13    # "canvas":Landroid/graphics/Canvas;
    new-instance v28, Landroid/graphics/Paint;

    invoke-direct/range {v28 .. v28}, Landroid/graphics/Paint;-><init>()V

    .line 1457
    .restart local v28    # "paint":Landroid/graphics/Paint;
    const/4 v6, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1458
    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v28

    invoke-virtual {v13, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 1460
    const/4 v6, 0x0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1461
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewBuffer([III)V

    .line 1463
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 1464
    const/4 v4, 0x0

    .line 1465
    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Bitmap;->recycle()V

    .line 1466
    const/16 v36, 0x0

    goto/16 :goto_4

    .line 1448
    .end local v4    # "result":Landroid/graphics/Bitmap;
    .end local v5    # "pixels":[I
    .end local v13    # "canvas":Landroid/graphics/Canvas;
    .end local v28    # "paint":Landroid/graphics/Paint;
    :cond_12
    move/from16 v0, v35

    neg-int v6, v0

    int-to-float v12, v6

    goto/16 :goto_6
.end method

.method private calculateSrcToDstDrawCanvasRoi(Z)V
    .locals 4
    .param p1, "needToRefreshSrc"    # Z

    .prologue
    .line 1692
    if-eqz p1, :cond_0

    .line 1693
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mSrcDrawCanvasRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1694
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mDstDrawCanvasRect:Landroid/graphics/RectF;

    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mSrcDrawCanvasRect:Landroid/graphics/Rect;

    invoke-direct {v2, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1697
    return-void
.end method

.method private contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Landroid/graphics/Path;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Z)V
    .locals 7
    .param p1, "previewMask"    # [B
    .param p2, "previewWidth"    # I
    .param p3, "previewHeight"    # I
    .param p4, "roi"    # Landroid/graphics/Rect;
    .param p5, "previewPath"    # Landroid/graphics/Path;
    .param p6, "originalPath"    # Landroid/graphics/Path;
    .param p7, "imageData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .param p8, "isSub"    # Z

    .prologue
    .line 1782
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    if-nez v0, :cond_0

    .line 1784
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    .line 1785
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->init(II)V

    .line 1787
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p8

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Z)V

    .line 1798
    return-void
.end method

.method private getRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 2
    .param p1, "current"    # Landroid/graphics/RectF;
    .param p2, "rectToDraw"    # Landroid/graphics/RectF;

    .prologue
    .line 699
    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    if-eq v0, v1, :cond_0

    .line 700
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 701
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalleft:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 706
    :cond_0
    :goto_0
    iget v0, p2, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    if-eq v0, v1, :cond_1

    .line 707
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p2, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    .line 708
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervaltop:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 713
    :cond_1
    :goto_1
    iget v0, p2, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    if-eq v0, v1, :cond_2

    .line 714
    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget v1, p2, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_7

    .line 715
    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalright:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 720
    :cond_2
    :goto_2
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    float-to-int v1, v1

    if-eq v0, v1, :cond_3

    .line 721
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_8

    .line 722
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalbottom:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 727
    :cond_3
    :goto_3
    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    if-ne v0, v1, :cond_4

    .line 728
    iget v0, p2, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    if-ne v0, v1, :cond_4

    .line 729
    iget v0, p2, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    if-ne v0, v1, :cond_4

    .line 730
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    float-to-int v1, v1

    if-ne v0, v1, :cond_4

    .line 731
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->isAnimating:Z

    .line 733
    :cond_4
    return-object p1

    .line 703
    :cond_5
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalleft:F

    sub-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    goto :goto_0

    .line 710
    :cond_6
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervaltop:F

    sub-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    goto :goto_1

    .line 717
    :cond_7
    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalright:F

    sub-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    goto :goto_2

    .line 724
    :cond_8
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalbottom:F

    sub-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_3
.end method

.method private initFixedCrop(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/graphics/Rect;)V
    .locals 6
    .param p1, "ii"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "roi"    # Landroid/graphics/Rect;

    .prologue
    const/16 v5, 0x10

    const/16 v4, 0x9

    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 1218
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1254
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->adjustRectWithRatio(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/graphics/Rect;)V

    .line 1256
    return-void

    .line 1225
    :pswitch_0
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedWidth:I

    .line 1226
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedHeight:I

    goto :goto_0

    .line 1229
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mToggleRotate:Z

    if-eqz v0, :cond_0

    .line 1231
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedWidth:I

    .line 1232
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedHeight:I

    goto :goto_0

    .line 1236
    :cond_0
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedWidth:I

    .line 1237
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedHeight:I

    goto :goto_0

    .line 1241
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mToggleRotate:Z

    if-eqz v0, :cond_1

    .line 1243
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedWidth:I

    .line 1244
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedHeight:I

    goto :goto_0

    .line 1248
    :cond_1
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedWidth:I

    .line 1249
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFixedHeight:I

    goto :goto_0

    .line 1218
    nop

    :pswitch_data_0
    .packed-switch 0x11201302
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private initResource(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/graphics/Rect;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imgInfo"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p3, "roi"    # Landroid/graphics/Rect;

    .prologue
    const/4 v7, -0x1

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x1

    .line 1700
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1702
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020374

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    .line 1704
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020377

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmapPress:Landroid/graphics/Bitmap;

    .line 1706
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 1707
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 1708
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 1709
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 1710
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    .line 1711
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    .line 1712
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    .line 1713
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    .line 1716
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropDarkBackgroundPaint:Landroid/graphics/Paint;

    .line 1717
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropDarkBackgroundPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x80000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1719
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mClearPaint:Landroid/graphics/Paint;

    .line 1720
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    .line 1721
    .local v0, "xmode":Landroid/graphics/Xfermode;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mClearPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 1723
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_w:Landroid/graphics/Paint;

    .line 1724
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_w:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 1725
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_w:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1726
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_w:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1727
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_w:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1728
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_b:Landroid/graphics/Paint;

    .line 1729
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_b:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1730
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_b:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/DashPathEffect;

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    const/high16 v4, 0x40400000    # 3.0f

    invoke-direct {v2, v3, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1731
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1732
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_b:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1733
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_b:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1734
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectOuterPaint:Landroid/graphics/Paint;

    .line 1735
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectOuterPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v6

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1736
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectOuterPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1737
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectOuterPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 1738
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectOuterPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1739
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectInnerPaint:Landroid/graphics/Paint;

    .line 1740
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectInnerPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1741
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectInnerPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1742
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectInnerPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 1743
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectInnerPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1746
    return-void

    .line 1730
    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method

.method private setHandlerIconPressed(I)V
    .locals 1
    .param p1, "touchType"    # I

    .prologue
    .line 764
    packed-switch p1, :pswitch_data_0

    .line 894
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 895
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 896
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 897
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 898
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    .line 899
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    .line 900
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    .line 901
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    .line 904
    :goto_0
    return-void

    .line 766
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    if-eqz v0, :cond_0

    .line 768
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmapPress:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 770
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 771
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 772
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 773
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    .line 774
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    .line 775
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    .line 776
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 779
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 782
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    if-eqz v0, :cond_1

    .line 784
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmapPress:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 786
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 787
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 788
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 789
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    .line 790
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    .line 791
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    .line 792
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 795
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 798
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    if-eqz v0, :cond_2

    .line 800
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmapPress:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 802
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 803
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 804
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 805
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    .line 806
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    .line 807
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    .line 808
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 811
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 814
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    if-eqz v0, :cond_3

    .line 816
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmapPress:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 818
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 819
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 820
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 821
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    .line 822
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    .line 823
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    .line 824
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 827
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 830
    :pswitch_4
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    if-eqz v0, :cond_4

    .line 832
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmapPress:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    .line 834
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 835
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 836
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 837
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 838
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    .line 839
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    .line 840
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 843
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 846
    :pswitch_5
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    if-eqz v0, :cond_5

    .line 848
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmapPress:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    .line 850
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 851
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 852
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 853
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 854
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    .line 855
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    .line 856
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 859
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 862
    :pswitch_6
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    if-eqz v0, :cond_6

    .line 864
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmapPress:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    .line 866
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 867
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 868
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 869
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 870
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    .line 871
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    .line 872
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 875
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 878
    :pswitch_7
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    if-eqz v0, :cond_7

    .line 880
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmapPress:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    .line 882
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 883
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 884
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 885
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 886
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    .line 887
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    .line 888
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 891
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 764
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private touchCropLasso(Landroid/view/MotionEvent;)V
    .locals 26
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 981
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v24

    .line 982
    .local v24, "x":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v25

    .line 983
    .local v25, "y":F
    new-instance v23, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    move-object/from16 v0, v23

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 985
    .local v23, "roi":Landroid/graphics/Rect;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sj, CE - touchCropLasso() - x : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / y : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / roi : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 986
    const-string v3, " / mImageData.getPreviewWidth() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 987
    const-string v3, " / +mImageData.getPreviewHeight() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 988
    const-string v3, " / mImageData.getDrawCanvasRoi() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 985
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 990
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1139
    :cond_0
    :goto_0
    return-void

    .line 993
    :pswitch_0
    move/from16 v0, v24

    float-to-int v2, v0

    move/from16 v0, v25

    float-to-int v3, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 995
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 996
    new-instance v6, Landroid/graphics/Point;

    move/from16 v0, v24

    float-to-int v2, v0

    move/from16 v0, v25

    float-to-int v3, v0

    invoke-direct {v6, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 1001
    .local v6, "p":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    if-nez v2, :cond_1

    .line 1002
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    .line 1003
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 1004
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1006
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mStartSelect:Z

    .line 1007
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    .line 1008
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([BB)V

    .line 1009
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 1012
    .local v9, "r":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mPrePt:Landroid/graphics/Point;

    if-nez v2, :cond_2

    .line 1013
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mPrePt:Landroid/graphics/Point;

    .line 1014
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    if-nez v2, :cond_3

    .line 1015
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    .line 1016
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFirstPt:Landroid/graphics/Point;

    if-nez v2, :cond_4

    .line 1017
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFirstPt:Landroid/graphics/Point;

    .line 1020
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    .line 1021
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    .line 1022
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 1023
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    .line 1024
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v8, 0x4

    const/4 v10, 0x0

    .line 1020
    invoke-static/range {v2 .. v10}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    .line 1025
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    invoke-virtual {v2, v9}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 1026
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mPrePt:Landroid/graphics/Point;

    iget v3, v6, Landroid/graphics/Point;->x:I

    iget v4, v6, Landroid/graphics/Point;->y:I

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 1027
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFirstPt:Landroid/graphics/Point;

    iget v3, v6, Landroid/graphics/Point;->x:I

    iget v4, v6, Landroid/graphics/Point;->y:I

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 1032
    .end local v6    # "p":Landroid/graphics/Point;
    .end local v9    # "r":Landroid/graphics/Rect;
    :pswitch_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mStartSelect:Z

    if-eqz v2, :cond_0

    .line 1034
    new-instance v6, Landroid/graphics/Point;

    move/from16 v0, v24

    float-to-int v2, v0

    move/from16 v0, v25

    float-to-int v3, v0

    invoke-direct {v6, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 1036
    .restart local v6    # "p":Landroid/graphics/Point;
    move/from16 v0, v24

    float-to-int v2, v0

    move/from16 v0, v25

    float-to-int v3, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1038
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 1039
    .restart local v9    # "r":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    .line 1040
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    .line 1041
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 1042
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    .line 1043
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v8, 0x4

    const/4 v10, 0x0

    .line 1039
    invoke-static/range {v2 .. v10}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    .line 1044
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    invoke-virtual {v2, v9}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 1045
    move/from16 v21, v24

    .line 1046
    .local v21, "lassoX":F
    move/from16 v22, v25

    .line 1087
    .local v22, "lassoY":F
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1088
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mPrePt:Landroid/graphics/Point;

    iget v3, v6, Landroid/graphics/Point;->x:I

    iget v4, v6, Landroid/graphics/Point;->y:I

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_0

    .line 1050
    .end local v9    # "r":Landroid/graphics/Rect;
    .end local v21    # "lassoX":F
    .end local v22    # "lassoY":F
    :cond_5
    iget v2, v6, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-le v2, v3, :cond_6

    .line 1051
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, v6, Landroid/graphics/Point;->x:I

    .line 1052
    :cond_6
    iget v2, v6, Landroid/graphics/Point;->x:I

    if-gez v2, :cond_7

    .line 1053
    const/4 v2, 0x0

    iput v2, v6, Landroid/graphics/Point;->x:I

    .line 1054
    :cond_7
    iget v2, v6, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-le v2, v3, :cond_8

    .line 1055
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, v6, Landroid/graphics/Point;->y:I

    .line 1056
    :cond_8
    iget v2, v6, Landroid/graphics/Point;->y:I

    if-gez v2, :cond_9

    .line 1057
    const/4 v2, 0x0

    iput v2, v6, Landroid/graphics/Point;->y:I

    .line 1058
    :cond_9
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 1059
    .restart local v9    # "r":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    .line 1060
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    .line 1061
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 1062
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    .line 1063
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v8, 0x4

    const/4 v10, 0x0

    .line 1059
    invoke-static/range {v2 .. v10}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    .line 1064
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    invoke-virtual {v2, v9}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 1074
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    cmpl-float v2, v24, v2

    if-lez v2, :cond_a

    .line 1075
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v0, v2

    move/from16 v24, v0

    .line 1076
    :cond_a
    const/4 v2, 0x0

    cmpg-float v2, v24, v2

    if-gez v2, :cond_b

    .line 1077
    const/16 v24, 0x0

    .line 1078
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    cmpl-float v2, v25, v2

    if-lez v2, :cond_c

    .line 1079
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v0, v2

    move/from16 v25, v0

    .line 1080
    :cond_c
    const/4 v2, 0x0

    cmpg-float v2, v25, v2

    if-gez v2, :cond_d

    .line 1081
    const/16 v25, 0x0

    .line 1083
    :cond_d
    move/from16 v21, v24

    .line 1084
    .restart local v21    # "lassoX":F
    move/from16 v22, v25

    .restart local v22    # "lassoY":F
    goto/16 :goto_1

    .line 1094
    .end local v6    # "p":Landroid/graphics/Point;
    .end local v9    # "r":Landroid/graphics/Rect;
    .end local v21    # "lassoX":F
    .end local v22    # "lassoY":F
    :pswitch_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mStartSelect:Z

    if-eqz v2, :cond_0

    .line 1096
    const/16 v19, 0x0

    .line 1097
    .local v19, "cnt":I
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    mul-int/2addr v2, v3

    move/from16 v0, v20

    if-lt v0, v2, :cond_e

    .line 1102
    const/16 v2, 0x64

    move/from16 v0, v19

    if-ge v0, v2, :cond_10

    .line 1104
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([BB)V

    .line 1105
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    .line 1107
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 1108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 1109
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFirstPt:Landroid/graphics/Point;

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 1110
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    goto/16 :goto_0

    .line 1099
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    aget-byte v2, v2, v20

    const/4 v3, 0x1

    if-ne v2, v3, :cond_f

    .line 1100
    add-int/lit8 v19, v19, 0x1

    .line 1097
    :cond_f
    add-int/lit8 v20, v20, 0x1

    goto :goto_2

    .line 1115
    :cond_10
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 1116
    .restart local v9    # "r":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v10

    .line 1117
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    .line 1118
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v12

    .line 1119
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v13

    .line 1120
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mPrePt:Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFirstPt:Landroid/graphics/Point;

    const/16 v16, 0x4

    const/16 v18, 0x1

    move-object/from16 v17, v9

    .line 1116
    invoke-static/range {v10 .. v18}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    .line 1121
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    invoke-virtual {v2, v9}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 1122
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 1123
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    .line 1124
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v12

    .line 1125
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v13

    .line 1126
    new-instance v14, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    invoke-direct {v14, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1127
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    const/16 v16, 0x0

    .line 1128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v17, v0

    .line 1129
    const/16 v18, 0x0

    move-object/from16 v10, p0

    .line 1123
    invoke-direct/range {v10 .. v18}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Landroid/graphics/Path;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Z)V

    .line 1131
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mPrePt:Landroid/graphics/Point;

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 1132
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFirstPt:Landroid/graphics/Point;

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 1133
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 1135
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mStartSelect:Z

    goto/16 :goto_0

    .line 990
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private touchCropRect(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 914
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 915
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 916
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 917
    .local v1, "y":F
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 918
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 920
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 958
    :goto_0
    return-void

    .line 923
    :pswitch_0
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    .line 925
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->InitMoveObject(FF)I

    move-result v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchType:I

    .line 927
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchType:I

    if-lt v2, v3, :cond_0

    .line 928
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->StartMoveObject(FF)V

    .line 930
    :cond_0
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchType:I

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->setHandlerIconPressed(I)V

    goto :goto_0

    .line 934
    :pswitch_1
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    .line 936
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchType:I

    if-lt v2, v3, :cond_1

    .line 938
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->StartMoveObject(FF)V

    .line 941
    :cond_1
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchType:I

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->setHandlerIconPressed(I)V

    goto :goto_0

    .line 946
    :pswitch_2
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    .line 948
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchType:I

    if-lt v2, v3, :cond_2

    .line 950
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->EndMoveObject()V

    .line 953
    :cond_2
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchType:I

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->setHandlerIconPressed(I)V

    .line 955
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchType:I

    goto :goto_0

    .line 920
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public AnimationDone()V
    .locals 1

    .prologue
    .line 1865
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->isAnimating:Z

    .line 1866
    return-void
.end method

.method public Destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 199
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->destroy()V

    .line 201
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    .line 207
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 210
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    if-eqz v0, :cond_2

    .line 219
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->destroy()V

    .line 220
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    .line 227
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 228
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmapPress:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 229
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 230
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 231
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 232
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 233
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 234
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 235
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 236
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 238
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 239
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    .line 241
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_b:Landroid/graphics/Paint;

    .line 242
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_w:Landroid/graphics/Paint;

    .line 243
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectOuterPaint:Landroid/graphics/Paint;

    .line 244
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectInnerPaint:Landroid/graphics/Paint;

    .line 245
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropDarkBackgroundPaint:Landroid/graphics/Paint;

    .line 246
    return-void
.end method

.method public applyOriginal()[I
    .locals 17

    .prologue
    .line 409
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v1

    const v2, 0x11201305

    if-ne v1, v2, :cond_4

    .line 411
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 413
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v16

    .line 414
    .local v16, "scale":F
    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    .line 415
    .local v12, "originalMaskRoi":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    div-float v1, v1, v16

    float-to-int v1, v1

    .line 416
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    div-float v2, v2, v16

    float-to-int v2, v2

    .line 417
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    div-float v3, v3, v16

    float-to-int v3, v3

    .line 418
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    div-float v5, v5, v16

    float-to-int v5, v5

    .line 415
    invoke-virtual {v12, v1, v2, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 420
    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v15

    .line 421
    .local v15, "resultWidth":I
    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v14

    .line 423
    .local v14, "resultHeight":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v9

    .line 424
    .local v9, "inBuffer":[I
    mul-int v1, v15, v14

    new-array v13, v1, [I

    .line 426
    .local v13, "outBuffer":[I
    array-length v1, v9

    new-array v4, v1, [B

    .line 428
    .local v4, "OriginalMask":[B
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    .line 429
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempWidth:I

    .line 430
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempHeight:I

    .line 432
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    .line 433
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v6

    .line 428
    invoke-static/range {v1 .. v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->makeScaleMaskBuff([BII[BII)V

    .line 434
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    .line 436
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-lt v7, v14, :cond_1

    .line 452
    const/4 v4, 0x0

    .line 453
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1, v13, v15, v14}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([III)V

    .line 460
    .end local v4    # "OriginalMask":[B
    .end local v7    # "i":I
    .end local v9    # "inBuffer":[I
    .end local v12    # "originalMaskRoi":Landroid/graphics/Rect;
    .end local v13    # "outBuffer":[I
    .end local v14    # "resultHeight":I
    .end local v15    # "resultWidth":I
    .end local v16    # "scale":F
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v1

    return-object v1

    .line 438
    .restart local v4    # "OriginalMask":[B
    .restart local v7    # "i":I
    .restart local v9    # "inBuffer":[I
    .restart local v12    # "originalMaskRoi":Landroid/graphics/Rect;
    .restart local v13    # "outBuffer":[I
    .restart local v14    # "resultHeight":I
    .restart local v15    # "resultWidth":I
    .restart local v16    # "scale":F
    :cond_1
    iget v1, v12, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v2

    mul-int v8, v1, v2

    .line 439
    .local v8, "ii":I
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_2
    if-lt v10, v15, :cond_2

    .line 436
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 441
    :cond_2
    iget v1, v12, Landroid/graphics/Rect;->left:I

    add-int v11, v10, v1

    .line 442
    .local v11, "jj":I
    add-int v1, v8, v11

    aget-byte v1, v4, v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 444
    mul-int v1, v7, v15

    add-int/2addr v1, v10

    add-int v2, v8, v11

    aget v2, v9, v2

    aput v2, v13, v1

    .line 439
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 448
    :cond_3
    mul-int v1, v7, v15

    add-int/2addr v1, v10

    const/4 v2, 0x0

    aput v2, v13, v1

    goto :goto_3

    .line 458
    .end local v4    # "OriginalMask":[B
    .end local v7    # "i":I
    .end local v8    # "ii":I
    .end local v9    # "inBuffer":[I
    .end local v10    # "j":I
    .end local v11    # "jj":I
    .end local v12    # "originalMaskRoi":Landroid/graphics/Rect;
    .end local v13    # "outBuffer":[I
    .end local v14    # "resultHeight":I
    .end local v15    # "resultWidth":I
    .end local v16    # "scale":F
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->calculateOrgRect(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    goto :goto_1
.end method

.method public applyPreview()Z
    .locals 13

    .prologue
    .line 366
    const/4 v10, 0x0

    .line 367
    .local v10, "ret":Z
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v11

    const v12, 0x11201305

    if-ne v11, v12, :cond_4

    .line 369
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    invoke-virtual {v11}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_0

    .line 371
    new-instance v7, Landroid/graphics/Rect;

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    invoke-direct {v7, v11}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 372
    .local v7, "previewMaskRoi":Landroid/graphics/Rect;
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v9

    .line 373
    .local v9, "resultWidth":I
    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v8

    .line 374
    .local v8, "resultHeight":I
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    .line 375
    .local v2, "inBuffer":[I
    mul-int v11, v9, v8

    new-array v5, v11, [I

    .line 376
    .local v5, "outBuffer":[I
    array-length v11, v2

    new-array v6, v11, [B

    .line 378
    .local v6, "previewMask":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v8, :cond_1

    .line 394
    const/4 v6, 0x0

    .line 395
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11, v5, v9, v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewBuffer([III)V

    .line 396
    const/4 v10, 0x1

    .line 404
    .end local v0    # "i":I
    .end local v2    # "inBuffer":[I
    .end local v5    # "outBuffer":[I
    .end local v6    # "previewMask":[B
    .end local v7    # "previewMaskRoi":Landroid/graphics/Rect;
    .end local v8    # "resultHeight":I
    .end local v9    # "resultWidth":I
    :cond_0
    :goto_1
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v11, v12}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->loadTextures(Landroid/content/Context;)V

    .line 405
    return v10

    .line 380
    .restart local v0    # "i":I
    .restart local v2    # "inBuffer":[I
    .restart local v5    # "outBuffer":[I
    .restart local v6    # "previewMask":[B
    .restart local v7    # "previewMaskRoi":Landroid/graphics/Rect;
    .restart local v8    # "resultHeight":I
    .restart local v9    # "resultWidth":I
    :cond_1
    iget v11, v7, Landroid/graphics/Rect;->top:I

    add-int/2addr v11, v0

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v12}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v12

    mul-int v1, v11, v12

    .line 381
    .local v1, "ii":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    if-lt v3, v9, :cond_2

    .line 378
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 383
    :cond_2
    iget v11, v7, Landroid/graphics/Rect;->left:I

    add-int v4, v3, v11

    .line 384
    .local v4, "jj":I
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    add-int v12, v1, v4

    aget-byte v11, v11, v12

    const/4 v12, 0x1

    if-ne v11, v12, :cond_3

    .line 386
    mul-int v11, v0, v9

    add-int/2addr v11, v3

    add-int v12, v1, v4

    aget v12, v2, v12

    aput v12, v5, v11

    .line 381
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 390
    :cond_3
    mul-int v11, v0, v9

    add-int/2addr v11, v3

    const/4 v12, 0x0

    aput v12, v5, v11

    goto :goto_3

    .line 401
    .end local v0    # "i":I
    .end local v1    # "ii":I
    .end local v2    # "inBuffer":[I
    .end local v3    # "j":I
    .end local v4    # "jj":I
    .end local v5    # "outBuffer":[I
    .end local v6    # "previewMask":[B
    .end local v7    # "previewMaskRoi":Landroid/graphics/Rect;
    .end local v8    # "resultHeight":I
    .end local v9    # "resultWidth":I
    :cond_4
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {p0, v11}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->calculateRect(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 402
    const/4 v10, 0x1

    goto :goto_1
.end method

.method public calculateInitialValues(Z)V
    .locals 4
    .param p1, "mFirst"    # Z

    .prologue
    .line 138
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v0

    .line 139
    .local v0, "drawRectRoi":Landroid/graphics/RectF;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    .line 140
    .local v2, "viewTransform":Landroid/graphics/Matrix;
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 141
    .local v1, "drawRoi":Landroid/graphics/RectF;
    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 142
    if-eqz p1, :cond_0

    .line 144
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    .line 146
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->rectToDraw:Landroid/graphics/RectF;

    .line 147
    return-void
.end method

.method public calculateOriginal()V
    .locals 1

    .prologue
    .line 974
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    if-eqz v0, :cond_0

    .line 976
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->calculateOriginal()V

    .line 978
    :cond_0
    return-void
.end method

.method public calculateanimationintertval()V
    .locals 3

    .prologue
    const/high16 v2, 0x40e00000    # 7.0f

    .line 519
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->rectToDraw:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervaltop:F

    .line 520
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->rectToDraw:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalright:F

    .line 521
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->rectToDraw:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalbottom:F

    .line 522
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->rectToDraw:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mAnimationIntervalleft:F

    .line 523
    return-void
.end method

.method public configurationChanged()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 288
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 311
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->configurationChanged()V

    .line 315
    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->calculateSrcToDstDrawCanvasRoi(Z)V

    .line 316
    return-void

    .line 291
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 292
    .local v2, "prevWidth":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 293
    .local v3, "prevHeight":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    if-nez v0, :cond_1

    .line 295
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    .line 296
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->init(II)V

    .line 298
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    if-nez v0, :cond_0

    .line 300
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    .line 301
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    .line 304
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v8, v8, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 305
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    const/4 v6, 0x0

    .line 306
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object v0, p0

    .line 301
    invoke-direct/range {v0 .. v8}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Landroid/graphics/Path;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Z)V

    goto :goto_0

    .line 288
    :pswitch_data_0
    .packed-switch 0x11201305
        :pswitch_0
    .end packed-switch
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/CropEffect;)V
    .locals 4
    .param p1, "crop"    # Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    .prologue
    const/4 v3, 0x0

    .line 66
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 67
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p1, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    .line 68
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->copy()Lcom/sec/android/mimage/photoretouching/Core/RectController;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    .line 69
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    array-length v0, v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    .line 72
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 74
    :cond_0
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempWidth:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempWidth:I

    .line 75
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempHeight:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempHeight:I

    .line 77
    return-void
.end method

.method public drawCropBdry(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 744
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    if-eqz v0, :cond_0

    .line 745
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->drawRectBdry(Landroid/graphics/Canvas;)V

    .line 746
    :cond_0
    return-void
.end method

.method public drawCropFreeBdry(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "viewBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "greyBitmap"    # Landroid/graphics/Bitmap;
    .param p4, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 466
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mStartSelect:Z

    if-nez v5, :cond_1

    .line 468
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 470
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    .line 471
    .local v3, "supMatrix":Landroid/graphics/Matrix;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    .line 473
    .local v4, "viewTransform":Landroid/graphics/Matrix;
    if-eqz p2, :cond_0

    .line 481
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    if-eqz v5, :cond_0

    .line 483
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 484
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 485
    .local v1, "m":Landroid/graphics/Matrix;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 487
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 489
    .local v0, "dst":Landroid/graphics/Path;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    invoke-virtual {v5, v1, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 491
    sget-object v5, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 492
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    const/4 v6, 0x0

    invoke-static {p1, p3, v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 493
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 495
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_w:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 496
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 512
    .end local v0    # "dst":Landroid/graphics/Path;
    .end local v1    # "m":Landroid/graphics/Matrix;
    .end local v3    # "supMatrix":Landroid/graphics/Matrix;
    .end local v4    # "viewTransform":Landroid/graphics/Matrix;
    :cond_0
    :goto_0
    return-void

    .line 504
    :cond_1
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 505
    .local v2, "path":Landroid/graphics/Path;
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 507
    .restart local v1    # "m":Landroid/graphics/Matrix;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 508
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    invoke-virtual {v5, v1, v2}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 509
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_w:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 510
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mLassoLinePaint_b:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public drawCropImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "viewBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 737
    .line 739
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 737
    invoke-static {p1, p2, v0, p3}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 741
    return-void
.end method

.method public drawCropImageWithBackground(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "viewBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "greyBitmap"    # Landroid/graphics/Bitmap;
    .param p4, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 526
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v9, :cond_8

    .line 528
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getAngle()I

    move-result v1

    .line 529
    .local v1, "angle":I
    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->calculateInitialValues(Z)V

    .line 531
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v8

    .line 534
    .local v8, "viewTransform":Landroid/graphics/Matrix;
    const/4 v7, 0x0

    .line 535
    .local v7, "temp":F
    iget-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->isAnimating:Z

    if-eqz v9, :cond_9

    .line 537
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->rectToDraw:Landroid/graphics/RectF;

    invoke-direct {p0, v9, v10}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->getRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    .line 544
    :goto_0
    new-instance v4, Landroid/graphics/RectF;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    invoke-direct {v4, v9}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 545
    .local v4, "innerRectH":Landroid/graphics/RectF;
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v9

    const/high16 v10, 0x40400000    # 3.0f

    div-float v7, v9, v10

    .line 546
    iget v9, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v9, v7

    iput v9, v4, Landroid/graphics/RectF;->top:F

    .line 547
    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v9, v7

    iput v9, v4, Landroid/graphics/RectF;->bottom:F

    .line 549
    new-instance v5, Landroid/graphics/RectF;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    invoke-direct {v5, v9}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 550
    .local v5, "innerRectV":Landroid/graphics/RectF;
    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v9

    const/high16 v10, 0x40400000    # 3.0f

    div-float v7, v9, v10

    .line 551
    iget v9, v5, Landroid/graphics/RectF;->left:F

    add-float/2addr v9, v7

    iput v9, v5, Landroid/graphics/RectF;->left:F

    .line 552
    iget v9, v5, Landroid/graphics/RectF;->right:F

    sub-float/2addr v9, v7

    iput v9, v5, Landroid/graphics/RectF;->right:F

    .line 554
    const/4 v9, 0x2

    new-array v6, v9, [F

    .line 555
    .local v6, "point":[F
    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v10

    iget v10, v10, Landroid/graphics/PointF;->x:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v11

    int-to-float v11, v11

    add-float/2addr v10, v11

    aput v10, v6, v9

    .line 556
    const/4 v9, 0x1

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v10

    iget v10, v10, Landroid/graphics/PointF;->y:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v11

    int-to-float v11, v11

    add-float/2addr v10, v11

    aput v10, v6, v9

    .line 557
    invoke-virtual {v8, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 560
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-static {p1, v0, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 562
    int-to-float v9, v1

    .line 563
    const/4 v10, 0x0

    aget v10, v6, v10

    .line 564
    const/4 v11, 0x1

    aget v11, v6, v11

    .line 562
    invoke-virtual {p1, v9, v10, v11}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 565
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    invoke-virtual {p1, v9}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 566
    neg-int v9, v1

    int-to-float v9, v9

    .line 567
    const/4 v10, 0x0

    aget v10, v6, v10

    .line 568
    const/4 v11, 0x1

    aget v11, v6, v11

    .line 566
    invoke-virtual {p1, v9, v10, v11}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 571
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    const/4 v10, 0x0

    invoke-static {p1, p2, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 572
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 574
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 575
    int-to-float v9, v1

    .line 576
    const/4 v10, 0x0

    aget v10, v6, v10

    .line 577
    const/4 v11, 0x1

    aget v11, v6, v11

    .line 575
    invoke-virtual {p1, v9, v10, v11}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 578
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectOuterPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v10}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 579
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectInnerPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v9}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 580
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mRectInnerPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v9}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 582
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    shr-int/lit8 v3, v9, 0x1

    .line 583
    .local v3, "halfWidth":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    shr-int/lit8 v2, v9, 0x1

    .line 586
    .local v2, "halfHeight":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    if-eqz v9, :cond_0

    .line 587
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 588
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    int-to-float v11, v3

    sub-float/2addr v10, v11

    .line 589
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    int-to-float v12, v2

    sub-float/2addr v11, v12

    .line 587
    move-object/from16 v0, p4

    invoke-virtual {p1, v9, v10, v11, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 592
    :cond_0
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    if-eqz v9, :cond_1

    .line 593
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 594
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    int-to-float v11, v3

    sub-float/2addr v10, v11

    .line 595
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    int-to-float v12, v2

    sub-float/2addr v11, v12

    .line 593
    move-object/from16 v0, p4

    invoke-virtual {p1, v9, v10, v11, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 598
    :cond_1
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    if-eqz v9, :cond_2

    .line 599
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 600
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    int-to-float v11, v3

    sub-float/2addr v10, v11

    .line 601
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->bottom:F

    int-to-float v12, v2

    sub-float/2addr v11, v12

    .line 599
    move-object/from16 v0, p4

    invoke-virtual {p1, v9, v10, v11, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 604
    :cond_2
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    if-eqz v9, :cond_3

    .line 605
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 606
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    int-to-float v11, v3

    sub-float/2addr v10, v11

    .line 607
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->bottom:F

    int-to-float v12, v2

    sub-float/2addr v11, v12

    .line 605
    move-object/from16 v0, p4

    invoke-virtual {p1, v9, v10, v11, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 628
    :cond_3
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->isFreeRect()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 630
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    if-eqz v9, :cond_4

    .line 631
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    .line 632
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    int-to-float v11, v3

    sub-float/2addr v10, v11

    .line 633
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    invoke-virtual {v11}, Landroid/graphics/RectF;->centerY()F

    move-result v11

    int-to-float v12, v2

    sub-float/2addr v11, v12

    .line 631
    move-object/from16 v0, p4

    invoke-virtual {p1, v9, v10, v11, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 636
    :cond_4
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    if-eqz v9, :cond_5

    .line 637
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    .line 638
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    invoke-virtual {v10}, Landroid/graphics/RectF;->centerX()F

    move-result v10

    int-to-float v11, v3

    sub-float/2addr v10, v11

    .line 639
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    int-to-float v12, v2

    sub-float/2addr v11, v12

    .line 637
    move-object/from16 v0, p4

    invoke-virtual {p1, v9, v10, v11, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 642
    :cond_5
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    if-eqz v9, :cond_6

    .line 643
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    .line 644
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    int-to-float v11, v3

    sub-float/2addr v10, v11

    .line 645
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    invoke-virtual {v11}, Landroid/graphics/RectF;->centerY()F

    move-result v11

    int-to-float v12, v2

    sub-float/2addr v11, v12

    .line 643
    move-object/from16 v0, p4

    invoke-virtual {p1, v9, v10, v11, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 648
    :cond_6
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    if-eqz v9, :cond_7

    .line 649
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    .line 650
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    invoke-virtual {v10}, Landroid/graphics/RectF;->centerX()F

    move-result v10

    int-to-float v11, v3

    sub-float/2addr v10, v11

    .line 651
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->bottom:F

    int-to-float v12, v2

    sub-float/2addr v11, v12

    .line 649
    move-object/from16 v0, p4

    invoke-virtual {p1, v9, v10, v11, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 673
    :cond_7
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 697
    .end local v1    # "angle":I
    .end local v2    # "halfHeight":I
    .end local v3    # "halfWidth":I
    .end local v4    # "innerRectH":Landroid/graphics/RectF;
    .end local v5    # "innerRectV":Landroid/graphics/RectF;
    .end local v6    # "point":[F
    .end local v7    # "temp":F
    .end local v8    # "viewTransform":Landroid/graphics/Matrix;
    :cond_8
    return-void

    .line 541
    .restart local v1    # "angle":I
    .restart local v7    # "temp":F
    .restart local v8    # "viewTransform":Landroid/graphics/Matrix;
    :cond_9
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->rectToDraw:Landroid/graphics/RectF;

    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->currentRect:Landroid/graphics/RectF;

    goto/16 :goto_0
.end method

.method public freeResource()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 177
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->rewind()V

    .line 180
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    if-eqz v0, :cond_1

    .line 189
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->destroy()V

    .line 190
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    .line 196
    :cond_1
    return-void
.end method

.method public getAngle()I
    .locals 2

    .prologue
    .line 256
    const/4 v0, 0x0

    .line 257
    .local v0, "angle":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    if-eqz v1, :cond_0

    .line 258
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getAngle()I

    move-result v0

    .line 259
    :cond_0
    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 86
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOrgDestPt1()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getOrgDestPt1()Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public getOrgDestPt2()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getOrgDestPt2()Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public getOrgDestPt3()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getOrgDestPt3()Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public getOrgDestPt4()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getOrgDestPt4()Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public getOriginalCenter()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getOriginalCenter()Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public getOriginalRoi()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getOriginalRoi()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getToggleRotate()Z
    .locals 1

    .prologue
    .line 1801
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mToggleRotate:Z

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 81
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(I)V
    .locals 3
    .param p1, "cropType"    # I

    .prologue
    const/4 v1, -0x1

    .line 149
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->freeResource()V

    .line 150
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 151
    const v0, 0x11201305

    if-ne p1, v0, :cond_0

    .line 152
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mPrePt:Landroid/graphics/Point;

    .line 153
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFirstPt:Landroid/graphics/Point;

    .line 155
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    .line 156
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropFreeRoi:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 157
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/jni/Util;->init_objectsel(II)I

    .line 158
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTempPreviewMaskBuffer:[B

    .line 161
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropLassoPath:Landroid/graphics/Path;

    .line 164
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    .line 165
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mChain:Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->init(II)V

    .line 168
    :cond_0
    return-void
.end method

.method public init(ILcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 6
    .param p1, "cropType"    # I
    .param p2, "imgInfo"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v5, 0x0

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->freeResource()V

    .line 92
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 93
    .local v1, "roi":Landroid/graphics/Rect;
    if-eqz v1, :cond_0

    .line 95
    iput v5, v1, Landroid/graphics/Rect;->left:I

    .line 96
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    div-int/lit8 v3, v3, 0x2

    iput v3, v1, Landroid/graphics/Rect;->right:I

    .line 97
    iput v5, v1, Landroid/graphics/Rect;->top:I

    .line 98
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    div-int/lit8 v3, v3, 0x2

    iput v3, v1, Landroid/graphics/Rect;->bottom:I

    .line 100
    iget v3, v1, Landroid/graphics/Rect;->right:I

    iget v4, v1, Landroid/graphics/Rect;->right:I

    div-int/lit8 v4, v4, 0x2

    sub-int v0, v3, v4

    .line 101
    .local v0, "hoffset":I
    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    iget v4, v1, Landroid/graphics/Rect;->bottom:I

    div-int/lit8 v4, v4, 0x2

    sub-int v2, v3, v4

    .line 103
    .local v2, "voffset":I
    iget v3, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v0

    iput v3, v1, Landroid/graphics/Rect;->left:I

    .line 104
    iget v3, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v0

    iput v3, v1, Landroid/graphics/Rect;->right:I

    .line 105
    iget v3, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v2

    iput v3, v1, Landroid/graphics/Rect;->top:I

    .line 106
    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v2

    iput v3, v1, Landroid/graphics/Rect;->bottom:I

    .line 108
    .end local v0    # "hoffset":I
    .end local v2    # "voffset":I
    :cond_0
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 109
    sparse-switch p1, :sswitch_data_0

    .line 125
    :goto_0
    return-void

    .line 114
    :sswitch_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0, v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->rotateFixedCrop(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/graphics/Rect;)V

    .line 115
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setRectRoi(Landroid/graphics/Rect;)V

    .line 116
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setFreeRect(Z)V

    goto :goto_0

    .line 121
    :sswitch_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v3, v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setRectRoi(Landroid/graphics/Rect;)V

    .line 122
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setFreeRect(Z)V

    goto :goto_0

    .line 109
    :sswitch_data_0
    .sparse-switch
        0x10001003 -> :sswitch_1
        0x11201302 -> :sswitch_0
        0x11201303 -> :sswitch_0
        0x11201304 -> :sswitch_0
        0x11201306 -> :sswitch_1
        0x11201307 -> :sswitch_1
    .end sparse-switch
.end method

.method public init(Landroid/graphics/Rect;I)V
    .locals 2
    .param p1, "autoReframeRect"    # Landroid/graphics/Rect;
    .param p2, "cropType"    # I

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->freeResource()V

    .line 130
    invoke-static {p2}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setRectRoi(Landroid/graphics/Rect;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setFreeRect(Z)V

    .line 134
    return-void
.end method

.method public isAnimationStarted()Z
    .locals 1

    .prologue
    .line 1862
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->isAnimating:Z

    return v0
.end method

.method public pinchZoom()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 320
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    if-eqz v0, :cond_2

    .line 323
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    if-eqz v0, :cond_0

    .line 325
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->setHandlerIconPressed(I)V

    .line 327
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mStartSelect:Z

    if-eqz v0, :cond_1

    .line 328
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mPrePt:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mFirstPt:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 332
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->pinchZoom()V

    .line 334
    :cond_2
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->calculateSrcToDstDrawCanvasRoi(Z)V

    .line 335
    return-void
.end method

.method public resetHandlerIconPressed()V
    .locals 1

    .prologue
    .line 750
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mTouchPressed:Z

    .line 752
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 753
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RT:Landroid/graphics/Bitmap;

    .line 754
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 755
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 756
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_L:Landroid/graphics/Bitmap;

    .line 757
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_T:Landroid/graphics/Bitmap;

    .line 758
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_R:Landroid/graphics/Bitmap;

    .line 759
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandleBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mHandler_B:Landroid/graphics/Bitmap;

    .line 760
    return-void
.end method

.method public rotateCropRect()V
    .locals 3

    .prologue
    .line 249
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 250
    .local v0, "roi":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setAngle(I)V

    .line 251
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->rotateFixedCrop(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/graphics/Rect;)V

    .line 252
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setRectRoi(Landroid/graphics/Rect;)V

    .line 253
    return-void
.end method

.method public rotateFixedCrop(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "imgInfo"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "roi"    # Landroid/graphics/Rect;

    .prologue
    .line 354
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    const v1, 0x11201302

    if-eq v0, v1, :cond_0

    .line 355
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    const v1, 0x11201303

    if-eq v0, v1, :cond_0

    .line 356
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    const v1, 0x11201304

    if-ne v0, v1, :cond_1

    .line 358
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->initFixedCrop(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/graphics/Rect;)V

    .line 361
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-virtual {v0, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setRectRoi(Landroid/graphics/Rect;)V

    .line 363
    return-void
.end method

.method public setActionBarManager(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)V
    .locals 0
    .param p1, "act"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .prologue
    .line 1871
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1872
    return-void
.end method

.method public setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V
    .locals 8
    .param p1, "mOrgDestRoi"    # Landroid/graphics/RectF;
    .param p2, "mOrgCenterPt"    # Landroid/graphics/PointF;
    .param p3, "mOrgDestPt1"    # Landroid/graphics/PointF;
    .param p4, "mOrgDestPt2"    # Landroid/graphics/PointF;
    .param p5, "mOrgDestPt3"    # Landroid/graphics/PointF;
    .param p6, "mOrgDestPt4"    # Landroid/graphics/PointF;
    .param p7, "angle"    # I

    .prologue
    .line 961
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    if-eqz v0, :cond_0

    .line 963
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mCropRect:Lcom/sec/android/mimage/photoretouching/Core/RectController;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V

    .line 971
    :cond_0
    return-void
.end method

.method public setToggleRotate(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 1804
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->mToggleRotate:Z

    .line 1805
    return-void
.end method

.method public startAnimation()V
    .locals 1

    .prologue
    .line 1868
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->isAnimating:Z

    .line 1869
    return-void
.end method

.method public touch(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 340
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 346
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->touchCropRect(Landroid/view/MotionEvent;)V

    .line 349
    :goto_0
    return-void

    .line 343
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->touchCropLasso(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 340
    nop

    :pswitch_data_0
    .packed-switch 0x11201305
        :pswitch_0
    .end packed-switch
.end method
