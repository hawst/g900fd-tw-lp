.class Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$17;
.super Ljava/lang/Object;
.source "NormalView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initActionbar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$17;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    .line 964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 6
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 973
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$17;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getButtonPosition(I)Landroid/graphics/Point;

    move-result-object v0

    .line 974
    .local v0, "point":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$17;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f060009

    iget v4, v0, Landroid/graphics/Point;->x:I

    iget v5, v0, Landroid/graphics/Point;->y:I

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, 0x2

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastActionbar(Landroid/content/Context;III)V

    .line 977
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$17;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "vibrator"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    .line 978
    .local v1, "vibrator":Landroid/os/Vibrator;
    const-wide/16 v2, 0xd

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 980
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$17;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$39(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Z)V

    .line 981
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 988
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$17;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$17;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 990
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$17;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 999
    :cond_0
    :goto_0
    return-void

    .line 992
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$17;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$17;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 994
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$17;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x2e000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 968
    return-void
.end method
