.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;
.super Ljava/lang/Object;
.source "PenActivity.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    .line 1805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1917
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 1808
    const/4 v3, -0x1

    .line 1809
    .local v3, "settingMode":I
    const/4 v0, -0x1

    .line 1810
    .local v0, "fingerAction":I
    const/4 v5, -0x1

    .line 1811
    .local v5, "spenAction":I
    const/4 v4, -0x1

    .line 1812
    .local v4, "settingViewSize":I
    const/4 v1, -0x1

    .line 1813
    .local v1, "mouseAction":I
    const/4 v2, -0x1

    .line 1814
    .local v2, "penhaveremove":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 1864
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    const v8, 0x1a001842

    if-eq v7, v8, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    const v8, 0x1a001843

    if-ne v7, v8, :cond_4

    .line 1868
    :cond_0
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v7

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getToolTypeAction(I)I

    move-result v7

    const/4 v8, 0x5

    if-ne v7, v8, :cond_1

    .line 1869
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/pen/util/SettingView;->closeSettingView()V

    .line 1872
    :cond_1
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v7

    invoke-virtual {v7, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getToolTypeAction(I)I

    move-result v7

    if-eq v7, v5, :cond_2

    .line 1875
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v7

    invoke-virtual {v7, v9, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    .line 1876
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v7

    invoke-virtual {v7, v10, v5}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    .line 1877
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v7, v8, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    .line 1878
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v7

    const/4 v8, 0x3

    invoke-virtual {v7, v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    .line 1884
    :cond_2
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mMode:I
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$43(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)I

    move-result v7

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    if-ne v7, v8, :cond_6

    .line 1888
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v7

    invoke-virtual {v7, v3, v4}, Lcom/sec/android/pen/util/SettingView;->toggleShowSettingView(II)Z

    .line 1890
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/pen/util/SettingView;->setVisibility(I)V

    .line 1899
    :goto_1
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    invoke-static {v7, v8}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$44(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;I)V

    .line 1901
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->checkSettingViewInited(I)Z
    invoke-static {v7, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$45(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;I)Z

    move-result v7

    if-nez v7, :cond_3

    .line 1903
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->refreshPopupState(IZ)V
    invoke-static {v7, v3, v9}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$42(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;IZ)V

    .line 1905
    :cond_3
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTipEnabled(Z)V

    .line 1907
    :cond_4
    return-void

    .line 1817
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$22(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v7

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    .line 1818
    const/4 v3, 0x1

    .line 1819
    const/4 v0, 0x2

    .line 1820
    const/4 v5, 0x2

    .line 1821
    const/4 v1, 0x2

    .line 1822
    const/4 v2, 0x4

    .line 1823
    const/4 v4, 0x2

    .line 1824
    goto/16 :goto_0

    .line 1826
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$22(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v7

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    .line 1827
    const/4 v3, 0x2

    .line 1828
    const/4 v0, 0x4

    .line 1829
    const/4 v5, 0x4

    .line 1830
    const/4 v1, 0x4

    .line 1831
    const/4 v2, 0x4

    .line 1832
    const/4 v4, 0x0

    .line 1833
    goto/16 :goto_0

    .line 1835
    :pswitch_2
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->closeControl()V

    .line 1841
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$25(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isUndoable()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1842
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$25(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->undo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v6

    .line 1843
    .local v6, "userData":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    .line 1849
    .end local v6    # "userData":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    :cond_5
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-static {v7, v9}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$26(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V

    .line 1850
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mUndoStarckEmpty:Z
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$27(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->refreshUndoRedoBtnState(Z)V

    goto/16 :goto_0

    .line 1853
    :pswitch_3
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->closeControl()V

    .line 1859
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$25(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->redo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v6

    .line 1860
    .restart local v6    # "userData":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->updateRedo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    .line 1861
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mUndoStarckEmpty:Z
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$27(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->refreshUndoRedoBtnState(Z)V

    goto/16 :goto_0

    .line 1894
    .end local v6    # "userData":[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;
    :cond_6
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/sec/android/pen/util/SettingView;->setVisibility(I)V

    .line 1895
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/pen/util/SettingView;->closeSettingView()V

    goto/16 :goto_1

    .line 1814
    nop

    :pswitch_data_0
    .packed-switch 0x1a001842
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1912
    return-void
.end method
