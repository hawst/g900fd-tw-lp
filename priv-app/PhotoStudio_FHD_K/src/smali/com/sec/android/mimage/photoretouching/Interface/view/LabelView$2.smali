.class Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;
.super Ljava/lang/Object;
.source "LabelView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnLabelCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setLabel(II)V
    .locals 5
    .param p1, "resId"    # I
    .param p2, "styleNum"    # I

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;I)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    invoke-static {v0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;I)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    move-result-object v0

    if-nez v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelCallback:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;)V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;)V

    .line 159
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sj, LV - onLabelCallback - resId : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / styleNum : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 160
    const-string v1, " / mLabelEffect.getCurLabelCount() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->getCurLabelCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->getCurLabelCount()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 165
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 172
    :goto_0
    return-void

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0601a6

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;I)V

    goto :goto_0
.end method
