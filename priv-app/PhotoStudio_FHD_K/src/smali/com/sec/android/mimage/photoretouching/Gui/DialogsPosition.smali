.class public Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;
.super Ljava/lang/Object;
.source "DialogsPosition.java"


# static fields
.field private static mLandscapeBrush:Landroid/graphics/Point;

.field private static mLandscapeColor:Landroid/graphics/Point;

.field private static mLandscapeCrop:Landroid/graphics/Point;

.field private static mLandscapeEffect:Landroid/graphics/Point;

.field private static mLandscapeEffectBlur:Landroid/graphics/Point;

.field private static mLandscapeEffectDistort:Landroid/graphics/Point;

.field private static mLandscapeEffectFilter:Landroid/graphics/Point;

.field private static mLandscapeEffectFrames:Landroid/graphics/Point;

.field private static mLandscapeEffectMotion:Landroid/graphics/Point;

.field private static mLandscapeProgress:Landroid/graphics/Point;

.field private static mLandscapeScale:Landroid/graphics/Point;

.field private static mLandscapeSelect:Landroid/graphics/Point;

.field private static mLandscapeSelectArea:Landroid/graphics/Point;

.field private static mLandscapeSelectBrush:Landroid/graphics/Point;

.field private static mLandscapeSelectMode:Landroid/graphics/Point;

.field private static mLandscapeSelectSize:Landroid/graphics/Point;

.field private static mLandscapeSticker:Landroid/graphics/Point;

.field private static mLandscapeTools:Landroid/graphics/Point;

.field private static mPortraitBrush:Landroid/graphics/Point;

.field private static mPortraitColor:Landroid/graphics/Point;

.field private static mPortraitCrop:Landroid/graphics/Point;

.field private static mPortraitEffect:Landroid/graphics/Point;

.field private static mPortraitEffectBlur:Landroid/graphics/Point;

.field private static mPortraitEffectDistort:Landroid/graphics/Point;

.field private static mPortraitEffectFilter:Landroid/graphics/Point;

.field private static mPortraitEffectFrames:Landroid/graphics/Point;

.field private static mPortraitEffectMotion:Landroid/graphics/Point;

.field private static mPortraitProgress:Landroid/graphics/Point;

.field private static mPortraitScale:Landroid/graphics/Point;

.field private static mPortraitSelect:Landroid/graphics/Point;

.field private static mPortraitSelectArea:Landroid/graphics/Point;

.field private static mPortraitSelectBrush:Landroid/graphics/Point;

.field private static mPortraitSelectMode:Landroid/graphics/Point;

.field private static mPortraitSelectSize:Landroid/graphics/Point;

.field private static mPortraitSticker:Landroid/graphics/Point;

.field private static mPortraitTools:Landroid/graphics/Point;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v2, 0x334

    const/16 v6, 0x1e4

    const/16 v5, 0x42

    const/16 v4, 0x1b9

    const/16 v3, 0x64

    .line 14
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0xae

    invoke-direct {v0, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeColor:Landroid/graphics/Point;

    .line 15
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v2, v4}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeEffect:Landroid/graphics/Point;

    .line 16
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x1e5

    invoke-direct {v0, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeTools:Landroid/graphics/Point;

    .line 17
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x36b

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeScale:Landroid/graphics/Point;

    .line 18
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v6, v4}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeEffectBlur:Landroid/graphics/Point;

    .line 19
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v6, v4}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeEffectMotion:Landroid/graphics/Point;

    .line 20
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x84

    const/16 v2, 0x22f

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeEffectFilter:Landroid/graphics/Point;

    .line 21
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x161

    const/16 v2, 0x22f

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeEffectFrames:Landroid/graphics/Point;

    .line 22
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x55

    invoke-direct {v0, v3, v1}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeSelect:Landroid/graphics/Point;

    .line 23
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x46

    const/16 v2, 0x1c2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeSelectBrush:Landroid/graphics/Point;

    .line 24
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x171

    const/16 v2, 0x276

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeProgress:Landroid/graphics/Point;

    .line 25
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v6, v4}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeEffectDistort:Landroid/graphics/Point;

    .line 27
    new-instance v0, Landroid/graphics/Point;

    const/4 v1, 0x0

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeBrush:Landroid/graphics/Point;

    .line 29
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x1a4

    const/16 v2, 0x39d

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeSelectArea:Landroid/graphics/Point;

    .line 30
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x564

    const/16 v2, 0x45b

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeSelectSize:Landroid/graphics/Point;

    .line 31
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x933

    const/16 v2, 0x294

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeSelectMode:Landroid/graphics/Point;

    .line 32
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x168

    const/16 v2, 0x118

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeCrop:Landroid/graphics/Point;

    .line 33
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeSticker:Landroid/graphics/Point;

    .line 35
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x14a

    const/16 v2, 0x230

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitColor:Landroid/graphics/Point;

    .line 36
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x192

    const/16 v2, 0x2e1

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitEffect:Landroid/graphics/Point;

    .line 37
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x1f1

    const/16 v2, 0x369

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitTools:Landroid/graphics/Point;

    .line 38
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x21c

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitScale:Landroid/graphics/Point;

    .line 39
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x2e1

    invoke-direct {v0, v5, v1}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitEffectBlur:Landroid/graphics/Point;

    .line 40
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x31b

    invoke-direct {v0, v5, v1}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitEffectMotion:Landroid/graphics/Point;

    .line 41
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x2d

    const/16 v2, 0x33e

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitEffectFilter:Landroid/graphics/Point;

    .line 42
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x6e

    const/16 v2, 0x33e

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitEffectFrames:Landroid/graphics/Point;

    .line 43
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0xa

    const/16 v2, 0xa0

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitSelect:Landroid/graphics/Point;

    .line 44
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x10e

    const/16 v2, 0xaf

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitSelectBrush:Landroid/graphics/Point;

    .line 45
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x80

    const/16 v2, 0x3c8

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitProgress:Landroid/graphics/Point;

    .line 46
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x31b

    invoke-direct {v0, v5, v1}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitEffectDistort:Landroid/graphics/Point;

    .line 48
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x96

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitBrush:Landroid/graphics/Point;

    .line 50
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x85

    const/16 v2, 0x755

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitSelectArea:Landroid/graphics/Point;

    .line 51
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x2c6

    const/16 v2, 0x816

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitSelectSize:Landroid/graphics/Point;

    .line 52
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x3ff

    const/16 v2, 0x64f

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitSelectMode:Landroid/graphics/Point;

    .line 53
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x7d

    const/16 v2, 0x2f8

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitCrop:Landroid/graphics/Point;

    .line 54
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitSticker:Landroid/graphics/Point;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBrushDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 79
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeBrush:Landroid/graphics/Point;

    .line 85
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitBrush:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getBrushSeekbarDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 245
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeSelectBrush:Landroid/graphics/Point;

    .line 251
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitSelectBrush:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getColorDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 157
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeColor:Landroid/graphics/Point;

    .line 163
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitColor:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getCropDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 168
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeCrop:Landroid/graphics/Point;

    .line 174
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitCrop:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getEffectBlurDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 201
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeEffectBlur:Landroid/graphics/Point;

    .line 207
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitEffectBlur:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getEffectDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 179
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeEffect:Landroid/graphics/Point;

    .line 185
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitEffect:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getEffectDistortionDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 256
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeEffectDistort:Landroid/graphics/Point;

    .line 262
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitEffectDistort:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getEffectMotionDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 212
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeEffectMotion:Landroid/graphics/Point;

    .line 218
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitEffectMotion:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getFilterDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 135
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeEffectFilter:Landroid/graphics/Point;

    .line 141
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitEffectFilter:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getFramesDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 146
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeEffectFrames:Landroid/graphics/Point;

    .line 152
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitEffectFrames:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getProgressDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 124
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeProgress:Landroid/graphics/Point;

    .line 130
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitProgress:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getScaleDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 223
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeScale:Landroid/graphics/Point;

    .line 229
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitScale:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getSelectAreaDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeSelectArea:Landroid/graphics/Point;

    .line 63
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitSelectArea:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getSelectDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 113
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeSelect:Landroid/graphics/Point;

    .line 119
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitSelect:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getSelectModeDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 102
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeSelectMode:Landroid/graphics/Point;

    .line 108
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitSelectMode:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getSelectSizeDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 90
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeSelectSize:Landroid/graphics/Point;

    .line 96
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitSelectSize:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getStickerDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 267
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeSticker:Landroid/graphics/Point;

    .line 273
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitSticker:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public static getToolsDialogPos()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 190
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mLandscapeTools:Landroid/graphics/Point;

    .line 196
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->mPortraitTools:Landroid/graphics/Point;

    goto :goto_0
.end method
