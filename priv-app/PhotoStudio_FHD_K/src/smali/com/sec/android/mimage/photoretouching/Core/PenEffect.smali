.class public Lcom/sec/android/mimage/photoretouching/Core/PenEffect;
.super Ljava/lang/Object;
.source "PenEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/PenEffect$OnCallback;
    }
.end annotation


# static fields
.field public static final ORGINALVIEW_OFF:I = 0x0

.field public static final ORGINALVIEW_ON:I = 0x1

.field public static isFirstTimeDnL:Z


# instance fields
.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mPreviewBuffer:[I

.field private mPreviewHeight:I

.field private mPreviewWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->isFirstTimeDnL:Z

    .line 84
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 86
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewBuffer:[I

    .line 87
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewWidth:I

    .line 88
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewHeight:I

    .line 23
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 5
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 86
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewBuffer:[I

    .line 87
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewWidth:I

    .line 88
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewHeight:I

    .line 26
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 28
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewWidth:I

    .line 29
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewHeight:I

    .line 30
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewWidth:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewHeight:I

    mul-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewBuffer:[I

    .line 31
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewBuffer:[I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewWidth:I

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewHeight:I

    mul-int/2addr v2, v3

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 32
    return-void
.end method


# virtual methods
.method public Destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 53
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 54
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewWidth:I

    .line 55
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewHeight:I

    .line 56
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewBuffer:[I

    .line 57
    return-void
.end method

.method public applyOriginal()[I
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 60
    const/4 v1, 0x0

    .line 61
    .local v1, "output":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewBuffer:[I

    if-eqz v3, :cond_0

    .line 63
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewBuffer:[I

    .line 64
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 65
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    .line 66
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 63
    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 68
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    .line 69
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v4

    .line 70
    const/4 v5, 0x1

    .line 67
    invoke-static {v8, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 71
    .local v0, "tempBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    new-array v1, v3, [I

    .line 72
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 73
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([I)V

    .line 74
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 75
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 76
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewBuffer:[I

    .line 78
    .end local v0    # "tempBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    return-object v1
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/PenEffect;)V
    .locals 1
    .param p1, "peneffect"    # Lcom/sec/android/mimage/photoretouching/Core/PenEffect;

    .prologue
    .line 35
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 36
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewWidth:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewWidth:I

    .line 37
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewHeight:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewHeight:I

    .line 38
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewBuffer:[I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mPreviewBuffer:[I

    .line 39
    return-void
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 49
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PenEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 44
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
