.class Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$5;
.super Ljava/lang/Object;
.source "SelectView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initActionbar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    .line 309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 0
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 321
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 325
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->clearSelectArea()V

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 329
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->getPreviousStatus()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(ILcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;)V

    .line 331
    :cond_1
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 315
    return-void
.end method
