.class Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;
.super Ljava/lang/Object;
.source "RotateView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    .line 823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    .locals 1

    .prologue
    .line 823
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    return-object v0
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 995
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v6, 0x7f0204f3

    const/4 v7, 0x1

    const v5, 0x11001108

    .line 826
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->anim:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->getIsAnimate()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 984
    :goto_0
    return-void

    .line 828
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 829
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->destroy()V

    .line 830
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;)V

    .line 832
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setClearSelectedButton()V

    .line 834
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v3, v5, :cond_2

    .line 836
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->checkHelpPopup(I)V
    invoke-static {v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;I)V

    .line 837
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v3

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeSelectionBtnIcon(II)V

    .line 838
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v3

    invoke-virtual {v3, p1, v7}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(Landroid/view/View;Z)V

    .line 840
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    invoke-static {v3, v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Z)V

    .line 841
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->applyToPreview()Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;

    move-result-object v0

    .line 842
    .local v0, "info":Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->getRotatedWidth()I

    move-result v2

    .line 843
    .local v2, "mirrorBufferWidth":I
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->getRotatedHeight()I

    move-result v1

    .line 844
    .local v1, "mirrorBufferHeight":I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;)V

    .line 845
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mMirrorEffect:Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;

    move-result-object v3

    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;

    invoke-direct {v4, p0, v2, v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;IILandroid/view/View;)V

    invoke-virtual {v3, v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;->init(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;)V

    .line 983
    .end local v0    # "info":Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;
    .end local v1    # "mirrorBufferHeight":I
    .end local v2    # "mirrorBufferWidth":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v3

    invoke-virtual {v3, p1, v7}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(Landroid/view/View;Z)V

    goto/16 :goto_0

    .line 979
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v3

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeSelectionBtnIcon(II)V

    .line 980
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Z)V

    .line 981
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mOnFilp3dAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    invoke-interface {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;->startAnimation(I)V

    goto :goto_1
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 989
    return-void
.end method
