.class public interface abstract Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo$DetailedInfoCallback;
.super Ljava/lang/Object;
.source "MultigridDetailedInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DetailedInfoCallback"
.end annotation


# virtual methods
.method public abstract getDrawItemAngel(I)I
.end method

.method public abstract getDrawItemControlRoi(I)Landroid/graphics/RectF;
.end method

.method public abstract getDrawItemRect(I)Landroid/graphics/RectF;
.end method

.method public abstract getDrawSrcRect(I)Landroid/graphics/RectF;
.end method

.method public abstract getGridSplitCount()I
.end method

.method public abstract getGridType()I
.end method
