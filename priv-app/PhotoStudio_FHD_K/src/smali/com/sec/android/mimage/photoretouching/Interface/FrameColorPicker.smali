.class public Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;
.super Landroid/widget/LinearLayout;
.source "FrameColorPicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;
    }
.end annotation


# static fields
.field public static COLLAGE_BACKGROUND:I

.field public static DECO_BACKGROUND:I


# instance fields
.field private final COLOR_01:I

.field private final COLOR_02:I

.field private final COLOR_03:I

.field private final COLOR_04:I

.field private final COLOR_05:I

.field private final COLOR_06:I

.field private final COLOR_07:I

.field private final COLOR_08:I

.field private final COLOR_09:I

.field private final COLOR_10:I

.field private final COLOR_11:I

.field private final COLOR_12:I

.field private final COLOR_13:I

.field private final COLOR_14:I

.field private final COLOR_BUTTON_NUM:I

.field private mColorButtonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mColorPalette:Landroid/widget/RelativeLayout;

.field private mColorPaletteBitmap:Landroid/graphics/Bitmap;

.field private mColorPalettePicker:Landroid/widget/LinearLayout;

.field private mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;

.field private mColorPickerLayout:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mCurrentColor:I

.field private mMainLayout:Landroid/widget/LinearLayout;

.field private mOnClickColorButton:Landroid/view/View$OnClickListener;

.field private mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

.field private mPaletteHeight:I

.field private mPaletteWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 361
    const v0, 0x7f020121

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLLAGE_BACKGROUND:I

    .line 362
    const v0, 0x7f0201ac

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->DECO_BACKGROUND:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 26
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 344
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_BUTTON_NUM:I

    .line 346
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_01:I

    .line 347
    const v0, -0x200d3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_02:I

    .line 348
    const/16 v0, -0x7ca3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_03:I

    .line 349
    const v0, -0xc4a5

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_04:I

    .line 350
    const v0, -0xb637

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_05:I

    .line 351
    const v0, -0x357a01

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_06:I

    .line 352
    const v0, -0x85c822

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_07:I

    .line 353
    const v0, -0xfe6bd2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_08:I

    .line 354
    const v0, -0xc75701

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_09:I

    .line 355
    const v0, -0xcc9803

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_10:I

    .line 356
    const v0, -0x595a5b

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_11:I

    .line 357
    const v0, -0xc9c9ca

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_12:I

    .line 358
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_13:I

    .line 359
    const/16 v0, -0x199e

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_14:I

    .line 364
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mContext:Landroid/content/Context;

    .line 365
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    .line 366
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    .line 367
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalette:Landroid/widget/RelativeLayout;

    .line 368
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    .line 369
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    .line 371
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mOnClickColorButton:Landroid/view/View$OnClickListener;

    .line 372
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

    .line 373
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    .line 375
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;

    .line 377
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mCurrentColor:I

    .line 378
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mPaletteWidth:I

    .line 379
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mPaletteHeight:I

    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->init(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 344
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_BUTTON_NUM:I

    .line 346
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_01:I

    .line 347
    const v0, -0x200d3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_02:I

    .line 348
    const/16 v0, -0x7ca3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_03:I

    .line 349
    const v0, -0xc4a5

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_04:I

    .line 350
    const v0, -0xb637

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_05:I

    .line 351
    const v0, -0x357a01

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_06:I

    .line 352
    const v0, -0x85c822

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_07:I

    .line 353
    const v0, -0xfe6bd2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_08:I

    .line 354
    const v0, -0xc75701

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_09:I

    .line 355
    const v0, -0xcc9803

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_10:I

    .line 356
    const v0, -0x595a5b

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_11:I

    .line 357
    const v0, -0xc9c9ca

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_12:I

    .line 358
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_13:I

    .line 359
    const/16 v0, -0x199e

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_14:I

    .line 364
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mContext:Landroid/content/Context;

    .line 365
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    .line 366
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    .line 367
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalette:Landroid/widget/RelativeLayout;

    .line 368
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    .line 369
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    .line 371
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mOnClickColorButton:Landroid/view/View$OnClickListener;

    .line 372
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

    .line 373
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    .line 375
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;

    .line 377
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mCurrentColor:I

    .line 378
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mPaletteWidth:I

    .line 379
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mPaletteHeight:I

    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->init(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 344
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_BUTTON_NUM:I

    .line 346
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_01:I

    .line 347
    const v0, -0x200d3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_02:I

    .line 348
    const/16 v0, -0x7ca3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_03:I

    .line 349
    const v0, -0xc4a5

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_04:I

    .line 350
    const v0, -0xb637

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_05:I

    .line 351
    const v0, -0x357a01

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_06:I

    .line 352
    const v0, -0x85c822

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_07:I

    .line 353
    const v0, -0xfe6bd2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_08:I

    .line 354
    const v0, -0xc75701

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_09:I

    .line 355
    const v0, -0xcc9803

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_10:I

    .line 356
    const v0, -0x595a5b

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_11:I

    .line 357
    const v0, -0xc9c9ca

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_12:I

    .line 358
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_13:I

    .line 359
    const/16 v0, -0x199e

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->COLOR_14:I

    .line 364
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mContext:Landroid/content/Context;

    .line 365
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    .line 366
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    .line 367
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalette:Landroid/widget/RelativeLayout;

    .line 368
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    .line 369
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    .line 371
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mOnClickColorButton:Landroid/view/View$OnClickListener;

    .line 372
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

    .line 373
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    .line 375
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;

    .line 377
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mCurrentColor:I

    .line 378
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mPaletteWidth:I

    .line 379
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mPaletteHeight:I

    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->init(Landroid/content/Context;)V

    .line 35
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V
    .locals 0

    .prologue
    .line 377
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mCurrentColor:I

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;I)V
    .locals 0

    .prologue
    .line 261
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->setSelected(I)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;Landroid/view/MotionEvent;Z)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->setPickerPos(Landroid/view/MotionEvent;Z)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;)Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;)I
    .locals 1

    .prologue
    .line 377
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mCurrentColor:I

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;Landroid/view/MotionEvent;)I
    .locals 1

    .prologue
    .line 202
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->getColor(Landroid/view/MotionEvent;)I

    move-result v0

    return v0
.end method

.method private getColor(Landroid/view/MotionEvent;)I
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    .line 204
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v0, v2

    .line 205
    .local v0, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v1, v2

    .line 207
    .local v1, "y":I
    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mPaletteWidth:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 208
    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mPaletteHeight:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 211
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    return v2
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mContext:Landroid/content/Context;

    .line 67
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030036

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    .line 68
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f090089

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalette:Landroid/widget/RelativeLayout;

    .line 69
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f09008a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    .line 72
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 75
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->initListener()V

    .line 76
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->initColorButtons()V

    .line 78
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalette:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalette:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 82
    :cond_0
    return-void
.end method

.method private initColorButtons()V
    .locals 4

    .prologue
    .line 245
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 247
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    .line 250
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v2, 0xe

    if-lt v1, v2, :cond_1

    .line 259
    return-void

    .line 252
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f09007a

    add-int/2addr v3, v1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 253
    .local v0, "button":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_2

    .line 255
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mOnClickColorButton:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 256
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 250
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private initListener()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mOnClickColorButton:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mOnClickColorButton:Landroid/view/View$OnClickListener;

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201a9

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    .line 165
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mPaletteWidth:I

    .line 166
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mPaletteHeight:I

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

    if-nez v0, :cond_3

    .line 171
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

    .line 200
    :cond_3
    return-void
.end method

.method private setPickerPos(Landroid/view/MotionEvent;Z)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "isShow"    # Z

    .prologue
    .line 50
    if-eqz p2, :cond_0

    .line 52
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 53
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 54
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iget v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 55
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iget v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 56
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 62
    .end local v0    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setSelected(I)V
    .locals 3
    .param p1, "selectedButtonId"    # I

    .prologue
    .line 263
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 274
    return-void

    .line 265
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne p1, v1, :cond_1

    .line 267
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 263
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 271
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto :goto_1
.end method


# virtual methods
.method public configurationChanged()V
    .locals 3

    .prologue
    .line 216
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : configurationChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 217
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 219
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 220
    .local v0, "viewGroup":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 222
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 226
    .end local v0    # "viewGroup":Landroid/view/ViewGroup;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const v2, 0x7f0900b1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    .line 232
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 236
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 237
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    .line 238
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

    .line 239
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mOnClickColorButton:Landroid/view/View$OnClickListener;

    .line 240
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    .line 241
    return-void
.end method

.method public getCurrentColor()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mCurrentColor:I

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 339
    const/4 v0, 0x1

    .line 341
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 92
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onLayout : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public setBackground(I)V
    .locals 1
    .param p1, "backId"    # I

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 87
    return-void
.end method

.method public setColorPickerCallback(Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;)V
    .locals 0
    .param p1, "colorPickerCallback"    # Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;

    .prologue
    .line 382
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker$ColorPickerCallback;

    .line 383
    return-void
.end method

.method public setCurrentColor(I)V
    .locals 0
    .param p1, "mCurrentColor"    # I

    .prologue
    .line 281
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mCurrentColor:I

    .line 282
    return-void
.end method

.method public setPosition(II)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v1, -0x2

    .line 286
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 287
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : setPosition : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 289
    add-int/lit8 v1, p1, -0x1e

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 290
    add-int/lit16 v1, p2, 0x8a

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 292
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 293
    const/4 v0, 0x0

    .line 294
    return-void
.end method

.method public show(ZII)V
    .locals 0
    .param p1, "isShow"    # Z
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 331
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->show(Z)Z

    .line 332
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->setPosition(II)V

    .line 333
    return-void
.end method

.method public show(Z)Z
    .locals 6
    .param p1, "show"    # Z

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 298
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 299
    .local v1, "viewGroup":Landroid/view/ViewGroup;
    const/4 v0, 0x0

    .line 301
    .local v0, "isHided":Z
    if-eqz v1, :cond_0

    .line 303
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 306
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const v3, 0x7f0900b1

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    .line 307
    if-eqz p1, :cond_3

    .line 309
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-nez v2, :cond_2

    .line 311
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 312
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 313
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 326
    :cond_2
    :goto_0
    return v0

    .line 318
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 320
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 321
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/FrameColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 322
    const/4 v0, 0x1

    goto :goto_0
.end method
