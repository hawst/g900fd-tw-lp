.class Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;
.super Ljava/lang/Thread;
.source "TrayButton.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->initHistoryManager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 314
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 319
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 320
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    if-eqz v1, :cond_0

    .line 321
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v1

    if-eqz v1, :cond_0

    .line 323
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v2

    .line 324
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    .line 325
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v4

    .line 323
    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->initFirstOriginalHistory([III)V

    .line 326
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    .line 327
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 328
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton$4;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    .line 326
    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->initFirstPreviewHistory([III)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 330
    :catch_0
    move-exception v0

    .line 331
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TrayButton - initHistoryManager() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method
