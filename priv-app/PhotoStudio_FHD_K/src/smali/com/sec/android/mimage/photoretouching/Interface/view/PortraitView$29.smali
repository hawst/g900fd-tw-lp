.class Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$29;
.super Ljava/lang/Object;
.source "PortraitView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initPortraitView(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$29;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    .line 1980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterApplyPreview()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1990
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$29;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$29;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    if-nez v3, :cond_1

    .line 2005
    :cond_0
    :goto_0
    return-void

    .line 1993
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$29;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 1994
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$29;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1995
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$29;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1998
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$29;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 2001
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$29;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 2002
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$29;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 2003
    const/4 v8, 0x1

    .line 2004
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$29;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$38(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/graphics/Paint;

    move-result-object v9

    move v4, v2

    move v5, v2

    .line 1996
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 1985
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$29;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->invalidateViewsWithThread()V

    .line 1986
    return-void
.end method
