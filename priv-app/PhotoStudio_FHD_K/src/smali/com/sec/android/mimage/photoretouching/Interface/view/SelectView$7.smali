.class Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$7;
.super Ljava/lang/Object;
.source "SelectView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initSelectNotiDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    .line 571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public TouchFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 575
    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 577
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    move-object v0, p1

    check-cast v0, Landroid/widget/CheckBox;

    invoke-static {v1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Landroid/widget/CheckBox;)V

    .line 578
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/widget/CheckBox;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 579
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->invalidate()V

    .line 580
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sj, SV - initSelectNotiDialog() - mCheckBox.isChecked() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 581
    const-string v1, " / v : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 580
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 583
    :cond_0
    return-void

    .line 578
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
