.class public final Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBase$CreateDB;
.super Ljava/lang/Object;
.source "BottomButtonDataBase.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateDB"
.end annotation


# static fields
.field public static final BUTTONID:Ljava/lang/String; = "button_id"

.field public static final BUTTONINDEX:Ljava/lang/String; = "button_idx"

.field public static final BUTTONTYPE:Ljava/lang/String; = "effect_type"

.field public static final RECENT:Ljava/lang/String; = "recent_used"

.field public static final USE:Ljava/lang/String; = "use"

.field public static final _CREATE:Ljava/lang/String; = "create table common_button_table(button_id integer primary key , effect_type integer , button_idx integer , recent_used integer , use integer );"

.field public static final _CREATE_EFFECT:Ljava/lang/String; = "create table effect_button_table(button_id integer primary key , effect_type integer , button_idx integer , use integer );"

.field public static final _TABLENAME:Ljava/lang/String; = "common_button_table"

.field public static final _TABLENAME_EFFECT:Ljava/lang/String; = "effect_button_table"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
