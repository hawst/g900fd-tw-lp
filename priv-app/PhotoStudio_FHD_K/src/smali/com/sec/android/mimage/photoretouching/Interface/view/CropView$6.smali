.class Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;
.super Ljava/lang/Object;
.source "CropView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->initButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    .line 260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 299
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 270
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->onButtonClicked(Landroid/view/View;)V
    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;Landroid/view/View;)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(Landroid/view/View;Z)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->resetZoom()V

    .line 273
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->pinchZoom()V

    .line 275
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v0

    const v1, 0x11201302

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->init(ILcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 278
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mIsLassoPressed:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 281
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->calculateInitialValues(Z)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->startAnimation()V

    .line 283
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    iput v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->i:I

    .line 284
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->calculateanimationintertval()V

    .line 285
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->drawRunner:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 292
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 293
    return-void

    .line 289
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;Z)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->invalidateViews()V

    goto :goto_0
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 266
    return-void
.end method
