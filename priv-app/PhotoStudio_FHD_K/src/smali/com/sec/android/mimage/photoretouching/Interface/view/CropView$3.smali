.class Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$3;
.super Ljava/lang/Object;
.source "CropView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isBottomMax()Z
    .locals 2

    .prologue
    .line 127
    const/4 v0, 0x0

    .line 128
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 129
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->isBottomMax()Z

    move-result v0

    .line 130
    :cond_0
    return v0
.end method

.method public isLeftMax()Z
    .locals 2

    .prologue
    .line 119
    const/4 v0, 0x0

    .line 120
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->isLeftMax()Z

    move-result v0

    .line 122
    :cond_0
    return v0
.end method

.method public isRightMax()Z
    .locals 2

    .prologue
    .line 111
    const/4 v0, 0x0

    .line 112
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->isRightMax()Z

    move-result v0

    .line 114
    :cond_0
    return v0
.end method

.method public isTopMax()Z
    .locals 2

    .prologue
    .line 103
    const/4 v0, 0x0

    .line 104
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->isTopMax()Z

    move-result v0

    .line 106
    :cond_0
    return v0
.end method
