.class Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$11;
.super Landroid/content/BroadcastReceiver;
.source "RotateView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->registerReceiverToActivity()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    .line 1039
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1044
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.TWIST_RIGHT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1047
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mOnFilp3dAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1048
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mOnFilp3dAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;

    move-result-object v0

    const v1, 0x11101102

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;->startAnimation(I)V

    .line 1051
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.TWIST_LEFT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1054
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mOnFilp3dAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1055
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$11;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mOnFilp3dAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;

    move-result-object v0

    const v1, 0x11101101

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$OnFilp3dAnimation;->startAnimation(I)V

    .line 1069
    :cond_1
    return-void
.end method
