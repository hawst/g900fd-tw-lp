.class public Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;
.super Landroid/widget/FrameLayout;
.source "ActivityLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;
    }
.end annotation


# instance fields
.field private mActivityLayoutTouchUpCallback:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mImageView:Landroid/widget/ImageView;

.field private mIsIntercept:Z

.field private mPosX:F

.field private mPosY:F

.field private mThumbnailHeight:I

.field private mThumbnailWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 130
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mContext:Landroid/content/Context;

    .line 131
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    .line 132
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mBitmap:Landroid/graphics/Bitmap;

    .line 134
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mIsIntercept:Z

    .line 137
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailWidth:I

    .line 138
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailHeight:I

    .line 140
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mActivityLayoutTouchUpCallback:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;

    .line 24
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mContext:Landroid/content/Context;

    .line 25
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->initLayout()V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 130
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mContext:Landroid/content/Context;

    .line 131
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    .line 132
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mBitmap:Landroid/graphics/Bitmap;

    .line 134
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mIsIntercept:Z

    .line 137
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailWidth:I

    .line 138
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailHeight:I

    .line 140
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mActivityLayoutTouchUpCallback:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;

    .line 29
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mContext:Landroid/content/Context;

    .line 30
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->initLayout()V

    .line 31
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)Z
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mIsIntercept:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;F)V
    .locals 0

    .prologue
    .line 135
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mPosX:F

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;F)V
    .locals 0

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mPosY:F

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)F
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mPosX:F

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailWidth:I

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)F
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mPosY:F

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailHeight:I

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;FF)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->setDisableIntercept(FF)V

    return-void
.end method

.method private initLayout()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 128
    return-void
.end method

.method private setDisableIntercept(FF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v1, 0x0

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mIsIntercept:Z

    .line 91
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->removeView(Landroid/view/View;)V

    .line 95
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mBitmap:Landroid/graphics/Bitmap;

    .line 99
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mActivityLayoutTouchUpCallback:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailWidth:I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailHeight:I

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;->touchUp(FFII)V

    .line 100
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 35
    const/4 v0, 0x0

    .line 36
    .local v0, "ret":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mPosX:F

    .line 37
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mPosY:F

    .line 38
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 66
    :cond_0
    :goto_0
    return v0

    .line 49
    :sswitch_0
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mIsIntercept:Z

    if-eqz v1, :cond_0

    .line 51
    const/4 v0, 0x1

    .line 52
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 56
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mPosX:F

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailWidth:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 57
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mPosY:F

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailHeight:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTranslationY(F)V

    goto :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1e110000 -> :sswitch_0
        0x2c000000 -> :sswitch_0
        0x31100000 -> :sswitch_0
    .end sparse-switch

    .line 52
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setEnableInterceptForThumbnail(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "viewTransformMatrix"    # Landroid/graphics/Matrix;
    .param p3, "callback"    # Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;

    .prologue
    .line 72
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mActivityLayoutTouchUpCallback:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mIsIntercept:Z

    .line 74
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mBitmap:Landroid/graphics/Bitmap;

    .line 76
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailWidth:I

    .line 77
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailHeight:I

    .line 78
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    .line 79
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->addView(Landroid/view/View;)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mPosX:F

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailWidth:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mPosY:F

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailHeight:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 85
    return-void
.end method
