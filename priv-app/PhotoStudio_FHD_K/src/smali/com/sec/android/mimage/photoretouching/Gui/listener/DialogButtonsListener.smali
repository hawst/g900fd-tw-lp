.class public Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;
.super Ljava/lang/Object;
.source "DialogButtonsListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;
    }
.end annotation


# instance fields
.field private mManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V
    .locals 1
    .param p1, "manager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p2, "touchInterface"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 71
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 20
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 21
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 22
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 27
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-nez v0, :cond_0

    .line 67
    :goto_0
    return v1

    .line 30
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 32
    :pswitch_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setPressed(Z)V

    .line 33
    invoke-virtual {p1, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 34
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->disableAnotherButtons(Landroid/view/View;)V

    goto :goto_0

    .line 37
    :pswitch_1
    invoke-static {p1, p2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isInButton(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 39
    invoke-virtual {p1, v1}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 43
    :cond_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 47
    :pswitch_2
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    .line 48
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->enableButtons()V

    goto :goto_0

    .line 51
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    invoke-virtual {p1, v2}, Landroid/view/View;->playSoundEffect(I)V

    .line 53
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setCurrentButton(Landroid/view/View;)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;->TouchFunction(Landroid/view/View;)V

    .line 57
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 58
    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 63
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->enableButtons()V

    goto :goto_0

    .line 60
    :cond_3
    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    goto :goto_1

    .line 30
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
