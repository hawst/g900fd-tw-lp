.class Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$15;
.super Ljava/lang/Object;
.source "StickerView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->init3DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$15;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    .line 993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 0
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 1025
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 997
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$15;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1008
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$15;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->saveToDB()V

    .line 1011
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$15;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1013
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$15;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->closeDrawer()V

    .line 1017
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$15;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->doDone()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    .line 1018
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$15;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x31000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 1019
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1031
    return-void
.end method
