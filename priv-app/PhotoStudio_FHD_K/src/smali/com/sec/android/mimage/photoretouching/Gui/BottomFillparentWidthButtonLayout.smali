.class public Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;
.super Landroid/widget/LinearLayout;
.source "BottomFillparentWidthButtonLayout.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDivider:Landroid/widget/LinearLayout;

.field private mLayout:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 99
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 100
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->mContext:Landroid/content/Context;

    .line 101
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 20
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->mContext:Landroid/content/Context;

    .line 22
    const v0, 0x7f030027

    invoke-static {p1, v0, p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 29
    return-void
.end method


# virtual methods
.method public init(II)V
    .locals 3
    .param p1, "buttonType"    # I
    .param p2, "iconId"    # I

    .prologue
    const/16 v2, 0x51

    const/4 v1, -0x1

    .line 33
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 34
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 35
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 36
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->setGravity(I)V

    .line 37
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->setId(I)V

    .line 38
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    .line 39
    return-void
.end method

.method public isFocused()Z
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->mLayout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 85
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->removeAllViewsInLayout()V

    .line 89
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->mContext:Landroid/content/Context;

    const v1, 0x7f030027

    invoke-static {v0, v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 96
    return-void
.end method

.method public removeDivider()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->mDivider:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 74
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 5
    .param p1, "enabled"    # Z

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const v3, 0x3f19999a    # 0.6f

    .line 43
    const v2, 0x7f090004

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 45
    .local v0, "icon":Landroid/widget/ImageView;
    const v2, 0x7f090005

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFillparentWidthButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 47
    .local v1, "tView":Landroid/widget/TextView;
    if-eqz p1, :cond_2

    .line 49
    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 53
    :cond_0
    if-eqz v1, :cond_1

    .line 55
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 69
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 70
    return-void

    .line 60
    :cond_2
    if-eqz v0, :cond_3

    .line 62
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 64
    :cond_3
    if-eqz v1, :cond_1

    .line 66
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0
.end method
