.class Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;
.super Ljava/lang/Object;
.source "ThreadPool.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/util/Future;
.implements Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/util/ThreadPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Worker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;",
        "Lcom/sec/android/mimage/photoretouching/util/Future",
        "<TT;>;",
        "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Worker"


# instance fields
.field private mCancelListener:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$CancelListener;

.field private volatile mIsCancelled:Z

.field private mIsDone:Z

.field private mJob:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mListener:Lcom/sec/android/mimage/photoretouching/util/FutureListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/mimage/photoretouching/util/FutureListener",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mMode:I

.field private mResult:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private mWaitOnResource:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/util/ThreadPool;Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;Lcom/sec/android/mimage/photoretouching/util/FutureListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job",
            "<TT;>;",
            "Lcom/sec/android/mimage/photoretouching/util/FutureListener",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 111
    .local p0, "this":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker<TT;>;"
    .local p2, "job":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job<TT;>;"
    .local p3, "listener":Lcom/sec/android/mimage/photoretouching/util/FutureListener;, "Lcom/sec/android/mimage/photoretouching/util/FutureListener<TT;>;"
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->this$0:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mJob:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;

    .line 113
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mListener:Lcom/sec/android/mimage/photoretouching/util/FutureListener;

    .line 114
    return-void
.end method

.method private acquireResource(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;)Z
    .locals 1
    .param p1, "counter"    # Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    .prologue
    .line 217
    .local p0, "this":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker<TT;>;"
    :goto_0
    monitor-enter p0

    .line 218
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mIsCancelled:Z

    if-eqz v0, :cond_0

    .line 219
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mWaitOnResource:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    .line 220
    monitor-exit p0

    const/4 v0, 0x0

    .line 243
    :goto_1
    return v0

    .line 222
    :cond_0
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mWaitOnResource:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    .line 217
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    monitor-enter p1

    .line 226
    :try_start_1
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;->value:I

    if-lez v0, :cond_1

    .line 227
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;->value:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p1, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;->value:I

    .line 228
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 239
    monitor-enter p0

    .line 240
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mWaitOnResource:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    .line 239
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 243
    const/4 v0, 0x1

    goto :goto_1

    .line 217
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 231
    :cond_1
    :try_start_4
    invoke-virtual {p1}, Ljava/lang/Object;->wait()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 225
    :goto_2
    :try_start_5
    monitor-exit p1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 239
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 232
    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method private modeToCounter(I)Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 206
    .local p0, "this":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker<TT;>;"
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->this$0:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;->mCpuCounter:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    .line 211
    :goto_0
    return-object v0

    .line 208
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->this$0:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;->mNetworkCounter:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    goto :goto_0

    .line 211
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private releaseResource(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;)V
    .locals 1
    .param p1, "counter"    # Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    .prologue
    .line 247
    .local p0, "this":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker<TT;>;"
    monitor-enter p1

    .line 248
    :try_start_0
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;->value:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;->value:I

    .line 249
    invoke-virtual {p1}, Ljava/lang/Object;->notifyAll()V

    .line 247
    monitor-exit p1

    .line 251
    return-void

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public declared-synchronized cancel()V
    .locals 2

    .prologue
    .line 142
    .local p0, "this":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker<TT;>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mIsCancelled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 152
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 143
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mIsCancelled:Z

    .line 144
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mWaitOnResource:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    if-eqz v0, :cond_2

    .line 145
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mWaitOnResource:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mWaitOnResource:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 145
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 149
    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mCancelListener:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$CancelListener;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mCancelListener:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$CancelListener;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$CancelListener;->onCancel()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 145
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public declared-synchronized get()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 163
    .local p0, "this":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker<TT;>;"
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mIsDone:Z

    if-eqz v1, :cond_0

    .line 171
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mResult:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 165
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 166
    :catch_0
    move-exception v0

    .line 167
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ingore exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogW(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 163
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 155
    .local p0, "this":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker<TT;>;"
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mIsCancelled:Z

    return v0
.end method

.method public declared-synchronized isDone()Z
    .locals 1

    .prologue
    .line 159
    .local p0, "this":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker<TT;>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mIsDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 4

    .prologue
    .local p0, "this":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker<TT;>;"
    const/4 v2, 0x1

    .line 118
    const/4 v1, 0x0

    .line 122
    .local v1, "result":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->setMode(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 124
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mJob:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;

    invoke-interface {v2, p0}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;->run(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 131
    .end local v1    # "result":Ljava/lang/Object;, "TT;"
    :cond_0
    :goto_0
    monitor-enter p0

    .line 132
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->setMode(I)Z

    .line 133
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mResult:Ljava/lang/Object;

    .line 134
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mIsDone:Z

    .line 135
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 131
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 137
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mListener:Lcom/sec/android/mimage/photoretouching/util/FutureListener;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mListener:Lcom/sec/android/mimage/photoretouching/util/FutureListener;

    invoke-interface {v2, p0}, Lcom/sec/android/mimage/photoretouching/util/FutureListener;->onFutureDone(Lcom/sec/android/mimage/photoretouching/util/Future;)V

    .line 138
    :cond_1
    return-void

    .line 125
    .restart local v1    # "result":Ljava/lang/Object;, "TT;"
    :catch_0
    move-exception v0

    .line 126
    .local v0, "ex":Ljava/lang/Throwable;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception in running a job"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogW(Ljava/lang/String;)V

    .line 127
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    .line 131
    .end local v0    # "ex":Ljava/lang/Throwable;
    .end local v1    # "result":Ljava/lang/Object;, "TT;"
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public declared-synchronized setCancelListener(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$CancelListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/mimage/photoretouching/util/ThreadPool$CancelListener;

    .prologue
    .line 181
    .local p0, "this":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker<TT;>;"
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mCancelListener:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$CancelListener;

    .line 182
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mIsCancelled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mCancelListener:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$CancelListener;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mCancelListener:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$CancelListener;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$CancelListener;->onCancel()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    :cond_0
    monitor-exit p0

    return-void

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setMode(I)Z
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .local p0, "this":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker<TT;>;"
    const/4 v1, 0x0

    .line 189
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mMode:I

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->modeToCounter(I)Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    move-result-object v0

    .line 190
    .local v0, "rc":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->releaseResource(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;)V

    .line 191
    :cond_0
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mMode:I

    .line 194
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->modeToCounter(I)Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    move-result-object v0

    .line 195
    if-eqz v0, :cond_2

    .line 196
    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->acquireResource(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 202
    :goto_0
    return v1

    .line 199
    :cond_1
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->mMode:I

    .line 202
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public waitDone()V
    .locals 0

    .prologue
    .line 175
    .local p0, "this":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;->get()Ljava/lang/Object;

    .line 176
    return-void
.end method
