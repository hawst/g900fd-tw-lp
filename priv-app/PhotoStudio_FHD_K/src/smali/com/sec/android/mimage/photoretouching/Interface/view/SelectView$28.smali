.class Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$28;
.super Ljava/lang/Object;
.source "SelectView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initSelection(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$28;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    .line 1194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isBottomMax()Z
    .locals 2

    .prologue
    .line 1222
    const/4 v0, 0x0

    .line 1223
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$28;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1224
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$28;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->isBottomMax()Z

    move-result v0

    .line 1225
    :cond_0
    return v0
.end method

.method public isLeftMax()Z
    .locals 2

    .prologue
    .line 1214
    const/4 v0, 0x0

    .line 1215
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$28;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1216
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$28;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->isLeftMax()Z

    move-result v0

    .line 1217
    :cond_0
    return v0
.end method

.method public isRightMax()Z
    .locals 2

    .prologue
    .line 1206
    const/4 v0, 0x0

    .line 1207
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$28;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1208
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$28;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->isRightMax()Z

    move-result v0

    .line 1209
    :cond_0
    return v0
.end method

.method public isTopMax()Z
    .locals 2

    .prologue
    .line 1198
    const/4 v0, 0x0

    .line 1199
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$28;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1200
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$28;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->isTopMax()Z

    move-result v0

    .line 1201
    :cond_0
    return v0
.end method
