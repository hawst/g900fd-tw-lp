.class Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;
.super Ljava/lang/Object;
.source "DecorationMenuLayoutManager.java"

# interfaces
.implements Landroid/widget/TabHost$OnTabChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTabChanged(Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 230
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 251
    :goto_0
    :sswitch_0
    const-string v0, "STICKER_RECENTLY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setImageContext(I)V

    .line 289
    :cond_0
    :goto_1
    const-string v0, "JW tabChanged end"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 290
    return-void

    .line 232
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)Landroid/widget/TabHost;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 235
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)Landroid/widget/TabHost;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    add-int/lit8 v1, v1, -0xe

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 238
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)Landroid/widget/TabHost;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    add-int/lit8 v1, v1, -0x9

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 244
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mDecorationMenuViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)Landroid/widget/TabHost;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    add-int/lit8 v1, v1, -0x5

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 253
    :cond_1
    const-string v0, "STICKER_TYPE1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 254
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setImageContext(I)V

    goto :goto_1

    .line 255
    :cond_2
    const-string v0, "STICKER_TYPE2"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 256
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setImageContext(I)V

    goto :goto_1

    .line 257
    :cond_3
    const-string v0, "STICKER_TYPE3"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 258
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setImageContext(I)V

    goto/16 :goto_1

    .line 260
    :cond_4
    const-string v0, "FRAME_RECENTLY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 261
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setThumbnailContext(I)V

    goto/16 :goto_1

    .line 262
    :cond_5
    const-string v0, "FRAME_TYPE1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 263
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setThumbnailContext(I)V

    goto/16 :goto_1

    .line 264
    :cond_6
    const-string v0, "FRAME_TYPE2"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 265
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setThumbnailContext(I)V

    goto/16 :goto_1

    .line 266
    :cond_7
    const-string v0, "FRAME_TYPE3"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 267
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setThumbnailContext(I)V

    goto/16 :goto_1

    .line 269
    :cond_8
    const-string v0, "LABEL_RECENTLY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 270
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setImageContext(I)V

    goto/16 :goto_1

    .line 271
    :cond_9
    const-string v0, "LABEL_TYPE1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 272
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setImageContext(I)V

    goto/16 :goto_1

    .line 273
    :cond_a
    const-string v0, "LABEL_TYPE2"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 274
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setImageContext(I)V

    goto/16 :goto_1

    .line 276
    :cond_b
    const-string v0, "WATERMARK_RECENTLY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 277
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setImageContext(I)V

    goto/16 :goto_1

    .line 278
    :cond_c
    const-string v0, "WATERMARK_TYPE1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 279
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setImageContext(I)V

    goto/16 :goto_1

    .line 281
    :cond_d
    const-string v0, "STAMP_RECENTLY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 282
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setThumbnailContext(I)V

    goto/16 :goto_1

    .line 283
    :cond_e
    const-string v0, "STAMP_TYPE1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 284
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setThumbnailContext(I)V

    goto/16 :goto_1

    .line 285
    :cond_f
    const-string v0, "STAMP_TYPE2"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setThumbnailContext(I)V

    goto/16 :goto_1

    .line 230
    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_1
        0x31200000 -> :sswitch_4
        0x31300000 -> :sswitch_3
        0x31400000 -> :sswitch_0
        0x31500000 -> :sswitch_2
    .end sparse-switch
.end method
