.class public final Lcom/sec/android/mimage/photoretouching/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final COLLAGE_BORDER_POPUP_ROUNDNESS_height:I = 0x7f050012

.field public static final COLLAGE_BORDER_POPUP_ROUNDNESS_text_left_margin:I = 0x7f050014

.field public static final COLLAGE_BORDER_POPUP_ROUNDNESS_text_top_margin:I = 0x7f050013

.field public static final COLLAGE_BORDER_POPUP_SPACING_text_top_margin:I = 0x7f050015

.field public static final COLLAGE_BORDER_POPUP_bg_height:I = 0x7f05000a

.field public static final COLLAGE_BORDER_POPUP_height:I = 0x7f050009

.field public static final COLLAGE_BORDER_POPUP_icon_height:I = 0x7f05000c

.field public static final COLLAGE_BORDER_POPUP_icon_left_margin:I = 0x7f05000d

.field public static final COLLAGE_BORDER_POPUP_icon_top_margin:I = 0x7f05000e

.field public static final COLLAGE_BORDER_POPUP_icon_width:I = 0x7f05000b

.field public static final COLLAGE_BORDER_POPUP_seekbar_left_margin:I = 0x7f050011

.field public static final COLLAGE_BORDER_POPUP_seekbar_width:I = 0x7f050010

.field public static final COLLAGE_BORDER_POPUP_text_height:I = 0x7f05000f

.field public static final COLLAGE_BORDER_POPUP_width:I = 0x7f050008

.field public static final COLLAGE_VIEW_empty_image_add_image_margin_left:I = 0x7f050007

.field public static final COLLAGE_VIEW_empty_image_add_image_size:I = 0x7f050006

.field public static final COLLAGE_VIEW_empty_image_text_height:I = 0x7f050004

.field public static final COLLAGE_VIEW_empty_image_text_line_spacing:I = 0x7f050005

.field public static final COLLAGE_VIEW_empty_image_text_size_maximum:I = 0x7f050000

.field public static final COLLAGE_VIEW_empty_image_text_size_minimum:I = 0x7f050001

.field public static final COLLAGE_VIEW_empty_image_text_size_minimum_two_line:I = 0x7f050002

.field public static final COLLAGE_VIEW_empty_image_text_width:I = 0x7f050003

.field public static final COLLAGE_VIEW_stamp_style_max_height:I = 0x7f050021

.field public static final COLLAGE_VIEW_stamp_style_max_width:I = 0x7f050020

.field public static final COLLAGE_VIEW_stamp_style_min_height:I = 0x7f05001f

.field public static final COLLAGE_VIEW_stamp_style_min_width:I = 0x7f05001e

.field public static final actionbar_button_text_size:I = 0x7f0502ea

.field public static final actionbar_divider_height:I = 0x7f0502f4

.field public static final actionbar_home_back_height:I = 0x7f0502f5

.field public static final actionbar_text_size:I = 0x7f0502f2

.field public static final actionbar_title_text_size:I = 0x7f0502f3

.field public static final app_defaultsize_h:I = 0x7f050023

.field public static final app_defaultsize_w:I = 0x7f050022

.field public static final app_minimumsize_h:I = 0x7f050025

.field public static final app_minimumsize_w:I = 0x7f050024

.field public static final base_thumbnail_size:I = 0x7f050039

.field public static final bottom_btn_3depth_height:I = 0x7f050249

.field public static final bottom_btn_3depth_margin_bottom:I = 0x7f050248

.field public static final bottom_btn_side_margin_1_land:I = 0x7f05023e

.field public static final bottom_btn_side_margin_1_port:I = 0x7f05023a

.field public static final bottom_btn_side_margin_2_land:I = 0x7f05023f

.field public static final bottom_btn_side_margin_2_port:I = 0x7f05023b

.field public static final bottom_btn_side_margin_3_land:I = 0x7f050240

.field public static final bottom_btn_side_margin_3_port:I = 0x7f05023c

.field public static final bottom_btn_side_margin_4_land:I = 0x7f050241

.field public static final bottom_btn_side_margin_4_port:I = 0x7f05023d

.field public static final bottom_collage_sub_layout_margin_bottom:I = 0x7f0502f9

.field public static final bottom_pen_btn_3depth_height:I = 0x7f05024a

.field public static final bottom_sub_image_layout_margin_bottom:I = 0x7f0502f7

.field public static final bottom_sub_thumbnail_layout_margin_bottom:I = 0x7f0502f8

.field public static final collage_border_popup_arrow_leftmargin_land:I = 0x7f050016

.field public static final collage_border_popup_arrow_leftmargin_port:I = 0x7f050017

.field public static final collage_colorpicker_chip_picker_width:I = 0x7f050019

.field public static final collage_colorpicker_margin:I = 0x7f050018

.field public static final collage_middle_button_bottom_margin:I = 0x7f0502c1

.field public static final collage_middle_button_bottom_padding:I = 0x7f0502c2

.field public static final collage_modifypopup_height:I = 0x7f05001d

.field public static final collage_modifypopup_width:I = 0x7f05001c

.field public static final collage_select_line_width:I = 0x7f05001b

.field public static final collage_submenu_arrow_width:I = 0x7f05001a

.field public static final color_picker_topmargin:I = 0x7f05029a

.field public static final decoration_scroll_offset:I = 0x7f05021f

.field public static final decoration_tab_height:I = 0x7f05021d

.field public static final decoration_tab_land_height:I = 0x7f05021e

.field public static final device_width:I = 0x7f05029b

.field public static final drawer_handle_width:I = 0x7f050038

.field public static final editor_bottom_menu_gradation_layout_height:I = 0x7f05022d

.field public static final editor_menu_button_text_size:I = 0x7f0502ed

.field public static final editor_top_menu_gradation_layout_height:I = 0x7f05022c

.field public static final effect_manager_actionbar_divider_height:I = 0x7f050289

.field public static final effect_manager_actionbar_divider_width:I = 0x7f050288

.field public static final effect_manager_actionbar_layout_height_l:I = 0x7f050294

.field public static final effect_manager_actionbar_layout_height_p:I = 0x7f05028f

.field public static final effect_manager_main_layout_height_l:I = 0x7f050295

.field public static final effect_manager_main_layout_height_p:I = 0x7f050290

.field public static final effect_manager_thumbnail_check_box_height:I = 0x7f05028c

.field public static final effect_manager_thumbnail_check_box_padding_left:I = 0x7f05028d

.field public static final effect_manager_thumbnail_check_box_padding_top:I = 0x7f05028e

.field public static final effect_manager_thumbnail_check_box_width:I = 0x7f05028b

.field public static final effect_manager_thumbnail_framebar_thickness:I = 0x7f05028a

.field public static final effect_manager_thumbnail_layout_height_l:I = 0x7f050297

.field public static final effect_manager_thumbnail_layout_height_p:I = 0x7f050292

.field public static final effect_manager_thumbnail_layout_width_l:I = 0x7f050296

.field public static final effect_manager_thumbnail_layout_width_p:I = 0x7f050291

.field public static final effect_manager_thumbnail_layout_width_withBar_l:I = 0x7f050298

.field public static final effect_manager_thumbnail_layout_width_withBar_p:I = 0x7f050293

.field public static final effect_manager_thumbnial_focus_image_height:I = 0x7f0502c5

.field public static final filter_frames_type_text_size:I = 0x7f0502f1

.field public static final frame_colorpicker_left_margin:I = 0x7f050263

.field public static final frame_colorpicker_top_margin:I = 0x7f050264

.field public static final frame_item_extra_offset_scroll_down:I = 0x7f050228

.field public static final frame_item_offset_scroll_down:I = 0x7f050227

.field public static final frame_item_offset_scroll_up:I = 0x7f050226

.field public static final frame_style02_bottom_margin:I = 0x7f050262

.field public static final frame_style02_left_margin:I = 0x7f05025f

.field public static final frame_style02_right_margin:I = 0x7f050260

.field public static final frame_style02_top_margin:I = 0x7f050261

.field public static final grid_item_label_side_margin_land:I = 0x7f050037

.field public static final grid_item_label_side_margin_port:I = 0x7f050036

.field public static final grid_item_margin:I = 0x7f050035

.field public static final grid_label_item_horizontal_spacing_land:I = 0x7f050033

.field public static final grid_label_item_horizontal_spacing_port:I = 0x7f050031

.field public static final grid_label_item_vertical_spacing_land:I = 0x7f050034

.field public static final grid_label_item_vertical_spacing_port:I = 0x7f050032

.field public static final grid_stamp_item_land_spacing:I = 0x7f05002e

.field public static final grid_stamp_item_left_margin_land:I = 0x7f05002f

.field public static final grid_stamp_item_right_margin_land:I = 0x7f050030

.field public static final grid_stamp_item_spacing:I = 0x7f05002d

.field public static final grid_sticker_item_horizontal_spacing_land:I = 0x7f050029

.field public static final grid_sticker_item_horizontal_spacing_port:I = 0x7f050027

.field public static final grid_sticker_item_side_margin_land:I = 0x7f05002c

.field public static final grid_sticker_item_side_margin_port:I = 0x7f05002b

.field public static final grid_sticker_item_vertical_spacing_land:I = 0x7f05002a

.field public static final grid_sticker_item_vertical_spacing_port:I = 0x7f050028

.field public static final help_popup_left_padding_port:I = 0x7f0502a0

.field public static final help_popup_text_left_margin_land:I = 0x7f050280

.field public static final help_popup_text_left_margin_port:I = 0x7f05027e

.field public static final help_popup_text_level_up_down_left_margin_land:I = 0x7f050285

.field public static final help_popup_text_level_up_down_left_margin_port:I = 0x7f050284

.field public static final help_popup_text_level_up_down_right_margin_land:I = 0x7f050287

.field public static final help_popup_text_level_up_down_right_margin_port:I = 0x7f050286

.field public static final help_popup_text_level_up_down_width_land:I = 0x7f050283

.field public static final help_popup_text_level_up_down_width_port:I = 0x7f050282

.field public static final help_popup_text_right_margin_land:I = 0x7f050281

.field public static final help_popup_text_right_margin_port:I = 0x7f05027f

.field public static final help_popup_text_width_land:I = 0x7f05027d

.field public static final help_popup_text_width_port:I = 0x7f05027c

.field public static final input_dialog_height_land:I = 0x7f050273

.field public static final input_dialog_height_port:I = 0x7f050271

.field public static final input_dialog_left_margin_land:I = 0x7f050276

.field public static final input_dialog_left_margin_port:I = 0x7f050274

.field public static final input_dialog_top_margin_land:I = 0x7f050277

.field public static final input_dialog_top_margin_port:I = 0x7f050275

.field public static final input_dialog_width_land:I = 0x7f050272

.field public static final input_dialog_width_port:I = 0x7f050270

.field public static final j_bottom_frame_viewpager_end_margin_land:I = 0x7f050254

.field public static final j_bottom_frame_viewpager_height_land:I = 0x7f050252

.field public static final j_bottom_frame_viewpager_height_port:I = 0x7f050250

.field public static final j_bottom_frame_viewpager_icon_margin_land:I = 0x7f050253

.field public static final j_bottom_frame_viewpager_icon_margin_port:I = 0x7f05024e

.field public static final j_bottom_frame_viewpager_width_land:I = 0x7f050251

.field public static final j_bottom_frame_viewpager_width_port:I = 0x7f05024f

.field public static final j_bottom_submenu_frame_flipper_indicator_bar_height:I = 0x7f05024d

.field public static final j_tray_thumnail_size:I = 0x7f050026

.field public static final label_item_extra_offset_scroll_down:I = 0x7f050225

.field public static final label_item_offset_scroll_down:I = 0x7f050224

.field public static final label_item_offset_scroll_up:I = 0x7f050223

.field public static final label_text_size:I = 0x7f05021c

.field public static final launcher_button_text_size:I = 0x7f0502ee

.field public static final margin:I = 0x7f050278

.field public static final margin_check:I = 0x7f050279

.field public static final middle_auto_button_top_margin:I = 0x7f05022f

.field public static final middle_button_auto_height:I = 0x7f050233

.field public static final middle_button_auto_text_size:I = 0x7f050234

.field public static final middle_button_auto_width:I = 0x7f050232

.field public static final middle_button_left_right_margin:I = 0x7f05022e

.field public static final middle_button_left_right_padding:I = 0x7f050231

.field public static final middle_button_selection_height:I = 0x7f050238

.field public static final middle_button_selection_text_size:I = 0x7f050239

.field public static final middle_button_selection_width:I = 0x7f050236

.field public static final middle_button_shuffle_height:I = 0x7f050235

.field public static final middle_button_unselection_width:I = 0x7f050237

.field public static final middle_select_button_top_margin:I = 0x7f050230

.field public static final multiwindow_app_default_size_height:I = 0x7f05029d

.field public static final multiwindow_app_default_size_width:I = 0x7f05029c

.field public static final multiwindow_app_minimum_size_height:I = 0x7f05029f

.field public static final multiwindow_app_minimum_size_width:I = 0x7f05029e

.field public static final noti_dialog_land:I = 0x7f050266

.field public static final noti_dialog_land_text:I = 0x7f05027b

.field public static final noti_dialog_port:I = 0x7f050265

.field public static final noti_dialog_port_text:I = 0x7f05027a

.field public static final pen_setting_padding:I = 0x7f050299

.field public static final popup_button_text_size:I = 0x7f0502ec

.field public static final popup_help_collage_view_arrow2_image_left_margin:I = 0x7f0502c7

.field public static final popup_help_collage_view_arrow_image_left_margin:I = 0x7f0502c6

.field public static final popup_help_collage_view_left_margin:I = 0x7f0502be

.field public static final popup_help_collage_view_middle_arrow_image_top_margin:I = 0x7f0502e8

.field public static final popup_help_collage_view_middle_button_image_left_margin:I = 0x7f0502c3

.field public static final popup_help_collage_view_middle_button_image_top_margin:I = 0x7f0502c4

.field public static final popup_help_collage_view_middle_text_top_margin:I = 0x7f0502e9

.field public static final popup_help_collage_view_text_left_margin:I = 0x7f0502c8

.field public static final popup_help_crop_view_left_margin_free:I = 0x7f0502a2

.field public static final popup_help_crop_view_left_margin_free_image:I = 0x7f0502b4

.field public static final popup_help_crop_view_left_margin_lasso:I = 0x7f0502a5

.field public static final popup_help_crop_view_left_margin_lasso_image:I = 0x7f0502b7

.field public static final popup_help_crop_view_left_margin_ninebysixteen:I = 0x7f0502a4

.field public static final popup_help_crop_view_left_margin_ninebysixteen_image:I = 0x7f0502b6

.field public static final popup_help_crop_view_left_margin_onebyone:I = 0x7f0502a3

.field public static final popup_help_crop_view_left_margin_onebyone_image:I = 0x7f0502b5

.field public static final popup_help_crop_view_left_margin_threebyfour:I = 0x7f0502a1

.field public static final popup_help_crop_view_left_margin_threebyfour_image:I = 0x7f0502b3

.field public static final popup_help_crop_view_top_margin:I = 0x7f0502c9

.field public static final popup_help_crop_view_top_margin_free:I = 0x7f0502cb

.field public static final popup_help_crop_view_top_margin_free_image:I = 0x7f0502da

.field public static final popup_help_crop_view_top_margin_lasso:I = 0x7f0502ce

.field public static final popup_help_crop_view_top_margin_lasso_image:I = 0x7f0502de

.field public static final popup_help_crop_view_top_margin_ninebysixteen:I = 0x7f0502cd

.field public static final popup_help_crop_view_top_margin_ninebysixteen_image:I = 0x7f0502dd

.field public static final popup_help_crop_view_top_margin_onebyone:I = 0x7f0502cc

.field public static final popup_help_crop_view_top_margin_onebyone_image:I = 0x7f0502db

.field public static final popup_help_crop_view_top_margin_threebyfour:I = 0x7f0502ca

.field public static final popup_help_crop_view_top_margin_threebyfour_image:I = 0x7f0502dc

.field public static final popup_help_normal_view_left_margin:I = 0x7f0502a6

.field public static final popup_help_normal_view_left_margin_icon:I = 0x7f0502a7

.field public static final popup_help_normal_view_left_margin_top_icon:I = 0x7f0502a8

.field public static final popup_help_rotate_view_left_margin_flip_horizontally:I = 0x7f0502ab

.field public static final popup_help_rotate_view_left_margin_flip_horizontally_image:I = 0x7f0502b0

.field public static final popup_help_rotate_view_left_margin_flip_vertically:I = 0x7f0502ac

.field public static final popup_help_rotate_view_left_margin_flip_vertically_image:I = 0x7f0502b1

.field public static final popup_help_rotate_view_left_margin_mirror:I = 0x7f0502ad

.field public static final popup_help_rotate_view_left_margin_mirror_image:I = 0x7f0502b2

.field public static final popup_help_rotate_view_left_margin_rotate_left:I = 0x7f0502a9

.field public static final popup_help_rotate_view_left_margin_rotate_left_image:I = 0x7f0502ae

.field public static final popup_help_rotate_view_left_margin_rotate_right:I = 0x7f0502aa

.field public static final popup_help_rotate_view_left_margin_rotate_right_image:I = 0x7f0502af

.field public static final popup_help_rotate_view_top_margin:I = 0x7f0502cf

.field public static final popup_help_rotate_view_top_margin_flip_horizontally:I = 0x7f0502d2

.field public static final popup_help_rotate_view_top_margin_flip_horizontally_image:I = 0x7f0502d7

.field public static final popup_help_rotate_view_top_margin_flip_mirror_image:I = 0x7f0502d9

.field public static final popup_help_rotate_view_top_margin_flip_vertically:I = 0x7f0502d3

.field public static final popup_help_rotate_view_top_margin_flip_vertically_image:I = 0x7f0502d8

.field public static final popup_help_rotate_view_top_margin_mirror:I = 0x7f0502d4

.field public static final popup_help_rotate_view_top_margin_rotate_left:I = 0x7f0502d0

.field public static final popup_help_rotate_view_top_margin_rotate_left_image:I = 0x7f0502d5

.field public static final popup_help_rotate_view_top_margin_rotate_right:I = 0x7f0502d1

.field public static final popup_help_rotate_view_top_margin_rotate_right_image:I = 0x7f0502d6

.field public static final popup_help_selection_view_height_round_selection_image:I = 0x7f0502e7

.field public static final popup_help_selection_view_height_square_selection_image:I = 0x7f0502e6

.field public static final popup_help_selection_view_height_studio_lasso_image:I = 0x7f0502e5

.field public static final popup_help_selection_view_left_margin_round_selection:I = 0x7f0502ba

.field public static final popup_help_selection_view_left_margin_round_selection_image:I = 0x7f0502bd

.field public static final popup_help_selection_view_left_margin_square_selection:I = 0x7f0502b9

.field public static final popup_help_selection_view_left_margin_square_selection_image:I = 0x7f0502bc

.field public static final popup_help_selection_view_left_margin_studio_lasso:I = 0x7f0502b8

.field public static final popup_help_selection_view_left_margin_studio_lasso_image:I = 0x7f0502bb

.field public static final popup_help_selection_view_top_margin_round_selection:I = 0x7f0502e1

.field public static final popup_help_selection_view_top_margin_round_selection_image:I = 0x7f0502e4

.field public static final popup_help_selection_view_top_margin_square_selection:I = 0x7f0502e0

.field public static final popup_help_selection_view_top_margin_square_selection_image:I = 0x7f0502e3

.field public static final popup_help_selection_view_top_margin_studio_lasso:I = 0x7f0502df

.field public static final popup_help_selection_view_top_margin_studio_lasso_image:I = 0x7f0502e2

.field public static final popup_menu_button_text_size:I = 0x7f0502f0

.field public static final popup_title_size:I = 0x7f0502eb

.field public static final popup_title_text_size:I = 0x7f0502ef

.field public static final progressbar_height:I = 0x7f050257

.field public static final progressbar_height2:I = 0x7f050258

.field public static final progressbar_left_text_margin:I = 0x7f050259

.field public static final progressbar_textview_width_color:I = 0x7f05025d

.field public static final progressbar_textview_width_resize:I = 0x7f05025c

.field public static final progressbar_top_magine:I = 0x7f050255

.field public static final progressbar_top_text_margin:I = 0x7f05025a

.field public static final progressbar_top_text_size:I = 0x7f05025b

.field public static final progressbar_width:I = 0x7f050256

.field public static final question_dialog_land:I = 0x7f050268

.field public static final question_dialog_land_1:I = 0x7f05026a

.field public static final question_dialog_minus:I = 0x7f05026b

.field public static final question_dialog_minus_1:I = 0x7f05026c

.field public static final question_dialog_port:I = 0x7f050267

.field public static final question_dialog_port_1:I = 0x7f050269

.field public static final question_dialog_tv_width_land:I = 0x7f05026e

.field public static final question_dialog_tv_width_port:I = 0x7f05026d

.field public static final question_left_right_margin:I = 0x7f05026f

.field public static final seekbar_step_text_size:I = 0x7f0502f6

.field public static final spen_bottom_padding:I = 0x7f0502bf

.field public static final spen_eraser_bottom_padding:I = 0x7f0502c0

.field public static final stamp_background_default_padding:I = 0x7f05003d

.field public static final stamp_background_default_size:I = 0x7f05003c

.field public static final stamp_background_except_margin_size:I = 0x7f05003b

.field public static final stamp_extra_margin:I = 0x7f05003f

.field public static final stamp_item_extra_offset_scroll_down:I = 0x7f05022b

.field public static final stamp_item_offset_scroll_down:I = 0x7f05022a

.field public static final stamp_item_offset_scroll_up:I = 0x7f050229

.field public static final stamp_layer_padding:I = 0x7f05003e

.field public static final stamp_minimum_text_size:I = 0x7f050040

.field public static final stamp_style10_1st_height:I = 0x7f0500af

.field public static final stamp_style10_1st_left_margin:I = 0x7f0500b8

.field public static final stamp_style10_1st_text_size:I = 0x7f0500b4

.field public static final stamp_style10_1st_top_margin:I = 0x7f0500b7

.field public static final stamp_style10_1st_width:I = 0x7f0500ae

.field public static final stamp_style10_2nd_height:I = 0x7f0500b1

.field public static final stamp_style10_2nd_left_margin:I = 0x7f0500ba

.field public static final stamp_style10_2nd_text_size:I = 0x7f0500b5

.field public static final stamp_style10_2nd_top_margin:I = 0x7f0500b9

.field public static final stamp_style10_2nd_width:I = 0x7f0500b0

.field public static final stamp_style10_3rd_height:I = 0x7f0500b3

.field public static final stamp_style10_3rd_left_margin:I = 0x7f0500bc

.field public static final stamp_style10_3rd_text_size:I = 0x7f0500b6

.field public static final stamp_style10_3rd_top_margin:I = 0x7f0500bb

.field public static final stamp_style10_3rd_width:I = 0x7f0500b2

.field public static final stamp_style11_1st_height:I = 0x7f0500be

.field public static final stamp_style11_1st_left_margin:I = 0x7f0500c7

.field public static final stamp_style11_1st_text_size:I = 0x7f0500c3

.field public static final stamp_style11_1st_top_margin:I = 0x7f0500c6

.field public static final stamp_style11_1st_width:I = 0x7f0500bd

.field public static final stamp_style11_2nd_height:I = 0x7f0500c0

.field public static final stamp_style11_2nd_left_margin:I = 0x7f0500c9

.field public static final stamp_style11_2nd_text_size:I = 0x7f0500c4

.field public static final stamp_style11_2nd_top_margin:I = 0x7f0500c8

.field public static final stamp_style11_2nd_width:I = 0x7f0500bf

.field public static final stamp_style11_3rd_height:I = 0x7f0500c2

.field public static final stamp_style11_3rd_left_margin:I = 0x7f0500cb

.field public static final stamp_style11_3rd_text_size:I = 0x7f0500c5

.field public static final stamp_style11_3rd_top_margin:I = 0x7f0500ca

.field public static final stamp_style11_3rd_width:I = 0x7f0500c1

.field public static final stamp_style12_1st_height:I = 0x7f0500cd

.field public static final stamp_style12_1st_left_margin:I = 0x7f0500d6

.field public static final stamp_style12_1st_text_size:I = 0x7f0500d2

.field public static final stamp_style12_1st_top_margin:I = 0x7f0500d5

.field public static final stamp_style12_1st_width:I = 0x7f0500cc

.field public static final stamp_style12_2nd_height:I = 0x7f0500cf

.field public static final stamp_style12_2nd_left_margin:I = 0x7f0500d8

.field public static final stamp_style12_2nd_text_size:I = 0x7f0500d3

.field public static final stamp_style12_2nd_top_margin:I = 0x7f0500d7

.field public static final stamp_style12_2nd_width:I = 0x7f0500ce

.field public static final stamp_style12_3rd_height:I = 0x7f0500d1

.field public static final stamp_style12_3rd_left_margin:I = 0x7f0500da

.field public static final stamp_style12_3rd_text_size:I = 0x7f0500d4

.field public static final stamp_style12_3rd_top_margin:I = 0x7f0500d9

.field public static final stamp_style12_3rd_width:I = 0x7f0500d0

.field public static final stamp_style13_1st_height:I = 0x7f0500dc

.field public static final stamp_style13_1st_left_margin:I = 0x7f0500e4

.field public static final stamp_style13_1st_text_size:I = 0x7f0500e0

.field public static final stamp_style13_1st_top_margin:I = 0x7f0500e5

.field public static final stamp_style13_1st_width:I = 0x7f0500db

.field public static final stamp_style13_2nd_height:I = 0x7f0500dd

.field public static final stamp_style13_2nd_text_size:I = 0x7f0500e1

.field public static final stamp_style13_2nd_top_margin:I = 0x7f0500e6

.field public static final stamp_style13_3rd_height:I = 0x7f0500de

.field public static final stamp_style13_3rd_text_size:I = 0x7f0500e2

.field public static final stamp_style13_3rd_top_margin:I = 0x7f0500e7

.field public static final stamp_style13_4th_height:I = 0x7f0500df

.field public static final stamp_style13_4th_text_size:I = 0x7f0500e3

.field public static final stamp_style13_4th_top_margin:I = 0x7f0500e8

.field public static final stamp_style13_noinfo_left_margin:I = 0x7f0500ea

.field public static final stamp_style13_noinfo_top_margin:I = 0x7f0500e9

.field public static final stamp_style14_1st_height:I = 0x7f0500ec

.field public static final stamp_style14_1st_text_size:I = 0x7f0500f1

.field public static final stamp_style14_1st_top_margin:I = 0x7f0500f5

.field public static final stamp_style14_1st_width:I = 0x7f0500eb

.field public static final stamp_style14_2nd_height:I = 0x7f0500ee

.field public static final stamp_style14_2nd_text_size:I = 0x7f0500f2

.field public static final stamp_style14_2nd_top_margin:I = 0x7f0500f6

.field public static final stamp_style14_2nd_width:I = 0x7f0500ed

.field public static final stamp_style14_3rd_height:I = 0x7f0500ef

.field public static final stamp_style14_3rd_text_size:I = 0x7f0500f3

.field public static final stamp_style14_3rd_top_margin:I = 0x7f0500f7

.field public static final stamp_style14_4th_height:I = 0x7f0500f0

.field public static final stamp_style14_4th_text_size:I = 0x7f0500f4

.field public static final stamp_style14_4th_top_margin:I = 0x7f0500f8

.field public static final stamp_style14_noinfo_left_margin:I = 0x7f0500fa

.field public static final stamp_style14_noinfo_top_margin:I = 0x7f0500f9

.field public static final stamp_style15_1st_height:I = 0x7f0500fc

.field public static final stamp_style15_1st_left_margin:I = 0x7f050104

.field public static final stamp_style15_1st_text_size:I = 0x7f050100

.field public static final stamp_style15_1st_top_margin:I = 0x7f050105

.field public static final stamp_style15_1st_width:I = 0x7f0500fb

.field public static final stamp_style15_2nd_height:I = 0x7f0500fd

.field public static final stamp_style15_2nd_text_size:I = 0x7f050101

.field public static final stamp_style15_2nd_top_margin:I = 0x7f050106

.field public static final stamp_style15_3rd_height:I = 0x7f0500fe

.field public static final stamp_style15_3rd_text_size:I = 0x7f050102

.field public static final stamp_style15_3rd_top_margin:I = 0x7f050107

.field public static final stamp_style15_4th_height:I = 0x7f0500ff

.field public static final stamp_style15_4th_text_size:I = 0x7f050103

.field public static final stamp_style15_4th_top_margin:I = 0x7f050108

.field public static final stamp_style15_noinfo_left_margin:I = 0x7f05010a

.field public static final stamp_style15_noinfo_top_margin:I = 0x7f050109

.field public static final stamp_style16_1st_height:I = 0x7f05010c

.field public static final stamp_style16_1st_left_margin:I = 0x7f050114

.field public static final stamp_style16_1st_text_size:I = 0x7f050110

.field public static final stamp_style16_1st_top_margin:I = 0x7f050115

.field public static final stamp_style16_1st_width:I = 0x7f05010b

.field public static final stamp_style16_2nd_height:I = 0x7f05010d

.field public static final stamp_style16_2nd_text_size:I = 0x7f050111

.field public static final stamp_style16_2nd_top_margin:I = 0x7f050116

.field public static final stamp_style16_3rd_height:I = 0x7f05010e

.field public static final stamp_style16_3rd_text_size:I = 0x7f050112

.field public static final stamp_style16_3rd_top_margin:I = 0x7f050117

.field public static final stamp_style16_4th_height:I = 0x7f05010f

.field public static final stamp_style16_4th_text_size:I = 0x7f050113

.field public static final stamp_style16_4th_top_margin:I = 0x7f050118

.field public static final stamp_style16_noinfo_left_margin:I = 0x7f05011a

.field public static final stamp_style16_noinfo_top_margin:I = 0x7f050119

.field public static final stamp_style17_1st_height:I = 0x7f05011c

.field public static final stamp_style17_1st_left_margin:I = 0x7f050128

.field public static final stamp_style17_1st_text_size:I = 0x7f050123

.field public static final stamp_style17_1st_top_margin:I = 0x7f050129

.field public static final stamp_style17_1st_width:I = 0x7f05011b

.field public static final stamp_style17_2nd_height:I = 0x7f05011e

.field public static final stamp_style17_2nd_text_size:I = 0x7f050124

.field public static final stamp_style17_2nd_top_margin:I = 0x7f05012a

.field public static final stamp_style17_2nd_width:I = 0x7f05011d

.field public static final stamp_style17_3rd_height:I = 0x7f050120

.field public static final stamp_style17_3rd_left_margin:I = 0x7f05012b

.field public static final stamp_style17_3rd_text_size:I = 0x7f050125

.field public static final stamp_style17_3rd_top_margin:I = 0x7f05012c

.field public static final stamp_style17_3rd_width:I = 0x7f05011f

.field public static final stamp_style17_4th_height:I = 0x7f050121

.field public static final stamp_style17_4th_text_size:I = 0x7f050126

.field public static final stamp_style17_4th_top_margin:I = 0x7f05012d

.field public static final stamp_style17_5th_height:I = 0x7f050122

.field public static final stamp_style17_5th_text_size:I = 0x7f050127

.field public static final stamp_style17_5th_top_margin:I = 0x7f05012e

.field public static final stamp_style17_noinfo_left_margin:I = 0x7f050130

.field public static final stamp_style17_noinfo_top_margin:I = 0x7f05012f

.field public static final stamp_style18_1st_height:I = 0x7f050132

.field public static final stamp_style18_1st_left_margin:I = 0x7f05013a

.field public static final stamp_style18_1st_text_size:I = 0x7f050137

.field public static final stamp_style18_1st_top_margin:I = 0x7f05013b

.field public static final stamp_style18_1st_width:I = 0x7f050131

.field public static final stamp_style18_2nd_height:I = 0x7f050134

.field public static final stamp_style18_2nd_text_size:I = 0x7f050138

.field public static final stamp_style18_2nd_top_margin:I = 0x7f05013c

.field public static final stamp_style18_2nd_width:I = 0x7f050133

.field public static final stamp_style18_3rd_height:I = 0x7f050136

.field public static final stamp_style18_3rd_left_margin:I = 0x7f05013d

.field public static final stamp_style18_3rd_text_size:I = 0x7f050139

.field public static final stamp_style18_3rd_top_margin:I = 0x7f05013e

.field public static final stamp_style18_3rd_width:I = 0x7f050135

.field public static final stamp_style19_1st_height:I = 0x7f050140

.field public static final stamp_style19_1st_left_margin:I = 0x7f050148

.field public static final stamp_style19_1st_text_size:I = 0x7f050145

.field public static final stamp_style19_1st_top_margin:I = 0x7f050149

.field public static final stamp_style19_1st_width:I = 0x7f05013f

.field public static final stamp_style19_2nd_height:I = 0x7f050142

.field public static final stamp_style19_2nd_left_margin:I = 0x7f05014a

.field public static final stamp_style19_2nd_text_size:I = 0x7f050146

.field public static final stamp_style19_2nd_top_margin:I = 0x7f05014b

.field public static final stamp_style19_2nd_width:I = 0x7f050141

.field public static final stamp_style19_3rd_height:I = 0x7f050144

.field public static final stamp_style19_3rd_left_margin:I = 0x7f05014c

.field public static final stamp_style19_3rd_text_size:I = 0x7f050147

.field public static final stamp_style19_3rd_top_margin:I = 0x7f05014d

.field public static final stamp_style19_3rd_width:I = 0x7f050143

.field public static final stamp_style1_1st_height:I = 0x7f050041

.field public static final stamp_style1_1st_text_size:I = 0x7f050043

.field public static final stamp_style1_1st_top_margin:I = 0x7f050045

.field public static final stamp_style1_2nd_bottom_margin:I = 0x7f050046

.field public static final stamp_style1_2nd_height:I = 0x7f050042

.field public static final stamp_style1_2nd_text_size:I = 0x7f050044

.field public static final stamp_style20_1st_height:I = 0x7f05014f

.field public static final stamp_style20_1st_left_margin:I = 0x7f050157

.field public static final stamp_style20_1st_text_size:I = 0x7f050154

.field public static final stamp_style20_1st_top_margin:I = 0x7f050158

.field public static final stamp_style20_1st_width:I = 0x7f05014e

.field public static final stamp_style20_2nd_height:I = 0x7f050151

.field public static final stamp_style20_2nd_left_margin:I = 0x7f050159

.field public static final stamp_style20_2nd_text_size:I = 0x7f050155

.field public static final stamp_style20_2nd_top_margin:I = 0x7f05015a

.field public static final stamp_style20_2nd_width:I = 0x7f050150

.field public static final stamp_style20_3rd_height:I = 0x7f050153

.field public static final stamp_style20_3rd_left_margin:I = 0x7f05015b

.field public static final stamp_style20_3rd_text_size:I = 0x7f050156

.field public static final stamp_style20_3rd_top_margin:I = 0x7f05015c

.field public static final stamp_style20_3rd_width:I = 0x7f050152

.field public static final stamp_style21_1st_height:I = 0x7f05015e

.field public static final stamp_style21_1st_left_margin:I = 0x7f050163

.field public static final stamp_style21_1st_text_size:I = 0x7f050161

.field public static final stamp_style21_1st_top_margin:I = 0x7f050164

.field public static final stamp_style21_1st_width:I = 0x7f05015d

.field public static final stamp_style21_2nd_height:I = 0x7f050160

.field public static final stamp_style21_2nd_left_margin:I = 0x7f050165

.field public static final stamp_style21_2nd_text_size:I = 0x7f050162

.field public static final stamp_style21_2nd_top_margin:I = 0x7f050166

.field public static final stamp_style21_2nd_width:I = 0x7f05015f

.field public static final stamp_style21_noinfo_left_margin:I = 0x7f050168

.field public static final stamp_style21_noinfo_top_margin:I = 0x7f050167

.field public static final stamp_style22_1st_height:I = 0x7f05016a

.field public static final stamp_style22_1st_left_margin:I = 0x7f050172

.field public static final stamp_style22_1st_text_size:I = 0x7f05016f

.field public static final stamp_style22_1st_top_margin:I = 0x7f050173

.field public static final stamp_style22_1st_width:I = 0x7f050169

.field public static final stamp_style22_2nd_height:I = 0x7f05016c

.field public static final stamp_style22_2nd_text_size:I = 0x7f050170

.field public static final stamp_style22_2nd_top_margin:I = 0x7f050174

.field public static final stamp_style22_2nd_width:I = 0x7f05016b

.field public static final stamp_style22_3rd_height:I = 0x7f05016e

.field public static final stamp_style22_3rd_text_size:I = 0x7f050171

.field public static final stamp_style22_3rd_top_margin:I = 0x7f050175

.field public static final stamp_style22_3rd_width:I = 0x7f05016d

.field public static final stamp_style23_1st_height:I = 0x7f050177

.field public static final stamp_style23_1st_left_margin:I = 0x7f05017d

.field public static final stamp_style23_1st_text_size:I = 0x7f05017c

.field public static final stamp_style23_1st_top_margin:I = 0x7f05017e

.field public static final stamp_style23_1st_width:I = 0x7f050176

.field public static final stamp_style23_2nd_height:I = 0x7f050179

.field public static final stamp_style23_2nd_top_margin:I = 0x7f05017f

.field public static final stamp_style23_2nd_width:I = 0x7f050178

.field public static final stamp_style23_3rd_height:I = 0x7f05017b

.field public static final stamp_style23_3rd_top_margin:I = 0x7f050180

.field public static final stamp_style23_3rd_width:I = 0x7f05017a

.field public static final stamp_style24_1st_height:I = 0x7f050182

.field public static final stamp_style24_1st_left_margin:I = 0x7f050188

.field public static final stamp_style24_1st_text_size:I = 0x7f050187

.field public static final stamp_style24_1st_top_margin:I = 0x7f050189

.field public static final stamp_style24_1st_width:I = 0x7f050181

.field public static final stamp_style24_2nd_height:I = 0x7f050184

.field public static final stamp_style24_2nd_left_margin:I = 0x7f05018a

.field public static final stamp_style24_2nd_top_margin:I = 0x7f05018b

.field public static final stamp_style24_2nd_width:I = 0x7f050183

.field public static final stamp_style24_3rd_height:I = 0x7f050186

.field public static final stamp_style24_3rd_left_margin:I = 0x7f05018c

.field public static final stamp_style24_3rd_top_margin:I = 0x7f05018d

.field public static final stamp_style24_3rd_width:I = 0x7f050185

.field public static final stamp_style25_1st_height:I = 0x7f05018f

.field public static final stamp_style25_1st_left_margin:I = 0x7f050195

.field public static final stamp_style25_1st_text_size:I = 0x7f050194

.field public static final stamp_style25_1st_top_margin:I = 0x7f050196

.field public static final stamp_style25_1st_width:I = 0x7f05018e

.field public static final stamp_style25_2nd_height:I = 0x7f050191

.field public static final stamp_style25_2nd_left_margin:I = 0x7f050197

.field public static final stamp_style25_2nd_top_margin:I = 0x7f050198

.field public static final stamp_style25_2nd_width:I = 0x7f050190

.field public static final stamp_style25_3rd_height:I = 0x7f050193

.field public static final stamp_style25_3rd_left_margin:I = 0x7f050199

.field public static final stamp_style25_3rd_top_margin:I = 0x7f05019a

.field public static final stamp_style25_3rd_width:I = 0x7f050192

.field public static final stamp_style26_1st_height:I = 0x7f05019c

.field public static final stamp_style26_1st_left_margin:I = 0x7f0501a5

.field public static final stamp_style26_1st_text_size:I = 0x7f0501a3

.field public static final stamp_style26_1st_top_margin:I = 0x7f0501a6

.field public static final stamp_style26_1st_width:I = 0x7f05019b

.field public static final stamp_style26_2nd_height:I = 0x7f05019e

.field public static final stamp_style26_2nd_left_margin:I = 0x7f0501a7

.field public static final stamp_style26_2nd_top_margin:I = 0x7f0501a8

.field public static final stamp_style26_2nd_width:I = 0x7f05019d

.field public static final stamp_style26_3rd_height:I = 0x7f0501a0

.field public static final stamp_style26_3rd_left_margin:I = 0x7f0501a9

.field public static final stamp_style26_3rd_top_margin:I = 0x7f0501aa

.field public static final stamp_style26_3rd_width:I = 0x7f05019f

.field public static final stamp_style26_4th_height:I = 0x7f0501a2

.field public static final stamp_style26_4th_left_margin:I = 0x7f0501ab

.field public static final stamp_style26_4th_text_size:I = 0x7f0501a4

.field public static final stamp_style26_4th_top_margin:I = 0x7f0501ac

.field public static final stamp_style26_4th_width:I = 0x7f0501a1

.field public static final stamp_style27_1st_height:I = 0x7f0501ae

.field public static final stamp_style27_1st_left_margin:I = 0x7f0501b7

.field public static final stamp_style27_1st_text_size:I = 0x7f0501b5

.field public static final stamp_style27_1st_top_margin:I = 0x7f0501b8

.field public static final stamp_style27_1st_width:I = 0x7f0501ad

.field public static final stamp_style27_2nd_height:I = 0x7f0501b0

.field public static final stamp_style27_2nd_left_margin:I = 0x7f0501b9

.field public static final stamp_style27_2nd_top_margin:I = 0x7f0501ba

.field public static final stamp_style27_2nd_width:I = 0x7f0501af

.field public static final stamp_style27_3rd_height:I = 0x7f0501b2

.field public static final stamp_style27_3rd_left_margin:I = 0x7f0501bb

.field public static final stamp_style27_3rd_top_margin:I = 0x7f0501bc

.field public static final stamp_style27_3rd_width:I = 0x7f0501b1

.field public static final stamp_style27_4th_height:I = 0x7f0501b4

.field public static final stamp_style27_4th_left_margin:I = 0x7f0501bd

.field public static final stamp_style27_4th_text_size:I = 0x7f0501b6

.field public static final stamp_style27_4th_top_margin:I = 0x7f0501be

.field public static final stamp_style27_4th_width:I = 0x7f0501b3

.field public static final stamp_style28_1st_height:I = 0x7f0501c0

.field public static final stamp_style28_1st_left_margin:I = 0x7f0501c6

.field public static final stamp_style28_1st_text_size:I = 0x7f0501c5

.field public static final stamp_style28_1st_top_margin:I = 0x7f0501c7

.field public static final stamp_style28_1st_width:I = 0x7f0501bf

.field public static final stamp_style28_2nd_height:I = 0x7f0501c2

.field public static final stamp_style28_2nd_left_margin:I = 0x7f0501c8

.field public static final stamp_style28_2nd_top_margin:I = 0x7f0501c9

.field public static final stamp_style28_2nd_width:I = 0x7f0501c1

.field public static final stamp_style28_3rd_height:I = 0x7f0501c4

.field public static final stamp_style28_3rd_left_margin:I = 0x7f0501ca

.field public static final stamp_style28_3rd_top_margin:I = 0x7f0501cb

.field public static final stamp_style28_3rd_width:I = 0x7f0501c3

.field public static final stamp_style29_1st_height:I = 0x7f0501cd

.field public static final stamp_style29_1st_left_margin:I = 0x7f0501d3

.field public static final stamp_style29_1st_text_size:I = 0x7f0501d2

.field public static final stamp_style29_1st_top_margin:I = 0x7f0501d4

.field public static final stamp_style29_1st_width:I = 0x7f0501cc

.field public static final stamp_style29_2nd_height:I = 0x7f0501cf

.field public static final stamp_style29_2nd_left_margin:I = 0x7f0501d5

.field public static final stamp_style29_2nd_top_margin:I = 0x7f0501d6

.field public static final stamp_style29_2nd_width:I = 0x7f0501ce

.field public static final stamp_style29_3rd_height:I = 0x7f0501d1

.field public static final stamp_style29_3rd_left_margin:I = 0x7f0501d7

.field public static final stamp_style29_3rd_top_margin:I = 0x7f0501d8

.field public static final stamp_style29_3rd_width:I = 0x7f0501d0

.field public static final stamp_style2_1st_height:I = 0x7f050047

.field public static final stamp_style2_1st_text_size:I = 0x7f050049

.field public static final stamp_style2_1st_top_margin:I = 0x7f05004b

.field public static final stamp_style2_2nd_bottom_margin:I = 0x7f05004c

.field public static final stamp_style2_2nd_height:I = 0x7f050048

.field public static final stamp_style2_2nd_text_size:I = 0x7f05004a

.field public static final stamp_style30_1st_height:I = 0x7f0501da

.field public static final stamp_style30_1st_left_margin:I = 0x7f0501e1

.field public static final stamp_style30_1st_text_size:I = 0x7f0501df

.field public static final stamp_style30_1st_top_margin:I = 0x7f0501e2

.field public static final stamp_style30_1st_width:I = 0x7f0501d9

.field public static final stamp_style30_2nd_height:I = 0x7f0501dc

.field public static final stamp_style30_2nd_left_margin:I = 0x7f0501e3

.field public static final stamp_style30_2nd_text_size:I = 0x7f0501e0

.field public static final stamp_style30_2nd_top_margin:I = 0x7f0501e4

.field public static final stamp_style30_2nd_width:I = 0x7f0501db

.field public static final stamp_style30_3rd_height:I = 0x7f0501de

.field public static final stamp_style30_3rd_left_margin:I = 0x7f0501e5

.field public static final stamp_style30_3rd_top_margin:I = 0x7f0501e6

.field public static final stamp_style30_3rd_width:I = 0x7f0501dd

.field public static final stamp_style31_1st_height:I = 0x7f0501e8

.field public static final stamp_style31_1st_left_margin:I = 0x7f0501ef

.field public static final stamp_style31_1st_text_size:I = 0x7f0501ed

.field public static final stamp_style31_1st_top_margin:I = 0x7f0501f0

.field public static final stamp_style31_1st_width:I = 0x7f0501e7

.field public static final stamp_style31_2nd_height:I = 0x7f0501ea

.field public static final stamp_style31_2nd_left_margin:I = 0x7f0501f1

.field public static final stamp_style31_2nd_text_size:I = 0x7f0501ee

.field public static final stamp_style31_2nd_top_margin:I = 0x7f0501f2

.field public static final stamp_style31_2nd_width:I = 0x7f0501e9

.field public static final stamp_style31_3rd_height:I = 0x7f0501ec

.field public static final stamp_style31_3rd_left_margin:I = 0x7f0501f3

.field public static final stamp_style31_3rd_top_margin:I = 0x7f0501f4

.field public static final stamp_style31_3rd_width:I = 0x7f0501eb

.field public static final stamp_style32_1st_height:I = 0x7f0501f6

.field public static final stamp_style32_1st_left_margin:I = 0x7f0501fc

.field public static final stamp_style32_1st_text_size:I = 0x7f0501fb

.field public static final stamp_style32_1st_top_margin:I = 0x7f0501fd

.field public static final stamp_style32_1st_width:I = 0x7f0501f5

.field public static final stamp_style32_2nd_height:I = 0x7f0501f8

.field public static final stamp_style32_2nd_left_margin:I = 0x7f0501fe

.field public static final stamp_style32_2nd_top_margin:I = 0x7f0501ff

.field public static final stamp_style32_2nd_width:I = 0x7f0501f7

.field public static final stamp_style32_3rd_height:I = 0x7f0501fa

.field public static final stamp_style32_3rd_left_margin:I = 0x7f050200

.field public static final stamp_style32_3rd_top_margin:I = 0x7f050201

.field public static final stamp_style32_3rd_width:I = 0x7f0501f9

.field public static final stamp_style33_1st_height:I = 0x7f050203

.field public static final stamp_style33_1st_left_margin:I = 0x7f050209

.field public static final stamp_style33_1st_text_size:I = 0x7f050208

.field public static final stamp_style33_1st_top_margin:I = 0x7f05020a

.field public static final stamp_style33_1st_width:I = 0x7f050202

.field public static final stamp_style33_2nd_height:I = 0x7f050205

.field public static final stamp_style33_2nd_left_margin:I = 0x7f05020b

.field public static final stamp_style33_2nd_top_margin:I = 0x7f05020c

.field public static final stamp_style33_2nd_width:I = 0x7f050204

.field public static final stamp_style33_3rd_height:I = 0x7f050207

.field public static final stamp_style33_3rd_left_margin:I = 0x7f05020d

.field public static final stamp_style33_3rd_top_margin:I = 0x7f05020e

.field public static final stamp_style33_3rd_width:I = 0x7f050206

.field public static final stamp_style34_1st_height:I = 0x7f050210

.field public static final stamp_style34_1st_left_margin:I = 0x7f050216

.field public static final stamp_style34_1st_text_size:I = 0x7f050215

.field public static final stamp_style34_1st_top_margin:I = 0x7f050217

.field public static final stamp_style34_1st_width:I = 0x7f05020f

.field public static final stamp_style34_2nd_height:I = 0x7f050212

.field public static final stamp_style34_2nd_left_margin:I = 0x7f050218

.field public static final stamp_style34_2nd_top_margin:I = 0x7f050219

.field public static final stamp_style34_2nd_width:I = 0x7f050211

.field public static final stamp_style34_3rd_height:I = 0x7f050214

.field public static final stamp_style34_3rd_left_margin:I = 0x7f05021a

.field public static final stamp_style34_3rd_top_margin:I = 0x7f05021b

.field public static final stamp_style34_3rd_width:I = 0x7f050213

.field public static final stamp_style3_1st_height:I = 0x7f05004d

.field public static final stamp_style3_1st_text_size:I = 0x7f050051

.field public static final stamp_style3_1st_top_margin:I = 0x7f050055

.field public static final stamp_style3_2nd_height:I = 0x7f05004e

.field public static final stamp_style3_2nd_text_size:I = 0x7f050052

.field public static final stamp_style3_2nd_top_margin:I = 0x7f050056

.field public static final stamp_style3_3rd_bottom_margin:I = 0x7f050057

.field public static final stamp_style3_3rd_height:I = 0x7f05004f

.field public static final stamp_style3_3rd_text_size:I = 0x7f050053

.field public static final stamp_style3_4th_bottom_margin:I = 0x7f050058

.field public static final stamp_style3_4th_height:I = 0x7f050050

.field public static final stamp_style3_4th_text_size:I = 0x7f050054

.field public static final stamp_style4_1st_height:I = 0x7f050059

.field public static final stamp_style4_1st_text_size:I = 0x7f05005d

.field public static final stamp_style4_1st_top_margin:I = 0x7f050061

.field public static final stamp_style4_2nd_height:I = 0x7f05005a

.field public static final stamp_style4_2nd_text_size:I = 0x7f05005e

.field public static final stamp_style4_2nd_top_margin:I = 0x7f050062

.field public static final stamp_style4_3rd_bottom_margin:I = 0x7f050063

.field public static final stamp_style4_3rd_height:I = 0x7f05005b

.field public static final stamp_style4_3rd_text_size:I = 0x7f05005f

.field public static final stamp_style4_4th_bottom_margin:I = 0x7f050064

.field public static final stamp_style4_4th_height:I = 0x7f05005c

.field public static final stamp_style4_4th_text_size:I = 0x7f050060

.field public static final stamp_style4_noinfo_top_margin:I = 0x7f050065

.field public static final stamp_style5_1st_height:I = 0x7f050066

.field public static final stamp_style5_1st_text_size:I = 0x7f05006c

.field public static final stamp_style5_1st_top_margin:I = 0x7f050070

.field public static final stamp_style5_2nd_height:I = 0x7f050068

.field public static final stamp_style5_2nd_text_size:I = 0x7f05006d

.field public static final stamp_style5_2nd_top_margin:I = 0x7f050071

.field public static final stamp_style5_2nd_width:I = 0x7f050067

.field public static final stamp_style5_3rd_bottom_margin:I = 0x7f050072

.field public static final stamp_style5_3rd_height:I = 0x7f05006a

.field public static final stamp_style5_3rd_text_size:I = 0x7f05006e

.field public static final stamp_style5_3rd_width:I = 0x7f050069

.field public static final stamp_style5_4th_bottom_margin:I = 0x7f050073

.field public static final stamp_style5_4th_height:I = 0x7f05006b

.field public static final stamp_style5_4th_text_size:I = 0x7f05006f

.field public static final stamp_style5_noinfo_top_margin:I = 0x7f050074

.field public static final stamp_style6_1st_height:I = 0x7f050075

.field public static final stamp_style6_1st_text_size:I = 0x7f050079

.field public static final stamp_style6_1st_top_margin:I = 0x7f05007d

.field public static final stamp_style6_2nd_height:I = 0x7f050076

.field public static final stamp_style6_2nd_text_size:I = 0x7f05007a

.field public static final stamp_style6_2nd_top_margin:I = 0x7f05007e

.field public static final stamp_style6_3rd_bottom_margin:I = 0x7f05007f

.field public static final stamp_style6_3rd_height:I = 0x7f050077

.field public static final stamp_style6_3rd_text_size:I = 0x7f05007b

.field public static final stamp_style6_4th_bottom_margin:I = 0x7f050080

.field public static final stamp_style6_4th_height:I = 0x7f050078

.field public static final stamp_style6_4th_text_size:I = 0x7f05007c

.field public static final stamp_style6_noinfo_top_margin:I = 0x7f050081

.field public static final stamp_style7_1st_bottom_margin:I = 0x7f050085

.field public static final stamp_style7_1st_height:I = 0x7f050082

.field public static final stamp_style7_1st_right_margin:I = 0x7f050086

.field public static final stamp_style7_1st_text_size:I = 0x7f050083

.field public static final stamp_style7_1st_top_margin:I = 0x7f050084

.field public static final stamp_style8_1st_height:I = 0x7f050088

.field public static final stamp_style8_1st_left_margin:I = 0x7f050092

.field public static final stamp_style8_1st_text_size:I = 0x7f05008d

.field public static final stamp_style8_1st_top_margin:I = 0x7f050091

.field public static final stamp_style8_1st_width:I = 0x7f050087

.field public static final stamp_style8_2nd_height:I = 0x7f050089

.field public static final stamp_style8_2nd_text_size:I = 0x7f05008e

.field public static final stamp_style8_2nd_top_margin:I = 0x7f050093

.field public static final stamp_style8_3rd_height:I = 0x7f05008a

.field public static final stamp_style8_3rd_text_size:I = 0x7f05008f

.field public static final stamp_style8_3rd_top_margin:I = 0x7f050094

.field public static final stamp_style8_4th_height:I = 0x7f05008c

.field public static final stamp_style8_4th_left_margin:I = 0x7f050096

.field public static final stamp_style8_4th_text_size:I = 0x7f050090

.field public static final stamp_style8_4th_top_margin:I = 0x7f050095

.field public static final stamp_style8_4th_width:I = 0x7f05008b

.field public static final stamp_style8_noinfo_left_margin:I = 0x7f050098

.field public static final stamp_style8_noinfo_top_margin:I = 0x7f050097

.field public static final stamp_style9_1st_height:I = 0x7f050099

.field public static final stamp_style9_1st_text_size:I = 0x7f05009d

.field public static final stamp_style9_1st_top_margin:I = 0x7f0500a1

.field public static final stamp_style9_2nd_height:I = 0x7f05009a

.field public static final stamp_style9_2nd_month_margin_3:I = 0x7f0500a7

.field public static final stamp_style9_2nd_month_margin_4:I = 0x7f0500a8

.field public static final stamp_style9_2nd_month_margin_5:I = 0x7f0500a9

.field public static final stamp_style9_2nd_month_margin_6:I = 0x7f0500aa

.field public static final stamp_style9_2nd_month_margin_7:I = 0x7f0500ab

.field public static final stamp_style9_2nd_month_margin_8:I = 0x7f0500ac

.field public static final stamp_style9_2nd_month_margin_9:I = 0x7f0500ad

.field public static final stamp_style9_2nd_right_margin:I = 0x7f0500a3

.field public static final stamp_style9_2nd_text_size:I = 0x7f05009e

.field public static final stamp_style9_2nd_top_margin:I = 0x7f0500a2

.field public static final stamp_style9_3rd_height:I = 0x7f05009b

.field public static final stamp_style9_3rd_left_margin:I = 0x7f0500a5

.field public static final stamp_style9_3rd_text_size:I = 0x7f05009f

.field public static final stamp_style9_3rd_top_margin:I = 0x7f0500a4

.field public static final stamp_style9_4th_height:I = 0x7f05009c

.field public static final stamp_style9_4th_text_size:I = 0x7f0500a0

.field public static final stamp_style9_4th_top_margin:I = 0x7f0500a6

.field public static final stamp_thumbnail_size:I = 0x7f05003a

.field public static final sticker_item_extra_offset_scroll_down:I = 0x7f050222

.field public static final sticker_item_offset_scroll_down:I = 0x7f050221

.field public static final sticker_item_offset_scroll_up:I = 0x7f050220

.field public static final sub_bottom_btn_left_margin_port:I = 0x7f050243

.field public static final sub_bottom_btn_scroll_effect:I = 0x7f050245

.field public static final sub_bottom_btn_scroll_minus:I = 0x7f050244

.field public static final sub_bottom_btn_scroll_size_port:I = 0x7f050242

.field public static final sub_bottom_image_btn_height:I = 0x7f050246

.field public static final sub_bottom_thumbnail_btn_height:I = 0x7f050247

.field public static final submenu_icon_padding:I = 0x7f05024b

.field public static final toast_actionbar_shifting:I = 0x7f05024c

.field public static final viewpager_margin:I = 0x7f05025e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
