.class public Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;
.super Landroid/widget/ImageView;
.source "ViewPagerImageView.java"


# instance fields
.field isSetBitmap:Z

.field mBitmap:Landroid/graphics/Bitmap;

.field mContext:Landroid/content/Context;

.field mImageBitmap:Landroid/graphics/Bitmap;

.field mLeft:I

.field mRotate:I

.field mTop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 77
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mImageBitmap:Landroid/graphics/Bitmap;

    .line 78
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mContext:Landroid/content/Context;

    .line 79
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->isSetBitmap:Z

    .line 80
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 81
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mRotate:I

    .line 82
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mLeft:I

    .line 83
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mTop:I

    .line 20
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mContext:Landroid/content/Context;

    .line 21
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->init()V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 77
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mImageBitmap:Landroid/graphics/Bitmap;

    .line 78
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mContext:Landroid/content/Context;

    .line 79
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->isSetBitmap:Z

    .line 80
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 81
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mRotate:I

    .line 82
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mLeft:I

    .line 83
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mTop:I

    .line 27
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mContext:Landroid/content/Context;

    .line 28
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->init()V

    .line 29
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020592

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 33
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mLeft:I

    .line 34
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mTop:I

    .line 35
    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 69
    const/4 v1, 0x0

    :try_start_0
    invoke-super {p0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 76
    :goto_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    goto :goto_0

    .line 73
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    .line 74
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 75
    throw v1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 53
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->isSetBitmap:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mImageBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mImageBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 54
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 66
    :goto_0
    return-void

    .line 57
    :cond_0
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 58
    .local v0, "matrix":Landroid/graphics/Matrix;
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mRotate:I

    add-int/lit8 v1, v1, 0x5

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mRotate:I

    .line 59
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mRotate:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 60
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mLeft:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mTop:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 62
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 63
    const-wide/16 v2, 0xa

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->postInvalidateDelayed(J)V

    goto :goto_0
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bm"    # Landroid/graphics/Bitmap;
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->isSetBitmap:Z

    .line 41
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->mImageBitmap:Landroid/graphics/Bitmap;

    .line 42
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 43
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0
    .param p1, "l"    # Landroid/view/View$OnTouchListener;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 48
    return-void
.end method
