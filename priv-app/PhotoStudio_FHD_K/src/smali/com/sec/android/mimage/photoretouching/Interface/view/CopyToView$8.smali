.class Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$8;
.super Ljava/lang/Object;
.source "CopyToView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->initCopyToAnotherNotiDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    .line 583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/4 v7, 0x0

    .line 587
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/widget/CheckBox;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 589
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/widget/CheckBox;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 591
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPackageInfo(Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 592
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 594
    .local v3, "versionCode":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v5

    const-string v6, "PhotoEditor"

    invoke-static {v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 595
    .local v2, "settings":Landroid/content/SharedPreferences;
    const-string v5, "CopyToAnother"

    invoke-interface {v2, v5, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 597
    .local v4, "viewedChangelogVersion":I
    if-ge v4, v3, :cond_0

    .line 598
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 599
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "CopyToAnother"

    invoke-interface {v0, v5, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 600
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 604
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v2    # "settings":Landroid/content/SharedPreferences;
    .end local v3    # "versionCode":I
    .end local v4    # "viewedChangelogVersion":I
    :cond_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 605
    return-void
.end method
