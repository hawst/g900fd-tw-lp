.class Lcom/sec/android/mimage/photoretouching/PhotoRetouching$2;
.super Landroid/os/Handler;
.source "PhotoRetouching.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/PhotoRetouching;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 625
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 629
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->CONFIG_CHANGE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 630
    .local v0, "configChange":Z
    if-eqz v0, :cond_0

    # getter for: Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mNewConfig:Landroid/content/res/Configuration;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->access$0()Landroid/content/res/Configuration;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 631
    # getter for: Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mActivity:Landroid/app/Activity;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->access$1()Landroid/app/Activity;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mNewConfig:Landroid/content/res/Configuration;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->access$0()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 632
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->access$2(Landroid/content/res/Configuration;)V

    .line 634
    :cond_0
    return-void
.end method
