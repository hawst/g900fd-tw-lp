.class Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;
.super Ljava/lang/Object;
.source "PortraitView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RedEyeAnimation"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;
    }
.end annotation


# instance fields
.field private mAnimationFrame:[Landroid/graphics/Bitmap;

.field private mAnimationThreadList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;",
            ">;"
        }
    .end annotation
.end field

.field private mId:[I

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Landroid/content/Context;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 2562
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    .line 2561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2523
    const/16 v1, 0x1e

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 2553
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mId:[I

    .line 2557
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationFrame:[Landroid/graphics/Bitmap;

    .line 2558
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationThreadList:Ljava/util/ArrayList;

    .line 2563
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationThreadList:Ljava/util/ArrayList;

    .line 2564
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mId:[I

    array-length v1, v1

    new-array v1, v1, [Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationFrame:[Landroid/graphics/Bitmap;

    .line 2566
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mId:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 2570
    return-void

    .line 2568
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationFrame:[Landroid/graphics/Bitmap;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mId:[I

    aget v3, v3, v0

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v0

    .line 2566
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2523
    nop

    :array_0
    .array-data 4
        0x7f0204cc
        0x7f0204cd
        0x7f0204ce
        0x7f0204cf
        0x7f0204d0
        0x7f0204d1
        0x7f0204d2
        0x7f0204d3
        0x7f0204d4
        0x7f0204d5
        0x7f0204d6
        0x7f0204d7
        0x7f0204d8
        0x7f0204d9
        0x7f0204da
        0x7f0204db
        0x7f0204dc
        0x7f0204dd
        0x7f0204de
        0x7f0204df
        0x7f0204e0
        0x7f0204e1
        0x7f0204e2
        0x7f0204e3
        0x7f0204e4
        0x7f0204e5
        0x7f0204e6
        0x7f0204e7
        0x7f0204e8
        0x7f0204e9
    .end array-data
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)[Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2557
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationFrame:[Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 2558
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationThreadList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)[I
    .locals 1

    .prologue
    .line 2523
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mId:[I

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;
    .locals 1

    .prologue
    .line 2521
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 4

    .prologue
    .line 2573
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationThreadList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2578
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationThreadList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2582
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationFrame:[Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 2584
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mId:[I

    array-length v2, v2

    if-lt v0, v2, :cond_3

    .line 2588
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationFrame:[Landroid/graphics/Bitmap;

    .line 2590
    .end local v0    # "i":I
    :cond_1
    return-void

    .line 2573
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;

    .line 2575
    .local v1, "thread":Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;
    if-eqz v1, :cond_0

    .line 2576
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->killThread()V

    goto :goto_0

    .line 2586
    .end local v1    # "thread":Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;
    .restart local v0    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationFrame:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v0

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 2584
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public drawCanvas(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2625
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationThreadList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2630
    return-void

    .line 2625
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;

    .line 2627
    .local v0, "thread":Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;
    if-eqz v0, :cond_0

    .line 2628
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->drawCanvas(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public isRunningAnimation()Z
    .locals 4

    .prologue
    .line 2605
    const/4 v0, 0x0

    .line 2610
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationThreadList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2617
    :goto_0
    return v0

    .line 2610
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;

    .line 2612
    .local v1, "thread":Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;
    if-eqz v1, :cond_2

    .line 2613
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->isRunning()Z

    move-result v0

    .line 2614
    :cond_2
    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public runAnimation(FF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 2596
    const-string v1, "runAnimation111"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2597
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;FF)V

    .line 2598
    .local v0, "animationThread":Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationThreadList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2599
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation$AnimationThread;->start()V

    .line 2600
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "runAnimation222 mAnimationThreadList.size():"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->mAnimationThreadList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2602
    return-void
.end method
