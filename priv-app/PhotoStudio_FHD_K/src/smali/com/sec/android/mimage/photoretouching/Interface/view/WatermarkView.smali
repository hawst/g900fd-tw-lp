.class public Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "WatermarkView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;
    }
.end annotation


# instance fields
.field private final ICON_HEIGHT:I

.field private final ICON_WIDTH:I

.field private final REDOALL_DIALOG:I

.field private final STICKER_MAX_NUM:I

.field private final UNDOALL_DIALOG:I

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentSaveSize:I

.field private mCurrentStickerCount:I

.field private mCurrentStickerIndex:I

.field private mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mIsLongClicked:Z

.field private mIsshowTabLayout:Z

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mPaint:Landroid/graphics/Paint;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

.field private mTouchType:I

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;

.field onWatermarkCallback:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnWatermarkCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p6, "decoManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .prologue
    const/16 v5, 0xe

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 84
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->onWatermarkCallback:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnWatermarkCallback;

    .line 1069
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mTouchType:I

    .line 1070
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mCurrentStickerCount:I

    .line 1071
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mCurrentStickerIndex:I

    .line 1073
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1075
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1076
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;

    .line 1077
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1078
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1079
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1080
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1081
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1082
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1083
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 1085
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    .line 1088
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1089
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mPaint:Landroid/graphics/Paint;

    .line 1093
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->ICON_WIDTH:I

    .line 1094
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->ICON_HEIGHT:I

    .line 1095
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->STICKER_MAX_NUM:I

    .line 1096
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->UNDOALL_DIALOG:I

    .line 1097
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->REDOALL_DIALOG:I

    .line 1098
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mIsshowTabLayout:Z

    .line 1099
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mIsLongClicked:Z

    .line 1101
    const v0, 0x7a1200

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mCurrentSaveSize:I

    .line 64
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 66
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 67
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 68
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 69
    iput-object p6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 71
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->setInterface(Ljava/lang/Object;)V

    .line 73
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mPaint:Landroid/graphics/Paint;

    .line 74
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 77
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 78
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->initTabLayout()V

    .line 81
    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->setViewLayerType(I)V

    .line 82
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 1075
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1076
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->getImageEditViewWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->getImageEditViewHeight()I

    move-result v0

    return v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1088
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 1079
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1088
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    .locals 1

    .prologue
    .line 1083
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;I)V
    .locals 0

    .prologue
    .line 1101
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mCurrentSaveSize:I

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V
    .locals 0

    .prologue
    .line 413
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->doCancel()V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)I
    .locals 1

    .prologue
    .line 1070
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mCurrentStickerCount:I

    return v0
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V
    .locals 0

    .prologue
    .line 433
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->doDone()V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 1082
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 1077
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)I
    .locals 1

    .prologue
    .line 1101
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mCurrentSaveSize:I

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 1075
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 1073
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 1073
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method private doCancel()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 415
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 417
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 418
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 421
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 424
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 425
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 426
    const/4 v8, 0x1

    .line 427
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 419
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 429
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->set2depth()V

    .line 431
    return-void
.end method

.method private doDone()V
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->applyPreview()V

    .line 444
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->set2depth()V

    .line 445
    return-void
.end method

.method private init2DepthActionBar()V
    .locals 5

    .prologue
    const/4 v2, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 139
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isValidBackKey()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 146
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 143
    invoke-virtual {v0, v4, v2, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 170
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 173
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 170
    invoke-virtual {v0, v4, v3, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 227
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x2

    .line 230
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$4;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 227
    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 287
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 290
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$5;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 287
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 312
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x5

    .line 315
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$6;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 312
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 342
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 344
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isSaved()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 347
    :cond_0
    return-void

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    goto :goto_0
.end method

.method private init3DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 350
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 353
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 356
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$7;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 353
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 380
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 383
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$8;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 380
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 408
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 409
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 410
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 412
    :cond_0
    return-void
.end method

.method private initSaveOptionDialog()V
    .locals 11

    .prologue
    const v10, 0x7f0601f0

    const v9, 0x7f0601cb

    const/16 v2, 0x9

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 769
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 823
    :goto_0
    return-void

    .line 774
    :cond_0
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$9;

    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 788
    .local v8, "saveListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 790
    const v3, 0x7f0601cd

    .line 792
    const/4 v5, 0x1

    .line 794
    const v7, 0x103012e

    .line 788
    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZZLandroid/graphics/Point;I)V

    .line 796
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 798
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x810

    move v3, v9

    move v5, v9

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 802
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x820

    move v3, v10

    move v5, v10

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 804
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$10;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 811
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$11;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 822
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    goto :goto_0
.end method

.method private initTabLayout()V
    .locals 2

    .prologue
    .line 648
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->initDataBase(Landroid/content/Context;)V

    .line 649
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setDrawerHandleVisibility(I)V

    .line 650
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTab()V

    .line 652
    return-void
.end method

.method private initUndoRedoAllDialog()V
    .locals 15

    .prologue
    const v6, 0x103012e

    const/16 v1, 0x1000

    const/16 v14, 0x500

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 826
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 828
    const v3, 0x7f0600a6

    move v4, v2

    .line 826
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 833
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 834
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 835
    const v3, 0x7f06009c

    .line 837
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$12;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 834
    invoke-virtual {v0, v14, v3, v5, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 844
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f06000a

    .line 845
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$13;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 844
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 852
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060009

    .line 853
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$14;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 852
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 861
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 863
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 864
    const/4 v9, 0x2

    .line 865
    const v10, 0x7f0600a1

    move v8, v1

    move v11, v2

    move-object v12, v5

    move v13, v6

    .line 863
    invoke-virtual/range {v7 .. v13}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 870
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 871
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 872
    const v1, 0x7f0601ce

    .line 874
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$15;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 871
    invoke-virtual {v0, v14, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 881
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000a

    .line 882
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$16;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$16;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 881
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 889
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 890
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$17;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$17;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 889
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 898
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 899
    return-void
.end method

.method private set2depth()V
    .locals 1

    .prologue
    .line 448
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->init2DepthActionBar()V

    .line 449
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    .line 451
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 452
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 457
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 458
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 459
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 466
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setClearSelectedSubButton()V

    .line 468
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->invalidateViews()V

    .line 469
    return-void

    .line 455
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    goto :goto_0

    .line 461
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 462
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_1
.end method

.method private showTabLayout(IZZ)V
    .locals 1
    .param p1, "assistantType"    # I
    .param p2, "initByconfig"    # Z
    .param p3, "doOpen"    # Z

    .prologue
    .line 639
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0, p1, p3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->showTabView(IZ)V

    .line 642
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mIsshowTabLayout:Z

    .line 644
    :cond_0
    return-void
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 516
    const/4 v0, 0x1

    return v0
.end method

.method public backPressed()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->saveToDB()V

    .line 120
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x31000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 130
    return-void
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 611
    return-void
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getActionbarHeight()I

    move-result v0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonHeight()I

    move-result v0

    return v0
.end method

.method public getStatusHeight()I
    .locals 3

    .prologue
    .line 631
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 632
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 633
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 634
    iget v2, v0, Landroid/graphics/Rect;->top:I

    return v2
.end method

.method public getmDecorationMenuLayoutManager()Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    .locals 1

    .prologue
    .line 656
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    return-object v0
.end method

.method public initActionbar()V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->init2DepthActionBar()V

    .line 135
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    .line 137
    :cond_0
    return-void
.end method

.method public initButtons()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW initButton: mDecorationMenuLayoutManager.getRecentlyList().size()="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v2, :cond_0

    .line 111
    const/16 v0, 0xd

    invoke-direct {p0, v0, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->showTabLayout(IZZ)V

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    const/16 v0, 0xc

    invoke-direct {p0, v0, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->showTabLayout(IZZ)V

    goto :goto_0
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 475
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->initUndoRedoAllDialog()V

    .line 477
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->initSaveOptionDialog()V

    .line 479
    :cond_0
    return-void
.end method

.method public initEffect()V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 497
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 599
    return-void
.end method

.method public initTrayLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 602
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 606
    :cond_0
    return-void
.end method

.method public initView()V
    .locals 0

    .prologue
    .line 104
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 544
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 545
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 559
    :goto_0
    return-void

    .line 551
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_1

    .line 553
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 554
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 555
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 557
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 521
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;

    .line 522
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 523
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mPaint:Landroid/graphics/Paint;

    .line 525
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 526
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 528
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 529
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 530
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 531
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->destroy()V

    .line 533
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 536
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 537
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 540
    :cond_0
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 483
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 487
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 488
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mPaint:Landroid/graphics/Paint;

    .line 485
    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 491
    :cond_0
    return-void
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 562
    const/16 v1, 0x17

    if-eq p1, v1, :cond_0

    const/16 v1, 0x42

    if-ne p1, v1, :cond_3

    .line 564
    :cond_0
    const/4 v0, 0x0

    .line 565
    .local v0, "isFocused":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_1

    .line 566
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    move-result v0

    .line 569
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_2

    .line 570
    if-nez v0, :cond_2

    .line 571
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 593
    .end local v0    # "isFocused":Z
    :cond_2
    :goto_0
    return v2

    .line 575
    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 577
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->backPressed()V

    goto :goto_0
.end method

.method public onLayout()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1044
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 1045
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1048
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1049
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1050
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1052
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1055
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1056
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 1050
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1058
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    if-eqz v0, :cond_0

    .line 1060
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mCurrentStickerCount:I

    if-lt v8, v0, :cond_1

    .line 1068
    .end local v8    # "i":I
    :cond_0
    return-void

    .line 1062
    .restart local v8    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v0, v0, v8

    if-eqz v0, :cond_2

    .line 1064
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/Sticker;->configurationChanged()V

    .line 1060
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 620
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 615
    return-void
.end method

.method public refreshView()V
    .locals 0

    .prologue
    .line 1107
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 3

    .prologue
    .line 501
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 502
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 503
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 504
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 505
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->configurationChanged()V

    .line 507
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 509
    const-string v0, "JW setConfigurationChanged: mIsshowTabLayout true"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 510
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getCurrnetType()I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->showTabLayout(IZZ)V

    .line 512
    :cond_0
    return-void
.end method
