.class Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;
.super Ljava/lang/Object;
.source "StampView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$AsyncCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->showTabLayout(IZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

.field private final synthetic val$assistantType:I

.field private final synthetic val$doOpen:Z

.field private final synthetic val$initByconfig:Z


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;IZZ)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->val$assistantType:I

    iput-boolean p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->val$doOpen:Z

    iput-boolean p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->val$initByconfig:Z

    .line 885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doInBackground()Landroid/graphics/Bitmap;
    .locals 66

    .prologue
    .line 921
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->val$initByconfig:Z

    if-nez v4, :cond_7

    .line 922
    const/4 v8, 0x0

    .line 923
    .local v8, "data":[I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 924
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 925
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getExifInfo(I)Ljava/lang/String;

    move-result-object v57

    .line 926
    .local v57, "exifInfo":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 927
    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getExifInfo(I)Ljava/lang/String;

    move-result-object v58

    .line 928
    .local v58, "exifLocationCityInfo":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 929
    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getExifInfo(I)Ljava/lang/String;

    move-result-object v59

    .line 930
    .local v59, "exifLocationThroughfareInfo":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 931
    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getExifInfo(I)Ljava/lang/String;

    move-result-object v55

    .line 933
    .local v55, "exifCameraInfo":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    move-object/from16 v0, v57

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setExifInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v57

    .line 934
    if-eqz v58, :cond_0

    .line 935
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 936
    move-object/from16 v0, v58

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setExifLocationCityInfo(Ljava/lang/String;)V

    .line 937
    :cond_0
    if-eqz v59, :cond_1

    .line 938
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 939
    move-object/from16 v0, v59

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setExifLocationThroughfareInfo(Ljava/lang/String;)V

    .line 940
    :cond_1
    if-eqz v55, :cond_2

    .line 941
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    move-object/from16 v0, v55

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setExifCameraInfo(Ljava/lang/String;)V

    .line 943
    :cond_2
    if-nez v58, :cond_3

    .line 944
    if-nez v59, :cond_3

    .line 945
    const-string v58, "None"

    .line 946
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->changeLocationInfo()V

    .line 948
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->isCityMaster()Z

    move-result v51

    .line 949
    .local v51, "cityIsMaster":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 950
    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->isThroughfareMaster()Z

    move-result v65

    .line 953
    .local v65, "throughfareIsMaster":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 955
    const/4 v5, 0x1

    .line 954
    move-object/from16 v0, v55

    invoke-virtual {v4, v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v56

    .line 956
    .local v56, "exifCameraIso":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 958
    const/4 v5, 0x2

    .line 957
    move-object/from16 v0, v55

    invoke-virtual {v4, v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v54

    .line 959
    .local v54, "exifCameraExposure":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 961
    const/4 v5, 0x3

    .line 960
    move-object/from16 v0, v55

    invoke-virtual {v4, v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCameraInfoByParsing(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v53

    .line 962
    .local v53, "exifCameraAperture":Ljava/lang/String;
    if-nez v56, :cond_4

    .line 963
    const-string v56, "-"

    .line 964
    :cond_4
    if-nez v54, :cond_5

    .line 965
    const-string v54, "-"

    .line 966
    :cond_5
    if-nez v53, :cond_6

    .line 967
    const-string v53, "-"

    .line 968
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sj, SV - iso : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v56

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 969
    const-string v5, " / exp : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v54

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 970
    const-string v5, " / ape : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 968
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 973
    const-string v4, "sans-serif"

    .line 974
    const/4 v5, 0x1

    .line 973
    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v25

    .line 976
    .local v25, "bold":Landroid/graphics/Typeface;
    const-string v4, "sans-serif-condensed"

    const/4 v5, 0x1

    .line 975
    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v52

    .line 977
    .local v52, "condensedBold":Landroid/graphics/Typeface;
    const-string v4, "sans-serif-thin"

    .line 978
    const/4 v5, 0x0

    .line 977
    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v64

    .line 979
    .local v64, "thin":Landroid/graphics/Typeface;
    const-string v4, "sans-serif"

    .line 980
    const/4 v5, 0x0

    .line 979
    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v62

    .line 981
    .local v62, "regular":Landroid/graphics/Typeface;
    const-string v4, "sans-serif-light"

    .line 982
    const/4 v5, 0x0

    .line 981
    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v61

    .line 985
    .local v61, "light":Landroid/graphics/Typeface;
    const/16 v60, 0x1

    .local v60, "i":I
    :goto_0
    sget v4, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE1:I

    .line 986
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE2:I

    add-int/2addr v4, v5

    .line 985
    move/from16 v0, v60

    if-le v0, v4, :cond_8

    .line 5314
    .end local v8    # "data":[I
    .end local v25    # "bold":Landroid/graphics/Typeface;
    .end local v51    # "cityIsMaster":Z
    .end local v52    # "condensedBold":Landroid/graphics/Typeface;
    .end local v53    # "exifCameraAperture":Ljava/lang/String;
    .end local v54    # "exifCameraExposure":Ljava/lang/String;
    .end local v55    # "exifCameraInfo":Ljava/lang/String;
    .end local v56    # "exifCameraIso":Ljava/lang/String;
    .end local v57    # "exifInfo":Ljava/lang/String;
    .end local v58    # "exifLocationCityInfo":Ljava/lang/String;
    .end local v59    # "exifLocationThroughfareInfo":Ljava/lang/String;
    .end local v60    # "i":I
    .end local v61    # "light":Landroid/graphics/Typeface;
    .end local v62    # "regular":Landroid/graphics/Typeface;
    .end local v64    # "thin":Landroid/graphics/Typeface;
    .end local v65    # "throughfareIsMaster":Z
    :cond_7
    const/4 v4, 0x0

    return-object v4

    .line 987
    .restart local v8    # "data":[I
    .restart local v25    # "bold":Landroid/graphics/Typeface;
    .restart local v51    # "cityIsMaster":Z
    .restart local v52    # "condensedBold":Landroid/graphics/Typeface;
    .restart local v53    # "exifCameraAperture":Ljava/lang/String;
    .restart local v54    # "exifCameraExposure":Ljava/lang/String;
    .restart local v55    # "exifCameraInfo":Ljava/lang/String;
    .restart local v56    # "exifCameraIso":Ljava/lang/String;
    .restart local v57    # "exifInfo":Ljava/lang/String;
    .restart local v58    # "exifLocationCityInfo":Ljava/lang/String;
    .restart local v59    # "exifLocationThroughfareInfo":Ljava/lang/String;
    .restart local v60    # "i":I
    .restart local v61    # "light":Landroid/graphics/Typeface;
    .restart local v62    # "regular":Landroid/graphics/Typeface;
    .restart local v64    # "thin":Landroid/graphics/Typeface;
    .restart local v65    # "throughfareIsMaster":Z
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 988
    const/4 v5, 0x0

    move/from16 v0, v60

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampLayer(II)V

    .line 989
    packed-switch v60, :pswitch_data_0

    .line 5293
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5295
    const v5, 0x7f05003a

    .line 5294
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 5296
    .local v9, "imageWidth":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5298
    const v5, 0x7f05003a

    .line 5297
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 5300
    .local v10, "imageHeight":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 5301
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v4

    .line 5302
    const/4 v5, 0x0

    .line 5303
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getImageEditViewWidth()I
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)I

    move-result v7

    move/from16 v6, v60

    .line 5302
    invoke-virtual/range {v4 .. v10}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->makeStampThumbnail(ZII[III)V

    .line 5307
    :cond_9
    const/4 v8, 0x0

    .line 986
    add-int/lit8 v60, v60, 0x1

    goto :goto_0

    .line 991
    .end local v9    # "imageWidth":I
    .end local v10    # "imageHeight":I
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 993
    const/4 v5, 0x0

    .line 994
    const v6, 0x7f02018a

    .line 995
    const/4 v7, 0x1

    .line 996
    const/4 v8, 0x1

    .line 997
    const/16 v9, 0x30

    .line 998
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .end local v8    # "data":[I
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 1000
    const v12, 0x7f050043

    .line 999
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 1001
    const/4 v11, -0x1

    .line 1002
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 1004
    const v13, 0x7f050041

    .line 1003
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 1005
    const/4 v13, 0x0

    .line 1006
    const/4 v14, 0x1

    .line 1007
    const/4 v15, 0x0

    .line 1008
    const/16 v16, 0xa

    .line 1009
    const/16 v17, 0xe

    .line 1010
    const/16 v18, 0x0

    .line 1011
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 1013
    const v20, 0x7f050045

    .line 1012
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 1014
    const/16 v20, 0x0

    .line 1015
    const/16 v21, 0x0

    .line 1016
    const/16 v22, 0x0

    .line 1017
    const/16 v23, 0x0

    .line 1018
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 1020
    const v26, 0x7f04000e

    .line 1019
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 1022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1023
    move-object/from16 v0, v26

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 1024
    const/16 v27, 0x0

    const/16 v28, 0x0

    .line 992
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1026
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1028
    const/4 v5, 0x0

    .line 1029
    const/4 v6, 0x1

    .line 1030
    const/4 v7, 0x2

    .line 1031
    const/4 v8, 0x1

    .line 1032
    const/16 v9, 0x50

    .line 1033
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 1035
    const v12, 0x7f050044

    .line 1034
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 1036
    const/4 v11, -0x1

    .line 1037
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 1039
    const v13, 0x7f050042

    .line 1038
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 1040
    const/4 v13, 0x0

    .line 1041
    const/4 v14, 0x1

    .line 1042
    const/4 v15, 0x0

    .line 1043
    const/16 v16, 0xc

    .line 1044
    const/16 v17, 0xe

    .line 1045
    const/16 v18, 0x0

    .line 1046
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 1048
    const v20, 0x7f050046

    .line 1047
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 1049
    const/16 v20, 0x0

    .line 1050
    const/16 v21, 0x0

    .line 1051
    const/16 v22, 0x0

    .line 1052
    const/16 v23, 0x0

    .line 1053
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 1055
    const v26, 0x7f04000e

    .line 1054
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 1057
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1059
    const/16 v27, -0x1

    .line 1058
    move-object/from16 v0, v26

    move-object/from16 v1, v57

    move/from16 v2, v60

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v26

    .line 1060
    const/16 v27, 0x0

    const/16 v28, 0x1

    .line 1027
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 1061
    .restart local v8    # "data":[I
    goto/16 :goto_1

    .line 1063
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1065
    const/16 v27, 0x0

    .line 1066
    const/16 v28, 0x1

    .line 1067
    const/16 v29, 0x1

    .line 1068
    const/16 v30, 0x1

    .line 1069
    const/16 v31, 0x30

    .line 1070
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1072
    const v5, 0x7f050049

    .line 1071
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1073
    const/16 v33, -0x1

    .line 1074
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1076
    const v5, 0x7f050047

    .line 1075
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1077
    const/16 v35, 0x0

    .line 1078
    const/16 v36, 0x1

    .line 1079
    const/16 v37, 0x0

    .line 1080
    const/16 v38, 0xd

    .line 1081
    const/16 v39, 0x0

    .line 1082
    const/16 v40, 0x0

    .line 1083
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1085
    const v5, 0x7f05004b

    .line 1084
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1086
    const/16 v42, 0x0

    .line 1087
    const/16 v43, 0x0

    .line 1088
    const/16 v44, 0x0

    .line 1089
    const/16 v45, 0x0

    .line 1090
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1092
    const v5, 0x7f04000f

    .line 1091
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1093
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 1094
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1095
    move-object/from16 v0, v57

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 1096
    const/16 v49, 0x0

    const/16 v50, 0x0

    .line 1064
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1098
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1100
    const/16 v27, 0x0

    .line 1101
    const/16 v28, 0x0

    .line 1102
    const/16 v29, 0x1

    .line 1103
    const/16 v30, 0x1

    .line 1104
    const/16 v31, 0x50

    .line 1105
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1107
    const v5, 0x7f05004a

    .line 1106
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1108
    const/16 v33, -0x1

    .line 1109
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1111
    const v5, 0x7f050048

    .line 1110
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1112
    const/16 v35, 0x0

    .line 1113
    const/16 v36, 0x1

    .line 1114
    const/16 v37, 0x0

    .line 1115
    const/16 v38, 0xd

    .line 1116
    const/16 v39, 0x0

    .line 1117
    const/16 v40, 0x0

    .line 1118
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1120
    const v5, 0x7f05004c

    .line 1119
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1121
    const/16 v42, 0x0

    .line 1122
    const/16 v43, 0x0

    .line 1123
    const/16 v44, 0x0

    .line 1124
    const/16 v45, 0x0

    .line 1125
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1127
    const v5, 0x7f04000f

    .line 1126
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1128
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 1129
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1131
    const/4 v5, -0x1

    .line 1130
    move-object/from16 v0, v57

    move/from16 v1, v60

    invoke-virtual {v4, v0, v1, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 1132
    const/16 v49, 0x0

    const/16 v50, 0x1

    .line 1099
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 1133
    const-string v4, "kbr : thumbnuail 2 "

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1136
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1138
    const/16 v27, 0x0

    .line 1139
    const/16 v28, 0x1

    .line 1140
    const/16 v29, 0x1

    .line 1141
    const/16 v30, 0x1

    .line 1142
    const/16 v31, 0x30

    .line 1143
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1145
    const v5, 0x7f050051

    .line 1144
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1146
    const/16 v33, -0x1

    .line 1147
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1149
    const v5, 0x7f05004d

    .line 1148
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1150
    const/16 v35, 0x0

    .line 1151
    const/16 v36, 0x1

    .line 1152
    const/16 v37, 0x0

    .line 1153
    const/16 v38, 0xa

    .line 1154
    const/16 v39, 0xe

    .line 1155
    const/16 v40, 0x0

    .line 1156
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1158
    const v5, 0x7f050055

    .line 1157
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1159
    const/16 v42, 0x0

    .line 1160
    const/16 v43, 0x0

    .line 1161
    const/16 v44, 0x0

    .line 1162
    const/16 v45, 0x0

    .line 1163
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1165
    const v5, 0x7f04000e

    .line 1164
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1166
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 1167
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1169
    const/4 v5, 0x1

    .line 1168
    move-object/from16 v0, v57

    move/from16 v1, v60

    invoke-virtual {v4, v0, v1, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 1170
    const/16 v49, 0x0

    const/16 v50, 0x0

    .line 1137
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1172
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1174
    const/16 v27, 0x0

    .line 1175
    const/16 v28, 0x1

    .line 1176
    const/16 v29, 0x2

    .line 1177
    const/16 v30, 0x1

    .line 1178
    const/16 v31, 0x30

    .line 1179
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1181
    const v5, 0x7f050052

    .line 1180
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1182
    const/16 v33, -0x1

    .line 1183
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1185
    const v5, 0x7f05004e

    .line 1184
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1186
    const/16 v35, 0x0

    .line 1187
    const/16 v36, 0x1

    .line 1188
    const/16 v37, 0x0

    .line 1189
    const/16 v38, 0xd

    .line 1190
    const/16 v39, 0xe

    .line 1191
    const/16 v40, 0x0

    .line 1192
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1194
    const v5, 0x7f050056

    .line 1193
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1195
    const/16 v42, 0x0

    .line 1196
    const/16 v43, 0x0

    .line 1197
    const/16 v44, 0x0

    .line 1198
    const/16 v45, 0x0

    .line 1199
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1201
    const v5, 0x7f04000e

    .line 1200
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1203
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1205
    const/4 v5, 0x2

    .line 1204
    move-object/from16 v0, v57

    move/from16 v1, v60

    invoke-virtual {v4, v0, v1, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 1206
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v47, v52

    .line 1173
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1208
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1210
    const/16 v27, 0x0

    .line 1211
    const/16 v28, 0x0

    .line 1212
    const/16 v29, 0x2

    .line 1213
    const/16 v30, 0x1

    .line 1214
    const/16 v31, 0x50

    .line 1215
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1217
    const v5, 0x7f050053

    .line 1216
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1218
    const/16 v33, -0x1

    .line 1219
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1221
    const v5, 0x7f05004f

    .line 1220
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1222
    const/16 v35, 0x0

    .line 1223
    const/16 v36, 0x1

    .line 1224
    const/16 v37, 0x0

    .line 1225
    const/16 v38, 0xd

    .line 1226
    const/16 v39, 0xe

    .line 1227
    const/16 v40, 0x0

    .line 1228
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1230
    const v5, 0x7f050057

    .line 1229
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1231
    const/16 v42, 0x0

    .line 1232
    const/16 v43, 0x0

    .line 1233
    const/16 v44, 0x0

    .line 1234
    const/16 v45, 0x0

    .line 1235
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1237
    const v5, 0x7f04000e

    .line 1236
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1239
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1241
    const/4 v5, 0x3

    .line 1240
    move-object/from16 v0, v57

    move/from16 v1, v60

    invoke-virtual {v4, v0, v1, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 1242
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v47, v52

    .line 1209
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1244
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1246
    const/16 v27, 0x0

    .line 1247
    const/16 v28, 0x1

    .line 1248
    const/16 v29, 0x3

    .line 1249
    const/16 v30, 0x1

    .line 1250
    const/16 v31, 0x50

    .line 1251
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1253
    const v5, 0x7f050054

    .line 1252
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1254
    const/16 v33, -0x1

    .line 1255
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1257
    const v5, 0x7f050050

    .line 1256
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1258
    const/16 v35, 0x0

    .line 1259
    const/16 v36, 0x1

    .line 1260
    const/16 v37, 0x0

    .line 1261
    const/16 v38, 0xc

    .line 1262
    const/16 v39, 0xe

    .line 1263
    const/16 v40, 0x0

    .line 1264
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1266
    const v5, 0x7f050058

    .line 1265
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1267
    const/16 v42, 0x0

    .line 1268
    const/16 v43, 0x0

    .line 1269
    const/16 v44, 0x0

    .line 1270
    const/16 v45, 0x0

    .line 1271
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1273
    const v5, 0x7f04000e

    .line 1272
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1274
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 1275
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1276
    move-object/from16 v0, v57

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 1277
    const/16 v49, 0x0

    const/16 v50, 0x1

    .line 1245
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 1278
    const-string v4, "kbr : thumbnuail 3 "

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1283
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1285
    const/16 v27, 0x0

    .line 1286
    const v28, 0x7f02018c

    .line 1287
    const/16 v29, 0x1

    .line 1288
    const/16 v30, 0x1

    .line 1289
    const/16 v31, 0x30

    .line 1290
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1292
    const v5, 0x7f05005d

    .line 1291
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1293
    const/16 v33, -0x1

    .line 1294
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1296
    const v5, 0x7f050059

    .line 1295
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1297
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1299
    const v5, 0x7f05005a

    .line 1298
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 1300
    const/16 v36, 0x1

    .line 1301
    const/16 v37, 0x0

    .line 1302
    const/16 v38, 0xd

    .line 1303
    const/16 v39, 0xe

    .line 1304
    const/16 v40, 0x0

    .line 1305
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1307
    const v5, 0x7f050061

    .line 1306
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1308
    const/16 v42, 0x0

    .line 1309
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1311
    const v5, 0x7f050065

    .line 1310
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v43

    .line 1312
    const v44, 0x7f02032e

    .line 1313
    const/16 v45, 0xe

    .line 1314
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1316
    const v5, 0x7f040010

    .line 1315
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1318
    const/16 v50, 0x0

    move-object/from16 v47, v64

    move-object/from16 v48, v58

    move/from16 v49, v51

    .line 1284
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1320
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1322
    const/16 v27, 0x0

    .line 1323
    const/16 v28, 0x0

    .line 1324
    const/16 v29, 0x1

    .line 1325
    const/16 v30, 0x1

    .line 1326
    const/16 v31, 0x30

    .line 1327
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1329
    const v5, 0x7f05005e

    .line 1328
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1330
    const/16 v33, -0x1

    .line 1331
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1333
    const v5, 0x7f05005a

    .line 1332
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1334
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1336
    const v5, 0x7f050059

    .line 1335
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 1337
    const/16 v36, 0x1

    .line 1338
    const/16 v37, 0x0

    .line 1339
    const/16 v38, 0xd

    .line 1340
    const/16 v39, 0xe

    .line 1341
    const/16 v40, 0x0

    .line 1342
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1344
    const v5, 0x7f050062

    .line 1343
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1345
    const/16 v42, 0x0

    .line 1346
    const/16 v43, 0x0

    .line 1347
    const/16 v44, 0x0

    .line 1348
    const/16 v45, 0x0

    .line 1349
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1351
    const v5, 0x7f040010

    .line 1350
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1354
    const/16 v50, 0x0

    move-object/from16 v47, v64

    move-object/from16 v48, v59

    move/from16 v49, v65

    .line 1321
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1356
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1358
    const/16 v27, 0x0

    .line 1359
    const/16 v28, 0x0

    .line 1360
    const/16 v29, 0x1

    .line 1361
    const/16 v30, 0x1

    .line 1362
    const/16 v31, 0x50

    .line 1363
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1365
    const v5, 0x7f05005f

    .line 1364
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1366
    const/16 v33, -0x1

    .line 1367
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1369
    const v5, 0x7f05005b

    .line 1368
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1370
    const/16 v35, 0x0

    .line 1371
    const/16 v36, 0x1

    .line 1372
    const/16 v37, 0x0

    .line 1373
    const/16 v38, 0xd

    .line 1374
    const/16 v39, 0xe

    .line 1375
    const/16 v40, 0x0

    .line 1376
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1378
    const v5, 0x7f050063

    .line 1377
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1379
    const/16 v42, 0x0

    .line 1380
    const/16 v43, 0x0

    .line 1381
    const/16 v44, 0x0

    .line 1382
    const/16 v45, 0x0

    .line 1383
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1385
    const v5, 0x7f040010

    .line 1384
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1387
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1389
    const/4 v5, 0x4

    const/4 v6, 0x0

    .line 1388
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 1390
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v47, v64

    .line 1357
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1392
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1394
    const/16 v27, 0x0

    .line 1395
    const/16 v28, 0x0

    .line 1396
    const/16 v29, 0x1

    .line 1397
    const/16 v30, 0x1

    .line 1398
    const/16 v31, 0x50

    .line 1399
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1401
    const v5, 0x7f050060

    .line 1400
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1402
    const/16 v33, -0x1

    .line 1403
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1405
    const v5, 0x7f05005c

    .line 1404
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1406
    const/16 v35, 0x0

    .line 1407
    const/16 v36, 0x1

    .line 1408
    const/16 v37, 0x0

    .line 1409
    const/16 v38, 0xd

    .line 1410
    const/16 v39, 0xe

    .line 1411
    const/16 v40, 0x0

    .line 1412
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1414
    const v5, 0x7f050064

    .line 1413
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1415
    const/16 v42, 0x0

    .line 1416
    const/16 v43, 0x0

    .line 1417
    const/16 v44, 0x0

    .line 1418
    const/16 v45, 0x0

    .line 1419
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1421
    const v5, 0x7f040010

    .line 1420
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1423
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1424
    move-object/from16 v0, v57

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 1425
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v47, v64

    .line 1393
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 1426
    goto/16 :goto_1

    .line 1429
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1431
    const/4 v5, 0x0

    .line 1432
    const v6, 0x7f02018e

    .line 1433
    const/4 v7, 0x1

    .line 1434
    const/4 v8, 0x1

    .line 1435
    const/16 v9, 0x30

    .line 1436
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .end local v8    # "data":[I
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 1438
    const v12, 0x7f05006c

    .line 1437
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 1439
    const/4 v11, -0x1

    .line 1440
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 1442
    const v13, 0x7f050066

    .line 1441
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 1443
    const/4 v13, 0x0

    .line 1444
    const/4 v14, 0x1

    .line 1445
    const/4 v15, 0x0

    .line 1446
    const/16 v16, 0xd

    .line 1447
    const/16 v17, 0xe

    .line 1448
    const/16 v18, 0x0

    .line 1449
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 1451
    const v20, 0x7f050070

    .line 1450
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 1452
    const/16 v20, 0x0

    .line 1453
    const/16 v21, 0x0

    .line 1454
    const/16 v22, 0x0

    .line 1455
    const/16 v23, 0x0

    .line 1456
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 1458
    const v26, 0x7f040010

    .line 1457
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 1460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1461
    move-object/from16 v0, v26

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 1462
    const/16 v27, 0x0

    const/16 v28, 0x0

    .line 1430
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1464
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1466
    const/16 v27, 0x0

    .line 1467
    const/16 v28, 0x0

    .line 1468
    const/16 v29, 0x1

    .line 1469
    const/16 v30, 0x1

    .line 1470
    const/16 v31, 0x30

    .line 1471
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1473
    const v5, 0x7f05006d

    .line 1472
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1474
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1476
    const v5, 0x7f050067

    .line 1475
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 1477
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1479
    const v5, 0x7f050068

    .line 1478
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1480
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1482
    const v5, 0x7f05006a

    .line 1481
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 1483
    const/16 v36, 0x1

    .line 1484
    const/16 v37, 0x0

    .line 1485
    const/16 v38, 0xd

    .line 1486
    const/16 v39, 0xe

    .line 1487
    const/16 v40, 0x0

    .line 1488
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1490
    const v5, 0x7f050071

    .line 1489
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1491
    const/16 v42, 0x0

    .line 1492
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1494
    const v5, 0x7f050074

    .line 1493
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v43

    .line 1495
    const v44, 0x7f02032e

    .line 1496
    const/16 v45, 0xe

    .line 1497
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1499
    const v5, 0x7f040010

    .line 1498
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1501
    const/16 v50, 0x0

    move-object/from16 v47, v64

    move-object/from16 v48, v58

    move/from16 v49, v51

    .line 1465
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1503
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1505
    const/16 v27, 0x0

    .line 1506
    const/16 v28, 0x0

    .line 1507
    const/16 v29, 0x1

    .line 1508
    const/16 v30, 0x1

    .line 1509
    const/16 v31, 0x50

    .line 1510
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1512
    const v5, 0x7f05006e

    .line 1511
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1513
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1515
    const v5, 0x7f050069

    .line 1514
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 1516
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1518
    const v5, 0x7f05006a

    .line 1517
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1519
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1521
    const v5, 0x7f050068

    .line 1520
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 1522
    const/16 v36, 0x1

    .line 1523
    const/16 v37, 0x0

    .line 1524
    const/16 v38, 0xd

    .line 1525
    const/16 v39, 0xe

    .line 1526
    const/16 v40, 0x0

    .line 1527
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1529
    const v5, 0x7f050072

    .line 1528
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1530
    const/16 v42, 0x0

    .line 1531
    const/16 v43, 0x0

    .line 1532
    const/16 v44, 0x0

    .line 1533
    const/16 v45, 0x0

    .line 1534
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1536
    const v5, 0x7f040010

    .line 1535
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1539
    const/16 v50, 0x0

    move-object/from16 v47, v64

    move-object/from16 v48, v59

    move/from16 v49, v65

    .line 1504
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1541
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1543
    const/16 v27, 0x0

    .line 1544
    const/16 v28, 0x0

    .line 1545
    const/16 v29, 0x1

    .line 1546
    const/16 v30, 0x1

    .line 1547
    const/16 v31, 0x50

    .line 1548
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1550
    const v5, 0x7f05006f

    .line 1549
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1551
    const/16 v33, -0x1

    .line 1552
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1554
    const v5, 0x7f05006b

    .line 1553
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1555
    const/16 v35, 0x0

    .line 1556
    const/16 v36, 0x1

    .line 1557
    const/16 v37, 0x0

    .line 1558
    const/16 v38, 0xd

    .line 1559
    const/16 v39, 0xe

    .line 1560
    const/16 v40, 0x0

    .line 1561
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1563
    const v5, 0x7f050073

    .line 1562
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1564
    const/16 v42, 0x0

    .line 1565
    const/16 v43, 0x0

    .line 1566
    const/16 v44, 0x0

    .line 1567
    const/16 v45, 0x0

    .line 1568
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1570
    const v5, 0x7f040010

    .line 1569
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1571
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 1573
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1575
    const/4 v5, 0x5

    const/4 v6, 0x0

    .line 1574
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 1576
    const/16 v49, 0x0

    const/16 v50, 0x1

    .line 1542
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 1577
    .restart local v8    # "data":[I
    const-string v4, "kbr : thumbnuail 5 "

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1580
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1582
    const/4 v5, 0x0

    .line 1583
    const v6, 0x7f020190

    .line 1584
    const/4 v7, 0x1

    .line 1585
    const/4 v8, 0x1

    .line 1586
    const/16 v9, 0x50

    .line 1587
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .end local v8    # "data":[I
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 1589
    const v12, 0x7f05007b

    .line 1588
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 1590
    const/4 v11, -0x1

    .line 1591
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 1593
    const v13, 0x7f050077

    .line 1592
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 1594
    const/4 v13, 0x0

    .line 1595
    const/4 v14, 0x1

    .line 1596
    const/4 v15, 0x0

    .line 1597
    const/16 v16, 0xc

    .line 1598
    const/16 v17, 0xe

    .line 1599
    const/16 v18, 0x0

    .line 1600
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 1602
    const v20, 0x7f05007f

    .line 1601
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 1603
    const/16 v20, 0x0

    .line 1604
    const/16 v21, 0x0

    .line 1605
    const/16 v22, 0x0

    .line 1606
    const/16 v23, 0x0

    .line 1607
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 1609
    const v26, 0x7f04000e

    .line 1608
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 1611
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1612
    move-object/from16 v0, v26

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 1613
    const/16 v27, 0x0

    const/16 v28, 0x0

    .line 1581
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1615
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1617
    const/4 v5, 0x0

    .line 1618
    const/4 v6, 0x0

    .line 1619
    const/4 v7, 0x1

    .line 1620
    const/4 v8, 0x1

    .line 1621
    const/16 v9, 0x50

    .line 1622
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 1624
    const v12, 0x7f05007c

    .line 1623
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 1625
    const/4 v11, -0x1

    .line 1626
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 1628
    const v13, 0x7f050078

    .line 1627
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 1629
    const/4 v13, 0x0

    .line 1630
    const/4 v14, 0x1

    .line 1631
    const/4 v15, 0x0

    .line 1632
    const/16 v16, 0xc

    .line 1633
    const/16 v17, 0xe

    .line 1634
    const/16 v18, 0x0

    .line 1635
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 1637
    const v20, 0x7f050080

    .line 1636
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 1638
    const/16 v20, 0x0

    .line 1639
    const/16 v21, 0x0

    .line 1640
    const/16 v22, 0x0

    .line 1641
    const/16 v23, 0x0

    .line 1642
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 1644
    const v26, 0x7f04000e

    .line 1643
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 1646
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1648
    const/16 v27, 0x6

    const/16 v28, 0x0

    .line 1647
    move-object/from16 v0, v26

    move-object/from16 v1, v57

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v26

    .line 1649
    const/16 v27, 0x0

    const/16 v28, 0x0

    .line 1616
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1651
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1653
    const/16 v27, 0x0

    .line 1654
    const/16 v28, 0x1

    .line 1655
    const/16 v29, 0x2

    .line 1656
    const/16 v30, 0x1

    .line 1657
    const/16 v31, 0x30

    .line 1658
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1660
    const v5, 0x7f050079

    .line 1659
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1661
    const/16 v33, -0x1

    .line 1662
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1664
    const v5, 0x7f050075

    .line 1663
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1665
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1667
    const v5, 0x7f050076

    .line 1666
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 1668
    const/16 v36, 0x1

    .line 1669
    const/16 v37, 0x0

    .line 1670
    const/16 v38, 0xd

    .line 1671
    const/16 v39, 0xe

    .line 1672
    const/16 v40, 0x0

    .line 1673
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1675
    const v5, 0x7f05007d

    .line 1674
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1676
    const/16 v42, 0x0

    .line 1677
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1679
    const v5, 0x7f050081

    .line 1678
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v43

    .line 1680
    const v44, 0x7f02032e

    .line 1681
    const/16 v45, 0xe

    .line 1682
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1684
    const v5, 0x7f04000e

    .line 1683
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1685
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 1687
    const/16 v50, 0x0

    move-object/from16 v48, v58

    move/from16 v49, v51

    .line 1652
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1689
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1691
    const/16 v27, 0x0

    .line 1692
    const/16 v28, 0x0

    .line 1693
    const/16 v29, 0x2

    .line 1694
    const/16 v30, 0x1

    .line 1695
    const/16 v31, 0x30

    .line 1696
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1698
    const v5, 0x7f05007a

    .line 1697
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1699
    const/16 v33, -0x1

    .line 1700
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1702
    const v5, 0x7f050076

    .line 1701
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1703
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1705
    const v5, 0x7f050075

    .line 1704
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 1706
    const/16 v36, 0x1

    .line 1707
    const/16 v37, 0x0

    .line 1708
    const/16 v38, 0xd

    .line 1709
    const/16 v39, 0xe

    .line 1710
    const/16 v40, 0x0

    .line 1711
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1713
    const v5, 0x7f05007e

    .line 1712
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1714
    const/16 v42, 0x0

    .line 1715
    const/16 v43, 0x0

    .line 1716
    const/16 v44, 0x0

    .line 1717
    const/16 v45, 0x0

    .line 1718
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1720
    const v5, 0x7f04000e

    .line 1719
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1721
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 1723
    const/16 v50, 0x1

    move-object/from16 v48, v59

    move/from16 v49, v65

    .line 1690
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 1724
    .restart local v8    # "data":[I
    goto/16 :goto_1

    .line 1726
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1728
    const/16 v27, 0x0

    .line 1729
    const/16 v28, 0x1

    .line 1730
    const/16 v29, 0x1

    .line 1731
    const/16 v30, 0x5

    .line 1732
    const/16 v31, 0x50

    .line 1733
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1735
    const v5, 0x7f050083

    .line 1734
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1736
    const/16 v33, -0x1

    .line 1737
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1739
    const v5, 0x7f050082

    .line 1738
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1740
    const/16 v35, 0x0

    .line 1741
    const/16 v36, 0x5

    .line 1742
    const/16 v37, 0x0

    .line 1743
    const/16 v38, 0xc

    .line 1744
    const/16 v39, 0xb

    .line 1745
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1747
    const v5, 0x7f050086

    .line 1746
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 1748
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1750
    const v5, 0x7f050085

    .line 1749
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1751
    const/16 v42, 0x0

    .line 1752
    const/16 v43, 0x0

    .line 1753
    const/16 v44, 0x0

    .line 1754
    const/16 v45, 0x0

    .line 1755
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1757
    const v5, 0x7f040011

    .line 1756
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1759
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1761
    const/4 v5, 0x7

    const/4 v6, 0x0

    .line 1760
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 1762
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v47, v62

    .line 1727
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 1763
    goto/16 :goto_1

    .line 1766
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1768
    const/4 v5, 0x0

    .line 1769
    const v6, 0x7f020176

    .line 1770
    const/4 v7, 0x1

    .line 1771
    const/4 v8, 0x3

    .line 1772
    const/16 v9, 0x30

    .line 1773
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .end local v8    # "data":[I
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 1775
    const v12, 0x7f05008d

    .line 1774
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 1776
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 1778
    const v12, 0x7f050087

    .line 1777
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 1779
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 1781
    const v13, 0x7f050088

    .line 1780
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 1782
    const/4 v13, 0x0

    .line 1783
    const/4 v14, 0x1

    .line 1784
    const/4 v15, 0x0

    .line 1785
    const/16 v16, 0xc

    .line 1786
    const/16 v17, 0xe

    .line 1787
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 1789
    const v19, 0x7f050092

    .line 1788
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 1790
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 1792
    const v20, 0x7f050091

    .line 1791
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 1793
    const/16 v20, 0x0

    .line 1794
    const/16 v21, 0x0

    .line 1795
    const/16 v22, 0x0

    .line 1796
    const/16 v23, 0x0

    .line 1797
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 1799
    const v26, 0x7f040012

    .line 1798
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 1801
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1803
    const/16 v27, 0x8

    const/16 v28, 0x1

    .line 1802
    move-object/from16 v0, v26

    move-object/from16 v1, v57

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v26

    .line 1804
    const/16 v27, 0x0

    const/16 v28, 0x0

    .line 1767
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1806
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1808
    const/16 v27, 0x0

    .line 1809
    const/16 v28, 0x0

    .line 1810
    const/16 v29, 0x1

    .line 1811
    const/16 v30, 0x3

    .line 1812
    const/16 v31, 0x30

    .line 1813
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1815
    const v5, 0x7f05008e

    .line 1814
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1816
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1818
    const v5, 0x7f050087

    .line 1817
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 1819
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1821
    const v5, 0x7f050089

    .line 1820
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1822
    const/16 v35, 0x0

    .line 1823
    const/16 v36, 0x1

    .line 1824
    const/16 v37, 0x0

    .line 1825
    const/16 v38, 0xc

    .line 1826
    const/16 v39, 0xe

    .line 1827
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1829
    const v5, 0x7f050092

    .line 1828
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 1830
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1832
    const v5, 0x7f050093

    .line 1831
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1833
    const/16 v42, 0x0

    .line 1834
    const/16 v43, 0x0

    .line 1835
    const/16 v44, 0x0

    .line 1836
    const/16 v45, 0x0

    .line 1837
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1839
    const v5, 0x7f040012

    .line 1838
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1841
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1843
    const/16 v5, 0x8

    const/4 v6, 0x2

    .line 1842
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 1844
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v47, v62

    .line 1807
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1846
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1848
    const/16 v27, 0x0

    .line 1849
    const/16 v28, 0x0

    .line 1850
    const/16 v29, 0x1

    .line 1851
    const/16 v30, 0x3

    .line 1852
    const/16 v31, 0x30

    .line 1853
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1855
    const v5, 0x7f05008f

    .line 1854
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1856
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1858
    const v5, 0x7f050087

    .line 1857
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 1859
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1861
    const v5, 0x7f05008a

    .line 1860
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1862
    const/16 v35, 0x0

    .line 1863
    const/16 v36, 0x1

    .line 1864
    const/16 v37, 0x0

    .line 1865
    const/16 v38, 0xc

    .line 1866
    const/16 v39, 0xe

    .line 1867
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1869
    const v5, 0x7f050092

    .line 1868
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 1870
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1872
    const v5, 0x7f050094

    .line 1871
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1873
    const/16 v42, 0x0

    .line 1874
    const/16 v43, 0x0

    .line 1875
    const/16 v44, 0x0

    .line 1876
    const/16 v45, 0x0

    .line 1877
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1879
    const v5, 0x7f040012

    .line 1878
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1880
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 1881
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1882
    move-object/from16 v0, v57

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 1883
    const/16 v49, 0x0

    const/16 v50, 0x1

    .line 1847
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 1922
    .restart local v8    # "data":[I
    goto/16 :goto_1

    .line 1924
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1925
    const/16 v5, 0x9

    const/4 v6, 0x2

    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v63

    .line 1928
    .local v63, "temp_str":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1930
    const/4 v5, 0x0

    .line 1931
    const v6, 0x7f020178

    .line 1932
    const/4 v7, 0x1

    .line 1933
    const/4 v8, 0x1

    .line 1934
    const/16 v9, 0x30

    .line 1935
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .end local v8    # "data":[I
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 1937
    const v12, 0x7f05009d

    .line 1936
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 1938
    const/4 v11, -0x1

    .line 1939
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 1941
    const v13, 0x7f050099

    .line 1940
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 1942
    const/4 v13, 0x0

    .line 1943
    const/4 v14, 0x1

    .line 1944
    const/4 v15, 0x0

    .line 1945
    const/16 v16, 0xc

    .line 1946
    const/16 v17, 0xe

    .line 1947
    const/16 v18, 0x0

    .line 1948
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 1950
    const v20, 0x7f0500a1

    .line 1949
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 1951
    const/16 v20, 0x0

    .line 1952
    const/16 v21, 0x0

    .line 1953
    const/16 v22, 0x0

    .line 1954
    const/16 v23, 0x0

    .line 1955
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 1957
    const v26, 0x7f040013

    .line 1956
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 1959
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1961
    const/16 v27, 0x9

    const/16 v28, 0x1

    .line 1960
    move-object/from16 v0, v26

    move-object/from16 v1, v57

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v26

    .line 1962
    const/16 v27, 0x0

    const/16 v28, 0x0

    .line 1929
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 1964
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 1966
    const/16 v27, 0x0

    .line 1967
    const/16 v28, 0x0

    .line 1968
    const/16 v29, 0x1

    .line 1969
    const/16 v30, 0x5

    .line 1970
    const/16 v31, 0x30

    .line 1971
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1973
    const v5, 0x7f05009e

    .line 1972
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 1974
    const/16 v33, -0x1

    .line 1975
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1977
    const v5, 0x7f05009a

    .line 1976
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 1978
    const/16 v35, 0x0

    .line 1979
    const/16 v36, 0x1

    .line 1980
    const/16 v37, 0x0

    .line 1981
    const/16 v38, 0xc

    .line 1982
    const/16 v39, 0xe

    .line 1984
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 1985
    move-object/from16 v0, v63

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->arrangeMonth(Ljava/lang/String;)I

    move-result v40

    .line 1988
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1990
    const v5, 0x7f0500a2

    .line 1989
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 1991
    const/16 v42, 0x0

    .line 1992
    const/16 v43, 0x0

    .line 1993
    const/16 v44, 0x0

    .line 1994
    const/16 v45, 0x0

    .line 1995
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1997
    const v5, 0x7f040013

    .line 1996
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 1999
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2001
    const/16 v5, 0x9

    const/4 v6, 0x2

    .line 2000
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 2002
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v47, v62

    .line 1965
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2004
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2006
    const/16 v27, 0x0

    .line 2007
    const/16 v28, 0x0

    .line 2008
    const/16 v29, 0x1

    .line 2009
    const/16 v30, 0x3

    .line 2010
    const/16 v31, 0x30

    .line 2011
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2013
    const v5, 0x7f05009f

    .line 2012
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2014
    const/16 v33, -0x1

    .line 2015
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2017
    const v5, 0x7f05009b

    .line 2016
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2018
    const/16 v35, 0x0

    .line 2019
    const/16 v36, 0x1

    .line 2020
    const/16 v37, 0x0

    .line 2021
    const/16 v38, 0xc

    .line 2022
    const/16 v39, 0xe

    .line 2023
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2025
    const v5, 0x7f0500a5

    .line 2024
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2026
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2028
    const v5, 0x7f0500a4

    .line 2027
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2029
    const/16 v42, 0x0

    .line 2030
    const/16 v43, 0x0

    .line 2031
    const/16 v44, 0x0

    .line 2032
    const/16 v45, 0x0

    .line 2033
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2035
    const v5, 0x7f040013

    .line 2034
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2037
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2039
    const/16 v5, 0x9

    const/4 v6, 0x3

    .line 2038
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 2040
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v47, v62

    .line 2005
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2042
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2044
    const/16 v27, 0x0

    .line 2045
    const/16 v28, 0x0

    .line 2046
    const/16 v29, 0x1

    .line 2047
    const/16 v30, 0x1

    .line 2048
    const/16 v31, 0x30

    .line 2049
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2051
    const v5, 0x7f0500a0

    .line 2050
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2052
    const/16 v33, -0x1

    .line 2053
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2055
    const v5, 0x7f05009c

    .line 2054
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2056
    const/16 v35, 0x0

    .line 2057
    const/16 v36, 0x1

    .line 2058
    const/16 v37, 0x0

    .line 2059
    const/16 v38, 0xc

    .line 2060
    const/16 v39, 0xe

    .line 2061
    const/16 v40, 0x0

    .line 2062
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2064
    const v5, 0x7f0500a6

    .line 2063
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2065
    const/16 v42, 0x0

    .line 2066
    const/16 v43, 0x0

    .line 2067
    const/16 v44, 0x0

    .line 2068
    const/16 v45, 0x0

    .line 2069
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2071
    const v5, 0x7f040014

    .line 2070
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2072
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2073
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2074
    move-object/from16 v0, v57

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 2075
    const/16 v49, 0x0

    const/16 v50, 0x1

    .line 2043
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 2076
    .restart local v8    # "data":[I
    goto/16 :goto_1

    .line 2078
    .end local v63    # "temp_str":Ljava/lang/String;
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2080
    const/16 v27, 0x0

    .line 2081
    const v28, 0x7f02017a

    .line 2082
    const/16 v29, 0x1

    .line 2083
    const/16 v30, 0x3

    .line 2084
    const/16 v31, 0x30

    .line 2085
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2087
    const v5, 0x7f0500b4

    .line 2086
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2088
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2090
    const v5, 0x7f0500ae

    .line 2089
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2091
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2093
    const v5, 0x7f0500af

    .line 2092
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2094
    const/16 v35, 0x0

    .line 2095
    const/16 v36, 0x1

    .line 2096
    const/16 v37, 0x0

    .line 2097
    const/16 v38, 0xc

    .line 2098
    const/16 v39, 0x9

    .line 2099
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2101
    const v5, 0x7f0500b8

    .line 2100
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2102
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2104
    const v5, 0x7f0500b7

    .line 2103
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2105
    const/16 v42, 0x0

    .line 2106
    const/16 v43, 0x0

    .line 2107
    const/16 v44, 0x0

    .line 2108
    const/16 v45, 0x0

    .line 2109
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2111
    const v5, 0x7f040010

    .line 2110
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2112
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2113
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2114
    move-object/from16 v0, v57

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 2115
    const/16 v49, 0x0

    const/16 v50, 0x0

    .line 2079
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2117
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2119
    const/16 v27, 0x0

    .line 2120
    const/16 v28, 0x0

    .line 2121
    const/16 v29, 0x1

    .line 2122
    const/16 v30, 0x3

    .line 2123
    const/16 v31, 0x30

    .line 2124
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2126
    const v5, 0x7f0500b5

    .line 2125
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2127
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2129
    const v5, 0x7f0500b0

    .line 2128
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2130
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2132
    const v5, 0x7f0500b1

    .line 2131
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2133
    const/16 v35, 0x0

    .line 2134
    const/16 v36, 0x1

    .line 2135
    const/16 v37, 0x0

    .line 2136
    const/16 v38, 0xc

    .line 2137
    const/16 v39, 0x9

    .line 2138
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2140
    const v5, 0x7f0500ba

    .line 2139
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2141
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2143
    const v5, 0x7f0500b9

    .line 2142
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2144
    const/16 v42, 0x0

    .line 2145
    const/16 v43, 0x0

    .line 2146
    const/16 v44, 0x0

    .line 2147
    const/16 v45, 0x0

    .line 2148
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2150
    const v5, 0x7f040010

    .line 2149
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2151
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2152
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2154
    const/16 v5, 0xa

    const/4 v6, 0x2

    .line 2153
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 2155
    const/16 v49, 0x0

    const/16 v50, 0x0

    .line 2118
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2157
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2159
    const/16 v27, 0x0

    .line 2160
    const/16 v28, 0x0

    .line 2161
    const/16 v29, 0x1

    .line 2162
    const/16 v30, 0x3

    .line 2163
    const/16 v31, 0x30

    .line 2164
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2166
    const v5, 0x7f0500b6

    .line 2165
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2167
    const/16 v33, -0x1

    .line 2168
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2170
    const v5, 0x7f0500b3

    .line 2169
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2171
    const/16 v35, 0x0

    .line 2172
    const/16 v36, 0x1

    .line 2173
    const/high16 v37, -0x3d4c0000    # -90.0f

    .line 2174
    const/16 v38, 0xc

    .line 2175
    const/16 v39, 0x9

    .line 2176
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2178
    const v5, 0x7f0500bc

    .line 2177
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2179
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2181
    const v5, 0x7f0500bb

    .line 2180
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2182
    const/16 v42, 0x0

    .line 2183
    const/16 v43, 0x0

    .line 2184
    const/16 v44, 0x0

    .line 2185
    const/16 v45, 0x0

    .line 2186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2188
    const v5, 0x7f040010

    .line 2187
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2189
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2190
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2192
    const/16 v5, 0xa

    const/4 v6, 0x3

    .line 2191
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 2193
    const/16 v49, 0x0

    const/16 v50, 0x1

    .line 2158
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 2194
    goto/16 :goto_1

    .line 2196
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2198
    const/16 v27, 0x0

    .line 2199
    const v28, 0x7f02017c

    .line 2200
    const/16 v29, 0x1

    .line 2201
    const/16 v30, 0x3

    .line 2202
    const/16 v31, 0x30

    .line 2203
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2205
    const v5, 0x7f0500c3

    .line 2204
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2206
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2208
    const v5, 0x7f0500bd

    .line 2207
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2209
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2211
    const v5, 0x7f0500be

    .line 2210
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2212
    const/16 v35, 0x0

    .line 2213
    const/16 v36, 0x1

    .line 2214
    const/16 v37, 0x0

    .line 2215
    const/16 v38, 0xc

    .line 2216
    const/16 v39, 0xe

    .line 2217
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2219
    const v5, 0x7f0500c7

    .line 2218
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2220
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2222
    const v5, 0x7f0500c6

    .line 2221
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2223
    const/16 v42, 0x0

    .line 2224
    const/16 v43, 0x0

    .line 2225
    const/16 v44, 0x0

    .line 2226
    const/16 v45, 0x0

    .line 2227
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2229
    const v5, 0x7f040015

    .line 2228
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2230
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2231
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2233
    const/16 v5, 0xb

    const/4 v6, 0x1

    .line 2232
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 2234
    const/16 v49, 0x0

    const/16 v50, 0x0

    .line 2197
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2236
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2238
    const/16 v27, 0x0

    .line 2239
    const/16 v28, 0x0

    .line 2240
    const/16 v29, 0x1

    .line 2241
    const/16 v30, 0x3

    .line 2242
    const/16 v31, 0x30

    .line 2243
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2245
    const v5, 0x7f0500c4

    .line 2244
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2246
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2248
    const v5, 0x7f0500bf

    .line 2247
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2249
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2251
    const v5, 0x7f0500c0

    .line 2250
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2252
    const/16 v35, 0x0

    .line 2253
    const/16 v36, 0x1

    .line 2254
    const/16 v37, 0x0

    .line 2255
    const/16 v38, 0xc

    .line 2256
    const/16 v39, 0xe

    .line 2257
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2259
    const v5, 0x7f0500c9

    .line 2258
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2260
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2262
    const v5, 0x7f0500c8

    .line 2261
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2263
    const/16 v42, 0x0

    .line 2264
    const/16 v43, 0x0

    .line 2265
    const/16 v44, 0x0

    .line 2266
    const/16 v45, 0x0

    .line 2267
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2269
    const v5, 0x7f040010

    .line 2268
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2270
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2271
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2273
    const/16 v5, 0xb

    const/4 v6, 0x2

    .line 2272
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 2274
    const/16 v49, 0x0

    const/16 v50, 0x0

    .line 2237
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2276
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2278
    const/16 v27, 0x0

    .line 2279
    const/16 v28, 0x0

    .line 2280
    const/16 v29, 0x1

    .line 2281
    const/16 v30, 0x3

    .line 2282
    const/16 v31, 0x30

    .line 2283
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2285
    const v5, 0x7f0500c5

    .line 2284
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2286
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2288
    const v5, 0x7f0500c1

    .line 2287
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2289
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2291
    const v5, 0x7f0500c2

    .line 2290
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2292
    const/16 v35, 0x0

    .line 2293
    const/16 v36, 0x1

    .line 2294
    const/16 v37, 0x0

    .line 2295
    const/16 v38, 0xc

    .line 2296
    const/16 v39, 0xe

    .line 2297
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2299
    const v5, 0x7f0500cb

    .line 2298
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2300
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2302
    const v5, 0x7f0500ca

    .line 2301
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2303
    const/16 v42, 0x0

    .line 2304
    const/16 v43, 0x0

    .line 2305
    const/16 v44, 0x0

    .line 2306
    const/16 v45, 0x0

    .line 2307
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2309
    const v5, 0x7f040010

    .line 2308
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2310
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2311
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2312
    move-object/from16 v0, v57

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 2313
    const/16 v49, 0x0

    const/16 v50, 0x1

    .line 2277
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 2314
    goto/16 :goto_1

    .line 2316
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2318
    const/16 v27, 0x0

    .line 2319
    const v28, 0x7f02017e

    .line 2320
    const/16 v29, 0x1

    .line 2321
    const/16 v30, 0x3

    .line 2322
    const/16 v31, 0x30

    .line 2323
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2325
    const v5, 0x7f0500d2

    .line 2324
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2326
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2328
    const v5, 0x7f0500cc

    .line 2327
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2329
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2331
    const v5, 0x7f0500cd

    .line 2330
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2332
    const/16 v35, 0x0

    .line 2333
    const/16 v36, 0x1

    .line 2334
    const/16 v37, 0x0

    .line 2335
    const/16 v38, 0xa

    .line 2336
    const/16 v39, 0x9

    .line 2337
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2339
    const v5, 0x7f0500d6

    .line 2338
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2340
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2342
    const v5, 0x7f0500d5

    .line 2341
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2343
    const/16 v42, 0x0

    .line 2344
    const/16 v43, 0x0

    .line 2345
    const/16 v44, 0x0

    .line 2346
    const/16 v45, 0x0

    .line 2347
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2349
    const v5, 0x7f040010

    .line 2348
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2350
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2351
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2352
    move-object/from16 v0, v57

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 2353
    const/16 v49, 0x0

    const/16 v50, 0x0

    .line 2317
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2355
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2357
    const/16 v27, 0x0

    .line 2358
    const/16 v28, 0x0

    .line 2359
    const/16 v29, 0x1

    .line 2360
    const/16 v30, 0x3

    .line 2361
    const/16 v31, 0x30

    .line 2362
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2364
    const v5, 0x7f0500d3

    .line 2363
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2365
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2367
    const v5, 0x7f0500ce

    .line 2366
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2368
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2370
    const v5, 0x7f0500cf

    .line 2369
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2371
    const/16 v35, 0x0

    .line 2372
    const/16 v36, 0x1

    .line 2373
    const/16 v37, 0x0

    .line 2374
    const/16 v38, 0xa

    .line 2375
    const/16 v39, 0x9

    .line 2376
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2378
    const v5, 0x7f0500d8

    .line 2377
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2379
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2381
    const v5, 0x7f0500d7

    .line 2380
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2382
    const/16 v42, 0x0

    .line 2383
    const/16 v43, 0x0

    .line 2384
    const/16 v44, 0x0

    .line 2385
    const/16 v45, 0x0

    .line 2386
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2388
    const v5, 0x7f040010

    .line 2387
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2389
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2390
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2392
    const/16 v5, 0xc

    const/4 v6, 0x2

    .line 2391
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 2393
    const/16 v49, 0x0

    const/16 v50, 0x0

    .line 2356
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2395
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2397
    const/16 v27, 0x0

    .line 2398
    const/16 v28, 0x0

    .line 2399
    const/16 v29, 0x1

    .line 2400
    const/16 v30, 0x3

    .line 2401
    const/16 v31, 0x30

    .line 2402
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2404
    const v5, 0x7f0500d4

    .line 2403
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2405
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2407
    const v5, 0x7f0500d0

    .line 2406
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2408
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2410
    const v5, 0x7f0500d1

    .line 2409
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2411
    const/16 v35, 0x0

    .line 2412
    const/16 v36, 0x1

    .line 2413
    const/16 v37, 0x0

    .line 2414
    const/16 v38, 0xa

    .line 2415
    const/16 v39, 0x9

    .line 2416
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2418
    const v5, 0x7f0500da

    .line 2417
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2419
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2421
    const v5, 0x7f0500d9

    .line 2420
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2422
    const/16 v42, 0x0

    .line 2423
    const/16 v43, 0x0

    .line 2424
    const/16 v44, 0x0

    .line 2425
    const/16 v45, 0x0

    .line 2426
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2428
    const v5, 0x7f040016

    .line 2427
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2429
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2430
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2432
    const/16 v5, 0xc

    const/4 v6, 0x3

    .line 2431
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 2433
    const/16 v49, 0x0

    const/16 v50, 0x1

    .line 2396
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 2434
    goto/16 :goto_1

    .line 2436
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setBottomAlign(Z)V

    .line 2437
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2439
    const/16 v27, 0x0

    .line 2440
    const v28, 0x7f020180

    .line 2441
    const/16 v29, 0x1

    .line 2442
    const/16 v30, 0x3

    .line 2443
    const/16 v31, 0x30

    .line 2444
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2446
    const v5, 0x7f0500e0

    .line 2445
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2447
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2449
    const v5, 0x7f0500db

    .line 2448
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2450
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2452
    const v5, 0x7f0500dc

    .line 2451
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2453
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2455
    const v5, 0x7f0500dd

    .line 2454
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 2456
    const/16 v36, 0x3

    .line 2457
    const/16 v37, 0x0

    .line 2458
    const/16 v38, 0xc

    .line 2459
    const/16 v39, 0xe

    .line 2460
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2462
    const v5, 0x7f0500e4

    .line 2461
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2463
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2465
    const v5, 0x7f0500e5

    .line 2464
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2466
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2468
    const v5, 0x7f0500ea

    .line 2467
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v42

    .line 2469
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2471
    const v5, 0x7f0500e9

    .line 2470
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v43

    .line 2472
    const v44, 0x7f02032f

    .line 2473
    const/16 v45, 0x5

    .line 2474
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2476
    const v5, 0x7f040017

    .line 2475
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2477
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2479
    const/16 v50, 0x0

    move-object/from16 v48, v58

    move/from16 v49, v51

    .line 2438
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2481
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2483
    const/16 v27, 0x0

    .line 2484
    const/16 v28, 0x0

    .line 2485
    const/16 v29, 0x1

    .line 2486
    const/16 v30, 0x3

    .line 2487
    const/16 v31, 0x30

    .line 2488
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2490
    const v5, 0x7f0500e1

    .line 2489
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2491
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2493
    const v5, 0x7f0500db

    .line 2492
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2494
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2496
    const v5, 0x7f0500dd

    .line 2495
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2497
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2499
    const v5, 0x7f0500dc

    .line 2498
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 2500
    const/16 v36, 0x3

    .line 2501
    const/16 v37, 0x0

    .line 2502
    const/16 v38, 0xc

    .line 2503
    const/16 v39, 0xe

    .line 2504
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2506
    const v5, 0x7f0500e4

    .line 2505
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2507
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2509
    const v5, 0x7f0500e6

    .line 2508
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2510
    const/16 v42, 0x0

    .line 2511
    const/16 v43, 0x0

    .line 2512
    const/16 v44, 0x0

    .line 2513
    const/16 v45, 0x0

    .line 2514
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2516
    const v5, 0x7f040017

    .line 2515
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2517
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2519
    const/16 v50, 0x0

    move-object/from16 v48, v59

    move/from16 v49, v65

    .line 2482
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2520
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setBottomAlign(Z)V

    .line 2522
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2524
    const/16 v27, 0x0

    .line 2525
    const/16 v28, 0x0

    .line 2526
    const/16 v29, 0x1

    .line 2527
    const/16 v30, 0x3

    .line 2528
    const/16 v31, 0x30

    .line 2529
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2531
    const v5, 0x7f0500e2

    .line 2530
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2532
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2534
    const v5, 0x7f0500db

    .line 2533
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2535
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2537
    const v5, 0x7f0500de

    .line 2536
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2538
    const/16 v35, 0x0

    .line 2539
    const/16 v36, 0x3

    .line 2540
    const/16 v37, 0x0

    .line 2541
    const/16 v38, 0xc

    .line 2542
    const/16 v39, 0xe

    .line 2543
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2545
    const v5, 0x7f0500e4

    .line 2544
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2546
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2548
    const v5, 0x7f0500e7

    .line 2547
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2549
    const/16 v42, 0x0

    .line 2550
    const/16 v43, 0x0

    .line 2551
    const/16 v44, 0x0

    .line 2552
    const/16 v45, 0x0

    .line 2553
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2555
    const v5, 0x7f040017

    .line 2554
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2557
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2559
    const/16 v5, 0xd

    const/4 v6, 0x3

    .line 2558
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 2560
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v47, v62

    .line 2523
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2562
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2564
    const/16 v27, 0x0

    .line 2565
    const/16 v28, 0x0

    .line 2566
    const/16 v29, 0x1

    .line 2567
    const/16 v30, 0x3

    .line 2568
    const/16 v31, 0x30

    .line 2569
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2571
    const v5, 0x7f0500e3

    .line 2570
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2572
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2574
    const v5, 0x7f0500db

    .line 2573
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2575
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2577
    const v5, 0x7f0500df

    .line 2576
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2578
    const/16 v35, 0x0

    .line 2579
    const/16 v36, 0x3

    .line 2580
    const/16 v37, 0x0

    .line 2581
    const/16 v38, 0xc

    .line 2582
    const/16 v39, 0xe

    .line 2583
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2585
    const v5, 0x7f0500e4

    .line 2584
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2586
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2588
    const v5, 0x7f0500e8

    .line 2587
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2589
    const/16 v42, 0x0

    .line 2590
    const/16 v43, 0x0

    .line 2591
    const/16 v44, 0x0

    .line 2592
    const/16 v45, 0x0

    .line 2593
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2595
    const v5, 0x7f040017

    .line 2594
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2597
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2599
    const/16 v5, 0xd

    const/4 v6, 0x4

    .line 2598
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 2600
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v47, v62

    .line 2563
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 2601
    goto/16 :goto_1

    .line 2603
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setBottomAlign(Z)V

    .line 2604
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2606
    const/16 v27, 0x0

    .line 2607
    const v28, 0x7f020182

    .line 2608
    const/16 v29, 0x1

    .line 2609
    const/16 v30, 0x1

    .line 2610
    const/16 v31, 0x30

    .line 2611
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2613
    const v5, 0x7f0500f1

    .line 2612
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2614
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2616
    const v5, 0x7f0500eb

    .line 2615
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2617
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2619
    const v5, 0x7f0500ec

    .line 2618
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2620
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2622
    const v5, 0x7f0500ee

    .line 2621
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 2623
    const/16 v36, 0x1

    .line 2624
    const/16 v37, 0x0

    .line 2625
    const/16 v38, 0xc

    .line 2626
    const/16 v39, 0xe

    .line 2627
    const/16 v40, 0x0

    .line 2628
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2630
    const v5, 0x7f0500f5

    .line 2629
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2631
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2633
    const v5, 0x7f0500fa

    .line 2632
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v42

    .line 2634
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2636
    const v5, 0x7f0500f9

    .line 2635
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v43

    .line 2637
    const v44, 0x7f02032f

    .line 2638
    const/16 v45, 0xe

    .line 2639
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2641
    const v5, 0x7f040018

    .line 2640
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2642
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2644
    const/16 v50, 0x0

    move-object/from16 v48, v58

    move/from16 v49, v51

    .line 2605
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2646
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2648
    const/16 v27, 0x0

    .line 2649
    const/16 v28, 0x0

    .line 2650
    const/16 v29, 0x1

    .line 2651
    const/16 v30, 0x1

    .line 2652
    const/16 v31, 0x30

    .line 2653
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2655
    const v5, 0x7f0500f2

    .line 2654
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2656
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2658
    const v5, 0x7f0500ed

    .line 2657
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2659
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2661
    const v5, 0x7f0500ee

    .line 2660
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2662
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2664
    const v5, 0x7f0500ec

    .line 2663
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 2665
    const/16 v36, 0x1

    .line 2666
    const/16 v37, 0x0

    .line 2667
    const/16 v38, 0xc

    .line 2668
    const/16 v39, 0xe

    .line 2669
    const/16 v40, 0x0

    .line 2670
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2672
    const v5, 0x7f0500f6

    .line 2671
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2673
    const/16 v42, 0x0

    .line 2674
    const/16 v43, 0x0

    .line 2675
    const/16 v44, 0x0

    .line 2676
    const/16 v45, 0x0

    .line 2677
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2679
    const v5, 0x7f040018

    .line 2678
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2680
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2682
    const/16 v50, 0x0

    move-object/from16 v48, v59

    move/from16 v49, v65

    .line 2647
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2683
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setBottomAlign(Z)V

    .line 2685
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2687
    const/16 v27, 0x0

    .line 2688
    const/16 v28, 0x0

    .line 2689
    const/16 v29, 0x1

    .line 2690
    const/16 v30, 0x1

    .line 2691
    const/16 v31, 0x30

    .line 2692
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2694
    const v5, 0x7f0500f3

    .line 2693
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2695
    const/16 v33, -0x1

    .line 2696
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2698
    const v5, 0x7f0500ef

    .line 2697
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2699
    const/16 v35, 0x0

    .line 2700
    const/16 v36, 0x1

    .line 2701
    const/16 v37, 0x0

    .line 2702
    const/16 v38, 0xc

    .line 2703
    const/16 v39, 0xe

    .line 2704
    const/16 v40, 0x0

    .line 2705
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2707
    const v5, 0x7f0500f7

    .line 2706
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2708
    const/16 v42, 0x0

    .line 2709
    const/16 v43, 0x0

    .line 2710
    const/16 v44, 0x0

    .line 2711
    const/16 v45, 0x0

    .line 2712
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2714
    const v5, 0x7f040018

    .line 2713
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2716
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2718
    const/16 v5, 0xe

    const/4 v6, 0x3

    .line 2717
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 2719
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v47, v62

    .line 2686
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2721
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2723
    const/16 v27, 0x0

    .line 2724
    const/16 v28, 0x0

    .line 2725
    const/16 v29, 0x1

    .line 2726
    const/16 v30, 0x1

    .line 2727
    const/16 v31, 0x30

    .line 2728
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2730
    const v5, 0x7f0500f4

    .line 2729
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2731
    const/16 v33, -0x1

    .line 2732
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2734
    const v5, 0x7f0500f0

    .line 2733
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2735
    const/16 v35, 0x0

    .line 2736
    const/16 v36, 0x1

    .line 2737
    const/16 v37, 0x0

    .line 2738
    const/16 v38, 0xc

    .line 2739
    const/16 v39, 0xe

    .line 2740
    const/16 v40, 0x0

    .line 2741
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2743
    const v5, 0x7f0500f8

    .line 2742
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2744
    const/16 v42, 0x0

    .line 2745
    const/16 v43, 0x0

    .line 2746
    const/16 v44, 0x0

    .line 2747
    const/16 v45, 0x0

    .line 2748
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2750
    const v5, 0x7f040018

    .line 2749
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2752
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2754
    const/16 v5, 0xe

    const/4 v6, 0x4

    .line 2753
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 2755
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v47, v62

    .line 2722
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 2756
    goto/16 :goto_1

    .line 2758
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setBottomAlign(Z)V

    .line 2759
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2761
    const/16 v27, 0x0

    .line 2762
    const v28, 0x7f020184

    .line 2763
    const/16 v29, 0x1

    .line 2764
    const/16 v30, 0x3

    .line 2765
    const/16 v31, 0x30

    .line 2766
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2768
    const v5, 0x7f050100

    .line 2767
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2769
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2771
    const v5, 0x7f0500fb

    .line 2770
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2772
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2774
    const v5, 0x7f0500fc

    .line 2773
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2775
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2777
    const v5, 0x7f0500fd

    .line 2776
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 2778
    const/16 v36, 0x3

    .line 2779
    const/16 v37, 0x0

    .line 2780
    const/16 v38, 0xc

    .line 2781
    const/16 v39, 0x9

    .line 2782
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2784
    const v5, 0x7f050104

    .line 2783
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2785
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2787
    const v5, 0x7f050105

    .line 2786
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2788
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2790
    const v5, 0x7f05010a

    .line 2789
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v42

    .line 2791
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2793
    const v5, 0x7f050109

    .line 2792
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v43

    .line 2794
    const v44, 0x7f020330

    .line 2795
    const/16 v45, 0x5

    .line 2796
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2798
    const v5, 0x7f040019

    .line 2797
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2799
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2801
    const/16 v50, 0x0

    move-object/from16 v48, v58

    move/from16 v49, v51

    .line 2760
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2803
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2805
    const/16 v27, 0x0

    .line 2806
    const/16 v28, 0x0

    .line 2807
    const/16 v29, 0x1

    .line 2808
    const/16 v30, 0x3

    .line 2809
    const/16 v31, 0x30

    .line 2810
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2812
    const v5, 0x7f050101

    .line 2811
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2813
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2815
    const v5, 0x7f0500fb

    .line 2814
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2816
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2818
    const v5, 0x7f0500fd

    .line 2817
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2819
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2821
    const v5, 0x7f0500fc

    .line 2820
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 2822
    const/16 v36, 0x3

    .line 2823
    const/16 v37, 0x0

    .line 2824
    const/16 v38, 0xc

    .line 2825
    const/16 v39, 0x9

    .line 2826
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2828
    const v5, 0x7f050104

    .line 2827
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2829
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2831
    const v5, 0x7f050106

    .line 2830
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2832
    const/16 v42, 0x0

    .line 2833
    const/16 v43, 0x0

    .line 2834
    const/16 v44, 0x0

    .line 2835
    const/16 v45, 0x0

    .line 2836
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2838
    const v5, 0x7f040019

    .line 2837
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2839
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2841
    const/16 v50, 0x0

    move-object/from16 v48, v59

    move/from16 v49, v65

    .line 2804
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2842
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setBottomAlign(Z)V

    .line 2844
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2846
    const/16 v27, 0x0

    .line 2847
    const/16 v28, 0x0

    .line 2848
    const/16 v29, 0x1

    .line 2849
    const/16 v30, 0x3

    .line 2850
    const/16 v31, 0x30

    .line 2851
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2853
    const v5, 0x7f050102

    .line 2852
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2854
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2856
    const v5, 0x7f0500fb

    .line 2855
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2857
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2859
    const v5, 0x7f0500fe

    .line 2858
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2860
    const/16 v35, 0x0

    .line 2861
    const/16 v36, 0x3

    .line 2862
    const/16 v37, 0x0

    .line 2863
    const/16 v38, 0xc

    .line 2864
    const/16 v39, 0x9

    .line 2865
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2867
    const v5, 0x7f050104

    .line 2866
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2868
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2870
    const v5, 0x7f050107

    .line 2869
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2871
    const/16 v42, 0x0

    .line 2872
    const/16 v43, 0x0

    .line 2873
    const/16 v44, 0x0

    .line 2874
    const/16 v45, 0x0

    .line 2875
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2877
    const v5, 0x7f040019

    .line 2876
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2879
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2881
    const/16 v5, 0xf

    const/4 v6, 0x3

    .line 2880
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 2882
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v47, v62

    .line 2845
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2884
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2886
    const/16 v27, 0x0

    .line 2887
    const/16 v28, 0x0

    .line 2888
    const/16 v29, 0x1

    .line 2889
    const/16 v30, 0x3

    .line 2890
    const/16 v31, 0x30

    .line 2891
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2893
    const v5, 0x7f050103

    .line 2892
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2894
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2896
    const v5, 0x7f0500fb

    .line 2895
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2897
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2899
    const v5, 0x7f0500ff

    .line 2898
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2900
    const/16 v35, 0x0

    .line 2901
    const/16 v36, 0x3

    .line 2902
    const/16 v37, 0x0

    .line 2903
    const/16 v38, 0xc

    .line 2904
    const/16 v39, 0x9

    .line 2905
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2907
    const v5, 0x7f050104

    .line 2906
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2908
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2910
    const v5, 0x7f050108

    .line 2909
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2911
    const/16 v42, 0x0

    .line 2912
    const/16 v43, 0x0

    .line 2913
    const/16 v44, 0x0

    .line 2914
    const/16 v45, 0x0

    .line 2915
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2917
    const v5, 0x7f040019

    .line 2916
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2919
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 2921
    const/16 v5, 0xf

    const/4 v6, 0x4

    .line 2920
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 2922
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v47, v62

    .line 2885
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 2923
    goto/16 :goto_1

    .line 2925
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setBottomAlign(Z)V

    .line 2926
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2928
    const/16 v27, 0x0

    .line 2929
    const v28, 0x7f020186

    .line 2930
    const/16 v29, 0x1

    .line 2931
    const/16 v30, 0x3

    .line 2932
    const/16 v31, 0x30

    .line 2933
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2935
    const v5, 0x7f050110

    .line 2934
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2936
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2938
    const v5, 0x7f05010b

    .line 2937
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2939
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2941
    const v5, 0x7f05010c

    .line 2940
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2942
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2944
    const v5, 0x7f05010d

    .line 2943
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 2945
    const/16 v36, 0x3

    .line 2946
    const/16 v37, 0x0

    .line 2947
    const/16 v38, 0xc

    .line 2948
    const/16 v39, 0xe

    .line 2949
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2951
    const v5, 0x7f050114

    .line 2950
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2952
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2954
    const v5, 0x7f050115

    .line 2953
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2955
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2957
    const v5, 0x7f05011a

    .line 2956
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v42

    .line 2958
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2960
    const v5, 0x7f050119

    .line 2959
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v43

    .line 2961
    const v44, 0x7f020330

    .line 2962
    const/16 v45, 0x5

    .line 2963
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2965
    const v5, 0x7f04001a

    .line 2964
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 2966
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 2968
    const/16 v50, 0x0

    move-object/from16 v48, v58

    move/from16 v49, v51

    .line 2927
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 2970
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 2972
    const/16 v27, 0x0

    .line 2973
    const/16 v28, 0x0

    .line 2974
    const/16 v29, 0x1

    .line 2975
    const/16 v30, 0x3

    .line 2976
    const/16 v31, 0x30

    .line 2977
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2979
    const v5, 0x7f050111

    .line 2978
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 2980
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2982
    const v5, 0x7f05010b

    .line 2981
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2983
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2985
    const v5, 0x7f05010d

    .line 2984
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 2986
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2988
    const v5, 0x7f05010c

    .line 2987
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 2989
    const/16 v36, 0x3

    .line 2990
    const/16 v37, 0x0

    .line 2991
    const/16 v38, 0xc

    .line 2992
    const/16 v39, 0xe

    .line 2993
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2995
    const v5, 0x7f050114

    .line 2994
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 2996
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2998
    const v5, 0x7f050116

    .line 2997
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 2999
    const/16 v42, 0x0

    .line 3000
    const/16 v43, 0x0

    .line 3001
    const/16 v44, 0x0

    .line 3002
    const/16 v45, 0x0

    .line 3003
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3005
    const v5, 0x7f04001a

    .line 3004
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3006
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 3008
    const/16 v50, 0x0

    move-object/from16 v48, v59

    move/from16 v49, v65

    .line 2971
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3009
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setBottomAlign(Z)V

    .line 3011
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3013
    const/16 v27, 0x0

    .line 3014
    const/16 v28, 0x0

    .line 3015
    const/16 v29, 0x1

    .line 3016
    const/16 v30, 0x3

    .line 3017
    const/16 v31, 0x30

    .line 3018
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3020
    const v5, 0x7f050112

    .line 3019
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3021
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3023
    const v5, 0x7f05010b

    .line 3022
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3024
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3026
    const v5, 0x7f05010e

    .line 3025
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3027
    const/16 v35, 0x0

    .line 3028
    const/16 v36, 0x3

    .line 3029
    const/16 v37, 0x0

    .line 3030
    const/16 v38, 0xc

    .line 3031
    const/16 v39, 0xe

    .line 3032
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3034
    const v5, 0x7f050114

    .line 3033
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3035
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3037
    const v5, 0x7f050117

    .line 3036
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3038
    const/16 v42, 0x0

    .line 3039
    const/16 v43, 0x0

    .line 3040
    const/16 v44, 0x0

    .line 3041
    const/16 v45, 0x0

    .line 3042
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3044
    const v5, 0x7f04001a

    .line 3043
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3046
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3048
    const/16 v5, 0x10

    const/4 v6, 0x3

    .line 3047
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 3049
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v47, v62

    .line 3012
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3051
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3053
    const/16 v27, 0x0

    .line 3054
    const/16 v28, 0x0

    .line 3055
    const/16 v29, 0x1

    .line 3056
    const/16 v30, 0x3

    .line 3057
    const/16 v31, 0x30

    .line 3058
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3060
    const v5, 0x7f050113

    .line 3059
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3061
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3063
    const v5, 0x7f05010b

    .line 3062
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3064
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3066
    const v5, 0x7f05010f

    .line 3065
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3067
    const/16 v35, 0x0

    .line 3068
    const/16 v36, 0x3

    .line 3069
    const/16 v37, 0x0

    .line 3070
    const/16 v38, 0xc

    .line 3071
    const/16 v39, 0xe

    .line 3072
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3074
    const v5, 0x7f050114

    .line 3073
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3075
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3077
    const v5, 0x7f050118

    .line 3076
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3078
    const/16 v42, 0x0

    .line 3079
    const/16 v43, 0x0

    .line 3080
    const/16 v44, 0x0

    .line 3081
    const/16 v45, 0x0

    .line 3082
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3084
    const v5, 0x7f04001a

    .line 3083
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3086
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3088
    const/16 v5, 0x10

    const/4 v6, 0x4

    .line 3087
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 3089
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v47, v62

    .line 3052
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 3090
    goto/16 :goto_1

    .line 3093
    :pswitch_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setBottomAlign(Z)V

    .line 3094
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3096
    const/16 v27, 0x0

    .line 3097
    const v28, 0x7f020188

    .line 3098
    const/16 v29, 0x1

    .line 3099
    const/16 v30, 0x3

    .line 3100
    const/16 v31, 0x30

    .line 3101
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3103
    const v5, 0x7f050123

    .line 3102
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3104
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3106
    const v5, 0x7f05011b

    .line 3105
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3107
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3109
    const v5, 0x7f05011c

    .line 3108
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3110
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3112
    const v5, 0x7f05011e

    .line 3111
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 3113
    const/16 v36, 0x1

    .line 3114
    const/16 v37, 0x0

    .line 3115
    const/16 v38, 0xa

    .line 3116
    const/16 v39, 0x9

    .line 3117
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3119
    const v5, 0x7f050128

    .line 3118
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3120
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3122
    const v5, 0x7f050129

    .line 3121
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3123
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3125
    const v5, 0x7f050130

    .line 3124
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v42

    .line 3126
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3128
    const v5, 0x7f05012f

    .line 3127
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v43

    .line 3129
    const v44, 0x7f020330

    .line 3130
    const/16 v45, 0xe

    .line 3131
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3133
    const v5, 0x7f04001b

    .line 3132
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3134
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 3136
    const/16 v50, 0x0

    move-object/from16 v48, v58

    move/from16 v49, v51

    .line 3095
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3138
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3140
    const/16 v27, 0x0

    .line 3141
    const/16 v28, 0x0

    .line 3142
    const/16 v29, 0x1

    .line 3143
    const/16 v30, 0x3

    .line 3144
    const/16 v31, 0x30

    .line 3145
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3147
    const v5, 0x7f050124

    .line 3146
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3148
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3150
    const v5, 0x7f05011d

    .line 3149
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3151
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3153
    const v5, 0x7f05011e

    .line 3152
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3154
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3156
    const v5, 0x7f05011c

    .line 3155
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v35

    .line 3157
    const/16 v36, 0x1

    .line 3158
    const/16 v37, 0x0

    .line 3159
    const/16 v38, 0xa

    .line 3160
    const/16 v39, 0x9

    .line 3161
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3163
    const v5, 0x7f050128

    .line 3162
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3164
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3166
    const v5, 0x7f05012a

    .line 3165
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3167
    const/16 v42, 0x0

    .line 3168
    const/16 v43, 0x0

    .line 3169
    const/16 v44, 0x0

    .line 3170
    const/16 v45, 0x0

    .line 3171
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3173
    const v5, 0x7f04001b

    .line 3172
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3174
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 3176
    const/16 v50, 0x0

    move-object/from16 v48, v59

    move/from16 v49, v65

    .line 3139
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3177
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setBottomAlign(Z)V

    .line 3179
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3181
    const/16 v27, 0x0

    .line 3182
    const/16 v28, 0x0

    .line 3183
    const/16 v29, 0x1

    .line 3184
    const/16 v30, 0x3

    .line 3185
    const/16 v31, 0x30

    .line 3186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3188
    const v5, 0x7f050125

    .line 3187
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3189
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3191
    const v5, 0x7f05011f

    .line 3190
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3192
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3194
    const v5, 0x7f050120

    .line 3193
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3195
    const/16 v35, 0x0

    .line 3196
    const/16 v36, 0x1

    .line 3197
    const/16 v37, 0x0

    .line 3198
    const/16 v38, 0xa

    .line 3199
    const/16 v39, 0x9

    .line 3200
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3202
    const v5, 0x7f05012b

    .line 3201
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3203
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3205
    const v5, 0x7f05012c

    .line 3204
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3206
    const/16 v42, 0x0

    .line 3207
    const/16 v43, 0x0

    .line 3208
    const/16 v44, 0x0

    .line 3209
    const/16 v45, 0x0

    .line 3210
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3212
    const v5, 0x7f04001b

    .line 3211
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3214
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3216
    const/16 v5, 0x11

    const/4 v6, 0x3

    .line 3215
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 3217
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v47, v62

    .line 3180
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3219
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3221
    const/16 v27, 0x0

    .line 3222
    const/16 v28, 0x0

    .line 3223
    const/16 v29, 0x1

    .line 3224
    const/16 v30, 0x3

    .line 3225
    const/16 v31, 0x30

    .line 3226
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3228
    const v5, 0x7f050126

    .line 3227
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3229
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3231
    const v5, 0x7f05011f

    .line 3230
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3232
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3234
    const v5, 0x7f050121

    .line 3233
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3235
    const/16 v35, 0x0

    .line 3236
    const/16 v36, 0x1

    .line 3237
    const/16 v37, 0x0

    .line 3238
    const/16 v38, 0xa

    .line 3239
    const/16 v39, 0x9

    .line 3240
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3242
    const v5, 0x7f05012b

    .line 3241
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3243
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3245
    const v5, 0x7f05012d

    .line 3244
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3246
    const/16 v42, 0x0

    .line 3247
    const/16 v43, 0x0

    .line 3248
    const/16 v44, 0x0

    .line 3249
    const/16 v45, 0x0

    .line 3250
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3252
    const v5, 0x7f04001b

    .line 3251
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3254
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3256
    const/16 v5, 0x11

    const/4 v6, 0x4

    .line 3255
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 3257
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v47, v62

    .line 3220
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 3289
    goto/16 :goto_1

    .line 3291
    :pswitch_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3293
    const/16 v27, 0x0

    .line 3294
    const v28, 0x7f02016e

    .line 3295
    const/16 v29, 0x1

    .line 3296
    const/16 v30, 0x3

    .line 3297
    const/16 v31, 0x30

    .line 3298
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3300
    const v5, 0x7f050137

    .line 3299
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3301
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3303
    const v5, 0x7f050131

    .line 3302
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3304
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3306
    const v5, 0x7f050132

    .line 3305
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3307
    const/16 v35, 0x0

    .line 3308
    const/16 v36, 0x11

    .line 3309
    const/16 v37, 0x0

    .line 3310
    const/16 v38, 0xc

    .line 3311
    const/16 v39, 0xe

    .line 3312
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3314
    const v5, 0x7f05013a

    .line 3313
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3315
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3317
    const v5, 0x7f05013b

    .line 3316
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3318
    const/16 v42, 0x0

    .line 3319
    const/16 v43, 0x0

    .line 3320
    const/16 v44, 0x0

    .line 3321
    const/16 v45, 0x0

    .line 3322
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3324
    const v5, 0x7f04001c

    .line 3323
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3325
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 3326
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3328
    const/16 v5, 0x12

    const/4 v6, 0x1

    .line 3327
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 3329
    const/16 v49, 0x0

    const/16 v50, 0x0

    .line 3292
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3331
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3333
    const/16 v27, 0x0

    .line 3334
    const/16 v28, 0x0

    .line 3335
    const/16 v29, 0x1

    .line 3336
    const/16 v30, 0x3

    .line 3337
    const/16 v31, 0x30

    .line 3338
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3340
    const v5, 0x7f050138

    .line 3339
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3341
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3343
    const v5, 0x7f050133

    .line 3342
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3344
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3346
    const v5, 0x7f050134

    .line 3345
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3347
    const/16 v35, 0x0

    .line 3348
    const/16 v36, 0x11

    .line 3349
    const/16 v37, 0x0

    .line 3350
    const/16 v38, 0xc

    .line 3351
    const/16 v39, 0xe

    .line 3352
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3354
    const v5, 0x7f05013a

    .line 3353
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3355
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3357
    const v5, 0x7f05013c

    .line 3356
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3358
    const/16 v42, 0x0

    .line 3359
    const/16 v43, 0x0

    .line 3360
    const/16 v44, 0x0

    .line 3361
    const/16 v45, 0x0

    .line 3362
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3364
    const v5, 0x7f04001c

    .line 3363
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3365
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 3366
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3368
    const/16 v5, 0x12

    const/4 v6, 0x2

    .line 3367
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 3369
    const/16 v49, 0x0

    const/16 v50, 0x0

    .line 3332
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3371
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3373
    const/16 v27, 0x0

    .line 3374
    const/16 v28, 0x0

    .line 3375
    const/16 v29, 0x1

    .line 3376
    const/16 v30, 0x3

    .line 3377
    const/16 v31, 0x30

    .line 3378
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3380
    const v5, 0x7f050139

    .line 3379
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3381
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3383
    const v5, 0x7f050135

    .line 3382
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3384
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3386
    const v5, 0x7f050136

    .line 3385
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3387
    const/16 v35, 0x0

    .line 3388
    const/16 v36, 0x11

    .line 3389
    const/16 v37, 0x0

    .line 3390
    const/16 v38, 0xc

    .line 3391
    const/16 v39, 0xe

    .line 3392
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3394
    const v5, 0x7f05013d

    .line 3393
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3395
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3397
    const v5, 0x7f05013e

    .line 3396
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3398
    const/16 v42, 0x0

    .line 3399
    const/16 v43, 0x0

    .line 3400
    const/16 v44, 0x0

    .line 3401
    const/16 v45, 0x0

    .line 3402
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3404
    const v5, 0x7f04001c

    .line 3403
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3405
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 3406
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3408
    const/16 v5, 0x12

    const/4 v6, 0x3

    .line 3407
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 3409
    const/16 v49, 0x0

    const/16 v50, 0x1

    .line 3372
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 3410
    goto/16 :goto_1

    .line 3412
    :pswitch_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3414
    const/16 v27, 0x0

    .line 3415
    const v28, 0x7f020170

    .line 3416
    const/16 v29, 0x1

    .line 3417
    const/16 v30, 0x3

    .line 3418
    const/16 v31, 0x30

    .line 3419
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3421
    const v5, 0x7f050145

    .line 3420
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3422
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3424
    const v5, 0x7f05013f

    .line 3423
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3425
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3427
    const v5, 0x7f050140

    .line 3426
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3428
    const/16 v35, 0x0

    .line 3429
    const/16 v36, 0x1

    .line 3430
    const/16 v37, 0x0

    .line 3431
    const/16 v38, 0xa

    .line 3432
    const/16 v39, 0x9

    .line 3433
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3435
    const v5, 0x7f050148

    .line 3434
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3436
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3438
    const v5, 0x7f050149

    .line 3437
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3439
    const/16 v42, 0x0

    .line 3440
    const/16 v43, 0x0

    .line 3441
    const/16 v44, 0x0

    .line 3442
    const/16 v45, 0x0

    .line 3443
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3445
    const v5, 0x7f04001d

    .line 3444
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3447
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3448
    move-object/from16 v0, v57

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDayOfTheWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 3449
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v47, v62

    .line 3413
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3451
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3453
    const/16 v27, 0x0

    .line 3454
    const/16 v28, 0x0

    .line 3455
    const/16 v29, 0x1

    .line 3456
    const/16 v30, 0x3

    .line 3457
    const/16 v31, 0x30

    .line 3458
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3460
    const v5, 0x7f050146

    .line 3459
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3461
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3463
    const v5, 0x7f050141

    .line 3462
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3464
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3466
    const v5, 0x7f050142

    .line 3465
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3467
    const/16 v35, 0x0

    .line 3468
    const/16 v36, 0x1

    .line 3469
    const/16 v37, 0x0

    .line 3470
    const/16 v38, 0xa

    .line 3471
    const/16 v39, 0x9

    .line 3472
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3474
    const v5, 0x7f05014a

    .line 3473
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3475
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3477
    const v5, 0x7f05014b

    .line 3476
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3478
    const/16 v42, 0x0

    .line 3479
    const/16 v43, 0x0

    .line 3480
    const/16 v44, 0x0

    .line 3481
    const/16 v45, 0x0

    .line 3482
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3484
    const v5, 0x7f04001d

    .line 3483
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3485
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 3486
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3488
    const/16 v5, 0x13

    const/4 v6, 0x2

    .line 3487
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 3489
    const/16 v49, 0x0

    const/16 v50, 0x0

    .line 3452
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3491
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3493
    const/16 v27, 0x0

    .line 3494
    const/16 v28, 0x0

    .line 3495
    const/16 v29, 0x1

    .line 3496
    const/16 v30, 0x3

    .line 3497
    const/16 v31, 0x30

    .line 3498
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3500
    const v5, 0x7f050147

    .line 3499
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3501
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3503
    const v5, 0x7f050143

    .line 3502
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3504
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3506
    const v5, 0x7f050144

    .line 3505
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3507
    const/16 v35, 0x0

    .line 3508
    const/16 v36, 0x1

    .line 3509
    const/16 v37, 0x0

    .line 3510
    const/16 v38, 0xa

    .line 3511
    const/16 v39, 0x9

    .line 3512
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3514
    const v5, 0x7f05014c

    .line 3513
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3515
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3517
    const v5, 0x7f05014d

    .line 3516
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3518
    const/16 v42, 0x0

    .line 3519
    const/16 v43, 0x0

    .line 3520
    const/16 v44, 0x0

    .line 3521
    const/16 v45, 0x0

    .line 3522
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3524
    const v5, 0x7f04001d

    .line 3523
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3525
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 3526
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3528
    const/16 v5, 0x13

    const/4 v6, 0x3

    .line 3527
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 3529
    const/16 v49, 0x0

    const/16 v50, 0x1

    .line 3492
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 3530
    goto/16 :goto_1

    .line 3532
    :pswitch_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3534
    const/16 v27, 0x0

    .line 3535
    const v28, 0x7f020172

    .line 3536
    const/16 v29, 0x1

    .line 3537
    const/16 v30, 0x3

    .line 3538
    const/16 v31, 0x30

    .line 3539
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3541
    const v5, 0x7f050154

    .line 3540
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3542
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3544
    const v5, 0x7f05014e

    .line 3543
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3545
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3547
    const v5, 0x7f05014f

    .line 3546
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3548
    const/16 v35, 0x0

    .line 3549
    const/16 v36, 0x1

    .line 3550
    const/16 v37, 0x0

    .line 3551
    const/16 v38, 0xc

    .line 3552
    const/16 v39, 0xb

    .line 3553
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3555
    const v5, 0x7f050157

    .line 3554
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3556
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3558
    const v5, 0x7f050158

    .line 3557
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3559
    const/16 v42, 0x0

    .line 3560
    const/16 v43, 0x0

    .line 3561
    const/16 v44, 0x0

    .line 3562
    const/16 v45, 0x0

    .line 3563
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3565
    const v5, 0x7f04001e

    .line 3564
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3567
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3569
    const/16 v5, 0x14

    const/4 v6, 0x1

    .line 3568
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 3570
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v47, v61

    .line 3533
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3572
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3574
    const/16 v27, 0x0

    .line 3575
    const/16 v28, 0x0

    .line 3576
    const/16 v29, 0x1

    .line 3577
    const/16 v30, 0x3

    .line 3578
    const/16 v31, 0x30

    .line 3579
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3581
    const v5, 0x7f050155

    .line 3580
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3582
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3584
    const v5, 0x7f050150

    .line 3583
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3585
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3587
    const v5, 0x7f050151

    .line 3586
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3588
    const/16 v35, 0x0

    .line 3589
    const/16 v36, 0x1

    .line 3590
    const/16 v37, 0x0

    .line 3591
    const/16 v38, 0xc

    .line 3592
    const/16 v39, 0xb

    .line 3593
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3595
    const v5, 0x7f050159

    .line 3594
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3596
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3598
    const v5, 0x7f05015a

    .line 3597
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3599
    const/16 v42, 0x0

    .line 3600
    const/16 v43, 0x0

    .line 3601
    const/16 v44, 0x0

    .line 3602
    const/16 v45, 0x0

    .line 3603
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3605
    const v5, 0x7f04001e

    .line 3604
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3607
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3609
    const/16 v5, 0x14

    const/4 v6, 0x2

    .line 3608
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 3610
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v47, v61

    .line 3573
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3612
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3614
    const/16 v27, 0x0

    .line 3615
    const/16 v28, 0x0

    .line 3616
    const/16 v29, 0x1

    .line 3617
    const/16 v30, 0x3

    .line 3618
    const/16 v31, 0x30

    .line 3619
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3621
    const v5, 0x7f050156

    .line 3620
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3622
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3624
    const v5, 0x7f050152

    .line 3623
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3625
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3627
    const v5, 0x7f050153

    .line 3626
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3628
    const/16 v35, 0x0

    .line 3629
    const/16 v36, 0x1

    .line 3630
    const/16 v37, 0x0

    .line 3631
    const/16 v38, 0xc

    .line 3632
    const/16 v39, 0xb

    .line 3633
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3635
    const v5, 0x7f05015b

    .line 3634
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3636
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3638
    const v5, 0x7f05015c

    .line 3637
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3639
    const/16 v42, 0x0

    .line 3640
    const/16 v43, 0x0

    .line 3641
    const/16 v44, 0x0

    .line 3642
    const/16 v45, 0x0

    .line 3643
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3645
    const v5, 0x7f04001f

    .line 3644
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3647
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3649
    const/16 v5, 0x14

    const/4 v6, 0x3

    .line 3648
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 3650
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v47, v61

    .line 3613
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 3651
    goto/16 :goto_1

    .line 3653
    :pswitch_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3655
    const/16 v27, 0x0

    .line 3656
    const v28, 0x7f020174

    .line 3657
    const/16 v29, 0x1

    .line 3658
    const/16 v30, 0x3

    .line 3659
    const/16 v31, 0x30

    .line 3660
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3662
    const v5, 0x7f050161

    .line 3661
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3663
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3665
    const v5, 0x7f05015d

    .line 3664
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3666
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3668
    const v5, 0x7f05015e

    .line 3667
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3669
    const/16 v35, 0x0

    .line 3670
    const/16 v36, 0x1

    .line 3671
    const/16 v37, 0x0

    .line 3672
    const/16 v38, 0xc

    .line 3673
    const/16 v39, 0x9

    .line 3674
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3676
    const v5, 0x7f050163

    .line 3675
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3677
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3679
    const v5, 0x7f050164

    .line 3678
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3680
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3682
    const v5, 0x7f050168

    .line 3681
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v42

    .line 3683
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3685
    const v5, 0x7f050167

    .line 3684
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v43

    .line 3686
    const v44, 0x7f020331

    .line 3687
    const/16 v45, 0xe

    .line 3688
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3690
    const v5, 0x7f040020

    .line 3689
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3691
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 3692
    const/16 v49, 0x0

    .line 3693
    const/16 v50, 0x0

    move-object/from16 v48, v58

    .line 3654
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3695
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 3697
    const/16 v27, 0x0

    .line 3698
    const/16 v28, 0x0

    .line 3699
    const/16 v29, 0x1

    .line 3700
    const/16 v30, 0x3

    .line 3701
    const/16 v31, 0x30

    .line 3702
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3704
    const v5, 0x7f050162

    .line 3703
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 3705
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3707
    const v5, 0x7f05015f

    .line 3706
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 3708
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3710
    const v5, 0x7f050160

    .line 3709
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 3711
    const/16 v35, 0x0

    .line 3712
    const/16 v36, 0x1

    .line 3713
    const/16 v37, 0x0

    .line 3714
    const/16 v38, 0xc

    .line 3715
    const/16 v39, 0x9

    .line 3716
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3718
    const v5, 0x7f050165

    .line 3717
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 3719
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3721
    const v5, 0x7f050166

    .line 3720
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 3722
    const/16 v42, 0x0

    .line 3723
    const/16 v43, 0x0

    .line 3724
    const/16 v44, 0x0

    .line 3725
    const/16 v45, 0x0

    .line 3726
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3728
    const v5, 0x7f040020

    .line 3727
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 3730
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3732
    const/16 v5, 0x15

    const/4 v6, 0x2

    .line 3731
    move-object/from16 v0, v57

    invoke-virtual {v4, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 3733
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v47, v62

    .line 3696
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 3734
    goto/16 :goto_1

    .line 3737
    :pswitch_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3739
    const/4 v5, 0x0

    .line 3740
    const v6, 0x7f0200ce

    .line 3741
    const/4 v7, 0x1

    .line 3742
    const/4 v8, 0x1

    .line 3743
    const/16 v9, 0x30

    .line 3744
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .end local v8    # "data":[I
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 3746
    const v12, 0x7f05016f

    .line 3745
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 3747
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 3749
    const v12, 0x7f050169

    .line 3748
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 3750
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 3752
    const v13, 0x7f05016a

    .line 3751
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 3753
    const/4 v13, 0x0

    .line 3754
    const/4 v14, 0x1

    .line 3755
    const/4 v15, 0x0

    .line 3756
    const/16 v16, 0xd

    .line 3757
    const/16 v17, 0xd

    .line 3758
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 3760
    const v19, 0x7f050172

    .line 3759
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 3761
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 3763
    const v20, 0x7f050173

    .line 3762
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 3764
    const/16 v20, 0x0

    .line 3765
    const/16 v21, 0x0

    .line 3766
    const/16 v22, 0x0

    .line 3767
    const/16 v23, 0x0

    .line 3768
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 3770
    const v26, 0x7f040021

    .line 3769
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 3772
    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v26, v53

    .line 3738
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3774
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3776
    const/4 v5, 0x0

    .line 3777
    const/4 v6, 0x0

    .line 3778
    const/4 v7, 0x1

    .line 3779
    const/4 v8, 0x1

    .line 3780
    const/16 v9, 0x30

    .line 3781
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 3783
    const v12, 0x7f050170

    .line 3782
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 3784
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 3786
    const v12, 0x7f05016b

    .line 3785
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 3787
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 3789
    const v13, 0x7f05016c

    .line 3788
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 3790
    const/4 v13, 0x0

    .line 3791
    const/4 v14, 0x1

    .line 3792
    const/4 v15, 0x0

    .line 3793
    const/16 v16, 0xd

    .line 3794
    const/16 v17, 0xd

    .line 3795
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 3797
    const v19, 0x7f050172

    .line 3796
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 3798
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 3800
    const v20, 0x7f050174

    .line 3799
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 3801
    const/16 v20, 0x0

    .line 3802
    const/16 v21, 0x0

    .line 3803
    const/16 v22, 0x0

    .line 3804
    const/16 v23, 0x0

    .line 3805
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 3807
    const v26, 0x7f040021

    .line 3806
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 3809
    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v26, v54

    .line 3775
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3811
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3813
    const/4 v5, 0x0

    .line 3814
    const/4 v6, 0x0

    .line 3815
    const/4 v7, 0x1

    .line 3816
    const/4 v8, 0x1

    .line 3817
    const/16 v9, 0x30

    .line 3818
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 3820
    const v12, 0x7f050171

    .line 3819
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 3821
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 3823
    const v12, 0x7f05016d

    .line 3822
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 3824
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 3826
    const v13, 0x7f05016e

    .line 3825
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 3827
    const/4 v13, 0x0

    .line 3828
    const/4 v14, 0x1

    .line 3829
    const/4 v15, 0x0

    .line 3830
    const/16 v16, 0xd

    .line 3831
    const/16 v17, 0xd

    .line 3832
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 3834
    const v19, 0x7f050172

    .line 3833
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 3835
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 3837
    const v20, 0x7f050175

    .line 3836
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 3838
    const/16 v20, 0x0

    .line 3839
    const/16 v21, 0x0

    .line 3840
    const/16 v22, 0x0

    .line 3841
    const/16 v23, 0x0

    .line 3842
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 3844
    const v26, 0x7f040021

    .line 3843
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 3845
    const/16 v27, 0x0

    .line 3846
    const/16 v28, 0x1

    move-object/from16 v26, v56

    .line 3812
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 3847
    .restart local v8    # "data":[I
    goto/16 :goto_1

    .line 3849
    :pswitch_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3851
    const/4 v5, 0x0

    .line 3852
    const v6, 0x7f0200d0

    .line 3853
    const/4 v7, 0x1

    .line 3854
    const/4 v8, 0x3

    .line 3855
    const/16 v9, 0x30

    .line 3856
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .end local v8    # "data":[I
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 3858
    const v12, 0x7f05017c

    .line 3857
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 3859
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 3861
    const v12, 0x7f050176

    .line 3860
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 3862
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 3864
    const v13, 0x7f050177

    .line 3863
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 3865
    const/4 v13, 0x0

    .line 3866
    const/4 v14, 0x3

    .line 3867
    const/4 v15, 0x0

    .line 3868
    const/16 v16, 0xa

    .line 3869
    const/16 v17, 0x9

    .line 3870
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 3872
    const v19, 0x7f05017d

    .line 3871
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 3873
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 3875
    const v20, 0x7f05017e

    .line 3874
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 3876
    const/16 v20, 0x0

    .line 3877
    const/16 v21, 0x0

    .line 3878
    const/16 v22, 0x0

    .line 3879
    const/16 v23, 0x0

    .line 3880
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 3882
    const v26, 0x7f040022

    .line 3881
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 3884
    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v26, v53

    .line 3850
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3886
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3888
    const/4 v5, 0x0

    .line 3889
    const/4 v6, 0x0

    .line 3890
    const/4 v7, 0x1

    .line 3891
    const/4 v8, 0x3

    .line 3892
    const/16 v9, 0x30

    .line 3893
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 3895
    const v12, 0x7f05017c

    .line 3894
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 3896
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 3898
    const v12, 0x7f050178

    .line 3897
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 3899
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 3901
    const v13, 0x7f050179

    .line 3900
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 3902
    const/4 v13, 0x0

    .line 3903
    const/4 v14, 0x3

    .line 3904
    const/4 v15, 0x0

    .line 3905
    const/16 v16, 0xa

    .line 3906
    const/16 v17, 0x9

    .line 3907
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 3909
    const v19, 0x7f05017d

    .line 3908
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 3910
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 3912
    const v20, 0x7f05017f

    .line 3911
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 3913
    const/16 v20, 0x0

    .line 3914
    const/16 v21, 0x0

    .line 3915
    const/16 v22, 0x0

    .line 3916
    const/16 v23, 0x0

    .line 3917
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 3919
    const v26, 0x7f040022

    .line 3918
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 3921
    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v26, v54

    .line 3887
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3923
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3925
    const/4 v5, 0x0

    .line 3926
    const/4 v6, 0x0

    .line 3927
    const/4 v7, 0x1

    .line 3928
    const/4 v8, 0x3

    .line 3929
    const/16 v9, 0x30

    .line 3930
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 3932
    const v12, 0x7f05017c

    .line 3931
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 3933
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 3935
    const v12, 0x7f05017a

    .line 3934
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 3936
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 3938
    const v13, 0x7f05017b

    .line 3937
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 3939
    const/4 v13, 0x0

    .line 3940
    const/4 v14, 0x3

    .line 3941
    const/4 v15, 0x0

    .line 3942
    const/16 v16, 0xa

    .line 3943
    const/16 v17, 0x9

    .line 3944
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 3946
    const v19, 0x7f05017d

    .line 3945
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 3947
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 3949
    const v20, 0x7f050180

    .line 3948
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 3950
    const/16 v20, 0x0

    .line 3951
    const/16 v21, 0x0

    .line 3952
    const/16 v22, 0x0

    .line 3953
    const/16 v23, 0x0

    .line 3954
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 3956
    const v26, 0x7f040022

    .line 3955
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 3957
    const/16 v27, 0x0

    .line 3958
    const/16 v28, 0x1

    move-object/from16 v26, v56

    .line 3924
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 3959
    .restart local v8    # "data":[I
    goto/16 :goto_1

    .line 3961
    :pswitch_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 3963
    const/4 v5, 0x0

    .line 3964
    const v6, 0x7f0200d2

    .line 3965
    const/4 v7, 0x1

    .line 3966
    const/4 v8, 0x3

    .line 3967
    const/16 v9, 0x30

    .line 3968
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .end local v8    # "data":[I
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 3970
    const v12, 0x7f050187

    .line 3969
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 3971
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 3973
    const v12, 0x7f050181

    .line 3972
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 3974
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 3976
    const v13, 0x7f050182

    .line 3975
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 3977
    const/4 v13, 0x0

    .line 3978
    const/16 v14, 0x11

    .line 3979
    const/4 v15, 0x0

    .line 3980
    const/16 v16, 0xc

    .line 3981
    const/16 v17, 0xb

    .line 3982
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 3984
    const v19, 0x7f050188

    .line 3983
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 3985
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 3987
    const v20, 0x7f050189

    .line 3986
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 3988
    const/16 v20, 0x0

    .line 3989
    const/16 v21, 0x0

    .line 3990
    const/16 v22, 0x0

    .line 3991
    const/16 v23, 0x0

    .line 3992
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 3994
    const v26, 0x7f040023

    .line 3993
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 3996
    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v26, v53

    .line 3962
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 3998
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 4000
    const/4 v5, 0x0

    .line 4001
    const/4 v6, 0x0

    .line 4002
    const/4 v7, 0x1

    .line 4003
    const/4 v8, 0x3

    .line 4004
    const/16 v9, 0x30

    .line 4005
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4007
    const v12, 0x7f050187

    .line 4006
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 4008
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4010
    const v12, 0x7f050183

    .line 4009
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 4011
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 4013
    const v13, 0x7f050184

    .line 4012
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 4014
    const/4 v13, 0x0

    .line 4015
    const/16 v14, 0x11

    .line 4016
    const/4 v15, 0x0

    .line 4017
    const/16 v16, 0xc

    .line 4018
    const/16 v17, 0xb

    .line 4019
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 4021
    const v19, 0x7f05018a

    .line 4020
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 4022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 4024
    const v20, 0x7f05018b

    .line 4023
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 4025
    const/16 v20, 0x0

    .line 4026
    const/16 v21, 0x0

    .line 4027
    const/16 v22, 0x0

    .line 4028
    const/16 v23, 0x0

    .line 4029
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 4031
    const v26, 0x7f040023

    .line 4030
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 4033
    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v26, v54

    .line 3999
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4035
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 4037
    const/4 v5, 0x0

    .line 4038
    const/4 v6, 0x0

    .line 4039
    const/4 v7, 0x1

    .line 4040
    const/4 v8, 0x3

    .line 4041
    const/16 v9, 0x30

    .line 4042
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4044
    const v12, 0x7f050187

    .line 4043
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 4045
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4047
    const v12, 0x7f050185

    .line 4046
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 4048
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 4050
    const v13, 0x7f050186

    .line 4049
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 4051
    const/4 v13, 0x0

    .line 4052
    const/16 v14, 0x11

    .line 4053
    const/4 v15, 0x0

    .line 4054
    const/16 v16, 0xc

    .line 4055
    const/16 v17, 0xb

    .line 4056
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 4058
    const v19, 0x7f05018c

    .line 4057
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 4059
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 4061
    const v20, 0x7f05018d

    .line 4060
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 4062
    const/16 v20, 0x0

    .line 4063
    const/16 v21, 0x0

    .line 4064
    const/16 v22, 0x0

    .line 4065
    const/16 v23, 0x0

    .line 4066
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 4068
    const v26, 0x7f040023

    .line 4067
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 4069
    const/16 v27, 0x0

    .line 4070
    const/16 v28, 0x1

    move-object/from16 v26, v56

    .line 4036
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 4071
    .restart local v8    # "data":[I
    goto/16 :goto_1

    .line 4073
    :pswitch_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 4075
    const/4 v5, 0x0

    .line 4076
    const v6, 0x7f0200d4

    .line 4077
    const/4 v7, 0x1

    .line 4078
    const/4 v8, 0x3

    .line 4079
    const/16 v9, 0x30

    .line 4080
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .end local v8    # "data":[I
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4082
    const v12, 0x7f050194

    .line 4081
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 4083
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4085
    const v12, 0x7f05018e

    .line 4084
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 4086
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 4088
    const v13, 0x7f05018f

    .line 4087
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 4089
    const/4 v13, 0x0

    .line 4090
    const/4 v14, 0x3

    .line 4091
    const/4 v15, 0x0

    .line 4092
    const/16 v16, 0xc

    .line 4093
    const/16 v17, 0xe

    .line 4094
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 4096
    const v19, 0x7f050195

    .line 4095
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 4097
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 4099
    const v20, 0x7f050196

    .line 4098
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 4100
    const/16 v20, 0x0

    .line 4101
    const/16 v21, 0x0

    .line 4102
    const/16 v22, 0x0

    .line 4103
    const/16 v23, 0x0

    .line 4104
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 4106
    const v26, 0x7f040024

    .line 4105
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 4108
    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v26, v53

    .line 4074
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4110
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 4112
    const/4 v5, 0x0

    .line 4113
    const/4 v6, 0x0

    .line 4114
    const/4 v7, 0x1

    .line 4115
    const/4 v8, 0x3

    .line 4116
    const/16 v9, 0x30

    .line 4117
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4119
    const v12, 0x7f050194

    .line 4118
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 4120
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4122
    const v12, 0x7f050190

    .line 4121
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 4123
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 4125
    const v13, 0x7f050191

    .line 4124
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 4126
    const/4 v13, 0x0

    .line 4127
    const/4 v14, 0x3

    .line 4128
    const/4 v15, 0x0

    .line 4129
    const/16 v16, 0xc

    .line 4130
    const/16 v17, 0xe

    .line 4131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 4133
    const v19, 0x7f050197

    .line 4132
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 4134
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 4136
    const v20, 0x7f050198

    .line 4135
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 4137
    const/16 v20, 0x0

    .line 4138
    const/16 v21, 0x0

    .line 4139
    const/16 v22, 0x0

    .line 4140
    const/16 v23, 0x0

    .line 4141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 4143
    const v26, 0x7f040024

    .line 4142
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 4145
    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v26, v54

    .line 4111
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4147
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 4149
    const/4 v5, 0x0

    .line 4150
    const/4 v6, 0x0

    .line 4151
    const/4 v7, 0x1

    .line 4152
    const/4 v8, 0x3

    .line 4153
    const/16 v9, 0x30

    .line 4154
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4156
    const v12, 0x7f050194

    .line 4155
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 4157
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4159
    const v12, 0x7f050192

    .line 4158
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 4160
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 4162
    const v13, 0x7f050193

    .line 4161
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 4163
    const/4 v13, 0x0

    .line 4164
    const/4 v14, 0x3

    .line 4165
    const/4 v15, 0x0

    .line 4166
    const/16 v16, 0xc

    .line 4167
    const/16 v17, 0xe

    .line 4168
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 4170
    const v19, 0x7f050199

    .line 4169
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 4171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 4173
    const v20, 0x7f05019a

    .line 4172
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 4174
    const/16 v20, 0x0

    .line 4175
    const/16 v21, 0x0

    .line 4176
    const/16 v22, 0x0

    .line 4177
    const/16 v23, 0x0

    .line 4178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 4180
    const v26, 0x7f040024

    .line 4179
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 4181
    const/16 v27, 0x0

    .line 4182
    const/16 v28, 0x1

    move-object/from16 v26, v56

    .line 4148
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 4183
    .restart local v8    # "data":[I
    goto/16 :goto_1

    .line 4185
    :pswitch_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 4187
    const/4 v5, 0x0

    .line 4188
    const v6, 0x7f0200c2

    .line 4189
    const/4 v7, 0x1

    .line 4190
    const/4 v8, 0x3

    .line 4191
    const/16 v9, 0x30

    .line 4192
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .end local v8    # "data":[I
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4194
    const v12, 0x7f0501a3

    .line 4193
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 4195
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4197
    const v12, 0x7f05019b

    .line 4196
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 4198
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 4200
    const v13, 0x7f05019c

    .line 4199
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 4201
    const/4 v13, 0x0

    .line 4202
    const/4 v14, 0x1

    .line 4203
    const/4 v15, 0x0

    .line 4204
    const/16 v16, 0xc

    .line 4205
    const/16 v17, 0xe

    .line 4206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 4208
    const v19, 0x7f0501a5

    .line 4207
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 4209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 4211
    const v20, 0x7f0501a6

    .line 4210
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 4212
    const/16 v20, 0x0

    .line 4213
    const/16 v21, 0x0

    .line 4214
    const/16 v22, 0x0

    .line 4215
    const/16 v23, 0x0

    .line 4216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 4218
    const v26, 0x7f040025

    .line 4217
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 4220
    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v26, v54

    .line 4186
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4222
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 4224
    const/4 v5, 0x0

    .line 4225
    const/4 v6, 0x0

    .line 4226
    const/4 v7, 0x1

    .line 4227
    const/4 v8, 0x3

    .line 4228
    const/16 v9, 0x30

    .line 4229
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4231
    const v12, 0x7f0501a3

    .line 4230
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 4232
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4234
    const v12, 0x7f05019d

    .line 4233
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 4235
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 4237
    const v13, 0x7f05019e

    .line 4236
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 4238
    const/4 v13, 0x0

    .line 4239
    const/4 v14, 0x1

    .line 4240
    const/4 v15, 0x0

    .line 4241
    const/16 v16, 0xc

    .line 4242
    const/16 v17, 0xe

    .line 4243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 4245
    const v19, 0x7f0501a7

    .line 4244
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 4246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 4248
    const v20, 0x7f0501a8

    .line 4247
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 4249
    const/16 v20, 0x0

    .line 4250
    const/16 v21, 0x0

    .line 4251
    const/16 v22, 0x0

    .line 4252
    const/16 v23, 0x0

    .line 4253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 4255
    const v26, 0x7f040025

    .line 4254
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 4256
    const/16 v27, 0x0

    .line 4257
    const/16 v28, 0x0

    move-object/from16 v26, v56

    .line 4223
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4259
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 4261
    const/4 v5, 0x0

    .line 4262
    const/4 v6, 0x0

    .line 4263
    const/4 v7, 0x1

    .line 4264
    const/4 v8, 0x3

    .line 4265
    const/16 v9, 0x30

    .line 4266
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4268
    const v12, 0x7f0501a3

    .line 4267
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 4269
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4271
    const v12, 0x7f05019f

    .line 4270
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 4272
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 4274
    const v13, 0x7f0501a0

    .line 4273
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 4275
    const/4 v13, 0x0

    .line 4276
    const/4 v14, 0x1

    .line 4277
    const/4 v15, 0x0

    .line 4278
    const/16 v16, 0xc

    .line 4279
    const/16 v17, 0xe

    .line 4280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 4282
    const v19, 0x7f0501a9

    .line 4281
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 4283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 4285
    const v20, 0x7f0501aa

    .line 4284
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 4286
    const/16 v20, 0x0

    .line 4287
    const/16 v21, 0x0

    .line 4288
    const/16 v22, 0x0

    .line 4289
    const/16 v23, 0x0

    .line 4290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 4292
    const v26, 0x7f040025

    .line 4291
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 4294
    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v26, v53

    .line 4260
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4296
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4298
    const/16 v27, 0x0

    .line 4299
    const/16 v28, 0x0

    .line 4300
    const/16 v29, 0x1

    .line 4301
    const/16 v30, 0x3

    .line 4302
    const/16 v31, 0x30

    .line 4303
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4305
    const v5, 0x7f0501a4

    .line 4304
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4306
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4308
    const v5, 0x7f0501a1

    .line 4307
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4309
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4311
    const v5, 0x7f0501a2

    .line 4310
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4312
    const/16 v35, 0x0

    .line 4313
    const/16 v36, 0x1

    .line 4314
    const/16 v37, 0x0

    .line 4315
    const/16 v38, 0xc

    .line 4316
    const/16 v39, 0xe

    .line 4317
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4319
    const v5, 0x7f0501ab

    .line 4318
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4320
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4322
    const v5, 0x7f0501ac

    .line 4321
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4323
    const/16 v42, 0x0

    .line 4324
    const/16 v43, 0x0

    .line 4325
    const/16 v44, 0x0

    .line 4326
    const/16 v45, 0x0

    .line 4327
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4329
    const v5, 0x7f040026

    .line 4328
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4330
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 4331
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 4333
    const/4 v5, 0x1

    .line 4332
    move-object/from16 v0, v57

    move/from16 v1, v60

    invoke-virtual {v4, v0, v1, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 4334
    const/16 v49, 0x0

    const/16 v50, 0x1

    .line 4297
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 4335
    .restart local v8    # "data":[I
    goto/16 :goto_1

    .line 4337
    :pswitch_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 4339
    const/4 v5, 0x0

    .line 4340
    const v6, 0x7f0200c4

    .line 4341
    const/4 v7, 0x1

    .line 4342
    const/4 v8, 0x3

    .line 4343
    const/16 v9, 0x30

    .line 4344
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .end local v8    # "data":[I
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4346
    const v12, 0x7f0501b5

    .line 4345
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 4347
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4349
    const v12, 0x7f0501ad

    .line 4348
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 4350
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 4352
    const v13, 0x7f0501ae

    .line 4351
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 4353
    const/4 v13, 0x0

    .line 4354
    const/4 v14, 0x1

    .line 4355
    const/4 v15, 0x0

    .line 4356
    const/16 v16, 0xc

    .line 4357
    const/16 v17, 0xe

    .line 4358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 4360
    const v19, 0x7f0501b7

    .line 4359
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 4361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 4363
    const v20, 0x7f0501b8

    .line 4362
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 4364
    const/16 v20, 0x0

    .line 4365
    const/16 v21, 0x0

    .line 4366
    const/16 v22, 0x0

    .line 4367
    const/16 v23, 0x0

    .line 4368
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 4370
    const v26, 0x7f040027

    .line 4369
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 4372
    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v26, v54

    .line 4338
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4374
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 4376
    const/4 v5, 0x0

    .line 4377
    const/4 v6, 0x0

    .line 4378
    const/4 v7, 0x1

    .line 4379
    const/4 v8, 0x3

    .line 4380
    const/16 v9, 0x30

    .line 4381
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4383
    const v12, 0x7f0501b5

    .line 4382
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 4384
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4386
    const v12, 0x7f0501af

    .line 4385
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 4387
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 4389
    const v13, 0x7f0501b0

    .line 4388
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 4390
    const/4 v13, 0x0

    .line 4391
    const/4 v14, 0x1

    .line 4392
    const/4 v15, 0x0

    .line 4393
    const/16 v16, 0xc

    .line 4394
    const/16 v17, 0xe

    .line 4395
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 4397
    const v19, 0x7f0501b9

    .line 4396
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 4398
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 4400
    const v20, 0x7f0501ba

    .line 4399
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 4401
    const/16 v20, 0x0

    .line 4402
    const/16 v21, 0x0

    .line 4403
    const/16 v22, 0x0

    .line 4404
    const/16 v23, 0x0

    .line 4405
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 4407
    const v26, 0x7f040027

    .line 4406
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 4408
    const/16 v27, 0x0

    .line 4409
    const/16 v28, 0x0

    move-object/from16 v26, v56

    .line 4375
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4411
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 4413
    const/4 v5, 0x0

    .line 4414
    const/4 v6, 0x0

    .line 4415
    const/4 v7, 0x1

    .line 4416
    const/4 v8, 0x3

    .line 4417
    const/16 v9, 0x30

    .line 4418
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4420
    const v12, 0x7f0501b5

    .line 4419
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 4421
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 4423
    const v12, 0x7f0501b1

    .line 4422
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 4424
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 4426
    const v13, 0x7f0501b2

    .line 4425
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 4427
    const/4 v13, 0x0

    .line 4428
    const/4 v14, 0x1

    .line 4429
    const/4 v15, 0x0

    .line 4430
    const/16 v16, 0xc

    .line 4431
    const/16 v17, 0xe

    .line 4432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 4434
    const v19, 0x7f0501bb

    .line 4433
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 4435
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 4437
    const v20, 0x7f0501bc

    .line 4436
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 4438
    const/16 v20, 0x0

    .line 4439
    const/16 v21, 0x0

    .line 4440
    const/16 v22, 0x0

    .line 4441
    const/16 v23, 0x0

    .line 4442
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 4444
    const v26, 0x7f040027

    .line 4443
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 4446
    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v26, v53

    .line 4412
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4448
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4450
    const/16 v27, 0x0

    .line 4451
    const/16 v28, 0x0

    .line 4452
    const/16 v29, 0x1

    .line 4453
    const/16 v30, 0x3

    .line 4454
    const/16 v31, 0x30

    .line 4455
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4457
    const v5, 0x7f0501b6

    .line 4456
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4458
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4460
    const v5, 0x7f0501b3

    .line 4459
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4461
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4463
    const v5, 0x7f0501b4

    .line 4462
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4464
    const/16 v35, 0x0

    .line 4465
    const/16 v36, 0x3

    .line 4466
    const/16 v37, 0x0

    .line 4467
    const/16 v38, 0xc

    .line 4468
    const/16 v39, 0xe

    .line 4469
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4471
    const v5, 0x7f0501bd

    .line 4470
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4472
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4474
    const v5, 0x7f0501be

    .line 4473
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4475
    const/16 v42, 0x0

    .line 4476
    const/16 v43, 0x0

    .line 4477
    const/16 v44, 0x0

    .line 4478
    const/16 v45, 0x0

    .line 4479
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4481
    const v5, 0x7f040028

    .line 4480
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4482
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 4483
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 4485
    const/4 v5, 0x0

    .line 4484
    move-object/from16 v0, v57

    move/from16 v1, v60

    invoke-virtual {v4, v0, v1, v5}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getDateInfoByParsing(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v48

    .line 4486
    const/16 v49, 0x0

    const/16 v50, 0x1

    .line 4449
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 4487
    .restart local v8    # "data":[I
    goto/16 :goto_1

    .line 4489
    :pswitch_1b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4491
    const/16 v27, 0x0

    .line 4492
    const v28, 0x7f0200c6

    .line 4493
    const/16 v29, 0x1

    .line 4494
    const/16 v30, 0x3

    .line 4495
    const/16 v31, 0x30

    .line 4496
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4498
    const v5, 0x7f0501c5

    .line 4497
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4499
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4501
    const v5, 0x7f0501bf

    .line 4500
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4502
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4504
    const v5, 0x7f0501c0

    .line 4503
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4505
    const/16 v35, 0x0

    .line 4506
    const/16 v36, 0x1

    .line 4507
    const/16 v37, 0x0

    .line 4508
    const/16 v38, 0xc

    .line 4509
    const/16 v39, 0x9

    .line 4510
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4512
    const v5, 0x7f0501c6

    .line 4511
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4513
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4515
    const v5, 0x7f0501c7

    .line 4514
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4516
    const/16 v42, 0x0

    .line 4517
    const/16 v43, 0x0

    .line 4518
    const/16 v44, 0x0

    .line 4519
    const/16 v45, 0x0

    .line 4520
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4522
    const v5, 0x7f040029

    .line 4521
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4523
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 4524
    const/16 v49, 0x0

    .line 4525
    const/16 v50, 0x0

    move-object/from16 v48, v54

    .line 4490
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4527
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4529
    const/16 v27, 0x0

    .line 4530
    const/16 v28, 0x0

    .line 4531
    const/16 v29, 0x1

    .line 4532
    const/16 v30, 0x3

    .line 4533
    const/16 v31, 0x30

    .line 4534
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4536
    const v5, 0x7f0501c5

    .line 4535
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4537
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4539
    const v5, 0x7f0501c1

    .line 4538
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4540
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4542
    const v5, 0x7f0501c2

    .line 4541
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4543
    const/16 v35, 0x0

    .line 4544
    const/16 v36, 0x1

    .line 4545
    const/16 v37, 0x0

    .line 4546
    const/16 v38, 0xc

    .line 4547
    const/16 v39, 0x9

    .line 4548
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4550
    const v5, 0x7f0501c8

    .line 4549
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4551
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4553
    const v5, 0x7f0501c9

    .line 4552
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4554
    const/16 v42, 0x0

    .line 4555
    const/16 v43, 0x0

    .line 4556
    const/16 v44, 0x0

    .line 4557
    const/16 v45, 0x0

    .line 4558
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4560
    const v5, 0x7f040029

    .line 4559
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4561
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 4562
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v48, v56

    .line 4528
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4564
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4566
    const/16 v27, 0x0

    .line 4567
    const/16 v28, 0x0

    .line 4568
    const/16 v29, 0x1

    .line 4569
    const/16 v30, 0x3

    .line 4570
    const/16 v31, 0x30

    .line 4571
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4573
    const v5, 0x7f0501c5

    .line 4572
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4574
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4576
    const v5, 0x7f0501c3

    .line 4575
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4577
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4579
    const v5, 0x7f0501c4

    .line 4578
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4580
    const/16 v35, 0x0

    .line 4581
    const/16 v36, 0x1

    .line 4582
    const/16 v37, 0x0

    .line 4583
    const/16 v38, 0xc

    .line 4584
    const/16 v39, 0x9

    .line 4585
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4587
    const v5, 0x7f0501ca

    .line 4586
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4588
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4590
    const v5, 0x7f0501cb

    .line 4589
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4591
    const/16 v42, 0x0

    .line 4592
    const/16 v43, 0x0

    .line 4593
    const/16 v44, 0x0

    .line 4594
    const/16 v45, 0x0

    .line 4595
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4597
    const v5, 0x7f040029

    .line 4596
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4598
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 4599
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v48, v53

    .line 4565
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 4600
    goto/16 :goto_1

    .line 4602
    :pswitch_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4604
    const/16 v27, 0x0

    .line 4605
    const v28, 0x7f0200c8

    .line 4606
    const/16 v29, 0x1

    .line 4607
    const/16 v30, 0x3

    .line 4608
    const/16 v31, 0x30

    .line 4609
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4611
    const v5, 0x7f0501d2

    .line 4610
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4612
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4614
    const v5, 0x7f0501cc

    .line 4613
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4615
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4617
    const v5, 0x7f0501cd

    .line 4616
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4618
    const/16 v35, 0x0

    .line 4619
    const/16 v36, 0x1

    .line 4620
    const/16 v37, 0x0

    .line 4621
    const/16 v38, 0xc

    .line 4622
    const/16 v39, 0xe

    .line 4623
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4625
    const v5, 0x7f0501d3

    .line 4624
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4626
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4628
    const v5, 0x7f0501d4

    .line 4627
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4629
    const/16 v42, 0x0

    .line 4630
    const/16 v43, 0x0

    .line 4631
    const/16 v44, 0x0

    .line 4632
    const/16 v45, 0x0

    .line 4633
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4635
    const v5, 0x7f04002a

    .line 4634
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4636
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    const/16 v49, 0x0

    .line 4637
    const/16 v50, 0x0

    move-object/from16 v48, v54

    .line 4603
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4639
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4641
    const/16 v27, 0x0

    .line 4642
    const/16 v28, 0x0

    .line 4643
    const/16 v29, 0x1

    .line 4644
    const/16 v30, 0x3

    .line 4645
    const/16 v31, 0x30

    .line 4646
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4648
    const v5, 0x7f0501d2

    .line 4647
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4649
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4651
    const v5, 0x7f0501ce

    .line 4650
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4652
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4654
    const v5, 0x7f0501cf

    .line 4653
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4655
    const/16 v35, 0x0

    .line 4656
    const/16 v36, 0x1

    .line 4657
    const/16 v37, 0x0

    .line 4658
    const/16 v38, 0xc

    .line 4659
    const/16 v39, 0xe

    .line 4660
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4662
    const v5, 0x7f0501d5

    .line 4661
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4663
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4665
    const v5, 0x7f0501d6

    .line 4664
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4666
    const/16 v42, 0x0

    .line 4667
    const/16 v43, 0x0

    .line 4668
    const/16 v44, 0x0

    .line 4669
    const/16 v45, 0x0

    .line 4670
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4672
    const v5, 0x7f04002a

    .line 4671
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4673
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 4674
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v48, v56

    .line 4640
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4676
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4678
    const/16 v27, 0x0

    .line 4679
    const/16 v28, 0x0

    .line 4680
    const/16 v29, 0x1

    .line 4681
    const/16 v30, 0x3

    .line 4682
    const/16 v31, 0x30

    .line 4683
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4685
    const v5, 0x7f0501d2

    .line 4684
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4686
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4688
    const v5, 0x7f0501d0

    .line 4687
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4689
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4691
    const v5, 0x7f0501d1

    .line 4690
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4692
    const/16 v35, 0x0

    .line 4693
    const/16 v36, 0x1

    .line 4694
    const/16 v37, 0x0

    .line 4695
    const/16 v38, 0xc

    .line 4696
    const/16 v39, 0xe

    .line 4697
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4699
    const v5, 0x7f0501d7

    .line 4698
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4700
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4702
    const v5, 0x7f0501d8

    .line 4701
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4703
    const/16 v42, 0x0

    .line 4704
    const/16 v43, 0x0

    .line 4705
    const/16 v44, 0x0

    .line 4706
    const/16 v45, 0x0

    .line 4707
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4709
    const v5, 0x7f04002a

    .line 4708
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4710
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 4712
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v48, v53

    .line 4677
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 4713
    goto/16 :goto_1

    .line 4715
    :pswitch_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4717
    const/16 v27, 0x0

    .line 4718
    const v28, 0x7f0200ca

    .line 4719
    const/16 v29, 0x1

    .line 4720
    const/16 v30, 0x3

    .line 4721
    const/16 v31, 0x30

    .line 4722
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4724
    const v5, 0x7f0501df

    .line 4723
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4725
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4727
    const v5, 0x7f0501d9

    .line 4726
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4728
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4730
    const v5, 0x7f0501da

    .line 4729
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4731
    const/16 v35, 0x0

    .line 4732
    const/16 v36, 0x1

    .line 4733
    const/16 v37, 0x0

    .line 4734
    const/16 v38, 0xa

    .line 4735
    const/16 v39, 0x9

    .line 4736
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4738
    const v5, 0x7f0501e1

    .line 4737
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4739
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4741
    const v5, 0x7f0501e2

    .line 4740
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4742
    const/16 v42, 0x0

    .line 4743
    const/16 v43, 0x0

    .line 4744
    const/16 v44, 0x0

    .line 4745
    const/16 v45, 0x0

    .line 4746
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4748
    const v5, 0x7f04002b

    .line 4747
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4749
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 4751
    const/16 v49, 0x0

    .line 4752
    const/16 v50, 0x0

    move-object/from16 v48, v54

    .line 4716
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4754
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4756
    const/16 v27, 0x0

    .line 4757
    const/16 v28, 0x0

    .line 4758
    const/16 v29, 0x1

    .line 4759
    const/16 v30, 0x3

    .line 4760
    const/16 v31, 0x30

    .line 4761
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4763
    const v5, 0x7f0501e0

    .line 4762
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4764
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4766
    const v5, 0x7f0501db

    .line 4765
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4767
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4769
    const v5, 0x7f0501dc

    .line 4768
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4770
    const/16 v35, 0x0

    .line 4771
    const/16 v36, 0x1

    .line 4772
    const/16 v37, 0x0

    .line 4773
    const/16 v38, 0xa

    .line 4774
    const/16 v39, 0x9

    .line 4775
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4777
    const v5, 0x7f0501e3

    .line 4776
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4778
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4780
    const v5, 0x7f0501e4

    .line 4779
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4781
    const/16 v42, 0x0

    .line 4782
    const/16 v43, 0x0

    .line 4783
    const/16 v44, 0x0

    .line 4784
    const/16 v45, 0x0

    .line 4785
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4787
    const v5, 0x7f04002c

    .line 4786
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4788
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 4790
    const/16 v49, 0x0

    .line 4791
    const/16 v50, 0x0

    move-object/from16 v48, v53

    .line 4755
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4793
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4795
    const/16 v27, 0x0

    .line 4796
    const/16 v28, 0x0

    .line 4797
    const/16 v29, 0x1

    .line 4798
    const/16 v30, 0x3

    .line 4799
    const/16 v31, 0x30

    .line 4800
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4802
    const v5, 0x7f0501df

    .line 4801
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4803
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4805
    const v5, 0x7f0501dd

    .line 4804
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4806
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4808
    const v5, 0x7f0501de

    .line 4807
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4809
    const/16 v35, 0x0

    .line 4810
    const/16 v36, 0x1

    .line 4811
    const/16 v37, 0x0

    .line 4812
    const/16 v38, 0xa

    .line 4813
    const/16 v39, 0x9

    .line 4814
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4816
    const v5, 0x7f0501e5

    .line 4815
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4817
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4819
    const v5, 0x7f0501e6

    .line 4818
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4820
    const/16 v42, 0x0

    .line 4821
    const/16 v43, 0x0

    .line 4822
    const/16 v44, 0x0

    .line 4823
    const/16 v45, 0x0

    .line 4824
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4826
    const v5, 0x7f04002b

    .line 4825
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4827
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 4829
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v48, v56

    .line 4794
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 4830
    goto/16 :goto_1

    .line 4832
    :pswitch_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4834
    const/16 v27, 0x0

    .line 4835
    const v28, 0x7f0200cc

    .line 4836
    const/16 v29, 0x1

    .line 4837
    const/16 v30, 0x3

    .line 4838
    const/16 v31, 0x30

    .line 4839
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4841
    const v5, 0x7f0501ed

    .line 4840
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4842
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4844
    const v5, 0x7f0501e7

    .line 4843
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4845
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4847
    const v5, 0x7f0501e8

    .line 4846
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4848
    const/16 v35, 0x0

    .line 4849
    const/16 v36, 0x1

    .line 4850
    const/16 v37, 0x0

    .line 4851
    const/16 v38, 0xa

    .line 4852
    const/16 v39, 0xb

    .line 4853
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4855
    const v5, 0x7f0501ef

    .line 4854
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4856
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4858
    const v5, 0x7f0501f0

    .line 4857
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4859
    const/16 v42, 0x0

    .line 4860
    const/16 v43, 0x0

    .line 4861
    const/16 v44, 0x0

    .line 4862
    const/16 v45, 0x0

    .line 4863
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4865
    const v5, 0x7f04002d

    .line 4864
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4866
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 4868
    const/16 v49, 0x0

    .line 4869
    const/16 v50, 0x0

    move-object/from16 v48, v54

    .line 4833
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4871
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4873
    const/16 v27, 0x0

    .line 4874
    const/16 v28, 0x0

    .line 4875
    const/16 v29, 0x1

    .line 4876
    const/16 v30, 0x3

    .line 4877
    const/16 v31, 0x30

    .line 4878
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4880
    const v5, 0x7f0501ee

    .line 4879
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4881
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4883
    const v5, 0x7f0501e9

    .line 4882
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4884
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4886
    const v5, 0x7f0501ea

    .line 4885
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4887
    const/16 v35, 0x0

    .line 4888
    const/16 v36, 0x1

    .line 4889
    const/16 v37, 0x0

    .line 4890
    const/16 v38, 0xa

    .line 4891
    const/16 v39, 0xb

    .line 4892
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4894
    const v5, 0x7f0501f1

    .line 4893
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4895
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4897
    const v5, 0x7f0501f2

    .line 4896
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4898
    const/16 v42, 0x0

    .line 4899
    const/16 v43, 0x0

    .line 4900
    const/16 v44, 0x0

    .line 4901
    const/16 v45, 0x0

    .line 4902
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4904
    const v5, 0x7f04002e

    .line 4903
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4905
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 4907
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v48, v56

    .line 4872
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4909
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4911
    const/16 v27, 0x0

    .line 4912
    const/16 v28, 0x0

    .line 4913
    const/16 v29, 0x1

    .line 4914
    const/16 v30, 0x3

    .line 4915
    const/16 v31, 0x30

    .line 4916
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4918
    const v5, 0x7f0501ee

    .line 4917
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4919
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4921
    const v5, 0x7f0501eb

    .line 4920
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4922
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4924
    const v5, 0x7f0501ec

    .line 4923
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4925
    const/16 v35, 0x0

    .line 4926
    const/16 v36, 0x1

    .line 4927
    const/16 v37, 0x0

    .line 4928
    const/16 v38, 0xa

    .line 4929
    const/16 v39, 0xb

    .line 4930
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4932
    const v5, 0x7f0501f3

    .line 4931
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4933
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4935
    const v5, 0x7f0501f4

    .line 4934
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4936
    const/16 v42, 0x0

    .line 4937
    const/16 v43, 0x0

    .line 4938
    const/16 v44, 0x0

    .line 4939
    const/16 v45, 0x0

    .line 4940
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4942
    const v5, 0x7f04002e

    .line 4941
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4943
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 4945
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v48, v53

    .line 4910
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 4946
    goto/16 :goto_1

    .line 4948
    :pswitch_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4950
    const/16 v27, 0x0

    .line 4951
    const v28, 0x7f0200bc

    .line 4952
    const/16 v29, 0x1

    .line 4953
    const/16 v30, 0x3

    .line 4954
    const/16 v31, 0x30

    .line 4955
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4957
    const v5, 0x7f0501fb

    .line 4956
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4958
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4960
    const v5, 0x7f0501f5

    .line 4959
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 4961
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4963
    const v5, 0x7f0501f6

    .line 4962
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 4964
    const/16 v35, 0x0

    .line 4965
    const/16 v36, 0x1

    .line 4966
    const/16 v37, 0x0

    .line 4967
    const/16 v38, 0xc

    .line 4968
    const/16 v39, 0xe

    .line 4969
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4971
    const v5, 0x7f0501fc

    .line 4970
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 4972
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4974
    const v5, 0x7f0501fd

    .line 4973
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 4975
    const/16 v42, 0x0

    .line 4976
    const/16 v43, 0x0

    .line 4977
    const/16 v44, 0x0

    .line 4978
    const/16 v45, 0x0

    .line 4979
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4981
    const v5, 0x7f04002f

    .line 4980
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 4982
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 4984
    const/16 v49, 0x0

    .line 4985
    const/16 v50, 0x0

    move-object/from16 v48, v54

    .line 4949
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 4987
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 4989
    const/16 v27, 0x0

    .line 4990
    const/16 v28, 0x0

    .line 4991
    const/16 v29, 0x1

    .line 4992
    const/16 v30, 0x3

    .line 4993
    const/16 v31, 0x30

    .line 4994
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4996
    const v5, 0x7f0501fb

    .line 4995
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 4997
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4999
    const v5, 0x7f0501f7

    .line 4998
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 5000
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5002
    const v5, 0x7f0501f8

    .line 5001
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 5003
    const/16 v35, 0x0

    .line 5004
    const/16 v36, 0x1

    .line 5005
    const/16 v37, 0x0

    .line 5006
    const/16 v38, 0xc

    .line 5007
    const/16 v39, 0xe

    .line 5008
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5010
    const v5, 0x7f0501fe

    .line 5009
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 5011
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5013
    const v5, 0x7f0501ff

    .line 5012
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 5014
    const/16 v42, 0x0

    .line 5015
    const/16 v43, 0x0

    .line 5016
    const/16 v44, 0x0

    .line 5017
    const/16 v45, 0x0

    .line 5018
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5020
    const v5, 0x7f04002f

    .line 5019
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 5021
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 5023
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v48, v56

    .line 4988
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 5025
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 5027
    const/16 v27, 0x0

    .line 5028
    const/16 v28, 0x0

    .line 5029
    const/16 v29, 0x1

    .line 5030
    const/16 v30, 0x3

    .line 5031
    const/16 v31, 0x30

    .line 5032
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5034
    const v5, 0x7f0501fb

    .line 5033
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 5035
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5037
    const v5, 0x7f0501f9

    .line 5036
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 5038
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5040
    const v5, 0x7f0501fa

    .line 5039
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 5041
    const/16 v35, 0x0

    .line 5042
    const/16 v36, 0x1

    .line 5043
    const/16 v37, 0x0

    .line 5044
    const/16 v38, 0xc

    .line 5045
    const/16 v39, 0xe

    .line 5046
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5048
    const v5, 0x7f050200

    .line 5047
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 5049
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5051
    const v5, 0x7f050201

    .line 5050
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 5052
    const/16 v42, 0x0

    .line 5053
    const/16 v43, 0x0

    .line 5054
    const/16 v44, 0x0

    .line 5055
    const/16 v45, 0x0

    .line 5056
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5058
    const v5, 0x7f04002f

    .line 5057
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 5059
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 5061
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v48, v53

    .line 5026
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 5062
    goto/16 :goto_1

    .line 5064
    :pswitch_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 5066
    const/16 v27, 0x0

    .line 5067
    const v28, 0x7f0200be

    .line 5068
    const/16 v29, 0x1

    .line 5069
    const/16 v30, 0x3

    .line 5070
    const/16 v31, 0x30

    .line 5071
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5073
    const v5, 0x7f050208

    .line 5072
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 5074
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5076
    const v5, 0x7f050202

    .line 5075
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 5077
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5079
    const v5, 0x7f050203

    .line 5078
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 5080
    const/16 v35, 0x0

    .line 5081
    const/16 v36, 0x3

    .line 5082
    const/16 v37, 0x0

    .line 5083
    const/16 v38, 0xc

    .line 5084
    const/16 v39, 0x9

    .line 5085
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5087
    const v5, 0x7f050209

    .line 5086
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 5088
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5090
    const v5, 0x7f05020a

    .line 5089
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 5091
    const/16 v42, 0x0

    .line 5092
    const/16 v43, 0x0

    .line 5093
    const/16 v44, 0x0

    .line 5094
    const/16 v45, 0x0

    .line 5095
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5097
    const v5, 0x7f040030

    .line 5096
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 5098
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 5100
    const/16 v49, 0x0

    .line 5101
    const/16 v50, 0x0

    move-object/from16 v48, v53

    .line 5065
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 5103
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 5105
    const/16 v27, 0x0

    .line 5106
    const/16 v28, 0x0

    .line 5107
    const/16 v29, 0x1

    .line 5108
    const/16 v30, 0x3

    .line 5109
    const/16 v31, 0x30

    .line 5110
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5112
    const v5, 0x7f050208

    .line 5111
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 5113
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5115
    const v5, 0x7f050204

    .line 5114
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 5116
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5118
    const v5, 0x7f050205

    .line 5117
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 5119
    const/16 v35, 0x0

    .line 5120
    const/16 v36, 0x3

    .line 5121
    const/16 v37, 0x0

    .line 5122
    const/16 v38, 0xc

    .line 5123
    const/16 v39, 0x9

    .line 5124
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5126
    const v5, 0x7f05020b

    .line 5125
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 5127
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5129
    const v5, 0x7f05020c

    .line 5128
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 5130
    const/16 v42, 0x0

    .line 5131
    const/16 v43, 0x0

    .line 5132
    const/16 v44, 0x0

    .line 5133
    const/16 v45, 0x0

    .line 5134
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5136
    const v5, 0x7f040030

    .line 5135
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 5137
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 5139
    const/16 v49, 0x0

    const/16 v50, 0x0

    move-object/from16 v48, v56

    .line 5104
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 5141
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v26

    .line 5143
    const/16 v27, 0x0

    .line 5144
    const/16 v28, 0x0

    .line 5145
    const/16 v29, 0x1

    .line 5146
    const/16 v30, 0x3

    .line 5147
    const/16 v31, 0x30

    .line 5148
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5150
    const v5, 0x7f050208

    .line 5149
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v0, v4

    move/from16 v32, v0

    .line 5151
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5153
    const v5, 0x7f050206

    .line 5152
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 5154
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5156
    const v5, 0x7f050207

    .line 5155
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v34

    .line 5157
    const/16 v35, 0x0

    .line 5158
    const/16 v36, 0x3

    .line 5159
    const/16 v37, 0x0

    .line 5160
    const/16 v38, 0xc

    .line 5161
    const/16 v39, 0x9

    .line 5162
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5164
    const v5, 0x7f05020d

    .line 5163
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v40

    .line 5165
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5167
    const v5, 0x7f05020e

    .line 5166
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v41

    .line 5168
    const/16 v42, 0x0

    .line 5169
    const/16 v43, 0x0

    .line 5170
    const/16 v44, 0x0

    .line 5171
    const/16 v45, 0x0

    .line 5172
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 5174
    const v5, 0x7f040030

    .line 5173
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v46

    .line 5175
    const-string v4, "sans-serif"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v47

    .line 5177
    const/16 v49, 0x0

    const/16 v50, 0x1

    move-object/from16 v48, v54

    .line 5142
    invoke-virtual/range {v26 .. v50}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 5178
    goto/16 :goto_1

    .line 5180
    :pswitch_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 5182
    const/4 v5, 0x0

    .line 5183
    const v6, 0x7f0200c0

    .line 5184
    const/4 v7, 0x1

    .line 5185
    const/4 v8, 0x3

    .line 5186
    const/16 v9, 0x30

    .line 5187
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .end local v8    # "data":[I
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 5189
    const v12, 0x7f050215

    .line 5188
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 5190
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 5192
    const v12, 0x7f05020f

    .line 5191
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 5193
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 5195
    const v13, 0x7f050210

    .line 5194
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 5196
    const/4 v13, 0x0

    .line 5197
    const/4 v14, 0x3

    .line 5198
    const/4 v15, 0x0

    .line 5199
    const/16 v16, 0xc

    .line 5200
    const/16 v17, 0xb

    .line 5201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 5203
    const v19, 0x7f050216

    .line 5202
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 5204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 5206
    const v20, 0x7f050217

    .line 5205
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 5207
    const/16 v20, 0x0

    .line 5208
    const/16 v21, 0x0

    .line 5209
    const/16 v22, 0x0

    .line 5210
    const/16 v23, 0x0

    .line 5211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 5213
    const v26, 0x7f040031

    .line 5212
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 5215
    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v26, v53

    .line 5181
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 5217
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 5219
    const/4 v5, 0x0

    .line 5220
    const/4 v6, 0x0

    .line 5221
    const/4 v7, 0x1

    .line 5222
    const/4 v8, 0x3

    .line 5223
    const/16 v9, 0x30

    .line 5224
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 5226
    const v12, 0x7f050215

    .line 5225
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 5227
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 5229
    const v12, 0x7f050211

    .line 5228
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 5230
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 5232
    const v13, 0x7f050212

    .line 5231
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 5233
    const/4 v13, 0x0

    .line 5234
    const/4 v14, 0x3

    .line 5235
    const/4 v15, 0x0

    .line 5236
    const/16 v16, 0xc

    .line 5237
    const/16 v17, 0xb

    .line 5238
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 5240
    const v19, 0x7f050218

    .line 5239
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 5241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 5243
    const v20, 0x7f050219

    .line 5242
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 5244
    const/16 v20, 0x0

    .line 5245
    const/16 v21, 0x0

    .line 5246
    const/16 v22, 0x0

    .line 5247
    const/16 v23, 0x0

    .line 5248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 5250
    const v26, 0x7f040031

    .line 5249
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 5251
    const/16 v27, 0x0

    .line 5252
    const/16 v28, 0x0

    move-object/from16 v26, v56

    .line 5218
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    .line 5254
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    move-result-object v4

    .line 5256
    const/4 v5, 0x0

    .line 5257
    const/4 v6, 0x0

    .line 5258
    const/4 v7, 0x1

    .line 5259
    const/4 v8, 0x3

    .line 5260
    const/16 v9, 0x30

    .line 5261
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 5263
    const v12, 0x7f050215

    .line 5262
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    int-to-float v10, v11

    .line 5264
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 5266
    const v12, 0x7f050213

    .line 5265
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 5267
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 5269
    const v13, 0x7f050214

    .line 5268
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 5270
    const/4 v13, 0x0

    .line 5271
    const/4 v14, 0x3

    .line 5272
    const/4 v15, 0x0

    .line 5273
    const/16 v16, 0xc

    .line 5274
    const/16 v17, 0xb

    .line 5275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 5277
    const v19, 0x7f05021a

    .line 5276
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 5278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 5280
    const v20, 0x7f05021b

    .line 5279
    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 5281
    const/16 v20, 0x0

    .line 5282
    const/16 v21, 0x0

    .line 5283
    const/16 v22, 0x0

    .line 5284
    const/16 v23, 0x0

    .line 5285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 5287
    const v26, 0x7f040031

    .line 5286
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    .line 5289
    const/16 v27, 0x0

    const/16 v28, 0x1

    move-object/from16 v26, v54

    .line 5255
    invoke-virtual/range {v4 .. v28}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setStampResizedStyle(IIIIIFIIIIFIIIIIIIIILandroid/graphics/Typeface;Ljava/lang/String;ZZ)[I

    move-result-object v8

    .line 5254
    .restart local v8    # "data":[I
    goto/16 :goto_1

    .line 989
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
    .end packed-switch
.end method

.method public onPostExcute(Landroid/graphics/Bitmap;)V
    .locals 7
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 892
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 894
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->val$assistantType:I

    .line 895
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getImageEditViewWidth()I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)I

    move-result v2

    .line 896
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v3

    .line 897
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v4

    .line 898
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v5

    iget-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->val$doOpen:Z

    .line 894
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->showTabView(II[IIIZ)V

    .line 900
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->val$initByconfig:Z

    if-nez v0, :cond_2

    .line 901
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v0

    .line 902
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 903
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    .line 904
    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_TYPE1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    .line 916
    :cond_0
    :goto_0
    return-void

    .line 906
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    .line 907
    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STAMP_RECENTLY:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    goto :goto_0

    .line 910
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    .line 911
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v1

    .line 912
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getCurrnetType()I

    move-result v1

    .line 911
    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    goto :goto_0
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 888
    return-void
.end method
