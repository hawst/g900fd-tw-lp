.class public Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonMaxTwoLineTextView;
.super Landroid/widget/TextView;
.source "BottomButtonMaxTwoLineTextView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 12
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonMaxTwoLineTextView;->init(Landroid/content/Context;)V

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonMaxTwoLineTextView;->init(Landroid/content/Context;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonMaxTwoLineTextView;->init(Landroid/content/Context;)V

    .line 24
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x2

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonMaxTwoLineTextView;->getLineCount()I

    move-result v0

    if-ge v0, v1, :cond_1

    .line 33
    const/4 v0, 0x0

    const/high16 v1, -0x3ef00000    # -9.0f

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 39
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 40
    return-void

    .line 35
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonMaxTwoLineTextView;->getLineCount()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 37
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomButtonMaxTwoLineTextView;->setLines(I)V

    goto :goto_0
.end method
