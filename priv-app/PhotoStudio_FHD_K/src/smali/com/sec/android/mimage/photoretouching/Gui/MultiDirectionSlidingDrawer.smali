.class public Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;
.super Landroid/view/ViewGroup;
.source "MultiDirectionSlidingDrawer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$DrawerToggler;,
        Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;,
        Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerOpenListener;,
        Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;,
        Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$SlidingHandler;
    }
.end annotation


# static fields
.field private static final ANIMATION_FRAME_DURATION:I = 0x10

.field private static final COLLAPSED_FULL_CLOSED:I = -0x2712

.field private static final EXPANDED_FULL_OPEN:I = -0x2711

.field public static final LOG_TAG:Ljava/lang/String; = "Sliding"

.field private static final MAXIMUM_ACCELERATION:F = 2000.0f

.field private static final MAXIMUM_MAJOR_VELOCITY:F = 200.0f

.field private static final MAXIMUM_MINOR_VELOCITY:F = 150.0f

.field private static final MAXIMUM_TAP_VELOCITY:F = 100.0f

.field private static final MSG_ANIMATE:I = 0x3e8

.field public static final ORIENTATION_BTT:I = 0x1

.field public static final ORIENTATION_LTR:I = 0x2

.field public static final ORIENTATION_RTL:I = 0x0

.field public static final ORIENTATION_TTB:I = 0x3

.field private static final TAP_THRESHOLD:I = 0x6

.field private static final VELOCITY_UNITS:I = 0x3e8


# instance fields
.field private mAllowSingleTap:Z

.field private mAnimateOnClick:Z

.field private mAnimatedAcceleration:F

.field private mAnimatedVelocity:F

.field private mAnimating:Z

.field private mAnimationLastTime:J

.field private mAnimationPosition:F

.field private mBottomOffset:I

.field private mContent:Landroid/view/View;

.field private final mContentId:I

.field private mCurrentAnimationTime:J

.field private mExpanded:Z

.field private final mFrame:Landroid/graphics/Rect;

.field private mHanderWidth:I

.field private mHandle:Landroid/view/View;

.field private mHandleHeight:I

.field private final mHandleId:I

.field private mHandleWidth:I

.field private final mHandler:Landroid/os/Handler;

.field private mHandlerParentWidth:I

.field private final mInvalidate:Landroid/graphics/Rect;

.field private mInvert:Z

.field private mLocked:Z

.field private mMaximumAcceleration:I

.field private mMaximumMajorVelocity:I

.field private mMaximumMinorVelocity:I

.field private final mMaximumTapVelocity:I

.field private mOnDrawerCloseListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;

.field private mOnDrawerOpenListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerOpenListener;

.field private mOnDrawerScrollListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;

.field private final mTapThreshold:I

.field private mTopOffset:I

.field private mTouchDelta:I

.field private mTracking:Z

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private final mVelocityUnits:I

.field private mVertical:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 158
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 159
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x3

    const/high16 v9, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 174
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 69
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mFrame:Landroid/graphics/Rect;

    .line 70
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvalidate:Landroid/graphics/Rect;

    .line 88
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$SlidingHandler;

    const/4 v8, 0x0

    invoke-direct {v5, p0, v8}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$SlidingHandler;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$SlidingHandler;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandler:Landroid/os/Handler;

    .line 106
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandlerParentWidth:I

    .line 107
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHanderWidth:I

    .line 175
    sget-object v5, Lcom/sec/android/mimage/photoretouching/R$styleable;->MultiDirectionSlidingDrawer:[I

    invoke-virtual {p1, p2, v5, p3, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 177
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    .line 178
    .local v4, "orientation":I
    if-eq v4, v7, :cond_0

    if-eq v4, v10, :cond_0

    move v5, v6

    :goto_0
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    .line 179
    invoke-virtual {v0, v10, v11}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    .line 180
    const/4 v5, 0x4

    invoke-virtual {v0, v5, v11}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    .line 181
    const/4 v5, 0x5

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAllowSingleTap:Z

    .line 182
    const/4 v5, 0x6

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimateOnClick:Z

    .line 183
    if-eq v4, v10, :cond_1

    const/4 v5, 0x2

    if-eq v4, v5, :cond_1

    move v5, v6

    :goto_1
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    .line 185
    invoke-virtual {v0, v7, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 186
    .local v3, "handleId":I
    if-nez v3, :cond_2

    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "The handle attribute is required and must refer to a valid child."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .end local v3    # "handleId":I
    :cond_0
    move v5, v7

    .line 178
    goto :goto_0

    :cond_1
    move v5, v7

    .line 183
    goto :goto_1

    .line 189
    .restart local v3    # "handleId":I
    :cond_2
    const/4 v5, 0x2

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 190
    .local v1, "contentId":I
    if-nez v1, :cond_3

    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "The content attribute is required and must refer to a valid child."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 193
    :cond_3
    if-ne v3, v1, :cond_4

    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "The content and handle attributes must refer to different children."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 195
    :cond_4
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleId:I

    .line 196
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContentId:I

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v2, v5, Landroid/util/DisplayMetrics;->density:F

    .line 199
    .local v2, "density":F
    const/high16 v5, 0x40c00000    # 6.0f

    mul-float/2addr v5, v2

    add-float/2addr v5, v9

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTapThreshold:I

    .line 200
    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v5, v2

    add-float/2addr v5, v9

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumTapVelocity:I

    .line 201
    const/high16 v5, 0x43160000    # 150.0f

    mul-float/2addr v5, v2

    add-float/2addr v5, v9

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMinorVelocity:I

    .line 202
    const/high16 v5, 0x43480000    # 200.0f

    mul-float/2addr v5, v2

    add-float/2addr v5, v9

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMajorVelocity:I

    .line 203
    const/high16 v5, 0x44fa0000    # 2000.0f

    mul-float/2addr v5, v2

    add-float/2addr v5, v9

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumAcceleration:I

    .line 204
    const/high16 v5, 0x447a0000    # 1000.0f

    mul-float/2addr v5, v2

    add-float/2addr v5, v9

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVelocityUnits:I

    .line 206
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_5

    .line 207
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumAcceleration:I

    neg-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumAcceleration:I

    .line 208
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMajorVelocity:I

    neg-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMajorVelocity:I

    .line 209
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMinorVelocity:I

    neg-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMinorVelocity:I

    .line 212
    :cond_5
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 213
    invoke-virtual {p0, v6}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 214
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;)Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mLocked:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;)Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimateOnClick:Z

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;)V
    .locals 0

    .prologue
    .line 766
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->doAnimation()V

    return-void
.end method

.method private animateClose(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 498
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->prepareTracking(I)V

    .line 499
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumAcceleration:I

    int-to-float v0, v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->performFling(IFZ)V

    .line 500
    return-void
.end method

.method private animateOpen(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 504
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->prepareTracking(I)V

    .line 505
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumAcceleration:I

    neg-int v0, v0

    int-to-float v0, v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->performFling(IFZ)V

    .line 506
    return-void
.end method

.method private closeDrawer()V
    .locals 2

    .prologue
    .line 938
    const/16 v0, -0x2712

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->moveHandle(I)V

    .line 939
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 940
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->destroyDrawingCache()V

    .line 942
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    if-nez v0, :cond_1

    .line 948
    :cond_0
    :goto_0
    return-void

    .line 944
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    .line 945
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mOnDrawerCloseListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;

    if-eqz v0, :cond_0

    .line 946
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mOnDrawerCloseListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;->onDrawerClosed()V

    goto :goto_0
.end method

.method private doAnimation()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x10

    const/16 v4, 0x3e8

    const/4 v3, 0x0

    .line 768
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimating:Z

    if-eqz v0, :cond_0

    .line 769
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->incrementAnimation()V

    .line 771
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v0, :cond_4

    .line 773
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationPosition:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 774
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimating:Z

    .line 775
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->closeDrawer()V

    .line 802
    :cond_0
    :goto_0
    return-void

    .line 777
    :cond_1
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationPosition:F

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getHeight()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_3

    .line 778
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimating:Z

    .line 779
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->openDrawer()V

    goto :goto_0

    .line 777
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getWidth()I

    move-result v0

    goto :goto_1

    .line 782
    :cond_3
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationPosition:F

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->moveHandle(I)V

    .line 783
    iget-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mCurrentAnimationTime:J

    add-long/2addr v0, v6

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mCurrentAnimationTime:J

    .line 784
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mCurrentAnimationTime:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    goto :goto_0

    .line 787
    :cond_4
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationPosition:F

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getHeight()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_6

    .line 788
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimating:Z

    .line 789
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->closeDrawer()V

    goto :goto_0

    .line 787
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getWidth()I

    move-result v0

    goto :goto_2

    .line 791
    :cond_6
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationPosition:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_7

    .line 792
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimating:Z

    .line 793
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->openDrawer()V

    goto :goto_0

    .line 796
    :cond_7
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationPosition:F

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->moveHandle(I)V

    .line 797
    iget-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mCurrentAnimationTime:J

    add-long/2addr v0, v6

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mCurrentAnimationTime:J

    .line 798
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mCurrentAnimationTime:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method private incrementAnimation()V
    .locals 8

    .prologue
    .line 806
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 807
    .local v2, "now":J
    iget-wide v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationLastTime:J

    sub-long v6, v2, v6

    long-to-float v6, v6

    const/high16 v7, 0x447a0000    # 1000.0f

    div-float v4, v6, v7

    .line 808
    .local v4, "t":F
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationPosition:F

    .line 809
    .local v1, "position":F
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedVelocity:F

    .line 810
    .local v5, "v":F
    iget-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v6, :cond_0

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedAcceleration:F

    .line 811
    .local v0, "a":F
    :goto_0
    mul-float v6, v5, v4

    add-float/2addr v6, v1

    const/high16 v7, 0x3f000000    # 0.5f

    mul-float/2addr v7, v0

    mul-float/2addr v7, v4

    mul-float/2addr v7, v4

    add-float/2addr v6, v7

    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationPosition:F

    .line 812
    mul-float v6, v0, v4

    add-float/2addr v6, v5

    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedVelocity:F

    .line 813
    iput-wide v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationLastTime:J

    .line 814
    return-void

    .line 810
    .end local v0    # "a":F
    :cond_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedAcceleration:F

    goto :goto_0
.end method

.method private moveHandle(I)V
    .locals 12
    .param p1, "position"    # I

    .prologue
    const/4 v11, 0x0

    const/16 v9, -0x2711

    const/16 v8, -0x2712

    .line 634
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    .line 636
    .local v3, "handle":Landroid/view/View;
    iget-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v7, :cond_6

    .line 637
    if-ne p1, v9, :cond_1

    .line 638
    iget-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v7, :cond_0

    .line 639
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getBottom()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getTop()I

    move-result v8

    sub-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleHeight:I

    sub-int/2addr v7, v8

    invoke-virtual {v3, v7}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 642
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->invalidate()V

    .line 708
    :goto_1
    return-void

    .line 641
    :cond_0
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v3, v7}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto :goto_0

    .line 643
    :cond_1
    if-ne p1, v8, :cond_3

    .line 644
    iget-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v7, :cond_2

    .line 645
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v3, v7}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 649
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->invalidate()V

    goto :goto_1

    .line 647
    :cond_2
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getBottom()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getTop()I

    move-result v8

    sub-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleHeight:I

    sub-int/2addr v7, v8

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v3, v7}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto :goto_2

    .line 652
    :cond_3
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v6

    .line 653
    .local v6, "top":I
    sub-int v1, p1, v6

    .line 654
    .local v1, "deltaY":I
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    if-ge p1, v7, :cond_5

    .line 655
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    sub-int v1, v7, v6

    .line 660
    :cond_4
    :goto_3
    invoke-virtual {v3, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 662
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mFrame:Landroid/graphics/Rect;

    .line 663
    .local v2, "frame":Landroid/graphics/Rect;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvalidate:Landroid/graphics/Rect;

    .line 665
    .local v5, "region":Landroid/graphics/Rect;
    invoke-virtual {v3, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 666
    invoke-virtual {v5, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 668
    iget v7, v2, Landroid/graphics/Rect;->left:I

    iget v8, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v1

    iget v9, v2, Landroid/graphics/Rect;->right:I

    iget v10, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v10, v1

    invoke-virtual {v5, v7, v8, v9, v10}, Landroid/graphics/Rect;->union(IIII)V

    .line 669
    iget v7, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v7, v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getWidth()I

    move-result v8

    iget v9, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v1

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {v5, v11, v7, v8, v9}, Landroid/graphics/Rect;->union(IIII)V

    .line 671
    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_1

    .line 656
    .end local v2    # "frame":Landroid/graphics/Rect;
    .end local v5    # "region":Landroid/graphics/Rect;
    :cond_5
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getBottom()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getTop()I

    move-result v8

    sub-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleHeight:I

    sub-int/2addr v7, v8

    sub-int/2addr v7, v6

    if-le v1, v7, :cond_4

    .line 657
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getBottom()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getTop()I

    move-result v8

    sub-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleHeight:I

    sub-int/2addr v7, v8

    sub-int v1, v7, v6

    goto :goto_3

    .line 674
    .end local v1    # "deltaY":I
    .end local v6    # "top":I
    :cond_6
    if-ne p1, v9, :cond_8

    .line 675
    iget-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v7, :cond_7

    .line 676
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getRight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getLeft()I

    move-result v8

    sub-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleWidth:I

    sub-int/2addr v7, v8

    invoke-virtual {v3, v7}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 679
    :goto_4
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->invalidate()V

    goto/16 :goto_1

    .line 678
    :cond_7
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v3, v7}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_4

    .line 680
    :cond_8
    if-ne p1, v8, :cond_a

    .line 681
    iget-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v7, :cond_9

    .line 682
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v3, v7}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 685
    :goto_5
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->invalidate()V

    goto/16 :goto_1

    .line 684
    :cond_9
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getRight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getLeft()I

    move-result v8

    sub-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleWidth:I

    sub-int/2addr v7, v8

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v3, v7}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_5

    .line 687
    :cond_a
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 688
    .local v4, "left":I
    sub-int v0, p1, v4

    .line 689
    .local v0, "deltaX":I
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    if-ge p1, v7, :cond_c

    .line 690
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    sub-int v0, v7, v4

    .line 694
    :cond_b
    :goto_6
    invoke-virtual {v3, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 696
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mFrame:Landroid/graphics/Rect;

    .line 697
    .restart local v2    # "frame":Landroid/graphics/Rect;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvalidate:Landroid/graphics/Rect;

    .line 699
    .restart local v5    # "region":Landroid/graphics/Rect;
    invoke-virtual {v3, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 700
    invoke-virtual {v5, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 702
    iget v7, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v7, v0

    iget v8, v2, Landroid/graphics/Rect;->top:I

    iget v9, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v9, v0

    iget v10, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v5, v7, v8, v9, v10}, Landroid/graphics/Rect;->union(IIII)V

    .line 703
    iget v7, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v7, v0

    iget v8, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v0

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getHeight()I

    move-result v9

    invoke-virtual {v5, v7, v11, v8, v9}, Landroid/graphics/Rect;->union(IIII)V

    .line 705
    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->invalidate(Landroid/graphics/Rect;)V

    goto/16 :goto_1

    .line 691
    .end local v2    # "frame":Landroid/graphics/Rect;
    .end local v5    # "region":Landroid/graphics/Rect;
    :cond_c
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getRight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getLeft()I

    move-result v8

    sub-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleWidth:I

    sub-int/2addr v7, v8

    sub-int/2addr v7, v4

    if-le v0, v7, :cond_b

    .line 692
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getRight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getLeft()I

    move-result v8

    sub-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleWidth:I

    sub-int/2addr v7, v8

    sub-int v0, v7, v4

    goto :goto_6
.end method

.method private openDrawer()V
    .locals 2

    .prologue
    .line 952
    const/16 v0, -0x2711

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->moveHandle(I)V

    .line 953
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 955
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    if-eqz v0, :cond_1

    .line 962
    :cond_0
    :goto_0
    return-void

    .line 957
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    .line 959
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mOnDrawerOpenListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerOpenListener;

    if-eqz v0, :cond_0

    .line 960
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mOnDrawerOpenListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerOpenListener;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerOpenListener;->onDrawerOpened()V

    goto :goto_0
.end method

.method private performFling(IFZ)V
    .locals 12
    .param p1, "position"    # I
    .param p2, "velocity"    # F
    .param p3, "always"    # Z

    .prologue
    .line 510
    int-to-float v5, p1

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationPosition:F

    .line 511
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedVelocity:F

    .line 517
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    if-eqz v5, :cond_11

    .line 519
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getBottom()I

    move-result v0

    .line 520
    .local v0, "bottom":I
    :goto_0
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v5, :cond_3

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleHeight:I

    .line 523
    .local v4, "handleHeight":I
    :goto_1
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_5

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMajorVelocity:I

    int-to-float v5, v5

    cmpg-float v5, p2, v5

    if-gez v5, :cond_4

    const/4 v1, 0x1

    .line 524
    .local v1, "c1":Z
    :goto_2
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_8

    add-int v5, p1, v4

    sub-int v5, v0, v5

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    add-int/2addr v5, v8

    if-le v5, v4, :cond_7

    const/4 v2, 0x1

    .line 525
    .local v2, "c2":Z
    :goto_3
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_c

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMajorVelocity:I

    neg-int v5, v5

    int-to-float v5, v5

    cmpg-float v5, p2, v5

    if-gez v5, :cond_b

    const/4 v3, 0x1

    .line 527
    .local v3, "c3":Z
    :goto_4
    if-nez p3, :cond_0

    if-nez v1, :cond_0

    if-eqz v2, :cond_f

    if-eqz v3, :cond_f

    .line 529
    :cond_0
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumAcceleration:I

    int-to-float v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedAcceleration:F

    .line 530
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_e

    .line 532
    const/4 v5, 0x0

    cmpl-float v5, p2, v5

    if-lez v5, :cond_1

    .line 533
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedVelocity:F

    .line 593
    .end local v0    # "bottom":I
    .end local v4    # "handleHeight":I
    :cond_1
    :goto_5
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 594
    .local v6, "now":J
    iput-wide v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationLastTime:J

    .line 595
    const-wide/16 v8, 0x10

    add-long/2addr v8, v6

    iput-wide v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mCurrentAnimationTime:J

    .line 596
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimating:Z

    .line 597
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandler:Landroid/os/Handler;

    const/16 v8, 0x3e8

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 598
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandler:Landroid/os/Handler;

    const/16 v9, 0x3e8

    invoke-virtual {v8, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v8

    iget-wide v10, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mCurrentAnimationTime:J

    invoke-virtual {v5, v8, v10, v11}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    .line 599
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->stopTracking()V

    .line 600
    const-string v5, "JW performFling"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 601
    return-void

    .line 519
    .end local v1    # "c1":Z
    .end local v2    # "c2":Z
    .end local v3    # "c3":Z
    .end local v6    # "now":J
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getRight()I

    move-result v0

    goto :goto_0

    .line 520
    .restart local v0    # "bottom":I
    :cond_3
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleWidth:I

    goto :goto_1

    .line 523
    .restart local v4    # "handleHeight":I
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    :cond_5
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMajorVelocity:I

    int-to-float v5, v5

    cmpl-float v5, p2, v5

    if-lez v5, :cond_6

    const/4 v1, 0x1

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    goto :goto_2

    .line 524
    .restart local v1    # "c1":Z
    :cond_7
    const/4 v2, 0x0

    goto :goto_3

    :cond_8
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v5, :cond_9

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleHeight:I

    :goto_6
    add-int/2addr v5, v8

    if-le p1, v5, :cond_a

    const/4 v2, 0x1

    goto :goto_3

    :cond_9
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleWidth:I

    goto :goto_6

    :cond_a
    const/4 v2, 0x0

    goto :goto_3

    .line 525
    .restart local v2    # "c2":Z
    :cond_b
    const/4 v3, 0x0

    goto :goto_4

    :cond_c
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMajorVelocity:I

    neg-int v5, v5

    int-to-float v5, v5

    cmpl-float v5, p2, v5

    if-lez v5, :cond_d

    const/4 v3, 0x1

    goto :goto_4

    :cond_d
    const/4 v3, 0x0

    goto :goto_4

    .line 536
    .restart local v3    # "c3":Z
    :cond_e
    const/4 v5, 0x0

    cmpg-float v5, p2, v5

    if-gez v5, :cond_1

    .line 537
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedVelocity:F

    goto :goto_5

    .line 543
    :cond_f
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumAcceleration:I

    neg-int v5, v5

    int-to-float v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedAcceleration:F

    .line 545
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_10

    .line 546
    const/4 v5, 0x0

    cmpg-float v5, p2, v5

    if-gez v5, :cond_1

    .line 547
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedVelocity:F

    goto :goto_5

    .line 550
    :cond_10
    const/4 v5, 0x0

    cmpl-float v5, p2, v5

    if-lez v5, :cond_1

    .line 551
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedVelocity:F

    goto/16 :goto_5

    .line 559
    .end local v0    # "bottom":I
    .end local v1    # "c1":Z
    .end local v2    # "c2":Z
    .end local v3    # "c3":Z
    .end local v4    # "handleHeight":I
    :cond_11
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_14

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMajorVelocity:I

    int-to-float v5, v5

    cmpg-float v5, p2, v5

    if-gez v5, :cond_13

    const/4 v1, 0x1

    .line 560
    .restart local v1    # "c1":Z
    :goto_7
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_18

    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v5, :cond_16

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getHeight()I

    move-result v5

    :goto_8
    div-int/lit8 v5, v5, 0x2

    if-ge p1, v5, :cond_17

    const/4 v2, 0x1

    .line 561
    .restart local v2    # "c2":Z
    :goto_9
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_1c

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMajorVelocity:I

    neg-int v5, v5

    int-to-float v5, v5

    cmpg-float v5, p2, v5

    if-gez v5, :cond_1b

    const/4 v3, 0x1

    .line 566
    .restart local v3    # "c3":Z
    :goto_a
    if-nez p3, :cond_1f

    if-nez v1, :cond_12

    if-eqz v2, :cond_1f

    if-eqz v3, :cond_1f

    .line 567
    :cond_12
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumAcceleration:I

    int-to-float v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedAcceleration:F

    .line 569
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_1e

    .line 570
    const/4 v5, 0x0

    cmpl-float v5, p2, v5

    if-lez v5, :cond_1

    .line 571
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedVelocity:F

    goto/16 :goto_5

    .line 559
    .end local v1    # "c1":Z
    .end local v2    # "c2":Z
    .end local v3    # "c3":Z
    :cond_13
    const/4 v1, 0x0

    goto :goto_7

    :cond_14
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMajorVelocity:I

    int-to-float v5, v5

    cmpl-float v5, p2, v5

    if-lez v5, :cond_15

    const/4 v1, 0x1

    goto :goto_7

    :cond_15
    const/4 v1, 0x0

    goto :goto_7

    .line 560
    .restart local v1    # "c1":Z
    :cond_16
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getWidth()I

    move-result v5

    goto :goto_8

    :cond_17
    const/4 v2, 0x0

    goto :goto_9

    :cond_18
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v5, :cond_19

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getHeight()I

    move-result v5

    :goto_b
    div-int/lit8 v5, v5, 0x2

    if-le p1, v5, :cond_1a

    const/4 v2, 0x1

    goto :goto_9

    :cond_19
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getWidth()I

    move-result v5

    goto :goto_b

    :cond_1a
    const/4 v2, 0x0

    goto :goto_9

    .line 561
    .restart local v2    # "c2":Z
    :cond_1b
    const/4 v3, 0x0

    goto :goto_a

    :cond_1c
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMajorVelocity:I

    neg-int v5, v5

    int-to-float v5, v5

    cmpl-float v5, p2, v5

    if-lez v5, :cond_1d

    const/4 v3, 0x1

    goto :goto_a

    :cond_1d
    const/4 v3, 0x0

    goto :goto_a

    .line 574
    .restart local v3    # "c3":Z
    :cond_1e
    const/4 v5, 0x0

    cmpg-float v5, p2, v5

    if-gez v5, :cond_1

    .line 575
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedVelocity:F

    goto/16 :goto_5

    .line 579
    :cond_1f
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumAcceleration:I

    neg-int v5, v5

    int-to-float v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedAcceleration:F

    .line 581
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_20

    .line 582
    const/4 v5, 0x0

    cmpg-float v5, p2, v5

    if-gez v5, :cond_1

    .line 583
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedVelocity:F

    goto/16 :goto_5

    .line 586
    :cond_20
    const/4 v5, 0x0

    cmpl-float v5, p2, v5

    if-lez v5, :cond_1

    .line 587
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedVelocity:F

    goto/16 :goto_5
.end method

.method private prepareContent()V
    .locals 10

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v9, 0x0

    .line 712
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimating:Z

    if-eqz v5, :cond_0

    .line 749
    :goto_0
    return-void

    .line 716
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    .line 717
    .local v0, "content":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isLayoutRequested()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 719
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v5, :cond_3

    .line 720
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleHeight:I

    .line 721
    .local v1, "handleHeight":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getBottom()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getTop()I

    move-result v6

    sub-int/2addr v5, v6

    sub-int/2addr v5, v1

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    sub-int v3, v5, v6

    .line 722
    .local v3, "height":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getRight()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getLeft()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v0, v5, v6}, Landroid/view/View;->measure(II)V

    .line 726
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_2

    .line 727
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {v0, v9, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    .line 745
    .end local v1    # "handleHeight":I
    .end local v3    # "height":I
    :cond_1
    :goto_1
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ViewTreeObserver;->dispatchOnPreDraw()Z

    .line 746
    invoke-virtual {v0}, Landroid/view/View;->buildDrawingCache()V

    .line 748
    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 729
    .restart local v1    # "handleHeight":I
    .restart local v3    # "height":I
    :cond_2
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    add-int/2addr v5, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    add-int/2addr v7, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {v0, v9, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    goto :goto_1

    .line 733
    .end local v1    # "handleHeight":I
    .end local v3    # "height":I
    :cond_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 734
    .local v2, "handleWidth":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getRight()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getLeft()I

    move-result v6

    sub-int/2addr v5, v6

    sub-int/2addr v5, v2

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    sub-int v4, v5, v6

    .line 735
    .local v4, "width":I
    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getBottom()I

    move-result v6

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getTop()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v0, v5, v6}, Landroid/view/View;->measure(II)V

    .line 737
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_4

    .line 738
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {v0, v5, v9, v6, v7}, Landroid/view/View;->layout(IIII)V

    goto :goto_1

    .line 740
    :cond_4
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    add-int/2addr v5, v2

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    add-int/2addr v6, v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {v0, v5, v9, v6, v7}, Landroid/view/View;->layout(IIII)V

    goto :goto_1
.end method

.method private prepareTracking(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/16 v7, 0x3e8

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 605
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTracking:Z

    .line 606
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 607
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    if-eqz v5, :cond_0

    move v2, v3

    .line 609
    .local v2, "opening":Z
    :goto_0
    if-eqz v2, :cond_3

    .line 610
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumAcceleration:I

    int-to-float v3, v3

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedAcceleration:F

    .line 611
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMajorVelocity:I

    int-to-float v3, v3

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimatedVelocity:F

    .line 612
    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v3, :cond_1

    .line 613
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    int-to-float v3, v3

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationPosition:F

    .line 616
    :goto_1
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationPosition:F

    float-to-int v3, v3

    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->moveHandle(I)V

    .line 617
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimating:Z

    .line 618
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 619
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 620
    .local v0, "now":J
    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationLastTime:J

    .line 621
    const-wide/16 v6, 0x10

    add-long/2addr v6, v0

    iput-wide v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mCurrentAnimationTime:J

    .line 622
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimating:Z

    .line 630
    .end local v0    # "now":J
    :goto_2
    return-void

    .end local v2    # "opening":Z
    :cond_0
    move v2, v4

    .line 607
    goto :goto_0

    .line 615
    .restart local v2    # "opening":Z
    :cond_1
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getHeight()I

    move-result v3

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleHeight:I

    sub-int/2addr v3, v6

    :goto_3
    add-int/2addr v3, v5

    int-to-float v3, v3

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimationPosition:F

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getWidth()I

    move-result v3

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleWidth:I

    sub-int/2addr v3, v6

    goto :goto_3

    .line 624
    :cond_3
    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimating:Z

    if-eqz v4, :cond_4

    .line 625
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimating:Z

    .line 626
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 628
    :cond_4
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->moveHandle(I)V

    goto :goto_2
.end method

.method private stopTracking()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 753
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 754
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTracking:Z

    .line 756
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mOnDrawerScrollListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;

    if-eqz v0, :cond_0

    .line 757
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mOnDrawerScrollListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;->onScrollEnded()V

    .line 760
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 761
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 762
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 764
    :cond_1
    return-void
.end method


# virtual methods
.method public animateClose()V
    .locals 2

    .prologue
    .line 897
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->prepareContent()V

    .line 898
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mOnDrawerScrollListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;

    .line 899
    .local v0, "scrollListener":Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;
    if-eqz v0, :cond_0

    .line 900
    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;->onScrollStarted()V

    .line 902
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->animateClose(I)V

    .line 904
    if-eqz v0, :cond_1

    .line 905
    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;->onScrollEnded()V

    .line 907
    :cond_1
    return-void

    .line 902
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    goto :goto_0
.end method

.method public animateOpen()V
    .locals 2

    .prologue
    .line 920
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->prepareContent()V

    .line 921
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mOnDrawerScrollListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;

    .line 922
    .local v0, "scrollListener":Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;
    if-eqz v0, :cond_0

    .line 923
    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;->onScrollStarted()V

    .line 925
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 926
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->animateOpen(I)V

    .line 928
    :cond_1
    const/16 v1, 0x20

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->sendAccessibilityEvent(I)V

    .line 930
    if-eqz v0, :cond_2

    .line 931
    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;->onScrollEnded()V

    .line 933
    :cond_2
    const-string v1, "JW animateOpen"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 934
    return-void

    .line 926
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    goto :goto_0
.end method

.method public animateToggle()V
    .locals 2

    .prologue
    .line 848
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW animateToggle: mExpanded="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 849
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    if-nez v0, :cond_0

    .line 850
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->animateOpen()V

    .line 854
    :goto_0
    return-void

    .line 852
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->animateClose()V

    goto :goto_0
.end method

.method public close()V
    .locals 0

    .prologue
    .line 881
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->closeDrawer()V

    .line 882
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->invalidate()V

    .line 883
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->requestLayout()V

    .line 884
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getDrawingTime()J

    move-result-wide v2

    .line 269
    .local v2, "drawingTime":J
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    .line 270
    .local v1, "handle":Landroid/view/View;
    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    .line 272
    .local v4, "isVertical":Z
    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 274
    iget-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTracking:Z

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimating:Z

    if-eqz v6, :cond_b

    .line 275
    :cond_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 276
    .local v0, "cache":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_5

    .line 277
    if-eqz v4, :cond_3

    .line 278
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_2

    .line 279
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getBottom()I

    move-result v6

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getTop()I

    move-result v7

    sub-int/2addr v6, v7

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleHeight:I

    add-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {p1, v0, v8, v5, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 296
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->invalidate()V

    .line 300
    .end local v0    # "cache":Landroid/graphics/Bitmap;
    :cond_1
    :goto_1
    return-void

    .line 281
    .restart local v0    # "cache":Landroid/graphics/Bitmap;
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1, v0, v8, v5, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 284
    :cond_3
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v5, :cond_4

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int/2addr v5, v6

    :goto_2
    int-to-float v5, v5

    invoke-virtual {p1, v0, v5, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v5

    goto :goto_2

    .line 287
    :cond_5
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 288
    iget-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v6, :cond_8

    .line 289
    if-eqz v4, :cond_7

    move v6, v5

    :goto_3
    int-to-float v6, v6

    if-eqz v4, :cond_6

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v5

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    sub-int/2addr v5, v7

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    sub-int/2addr v5, v7

    :cond_6
    int-to-float v5, v5

    invoke-virtual {p1, v6, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 293
    :goto_4
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    invoke-virtual {p0, p1, v5, v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 294
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    .line 289
    :cond_7
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v6

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    sub-int/2addr v6, v7

    goto :goto_3

    .line 291
    :cond_8
    if-eqz v4, :cond_a

    move v6, v5

    :goto_5
    int-to-float v6, v6

    if-eqz v4, :cond_9

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v5

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    sub-int/2addr v5, v7

    :cond_9
    int-to-float v5, v5

    invoke-virtual {p1, v6, v5}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_4

    :cond_a
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v6

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    sub-int/2addr v6, v7

    goto :goto_5

    .line 297
    .end local v0    # "cache":Landroid/graphics/Bitmap;
    :cond_b
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    if-eqz v5, :cond_1

    .line 298
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    invoke-virtual {p0, p1, v5, v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    goto :goto_1
.end method

.method public getContent()Landroid/view/View;
    .locals 1

    .prologue
    .line 1029
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    return-object v0
.end method

.method public getHandle()Landroid/view/View;
    .locals 1

    .prologue
    .line 1018
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    return-object v0
.end method

.method public isMoving()Z
    .locals 1

    .prologue
    .line 1069
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTracking:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimating:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isOpened()Z
    .locals 1

    .prologue
    .line 1059
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    return v0
.end method

.method public lock()V
    .locals 1

    .prologue
    .line 1049
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mLocked:Z

    .line 1050
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 219
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleId:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    .line 220
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The handle attribute is must refer to an existing child."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$DrawerToggler;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$DrawerToggler;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$DrawerToggler;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContentId:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    .line 224
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The content attribute is must refer to an existing child."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 227
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    .line 354
    const-string v8, "JW onInterceptTouchEvent"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 355
    iget-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mLocked:Z

    if-eqz v8, :cond_1

    .line 392
    :cond_0
    :goto_0
    return v7

    .line 357
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 359
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    .line 360
    .local v5, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    .line 362
    .local v6, "y":F
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mFrame:Landroid/graphics/Rect;

    .line 363
    .local v1, "frame":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    .line 365
    .local v2, "handle":Landroid/view/View;
    invoke-virtual {v2, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 366
    iget-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTracking:Z

    if-nez v8, :cond_2

    float-to-int v8, v5

    float-to-int v9, v6

    invoke-virtual {v1, v8, v9}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-eqz v8, :cond_3

    :cond_2
    float-to-int v8, v5

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandlerParentWidth:I

    iget v10, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHanderWidth:I

    sub-int/2addr v9, v10

    if-ge v8, v9, :cond_4

    :cond_3
    const/4 v7, 0x0

    goto :goto_0

    .line 368
    :cond_4
    if-nez v0, :cond_0

    .line 369
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTracking:Z

    .line 371
    invoke-virtual {v2, v7}, Landroid/view/View;->setPressed(Z)V

    .line 373
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->prepareContent()V

    .line 376
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mOnDrawerScrollListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;

    if-eqz v8, :cond_5

    .line 377
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mOnDrawerScrollListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;

    invoke-interface {v8}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;->onScrollStarted()V

    .line 380
    :cond_5
    iget-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v8, :cond_6

    .line 381
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v4

    .line 382
    .local v4, "top":I
    float-to-int v8, v6

    sub-int/2addr v8, v4

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTouchDelta:I

    .line 383
    invoke-direct {p0, v4}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->prepareTracking(I)V

    .line 389
    .end local v4    # "top":I
    :goto_1
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v8, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 385
    :cond_6
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 386
    .local v3, "left":I
    float-to-int v8, v5

    sub-int/2addr v8, v3

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTouchDelta:I

    .line 387
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->prepareTracking(I)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 13
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 307
    const-string v8, "JW slidingDrawer onLayout"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 308
    iget-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTracking:Z

    if-eqz v8, :cond_0

    .line 349
    :goto_0
    return-void

    .line 310
    :cond_0
    sub-int v7, p4, p2

    .line 311
    .local v7, "width":I
    sub-int v6, p5, p3

    .line 313
    .local v6, "height":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    .line 315
    .local v1, "handle":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 316
    .local v5, "handleWidth":I
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 323
    .local v2, "handleHeight":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    .line 325
    .local v0, "content":Landroid/view/View;
    iget-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v8, :cond_4

    .line 326
    sub-int v8, v7, v5

    div-int/lit8 v3, v8, 0x2

    .line 327
    .local v3, "handleLeft":I
    iget-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v8, :cond_2

    .line 329
    iget-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    if-eqz v8, :cond_1

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    sub-int v8, v6, v8

    sub-int v4, v8, v2

    .line 330
    .local v4, "handleTop":I
    :goto_1
    const/4 v8, 0x0

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v11, v12

    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/view/View;->layout(IIII)V

    .line 346
    :goto_2
    add-int v8, v3, v5

    add-int v9, v4, v2

    invoke-virtual {v1, v3, v4, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 347
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v8

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleHeight:I

    .line 348
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v8

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleWidth:I

    goto :goto_0

    .line 329
    .end local v4    # "handleTop":I
    :cond_1
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    goto :goto_1

    .line 332
    :cond_2
    iget-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    if-eqz v8, :cond_3

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    .line 333
    .restart local v4    # "handleTop":I
    :goto_3
    const/4 v8, 0x0

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    add-int/2addr v9, v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    add-int/2addr v11, v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v11, v12

    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/view/View;->layout(IIII)V

    goto :goto_2

    .line 332
    .end local v4    # "handleTop":I
    :cond_3
    sub-int v8, v6, v2

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    add-int v4, v8, v9

    goto :goto_3

    .line 336
    .end local v3    # "handleLeft":I
    :cond_4
    sub-int v8, v6, v2

    div-int/lit8 v4, v8, 0x2

    .line 337
    .restart local v4    # "handleTop":I
    iget-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    if-eqz v8, :cond_6

    .line 338
    iget-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    if-eqz v8, :cond_5

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    sub-int v8, v7, v8

    sub-int v3, v8, v5

    .line 339
    .restart local v3    # "handleLeft":I
    :goto_4
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    const/4 v9, 0x0

    iget v10, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    add-int/2addr v10, v11

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/view/View;->layout(IIII)V

    goto :goto_2

    .line 338
    .end local v3    # "handleLeft":I
    :cond_5
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    goto :goto_4

    .line 341
    :cond_6
    iget-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    if-eqz v8, :cond_7

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    .line 342
    .restart local v3    # "handleLeft":I
    :goto_5
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    add-int/2addr v8, v5

    const/4 v9, 0x0

    iget v10, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    add-int/2addr v10, v5

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    add-int/2addr v10, v11

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/view/View;->layout(IIII)V

    goto :goto_2

    .line 341
    .end local v3    # "handleLeft":I
    :cond_7
    sub-int v8, v7, v5

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    add-int v3, v8, v9

    goto :goto_5
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 232
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    if-eqz v8, :cond_3

    .line 234
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    .line 235
    .local v6, "widthSpecMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 237
    .local v7, "widthSpecSize":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 238
    .local v3, "heightSpecMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 240
    .local v4, "heightSpecSize":I
    if-eqz v6, :cond_0

    if-nez v3, :cond_1

    :cond_0
    new-instance v8, Ljava/lang/RuntimeException;

    .line 241
    const-string v9, "SlidingDrawer cannot have UNSPECIFIED dimensions"

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 243
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    .line 244
    .local v1, "handle":Landroid/view/View;
    invoke-virtual {p0, v1, p1, p2}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->measureChild(Landroid/view/View;II)V

    .line 247
    :try_start_0
    iget-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    if-eqz v8, :cond_4

    .line 248
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    sub-int v8, v4, v8

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    sub-int v2, v8, v9

    .line 249
    .local v2, "height":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    if-eqz v8, :cond_2

    .line 250
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v7, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    const/high16 v10, 0x40000000    # 2.0f

    invoke-static {v2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/view/View;->measure(II)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 260
    .end local v2    # "height":I
    :cond_2
    :goto_0
    invoke-virtual {p0, v7, v4}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->setMeasuredDimension(II)V

    .line 263
    .end local v1    # "handle":Landroid/view/View;
    .end local v3    # "heightSpecMode":I
    .end local v4    # "heightSpecSize":I
    .end local v6    # "widthSpecMode":I
    .end local v7    # "widthSpecSize":I
    :cond_3
    return-void

    .line 252
    .restart local v1    # "handle":Landroid/view/View;
    .restart local v3    # "heightSpecMode":I
    .restart local v4    # "heightSpecSize":I
    .restart local v6    # "widthSpecMode":I
    .restart local v7    # "widthSpecSize":I
    :cond_4
    :try_start_1
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    sub-int v8, v7, v8

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    sub-int v5, v8, v9

    .line 253
    .local v5, "width":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    if-eqz v8, :cond_2

    .line 254
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mContent:Landroid/view/View;

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    const/high16 v10, 0x40000000    # 2.0f

    invoke-static {v4, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/view/View;->measure(II)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 256
    .end local v5    # "width":I
    :catch_0
    move-exception v0

    .line 257
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 22
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 398
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "JW slidingDrawer onTouchEvent:event.getX()="

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 399
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mLocked:Z

    move/from16 v17, v0

    if-eqz v17, :cond_0

    .line 400
    const/16 v17, 0x0

    .line 493
    :goto_0
    return v17

    .line 402
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTracking:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    .line 403
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 404
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 405
    .local v2, "action":I
    packed-switch v2, :pswitch_data_0

    .line 493
    .end local v2    # "action":I
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTracking:Z

    move/from16 v17, v0

    if-nez v17, :cond_22

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAnimating:Z

    move/from16 v17, v0

    if-nez v17, :cond_22

    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v17

    if-nez v17, :cond_22

    const/16 v17, 0x0

    goto :goto_0

    .line 407
    .restart local v2    # "action":I
    :pswitch_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v17

    :goto_2
    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTouchDelta:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->moveHandle(I)V

    goto :goto_1

    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v17

    goto :goto_2

    .line 411
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 412
    .local v13, "velocityTracker":Landroid/view/VelocityTracker;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVelocityUnits:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 414
    invoke-virtual {v13}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v16

    .line 415
    .local v16, "yVelocity":F
    invoke-virtual {v13}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v15

    .line 418
    .local v15, "xVelocity":F
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mVertical:Z

    .line 419
    .local v14, "vertical":Z
    if-eqz v14, :cond_a

    .line 420
    const/16 v17, 0x0

    cmpg-float v17, v16, v17

    if-gez v17, :cond_9

    const/4 v11, 0x1

    .line 421
    .local v11, "negative":Z
    :goto_3
    const/16 v17, 0x0

    cmpg-float v17, v15, v17

    if-gez v17, :cond_3

    .line 422
    neg-float v15, v15

    .line 425
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMinorVelocity:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    cmpl-float v17, v15, v17

    if-gtz v17, :cond_5

    :cond_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    move/from16 v17, v0

    if-eqz v17, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMinorVelocity:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    cmpg-float v17, v15, v17

    if-gez v17, :cond_6

    .line 426
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMinorVelocity:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v15, v0

    .line 439
    :cond_6
    :goto_4
    float-to-double v0, v15

    move-wide/from16 v18, v0

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v12, v0

    .line 440
    .local v12, "velocity":F
    if-eqz v11, :cond_7

    .line 441
    neg-float v12, v12

    .line 444
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getTop()I

    move-result v10

    .line 445
    .local v10, "handleTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getLeft()I

    move-result v8

    .line 446
    .local v8, "handleLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getBottom()I

    move-result v7

    .line 447
    .local v7, "handleBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandle:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getRight()I

    move-result v9

    .line 449
    .local v9, "handleRight":I
    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumTapVelocity:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    cmpg-float v17, v17, v18

    if-gez v17, :cond_20

    .line 455
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    move/from16 v17, v0

    if-eqz v17, :cond_13

    .line 456
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    move/from16 v17, v0

    if-eqz v17, :cond_f

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getBottom()I

    move-result v17

    sub-int v17, v17, v7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTapThreshold:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_f

    const/4 v3, 0x1

    .line 457
    .local v3, "c1":Z
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    move/from16 v17, v0

    if-nez v17, :cond_10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleHeight:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTapThreshold:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    move/from16 v0, v17

    if-ge v10, v0, :cond_10

    const/4 v4, 0x1

    .line 458
    .local v4, "c2":Z
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    move/from16 v17, v0

    if-eqz v17, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getRight()I

    move-result v17

    sub-int v17, v17, v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTapThreshold:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_11

    const/4 v5, 0x1

    .line 459
    .local v5, "c3":Z
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    move/from16 v17, v0

    if-nez v17, :cond_12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleWidth:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTapThreshold:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move/from16 v0, v17

    if-ge v8, v0, :cond_12

    const/4 v6, 0x1

    .line 469
    .local v6, "c4":Z
    :goto_8
    if-eqz v14, :cond_18

    if-nez v3, :cond_8

    if-eqz v4, :cond_19

    .line 471
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mAllowSingleTap:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1d

    .line 472
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->playSoundEffect(I)V

    .line 474
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1b

    .line 475
    if-eqz v14, :cond_1a

    .end local v10    # "handleTop":I
    :goto_9
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->animateClose(I)V

    goto/16 :goto_1

    .line 420
    .end local v3    # "c1":Z
    .end local v4    # "c2":Z
    .end local v5    # "c3":Z
    .end local v6    # "c4":Z
    .end local v7    # "handleBottom":I
    .end local v8    # "handleLeft":I
    .end local v9    # "handleRight":I
    .end local v11    # "negative":Z
    .end local v12    # "velocity":F
    :cond_9
    const/4 v11, 0x0

    goto/16 :goto_3

    .line 429
    :cond_a
    const/16 v17, 0x0

    cmpg-float v17, v15, v17

    if-gez v17, :cond_e

    const/4 v11, 0x1

    .line 430
    .restart local v11    # "negative":Z
    :goto_a
    const/16 v17, 0x0

    cmpg-float v17, v16, v17

    if-gez v17, :cond_b

    .line 431
    move/from16 v0, v16

    neg-float v0, v0

    move/from16 v16, v0

    .line 434
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    move/from16 v17, v0

    if-nez v17, :cond_c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMinorVelocity:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    cmpl-float v17, v16, v17

    if-gtz v17, :cond_d

    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mInvert:Z

    move/from16 v17, v0

    if-eqz v17, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMinorVelocity:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    cmpg-float v17, v16, v17

    if-gez v17, :cond_6

    .line 435
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mMaximumMinorVelocity:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v16, v0

    goto/16 :goto_4

    .line 429
    .end local v11    # "negative":Z
    :cond_e
    const/4 v11, 0x0

    goto :goto_a

    .line 456
    .restart local v7    # "handleBottom":I
    .restart local v8    # "handleLeft":I
    .restart local v9    # "handleRight":I
    .restart local v10    # "handleTop":I
    .restart local v11    # "negative":Z
    .restart local v12    # "velocity":F
    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 457
    .restart local v3    # "c1":Z
    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_6

    .line 458
    .restart local v4    # "c2":Z
    :cond_11
    const/4 v5, 0x0

    goto/16 :goto_7

    .line 459
    .restart local v5    # "c3":Z
    :cond_12
    const/4 v6, 0x0

    goto/16 :goto_8

    .line 461
    .end local v3    # "c1":Z
    .end local v4    # "c2":Z
    .end local v5    # "c3":Z
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    move/from16 v17, v0

    if-eqz v17, :cond_14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTapThreshold:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move/from16 v0, v17

    if-ge v10, v0, :cond_14

    const/4 v3, 0x1

    .line 462
    .restart local v3    # "c1":Z
    :goto_b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    move/from16 v17, v0

    if-nez v17, :cond_15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    move/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getBottom()I

    move-result v18

    add-int v17, v17, v18

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getTop()I

    move-result v18

    sub-int v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleHeight:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTapThreshold:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    move/from16 v0, v17

    if-le v10, v0, :cond_15

    const/4 v4, 0x1

    .line 463
    .restart local v4    # "c2":Z
    :goto_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    move/from16 v17, v0

    if-eqz v17, :cond_16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTapThreshold:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTopOffset:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move/from16 v0, v17

    if-ge v8, v0, :cond_16

    const/4 v5, 0x1

    .line 464
    .restart local v5    # "c3":Z
    :goto_d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    move/from16 v17, v0

    if-nez v17, :cond_17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mBottomOffset:I

    move/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getRight()I

    move-result v18

    add-int v17, v17, v18

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->getLeft()I

    move-result v18

    sub-int v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandleWidth:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mTapThreshold:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    move/from16 v0, v17

    if-le v8, v0, :cond_17

    const/4 v6, 0x1

    .restart local v6    # "c4":Z
    :goto_e
    goto/16 :goto_8

    .line 461
    .end local v3    # "c1":Z
    .end local v4    # "c2":Z
    .end local v5    # "c3":Z
    .end local v6    # "c4":Z
    :cond_14
    const/4 v3, 0x0

    goto :goto_b

    .line 462
    .restart local v3    # "c1":Z
    :cond_15
    const/4 v4, 0x0

    goto :goto_c

    .line 463
    .restart local v4    # "c2":Z
    :cond_16
    const/4 v5, 0x0

    goto :goto_d

    .line 464
    .restart local v5    # "c3":Z
    :cond_17
    const/4 v6, 0x0

    goto :goto_e

    .line 469
    .restart local v6    # "c4":Z
    :cond_18
    if-nez v5, :cond_8

    if-nez v6, :cond_8

    .line 483
    :cond_19
    if-eqz v14, :cond_1f

    .end local v10    # "handleTop":I
    :goto_f
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v10, v12, v1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->performFling(IFZ)V

    goto/16 :goto_1

    .restart local v10    # "handleTop":I
    :cond_1a
    move v10, v8

    .line 475
    goto/16 :goto_9

    .line 477
    :cond_1b
    if-eqz v14, :cond_1c

    .end local v10    # "handleTop":I
    :goto_10
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->animateOpen(I)V

    goto/16 :goto_1

    .restart local v10    # "handleTop":I
    :cond_1c
    move v10, v8

    goto :goto_10

    .line 480
    :cond_1d
    if-eqz v14, :cond_1e

    .end local v10    # "handleTop":I
    :goto_11
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v10, v12, v1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->performFling(IFZ)V

    goto/16 :goto_1

    .restart local v10    # "handleTop":I
    :cond_1e
    move v10, v8

    goto :goto_11

    :cond_1f
    move v10, v8

    .line 483
    goto :goto_f

    .line 486
    .end local v3    # "c1":Z
    .end local v4    # "c2":Z
    .end local v5    # "c3":Z
    .end local v6    # "c4":Z
    :cond_20
    if-eqz v14, :cond_21

    .end local v10    # "handleTop":I
    :goto_12
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v10, v12, v1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->performFling(IFZ)V

    goto/16 :goto_1

    .restart local v10    # "handleTop":I
    :cond_21
    move v10, v8

    goto :goto_12

    .line 493
    .end local v2    # "action":I
    .end local v7    # "handleBottom":I
    .end local v8    # "handleLeft":I
    .end local v9    # "handleRight":I
    .end local v10    # "handleTop":I
    .end local v11    # "negative":Z
    .end local v12    # "velocity":F
    .end local v13    # "velocityTracker":Landroid/view/VelocityTracker;
    .end local v14    # "vertical":Z
    .end local v15    # "xVelocity":F
    .end local v16    # "yVelocity":F
    :cond_22
    const/16 v17, 0x1

    goto/16 :goto_0

    .line 405
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public open()V
    .locals 1

    .prologue
    .line 865
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->openDrawer()V

    .line 866
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->invalidate()V

    .line 867
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->requestLayout()V

    .line 869
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->sendAccessibilityEvent(I)V

    .line 870
    return-void
.end method

.method public setExpanded(Z)V
    .locals 0
    .param p1, "isExpanded"    # Z

    .prologue
    .line 965
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    .line 966
    return-void
.end method

.method public setOnDrawerCloseListener(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;)V
    .locals 0
    .param p1, "onDrawerCloseListener"    # Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;

    .prologue
    .line 988
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mOnDrawerCloseListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;

    .line 989
    return-void
.end method

.method public setOnDrawerCloseListener(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;II)V
    .locals 0
    .param p1, "onDrawerCloseListener"    # Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;
    .param p2, "handleParentWidth"    # I
    .param p3, "handlerWidth"    # I

    .prologue
    .line 992
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mOnDrawerCloseListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;

    .line 993
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHandlerParentWidth:I

    .line 994
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mHanderWidth:I

    .line 995
    return-void
.end method

.method public setOnDrawerOpenListener(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerOpenListener;)V
    .locals 0
    .param p1, "onDrawerOpenListener"    # Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerOpenListener;

    .prologue
    .line 976
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mOnDrawerOpenListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerOpenListener;

    .line 977
    return-void
.end method

.method public setOnDrawerScrollListener(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;)V
    .locals 0
    .param p1, "onDrawerScrollListener"    # Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;

    .prologue
    .line 1007
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mOnDrawerScrollListener:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerScrollListener;

    .line 1008
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 827
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mExpanded:Z

    if-nez v0, :cond_0

    .line 828
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->openDrawer()V

    .line 832
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->invalidate()V

    .line 833
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->requestLayout()V

    .line 834
    const-string v0, "JW toggle"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 835
    return-void

    .line 830
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->closeDrawer()V

    goto :goto_0
.end method

.method public unlock()V
    .locals 1

    .prologue
    .line 1039
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->mLocked:Z

    .line 1040
    return-void
.end method
