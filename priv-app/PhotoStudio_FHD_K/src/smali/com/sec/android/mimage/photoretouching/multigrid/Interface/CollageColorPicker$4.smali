.class Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;
.super Ljava/lang/Object;
.source "CollageColorPicker.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->initListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    .line 221
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 268
    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : mOnTouchColorPalette : mCurrentColor : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColor:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$4(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 269
    return v4

    .line 225
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColorBtn:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$1(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)Landroid/widget/LinearLayout;

    move-result-object v1

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setSelected(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$5(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Landroid/view/View;)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mIsForCollage:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$6(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->getColor(Landroid/view/MotionEvent;)I
    invoke-static {v1, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$7(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Landroid/view/MotionEvent;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setCurrentColor(I)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setPickerPos(Landroid/view/MotionEvent;Z)V
    invoke-static {v0, p2, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Landroid/view/MotionEvent;Z)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColor:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$4(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;->setBackgroundColor(I)Z

    goto :goto_0

    .line 239
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mIsForCollage:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$6(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->getColor(Landroid/view/MotionEvent;)I
    invoke-static {v1, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$7(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Landroid/view/MotionEvent;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setCurrentColor(I)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setPickerPos(Landroid/view/MotionEvent;Z)V
    invoke-static {v0, p2, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Landroid/view/MotionEvent;Z)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColor:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$4(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;->setBackgroundColor(I)Z

    goto/16 :goto_0

    .line 248
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mTime:J
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$8(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v2, 0x64

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->getColor(Landroid/view/MotionEvent;)I
    invoke-static {v1, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$7(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Landroid/view/MotionEvent;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setCurrentColor(I)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setPickerPos(Landroid/view/MotionEvent;Z)V
    invoke-static {v0, p2, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Landroid/view/MotionEvent;Z)V

    goto/16 :goto_0

    .line 260
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColor:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$4(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;->setBackgroundColor(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->getColor(Landroid/view/MotionEvent;)I
    invoke-static {v1, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$7(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Landroid/view/MotionEvent;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setCurrentColor(I)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->access$9(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;J)V

    goto/16 :goto_0

    .line 221
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
