.class public Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "ViewPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;
    }
.end annotation


# instance fields
.field public mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

.field public mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;)V
    .locals 1
    .param p1, "viewpagermanager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;
    .param p2, "viewPagerInterface"    # Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 72
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    .line 73
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    .line 16
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    .line 17
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    .line 18
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 35
    return-void
.end method

.method public finishUpdate(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 58
    return-void
.end method

.method public getBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getEffect(I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;->mViewPagerInterface:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;->getCount()I

    move-result v0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 43
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->updateBitmap(Landroid/graphics/Bitmap;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .local v0, "view":Landroid/view/View;
    move-object v1, p1

    .line 45
    check-cast v1, Landroid/support/v4/view/ViewPager;

    move-object v2, p1

    check-cast v2, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v2

    if-le v2, p2, :cond_0

    .end local p1    # "container":Landroid/view/ViewGroup;
    .end local p2    # "position":I
    :goto_0
    invoke-virtual {v1, v0, p2}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;I)V

    .line 46
    return-object v0

    .line 45
    .restart local p1    # "container":Landroid/view/ViewGroup;
    .restart local p2    # "position":I
    :cond_0
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result p2

    goto :goto_0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 29
    check-cast p2, Landroid/view/View;

    .end local p2    # "object":Ljava/lang/Object;
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 64
    invoke-super {p0}, Landroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V

    .line 65
    return-void
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0
    .param p1, "pc"    # Landroid/os/Parcelable;
    .param p2, "cl"    # Ljava/lang/ClassLoader;

    .prologue
    .line 60
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return-object v0
.end method

.method public startUpdate(Landroid/view/View;)V
    .locals 1
    .param p1, "container"    # Landroid/view/View;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->startUpdate()V

    .line 52
    return-void
.end method
