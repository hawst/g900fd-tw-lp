.class public Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "EffectView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$ResolverSetAsAdapter;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;
    }
.end annotation


# instance fields
.field private REDOALL_DIALOG:I

.field private final TWIRLSPHERIZE_BRUSH_ID:I

.field private UNDOALL_DIALOG:I

.field private isEffectManagerPressed:Z

.field private isInit:Z

.field private isReverseAnimRunning:Z

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mAlpha:I

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mClearPaint:Landroid/graphics/Paint;

.field private mColor:I

.field private mConfigurationChanged:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentEffectType:I

.field private mCurrentSaveSize:I

.field private mDashPathEffectPaint:Landroid/graphics/Paint;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mDrawAnimRunnable:Ljava/lang/Runnable;

.field private mDrawContour:Z

.field private mDrawLinePaint:Landroid/graphics/Paint;

.field private mDrawLinePaint1:Landroid/graphics/Paint;

.field private mDrawOriginal:Z

.field private mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

.field private mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHandler:Landroid/os/Handler;

.field private mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mIsMinimum:Z

.field private mLinePaint:Landroid/graphics/Paint;

.field private mLongpressButton:Z

.field private mMaskPaint:Landroid/graphics/Paint;

.field private mMiddleBtn:Landroid/widget/Button;

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mOnlayoutCalledTime:J

.field private mOptionItemId:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPathEffect:Landroid/graphics/DashPathEffect;

.field private mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mProgressTextRect:Landroid/graphics/Rect;

.field private mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;

.field private mViewInputBitmap:Landroid/graphics/Bitmap;

.field private msave:Z

.field private msetas:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p3, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p4, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p5, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 129
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 550
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isEffectManagerPressed:Z

    .line 2917
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    .line 2918
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 2919
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    .line 2920
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 2921
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 2922
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 2923
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2924
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 2925
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 2926
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 2928
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPaint:Landroid/graphics/Paint;

    .line 2929
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMaskPaint:Landroid/graphics/Paint;

    .line 2930
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mClearPaint:Landroid/graphics/Paint;

    .line 2931
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mLinePaint:Landroid/graphics/Paint;

    .line 2932
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 2933
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .line 2935
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentEffectType:I

    .line 2937
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawContour:Z

    .line 2938
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    .line 2939
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 2941
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mColor:I

    .line 2942
    const v0, -0x66666667

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->TWIRLSPHERIZE_BRUSH_ID:I

    .line 2943
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isInit:Z

    .line 2944
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawOriginal:Z

    .line 2945
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2946
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    .line 2947
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 2948
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mConfigurationChanged:Z

    .line 2950
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    .line 2952
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressTextRect:Landroid/graphics/Rect;

    .line 2954
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mOnlayoutCalledTime:J

    .line 2956
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .line 2959
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->UNDOALL_DIALOG:I

    .line 2960
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->REDOALL_DIALOG:I

    .line 2962
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mOptionItemId:I

    .line 2964
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 2966
    const v0, 0x7a1200

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentSaveSize:I

    .line 2967
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mLongpressButton:Z

    .line 2969
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 2970
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 2971
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPathEffect:Landroid/graphics/DashPathEffect;

    .line 2973
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->msave:Z

    .line 2974
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->msetas:Z

    .line 2975
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isReverseAnimRunning:Z

    .line 2977
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    .line 2978
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    .line 130
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 131
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initEffectView(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 132
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initGesture()V

    .line 133
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 107
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 550
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isEffectManagerPressed:Z

    .line 2917
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    .line 2918
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 2919
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    .line 2920
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 2921
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 2922
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 2923
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2924
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 2925
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 2926
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 2928
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPaint:Landroid/graphics/Paint;

    .line 2929
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMaskPaint:Landroid/graphics/Paint;

    .line 2930
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mClearPaint:Landroid/graphics/Paint;

    .line 2931
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mLinePaint:Landroid/graphics/Paint;

    .line 2932
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 2933
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .line 2935
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentEffectType:I

    .line 2937
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawContour:Z

    .line 2938
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    .line 2939
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 2941
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mColor:I

    .line 2942
    const v0, -0x66666667

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->TWIRLSPHERIZE_BRUSH_ID:I

    .line 2943
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isInit:Z

    .line 2944
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawOriginal:Z

    .line 2945
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2946
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    .line 2947
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 2948
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mConfigurationChanged:Z

    .line 2950
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    .line 2952
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressTextRect:Landroid/graphics/Rect;

    .line 2954
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mOnlayoutCalledTime:J

    .line 2956
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .line 2959
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->UNDOALL_DIALOG:I

    .line 2960
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->REDOALL_DIALOG:I

    .line 2962
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mOptionItemId:I

    .line 2964
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 2966
    const v0, 0x7a1200

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentSaveSize:I

    .line 2967
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mLongpressButton:Z

    .line 2969
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 2970
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 2971
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPathEffect:Landroid/graphics/DashPathEffect;

    .line 2973
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->msave:Z

    .line 2974
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->msetas:Z

    .line 2975
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isReverseAnimRunning:Z

    .line 2977
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    .line 2978
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    .line 108
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initEffectView(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 110
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initGesture()V

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p6, "info"    # Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 115
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 550
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isEffectManagerPressed:Z

    .line 2917
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    .line 2918
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 2919
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    .line 2920
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 2921
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 2922
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 2923
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2924
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 2925
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 2926
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 2928
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPaint:Landroid/graphics/Paint;

    .line 2929
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMaskPaint:Landroid/graphics/Paint;

    .line 2930
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mClearPaint:Landroid/graphics/Paint;

    .line 2931
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mLinePaint:Landroid/graphics/Paint;

    .line 2932
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 2933
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .line 2935
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentEffectType:I

    .line 2937
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawContour:Z

    .line 2938
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    .line 2939
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 2941
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mColor:I

    .line 2942
    const v0, -0x66666667

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->TWIRLSPHERIZE_BRUSH_ID:I

    .line 2943
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isInit:Z

    .line 2944
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawOriginal:Z

    .line 2945
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2946
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    .line 2947
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 2948
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mConfigurationChanged:Z

    .line 2950
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    .line 2952
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressTextRect:Landroid/graphics/Rect;

    .line 2954
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mOnlayoutCalledTime:J

    .line 2956
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .line 2959
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->UNDOALL_DIALOG:I

    .line 2960
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->REDOALL_DIALOG:I

    .line 2962
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mOptionItemId:I

    .line 2964
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 2966
    const v0, 0x7a1200

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentSaveSize:I

    .line 2967
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mLongpressButton:Z

    .line 2969
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 2970
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 2971
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPathEffect:Landroid/graphics/DashPathEffect;

    .line 2973
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->msave:Z

    .line 2974
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->msetas:Z

    .line 2975
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isReverseAnimRunning:Z

    .line 2977
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    .line 2978
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    .line 116
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initEffectView(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 117
    iput-object p6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .line 118
    if-eqz p6, :cond_0

    .line 119
    invoke-virtual {p6}, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->getPreviousSubStatus()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentEffectType:I

    .line 120
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initGesture()V

    .line 121
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 2918
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 2919
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;I)Z
    .locals 1

    .prologue
    .line 2273
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->runOptionItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2946
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2945
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 2922
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;I)V
    .locals 0

    .prologue
    .line 286
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->checkHelpPopup(I)V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 2923
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;I)V
    .locals 0

    .prologue
    .line 2935
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentEffectType:I

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I
    .locals 1

    .prologue
    .line 2935
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentEffectType:I

    return v0
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    .locals 1

    .prologue
    .line 2933
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    return-object v0
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;
    .locals 1

    .prologue
    .line 2925
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 2921
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 1109
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->init3DepthActionBar()V

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .locals 1

    .prologue
    .line 2924
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    return-object v0
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 636
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initMiddleButton()V

    return-void
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 312
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->setMainBtnListener()V

    return-void
.end method

.method static synthetic access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;
    .locals 1

    .prologue
    .line 2917
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    return-object v0
.end method

.method static synthetic access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;)V
    .locals 0

    .prologue
    .line 2917
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    return-void
.end method

.method static synthetic access$26(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 2907
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->disableButtonsForCollage()V

    return-void
.end method

.method static synthetic access$27(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V
    .locals 0

    .prologue
    .line 2926
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    return-void
.end method

.method static synthetic access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Z
    .locals 1

    .prologue
    .line 2938
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    return v0
.end method

.method static synthetic access$29(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Z
    .locals 1

    .prologue
    .line 550
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isEffectManagerPressed:Z

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 2920
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$30(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Z)V
    .locals 0

    .prologue
    .line 550
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isEffectManagerPressed:Z

    return-void
.end method

.method static synthetic access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 729
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->clearnSelectionArea()V

    return-void
.end method

.method static synthetic access$32(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$33(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewHeight()I

    move-result v0

    return v0
.end method

.method static synthetic access$34(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 2691
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->refreshImageAndBottomButtons()V

    return-void
.end method

.method static synthetic access$35(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I
    .locals 1

    .prologue
    .line 2959
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->UNDOALL_DIALOG:I

    return v0
.end method

.method static synthetic access$36(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I
    .locals 1

    .prologue
    .line 2960
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->REDOALL_DIALOG:I

    return v0
.end method

.method static synthetic access$37(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Z)V
    .locals 0

    .prologue
    .line 2943
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isInit:Z

    return-void
.end method

.method static synthetic access$38(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Z)V
    .locals 0

    .prologue
    .line 2967
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mLongpressButton:Z

    return-void
.end method

.method static synthetic access$39(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Z
    .locals 1

    .prologue
    .line 2967
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mLongpressButton:Z

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 2939
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method static synthetic access$40(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Landroid/content/Intent;Z)V
    .locals 0

    .prologue
    .line 2347
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    return-void
.end method

.method static synthetic access$41(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;I)V
    .locals 0

    .prologue
    .line 2962
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mOptionItemId:I

    return-void
.end method

.method static synthetic access$42(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;I)V
    .locals 0

    .prologue
    .line 2966
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentSaveSize:I

    return-void
.end method

.method static synthetic access$43(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 1725
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->doCancel()V

    return-void
.end method

.method static synthetic access$44(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 1689
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->doDone()V

    return-void
.end method

.method static synthetic access$45(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Z
    .locals 1

    .prologue
    .line 2975
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isReverseAnimRunning:Z

    return v0
.end method

.method static synthetic access$46(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Z)V
    .locals 0

    .prologue
    .line 2975
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isReverseAnimRunning:Z

    return-void
.end method

.method static synthetic access$47(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;I)V
    .locals 0

    .prologue
    .line 2977
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    return-void
.end method

.method static synthetic access$48(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Z)V
    .locals 0

    .prologue
    .line 2944
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawOriginal:Z

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I
    .locals 1

    .prologue
    .line 2966
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentSaveSize:I

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 2918
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 2947
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 2947
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I
    .locals 1

    .prologue
    .line 2962
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mOptionItemId:I

    return v0
.end method

.method private checkHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 288
    const/4 v0, 0x0

    .line 289
    .local v0, "isNoShow":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->isDoNotShowPopup(Landroid/content/Context;I)Z

    move-result v0

    .line 291
    if-nez v0, :cond_0

    .line 293
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-nez v1, :cond_0

    .line 295
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initHelpPopup(I)V

    .line 296
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->showCurrentModeHelpPopup()V

    .line 299
    :cond_0
    return-void
.end method

.method private clearnSelectionArea()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 731
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 734
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawPathList()Landroid/graphics/Path;

    move-result-object v2

    .line 735
    .local v2, "path":Landroid/graphics/Path;
    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 736
    invoke-virtual {v2}, Landroid/graphics/Path;->rewind()V

    .line 737
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskBuffer()[B

    move-result-object v3

    invoke-static {v3, v7}, Ljava/util/Arrays;->fill([BB)V

    .line 738
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    new-instance v4, Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    invoke-direct {v4, v5, v6, v7, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPreviewMaskRoi(Landroid/graphics/Rect;)V

    .line 740
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;-><init>()V

    .line 741
    .local v0, "clearEffect":Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->init(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 742
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    invoke-direct {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;)V

    .line 743
    .local v1, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addMaskBuffer(Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;)V

    .line 744
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;->destroy()V

    .line 746
    .end local v0    # "clearEffect":Lcom/sec/android/mimage/photoretouching/Core/ClearSelectEffect;
    .end local v1    # "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    .end local v2    # "path":Landroid/graphics/Path;
    :cond_0
    return-void
.end method

.method private disableButtonsForCollage()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2909
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v0, :cond_0

    .line 2911
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setEnabledWithChildren(Z)V

    .line 2912
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->showArrow(Z)V

    .line 2914
    :cond_0
    return-void
.end method

.method private doCancel()V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 1727
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 1729
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1730
    .local v1, "output":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 1731
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1734
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1737
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1738
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 1740
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 1732
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 1742
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->set2depth()V

    .line 1743
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 1744
    return-void
.end method

.method private doDone()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1691
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : doDone"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1692
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->blending()V

    .line 1693
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->applyPreview()V

    .line 1695
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->refreshInputBitmap()V

    .line 1697
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-direct {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V

    .line 1700
    .local v4, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_0

    .line 1702
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1703
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 1704
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 1702
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([IIILcom/sec/android/mimage/photoretouching/Core/EffectInfo;Z)V

    .line 1706
    sput-boolean v5, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->enhanceFlag:Z

    .line 1709
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v0, :cond_1

    .line 1711
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->applyOriginal()[I

    .line 1712
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->changeImageData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 1713
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const/high16 v1, 0x1e110000

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->changeViewStatus(II)V

    .line 1721
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->setMaskAlpha(I)V

    .line 1722
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 1723
    return-void

    .line 1717
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->set2depth()V

    goto :goto_0
.end method

.method private init2DepthActionBar()V
    .locals 5

    .prologue
    const/4 v2, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 799
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 800
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isValidBackKey()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 802
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 805
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$15;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 802
    invoke-virtual {v0, v4, v2, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 844
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-virtual {v0, v4, v3, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 889
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x2

    .line 890
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$17;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$17;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 889
    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 937
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xe

    .line 938
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$18;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$18;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 937
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 973
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 974
    const/4 v1, 0x4

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$19;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$19;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 973
    invoke-virtual {v0, v1, v4, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1015
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0x10

    .line 1018
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$20;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$20;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 1015
    invoke-virtual {v0, v1, v4, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1054
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x5

    .line 1057
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$21;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 1054
    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1094
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 1095
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 1097
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 1099
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1100
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isSaved()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 1107
    :cond_0
    :goto_1
    return-void

    .line 842
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    goto :goto_0

    .line 1103
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isSaved()Z

    move-result v1

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_1
.end method

.method private init3DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1111
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 1113
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 1114
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 1117
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$22;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$22;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 1114
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1147
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 1150
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$23;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$23;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 1147
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1174
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 1175
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 1176
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 1178
    :cond_0
    return-void
.end method

.method private initEffectView(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    const/high16 v7, 0x42c80000    # 100.0f

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    .line 2207
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHandler:Landroid/os/Handler;

    .line 2208
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    .line 2209
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 2210
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 2211
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2212
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 2214
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->setInterface(Ljava/lang/Object;)V

    .line 2216
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPaint:Landroid/graphics/Paint;

    .line 2217
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2219
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMaskPaint:Landroid/graphics/Paint;

    .line 2220
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2222
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mClearPaint:Landroid/graphics/Paint;

    .line 2223
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mClearPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 2225
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mLinePaint:Landroid/graphics/Paint;

    .line 2226
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2227
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2229
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 2230
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2231
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2232
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    const/4 v2, 0x4

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-direct {v1, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 2234
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 2236
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 2237
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 2239
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2240
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .line 2242
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    const/high16 v1, 0x16000000

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$36;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$36;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;-><init>(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 2250
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v7

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v7

    if-gez v0, :cond_2

    .line 2251
    :cond_1
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    .line 2255
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->refreshInputBitmap()V

    .line 2256
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initProgressText()V

    .line 2257
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->setViewLayerType(I)V

    .line 2260
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_1

    const/high16 v2, 0x40400000    # 3.0f

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPathEffect:Landroid/graphics/DashPathEffect;

    .line 2261
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 2262
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2263
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPathEffect:Landroid/graphics/DashPathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 2264
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2265
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2267
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 2268
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint1:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2269
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint1:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2270
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2272
    return-void

    .line 2253
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    goto :goto_0

    .line 2232
    nop

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data

    .line 2260
    :array_1
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method

.method private initGesture()V
    .locals 3

    .prologue
    .line 2611
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$37;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$37;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 2666
    .local v0, "gesture":Landroid/view/GestureDetector$OnGestureListener;
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 2667
    return-void
.end method

.method private initHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 302
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 303
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->setHelpPopupCallback(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;)V

    .line 310
    return-void
.end method

.method private initMiddleButton()V
    .locals 13

    .prologue
    const v12, 0x7f050231

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 637
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050236

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 638
    .local v5, "viewWitdh":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050238

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 639
    .local v4, "viewHeight":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050239

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 640
    .local v3, "textSize":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    const v7, 0x7f0601c0

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 641
    .local v2, "text":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 643
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    const v7, 0x7f060193

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 644
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050237

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 646
    :cond_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    const v7, 0x7f0900b6

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    .line 649
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    instance-of v6, v6, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-nez v6, :cond_1

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v6

    const v7, 0x16001613

    if-ne v6, v7, :cond_4

    .line 651
    :cond_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    if-eqz v6, :cond_2

    .line 653
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 656
    :cond_2
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v6, :cond_3

    .line 658
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v6, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 728
    :cond_3
    :goto_0
    return-void

    .line 665
    :cond_4
    const-string v6, "sec-roboto-ligh"

    invoke-static {v6, v9}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 667
    .local v0, "font":Landroid/graphics/Typeface;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    if-eqz v6, :cond_3

    .line 670
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-static {v6, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->doNeedUppercaseString(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 671
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->toUppercaseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 674
    :cond_5
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    const v7, 0x7f020326

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 675
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 676
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 677
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    int-to-float v7, v3

    invoke-virtual {v6, v9, v7}, Landroid/widget/Button;->setTextSize(IF)V

    .line 678
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f040034

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v10, v10, v10, v7}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    .line 679
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v5}, Landroid/widget/Button;->setWidth(I)V

    .line 680
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v4}, Landroid/widget/Button;->setHeight(I)V

    .line 681
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v11}, Landroid/widget/Button;->setEnabled(Z)V

    .line 682
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v9}, Landroid/widget/Button;->setPressed(Z)V

    .line 683
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/Button;->setSingleLine()V

    .line 684
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 685
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v11}, Landroid/widget/Button;->setHorizontalFadingEdgeEnabled(Z)V

    .line 686
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 689
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 690
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v6, 0x9

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 691
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f05022e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 692
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050230

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 693
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 695
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    .line 696
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    .line 695
    invoke-virtual {v6, v7, v9, v8, v9}, Landroid/widget/Button;->setPadding(IIII)V

    .line 698
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMiddleBtn:Landroid/widget/Button;

    new-instance v7, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;

    invoke-direct {v7, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method private initSaveOptionDialog()V
    .locals 11

    .prologue
    const v10, 0x7f0601f0

    const v9, 0x7f0601cb

    const/16 v2, 0x9

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 2075
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2129
    :goto_0
    return-void

    .line 2080
    :cond_0
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$30;

    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$30;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 2095
    .local v8, "saveListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2097
    const v3, 0x7f0601cd

    .line 2099
    const/4 v5, 0x1

    .line 2101
    const v7, 0x103012e

    .line 2095
    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZZLandroid/graphics/Point;I)V

    .line 2103
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 2105
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x810

    move v3, v9

    move v5, v9

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2109
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x820

    move v3, v10

    move v5, v10

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2110
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$31;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$31;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2117
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$32;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$32;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2128
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    goto :goto_0
.end method

.method private initSaveYesNoCancelForFinish()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 2133
    const/4 v7, 0x0

    .line 2134
    .local v7, "fileName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 2135
    .local v8, "path":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 2137
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2138
    if-nez v7, :cond_0

    .line 2140
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2144
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2145
    const/16 v2, 0x8

    .line 2146
    const v3, 0x7f06019d

    .line 2147
    const/4 v4, 0x1

    .line 2149
    const v6, 0x103012e

    .line 2144
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2151
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 2152
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x500

    .line 2155
    const v4, 0x7f060192

    move v2, v9

    move v3, v9

    move-object v6, v5

    .line 2152
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2159
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    .line 2160
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$33;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$33;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 2159
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnShowListener;)V

    .line 2182
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601b2

    .line 2183
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$34;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$34;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 2182
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2191
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 2192
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$35;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$35;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 2191
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2199
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2200
    return-void
.end method

.method private initSetAsDialog()V
    .locals 7

    .prologue
    .line 2598
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 2600
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2601
    const v2, 0x7f090158

    .line 2602
    const v3, 0x7f0600a3

    .line 2603
    const/4 v4, 0x0

    .line 2604
    const/4 v5, 0x0

    .line 2605
    const v6, 0x103012e

    .line 2600
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2606
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2608
    :cond_0
    return-void
.end method

.method private initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isShareVia"    # Z

    .prologue
    .line 2350
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2351
    .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    const/4 v2, 0x0

    .line 2352
    .local v2, "shareChooser":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 2353
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    const v4, 0x7f06000f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 2357
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Landroid/content/pm/LabeledIntent;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/pm/LabeledIntent;

    .line 2358
    .local v0, "extraIntents":[Landroid/content/pm/LabeledIntent;
    const-string v3, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2359
    const/high16 v3, 0x24000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2360
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2540
    return-void

    .line 2355
    .end local v0    # "extraIntents":[Landroid/content/pm/LabeledIntent;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    const v4, 0x7f0600a3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method private initShareViaDialog()V
    .locals 7

    .prologue
    .line 2586
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 2588
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2589
    const v2, 0x7f090157

    .line 2590
    const v3, 0x7f06000f

    .line 2591
    const/4 v4, 0x0

    .line 2592
    const/4 v5, 0x0

    .line 2593
    const v6, 0x103012e

    .line 2588
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2594
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2596
    :cond_0
    return-void
.end method

.method private initUndoRedoAllDialog()V
    .locals 8

    .prologue
    const v6, 0x103012e

    const/16 v1, 0x1000

    const/16 v7, 0x500

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1945
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1946
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->UNDOALL_DIALOG:I

    .line 1947
    const v3, 0x7f0600a6

    .line 1945
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1952
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1953
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1954
    const v2, 0x7f06009c

    .line 1956
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$24;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$24;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 1953
    invoke-virtual {v0, v7, v2, v5, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1963
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v2, 0x7f060007

    .line 1964
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$25;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 1963
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1997
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v2, 0x7f060009

    .line 1998
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$26;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$26;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 1997
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2006
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2008
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2009
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->REDOALL_DIALOG:I

    .line 2010
    const v3, 0x7f0600a1

    .line 2008
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2015
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 2016
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2017
    const v1, 0x7f0601ce

    .line 2019
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$27;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$27;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 2016
    invoke-virtual {v0, v7, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 2026
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 2027
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$28;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$28;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 2026
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2061
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 2062
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$29;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$29;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 2061
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2070
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2071
    return-void
.end method

.method private refreshImageAndBottomButtons()V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/high16 v10, 0x42c80000    # 100.0f

    const/4 v2, 0x0

    .line 2693
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_1

    .line 2695
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-ltz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-gez v3, :cond_5

    .line 2696
    :cond_0
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    .line 2700
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->refreshInputBitmap()V

    .line 2702
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2703
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 2704
    .local v1, "output":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 2705
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2708
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 2711
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 2712
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 2714
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 2706
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 2716
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "output":[I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_4

    .line 2718
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_3

    .line 2720
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-ltz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-gez v3, :cond_6

    .line 2721
    :cond_2
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    .line 2725
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$38;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$38;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2734
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->invalidateViews()V

    .line 2735
    return-void

    .line 2698
    :cond_5
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    goto :goto_0

    .line 2723
    :cond_6
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    goto :goto_1
.end method

.method private refreshImageAndBottomButtonsAndActionBar()V
    .locals 13

    .prologue
    const/4 v8, 0x1

    const/high16 v12, 0x42c80000    # 100.0f

    const/4 v2, 0x0

    .line 2741
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_1

    .line 2743
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v12

    if-ltz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v12

    if-gez v3, :cond_5

    .line 2744
    :cond_0
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    .line 2748
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->refreshInputBitmap()V

    .line 2750
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2752
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 2753
    .local v1, "output":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 2754
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2757
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 2760
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 2761
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 2763
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 2755
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 2765
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "output":[I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_4

    .line 2767
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_3

    .line 2769
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v12

    if-ltz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v12

    if-gez v3, :cond_6

    .line 2770
    :cond_2
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    .line 2775
    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initSubViewButtons(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V

    .line 2776
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewWidth()I

    move-result v4

    .line 2777
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v5

    .line 2778
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 2779
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 2776
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->showSubBottomThumbnailButton(I[III)V

    .line 2781
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v10, v3, :cond_7

    .line 2821
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->set2depth()V

    .line 2822
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resetMenu()V

    .line 2823
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentEffectType:I

    .line 2833
    .end local v10    # "i":I
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->invalidateViews()V

    .line 2834
    return-void

    .line 2746
    :cond_5
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    goto/16 :goto_0

    .line 2772
    :cond_6
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    goto :goto_1

    .line 2783
    .restart local v10    # "i":I
    :cond_7
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/View;

    .line 2785
    .local v11, "v":Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x16001630

    if-ne v3, v4, :cond_8

    .line 2787
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v11}, Landroid/view/View;->getId()I

    move-result v4

    .line 2789
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$39;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$39;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 2787
    invoke-virtual {v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 2781
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 2810
    :cond_8
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v11}, Landroid/view/View;->getId()I

    move-result v4

    .line 2811
    invoke-virtual {v11}, Landroid/view/View;->getId()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    move-result-object v5

    .line 2812
    new-instance v6, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$40;

    invoke-direct {v6, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$40;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 2818
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getTextureData()Ljava/util/HashMap;

    move-result-object v7

    .line 2810
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;Ljava/util/HashMap;)V

    goto :goto_3
.end method

.method private refreshInputBitmap()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1656
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1658
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    .line 1660
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    .line 1661
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    .line 1662
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1660
    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    .line 1664
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1665
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 1664
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1686
    :goto_0
    return-void

    .line 1669
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 1671
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    .line 1673
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    .line 1674
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    .line 1675
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1673
    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    .line 1677
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1678
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 1677
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    goto :goto_0

    .line 1682
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1683
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 1682
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    goto :goto_0
.end method

.method private runOptionItem(I)Z
    .locals 7
    .param p1, "optionItemId"    # I

    .prologue
    const v6, 0x7f090158

    const v5, 0x7a1200

    const/4 v4, 0x7

    const/16 v3, 0x9

    .line 2275
    const/4 v0, 0x0

    .line 2276
    .local v0, "ret":Z
    const/4 v1, 0x0

    .line 2277
    .local v1, "uri":Landroid/net/Uri;
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mOptionItemId:I

    .line 2278
    sparse-switch p1, :sswitch_data_0

    .line 2345
    :goto_0
    return v0

    .line 2281
    :sswitch_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2283
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2290
    :goto_1
    const/4 v0, 0x1

    .line 2291
    goto :goto_0

    .line 2288
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->openCamera(Landroid/content/Context;)V

    goto :goto_1

    .line 2293
    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2295
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentSaveSize:I

    .line 2296
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2297
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2309
    :cond_1
    :goto_2
    const/4 v0, 0x1

    .line 2310
    goto :goto_0

    .line 2299
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f090157

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2300
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 2301
    if-nez v1, :cond_3

    .line 2303
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2305
    :cond_3
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    .line 2307
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "JW runOptionItem: share uri="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_2

    .line 2312
    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v3, 0x2e000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 2313
    const/4 v0, 0x1

    .line 2314
    goto/16 :goto_0

    .line 2317
    :sswitch_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2319
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentSaveSize:I

    .line 2320
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2321
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2334
    :cond_4
    :goto_3
    const/4 v0, 0x1

    .line 2335
    goto/16 :goto_0

    .line 2323
    :cond_5
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2326
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 2327
    if-nez v1, :cond_6

    .line 2329
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2331
    :cond_6
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeSetAsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    .line 2332
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    goto :goto_3

    .line 2340
    :sswitch_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v2, :cond_7

    .line 2341
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v3, 0x17000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 2342
    :cond_7
    const-string v2, "JW runOptionItem: clipboard"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2278
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f090007 -> :sswitch_2
        0x7f090157 -> :sswitch_1
        0x7f090158 -> :sswitch_3
        0x7f09015a -> :sswitch_0
        0x7f09015b -> :sswitch_4
    .end sparse-switch
.end method

.method private set2depth()V
    .locals 2

    .prologue
    .line 1748
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->init2DepthActionBar()V

    .line 1749
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    .line 1751
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1752
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 1753
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 1757
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1758
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 1759
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 1765
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->stop()V

    .line 1766
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setClearSelectedSubButton()V

    .line 1767
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 1769
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->clearnSelectionArea()V

    .line 1771
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->setMainBtnListener()V

    .line 1773
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->invalidateViews()V

    .line 1774
    return-void

    .line 1755
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    goto :goto_0

    .line 1761
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 1762
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_1
.end method

.method private setMainBtnListener()V
    .locals 8

    .prologue
    const v7, 0x1000100b

    const v6, 0x10001004

    const v5, 0x10001005

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 314
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isVisibleTitle()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 317
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$6;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 350
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001006

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 351
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 523
    :goto_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getPreviousMode()I

    move-result v0

    const/high16 v1, -0x10000

    and-int/2addr v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 547
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    .line 548
    return-void

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$7;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 386
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$8;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 411
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$9;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 444
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001006

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$10;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 469
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEnableDecoration()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 471
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$11;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-virtual {v0, v7, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_0

    .line 499
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_0

    .line 505
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mIsMinimum:Z

    if-eqz v0, :cond_3

    .line 507
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 508
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 509
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 510
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001006

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 511
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_0

    .line 515
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 516
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 517
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5, v3, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 518
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001006

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 519
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto/16 :goto_0

    .line 526
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001007

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 529
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001009

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 532
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001008

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 535
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 538
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 541
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001003

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 544
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001002

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 523
    nop

    :sswitch_data_0
    .sparse-switch
        0x11100000 -> :sswitch_6
        0x11200000 -> :sswitch_5
        0x15000000 -> :sswitch_4
        0x16000000 -> :sswitch_3
        0x1a000000 -> :sswitch_1
        0x31100000 -> :sswitch_2
        0x31200000 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v5, 0xff

    const/4 v4, 0x1

    .line 1350
    const/4 v0, 0x1

    .line 1351
    .local v0, "ret":Z
    const/4 v1, 0x0

    .line 1352
    .local v1, "runningAnimation":Z
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    if-eqz v2, :cond_0

    .line 1354
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->runningAnimation()Z

    move-result v1

    .line 1356
    :cond_0
    if-eqz v1, :cond_2

    .line 1413
    :cond_1
    :goto_0
    :pswitch_0
    return v4

    .line 1359
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->isStartInitializing()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1361
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v2, :cond_3

    .line 1362
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 1364
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bigheadk, mDrawOriginal = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawOriginal:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1366
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawOriginal:Z

    if-nez v2, :cond_4

    .line 1368
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v2, :cond_4

    .line 1369
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v2, p2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 1371
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1373
    const/4 v0, 0x1

    .line 1374
    goto :goto_0

    .line 1377
    :cond_5
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentEffectType:I

    packed-switch v2, :pswitch_data_0

    .line 1383
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 1391
    :pswitch_1
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawOriginal:Z

    if-eqz v2, :cond_6

    .line 1393
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawOriginal:Z

    .line 1394
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->resetPreProgressValue()V

    .line 1395
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->invalidateViews()V

    .line 1397
    :cond_6
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v2

    if-eq v2, v4, :cond_1

    .line 1398
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->startseek()V

    .line 1399
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isReverseAnimRunning:Z

    .line 1400
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    if-le v2, v5, :cond_7

    .line 1402
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    .line 1404
    :cond_7
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1377
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1383
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public backPressed()V
    .locals 3

    .prologue
    .line 749
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    .line 750
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->updateAnimation()V

    .line 751
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isVisibleTitle()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 754
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v0, :cond_2

    .line 755
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 766
    :cond_1
    :goto_0
    return-void

    .line 756
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v0, :cond_1

    .line 757
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const/high16 v1, 0x1e110000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->changeViewStatus(II)V

    goto :goto_0

    .line 762
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->doCancel()V

    .line 763
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resetMenu()V

    goto :goto_0
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 1518
    return-void
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 1575
    const/4 v0, 0x0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 1588
    const/4 v0, 0x0

    return v0
.end method

.method public getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 210
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    return-object v0
.end method

.method public getStatusHeight()I
    .locals 3

    .prologue
    .line 1593
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1594
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1595
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1596
    iget v2, v0, Landroid/graphics/Rect;->top:I

    return v2
.end method

.method public initActionbar()V
    .locals 1

    .prologue
    .line 771
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v0, :cond_4

    .line 773
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->init3DepthActionBar()V

    .line 774
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 783
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 784
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 785
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 789
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 790
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_3

    .line 791
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 796
    :cond_3
    :goto_2
    return-void

    .line 778
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->init2DepthActionBar()V

    .line 779
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 780
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    goto :goto_0

    .line 787
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    goto :goto_1

    .line 793
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_3

    .line 794
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_2
.end method

.method public initButtons()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 553
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isEffectManagerPressed:Z

    .line 554
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v2, :cond_1

    .line 555
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setClearSelectedButton()V

    .line 556
    const/4 v0, 0x0

    .line 557
    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v2, :cond_1

    .line 558
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    if-eqz v2, :cond_0

    .line 559
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 560
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initSubViewButtons(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V

    .line 566
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->loadTextures(Landroid/content/Context;)V

    .line 567
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewWidth()I

    move-result v3

    .line 568
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v4

    .line 569
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    .line 570
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    .line 567
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->showSubBottomThumbnailButton(I[III)V

    .line 572
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->setMainBtnListener()V

    .line 573
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initMiddleButton()V

    .line 575
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_2

    .line 615
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    if-eqz v2, :cond_5

    .line 617
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->getPreviousSubStatus()I

    move-result v3

    invoke-virtual {v2, v3, v8}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSubViewButtonSelected(IZ)V

    .line 618
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v2, :cond_4

    .line 620
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2, v7}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 633
    .end local v0    # "i":I
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->disableButtonsForCollage()V

    .line 634
    return-void

    .line 577
    .restart local v0    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 579
    .local v1, "v":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x16001630

    if-ne v2, v3, :cond_3

    .line 581
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    .line 583
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$12;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 581
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 575
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 604
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    .line 605
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    move-result-object v4

    .line 606
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$13;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 612
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getTextureData()Ljava/util/HashMap;

    move-result-object v6

    .line 604
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;Ljava/util/HashMap;)V

    goto :goto_2

    .line 624
    .end local v1    # "v":Landroid/view/View;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2, v8}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    goto :goto_1

    .line 629
    :cond_5
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2, v7}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    goto :goto_1
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 1182
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-nez v0, :cond_0

    .line 1194
    :goto_0
    return-void

    .line 1187
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 1188
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initShareViaDialog()V

    .line 1189
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initSetAsDialog()V

    .line 1191
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initUndoRedoAllDialog()V

    .line 1192
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initSaveOptionDialog()V

    .line 1193
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initSaveYesNoCancelForFinish()V

    goto :goto_0
.end method

.method public initEffect()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 140
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->setOnCallback(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->setOnActionbarCallback(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->getPreviousSubStatus()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->init(I)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->getPreviousEffectValue()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->setStep(F)V

    .line 180
    :cond_0
    return-void
.end method

.method public initProgressText()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1306
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressTextRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 1307
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressTextRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 1315
    :goto_0
    return-void

    .line 1309
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressTextRect:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 1506
    return-void
.end method

.method public initTrayLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1510
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 1511
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 1513
    :cond_0
    return-void
.end method

.method public initView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 184
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 190
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->showPreviousGone()V

    .line 191
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->brushGone()V

    .line 192
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 193
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 194
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    array-length v2, v2

    .line 192
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->invalidateViews()V

    .line 199
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawContour:Z

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->applyPreview()I

    .line 206
    :cond_1
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 1449
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1450
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1451
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1462
    :cond_0
    :goto_0
    return-void

    .line 1453
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_2

    .line 1455
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 1456
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 1457
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1459
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v0, :cond_0

    .line 1460
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1418
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    if-eqz v0, :cond_0

    .line 1419
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->killThread()V

    .line 1420
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->destroy()V

    .line 1421
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPaint:Landroid/graphics/Paint;

    .line 1422
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mClearPaint:Landroid/graphics/Paint;

    .line 1423
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mLinePaint:Landroid/graphics/Paint;

    .line 1424
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 1426
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 1427
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1429
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1430
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1431
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_1

    .line 1432
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->free()V

    .line 1433
    :cond_1
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1434
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->Destroy()V

    .line 1435
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1437
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1438
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    .line 1441
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 1442
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1445
    :cond_2
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation",
            "DrawAllocation",
            "DrawAllocation",
            "DrawAllocation"
        }
    .end annotation

    .prologue
    const/16 v13, 0xff

    const/4 v2, 0x0

    .line 1204
    const/high16 v1, -0x1000000

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1205
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_0

    .line 1207
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawOriginal:Z

    if-eqz v1, :cond_3

    .line 1209
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 1210
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1208
    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 1211
    .local v12, "tempOriginalViewBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1212
    .local v0, "tempCanvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 1213
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1214
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 1215
    const/4 v8, 0x1

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 1212
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 1218
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPaint:Landroid/graphics/Paint;

    invoke-static {p1, v12, v1, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 1221
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->recycle()V

    .line 1254
    .end local v0    # "tempCanvas":Landroid/graphics/Canvas;
    .end local v12    # "tempOriginalViewBitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v1, :cond_1

    .line 1257
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isReverseAnimRunning:Z

    if-eqz v1, :cond_7

    .line 1261
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    if-lez v1, :cond_6

    .line 1264
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1265
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getEffectType()I

    move-result v3

    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawOriginal:Z

    .line 1266
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    .line 1264
    invoke-virtual {v1, p1, v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    .line 1267
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawOriginal:Z

    if-eqz v1, :cond_5

    .line 1268
    iput v13, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    .line 1272
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1300
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    iput-boolean v2, v1, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mutexOn:Z

    .line 1301
    :cond_2
    return-void

    .line 1226
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMaskPaint:Landroid/graphics/Paint;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    if-eqz v1, :cond_2

    .line 1228
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMaskPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getMaskAlpha()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1229
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewInputBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPaint:Landroid/graphics/Paint;

    invoke-static {p1, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 1230
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mMaskPaint:Landroid/graphics/Paint;

    invoke-static {p1, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 1232
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v1

    const v3, 0x16001613

    if-eq v1, v3, :cond_4

    .line 1235
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 1236
    .local v11, "p":Landroid/graphics/Path;
    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    .line 1237
    .local v10, "m":Landroid/graphics/Matrix;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v10, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1239
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawPathList()Landroid/graphics/Path;

    move-result-object v1

    invoke-virtual {v1, v10, v11}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    .line 1240
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v11, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1241
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v11, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1242
    invoke-virtual {v11}, Landroid/graphics/Path;->rewind()V

    .line 1246
    .end local v10    # "m":Landroid/graphics/Matrix;
    .end local v11    # "p":Landroid/graphics/Path;
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->runningAnimation()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1248
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectAnimationThread:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;

    invoke-virtual {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$EffectAnimationThread;->drawCanvas(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 1270
    :cond_5
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    add-int/lit8 v1, v1, -0xf

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    goto/16 :goto_1

    .line 1276
    :cond_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->resetPreProgressValue()V

    .line 1277
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isReverseAnimRunning:Z

    goto/16 :goto_2

    .line 1281
    :cond_7
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    if-gt v1, v13, :cond_9

    .line 1283
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1284
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getEffectType()I

    move-result v3

    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawOriginal:Z

    .line 1285
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    .line 1283
    invoke-virtual {v1, p1, v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    .line 1286
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawOriginal:Z

    if-eqz v1, :cond_8

    .line 1287
    iput v13, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    goto/16 :goto_2

    .line 1289
    :cond_8
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    add-int/lit8 v1, v1, 0xf

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mAlpha:I

    goto/16 :goto_2

    .line 1293
    :cond_9
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1294
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getEffectType()I

    move-result v3

    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawOriginal:Z

    .line 1293
    invoke-virtual {v1, p1, v3, v4, v13}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    goto/16 :goto_2
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x1

    .line 1466
    const/16 v1, 0x17

    if-eq p1, v1, :cond_0

    const/16 v1, 0x42

    if-ne p1, v1, :cond_3

    .line 1468
    :cond_0
    const/4 v0, 0x0

    .line 1469
    .local v0, "isFocused":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_1

    .line 1470
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    move-result v0

    .line 1473
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_2

    .line 1474
    if-nez v0, :cond_2

    .line 1475
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 1499
    .end local v0    # "isFocused":Z
    :cond_2
    :goto_0
    return v4

    .line 1478
    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 1480
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    instance-of v1, v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v1, :cond_4

    .line 1482
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const/high16 v2, 0x1e110000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->changeViewStatus(II)V

    goto :goto_0

    .line 1486
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->backPressed()V

    goto :goto_0
.end method

.method public onLayout()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 1600
    const-string v0, "bigheadk, onLayout()"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1614
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 1615
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeLayoutSize(I)V

    .line 1616
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V

    .line 1617
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1619
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 1620
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1622
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 1623
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1624
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1626
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1627
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 1623
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1630
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1631
    iput-boolean v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDrawContour:Z

    .line 1633
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->configurationChanged()V

    .line 1635
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, -0x66666667

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1636
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    .line 1637
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    .line 1638
    .local v8, "display":Landroid/view/Display;
    new-instance v10, Landroid/graphics/Point;

    invoke-direct {v10}, Landroid/graphics/Point;-><init>()V

    .line 1639
    .local v10, "size":Landroid/graphics/Point;
    invoke-virtual {v8, v10}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1641
    iget v11, v10, Landroid/graphics/Point;->x:I

    .line 1642
    .local v11, "width":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getWidth(I)I

    move-result v9

    .line 1643
    .local v9, "left":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getWidth(I)I

    move-result v0

    add-int/2addr v9, v0

    .line 1644
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0, v12}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getWidth(I)I

    move-result v0

    add-int/2addr v9, v0

    .line 1645
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getWidth(I)I

    move-result v0

    add-int/2addr v9, v0

    .line 1646
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getWidth(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v9, v0

    .line 1648
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v11, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->moveDialog(II)V

    .line 1650
    .end local v8    # "display":Landroid/view/Display;
    .end local v9    # "left":I
    .end local v10    # "size":Landroid/graphics/Point;
    .end local v11    # "width":I
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mOnlayoutCalledTime:J

    .line 1651
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .line 1652
    return-void
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 1570
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->runOptionItem(I)Z

    .line 1571
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1527
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->msave:Z

    if-eqz v0, :cond_0

    .line 1528
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1529
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentSaveSize:I

    const v1, 0x7a1200

    if-ne v0, v1, :cond_2

    .line 1530
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cb

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setSelectedFalseAnotherButtons(I)V

    .line 1533
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->msave:Z

    .line 1536
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1537
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 1538
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1540
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->refreshScrollView()V

    .line 1552
    return-void

    .line 1532
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601f0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setSelectedFalseAnotherButtons(I)V

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1556
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->pause()V

    .line 1557
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f090158

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1558
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->msetas:Z

    .line 1559
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 1561
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1562
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->msave:Z

    .line 1563
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 1566
    :cond_1
    return-void
.end method

.method public refreshView()V
    .locals 3

    .prologue
    .line 2672
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 2674
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->refreshImageAndBottomButtons()V

    .line 2675
    return-void
.end method

.method public refreshViewFromEffectManager(I)V
    .locals 1
    .param p1, "resultCode"    # I

    .prologue
    .line 2679
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->isEffectManagerPressed:Z

    .line 2680
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 2682
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 2683
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 2686
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->refreshImageAndBottomButtonsAndActionBar()V

    .line 2688
    :cond_1
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1319
    const-string v0, "bigheadk, setConfigurationChanged()"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1321
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v0, :cond_0

    .line 1322
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setConfigurationChangedFromMultigridView(Z)V

    .line 1325
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_1

    .line 1326
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 1327
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 1328
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 1329
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 1330
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V

    .line 1331
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_2

    .line 1332
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 1334
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initProgressText()V

    .line 1336
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initMiddleButton()V

    .line 1340
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mConfigurationChanged:Z

    .line 1341
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->disableButtonsForCollage()V

    .line 1342
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->refreshScrollView()V

    .line 1343
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPopupLayout()V

    .line 1344
    return-void
.end method
