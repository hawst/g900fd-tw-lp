.class public Lcom/sec/android/mimage/photoretouching/Core/ChainContour;
.super Ljava/lang/Object;
.source "ChainContour.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    }
.end annotation


# instance fields
.field private final BGVAL:I

.field private final FGVAL:I

.field private final VISITED:I

.field private mChainContourMask:[B

.field private mChainContourMaskHeight:I

.field private mChainContourMaskWidth:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->FGVAL:I

    .line 20
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->BGVAL:I

    .line 21
    const/16 v0, 0x21

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->VISITED:I

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    .line 24
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    .line 25
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskHeight:I

    .line 36
    return-void
.end method

.method private getNextCntr([BIIIII)Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    .locals 9
    .param p1, "image"    # [B
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "idirn"    # I

    .prologue
    .line 334
    const/4 v0, 0x0

    .line 337
    .local v0, "cntr":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    const/16 v5, 0x10

    new-array v4, v5, [B

    .line 338
    .local v4, "ring":[B
    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/util/Arrays;->fill([BB)V

    .line 341
    add-int/lit8 v5, p3, -0x1

    if-ltz v5, :cond_0

    .line 342
    const/4 v5, 0x0

    const/16 v6, 0x8

    add-int/lit8 v7, p3, -0x1

    mul-int/2addr v7, p4

    add-int/2addr v7, p2

    aget-byte v7, p1, v7

    aput-byte v7, v4, v6

    aput-byte v7, v4, v5

    .line 343
    :cond_0
    add-int/lit8 v5, p3, -0x1

    if-ltz v5, :cond_1

    add-int/lit8 v5, p2, 0x1

    if-ge v5, p4, :cond_1

    .line 344
    const/4 v5, 0x1

    const/16 v6, 0x9

    add-int/lit8 v7, p3, -0x1

    mul-int/2addr v7, p4

    add-int/lit8 v8, p2, 0x1

    add-int/2addr v7, v8

    aget-byte v7, p1, v7

    aput-byte v7, v4, v6

    aput-byte v7, v4, v5

    .line 345
    :cond_1
    add-int/lit8 v5, p2, 0x1

    if-ge v5, p4, :cond_2

    .line 346
    const/4 v5, 0x2

    const/16 v6, 0xa

    mul-int v7, p3, p4

    add-int/lit8 v8, p2, 0x1

    add-int/2addr v7, v8

    aget-byte v7, p1, v7

    aput-byte v7, v4, v6

    aput-byte v7, v4, v5

    .line 347
    :cond_2
    add-int/lit8 v5, p3, 0x1

    if-ge v5, p5, :cond_3

    add-int/lit8 v5, p2, 0x1

    if-ge v5, p4, :cond_3

    .line 348
    const/4 v5, 0x3

    const/16 v6, 0xb

    add-int/lit8 v7, p3, 0x1

    mul-int/2addr v7, p4

    add-int/lit8 v8, p2, 0x1

    add-int/2addr v7, v8

    aget-byte v7, p1, v7

    aput-byte v7, v4, v6

    aput-byte v7, v4, v5

    .line 349
    :cond_3
    add-int/lit8 v5, p3, 0x1

    if-ge v5, p5, :cond_4

    .line 350
    const/4 v5, 0x4

    const/16 v6, 0xc

    add-int/lit8 v7, p3, 0x1

    mul-int/2addr v7, p4

    add-int/2addr v7, p2

    aget-byte v7, p1, v7

    aput-byte v7, v4, v6

    aput-byte v7, v4, v5

    .line 351
    :cond_4
    add-int/lit8 v5, p3, 0x1

    if-ge v5, p5, :cond_5

    add-int/lit8 v5, p2, -0x1

    if-ltz v5, :cond_5

    .line 352
    const/4 v5, 0x5

    const/16 v6, 0xd

    add-int/lit8 v7, p3, 0x1

    mul-int/2addr v7, p4

    add-int/lit8 v8, p2, -0x1

    add-int/2addr v7, v8

    aget-byte v7, p1, v7

    aput-byte v7, v4, v6

    aput-byte v7, v4, v5

    .line 353
    :cond_5
    add-int/lit8 v5, p2, -0x1

    if-ltz v5, :cond_6

    .line 354
    const/4 v5, 0x6

    const/16 v6, 0xe

    mul-int v7, p3, p4

    add-int/lit8 v8, p2, -0x1

    add-int/2addr v7, v8

    aget-byte v7, p1, v7

    aput-byte v7, v4, v6

    aput-byte v7, v4, v5

    .line 355
    :cond_6
    add-int/lit8 v5, p3, -0x1

    if-ltz v5, :cond_7

    add-int/lit8 v5, p2, -0x1

    if-ltz v5, :cond_7

    .line 356
    const/4 v5, 0x7

    const/16 v6, 0xf

    add-int/lit8 v7, p3, -0x1

    mul-int/2addr v7, p4

    add-int/lit8 v8, p2, -0x1

    add-int/2addr v7, v8

    aget-byte v7, p1, v7

    aput-byte v7, v4, v6

    aput-byte v7, v4, v5

    .line 358
    :cond_7
    add-int/lit8 v5, p6, 0x4

    add-int/lit8 v5, v5, 0x1

    rem-int/lit8 v2, v5, 0x8

    .line 359
    .local v2, "i":I
    if-nez v2, :cond_8

    .line 360
    const/16 v2, 0x8

    .line 362
    :cond_8
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_0
    const/16 v5, 0x8

    if-lt v3, v5, :cond_a

    .line 367
    :cond_9
    const/16 v5, 0x8

    if-ne v3, v5, :cond_c

    move-object v1, v0

    .line 417
    .end local v0    # "cntr":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    .local v1, "cntr":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    :goto_1
    return-object v1

    .line 364
    .end local v1    # "cntr":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    .restart local v0    # "cntr":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    :cond_a
    aget-byte v5, v4, v2

    if-eqz v5, :cond_b

    add-int/lit8 v5, v2, -0x1

    aget-byte v5, v4, v5

    if-eqz v5, :cond_9

    .line 362
    :cond_b
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 370
    :cond_c
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;

    .end local v0    # "cntr":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ChainContour;)V

    .line 371
    .restart local v0    # "cntr":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    rem-int/lit8 v5, v2, 0x8

    iput v5, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;->idirn:I

    .line 373
    packed-switch v2, :pswitch_data_0

    .line 413
    :goto_2
    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;->position:Landroid/graphics/Point;

    iput p2, v5, Landroid/graphics/Point;->x:I

    .line 414
    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;->position:Landroid/graphics/Point;

    iput p3, v5, Landroid/graphics/Point;->y:I

    move-object v1, v0

    .line 417
    .end local v0    # "cntr":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    .restart local v1    # "cntr":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    goto :goto_1

    .line 376
    .end local v1    # "cntr":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    .restart local v0    # "cntr":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    :pswitch_0
    add-int/lit8 p2, p2, 0x1

    .line 377
    add-int/lit8 p3, p3, -0x1

    .line 378
    goto :goto_2

    .line 381
    :pswitch_1
    add-int/lit8 p2, p2, 0x1

    .line 382
    goto :goto_2

    .line 385
    :pswitch_2
    add-int/lit8 p2, p2, 0x1

    .line 386
    add-int/lit8 p3, p3, 0x1

    .line 387
    goto :goto_2

    .line 390
    :pswitch_3
    add-int/lit8 p3, p3, 0x1

    .line 391
    goto :goto_2

    .line 394
    :pswitch_4
    add-int/lit8 p2, p2, -0x1

    .line 395
    add-int/lit8 p3, p3, 0x1

    .line 396
    goto :goto_2

    .line 399
    :pswitch_5
    add-int/lit8 p2, p2, -0x1

    .line 400
    goto :goto_2

    .line 403
    :pswitch_6
    add-int/lit8 p2, p2, -0x1

    .line 404
    add-int/lit8 p3, p3, -0x1

    .line 405
    goto :goto_2

    .line 408
    :pswitch_7
    add-int/lit8 p3, p3, -0x1

    .line 409
    goto :goto_2

    .line 373
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private pathUnion(Landroid/graphics/Path;Landroid/graphics/RectF;Landroid/graphics/Path;Landroid/graphics/RectF;)Landroid/graphics/Path;
    .locals 10
    .param p1, "path1"    # Landroid/graphics/Path;
    .param p2, "clip1"    # Landroid/graphics/RectF;
    .param p3, "path2"    # Landroid/graphics/Path;
    .param p4, "clip2"    # Landroid/graphics/RectF;

    .prologue
    .line 313
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 314
    invoke-virtual {p3}, Landroid/graphics/Path;->close()V

    .line 315
    new-instance v2, Landroid/graphics/Region;

    invoke-direct {v2}, Landroid/graphics/Region;-><init>()V

    .line 316
    .local v2, "region":Landroid/graphics/Region;
    new-instance v3, Landroid/graphics/Region;

    invoke-direct {v3}, Landroid/graphics/Region;-><init>()V

    .line 317
    .local v3, "region1":Landroid/graphics/Region;
    new-instance v0, Landroid/graphics/Region;

    iget v6, p2, Landroid/graphics/RectF;->left:F

    float-to-int v6, v6

    iget v7, p2, Landroid/graphics/RectF;->top:F

    float-to-int v7, v7

    iget v8, p2, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    iget v9, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v9, v9

    invoke-direct {v0, v6, v7, v8, v9}, Landroid/graphics/Region;-><init>(IIII)V

    .line 318
    .local v0, "clipRegion1":Landroid/graphics/Region;
    invoke-virtual {v3, p1, v0}, Landroid/graphics/Region;->setPath(Landroid/graphics/Path;Landroid/graphics/Region;)Z

    .line 319
    new-instance v4, Landroid/graphics/Region;

    invoke-direct {v4}, Landroid/graphics/Region;-><init>()V

    .line 320
    .local v4, "region2":Landroid/graphics/Region;
    new-instance v1, Landroid/graphics/Region;

    iget v6, p4, Landroid/graphics/RectF;->left:F

    float-to-int v6, v6

    iget v7, p4, Landroid/graphics/RectF;->top:F

    float-to-int v7, v7

    iget v8, p4, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    iget v9, p4, Landroid/graphics/RectF;->bottom:F

    float-to-int v9, v9

    invoke-direct {v1, v6, v7, v8, v9}, Landroid/graphics/Region;-><init>(IIII)V

    .line 321
    .local v1, "clipRegion2":Landroid/graphics/Region;
    invoke-virtual {v4, p3, v1}, Landroid/graphics/Region;->setPath(Landroid/graphics/Path;Landroid/graphics/Region;)Z

    .line 322
    sget-object v6, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    invoke-virtual {v2, v3, v4, v6}, Landroid/graphics/Region;->op(Landroid/graphics/Region;Landroid/graphics/Region;Landroid/graphics/Region$Op;)Z

    .line 323
    invoke-virtual {v0}, Landroid/graphics/Region;->setEmpty()V

    .line 324
    invoke-virtual {v1}, Landroid/graphics/Region;->setEmpty()V

    .line 325
    const/4 v3, 0x0

    .line 326
    const/4 v4, 0x0

    .line 327
    invoke-virtual {v2}, Landroid/graphics/Region;->getBoundaryPath()Landroid/graphics/Path;

    move-result-object v5

    .line 328
    .local v5, "ret":Landroid/graphics/Path;
    const/4 v2, 0x0

    .line 329
    return-object v5
.end method


# virtual methods
.method public contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Ljava/util/Vector;Z)V
    .locals 21
    .param p1, "mask"    # [B
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "maskRoi"    # Landroid/graphics/Rect;
    .param p5, "path"    # Landroid/graphics/Path;
    .param p7, "added"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII",
            "Landroid/graphics/Rect;",
            "Landroid/graphics/Path;",
            "Ljava/util/Vector",
            "<",
            "Landroid/graphics/Point;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 244
    .local p6, "points":Ljava/util/Vector;, "Ljava/util/Vector<Landroid/graphics/Point;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    if-nez v3, :cond_0

    .line 310
    :goto_0
    return-void

    .line 247
    :cond_0
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    move/from16 v0, p3

    if-lt v15, v0, :cond_1

    .line 256
    new-instance v17, Landroid/graphics/Path;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Path;-><init>()V

    .line 257
    .local v17, "path2":Landroid/graphics/Path;
    move-object/from16 v0, p4

    iget v3, v0, Landroid/graphics/Rect;->top:I

    add-int/lit8 v6, v3, 0x1

    .local v6, "y":I
    :goto_2
    move-object/from16 v0, p4

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v3, v3, 0x1

    if-lt v6, v3, :cond_2

    .line 294
    invoke-virtual/range {p6 .. p6}, Ljava/util/Vector;->lastElement()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 296
    if-eqz p7, :cond_7

    .line 298
    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11}, Landroid/graphics/RectF;-><init>()V

    .line 299
    .local v11, "clip1":Landroid/graphics/RectF;
    const/4 v3, 0x1

    move-object/from16 v0, p5

    invoke-virtual {v0, v11, v3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 300
    new-instance v12, Landroid/graphics/RectF;

    invoke-direct {v12}, Landroid/graphics/RectF;-><init>()V

    .line 301
    .local v12, "clip2":Landroid/graphics/RectF;
    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 302
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v11, v2, v12}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->pathUnion(Landroid/graphics/Path;Landroid/graphics/RectF;Landroid/graphics/Path;Landroid/graphics/RectF;)Landroid/graphics/Path;

    move-result-object v16

    .line 303
    .local v16, "p":Landroid/graphics/Path;
    move-object/from16 v0, p5

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    goto :goto_0

    .line 249
    .end local v6    # "y":I
    .end local v11    # "clip1":Landroid/graphics/RectF;
    .end local v12    # "clip2":Landroid/graphics/RectF;
    .end local v16    # "p":Landroid/graphics/Path;
    .end local v17    # "path2":Landroid/graphics/Path;
    :cond_1
    mul-int v3, v15, p2

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v3, v1}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v18

    .line 250
    .local v18, "src":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    add-int/lit8 v4, v15, 0x1

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    mul-int/2addr v4, v7

    add-int/lit8 v4, v4, 0x1

    move/from16 v0, p2

    invoke-static {v3, v4, v0}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v14

    .line 251
    .local v14, "dst":Ljava/nio/ByteBuffer;
    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 247
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 259
    .end local v14    # "dst":Ljava/nio/ByteBuffer;
    .end local v18    # "src":Ljava/nio/ByteBuffer;
    .restart local v6    # "y":I
    .restart local v17    # "path2":Landroid/graphics/Path;
    :cond_2
    move-object/from16 v0, p4

    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v5, v3, 0x1

    .local v5, "x":I
    :goto_3
    move-object/from16 v0, p4

    iget v3, v0, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, 0x1

    if-lt v5, v3, :cond_3

    .line 257
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 261
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    mul-int/2addr v3, v6

    add-int/2addr v3, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    array-length v4, v4

    if-ge v3, v4, :cond_6

    .line 262
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    mul-int/2addr v4, v6

    add-int/2addr v4, v5

    aget-byte v3, v3, v4

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    .line 263
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    mul-int/2addr v4, v6

    add-int/lit8 v7, v5, -0x1

    add-int/2addr v4, v7

    aget-byte v3, v3, v4

    if-nez v3, :cond_6

    .line 265
    new-instance v13, Ljava/util/Vector;

    invoke-direct {v13}, Ljava/util/Vector;-><init>()V

    .line 267
    .local v13, "cntlVector":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;>;"
    const/4 v9, 0x2

    .line 268
    .local v9, "idirn":I
    move/from16 v19, v5

    .line 269
    .local v19, "xStart":I
    move/from16 v20, v6

    .line 271
    .local v20, "yStart":I
    add-int/lit8 v3, v5, -0x1

    int-to-float v3, v3

    add-int/lit8 v4, v6, -0x1

    int-to-float v4, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 272
    new-instance v3, Landroid/graphics/Point;

    add-int/lit8 v4, v5, -0x1

    add-int/lit8 v7, v6, -0x1

    invoke-direct {v3, v4, v7}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 275
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    mul-int/2addr v4, v6

    add-int/2addr v4, v5

    const/16 v7, 0x21

    aput-byte v7, v3, v4

    .line 277
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskHeight:I

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->getNextCntr([BIIIII)Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;

    move-result-object v10

    .line 278
    .local v10, "c":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    if-eqz v10, :cond_5

    .line 280
    iget-object v3, v10, Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;->position:Landroid/graphics/Point;

    iget v5, v3, Landroid/graphics/Point;->x:I

    .line 281
    iget-object v3, v10, Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;->position:Landroid/graphics/Point;

    iget v6, v3, Landroid/graphics/Point;->y:I

    .line 282
    iget v9, v10, Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;->idirn:I

    .line 283
    invoke-virtual {v13, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 284
    add-int/lit8 v3, v5, -0x1

    int-to-float v3, v3

    add-int/lit8 v4, v6, -0x1

    int-to-float v4, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 285
    new-instance v3, Landroid/graphics/Point;

    add-int/lit8 v4, v5, -0x1

    add-int/lit8 v7, v6, -0x1

    invoke-direct {v3, v4, v7}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 288
    :cond_5
    move/from16 v0, v19

    if-ne v5, v0, :cond_4

    move/from16 v0, v20

    if-ne v6, v0, :cond_4

    .line 259
    .end local v9    # "idirn":I
    .end local v10    # "c":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    .end local v13    # "cntlVector":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;>;"
    .end local v19    # "xStart":I
    .end local v20    # "yStart":I
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_3

    .line 307
    .end local v5    # "x":I
    :cond_7
    move-object/from16 v0, p5

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    goto/16 :goto_0
.end method

.method public contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Z)V
    .locals 22
    .param p1, "mask"    # [B
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "maskRoi"    # Landroid/graphics/Rect;
    .param p5, "path"    # Landroid/graphics/Path;
    .param p6, "added"    # Z

    .prologue
    .line 179
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    if-nez v3, :cond_0

    .line 240
    :goto_0
    return-void

    .line 182
    :cond_0
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    move/from16 v0, p3

    if-lt v15, v0, :cond_1

    .line 191
    new-instance v17, Landroid/graphics/Path;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Path;-><init>()V

    .line 192
    .local v17, "path2":Landroid/graphics/Path;
    move-object/from16 v0, p4

    iget v3, v0, Landroid/graphics/Rect;->top:I

    add-int/lit8 v6, v3, 0x1

    .local v6, "y":I
    :goto_2
    move-object/from16 v0, p4

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v3, v3, 0x1

    if-lt v6, v3, :cond_2

    .line 227
    if-eqz p6, :cond_7

    .line 229
    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11}, Landroid/graphics/RectF;-><init>()V

    .line 230
    .local v11, "clip1":Landroid/graphics/RectF;
    const/4 v3, 0x1

    move-object/from16 v0, p5

    invoke-virtual {v0, v11, v3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 231
    new-instance v12, Landroid/graphics/RectF;

    invoke-direct {v12}, Landroid/graphics/RectF;-><init>()V

    .line 232
    .local v12, "clip2":Landroid/graphics/RectF;
    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 233
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v11, v2, v12}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->pathUnion(Landroid/graphics/Path;Landroid/graphics/RectF;Landroid/graphics/Path;Landroid/graphics/RectF;)Landroid/graphics/Path;

    move-result-object v16

    .line 234
    .local v16, "p":Landroid/graphics/Path;
    move-object/from16 v0, p5

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    goto :goto_0

    .line 184
    .end local v6    # "y":I
    .end local v11    # "clip1":Landroid/graphics/RectF;
    .end local v12    # "clip2":Landroid/graphics/RectF;
    .end local v16    # "p":Landroid/graphics/Path;
    .end local v17    # "path2":Landroid/graphics/Path;
    :cond_1
    mul-int v3, v15, p2

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v3, v1}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v18

    .line 185
    .local v18, "src":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    add-int/lit8 v4, v15, 0x1

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    mul-int/2addr v4, v7

    add-int/lit8 v4, v4, 0x1

    move/from16 v0, p2

    invoke-static {v3, v4, v0}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v14

    .line 186
    .local v14, "dst":Ljava/nio/ByteBuffer;
    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 182
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 194
    .end local v14    # "dst":Ljava/nio/ByteBuffer;
    .end local v18    # "src":Ljava/nio/ByteBuffer;
    .restart local v6    # "y":I
    .restart local v17    # "path2":Landroid/graphics/Path;
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    mul-int v21, v6, v3

    .line 195
    .local v21, "yy":I
    move-object/from16 v0, p4

    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v5, v3, 0x1

    .local v5, "x":I
    :goto_3
    move-object/from16 v0, p4

    iget v3, v0, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, 0x1

    if-lt v5, v3, :cond_3

    .line 192
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 197
    :cond_3
    add-int v3, v21, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    array-length v4, v4

    if-ge v3, v4, :cond_6

    .line 198
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    add-int v4, v21, v5

    aget-byte v3, v3, v4

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    .line 199
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    add-int/lit8 v4, v5, -0x1

    add-int v4, v4, v21

    aget-byte v3, v3, v4

    if-nez v3, :cond_6

    .line 201
    new-instance v13, Ljava/util/Vector;

    invoke-direct {v13}, Ljava/util/Vector;-><init>()V

    .line 203
    .local v13, "cntlVector":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;>;"
    const/4 v9, 0x2

    .line 204
    .local v9, "idirn":I
    move/from16 v19, v5

    .line 205
    .local v19, "xStart":I
    move/from16 v20, v6

    .line 207
    .local v20, "yStart":I
    add-int/lit8 v3, v5, -0x1

    int-to-float v3, v3

    add-int/lit8 v4, v6, -0x1

    int-to-float v4, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 210
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    mul-int/2addr v4, v6

    add-int/2addr v4, v5

    const/16 v7, 0x21

    aput-byte v7, v3, v4

    .line 212
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskHeight:I

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->getNextCntr([BIIIII)Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;

    move-result-object v10

    .line 213
    .local v10, "c":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    if-eqz v10, :cond_5

    .line 215
    iget-object v3, v10, Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;->position:Landroid/graphics/Point;

    iget v5, v3, Landroid/graphics/Point;->x:I

    .line 216
    iget-object v3, v10, Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;->position:Landroid/graphics/Point;

    iget v6, v3, Landroid/graphics/Point;->y:I

    .line 217
    iget v9, v10, Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;->idirn:I

    .line 218
    invoke-virtual {v13, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 219
    add-int/lit8 v3, v5, -0x1

    int-to-float v3, v3

    add-int/lit8 v4, v6, -0x1

    int-to-float v4, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 222
    :cond_5
    move/from16 v0, v19

    if-ne v5, v0, :cond_4

    move/from16 v0, v20

    if-ne v6, v0, :cond_4

    .line 195
    .end local v9    # "idirn":I
    .end local v10    # "c":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    .end local v13    # "cntlVector":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;>;"
    .end local v19    # "xStart":I
    .end local v20    # "yStart":I
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_3

    .line 238
    .end local v5    # "x":I
    .end local v21    # "yy":I
    :cond_7
    move-object/from16 v0, p5

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    goto/16 :goto_0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    .line 47
    return-void
.end method

.method public init(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 39
    add-int/lit8 v0, p1, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    .line 40
    add-int/lit8 v0, p2, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskHeight:I

    .line 41
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskHeight:I

    mul-int/2addr v0, v1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    .line 42
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 43
    return-void
.end method

.method public inverseContourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;)V
    .locals 18
    .param p1, "mask"    # [B
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "maskRoi"    # Landroid/graphics/Rect;
    .param p5, "path"    # Landroid/graphics/Path;

    .prologue
    .line 50
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    if-nez v2, :cond_0

    .line 99
    :goto_0
    return-void

    .line 52
    :cond_0
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Path;->rewind()V

    .line 53
    const/16 p5, 0x0

    .line 54
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    move/from16 v0, p3

    if-lt v12, v0, :cond_1

    .line 61
    const/4 v13, 0x0

    .line 63
    .local v13, "idx":I
    new-instance v14, Landroid/graphics/Path;

    invoke-direct {v14}, Landroid/graphics/Path;-><init>()V

    .line 64
    .local v14, "path2":Landroid/graphics/Path;
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/lit8 v5, v2, 0x1

    .local v5, "y":I
    :goto_2
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v2, v2, 0x1

    if-lt v5, v2, :cond_2

    .line 98
    move-object/from16 p5, v14

    .line 99
    goto :goto_0

    .line 56
    .end local v5    # "y":I
    .end local v13    # "idx":I
    .end local v14    # "path2":Landroid/graphics/Path;
    :cond_1
    mul-int v2, v12, p2

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v2, v1}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v15

    .line 57
    .local v15, "src":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    add-int/lit8 v3, v12, 0x1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    mul-int/2addr v3, v6

    add-int/lit8 v3, v3, 0x1

    move/from16 v0, p2

    invoke-static {v2, v3, v0}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 58
    .local v11, "dst":Ljava/nio/ByteBuffer;
    invoke-virtual {v11, v15}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 54
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 66
    .end local v11    # "dst":Ljava/nio/ByteBuffer;
    .end local v15    # "src":Ljava/nio/ByteBuffer;
    .restart local v5    # "y":I
    .restart local v13    # "idx":I
    .restart local v14    # "path2":Landroid/graphics/Path;
    :cond_2
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v4, v2, 0x1

    .local v4, "x":I
    :goto_3
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Rect;->right:I

    add-int/lit8 v2, v2, 0x1

    if-lt v4, v2, :cond_3

    .line 64
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 68
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    mul-int/2addr v2, v5

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    array-length v3, v3

    if-ge v2, v3, :cond_6

    .line 69
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    mul-int/2addr v3, v5

    add-int/2addr v3, v4

    aget-byte v2, v2, v3

    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    .line 70
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    mul-int/2addr v3, v5

    add-int/lit8 v6, v4, -0x1

    add-int/2addr v3, v6

    aget-byte v2, v2, v3

    if-nez v2, :cond_6

    .line 72
    new-instance v10, Ljava/util/Vector;

    invoke-direct {v10}, Ljava/util/Vector;-><init>()V

    .line 74
    .local v10, "cntlVector":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;>;"
    const/4 v8, 0x2

    .line 75
    .local v8, "idirn":I
    move/from16 v16, v4

    .line 76
    .local v16, "xStart":I
    move/from16 v17, v5

    .line 78
    .local v17, "yStart":I
    add-int/lit8 v2, v4, -0x1

    int-to-float v2, v2

    add-int/lit8 v3, v5, -0x1

    int-to-float v3, v3

    invoke-virtual {v14, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 81
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    mul-int/2addr v3, v5

    add-int/2addr v3, v4

    const/16 v6, 0x21

    aput-byte v6, v2, v3

    .line 83
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMask:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->mChainContourMaskHeight:I

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->getNextCntr([BIIIII)Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;

    move-result-object v9

    .line 84
    .local v9, "c":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    if-eqz v9, :cond_5

    .line 86
    iget-object v2, v9, Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;->position:Landroid/graphics/Point;

    iget v4, v2, Landroid/graphics/Point;->x:I

    .line 87
    iget-object v2, v9, Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;->position:Landroid/graphics/Point;

    iget v5, v2, Landroid/graphics/Point;->y:I

    .line 88
    iget v8, v9, Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;->idirn:I

    .line 89
    invoke-virtual {v10, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 90
    add-int/lit8 v2, v4, -0x1

    int-to-float v2, v2

    add-int/lit8 v3, v5, -0x1

    int-to-float v3, v3

    invoke-virtual {v14, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 93
    :cond_5
    move/from16 v0, v16

    if-ne v4, v0, :cond_4

    move/from16 v0, v17

    if-ne v5, v0, :cond_4

    .line 94
    add-int/lit8 v13, v13, 0x1

    .line 66
    .end local v8    # "idirn":I
    .end local v9    # "c":Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;
    .end local v10    # "cntlVector":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/sec/android/mimage/photoretouching/Core/ChainContour$Cntr;>;"
    .end local v16    # "xStart":I
    .end local v17    # "yStart":I
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_3
.end method
